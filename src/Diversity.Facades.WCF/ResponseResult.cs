﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Diversity.Domain.Model;

namespace Diversity.Facades.WCF
{
    [DataContract(Namespace="")]
    public class ResponseResult<T>
    {
        private readonly List<ErrorMessage> _errors;
        
        public ResponseResult()
        {
            _errors = new List<ErrorMessage>();
        }

        /// <summary>
        /// Result of the contract/operation
        /// </summary>
        [DataMember]
        public T Result { get; set; }

        /// <summary>
        /// Error messages of associated contract/operation
        /// </summary>
        [DataMember]
        public List<ErrorMessage> Errors
        {
            get { return _errors; }
        }
    }
}