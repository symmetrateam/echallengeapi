﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Diversity.Common.Exceptions;
using Diversity.Domain.Model;

namespace Diversity.Facades.WCF
{    
    /// <summary>
    /// This services handles all the authentication, authorization and
    /// security related actions available in the system.
    /// </summary>
    [ServiceContract(Namespace = "")]
    public interface IAuthenticationService
    {
        /// <summary>
        /// Authenticates a user arriving from a SCORM compliant learning management system
        /// </summary>
        /// <param name="username">The learning management system identifier</param>
        /// <param name="clientCode">The learning management system client identifier</param>
        /// <exception cref="AuthenticationFailedException">The user failed to authenticate correctly</exception>
        /// <exception cref="ClientException">An exception with a dynamic message indicating an error occured with the specific <paramref name="clientCode"/></exception>
        /// <exception cref="UserException">An exception with a dynamic message indicating an error occured with the specific <paramref name="username"/></exception>
        /// <returns>An <c>AuthenticationTicket</c> indicating a success</returns>
        [OperationContract]
        [Description("Authenticate a scorm user")]
        [FaultContract(typeof(AuthenticationFailedException))]
        [FaultContract(typeof(ClientException))]
        [FaultContract(typeof(UserException))]
        AuthenticationTicket AuthenticateScorm(string username, string clientCode);

        /// <summary>
        /// Authenticates a user
        /// </summary>
        /// <param name="username">The username</param>
        /// <param name="password">The password</param>
        /// <exception cref="AuthenticationFailedException">The user failed to authenticate correctly</exception>
        /// <returns>An <c>AuthenticationTicket</c> indicating a success</returns>
        [OperationContract]
        [Description("Authenticate the user")]        
        [FaultContract(typeof(AuthenticationFailedException))]
        AuthenticationTicket Authenticate(string username, string password);

        /// <summary>
        /// Allows for the reset of a user password and provides a notification to the registered email address
        /// </summary>
        /// <param name="username">The username</param>
        /// <param name="message">A message indicating an outcome for display</param>
        /// <exception cref="AuthenticationFailedException">The user was not found</exception>
        /// <exception cref="PasswordResetException">The password reset failed</exception>
        [OperationContract]
        [Description("Resets the users password and notifies them via email")]
        [FaultContract(typeof(AuthenticationFailedException))]
        [FaultContract(typeof(PasswordResetException))]
        void ResetPassword(string username, out string message);

        /// <summary>
        /// Allows an authenticated user to change their password
        /// </summary>
        /// <param name="oldPassword">The old password</param>
        /// <param name="newPassword">The new password</param>
        /// <exception cref="CustomValidationException">A dynamic message indicating a failure in complex validation</exception>
        /// <exception cref="AuthenticationFailedException">The user was not authenticated</exception>
        /// <exception cref="PasswordResetException">The password reset failed</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>
        [OperationContract]
        [Description("Change a users password")]
        [FaultContract(typeof(CustomValidationException))]
        [FaultContract(typeof(AuthenticationFailedException))]
        [FaultContract(typeof(PasswordResetException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        void ChangePassword(string oldPassword, string newPassword);

        /// <summary>
        /// Determines if the authentication service is available for use
        /// </summary>
        /// <returns>A boolean</returns>        
        /// <remarks>Only accessible if there is an underlying communication connection to the service</remarks>
        [OperationContract]
        bool IsAuthenticationServiceAvailable();

        /// <summary>
        /// Indicates if a user is authenticated
        /// </summary>
        /// <param name="authToken">The authentication token</param>
        /// <param name="userId">Outputs the user identifier</param>
        /// <param name="isTimedOut">Outputs a value indicating timeout has occurred in the authenticated session</param>
        /// <returns>A boolean indicating if the user is authenticated</returns>
        bool IsAuthenticated(string authToken, out Guid userId, out bool isTimedOut);

        /// <summary>
        /// Indicates if the user session is authenticated
        /// </summary>        
        /// <param name="isTimedOut">Outputs a value indicating timeout has occurred in the authenticated session</param>
        /// <returns>A boolean indicating if the user is authenticated</returns>
        bool IsAuthenticated(out bool isTimedOut);

        /// <summary>
        /// Indicates if the user session is authenticated
        /// </summary>        
        /// <param name="userId">Outputs the user identifier of the authenticated user</param>
        /// <param name="isTimedOut">Outputs a value indicating timeout has occurred in the authenticated session</param>
        /// <returns>A boolean indicating if the user is authenticated</returns>
        bool IsAuthenticated(out bool isTimedOut, out Guid userId);
    }
}
