﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Diversity.Common.Exceptions;
using Diversity.Domain.Model;

namespace Diversity.Facades.WCF
{
    /// <summary>
    /// This class caters for all actions that can be taken by the user in relation
    /// to games that are associated to their authenticated session
    /// </summary>
    [ServiceContract(Namespace = "")]
    [ServiceKnownType(typeof(MultipleChoicePlayerResult))]
    [ServiceKnownType(typeof(MultipleCheckBoxPlayerResult))]
    [ServiceKnownType(typeof(TextPlayerResult))]
    [ServiceKnownType(typeof(VotingPlayerResult))]
    [ServiceKnownType(typeof(ArrangeSequencePlayerResult))]
    [ServiceKnownType(typeof(MatchingListPlayerResult))]
    [ServiceKnownType(typeof(NumericPlayerResult))]
    [ServiceKnownType(typeof(FillInTheBlankPlayerResult))]
    public interface IGameService
    {

        /// <summary>
        /// Determines if the game service is available for use
        /// </summary>
        /// <returns>A boolean</returns>        
        /// <remarks>Only accessible if there is an underlying communication connection to the service</remarks>
        [OperationContract]
        bool IsGameServiceAvailable();

        /// <summary>
        /// Gets all the languages available for use
        /// </summary>
        /// <returns>A list of <c>Language</c></returns>
        /// <remarks>A language is used by the service to return appropriately transalated data</remarks>
        /// <exception cref="ServiceControllerException">A service generated exception with a dynamic custom error message</exception>
        [OperationContract]
        [Description("Gets a list of all languages")]
        [FaultContract(typeof(ServiceControllerException))]
        List<Language> GetLanguages();

        /// <summary>
        /// Get a list of stored user assets
        /// </summary>
        /// <returns>A list of flash assests</returns>        
        /// <exception cref="ServiceControllerException">A service generated exception with a dynamic custom error message</exception>
        [OperationContract]
        [Description("Gets a list of all flash assets")]
        [FaultContract(typeof(ServiceControllerException))]
        List<FlashAsset> GetFlashAssets();

        /// <summary>
        /// Gets a game session for a user. A session includes the currently logged in session and the entire game associated
        /// to the session including the user's previous results if the game has been resumed
        /// </summary>
        /// <param name="gameId">The game identifier</param>
        /// <returns>A <c>Session</c> containing the game information for the current logged in session</returns>
        /// <exception cref="RangeValidationException">
        /// if <paramref name="gameId"/> is less than or equal to zero
        /// </exception>
        /// <exception cref="ServiceControllerException">A service generated exception with a dynamic custom error message</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>
        [OperationContract]
        [Description("Gets are game session")]
        [FaultContract(typeof(RangeValidationException))]
        [FaultContract(typeof(ServiceControllerException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        Session GetGameSession(int gameId);

        /// <summary>
        /// Gets the dashboard information
        /// </summary>
        /// <returns>
        /// A <c>Dashboard</c> which contains the list of games a user has assigned to them and each of their status, 
        /// as well as other miscellaneous data
        /// </returns>
        /// <exception cref="ServiceControllerException">A service generated exception with a dynamic custom error message</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>        
        [OperationContract]
        [Description("Gets the dashboard")]
        [FaultContract(typeof(ServiceControllerException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        Dashboard GetDashboard();

        /// <summary>
        /// Saves the players profile data
        /// </summary>
        /// <param name="firstname">The player's first name</param>
        /// <param name="lastname">The player's last name</param>
        /// <returns>A boolean indicating success</returns>
        /// <exception cref="RequiredValidationException">indicates that either <paramref name="firstname"/> 
        /// or <paramref name="lastname"/> must not be <c>null</c> or an empty <c>string</c>
        /// </exception>
        /// <exception cref="ServiceControllerException">a service generated exception with a dynamic custom error message</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>   
        [OperationContract]
        [Description("Save a players profile")]
        [FaultContract(typeof(RequiredValidationException))]
        [FaultContract(typeof(ServiceControllerException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        bool SavePlayerProfile(string firstname, string lastname);

        /// <summary>
        /// Saves the player's avatar profile information
        /// </summary>
        /// <param name="avatarName">The avatar name assigned to the player</param>
        /// <param name="avatarMetaData">Any meta data that is associated to the avatar</param>
        /// <returns>A boolean indicating success</returns>
        /// <exception cref="RequiredValidationException">
        /// indicates that either <paramref name="avatarName"/> or <paramref name="avatarMetaData"/> 
        /// must not be <c>null</c> or an empty <c>string</c>
        /// </exception>
        /// <exception cref="ServiceControllerException">a service generated exception with a dynamic custom error message</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>   
        [OperationContract]
        [Description("Save the players avatar profile")]
        [FaultContract(typeof(RequiredValidationException))]
        [FaultContract(typeof(ServiceControllerException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        bool SaveAvatarProfile(string avatarName, string avatarMetaData);

        /// <summary>
        /// Saves the players result (answer) that was supplied for the associated card
        /// </summary>
        /// <param name="cardSequenceId">The card sequence identier of the card</param>
        /// <param name="result">
        /// <para>
        /// The response of the player as a <c>PlayerResult</c>. 
        /// </para>
        /// <para>
        /// The <c>PlayerResult</c> type should be based on the type of card for which the result is being supplied. The specialized
        /// types of <c>PlayerResult</c> could be any of the following:
        /// <list type="bullet">
        /// <item><c>MultipleChoicePlayerResult</c></item>
        /// <item><c>ArrangeSequencePlayerResult</c></item>
        /// <item><c>FillInTheBlankPlayerResult</c></item>
        /// <item><c>MatchingListPlayerResult</c></item>
        /// <item><c>MultipleCheckBoxPlayerResult</c></item>
        /// <item><c>NumericPlayerResult</c></item>
        /// <item><c>TextPlayerResult</c></item>
        /// <item><c>VotingPlayerResult</c></item>
        /// <item><c>BranchingCardPlayerResult</c></item>
        /// <item><c>VotingCardFeedbackResult</c></item>
        /// </list>
        /// </para>
        /// </param>
        /// <returns>A boolean indicating success</returns>
        /// <exception cref="RangeValidationException">
        /// if <paramref name="cardSequenceId"/> is less than or equal to zero
        /// </exception>
        /// <exception cref="RequiredValidationException">
        /// if <paramref name="result"/> is <c>null</c> for the associated <paramref name="cardSequenceId"/> that was supplied/> 
        /// </exception>
        /// <exception cref="ServiceControllerException">a service generated exception with a dynamic custom error message</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>           
        [OperationContract]
        [Description("Save the result of a card that requires a response")]
        [FaultContract(typeof(RangeValidationException))]
        [FaultContract(typeof(RequiredValidationException))]
        [FaultContract(typeof(ServiceControllerException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        [FaultContract(typeof(CustomValidationException))]
        [FaultContract(typeof(CardException))]
        bool SaveCardResult(int cardSequenceId, PlayerResult result);

        /// <summary>Allows for a card to be skipped within a game</summary>
        /// <param name="cardSequenceId"></param>
        /// <returns>A boolean indicating success</returns>
        /// <exception cref="RangeValidationException">
        /// if <paramref name="cardSequenceId"/> is less than or equal to zero
        /// </exception>
        /// <exception cref="ServiceControllerException">a service generated exception with a dynamic custom error message</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>   
        [OperationContract]
        [Description("Skip a card and mark it as viewed within the game.")]
        [FaultContract(typeof(RangeValidationException))]
        [FaultContract(typeof(ServiceControllerException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        bool SkipCard(int cardSequenceId);

        /// <summary>Closes off a game instance so that it cannot be played again</summary>
        /// <param name="gameId"></param>        
        /// <returns>A boolean indicating success</returns>
        /// <exception cref="RangeValidationException">
        /// if <paramref name="gameId"/> is less than or equal to zero
        /// </exception>
        /// <exception cref="ServiceControllerException">a service generated exception with a dynamic custom error message</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>  
        /// <exception cref="ServiceControllerException">a service generated exception with a dynamic custom error message</exception>
        /// <exception cref="GameException">occurs if constraints and errors occur in relation to the actual game. this is a dynamic custom error message</exception>
        /// <remarks>A game can only be set as completed if:
        /// <list type="bullet">
        /// <item>all mandatory cards have been answered</item>
        /// <item>all cards that were required to be view have been seen by the user</item>
        /// </list>
        /// </remarks> 
        [OperationContract]
        [Description("Sets a game to completed")]
        [FaultContract(typeof(RangeValidationException))]
        [FaultContract(typeof(ServiceControllerException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        [FaultContract(typeof(GameException))]
        bool SetGameAsCompleted(int gameId);

        /// <summary>Registers a learning management system game against the player</summary>
        /// <param name="courseCode">A learning management system code</param>        
        /// <param name="themeCode">A learning management system theme code.</param>
        /// <returns>An integer representing the game identifier</returns>
        /// <exception cref="RequiredValidationException">
        /// if <paramref name="courseCode"/> is equal to <c>null</c> or empty <c>string</c>
        /// </exception>
        /// <exception cref="RangeValidationException">
        /// if <paramref name="courseCode"/> cannot map to an appropriate game identifer
        /// </exception>
        /// <exception cref="ServiceControllerException">a service generated exception with a dynamic custom error message</exception>
        /// <exception cref="SessionTimeoutException">the user session has expired</exception>
        /// <exception cref="UnauthorizedAccessException">the user is not authenticated</exception>  
        /// <exception cref="ServiceControllerException">a service generated exception with a dynamic custom error message</exception>        
        [OperationContract]
        [Description("Registers a scorm course to the user and returns the game identifier")]
        [FaultContract(typeof(RequiredValidationException))]
        [FaultContract(typeof(RangeValidationException))]
        [FaultContract(typeof(ServiceControllerException))]
        [FaultContract(typeof(SessionTimeoutException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        int RegisterScormCourseCode(string courseCode, string themeCode);

    }
}
