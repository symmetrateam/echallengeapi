﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using ComLib.Account;
using Diversity.Common;
using Diversity.Common.Exceptions;
using Diversity.Domain.Model;
using Diversity.Facades.WCF.MyResources;
using Diversity.Services.Business;
using Diversity.Services.Business.Implementation;
using log4net;

namespace Diversity.Facades.WCF
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]   
    public class AuthenticationService : IAuthenticationService
    {
        private static readonly ILog _log = LogManager.GetLogger((System.Reflection.MethodBase.GetCurrentMethod().DeclaringType));
        private const string PASSWORD_RESET_EMAIL = @"~/MyResources/Emails/ForgotPassword.txt";
        private const string EMAIL_SUBJECT = "e-Challenge Password Reset";

        private readonly IAuthenticationControllerService _authenticationControllerService;
                

        public AuthenticationService()
        {
            _authenticationControllerService = new AuthenticationControllerService();            
        }

        [OperationBehavior]
        public AuthenticationTicket AuthenticateScorm(string username, string clientCode)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(clientCode))
            {
                _log.DebugFormat("The username or clientcode supplied was an empty string. username:{0}, clientcode:{1}", username, clientCode);

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Login_Failed_Scorm);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.InvalidUsernameOrClientCodeMessage), faultReason);
            }

            var hostAddress = HttpContext.Current.Request.UserHostAddress;

            bool newUserCreated;
            MembershipUser membershipUser = null;

            try
            {
                membershipUser = _authenticationControllerService.AuthenticateScormUser(username, clientCode,
                    hostAddress, out newUserCreated);
            }
            catch (MembershipCreateUserException ex)
            {
                _log.Debug(@"AuthenticateScormUser failed to create a new user.");
                _log.DebugFormat(@"Failed with status code = {0}", Enum.GetName(typeof(MembershipCreateStatus), ex.StatusCode));

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Create_User);
                throw new FaultException<UserException>(new UserException(Messages.UnableToCreateNewUser), faultReason);   
            }
            catch (InvalidClientException)
            {                
                _log.Debug(@"AuthenticateScormUser failed due to invalid client code. Unable to find client.");
                _log.DebugFormat(@"Client Code = {0}", clientCode);

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Client_Code);
                throw new FaultException<ClientException>(new ClientException(Messages.InvalidClientCodeMessage), faultReason);
            }

            if (membershipUser == null)
            {
                _log.DebugFormat("Unable to find user with the supplied credentials {0} and {1}.", username, clientCode);

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Login_Failed_Scorm);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.InvalidUsernameOrClientCodeMessage), faultReason);
            }
            
            if (newUserCreated)
            {
                IGameControllerService service = new GameControllerService();
                var firstName = username.ToUpper();
                var lastName = clientCode.ToUpper();

                service.SaveScormPlayerProfile((Guid) membershipUser.ProviderUserKey, username, firstName, lastName, membershipUser.Email);                
            }

            return CompleteAuthenticationVerification(membershipUser);
        }

        [OperationBehavior]
        public AuthenticationTicket Authenticate(string username, string password)
        {
            if(string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                _log.DebugFormat("The username or password supplied was an empty string. username:{0}, password:{1}", username, password);

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Login_Failed);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.InvalidUsernameOrPasswordMessage), faultReason);                
            }            

            if(!Regex.IsMatch(username, ConfigurationHelper.EmailValidationRegex))
            {
                _log.DebugFormat("The username as email address failed regular expression validation check. email:{0}", username);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Email);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.InvalidEmailAddressMessage), faultReason);                
            }

            var hostAddress = HttpContext.Current.Request.UserHostAddress;           

            var membershipUser = _authenticationControllerService.Authenticate(username, password, hostAddress);

            if (membershipUser == null)
            {
                _log.DebugFormat("Unable to find user with the supplied credentials {0} and {1}.", username, password);

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Login_Failed);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.InvalidUsernameOrPasswordMessage), faultReason);
            }

            _log.DebugFormat(@"Authetication - Username: {0}", membershipUser.UserName);
            _log.DebugFormat(@"Authetication - Password: {0}", password);
            return CompleteAuthenticationVerification(membershipUser);
        }

        AuthenticationTicket CompleteAuthenticationVerification(MembershipUser membershipUser)
        {            
            if (!membershipUser.IsApproved)
            {
                _log.DebugFormat("The account for {0} is not activated.", membershipUser.UserName);


                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Account_Not_Activated);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.AccountNotActivatedMessage), faultReason);
            }

            if (membershipUser.IsLockedOut)
            {
                _log.DebugFormat("The account for {0} is locked.", membershipUser.UserName);

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Account_Locked);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.AccountLockedMessage), faultReason);
            }

            var userId = (Guid)membershipUser.ProviderUserKey;

            var authenticationTicket = _authenticationControllerService.GetAuthenticationTicket(userId);

            var key = string.Format(@"{0}{1}", userId, DateTime.Now.Ticks.ToString());
            var sha = SHA256.Create();
            var authToken = Convert.ToBase64String(sha.ComputeHash(Encoding.UTF8.GetBytes(key)));
            var expiresAt = DateTime.Now.AddMinutes(ConfigurationHelper.TimeoutInMinutes);

            if (authenticationTicket == null)
            {
                authenticationTicket = new AuthenticationTicket
                {
                    UserId = userId,
                    Token = authToken,
                    IsFirstLogin = true,
                    ExpiresAt = expiresAt
                };

                _authenticationControllerService.CreateAuthenticationTicket(authenticationTicket);
                authenticationTicket = _authenticationControllerService.GetAuthenticationTicket(userId);
            }
            else
            {
                authenticationTicket.Token = authToken;
                authenticationTicket.ExpiresAt = expiresAt;
                _authenticationControllerService.RenewAuthenticationTicket(authenticationTicket);
            }

            _log.DebugFormat(@"Authetication - Username: {0}", membershipUser.UserName);
            _log.DebugFormat(@"Authentication Ticket information for: {0}", userId);
            _log.DebugFormat(@"Authetication Token: {0}", authenticationTicket.Token);
            _log.DebugFormat(@"Authentication Token Expires At: {0}", authenticationTicket.ExpiresAt);
            _log.DebugFormat(@"Authentication IsFirstLogin : {0}", authenticationTicket.IsFirstLogin);

            return authenticationTicket;
        }

        [OperationBehavior]
        public void ResetPassword(string username, out string message)
        {
            _log.Debug(@"Entered ResetPassword");

            if(string.IsNullOrEmpty(username) || string.IsNullOrWhiteSpace(username))
            {
                _log.Debug(@"Unable to reset password as the username supplied is an empty string.");               

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Username);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.InvalidUsernameMessage), faultReason);         
            }

            if (!Regex.IsMatch(username, ConfigurationHelper.EmailValidationRegex))
            {
                _log.DebugFormat(@"Unable to reset password as the username/email ({0}) supplied does not match the regular expression pattern for an email address", username);

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Email);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.InvalidEmailAddressMessage), faultReason);                
            }

            var user = _authenticationControllerService.GetUser(username);

            if(user == null)
            {
                _log.DebugFormat(@"Unable to reset password. Cannot find user with username {0}", username);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Username);
                throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.InvalidUsernameMessage), faultReason);                
            }

            try
            {
                var userId = (Guid) user.ProviderUserKey;
                string password;
                var result = _authenticationControllerService.ResetPassword(userId, out password);
                
                if (result)
                {
                    var addresses = new List<MailAddress>();
                    addresses.Add(new MailAddress(user.Email));

                    var replacements = new Dictionary<string, string>();
                    replacements.Add("<%UserName%>", username);
                    replacements.Add("<%Password%>", password);

                    SendMessage(EMAIL_SUBJECT, replacements, addresses, PASSWORD_RESET_EMAIL, false);

                    message = ErrorMessageProvider.Instance.GetMessageDescription(ErrorCodes.Password_Reset_Email_Sent);                    
                }
                else
                {
                    _log.DebugFormat(@"Password reset on auth controller returned false. Unable to reset password for username: {0}.", username);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Password_Reset_Email_Failed);
                    throw new FaultException<PasswordResetException>(new PasswordResetException(Messages.PasswordResetEmailFailedMessage), faultReason);                    
                }
            }
            catch (Exception ex)
            {                
                _log.Error(string.Format(@"Exception thrown. Unable to reset password for username: {0}.", username), ex);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Password_Reset_Email_Failed);
                throw new FaultException<PasswordResetException>(new PasswordResetException(Messages.PasswordResetEmailFailedMessage), faultReason);
            }
            finally
            {
                _log.Debug(@"Exiting Reset Password");
            }                
        }

        [OperationBehavior]
        public void ChangePassword(string oldPassword, string newPassword)
        {
            _log.Debug(@"Entered ChangePassword");

            if(!_authenticationControllerService.PassedMinimumPasswordRules(oldPassword))
            {
                _log.DebugFormat(@"Old password does not meet the password requirements.");
                
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Minimum_Password_Requirement);
                throw new FaultException<CustomValidationException>(new CustomValidationException("oldPassword", Messages.MinimumPasswordRequirementMessage), faultReason);                
            }

            if(!_authenticationControllerService.PassedMinimumPasswordRules(newPassword))
            {
                _log.DebugFormat(@"New Password does not meet the minimum password requirements");

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Minimum_Password_Requirement);
                throw new FaultException<CustomValidationException>(new CustomValidationException("newPassword", Messages.MinimumPasswordRequirementMessage), faultReason);                
            }

            bool isTimedOut;
            Guid userId;
            if (IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                MembershipUser authenticatedUser = null;

                try
                {
                    var hostAddress = HttpContext.Current.Request.UserHostAddress;
                    var user = _authenticationControllerService.GetUser(userId);
                    authenticatedUser = _authenticationControllerService.Authenticate(user.UserName, oldPassword, hostAddress);

                    if (authenticatedUser != null)
                    {
                        _authenticationControllerService.ChangePassword(userId, oldPassword, newPassword);

                        return;
                    }                    
                }
                catch (Exception ex)
                {
                    _log.DebugFormat(@"Exception thrown: Unable to change password for userID:{0}", userId);
                    _log.Debug(@"PasswordChangeFailedException", ex);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Change_Password_Failed);
                    throw new FaultException<PasswordResetException>(new PasswordResetException(Messages.UnableToChangePasswordMessage), faultReason);
                }
                finally
                {
                    _log.Debug(@"Exiting ChangePassword");                                        
                }

                if (authenticatedUser == null)
                {
                    _log.DebugFormat(@"Unable to change password. Unable to authenticate the user as an invalid old password was supplied for userID: {0}.", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Password_Change_Old_Password_Failed);
                    throw new FaultException<AuthenticationFailedException>(new AuthenticationFailedException(Messages.PasswordChangeOldPasswordIncorrect), faultReason);
                }
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }

        [OperationBehavior]
        public bool IsAuthenticationServiceAvailable()
        {
            return true;
        }

        #region Public Helper Methods

        public bool IsAuthenticated(string authToken, out Guid userId, out bool isTimedOut)
        {
            userId = Guid.Empty;
            isTimedOut = false;

            if(string.IsNullOrEmpty(authToken) || string.IsNullOrWhiteSpace(authToken))
            {
                return false;
            }

            var authenticationTicket = _authenticationControllerService.GetAuthenticationTicket(authToken);

            if(authenticationTicket == null)
            {
                return false;
            }

            if(authenticationTicket.ExpiresAt < DateTime.Now)
            {
                isTimedOut = true;
                _log.DebugFormat(@"Authentication Ticket expired for user: {0}", authenticationTicket.UserId);
                return false;
            }

            userId = authenticationTicket.UserId;
            authenticationTicket.ExpiresAt = DateTime.Now.AddMinutes(ConfigurationHelper.TimeoutInMinutes);
            return _authenticationControllerService.RenewAuthenticationTicket(authenticationTicket);
        }

        string GetAuthTokenFromHeader()
        {
            return OperationContext.Current.IncomingMessageHeaders.GetHeader<string>("Authorization", string.Empty);
        }

        public bool IsAuthenticated( out bool isTimedOut)
        {
            var userId = Guid.Empty;
            var authToken = GetAuthTokenFromHeader();
            return IsAuthenticated(authToken, out userId, out isTimedOut);
        }

        public bool IsAuthenticated(out bool isTimedOut, out Guid userId)
        {
            var authToken = GetAuthTokenFromHeader();
            return IsAuthenticated(authToken, out userId, out isTimedOut);
        }

        #endregion

        #region Private Methods

        static void SendMessage(string subject, Dictionary<string, string> replacements, IEnumerable<MailAddress> addresses, string emailPath, bool isHtml)
        {
            var client = new SmtpClient();
            client.EnableSsl = ConfigurationHelper.SmtpEnableSsl;

            var message = new MailMessage();
            message.IsBodyHtml = isHtml;

            foreach (var address in addresses)
            {
                message.To.Add(address);
            }

            var path = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute(emailPath));

            message.Subject = subject;

            var body = string.Empty;
            using (var reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }

            foreach (var item in replacements)
            {
                body = body.Replace(item.Key, item.Value);
            }

            message.Body = body;

            client.Send(message);
        }

        static void SendMessage(string subject, IEnumerable<MailAddress> addresses, string body, bool isHtml)
        {
            var client = new SmtpClient();
            client.EnableSsl = ConfigurationHelper.SmtpEnableSsl;

            var message = new MailMessage();
            message.IsBodyHtml = isHtml;

            foreach (var address in addresses)
            {
                message.To.Add(address);
            }

            message.Subject = subject;

            message.Body = body;

            client.Send(message);
        }

        #endregion
    }
}
