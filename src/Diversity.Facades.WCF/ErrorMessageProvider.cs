﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web;
using Diversity.Common;
using Diversity.Common.Exceptions;
using Diversity.Domain.Model;
using Diversity.Services.Business;
using Diversity.Services.Business.Implementation;

namespace Diversity.Facades.WCF
{
    public sealed class ErrorMessageProvider
    {
        private static readonly Lazy<ErrorMessageProvider> LazyInstance =
            new Lazy<ErrorMessageProvider>(() => new ErrorMessageProvider());

        private readonly IErrorMessageService _errorMessageService;
        private List<ErrorMessage> _errorMessages;

        private ErrorMessageProvider()
        {   
            _errorMessages = new List<ErrorMessage>();

            _errorMessageService = new ErrorMessageService();
            _errorMessages.AddRange(_errorMessageService.GetErrorMessages());
        }

        public static ErrorMessageProvider Instance { get { return LazyInstance.Value; } }

        public List<ErrorMessage> ErrorMessages
        {
            get { return _errorMessages; }
        }

        List<ErrorMessage> GetErrorMessages(ErrorCodes errorCode)
        {
            var code = Enum.GetName(typeof (ErrorCodes), errorCode);
            if(code == null)
            {
                return new List<ErrorMessage>();
            }

            code = code.ToLower();

            return _errorMessages.Where(e => e.Code.ToLower().Equals(code)).ToList();
        }        

        public string GetMessageDescription(ErrorCodes errorCode)
        {
            var lcid = ServiceHelper.GetRequestCultureInfo();

            var code = Enum.GetName(typeof (ErrorCodes), errorCode);
            if(code == null)
            {
                throw new ArgumentOutOfRangeException("errorCode");
            }

            code = code.ToLower();

            if(_errorMessages.Any())
            {
                var message =
                    _errorMessages.FirstOrDefault(
                        e => e.Code.ToLower().Equals(code) && e.Language.LCID.ToLower().Equals(lcid.ToLower()));
                return message == null ? string.Empty : message.Description;
            }

            return string.Empty;
        }

        public FaultReason GetFaultReason(ErrorCodes errorCode)
        {
            var errorMessages = GetErrorMessages(errorCode);

            var translations = new Collection<FaultReasonText>();

            foreach (var errorMessage in errorMessages)
            {
                if (errorMessage.Language.LCID.Equals(ConfigurationHelper.DefaultCultureLanguage))
                {
                    translations.Add(new FaultReasonText(errorMessage.Description, CultureInfo.CurrentCulture));
                }
                else
                {
                    translations.Add(new FaultReasonText(errorMessage.Description,
                                                         CultureInfo.CreateSpecificCulture(errorMessage.Language.LCID)));
                }
            }

            return new FaultReason(translations);
        }
    }
}