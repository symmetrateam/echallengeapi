﻿Your password has been reset, <%UserName%>!

According to our records, you have requested that your password be reset. Your new
password is: <%Password%>

Please sign in using this new password and use the Change Password option to change this password
to a password of your choice.

If you have any questions or trouble logging on please contact a site administrator.

Thank you!