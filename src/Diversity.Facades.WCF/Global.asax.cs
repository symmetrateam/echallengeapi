﻿using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Diversity.Facades.WCF
{
    public class Global : HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            //RegisterRoutes();
        }

        private void RegisterRoutes()
        {
            RouteTable.Routes.Add(new ServiceRoute("Security", new WebServiceHostFactory(), typeof(AuthenticationService)));
            RouteTable.Routes.Add(new ServiceRoute("Game", new WebServiceHostFactory(), typeof(GameService)));
        }

    }
}