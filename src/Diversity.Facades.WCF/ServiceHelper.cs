﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using Diversity.Common;
using Diversity.Common.Exceptions;
using Diversity.Domain.Model;
using Diversity.Services.Business.Implementation;
using log4net;

namespace Diversity.Facades.WCF
{
    public static class ServiceHelper
    {
        private static readonly ILog _log = LogManager.GetLogger((System.Reflection.MethodBase.GetCurrentMethod().DeclaringType));

        public static string GetRequestCultureInfo()
        {
            var lcid = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>("X-Accept-Language", string.Empty);            
            _log.DebugFormat("Incoming Accept-Language header : {0}", lcid);

            var gameControllerService = new GameControllerService();
            var language = gameControllerService.GetLanguages().FirstOrDefault(
                l => string.Compare(l.LCID, lcid, StringComparison.OrdinalIgnoreCase) == 0);
            
            lcid = language == null ? ConfigurationHelper.DefaultCultureLanguage : language.LCID;
            _log.DebugFormat("Using header : {0}", lcid);

            return lcid;
        }
       
        public static bool WebOperationContextIsForbidden()
        {            
            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
            WebOperationContext.Current.OutgoingResponse.StatusDescription =
                GetErrorMessage(ErrorCodes.Unauthorized_Forbidden).Description;                
            return false;
        }

        public static void SetBadRequestWebOperationContext(string argumentName, string errorCode)
        {
            var lcid = GetRequestCultureInfo();
            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;

            var errorMessageBuilder = new StringBuilder(); 
            if(!string.IsNullOrEmpty(argumentName))
            {
                errorMessageBuilder.AppendFormat(@"{0} : ", argumentName);
            }

            if(!string.IsNullOrEmpty(errorCode))
            {
                
                var errorMsg = new ErrorMessageService().GetErrorMessage(lcid, errorCode);
                errorMessageBuilder.AppendFormat(@"{0}", errorMsg.Description);
            }

            WebOperationContext.Current.OutgoingResponse.StatusDescription = errorMessageBuilder.ToString();

        }
        
        public static ResponseResult<T> CreateResult<T>(T result, ErrorCodes errorCode)
        {
            return CreateResult(result, string.Empty, errorCode);
        }

        public static ResponseResult<T> CreateResult<T>(T result, Exception ex, ErrorCodes errorCode)
        {
            var responseResult = CreateResult(result, errorCode);

            if (ex != null)
            {
                if (responseResult.Errors.Count > 0)
                {
                    var errorMessage = responseResult.Errors[0];
                    errorMessage.Description = string.Format(@"{0}/n{1}", errorMessage.Description, ex.Message);
                }
            }

            return responseResult;
        }

        public static ResponseResult<T> CreateResult<T>(T result, string argument, ErrorCodes errorCode)
        {
            var response = new ResponseResult<T>();
            response.Result = result;

            if (errorCode != ErrorCodes.None)
            {
                var errorMsg = GetErrorMessage(errorCode);

                if (errorMsg != null)
                {
                    if(!string.IsNullOrEmpty(argument))
                    {
                        errorMsg.Description = string.Format(@"{0} : {1}", argument, errorMsg.Description);
                    }

                    //if(errorCode == ErrorCodes.UNAUTHORIZED_FORBIDDEN || 
                    //    errorCode == ErrorCodes.LOGIN_FAILED || 
                    //    errorCode == ErrorCodes.ACCOUNT_LOCKED || 
                    //    errorCode == ErrorCodes.ACCOUNT_NOT_ACTIVATED)
                    //{
                    //    errorMsg.Code = "403";
                    //}
                    //else
                    //{
                    //    errorMsg.Code = "400";
                    //}

                    response.Errors.Add(errorMsg);
                }
            }

            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
            return response;            
        }

        static ErrorMessage GetErrorMessage(ErrorCodes errorCode)
        {
            var lcid = GetRequestCultureInfo();
            _log.DebugFormat("Creating error message for language {0} and error code {1}", lcid, Enum.GetName(typeof(ErrorCodes), errorCode));            
            return new ErrorMessageService().GetErrorMessage(lcid, Enum.GetName(typeof(ErrorCodes), errorCode));
        }

        public static string GetMessageText(ErrorCodes errorCode)
        {
            var lcid = GetRequestCultureInfo();
            _log.DebugFormat("Creating error message for language {0} and error code {1}", lcid, Enum.GetName(typeof(ErrorCodes), errorCode));            
            var message =  new ErrorMessageService().GetErrorMessage(lcid, Enum.GetName(typeof(ErrorCodes), errorCode));
            return message.Description;
        }
    }
}