﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Diversity.Common;
using log4net;
using Diversity.Common.Exceptions;
using Diversity.Domain.Model;
using Diversity.Services.Business;
using Diversity.Services.Business.Implementation;


namespace Diversity.Facades.WCF
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GameService : IGameService
    {
        private static readonly ILog _log = LogManager.GetLogger((System.Reflection.MethodBase.GetCurrentMethod().DeclaringType));

        private readonly IGameControllerService _gameControllerService;
        private readonly IAuthenticationService _authService;

        public GameService()
        {
            _gameControllerService = new GameControllerService();
            _authService = new AuthenticationService();
        }

        [OperationBehavior]
        public bool IsGameServiceAvailable()
        {
            return true;
        }

        [OperationBehavior]
        public List<Language> GetLanguages()
        {
            _log.Debug(@"Entered GetLanguages");
            try
            {
                var result = _gameControllerService.GetLanguages().ToList();
                _log.DebugFormat(@"Retrieved GetLanguages result = {0}", result);

                return result;
            }
            catch (Exception ex)
            {
                _log.Error(@"Failed to retrieve languages", ex);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unhandled_Exception);
                throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnhandledException), faultReason);
            }
            finally
            {
                _log.Debug(@"Exiting GetLanguages");
            }
        }

        [OperationBehavior]
        public List<FlashAsset> GetFlashAssets()
        {
            _log.Debug(@"Entered GetFlashAssets");
            try
            {
                var defaultLcid = ConfigurationHelper.DefaultCultureLanguage;
                var lcid = ServiceHelper.GetRequestCultureInfo();
                var result = _gameControllerService.GetFlashAssets(lcid, defaultLcid).ToList();
                _log.DebugFormat(@"Retrieved GetFlashAssests result = {0}", result);
                return result;
            }
            catch (Exception ex)
            {
                _log.Error(@"Failed to retrieve flash assets", ex);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unhandled_Exception);
                throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnhandledException), faultReason);
            }
            finally
            {
                _log.Debug(@"Exiting GetFlashAssets");
            }
        }


        [OperationBehavior]
        public Session GetGameSession(int gameId)
        {
            _log.Debug(@"Entered GetGetGameSession");

            if (gameId <= 0)
            {
                _log.DebugFormat(@"Game ID = {0}", gameId);
                _log.DebugFormat(@"Game ID must not equal NULL and must be greater than zero");
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Game_Provided);
                throw new FaultException<RangeValidationException>(new RangeValidationException("gameId", Messages.InvalidGameProvidedMessage), faultReason);
            }

            Guid userId;
            bool isTimedOut;

            if (_authService.IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                try
                {
                    var lcid = ServiceHelper.GetRequestCultureInfo();
                    var result = _gameControllerService.GetGameSession(userId, gameId, lcid);

                    _log.DebugFormat(@"Retrieved game session result = {0}", result);

                    return result;
                }
                catch (MaximumGamesPlayedException maxPlaysException)
                {
                    _log.ErrorFormat(@"Exception thrown: GetGameSession for userID:{0} and gameID:{1}", userId, gameId);
                    _log.Error(@"GetGameSessionException", maxPlaysException);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Maximum_Games_Played);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.GamePlayedMaximumNumberOfTimes), faultReason);
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(@"Exception thrown: GetGameSession for userID:{0} and gameID:{1}", userId, gameId);
                    _log.Error(@"GetGameSessionException", ex);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Retrieve_Game_Session);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnableToRetrieveGameSessionMessage), faultReason);
                }
                finally
                {
                    _log.Debug(@"Exiting GetGameSession");
                }
            }

            if (isTimedOut)
            {
                _log.DebugFormat("Session timed out for userID: {0}", userId);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }

        [OperationBehavior]
        public Dashboard GetDashboard()
        {
            _log.Debug(@"Entered GetDashboard");

            Guid userId;
            bool isTimedOut;

            if (_authService.IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                try
                {
                    var lcid = ServiceHelper.GetRequestCultureInfo();
                    var result = _gameControllerService.GetDashboard(userId, lcid);

                    _log.DebugFormat(@"Retrieved dashboard result = {0}", result);

                    return result;
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(@"Exception thrown: GetDashboard for userID:{0}.", userId);
                    _log.Error(@"GetDashboardException", ex);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Retrieve_Dashboard);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnableToRetrieveDashboardMessage), faultReason);
                }
                finally
                {
                    _log.Debug(@"Exiting GetDashboard");
                }
            }

            if (isTimedOut)
            {
                _log.DebugFormat("Session timed out for userID: {0}", userId);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }

        [OperationBehavior]
        public bool SavePlayerProfile(string firstname, string lastname)
        {
            _log.Debug(@"Entered SavePlayerProfile");

            if (string.IsNullOrEmpty(firstname) || string.IsNullOrWhiteSpace(firstname))
            {
                _log.Debug(@"Firstname must not equal NULL and have one or more characters in length.");

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.String_Greater_Than_Zero);
                throw new FaultException<RequiredValidationException>(new RequiredValidationException("firstname", Messages.StringLengthGreaterThanZeroRequiredMessage), faultReason);
            }

            if (string.IsNullOrEmpty(lastname) || string.IsNullOrWhiteSpace(lastname))
            {
                _log.DebugFormat(@"Lastname = {0}", lastname);
                _log.Debug(@"Lastname must not equal NULL and have one or more characters in length.");
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.String_Greater_Than_Zero);
                throw new FaultException<RequiredValidationException>(new RequiredValidationException("lastname", Messages.StringLengthGreaterThanZeroRequiredMessage), faultReason);
            }

            Guid userId;
            bool isTimedOut;

            if (_authService.IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                try
                {
                    var result = _gameControllerService.SavePlayerProfile(userId, firstname, lastname);
                    return result;
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(@"Exception thrown: SavePlayerProfile for userID:{0}, firstname:{1}, lastname:{2}.", userId, firstname, lastname);
                    _log.Error(@"SavePlayerProfileException", ex);

                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Save_Player_Profile);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnableToSavePlayerProfileMessage), faultReason);
                }
                finally
                {
                    _log.Debug(@"Exiting SavePlayerProfile");
                }
            }

            if (isTimedOut)
            {
                _log.DebugFormat("Session timed out for userID: {0}", userId);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }

        [OperationBehavior]
        public bool SaveAvatarProfile(string avatarName, string avatarMetaData)
        {
            _log.Debug(@"Entered SaveAvatarProfile");

            if (string.IsNullOrEmpty(avatarName) || string.IsNullOrWhiteSpace(avatarName))
            {
                _log.DebugFormat(@"AvatarName = {0}", avatarName);
                _log.Debug(@"AvatarName must not equal NULL and have one or more characters in length.");
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.String_Greater_Than_Zero);
                throw new FaultException<RequiredValidationException>(new RequiredValidationException("avatarName", Messages.StringLengthGreaterThanZeroRequiredMessage), faultReason);
            }

            if (string.IsNullOrEmpty(avatarMetaData) || string.IsNullOrWhiteSpace(avatarMetaData))
            {
                _log.DebugFormat(@"AvatarMetaData = {0}", avatarMetaData);
                _log.Debug(@"AvatarMetaData must not equal NULL and have one or more characters in length.");

                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.String_Greater_Than_Zero);
                throw new FaultException<RequiredValidationException>(new RequiredValidationException("avatarMetaData", Messages.StringLengthGreaterThanZeroRequiredMessage), faultReason);

            }

            Guid userId;
            bool isTimedOut;

            if (_authService.IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                try
                {
                    return _gameControllerService.SaveAvatarProfile(userId, avatarName, avatarMetaData);
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(@"Exception thrown: SaveAvatar profile for userID:{0}, avatarName:{1}, avatarMetaData:{2}", userId, avatarName, avatarMetaData);
                    _log.Error(@"SaveAvatarProfileException", ex);

                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Save_Avatar_Profile);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnableToSaveAvatarProfileMessage), faultReason);
                }
                finally
                {
                    _log.Debug(@"Exiting SaveAvatarProfile");
                }
            }

            if (isTimedOut)
            {
                _log.DebugFormat("Session timed out for userID: {0}", userId);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }

        [OperationBehavior]
        public bool SaveCardResult(int cardSequenceId, PlayerResult result)
        {
            _log.Debug(@"Entered SaveCardResult");

            if (cardSequenceId <= 0)
            {
                _log.DebugFormat(@"Card Sequence ID = {0}", cardSequenceId);
                _log.DebugFormat(@"Card Sequence ID must not equal NULL and must be greater than zero");
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_CardSequence_Number_Provided);
                throw new FaultException<RangeValidationException>(new RangeValidationException("cardSequenceId", Messages.InvalidCardSequenceProvidedMessage), faultReason);
            }

            Guid userId;
            bool isTimedOut;
            if (_authService.IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                try
                {
                    var saved = _gameControllerService.SaveCardResult(userId, cardSequenceId, result);

                    if (result == null)
                    {
                        if (saved)
                        {
                            _log.DebugFormat(@"Non Mandatory card, PlayerResult is equal null ");
                            return true;
                        }
                        else
                        {
                            _log.DebugFormat(@"PlayerResult must not equal null value.");
                            var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Argument_Null_Exception);
                            throw new FaultException<RequiredValidationException>(new RequiredValidationException("result", Messages.ArgumentNullException), faultReason);
                        }
                    }

                    return saved;
                }
                catch (InvalidCardException ex)
                {
                    _log.ErrorFormat(@"Exception thrown: Invalid card provided. SaveCard result userID:{0}, cardsequenceId:{1}, playerresult:{2}", userId, cardSequenceId, result);
                    _log.Error(@"SaveCardResultException", ex);

                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Save_Card_Result);
                    throw new FaultException<CardException>(new CardException(Messages.UnableToSaveCardResultMessage), faultReason);
                }
                catch (InvalidAnswerException ex)
                {
                    _log.ErrorFormat(@"Exception thrown: SaveCard result userID:{0}, cardsequenceId:{1}, playerresult:{2}", userId, cardSequenceId, result);
                    _log.Error(@"SaveCardResultException", ex);

                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Answer_Provided);
                    throw new FaultException<CustomValidationException>(new CustomValidationException(Messages.InvalidAnswerMessage), faultReason);
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(@"Exception thrown: SaveCard result userID:{0}, cardsequenceId:{1}, playerresult:{2}", userId, cardSequenceId, result);
                    _log.Error(@"SaveCardResultException", ex);

                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Save_Card_Result);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnableToSaveCardResultMessage), faultReason);
                }
                finally
                {
                    _log.Debug(@"Exiting SaveCardResult");
                }
            }

            if (isTimedOut)
            {
                _log.DebugFormat("Session timed out for userID: {0}", userId);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }

        [OperationBehavior]
        public bool SkipCard(int cardSequenceId)
        {
            _log.Debug(@"Entered SkipCard");

            if (cardSequenceId <= 0)
            {
                _log.DebugFormat(@"Card Sequence ID = {0}", cardSequenceId);
                _log.DebugFormat(@"Card Sequence ID must not equal NULL and must be greater than zero");
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_CardSequence_Number_Provided);
                throw new FaultException<RangeValidationException>(new RangeValidationException("cardSequenceId", Messages.InvalidCardSequenceProvidedMessage), faultReason);
            }

            Guid userId;
            bool isTimedOut;

            if (_authService.IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                try
                {
                    return _gameControllerService.SkipCard(userId, cardSequenceId);
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(@"Exception thrown: Skip card userID:{0}, cardsequenceId:{1}", userId, cardSequenceId);
                    _log.Error(@"SkipCardException", ex);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Skip_Card);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnableToSkipCardMessage), faultReason);
                }
                finally
                {
                    _log.Debug(@"Exiting SaveCardResult");
                }
            }

            if (isTimedOut)
            {
                _log.DebugFormat("Session timed out for userID: {0}", userId);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }

        [OperationBehavior]
        public bool SetGameAsCompleted(int gameId)
        {
            _log.Debug(@"Entered SetGameAsCompleted");

            if (gameId <= 0)
            {
                _log.DebugFormat(@"Game ID = {0}", gameId);
                _log.DebugFormat(@"Game ID must not equal NULL and must be greater than zero");
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Game_Provided);
                throw new FaultException<RangeValidationException>(new RangeValidationException("gameId", Messages.InvalidGameProvidedMessage), faultReason);
            }

            Guid userId;
            bool isTimedOut;

            if (_authService.IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                _log.DebugFormat(@"Begin processing set game as completed for Game ID = {0}", gameId);

                if (!_gameControllerService.IsAPlayableGame(userId, gameId))
                {
                    _log.Debug(string.Format(@"Game is a template game and not a playable game. Userid={0}, GameId={1}",
                                            userId, gameId));

                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Game_Cannot_Be_Played);
                    throw new FaultException<RangeValidationException>(
                        new RangeValidationException("gameId", Messages.GameCannotBePlayedMessage), faultReason);
                }

                if (_gameControllerService.IsGameCompleted(userId, gameId))
                {
                    _log.Debug(string.Format(@"Game has already been completed. Userid={0}, GameId={1}", userId,
                                             gameId));

                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Game_Already_Completed);
                    throw new FaultException<GameException>(new GameException(Messages.GameAlreadyCompletedMesage),
                                                            faultReason);
                }


                try
                {
                    return _gameControllerService.SetGameAsCompleted(userId, gameId);
                }
                catch (GameCompletedException ex)
                {
                    _log.ErrorFormat(@"Exception thrown: Set game as completed. userID:{0}, gameId:{1}", userId, gameId);
                    _log.Error(@"SetGameAsCompletedException.", ex);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Complete_Game);
                    throw new FaultException<GameException>(new GameException(Messages.IncompleteGameMessage), faultReason);
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(@"Exception thrown: Set game as completed. userID:{0}, gameId:{1}", userId, gameId);
                    _log.Error(@"SetGameAsCompletedException.", ex);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Set_Game_As_Completed);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnableToSetGameAsCompleted), faultReason);
                }
                finally
                {
                    _log.Debug(@"Exiting SetGameAsCompleted");
                }
            }

            if (isTimedOut)
            {
                _log.DebugFormat("Session timed out for userID: {0}", userId);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }

        [OperationBehavior]
        public int RegisterScormCourseCode(string courseCode, string themeCode)
        {
            _log.Debug(@"Entered Register Scorm Game");

            if (string.IsNullOrEmpty(courseCode))
            {
                _log.DebugFormat(@"Course Code (ExternalID) = {0}", courseCode);
                _log.DebugFormat(@"Course Code (ExternalID) cannot equal NULL or empty string");
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.String_Greater_Than_Zero);
                throw new FaultException<RequiredValidationException>(new RequiredValidationException("courseCode", Messages.StringLengthGreaterThanZeroRequiredMessage), faultReason);
            }

            Guid userId;
            bool isTimedOut;
            if (_authService.IsAuthenticated(out isTimedOut, out userId))
            {
                if (isTimedOut)
                {
                    _log.DebugFormat("Session timed out for userID: {0}", userId);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                    throw new FaultException<SessionTimeoutException>(
                        new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
                }

                _log.Debug(@"Begin processing the register scorm game");

                if (!_gameControllerService.ScormGameExists(courseCode))
                {
                    _log.DebugFormat(@"Cannot find a game mapped to the course code (ExternalID) = {0}", courseCode);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Game_Provided);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.InvalidCourseCodeMessage), faultReason);
                }


                if (!string.IsNullOrEmpty(themeCode) &&
                    !_gameControllerService.UserExistsInThemeClientUserGroup(userId, themeCode))
                {
                    _gameControllerService.AddUserToThemeClientUserGroup(userId, themeCode);
                }

                var gameId = _gameControllerService.GetGameIdFromExternalId(courseCode);

                if (!gameId.HasValue)
                {
                    _log.DebugFormat(@"Cannot find gameId for the course code (ExternalID) = {0}", courseCode);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Invalid_Game_Provided);
                    throw new FaultException<RangeValidationException>(new RangeValidationException("gameId", Messages.InvalidGameProvidedMessage), faultReason);
                }
                
                var invited = _gameControllerService.InviteUserToGame(userId, gameId.Value, themeCode);

                if (!invited)
                {
                    _log.DebugFormat(@"Unable to invite user to game with UserID = {0}, GameID = {1}", userId, courseCode);
                    var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unable_To_Auto_Invite_User);
                    throw new FaultException<ServiceControllerException>(new ServiceControllerException(Messages.UnableToInviteUserToGameMessage), faultReason);
                }

                return gameId.Value;
            }

            if (isTimedOut)
            {
                _log.DebugFormat("Session timed out for userID: {0}", userId);
                var faultReason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Session_Timeout_Exception);
                throw new FaultException<SessionTimeoutException>(new SessionTimeoutException(Messages.SessionTimedOutMessage), faultReason);
            }

            var reason = ErrorMessageProvider.Instance.GetFaultReason(ErrorCodes.Unauthorized_Forbidden);
            throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException(Messages.UnauthorizedForbiddenMessage), reason);
        }
    }
}
