﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Diversity.Presentation.TestClient
{
    class Program
    {
        private static MessageHeader _authorizationHeader;
        private static MessageHeader _acceptLanguageHeader;

        static void Main(string[] args)
        {
            _acceptLanguageHeader = MessageHeader.CreateHeader("X-Accept-Language", string.Empty, "en-EN");


            var n = 0;
            var loginAnswer = string.Empty;
            var useScormAuthentication = false;
            do
            {
                Console.WriteLine(@"Use scorm login Y/N:");
                loginAnswer = Console.ReadLine();

                if (string.Compare(loginAnswer, "Y", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    useScormAuthentication = true;
                    break;
                }

                if (string.Compare(loginAnswer, "N", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    useScormAuthentication = false;
                    break;
                }

                Console.WriteLine(@"Enter Y/N:");
            } while (n == 0);




            Console.WriteLine(@"Please enter your username/email address:");
            var username = Console.ReadLine();

            var password = string.Empty;
            var clientCode = string.Empty;
            if (useScormAuthentication)
            {
                Console.WriteLine(@"Please enter your clientCode:");
                clientCode = Console.ReadLine();
            }
            else
            {
                Console.WriteLine(@"Please enter your password:");
                password = Console.ReadLine();
            }

            //Run the reset password
            //ResetPassword(username);
            //Console.WriteLine("Press a key to continue");
            //Console.ReadKey();

            AuthenticationTicket authenticationTicket = null;

            using (var authenticationServiceClient = new AuthenticationServiceClient())
            {
                try
                {
                    authenticationTicket = useScormAuthentication ? 
                        authenticationServiceClient.AuthenticateScorm(username, clientCode) : 
                        authenticationServiceClient.Authenticate(username, password);
                }
                catch (FaultException<AuthenticationFailedException> ex)
                {
                    Console.WriteLine(@"FaultException of type AuthenticationFailedException thrown with message: {0}",
                        ex.Message);
                }
                catch (FaultException<ClientException> ex)
                {
                    Console.WriteLine(@"FaultException of type ClientException thrown with message: {0}",
                        ex.Message);
                }
            }

            if (authenticationTicket == null)
            {
                Console.WriteLine("Authentication failed.");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Authentication Token:{0}", authenticationTicket.Token);
            Console.WriteLine("Authentication Token Expires At:{0}", authenticationTicket.ExpiresAt);
            Console.WriteLine("Platform Theme Key:{0}", authenticationTicket.ThemeKey);
            Console.WriteLine();
            Console.WriteLine("Press a key to continue");
            Console.ReadKey();


            _authorizationHeader = MessageHeader.CreateHeader("Authorization", string.Empty,
                                                     authenticationTicket.Token);


            //Start testing all Authentication Service calls
            //ChangePassword(password);

            int? gameId;
            if (useScormAuthentication)
            {
                Console.WriteLine(@"Enter your course code:");
                var courseCode = Console.ReadLine();
                RegisterCourseCode(courseCode, out gameId);
            }

            //GetLanguages();
            //GetFlashAssets();
            //GetDashboard();
            //Console.WriteLine("Press a key to exit");
            //Console.ReadKey();
            //var start = DateTime.Now;
            GetGameSession(668);

            //var result = GetPriorityMultipleChoiceFeedbackResult();
            //SaveCardResult(12819, result);

            //var votingResult = GetVotingPriorityIssueCardResult();
            //SaveCardResult(12820, votingResult);

            //var result = GetPriorityMultipleCheckBoxFeedbackResult();
            //SaveCardResult(12821, result);

            //var result = GetMatchingListPlayerResult();
            //SaveCardResult(4608, result);

            //GetGameSession(6);
            //var timeTaken = DateTime.Now.Subtract(start).TotalMilliseconds;
            //Console.WriteLine("Time taken in Milliseconds: " + timeTaken);
            //SavePlayerProfile("Zaid", "Text");
            //SaveAvatarProfile("Bob", "Joe");

            //var result = new MultipleChoicePlayerResult();
            //result.PlayerId = Guid.Parse(@"4ac4dfd4-279f-4435-a877-e785e985ecb0");
            //result.AnswerId = null;
            //result.TimeTakenToAnswerInSeconds = 0;
            //SaveCardResult(13866, result);

            //4643
            //var result = new BranchingCardPlayerResult();
            //result.BranchingCardPlayerOptions = new BranchingCardPlayerOption[1];
            //result.BranchingCardPlayerOptions[0] = new BranchingCardPlayerOption()
            //                                       {
            //                                           GameCardGroupSequenceID = 4066,
            //                                           Position = 1
            //                                       };
            //result.PlayerId = Guid.Parse(@"56722BC7-AA38-4410-B11F-F7EE446C938C");
            //SaveCardResult(4643, result);


            //SetGameAsCompleted(155);


            Console.WriteLine("Press a key to exit");
            Console.ReadKey();

            _acceptLanguageHeader = null;
            _authorizationHeader = null;
        }

        private static PlayerResult GetVotingPriorityIssueCardResult()
        {
            var playerResult = new VotingPlayerResult();
            playerResult.TimeTakenToAnswerInSeconds = 5;
            var answers = new VotingPlayerResultAnswer[]
                          {
                              new VotingPlayerResultAnswer(){ AnswerId = 569}, 
                              new VotingPlayerResultAnswer(){ AnswerId = 570}
                          };

            playerResult.Results = answers;

            return playerResult;

        }

        private static PlayerResult GetPriorityTextFeedbackResult()
        {
            var playerResult = new VotingCardFeedbackResult();
            playerResult.VotingCardId = 1338; // PriorityIssueVotingCardId
            playerResult.AnswerId = 560; // PriorityIssueVotingCardAnswrId

            var textResult = new TextPlayerResult();
            textResult.Answer = "Testing";
            textResult.TimeTakenToAnswerInSeconds = 5;

            playerResult.FeedbackResult = textResult;

            return playerResult;
        }

        private static PlayerResult GetPriorityMultipleChoiceFeedbackResult()
        {
            var playerResult = new VotingCardFeedbackResult();
            playerResult.VotingCardId = 304; // PriorityIssueVotingCardId
            playerResult.AnswerId = 481; // PriorityIssueVotingCardAnswrId

            var textResult = new MultipleChoicePlayerResult();
            textResult.AnswerId = 487;
            textResult.TimeTakenToAnswerInSeconds = 5;

            playerResult.FeedbackResult = textResult;

            return playerResult;
        }

        private static PlayerResult GetPriorityMultipleCheckBoxFeedbackResult()
        {
            var votingResult = new VotingCardFeedbackResult();
            votingResult.VotingCardId = 1341;
            votingResult.AnswerId = 569;

            var checkBoxResult = new MultipleCheckBoxPlayerResult();
            var checkBoxResultAnswers = new MultipleCheckBoxPlayerResultAnswer[]
                                        {
                                            new MultipleCheckBoxPlayerResultAnswer() { AnswerId = 576 },
                                            new MultipleCheckBoxPlayerResultAnswer() { AnswerId = 577 },
                                            new MultipleCheckBoxPlayerResultAnswer() { AnswerId = 578 }
                                        };

            checkBoxResult.TimeTakenToAnswerInSeconds = 15;
            checkBoxResult.Results = checkBoxResultAnswers;

            votingResult.FeedbackResult = checkBoxResult;

            return votingResult;
        }


        private static PlayerResult GetMatchingListPlayerResult()
        {
            var playerResult = new MatchingListPlayerResult();
            playerResult.TimeTakenToAnswerInSeconds = 10;
            playerResult.Results = new PlayerResultAnswer[3];

            playerResult.Results[0] = new MatchingListPlayerResultAnswer()
                                      {
                                          AnswerSequenceNo = 0,
                                          LHAnswerId = 176,
                                          RHAnswerId = 174
                                      };

            playerResult.Results[1] = new MatchingListPlayerResultAnswer()
            {
                AnswerSequenceNo = 1,
                LHAnswerId = 174,
                RHAnswerId = 175
            };

            playerResult.Results[2] = new MatchingListPlayerResultAnswer()
            {
                AnswerSequenceNo = 2,
                LHAnswerId = 175,
                RHAnswerId = 176
            };

            return playerResult;
        }

        private static void ResetPassword(string username)
        {
            Console.WriteLine(@"Running Reset Password");
            Console.WriteLine(@"Please enter your username");

            try
            {
                using (var client = new AuthenticationServiceClient())
                {
                    var authScope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);
                    var message = client.ResetPassword(username);
                    Console.WriteLine(message);
                    Console.WriteLine(@"Password reset");
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<PasswordResetException> ex)
            {
                Console.WriteLine(@"FaultException of type PasswordResetException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<AuthenticationFailedException> ex)
            {
                Console.WriteLine(@"FaultException of type AuthenticationFailedException thrown with message: {0}",
                                  ex.Message);
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void ChangePassword(string oldPassword)
        {
            Console.WriteLine(@"Running ChangePassword");
            Console.WriteLine(@"Please enter your new password");
            var newPassword = Console.ReadLine();

            try
            {
                using (var client = new AuthenticationServiceClient())
                {
                    var authScope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);
                    client.ChangePassword(oldPassword, newPassword);
                    Console.WriteLine(@"Password changed");
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<PasswordResetException> ex)
            {
                Console.WriteLine(@"FaultException of type PasswordResetException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<AuthenticationFailedException> ex)
            {
                Console.WriteLine(@"FaultException of type AuthenticationFailedException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<CustomValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type CustomValidationException thrown with message: {0}",
                                  ex.Message);
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void SaveCardResult(int cardSequenceId, PlayerResult result)
        {
            Console.WriteLine(@"Running SaveCardResult");

            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    var saved = client.SaveCardResult(cardSequenceId, result);
                    Console.WriteLine(@"{0} Card Result", saved ? "Saved" : "Unable to Save");
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<RequiredValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type RequiredValidationException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<RangeValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type RangeValidationException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<CardException> ex)
            {
                Console.WriteLine(@"FaultException of type CardException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<CustomValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type CustomValidationException thrown with message: {0}",
                                  ex.Message);
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void SaveAvatarProfile(string avatarName, string avatarMetaData)
        {
            Console.WriteLine(@"Running SaveAvatarProfile");

            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    var saved = client.SaveAvatarProfile(avatarName, avatarMetaData);

                    Console.WriteLine(@"{0} Avatar Profile", saved ? "Saved" : "Unable to Save");
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<RequiredValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type RequiredValidationException thrown with message: {0}",
                                  ex.Message);
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void SavePlayerProfile(string firstname, string lastname)
        {
            Console.WriteLine(@"Running SavePlayProfile");

            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    var saved = client.SavePlayerProfile(firstname, lastname);

                    Console.WriteLine(@"{0} Player Profile", saved ? "Saved" : "Unable to Save");
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<RequiredValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type RequiredValidationException thrown with message: {0}",
                                  ex.Message);
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void GetGameSession(int gameId)
        {
            Console.WriteLine(@"Running GetGameSession");

            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    var session = client.GetGameSession(gameId);

                    if (session == null)
                    {
                        Console.WriteLine(@"No game session found");
                    }

                    Console.WriteLine(@"Retrieved Session {0}", session.Id);
                    Console.WriteLine(@"Contains a game:{0}", session.Game == null ? "No" : "Yes");
                    Console.WriteLine(@"Game theme key:{0}", (session.Game == null || (session.Game != null && string.IsNullOrEmpty(session.Game.ThemeKey))) ? "" : session.Game.ThemeKey );

                    if (session.Game == null)
                    {
                        return;
                    }
                    Console.WriteLine(@"GameID = {0}", session.Game.Id);
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<RangeValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type RangeValidationException thrown with message: {0}",
                                  ex.Message);
            }

            Console.WriteLine("Press a key to continue");
            //Console.ReadKey();
        }

        private static void GetDashboard()
        {
            Console.WriteLine(@"Running GetDashboard");

            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    var dashboard = client.GetDashboard();

                    if (dashboard == null)
                    {
                        Console.WriteLine(@"No dashboard found");
                    }

                    Console.WriteLine(ToXmlString(dashboard));
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}",
                                  ex.Message);
            }
            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void GetFlashAssets()
        {
            Console.WriteLine(@"Running GetFlashAssets");


            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    var assets = client.GetFlashAssets();

                    if (assets == null)
                    {
                        Console.WriteLine(@"No flash assets returned");
                    }

                    assets.ToList().ForEach(
                        a => Console.WriteLine(@"lcid={0}, key={1} value={2}", a.Lcid, a.Name, a.Value));
                }
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}",
                                  ex.Message);
            }


            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void RegisterCourseCode(string courseCode, out int? gameId, string themeCode = null)
        {
            Console.WriteLine(@"Running Register Course Code");
            int? tempGameId = null;
            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);
                    
                    tempGameId = client.RegisterScormCourseCode(courseCode, themeCode);
                }
            }
            catch (FaultException<RequiredValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type RequiredValidationException thrown with message: {0}",
                    ex.Message);
            }
            catch (FaultException<RangeValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type RangeValidationException throw with message: {0}", ex.Message);
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }

            gameId = tempGameId;
            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void GetLanguages()
        {
            Console.WriteLine(@"Running GetLanguages");
            //Get languages
            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    var languages = client.GetLanguages();

                    if (languages == null)
                    {
                        Console.WriteLine(@"No languages returned");
                    }

                    languages.ToList().ForEach(l => Console.WriteLine(@"lcid={0} description={1}", l.LCID, l.Name));
                }
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}", ex.Message);
            }


            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }

        private static void SetGameAsCompleted(int gameId)
        {
            Console.WriteLine(@"Running SetGameAsCompleted");

            try
            {
                using (var client = new GameServiceClient())
                {
                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    //UPDATE GameCardSequence set CardViewedInGame = 0, CardViewedInGameOn = NULL WHERE GameCardSequenceID = 12030
                    //UPDATE Game SET GameCompletionDate = NULL WHERE GameID = 143

                    var saved = client.SetGameAsCompleted(gameId);

                    Console.WriteLine(@"{0} Game as completed.", saved ? "SET" : "CANNOT SET");
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                Console.WriteLine(@"FaultException of type UnauthorizedAccessException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<SessionTimeoutException> ex)
            {
                Console.WriteLine(@"FaultException of type SessionTimeoutException thrown with message: {0}", ex.Message);
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                Console.WriteLine(@"FaultException of type ServiceControllerException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<RangeValidationException> ex)
            {
                Console.WriteLine(@"FaultException of type RangeValidationException thrown with message: {0}",
                                  ex.Message);
            }
            catch (FaultException<GameException> ex)
            {
                Console.WriteLine(@"FaultException of type GameException thrown with message: {0}",
                                  ex.Message);
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }


        public static string ToXmlString(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(@"obj");
            }

            try
            {
                var serializer = new XmlSerializer(obj.GetType());

                var encoding = new UTF8Encoding(false);

                var stream = new MemoryStream();
                var writer = new XmlTextWriter(stream, encoding);

                serializer.Serialize(writer, obj);
                writer.Flush();

                return encoding.GetString(stream.ToArray());
            }
            catch (SerializationException ex)
            {
                var e = new SerializationException("XmlSerializer encountered a serialization problem", ex);
                throw e;
            }
        }
    }
}
