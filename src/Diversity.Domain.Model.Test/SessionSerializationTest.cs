﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Diversity.Common.Enumerations;
using NUnit.Framework;

namespace Diversity.Domain.Model.Test
{
    [TestFixture]
    public class SessionSerializationTest : TestBase
    {
        [SetUp]
        public void StartUp()
        {
        }

        [Test]
        public void SessionSerializationTest_ToXmlString()
        {
            var data = string.Empty;
            var tempData = new Session();

            tempData.Id = Guid.NewGuid();


            var game = new Game();
            game.Id = 1;
            game.AvatarSettings = new Game.AvatarTurnSettingsInfo(true, true, true);
            game.PlayerTurnSettings = new Game.PlayerTurnSettingsInfo()
                                          {
                                              BySequenceNoElseRandom=true, 
                                              RandomNoRepeatingUntilAllHavePlayed = true
                                          };
            game.GameSettings = new Game.GameSettingsInfo()
                                    {
                                        CanSkipTutorial = true,
                                        CompletionDate = DateTime.Now.AddDays(100),
                                        DefaultGameTimeInSeconds = 1000,
                                        GameTemplateNameForDashboard = "Game Template name for dashboard",
                                        IsOnline = true,
                                        IsTemplate = false,
                                        IsTimed = true,
                                        StartDate = DateTime.Now.AddDays(-20),
                                        UseCardTime = false
                                    };
            
            CreatePlayers().ForEach(p => game.Players.Add(p));

            CreateCardGroups().ForEach(c => game.CardGroups.Add(c));
                                   
            tempData.Game = game;

            var serializer = new DataContractSerializer(tempData.GetType());

            using (var backing = new System.IO.StringWriter())
            {
                using (var writer = new System.Xml.XmlTextWriter(backing))
                {
                    serializer.WriteObject(writer, tempData);

                    data = backing.ToString();

                }
            }

            using (var writer = new System.IO.StreamWriter(@"sample.xml", false))
            {
                writer.Write(data);
                writer.Close();
            }

            Assert.IsNotNullOrEmpty(data);
        }


        List<CardGroup> CreateCardGroups()
        {
            var cardGroups = new List<CardGroup>();

            var group1 = new CardGroup();
            group1.Cards.Add(CreateMultipleChoiceSurveyCard());
            group1.Cards.Add(CreateMultipleCheckBoxSurveyCard());
            group1.Cards.Add(CreateTextAreaSurveyCard());
            group1.Cards.Add(CreateTextBoxSurveyCard());
            cardGroups.Add(group1);

            var group2 = new CardGroup();
            group2.Cards.Add(CreateLcdCard());
            cardGroups.Add(group2);

            var group3 = new CardGroup();
            group3.Cards.Add(CreateMultipleChoiceAuditStatementCard());
            cardGroups.Add(group3);

            var group4 = new CardGroup();
            group4.Cards.Add(CreateMultipleChoiceQuestionCard());
            cardGroups.Add(group4);

            var group5 = new CardGroup();
            group5.Cards.Add(CreateLeadingQuestionCard());
            cardGroups.Add(group5);

            var group6 = new CardGroup();
            group6.Cards.Add(CreateCompanyPolicyCard());
            cardGroups.Add(group6);

            var group7 = new CardGroup();
            group7.Cards.Add(CreateMultipleChoiceAuditStatementCard());
            group7.Cards.Add(CreateMultipleChoiceAuditStatementCard());
            cardGroups.Add(group7);

            var group8 = new CardGroup();
            group8.Cards.Add(CreateTextBoxSurveyCard());
            cardGroups.Add(group8);

            var group9 = new CardGroup();
            group9.Cards.Add(CreateMultipleCheckBoxQuestionCard());
            cardGroups.Add(group9);

            var group10 = new CardGroup();
            group10.Cards.Add(CreateFillInTheBlankQuestionCard());
            cardGroups.Add(group10);

            var group11 = new CardGroup();
            group11.Cards.Add(CreateArrangeSequenceCard());
            cardGroups.Add(group11);

            var group12 = new CardGroup();
            group12.Cards.Add(CreatePotLuckQuestionCard());
            cardGroups.Add(group12);

            var group13 = new CardGroup();
            group13.Cards.Add(CreateMatchingListQuestionCard());
            cardGroups.Add(group13);

            var group14 = new CardGroup();
            group14.Cards.Add(CreateNumericQuestionCard());
            cardGroups.Add(group14);

            for (var i = 0; i < 14; i++ )
            {
                var group = cardGroups[i];
                group.CardGroupSequenceNo = i + 1;
                group.IsMandatoryGroup = true;
            }

            return cardGroups;
        }

        #region Cards

        ArrangeSequenceQuestionCard CreateArrangeSequenceCard()
        {
            return new ArrangeSequenceQuestionCard()
                       {
                         AnswerExplanation  = GetAnswerExplanation(),
                         CardSequenceNo = 1,
                         CardType = CardType.ArrangeSequenceQuestionCard.ToString(),
                         Id = 2,
                         IgnoreAnyTimersOnThisCard = false,
                         IsATimedCard = true,
                         MaximumTimeToAnswerInSeconds = 1000,
                         Question = GetQuestion(),
                         UseCardTimeNotGameTime = false,
                         Scoring = GetScoring(),
                         PossibleAnswers = GetArrangeSequenceAnswers().Cast<Answer>().ToList(),
                         Results = GetArrangeSequencePlayerResults().Cast<PlayerResult>().ToList()
                       };
        }

        IEnumerable<ArrangeSequencePlayerResult> GetArrangeSequencePlayerResults()
        {
            var list = new List<ArrangeSequencePlayerResult>();
            for (int i = 0; i < 5; i++)
            {
                list.Add(
                     new ArrangeSequencePlayerResult()
                         {
                            PlayerId = Guid.NewGuid(),
                            Results = GetArrangeSequencePlayerResultAnswer().Cast<PlayerResultAnswer>().ToList()
                         }
                    );
            }

            return list;
        }

        IList<ArrangeSequencePlayerResultAnswer> GetArrangeSequencePlayerResultAnswer()
        {
            var list = new List<ArrangeSequencePlayerResultAnswer>();

            for (int j = 0; j < 5; j++)
            {
                list.Add(
                        new ArrangeSequencePlayerResultAnswer()
                            {
                                AnswerId = j + 1,
                                AnswerSequenceNo = j + 1
                            }
                    );
            }

            return list;
        }

        IEnumerable<ArrangeSequenceAnswer> GetArrangeSequenceAnswers()
        {
            var list = new List<ArrangeSequenceAnswer>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new ArrangeSequenceAnswer()
                        {
                            AnswerSequenceNo = i + 1,
                            Id = i,
                            IsCorrectAnswer = true,
                            PossibleAnswer = "possible answer " + i.ToString()
                        }
                    );
            }

            return list;
        }

        CompanyPolicyCard CreateCompanyPolicyCard()
        {
            return new CompanyPolicyCard()
                       {
                           CardSequenceNo = 1,
                           CardType = CardType.CompanyPolicyCard.ToString(),
                           CompanyPolicyAssociatedImageUrl = "Image.url",
                           Id = 10,
                           IsATimedCard = false
                       };
        }

        FillInTheBlankQuestionCard CreateFillInTheBlankQuestionCard()
        {
            return new FillInTheBlankQuestionCard()
                       {
                           AnswerExplanation = GetAnswerExplanation(),
                           AnswerIsCaseSensitive = false,
                           CardSequenceNo = 2,
                           CardType = CardType.FillInTheBlankQuestionCard.ToString(),
                           Id = 4,
                           IgnoreAnyTimersOnThisCard = false,
                           IsATimedCard = true,
                           MaximumTimeToAnswerInSeconds = 1000,
                           Question = GetQuestion(),
                           Scoring = GetScoring(),
                           UseCardTimeNotGameTime = false,
                           PossibleAnswers = GetFillInTheBlankAnswers().Cast<Answer>().ToList(),
                           Results = GetFillInTheBlankPlayerResults().Cast<PlayerResult>().ToList()
                           
                       };
        }

        IList<FillInTheBlankPlayerResult> GetFillInTheBlankPlayerResults()
        {
            var list = new List<FillInTheBlankPlayerResult>();
            var random = new Random();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new FillInTheBlankPlayerResult()
                        {
                            PlayerId = Guid.NewGuid(),
                            AnswerId = i + 1,
                            TimeTakenToAnswerInSeconds = random.Next(100, 1000)
                        }
                    );
            }

            return list;

        }

        IEnumerable<FillInTheBlankAnswer> GetFillInTheBlankAnswers()
        {
            var list = new List<FillInTheBlankAnswer>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new FillInTheBlankAnswer()
                        {
                            Id = i,
                            IsCorrectAnswer = true,
                            PossibleAnswer = "possible answer " + i,
                        }
                    );
            }

            return list;
        }

        LcdCard CreateLcdCard()
        {
            return new LcdCard()
                       {
                           CardSequenceNo = 1,
                           CardType = CardType.LcdCategoryCard.ToString(),
                           Id = 21,
                           IsATimedCard = true,
                           LcdAssociatedImageUrl = "Image.jpg",
                           LcdCategory = "LCD Category"
                       };
        }

        LeadingQuestionCard CreateLeadingQuestionCard()
        {
            return new LeadingQuestionCard()
                       {
                           CardSequenceNo = 3,
                           CardType = CardType.LeadingQuestionCard.ToString(),
                           Id = 33,
                           IsATimedCard = false,
                           Question = GetQuestion()
                       };
        }

        MatchingListQuestionCard CreateMatchingListQuestionCard()
        {
            return new MatchingListQuestionCard()
                       {
                           AnswerExplanation = GetAnswerExplanation(),
                           ApplyNegScoringForEachInCorrectAnswer = false,
                           ApplyScoreByCardElseForEachCorrectAnswer = true,
                           CardSequenceNo = 1,
                           CardType = CardType.MatchingListQuestionCard.ToString(),
                           Id = 89,
                           IgnoreAnyTimersOnThisCard = false,
                           IsATimedCard = true,
                           MaximumTimeToAnswerInSeconds = 500,
                           Question = GetQuestion(),
                           QuestionIsOnlyCorrectIfAllAnswersAreTrue = true,
                           Scoring = GetScoring(),
                           UseCardTimeNotGameTime = false,
                           PossibleAnswers = GetMatchingListAnswers().Cast<Answer>().ToList(),
                           Results = GetMatchingListPlayerResults().Cast<PlayerResult>().ToList()
                       };
        }

        IEnumerable<MatchingListAnswer> GetMatchingListAnswers()
        {
            var list = new List<MatchingListAnswer>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new MatchingListAnswer()
                    {
                        Id = i,
                        IsCorrectAnswer = true,
                        LHStatement = "Left Statement" + i,
                        RHStatement = "Right Statement" + i
                    }
                    );
            }

            return list;
        }

        IList<MatchingListPlayerResult> GetMatchingListPlayerResults()
        {
            var list = new List<MatchingListPlayerResult>();
            var random = new Random();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new MatchingListPlayerResult()
                    {
                        PlayerId = Guid.NewGuid(),
                        Results = GetMatchingListPlayerResultAnswers().Cast<PlayerResultAnswer>().ToList(),
                        TimeTakenToAnswerInSeconds = random.Next(100, 1000)                       
                    }
                    );
            }

            return list;
        }

        IList<MatchingListPlayerResultAnswer> GetMatchingListPlayerResultAnswers()
        {
            var list = new List<MatchingListPlayerResultAnswer>();
            
            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new MatchingListPlayerResultAnswer()
                    {
                        AnswerSequenceNo =  i + 1,
                        LHAnswerId = i + 1,
                        RHAnswerId = i + 1                        
                    }
                    );
            }

            return list;

        }

        MultipleCheckBoxQuestionCard CreateMultipleCheckBoxQuestionCard()
        {
            return new MultipleCheckBoxQuestionCard()
                       {
                           AnswerExplanation = GetAnswerExplanation(),
                           ApplyNegScoringForEachInCorrectAnswer = false,
                           ApplyScoreByCardElseForEachCorrectAnswer = false,
                           CardSequenceNo = 4,
                           Id = 22,
                           CardType = CardType.MultipleCheckBoxQuestionCard.ToString(),
                           IgnoreAnyTimersOnThisCard = true,
                           IsATimedCard = true,
                           MaximumTimeToAnswerInSeconds = 1000,
                           OfferOptOutChoice = false,
                           Question = GetQuestion(),
                           QuestionIsOnlyCorrectIfAllAnswersAreTrue = true,
                           Scoring = GetScoring(),
                           UseCardTimeNotGameTime = false,
                           PossibleAnswers = GetMultipleCheckBoxAnswers().Cast<Answer>().ToList(),
                           Results = GetMultipleCheckBoxPlayerResults().Cast<PlayerResult>().ToList()
                       };
        }

        IEnumerable<MultipleCheckBoxAnswer> GetMultipleCheckBoxAnswers()
        {
            var list = new List<MultipleCheckBoxAnswer>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new MultipleCheckBoxAnswer()
                    {
                        Id = i,
                        IsCorrectAnswer = true,
                        PossibleAnswer = "Possible answer" + i                        
                    }
                    );
            }

            return list;
        }

        IList<MultipleCheckBoxPlayerResult> GetMultipleCheckBoxPlayerResults()
        {
            var list = new List<MultipleCheckBoxPlayerResult>();
            var random = new Random();
            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new MultipleCheckBoxPlayerResult()
                        {
                            Results = GetMultipleCheckBoxPlayerResultAnswers().Cast<PlayerResultAnswer>().ToList(),
                            PlayerId = Guid.NewGuid(),
                            TimeTakenToAnswerInSeconds = random.Next(100, 1000)
                        }
                    );
            }

            return list;
        }

        IList<MultipleCheckBoxPlayerResultAnswer> GetMultipleCheckBoxPlayerResultAnswers()
        {
            var list = new List<MultipleCheckBoxPlayerResultAnswer>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new MultipleCheckBoxPlayerResultAnswer()
                    {
                        AnswerId = i + 1                        
                    }
                    );
            }

            return list;
        }

        MultipleChoiceQuestionCard CreateMultipleChoiceQuestionCard()
        {
            return new MultipleChoiceQuestionCard()
                       {
                           AnswerExplanation = GetAnswerExplanation(),
                           CardSequenceNo = 2,
                           CardType = CardType.MultipleChoiceQuestionCard.ToString(),
                           Id = 100,
                           IgnoreAnyTimersOnThisCard = false,
                           IsATimedCard = true,
                           MaximumTimeToAnswerInSeconds = 1000,
                           OfferOptOutChoice = false,
                           PossibleAnswers = GetMultipleChoiceAnswers().Cast<Answer>().ToList(),
                           Question = GetQuestion(),
                           Scoring = GetScoring(),
                           UseCardTimeNotGameTime = false,
                           Results = GetMultipleChoicePlayerResults().Cast<PlayerResult>().ToList()
                       };

        }

        IEnumerable<MultipleChoiceAnswer> GetMultipleChoiceAnswers()
        {
            var list = new List<MultipleChoiceAnswer>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new MultipleChoiceAnswer()
                    {
                        Id = i,
                        IsCorrectAnswer = true,
                        PossibleAnswer = "Possible answer" + i
                    }
                    );
            }

            return list;
        }

        IList<MultipleChoicePlayerResult> GetMultipleChoicePlayerResults()
        {
            var list = new List<MultipleChoicePlayerResult>();
            var random = new Random();
            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new MultipleChoicePlayerResult()
                    {
                        AnswerId = i + 1,
                        PlayerId = Guid.NewGuid(),
                        TimeTakenToAnswerInSeconds = random.Next(100, 1000)
                    }
                    );
            }

            return list;
        }

        MultipleCheckBoxSurveyCard CreateMultipleCheckBoxSurveyCard()
        {
            return new MultipleCheckBoxSurveyCard()
                       {
                           CardSequenceNo = 2,
                           CardType = CardType.MultipleCheckBoxSurveyCard.ToString(),
                           Id = 55,
                           IsAMandatoryQuestion = false,
                           IsATimedCard = false,
                           OfferOptOutChoice = false,
                           OfferOtherOption = false,
                           OtherOptionIsTextBoxElseTextArea = false,
                           Question = GetQuestion(),
                           PossibleAnswers = GetRhethoricalAnswers().Cast<Answer>().ToList()                           
                       };
        }

        MultipleChoiceAuditStatementCard CreateMultipleChoiceAuditStatementCard()
        {
            return new MultipleChoiceAuditStatementCard()
                       {
                           CardSequenceNo = 5,
                           CardType = CardType.MultipleChoiceAuditStatementCard.ToString(),
                           Id = 77,
                           IsATimedCard = false,
                           OfferOptOutChoice = false,
                           Question = GetQuestion(),
                           PossibleAnswers = GetRhethoricalAnswers().Cast<Answer>().ToList()                          
                       };
        }

        IEnumerable<StatementAnswer> GetRhethoricalAnswers()
        {
            var list = new List<StatementAnswer>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new StatementAnswer()
                    {
                        Id = i,
                        PossibleAnswer = "Possible answer" + i                        
                    }
                    );
            }

            return list;
        }

        MultipleChoiceSurveyCard CreateMultipleChoiceSurveyCard()
        {
            return new MultipleChoiceSurveyCard()
                       {
                           CardSequenceNo = 3,
                           CardType = CardType.MultipleChoiceSurveyCard.ToString(),
                           Id = 29,
                           IsAMandatoryQuestion = false,
                           IsATimedCard = true,
                           OfferOptOutChoice = false,
                           OfferOtherOption = false,
                           OtherOptionIsTextBoxElseTextArea = false,
                           PossibleAnswers = GetRhethoricalAnswers().Cast<Answer>().ToList(),
                           Question = GetQuestion()
                       };
        }

        NumericQuestionCard CreateNumericQuestionCard()
        {
            return new NumericQuestionCard()
                       {
                           AnswerExplanation = GetAnswerExplanation(),
                           ApplyNegScoringForEachInCorrectAnswer = false,
                           ApplyScoreByCardElseForEachCorrectAnswer = false,
                           CardSequenceNo = 7,
                           CardType = CardType.NumericQuestionCard.ToString(),
                           Id = 18,
                           IgnoreAnyTimersOnThisCard = false,
                           IsATimedCard = true,
                           MaximumTimeToAnswerInSeconds = 1000,
                           PossibleAnswers = GetNumericAnswers().Cast<Answer>().ToList(),
                           Question = GetQuestion(),
                           QuestionIsOnlyCorrectIfAllAnswersAreTrue = false,
                           Scoring = GetScoring(),
                           UseCardTimeNotGameTime = false,
                           Results = GetNumericPlayerResults().Cast<PlayerResult>().ToList()
                       };
        }

        IEnumerable<NumericAnswer> GetNumericAnswers()
        {
            var list = new List<NumericAnswer>();

            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new NumericAnswer()
                    {
                        Id = i,
                        IsCorrectAnswer = true,
                        Operand = "IsGreaterThan",
                        Value1 = 10,
                        Value2 = 200
                    }
                    );
            }

            return list;
        }

        IList<NumericPlayerResult> GetNumericPlayerResults()
        {
            var list = new List<NumericPlayerResult>();
            var random = new Random();
            for (int i = 0; i < 5; i++)
            {
                list.Add(
                    new NumericPlayerResult()
                    {
                        PlayerId = Guid.NewGuid(),
                        TimeTakenToAnswerInSeconds = random.Next(100, 1000),
                        Answer = random.Next(1,8)
                    }
                    );
            }

            return list;
        }


        PotLuckQuestionCard CreatePotLuckQuestionCard()
        {
            return new PotLuckQuestionCard()
                       {
                           AnswerExplanation = GetAnswerExplanation(),
                           CardSequenceNo = 11,
                           CardType = CardType.PotLuckQuestionCard.ToString(),
                           Id = 111,
                           IgnoreAnyTimersOnThisCard = false,
                           IsATimedCard = true,
                           MaximumTimeToAnswerInSeconds = 100,
                           Question = GetQuestion(),
                           Scoring = GetScoring(),
                           UseCardTimeNotGameTime = false                           
                       };
        }

        TextAreaSurveyCard CreateTextAreaSurveyCard()
        {
            return new TextAreaSurveyCard()
                       {
                           CardSequenceNo = 2,
                           CardType = CardType.TextAreaSurveyCard.ToString(),
                           Id = 222,
                           IsAMandatoryQuestion = false,
                           IsATimedCard = false,
                           OtherOptionIsTextBoxElseTextArea = false,
                           Question = GetQuestion()
                       };
        }

        TextBoxSurveyCard CreateTextBoxSurveyCard()
        {
            return new TextBoxSurveyCard()
            {
                CardSequenceNo = 8,
                CardType = CardType.TextAreaSurveyCard.ToString(),
                Id = 223,
                IsAMandatoryQuestion = false,
                IsATimedCard = false,
                OtherOptionIsTextBoxElseTextArea = false,
                Question = GetQuestion()
            };
        }

        #endregion

        #region General Helper Methods

        List<Player> CreatePlayers ()
        {
            var players = new List<Player>();
            for(int i=0; i < 4; i++)
            {
                players.Add(
                    new Player()
                        {
                            Id = Guid.NewGuid(),
                            AvatarData = "Image.png",
                            AvatarGender = "M",
                            AvatarName = "Test Avatar" + i,
                            HasViewedGameTutorial = true,
                            IsSimulated = false,
                            RealName = "User Realname" + i,
                            PlayerFirstName = "User",
                            PlayerLastName = "Realname" + i
                        }
                    );
            }

            return players;
        }

        AnswerExplain GetAnswerExplanation()
        {
            return new AnswerExplain()
                       {
                           AnswerExplanation = "Answer statement",
                           AnswerMultimediaShowPlaybackControls = true,
                           AnswerMultimediaStartAutoElsePushPlayToStart = true,
                           AnswerMultimediaTypeImageVideoEtc = "YouTube",
                           AnswerMultimediaUrl = "www.youtube.com"
                       };
        }

        QuestionExplain GetQuestion()
        {
            return new QuestionExplain()
                       {
                           QuestionMultimediaShowPlaybackControls = true,
                           QuestionMultimediaStartAutoElsePushPlayToStart = true,
                           QuestionMultimediaTypeImageVideoEtc = "YouTube",
                           QuestionMultimediaUrl = "www.youtube.com",
                           QuestionStatement = "Question statement"
                       };
        }

        OnlineScore GetScoring()
        {
            return new OnlineScore()
                       {
                           OnlinePlayerLosing = 3,
                           OnlinePlayerWinning = 3
                       };
        }

        #endregion
    }
}
