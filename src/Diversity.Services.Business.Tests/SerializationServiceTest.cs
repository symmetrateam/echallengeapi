﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Domain.Model;
using Diversity.Services.Business.Implementation;
using NUnit.Framework;

namespace Diversity.Services.Business.Tests
{
    [TestFixture]
    public class SerializationServiceTest : TestBase
    {
        [SetUp]
        public void StartUp()
        {
            _serializationService = new SerializationService("Diversity.Domain.Model", "Diversity.Domain.Model");
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SerializationService_ToXmlString_IsNull()
        {
            var result = _serializationService.ToXmlString(null);
        }

        [Test]
        public void SerializationService_ToXmlString_Convert()
        {
            var session = new Session();
            var result = _serializationService.ToXmlString(session);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }

        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void SerializationService_FromXmlString_IsEmpty()
        {
            var result = _serializationService.FromXmlString(string.Empty);
        }


    }
}
