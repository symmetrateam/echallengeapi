﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Diversity.Presentation.TestApplication
{
    public enum StatusEnum : int
    {
        Passed,
        Failed,
        Unknown
    }
    public partial class StatusControl : System.Web.UI.UserControl
    {
        private const string StatusKey = "StatusViewState";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public StatusEnum Status
        {
            get { return ViewState[StatusKey] == null ? StatusEnum.Unknown : (StatusEnum) ViewState[StatusKey]; }
            set { ViewState[StatusKey] = value; }
        }

    }
}