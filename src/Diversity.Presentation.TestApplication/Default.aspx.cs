﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.Presentation.TestApplication.AuthenticationService;
using Diversity.Presentation.TestApplication.GameService;

namespace Diversity.Presentation.TestApplication
{
    public partial class Default : System.Web.UI.Page
    {
        private MessageHeader _authorizationHeader;
        private MessageHeader _acceptLanguageHeader;
        private AuthenticationTicket _authenticationTicket;
        private const string FailedMessage = "Failed";
        private const string OkMessage = "Ok";
        private const string YesMessage = "Yes";
        private const string NoMessage = "No";
        private const string UnknownMessage = "Unknown";

        protected void Page_Load(object sender, EventArgs e)
        {
            var s = SetBrowserDetails();
            BrowserDetails.Text = s;

            HostNameLabel.Text = Request.UserHostName;
            HostIpLabel.Text = Request.UserHostAddress;

            CanAccessAuthenticationService();
            CanAccessGameService();
        }

        private void CanAccessAuthenticationService()
        {
            using (var gameServiceClient = new GameServiceClient())
            {
                try
                {
                    var canContactGameClient = gameServiceClient.IsGameServiceAvailable();
                    if (canContactGameClient)
                    {
                        ContactAuthenticationServiceStatus.Status = StatusEnum.Passed;
                    }
                }
                catch (Exception e)
                {
                    ContactAuthenticationServiceStatus.Status = StatusEnum.Failed;
                }

                ContactAuthenticationServiceStatus.Visible = true;
            }
        }

        private void CanAccessGameService()
        {
            using (var authenticationServiceClient = new AuthenticationServiceClient())
            {
                try
                {
                    var canContactAdmin = authenticationServiceClient.IsAuthenticationServiceAvailable();
                    if (canContactAdmin)
                    {
                        ContactGameServiceStatus.Status = StatusEnum.Passed;
                    }
                }
                catch (Exception e)
                {
                    ContactGameServiceStatus.Status = StatusEnum.Failed;
                }

                ContactGameServiceStatus.Visible = true;
            }
        }

        private bool AuthenticateUser(string username, string password)
        {
            _authenticationTicket = null;

            using (var authenticationServiceClient = new AuthenticationServiceClient())
            {
                try
                {
                    _authenticationTicket = authenticationServiceClient.Authenticate(username.Trim(), password.Trim());
                    return true;
                }
                catch (FaultException<AuthenticationFailedException> ex)
                {
                    LoginValidator.IsValid = false;
                    LoginValidator.ErrorMessage = ex.Message;
                    return false;
                }                
            }
        }

        private bool GetDashboard(out Dashboard dashboard)
        {
            try
            {
                using (var client = new GameServiceClient())
                {
                    _authorizationHeader = MessageHeader.CreateHeader("Authorization", string.Empty,
                                                     _authenticationTicket.Token);
                    _acceptLanguageHeader = MessageHeader.CreateHeader("X-Accept-Language", string.Empty, "en-EN");


                    var scope = new OperationContextScope(client.InnerChannel);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_authorizationHeader);
                    OperationContext.Current.OutgoingMessageHeaders.Add(_acceptLanguageHeader);

                    var dash = client.GetDashboard();
                    dashboard = dash;
                    return true;
                }
            }
            catch (FaultException<UnauthorizedAccessException> ex)
            {
                dashboard = null;
                return false;
            }
            catch (FaultException<GameService.SessionTimeoutException> ex)
            {
                dashboard = null;
                return false;
            }
            catch (FaultException<ServiceControllerException> ex)
            {
                dashboard = null;
                return false;
            }
        }

        private string SetBrowserDetails()
        {
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            string s = "Browser Capabilities\n"
                + "Type = " + browser.Type + "\n"
                + "Name = " + browser.Browser + "\n"
                + "Version = " + browser.Version + "\n"
                + "Major Version = " + browser.MajorVersion + "\n"
                + "Minor Version = " + browser.MinorVersion + "\n"
                + "Platform = " + browser.Platform + "\n"
                + "Is Beta = " + browser.Beta + "\n"
                + "Is Crawler = " + browser.Crawler + "\n"
                + "Is AOL = " + browser.AOL + "\n"
                + "Is Win16 = " + browser.Win16 + "\n"
                + "Is Win32 = " + browser.Win32 + "\n"
                + "Supports Frames = " + browser.Frames + "\n"
                + "Supports Tables = " + browser.Tables + "\n"
                + "Supports Cookies = " + browser.Cookies + "\n"
                + "Supports VBScript = " + browser.VBScript + "\n"
                + "Supports JavaScript = " +
                    browser.EcmaScriptVersion.ToString() + "\n"
                + "Supports Java Applets = " + browser.JavaApplets + "\n"
                + "Supports ActiveX Controls = " + browser.ActiveXControls
                      + "\n"
                + "Supports JavaScript Version = " +
                    browser["JavaScriptVersion"] + "\n";

            return s;
        }

        protected void OnClick(object sender, EventArgs e)
        {

            AuthenticatedUserStatus.Status = StatusEnum.Unknown;

            DashboardStatus.Status = StatusEnum.Unknown;                       

            DashboardAvailableStatus.Status = StatusEnum.Unknown;            

            DashboardHasGamesStatus.Status = StatusEnum.Unknown;

            var username = UserNameBox.Text;
            var password = PasswordBox.Text;

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrEmpty(username) ||
                string.IsNullOrWhiteSpace(password) || string.IsNullOrEmpty(password))
            {
                return;
            }

            var authenticated = AuthenticateUser(username, password);

            if (authenticated)
            {
                AuthenticatedUserStatus.Status = StatusEnum.Passed;                

                Dashboard dashboard;
                var hasDashboard = GetDashboard(out dashboard);

                if (hasDashboard)
                {
                    DashboardStatus.Status = StatusEnum.Passed;                    

                    if (dashboard == null)
                    {
                        DashboardAvailableStatus.Status = StatusEnum.Unknown;                        
                    }
                    else
                    {
                        DashboardAvailableStatus.Status = StatusEnum.Passed;

                        if (dashboard.Games.Any())
                        {
                            DashboardHasGamesStatus.Status = StatusEnum.Passed;                            
                        }
                        else
                        {
                            DashboardHasGamesStatus.Status = StatusEnum.Failed;                            
                        }
                    }
                }
                else
                {
                    DashboardStatus.Status = StatusEnum.Failed;
                }
            }
            else
            {
                AuthenticatedUserStatus.Status = StatusEnum.Failed;
            }
        }
    }
}