﻿<%@ control language="C#" autoeventwireup="true" codebehind="StatusControl.ascx.cs" inherits="Diversity.Presentation.TestApplication.StatusControl" %>
<%@ import namespace="Diversity.Presentation.TestApplication" %>
<style>

</style>
<% if (Status == StatusEnum.Passed)
   { %>
<div class="green">
    Passed
</div>
<%--<img src="Images/Check.png" alt="Pass" class="image" />--%>
<% }
   else if (Status == StatusEnum.Failed)
   { %>
<div class="red">Failed</div>
<%--<img src="Images/Cross.png" alt="Failed" class="image" />--%>
<% }
   else
   { %>
<div class="orange">Unknown</div>
<%--<img src="Images/Unknown.png" alt="Unknown" class="image" />--%>
<% } %>
