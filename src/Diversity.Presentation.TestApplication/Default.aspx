﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Diversity.Presentation.TestApplication.Default" %>

<%@ Register Src="StatusControl.ascx" TagName="Status" TagPrefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Client Test</title>
    <link type="text/css" href="Styles.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr>
                <td>
                    <h1>Host Information</h1>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <h2>Hostname:</h2>
                </td>
                <td>
                    <h2>
                        <asp:Label ID="HostNameLabel" runat="server" Text="HOST INFORMATION"></asp:Label></h2>
                </td>
            </tr>
            <tr>
                <td>
                    <h2>IP Address:</h2>
                </td>
                <td>
                    <h2>
                        <asp:Label ID="HostIpLabel" runat="server" Text="HOST INFORMATION"></asp:Label></h2>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <h2>Browser Information</h2>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox Height="200" Width="600" Wrap="True" TextMode="MultiLine" runat="server" ID="BrowserDetails"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td>
                    <h2>Service Information</h2>
                </td>
            </tr>
        </table>
        <table class="gridview">
            <tr>
                <th>Testing</th>
                <th>Status</th>
            </tr>
            <tr>
                <td>Connection to authentication service 
                </td>
                <td>
                    <uc1:Status ID="ContactAuthenticationServiceStatus" runat="server" />
                </td>
            </tr>
            <tr>
                <td>Connection to game service
                </td>
                <td>
                    <uc1:Status ID="ContactGameServiceStatus" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td colspan="2">
                            <h2>User Details</h2>                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="UserValidationSummary" runat="server" DisplayMode="BulletList" ShowMessageBox="False" CssClass="validationsummary" />
                        </td>
                    </tr>
                    <tr>
                        <td>Email address</td>
                        <td>
                            <asp:TextBox runat="server" ID="UserNameBox">                            
                            </asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="errormsg" runat="server" ControlToValidate="UserNameBox" ErrorMessage="Email address is required" EnableClientScript="True" Text="*" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator CssClass="errormsg" runat="server" ControlToValidate="UserNameBox" ErrorMessage="Email address is not in a valid format" EnableClientScript="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Text="*" Display="None"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator CssClass="errormsg" runat="server" ControlToValidate="UserNameBox" ErrorMessage="Email address cannot contain spaces" EnableClientScript="True" ValidationExpression="^[^\ ]+$" Text="*" Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>
                            <asp:TextBox runat="server" ID="PasswordBox" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="errormsg" runat="server" ControlToValidate="PasswordBox" ErrorMessage="Password is required" EnableClientScript="True" Text="*" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator CssClass="errormsg" runat="server" ControlToValidate="PasswordBox" ErrorMessage="Password does not meet the minimum password requirements" EnableClientScript="True" ValidationExpression="(?=^.{6,}$)(?=.*\d)(?=.*\W+)(?![.\n])[^/]*$" Text="*" Display="None"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator CssClass="errormsg" runat="server" ControlToValidate="PasswordBox" ErrorMessage="Password cannot contain spaces" EnableClientScript="True" ValidationExpression="^[^\ ]+$" Text="*" Display="None"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan>
                            <asp:Button ID="Button1" runat="server" Text="Login" OnClick="OnClick" />
                            <asp:CustomValidator ID="LoginValidator" runat="server" Display="None" EnableClientScript="False"></asp:CustomValidator>
                        </td>
                    </tr>
                </table>
                <br />
                <table class="gridview">
                    <tr>
                        <th>Testing</th>
                        <th>Status</th>
                    </tr>
                    <tr>
                        <td>User authentication</td>
                        <td>
                            <uc1:Status ID="AuthenticatedUserStatus" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Dashboard service call</td>
                        <td>
                            <uc1:Status ID="DashboardStatus" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Dashboard data found</td>
                        <td>
                            <uc1:Status ID="DashboardAvailableStatus" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Dashboard data contains games</td>
                        <td>
                            <uc1:Status ID="DashboardHasGamesStatus" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
