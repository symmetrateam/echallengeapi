﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;

namespace BlueTorque.Net.Security.Web
{
    public class AuthenticationHelper
    {
        public static bool ValidateUser(string userName, string password, string ipAddress, out string errorMessage)
        {
            bool authenticated = false;
            errorMessage = string.Empty;
            //now determine user
            if (Membership.ValidateUser(userName, password))
            {
                authenticated = true;
            }
            else
            {
                var userInfo = Membership.GetUser(userName);
                errorMessage = GetFriendlyLoginFailMessage(userInfo);
            }

            return authenticated;
        }

        public static bool ValidateUser(string userName, string password, string ipAddress, out string errorMessage, out object providerUserKey)
        {
            bool authenticated = false;
            errorMessage = string.Empty;
            //now determine user
            if (Membership.ValidateUser(userName, password))
            {
                authenticated = true;

                var user = Membership.GetUser(userName);
                providerUserKey = user.ProviderUserKey;
            }
            else
            {
                providerUserKey = null;
                var userInfo = Membership.GetUser(userName);
                errorMessage = GetFriendlyLoginFailMessage(userInfo);
            }

            return authenticated;
        }

        public static MembershipUser ValidateUser(string userName, string password, string ipAddress)
        {
            MembershipUser validatedUser = null;
            //now determine user
            if (Membership.ValidateUser(userName, password))
            {
                validatedUser = Membership.GetUser(userName);           
            }         

            return validatedUser;
        }        

        #region Private Methods

        private static string GetFriendlyLoginFailMessage(MembershipUser userInfo)
        {
            string msg = string.Empty;

            if (userInfo == null)
            {
                msg = @"Your login attempt was not successful. Please try again.";
            }
            else
            {
                if (!userInfo.IsApproved)
                {
                    msg = @"Your account is not activated. Please contact your administrator.";
                }
                else if (userInfo.IsLockedOut)
                {
                    msg = @"Your account has been locked out. Please contact your administrator.";
                }
                else
                {                    
                    msg = @"Your username and/or password is incorrect.";
                }
            }
            return msg;
        }

        #endregion
    }
}
