﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web;

namespace BlueTorque.Net.Security.Web
{
    public class RolesGroups
    {
        public static List<RolesGroup> GetRoleGroups()
        {            
            return SecurityDataAccessor.GetUserRoleGroups(Membership.ApplicationName);
        }

        public static List<string> GetRoleGroupRoles(string roleGroupName)
        {
            if (string.IsNullOrEmpty(roleGroupName))
            {
                throw new ArgumentNullException("roleGroupName");
            }

            return SecurityDataAccessor.GetRoleGroupDefaultRoles(Membership.ApplicationName, roleGroupName);
        }

        public static void CreateRoleGroup(string roleGroupName, List<string> roleNames)
        {
            if (string.IsNullOrEmpty(roleGroupName))
            {
                throw new ArgumentNullException("roleGroupName");
            }

            if (roleNames == null
                || roleNames.Count <= 0)
            {
                throw new ArgumentNullException("roleNames");
            }

            Guid? roleGroupId = null;

            int result = SecurityDataAccessor.CreateRoleGroup(Membership.ApplicationName, roleGroupName, roleNames, roleGroupId);

            if (result < 0)
            {
                throw new System.Configuration.Provider.ProviderException(string.Format(@"Cannot create role group {0}", roleGroupName));
            }
        }

        //public static void UpdateRoleGroup(string roleGroupName, List<string> roleNames)
        //{
        //    if (string.IsNullOrEmpty(roleGroupName))
        //    {
        //        throw new ArgumentNullException("roleGroupName");
        //    }

        //    if (roleNames == null
        //        || roleNames.Count <= 0)
        //    {
        //        throw new ArgumentNullException("roleNames");
        //    }

        //    int result = SecurityDataAccessor.UpdateRoleGroup(Membership.ApplicationName, roleGroupName, roleNames);

        //    if (result < 0)
        //    {
        //        throw new System.Configuration.Provider.ProviderException(string.Format(@"Cannot update role group {0}", roleGroupName));
        //    }
        //}

        public static void DeleteRoleGroup(string roleGroupName)
        {
            if (string.IsNullOrEmpty(roleGroupName))
            {
                throw new ArgumentNullException("roleGroupName");
            }

            int result = SecurityDataAccessor.DeleteRoleGroup(Membership.ApplicationName, roleGroupName);

            if (result < 0)
            {
                throw new System.Configuration.Provider.ProviderException(string.Format(@"Unable to delete role group {0}", roleGroupName));
            }
        }
    }
}
