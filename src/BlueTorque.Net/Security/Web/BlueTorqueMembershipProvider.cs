﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Reflection;
using Diversity.Common.Extensions;
using Diversity.DataModel.SqlRepository;

namespace BlueTorque.Net.Security.Web
{
    public class BlueTorqueMembershipProvider : SqlMembershipProvider
    {
        private const int WorkFactor = 10;
        private const string ChangePasswordAtNextLogin = "";       

        EChallengeDataContext GetObjectContext()
        {
            return new EChallengeDataContext();
        }

        public override bool ValidateUser(string username, string password)
        {
            var objectContext = GetObjectContext();

            var user = objectContext.Users.FirstOrDefault(u => u.LoweredUserName == username.ToLower());

            if (user == null)
            {
                return false;
            }

            if (!user.IsApproved)
            {
                return false;
            }

            if (user.IsLockedOut && user.LastLockoutDate.AddMinutes(PasswordAttemptWindow) <= DateTime.UtcNow)
            {
                return false;
            }

            var isValidated = BCrypt.Net.BCrypt.Verify(password, user.Password);

            var minSqlDate = new DateTime(1754, 01, 01);

            if (isValidated)
            {
                if (user.IsLockedOut)
                {
                    user.IsLockedOut = false;
                    user.LastLockoutDate = minSqlDate;
                    user.FailedPasswordAttemptCount = 0;
                    user.FailedPasswordAttemptWindowStart = minSqlDate;
                }
            }
            else
            {
                var failedPasswordAttemptCount = user.FailedPasswordAttemptCount;
                failedPasswordAttemptCount++;

                if (failedPasswordAttemptCount == Membership.MaxInvalidPasswordAttempts)
                {
                    user.IsLockedOut = true;
                    user.LastLockoutDate = DateTime.UtcNow.RoundToSeconds();
                    user.FailedPasswordAttemptCount = failedPasswordAttemptCount;
                }

                user.FailedPasswordAttemptCount = failedPasswordAttemptCount;
            }

            objectContext.SaveChanges();

            return isValidated;
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            var objectContext = GetObjectContext();

            if (ValidateUser(username, oldPassword))
            {
                var user = objectContext.Users.FirstOrDefault(u => u.LoweredUserName == username.ToLower());
                var encodedPassword = EncodePassword(newPassword);
                user.Password = encodedPassword;
                user.PasswordSalt = string.Empty;
                user.LastPasswordChangedDate = DateTime.UtcNow.RoundToSeconds();

                if (objectContext.SaveChanges() > 0)
                {
                    var membershipUser = Membership.GetUser(username);
                    if (!string.IsNullOrEmpty(membershipUser.Comment))
                    {
                        membershipUser.Comment = string.Empty;
                        Membership.UpdateUser(membershipUser);
                    }
                    return true;
                }
            }

            return false;
        }

        public string ResetPassword(string username)
        {
            var objectContext = GetObjectContext();

            var user = objectContext.Users.FirstOrDefault(u => u.LoweredUserName == username.ToLower());

            if (user == null)
            {
                return null;
            }


            if (string.IsNullOrEmpty(user.Email) && user.Email != username)
            {
                return null;
            }

            var generatedPassword = GeneratePassword();
            var encodedPassword = EncodePassword(generatedPassword);

            var dateUtc = DateTime.UtcNow.RoundToSeconds();

            user.Password = encodedPassword;
            user.LastPasswordChangedDate = dateUtc;

            objectContext.SaveChanges();

            //if (result > 0)
            //{
            //    var membershipUser = Membership.GetUser(username);
            //    membershipUser.Comment = ChangePasswordAtNextLogin;
            //    Membership.UpdateUser(membershipUser);
            //}

            return generatedPassword;
        }

        public override string ResetPassword(string username, string passwordAnswer)
        {
            return ResetPassword(username);
        }

        internal string GeneratePasswordSalt()
        {
            return BCrypt.Net.BCrypt.GenerateSalt(WorkFactor);
        }

        internal string EncodePassword(string password)
        {
            return EncodePassword(password, GeneratePasswordSalt());
        }

        internal string EncodePassword(string password, string passwordSalt)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, passwordSalt);
        }

        internal new string GeneratePassword()
        {
            var password = string.Empty;
            var passwordValid = false;

            while (!passwordValid)
            {
                password = Membership.GeneratePassword(10, Membership.MinRequiredNonAlphanumericCharacters);
                passwordValid = Regex.IsMatch(password, Membership.PasswordStrengthRegularExpression);
            }

            return password;
        }
    }
}
