﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using System.Web.Security;
using Diversity.DataModel.SqlRepository;

namespace BlueTorque.Net.Security.Web
{
    internal class SecurityDataAccessor
    {
        #region User Roles Groups

        public static List<RolesGroup> GetUserRoleGroups(string applicationName)
        {
            var context = new EChallengeDataContext();
            
            var roleGroupsResultSet = context.GetRoleGroups(applicationName);
           List<string> roleGroupNameList = roleGroupsResultSet
                .Select(c => c.UserRolesGroupName).ToList<string>();

            List<RolesGroup> collection = null;

            if (roleGroupNameList.Count() > 0)
            {
                if (collection == null)
                {
                    collection = new List<RolesGroup>();
                }

                foreach (string name in roleGroupNameList)
                {
                    collection.Add(
                        new RolesGroup
                        {
                            GroupName = name
                        });
                }
            }

            return collection;
        }

        public static List<string> GetRoleGroupDefaultRoles(string applicatonName, string userRoleGroupName)
        {
            var context = new EChallengeDataContext();

            var rolesResultSet = context.GetAllUserRolesGroupsDefaults(applicatonName, userRoleGroupName);
            IEnumerable<string> roles = rolesResultSet.Select(x=> x);

            return roles.ToList();           
        }

        public static int CreateRoleGroup(string applicationName, string roleGroupName, List<string> roleNames, Guid? roleGroupId)
        {
            int result = 0;

            if (!string.IsNullOrEmpty(roleGroupName) && roleNames.Count > 0)
            {
                var context = new EChallengeDataContext();

                var param = new ObjectParameter("UserRolesGroupId", roleGroupId);
                result = context.CreateUserRolesGroup(applicationName, roleGroupName, param);

                roleNames.ForEach(delegate(string s)
                {
                    result = context.CreateUserRolesGroupsDefault(applicationName, roleGroupName, s);
                });
            }
            
            return result;
        }

        //public static int UpdateRoleGroup(string applicationName, string roleGroupName, List<string> roleNames)
        //{
        //    int result = 0;
        //    if (!string.IsNullOrEmpty(roleGroupName) && roleNames.Count > 0)
        //    {
        //        SecurityDbDataContext context = new SecurityDbDataContext(DatabaseFactory.CreateDatabase().CreateConnection());

        //        roleNames.ForEach(delegate(string s)
        //        {
        //            result += context.DeleteUserRolesGroup(applicationName, roleGroupName);
        //        });

        //        roleNames.ForEach(delegate(string s)
        //        {
        //            Guid? roleGroupId = null;
        //            result += CreateRoleGroup(applicationName, roleGroupName, roleNames, roleGroupId); 
        //        });
        //    }

        //    return result;
        //}

        public static int DeleteRoleGroup(string applicationName, string roleGroupName)
        {
            var context = new EChallengeDataContext();

            int result = context.DeleteUserRolesGroup(applicationName, roleGroupName);

            return result;
        }

        #endregion

        //#region Applications

        ///// <summary>
        ///// Creates a new application 
        ///// </summary>
        ///// <param name="applicationName">The application name</param>
        ///// <returns>A unique identifier for the application</returns>
        //public Guid CreateApplication(string applicationName)
        //{
        //    Database db = DatabaseFactory.CreateDatabase(Properties.Settings.Default.SecurityDbKey);
     
        //    string sqlCommand = "Applicatons_CreateApplication";
        //    DbCommand command = db.GetStoredProcCommand(sqlCommand);
            
        //    //input parameters
        //    db.AddInParameter(command, "applicationName", DbType.String, applicationName);
            
        //    //output parameters
        //    db.AddOutParameter(command, "applicationId", DbType.Guid, 16);

        //    db.ExecuteNonQuery(command);

        //    Guid applicationGuid = (Guid)db.GetParameterValue(command, "applicationId");

        //    return applicationGuid;
        //}

        //#endregion

        //#region Users

        ///// <summary>
        ///// Creates a new user without membership 
        ///// </summary>
        ///// <param name="applicationId">The unique identifier of the application</param>
        ///// <param name="userName">The user name to create</param>
        ///// <param name="lastActivityDate">The last datetime an authentication request was made</param>
        ///// <returns>A unique identifier of the user</returns>
        //public Guid CreateUser(Guid applicationId, string userName, DateTime lastActivityDate)
        //{
        //    Database db = DatabaseFactory.CreateDatabase(Properties.Settings.Default.SecurityDbKey);

        //    string sqlCommand = "Users_CreateUser.sql";
        //    DbCommand command = db.GetStoredProcCommand(sqlCommand);
            
        //    //input parameters
        //    db.AddInParameter(command, "applicationId", DbType.Guid, applicationId);
        //    db.AddInParameter(command, "userName", DbType.String, userName);
        //    db.AddInParameter(command, "lastActivityDate", DbType.DateTime, lastActivityDate);
 
        //    //output parameters
        //    db.AddOutParameter(command, "userId", DbType.Guid, 16);

        //    db.ExecuteNonQuery(command);

        //    Guid userId = (Guid)db.GetParameterValue(command, "userId");

        //    return userId;
        //}

        ///// <summary>
        ///// Deletes a user from the db depending on auditing requirements
        ///// </summary>
        ///// <param name="applicationName">The name of the application</param>
        ///// <param name="userName">The username to delete</param>
        ///// <param name="tableToDeleteFrom">The table to delete from</param>
        ///// <returns>An integer values indicating the number of tables deleted from</returns>
        //public int DeleteUser(string applicationName, string userName, TablesToDeleteFrom tableToDeleteFrom)
        //{
        //    Database db = DatabaseFactory.CreateDatabase(Properties.Settings.Default.SecurityDbKey);

        //    string sqlCommand = "Users_DeleteUser";
        //    DbCommand command = db.GetStoredProcCommand(sqlCommand);

        //    //input parameters
        //    db.AddInParameter(command, "applicationName", DbType.String, applicationName);
        //    db.AddInParameter(command, "userName", DbType.String, userName);
        //    db.AddInParameter(command, "tablesToDeleteFrom", DbType.Int32, (int)tableToDeleteFrom);

        //    //output parameters
        //    db.AddOutParameter(command, "numTablesDeletedFrom", DbType.Int32, 4);

        //    db.ExecuteNonQuery(command);

        //    int numTablesDeletedFrom = (int)db.GetParameterValue(command, "numTablesDeletedFrom");

        //    return numTablesDeletedFrom;
        //}

        //#endregion

        //#region Membership

        ///// <summary>
        ///// Creates a new user and membership
        ///// </summary>
        ///// <param name="applicationName">The application name</param>
        ///// <param name="userName">The user name</param>
        ///// <param name="password">A password</param>
        ///// <param name="passwordSalt">The salt used for encryption</param>
        ///// <param name="email">The email address</param>
        ///// <param name="passwordQuestion">The password question</param>
        ///// <param name="passwordAnswer">An answer to the password question</param>
        ///// <param name="isApproved">Is the user approved for access</param>
        ///// <param name="currentTimeUtc">The current universal time</param>
        ///// <param name="createDate">The date the user is created</param>
        ///// <param name="uniqueEmail">Use unique email only</param>
        ///// <returns>A unique indentifier of the user</returns>
        //public Guid CreateUser(string applicationName, string userName, string password,
        //    string passwordSalt, string email, string passwordQuestion, string passwordAnswer,
        //    bool isApproved, DateTime currentTimeUtc, DateTime createDate, bool uniqueEmail)
        //{
        //    Database db = DatabaseFactory.CreateDatabase(Properties.Settings.Default.SecurityDbKey);

        //    string sqlCommand = "Membership_CreateUser";
        //    DbCommand command = db.GetStoredProcCommand(sqlCommand);

        //    //input parameters
        //    db.AddInParameter(command, "applicationName", DbType.String, applicationName);
        //    db.AddInParameter(command, "userName", DbType.String, userName);
        //    db.AddInParameter(command, "password", DbType.String, password);
        //    db.AddInParameter(command, "passwordSalt", DbType.String, passwordSalt);
        //    db.AddInParameter(command, "email", DbType.String, email);
        //    db.AddInParameter(command, "passwordQuestion", DbType.String, passwordQuestion);
        //    db.AddInParameter(command, "passwordAnswer", DbType.String, passwordAnswer);
        //    db.AddInParameter(command, "isApproved", DbType.Boolean, isApproved);
        //    db.AddInParameter(command, "currentTimeUtc", DbType.DateTime, currentTimeUtc);
        //    db.AddInParameter(command, "createDate", DbType.DateTime, createDate);
        //    db.AddInParameter(command, "uniqueEmail", DbType.Int32, Convert.ToInt32(uniqueEmail));

        //    //output parameters
        //    db.AddOutParameter(command, "userId", DbType.Guid, 16);

        //    db.ExecuteNonQuery(command);

        //    Guid userId = (Guid)db.GetParameterValue(command, "userId");

        //    return userId;
        //}

        ///// <summary>
        ///// Changes the password question and answer
        ///// </summary>
        ///// <param name="applicationName">The application name</param>
        ///// <param name="userName">The user name</param>
        ///// <param name="newPasswordQuestion">The new password question</param>
        ///// <param name="newPasswordAnswer">The new answer to the password question</param>
        ///// <returns>A boolean indicating the result of the change</returns>
        //public bool ChangePasswordQuestionAndAnswer(string applicationName, string userName,
        //    string newPasswordQuestion, string newPasswordAnswer)
        //{
        //    Database db = DatabaseFactory.CreateDatabase(Properties.Settings.Default.SecurityDbKey);

        //    string sqlCommand = "Membership_ChangePasswordQuestionAndAnswer";
        //    DbCommand command = db.GetStoredProcCommand(sqlCommand);

        //    //input parameters
        //    db.AddInParameter(command, "applicationName", DbType.String, applicationName);
        //    db.AddInParameter(command, "userName", DbType.String, userName);
        //    db.AddInParameter(command, "newPasswordQuestion", DbType.String, newPasswordQuestion);
        //    db.AddInParameter(command, "newPasswordAnswer", DbType.String, newPasswordAnswer);

        //    int result = db.ExecuteNonQuery(command);

        //    return result == 0 ? true : false;
        //}

        //public void GetAllUsers()
        //{
        //    System.Web.Security.MembershipUser user = new System.Web.Security.MembershipUser();
        //    user.
        //}

        //#endregion
    }
}
