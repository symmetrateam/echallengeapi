﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Domain.Model;

namespace Diversity.Services.Business
{
    public interface IGameControllerService : IService
    {
        Session GetGameSession(Guid userId, int gameId, string lcid);
        IEnumerable<Language> GetLanguages();
        bool SavePlayerProfile(Guid userId, string firstname, string lastname);
        bool SaveScormPlayerProfile(Guid userId, string scormUserId, string firstname, string lastname, string email);
        bool SaveAvatarProfile(Guid userId, string avatarName, string avatarMetaData);
        Dashboard GetDashboard(Guid userId, string lcid);
        bool SaveCardResult(Guid userId, int cardSequenceId, PlayerResult result);
        bool SkipCard(Guid userId, int cardSequenceId);
        bool SetGameAsCompleted(Guid userId, int gameId);
        bool IsAPlayableGame(Guid userId, int gameId);
        IEnumerable<FlashAsset> GetFlashAssets(string lcid, string defaultLcid);
        bool IsGameCompleted(Guid userId, int gameId);
        bool ScormGameExists(string externalId);
        bool InviteUserToGame(Guid userId, int gameId, string themeCode);
        int? GetGameIdFromExternalId(string externalId);
        bool UserExistsInThemeClientUserGroup(Guid userId, string themeCode);
        bool AddUserToThemeClientUserGroup(Guid userId, string themeCode);
    }
}
