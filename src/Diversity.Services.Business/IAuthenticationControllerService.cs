﻿using System;
using System.Web.Security;
using Diversity.Domain.Model;

namespace Diversity.Services.Business
{
    public interface IAuthenticationControllerService
    {
        MembershipUser AuthenticateScormUser(string username, string clientCode, string hostAddress, out bool isNewUser);

        MembershipUser Authenticate(string username, string password, string ipAddress);

        bool ResetPassword(Guid userId, out string password);

        bool ChangePassword(Guid userId, string oldPassword, string newPassword);

        MembershipUser GetUser(string username);
        MembershipUser GetUser(Guid userId);
        AuthenticationTicket GetAuthenticationTicket(Guid userId);
        AuthenticationTicket GetAuthenticationTicket(string authToken);
        bool CreateAuthenticationTicket(AuthenticationTicket ticket);
        bool RenewAuthenticationTicket(AuthenticationTicket ticket);

        bool PassedMinimumPasswordRules(string password);
    }
}
