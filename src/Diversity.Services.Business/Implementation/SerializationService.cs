﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;

namespace Diversity.Services.Business.Implementation
{
    public class SerializationService : ISerializationService
    {
        #region Member Variables

        private static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string _assemblyName;
        private string _namespace;

        #endregion

        #region Constructors

        public SerializationService(string assemblyName, string typeNamespace)
        {
            _assemblyName = assemblyName;
            _namespace = typeNamespace;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        private string UTF8ByteArrayToString(byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            string constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// Converts the String to UTF8 Byte array and is used in De serialization
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        private byte[] StringToUTF8ByteArray(string xmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] byteArray = encoding.GetBytes(xmlString);
            return byteArray;
        }

        private Type CreateType(string typeName)
        {
            var assembly = Assembly.Load(_assemblyName);

            if (assembly == null)
            {
                throw new NullReferenceException(string.Format(@"Unable to load referenced assembly {0}", _assemblyName));
            }

            var type = assembly.GetType(string.Format(@"{0}.{1}", _namespace, typeName));

            if (type == null)
            {
                throw new NotImplementedException(string.Format("The type {0} has not been implemented as yet.", typeName));
            }

            return type;
        }

        private string GetName(XmlDocument document)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            if (document.DocumentElement == null)
            {
                throw new NullReferenceException("document.DocumentElement is null.");
            }

            return document.DocumentElement.Name;
        }

        private Type GetType(XmlDocument document)
        {
            return CreateType(GetName(document));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Convert an object to XML string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string ToXmlString(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(@"obj");
            }            

            try
            {
                var serializer = new XmlSerializer(obj.GetType());

                var encoding = new UTF8Encoding(false);

                var stream = new MemoryStream();
                var writer = new XmlTextWriter(stream, encoding);

                serializer.Serialize(writer, obj);
                writer.Flush();

                return encoding.GetString(stream.ToArray());
            }
            catch (SerializationException ex)
            {
                var e = new SerializationException("XmlSerializer encountered a serialization problem", ex);
                _log.Error(@"Serialization error.", e);
                throw e;
            }
        }
        /// <summary>
        /// Reconstruct an object from XML string
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public object FromXmlString(string xmlString)
        {
            if (xmlString == null)
            {
                throw new ArgumentNullException(@"xmlString");
            }

            if (xmlString.Length <= 0)
            {
                throw new ArgumentOutOfRangeException(@"xmlString");
            }

            var document = new XmlDocument();
            document.LoadXml(xmlString);
            return FromXmlDocument(document);
        }

        /// <summary>
        /// Reconstruct an object from XML document
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public object FromXmlDocument(XmlDocument document)
        {
            if (document == null)
            {
                throw new ArgumentNullException(@"document");
            }

            try
            {
                var serializer = new XmlSerializer(GetType(document));
                return serializer.Deserialize(new XmlTextReader(new StringReader(document.OuterXml)));
            }
            catch (SerializationException ex)
            {
                var e = new SerializationException("XmlSerializer encountered a de-serialization problem", ex);
                _log.Error("De-serialization error.", e);
                throw e;
            }
        }

        #endregion

    }
}
