﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Data.DataAccess;
using Diversity.Data.DataAccess.SqlServer;
using Diversity.Domain.Model;

namespace Diversity.Services.Business.Implementation
{
    public class ErrorMessageService : IErrorMessageService
    {
        private readonly IErrorMessageRepository _errorMessageRepository; 

        public ErrorMessageService()
        {
            _errorMessageRepository = new SqlErrorMessageRepository();            
        }
       
        public IEnumerable<ErrorMessage> GetErrorMessages(string lcid)
        {
            return _errorMessageRepository.GetErrorMessages().Where(c => c.Language.LCID.ToLower() == lcid.ToLower());
        }

        public IEnumerable<ErrorMessage> GetErrorMessages(Language language)
        {
            return GetErrorMessages(language.LCID);
        }

        public IEnumerable<ErrorMessage> GetErrorMessagesByCode(string errorCode)
        {
            return _errorMessageRepository.GetErrorMessages().Where(c => c.Code.ToLower() == errorCode.ToLower());
        }

        public ErrorMessage GetErrorMessage(string lcid, string errorCode)
        {
            return GetErrorMessages(lcid).Where(c => c.Code == errorCode).FirstOrDefault();
        }

        public ErrorMessage GetErrorMessage(Language language, string errorCode)
        {
            return GetErrorMessages(language).Where(c => c.Code == errorCode).FirstOrDefault();
        }

        public IEnumerable<ErrorMessage> GetErrorMessages()
        {
            return _errorMessageRepository.GetErrorMessages();
        }
    }
}
