﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Diversity.Common;
using Diversity.Data.DataAccess;
using Diversity.Data.DataAccess.SqlServer;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Services.Business.Implementation
{
    public class CardService : ICardService
    {
        public IGameRepository _gameRepository;

        public CardService()
        {
            _gameRepository = new SqlGameRepository();
        }
        /// <summary>
        /// Creates a duplicate card for a game 
        /// with assets associated to the card located in the temporary folders
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Card CreateDuplicateCard(int cardId, Guid userId)
        {
            return CreateDuplicateCard(cardId, userId, false);
        }

        /// <summary>
        /// Creates a duplicate card for a game with assets located in either temporary of live
        /// folders. 
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="userId"></param>
        ///<param name="storeAssetsInLiveFolder"></param>
        /// <returns></returns>
        public Card CreateDuplicateCard(int cardId, Guid userId, bool storeAssetsInLiveFolder)
        {
            var card = _gameRepository.DataContext.Cards.Where(c => c.CardID == cardId).FirstOrDefault();

            if (card == null)
            {
                return null;
            }

            var currentDateTime = DateTime.Now;
            Card duplicateCard = null;
            if (card.BranchingCard != null)
            {
                duplicateCard = DuplicateBranchingCard((BranchingCard) card, currentDateTime, userId,
                    storeAssetsInLiveFolder);

            }
            else
            {
                duplicateCard = new Card();
            }

            duplicateCard.CardContentNextRefreshDate = card.CardContentNextRefreshDate;
            duplicateCard.CardID_LCDCategoryCard = card.CardID_LCDCategoryCard;
            duplicateCard.CardMaxTimeToAnswerInSeconds = card.CardMaxTimeToAnswerInSeconds;
            duplicateCard.CardShortcutCode = card.CardShortcutCode;
            duplicateCard.CreatedOnDateTime = currentDateTime;
            duplicateCard.ClientID_ForLibrary = card.ClientID_ForLibrary;
            duplicateCard.DefaultLanguageID = card.DefaultLanguageID;
            duplicateCard.FacilitatorSlideNotes = card.FacilitatorSlideNotes;
            duplicateCard.IgnoreAnyTimersOnThisCard = card.IgnoreAnyTimersOnThisCard;
            duplicateCard.Internal_CardTypeID = card.Internal_CardTypeID;
            duplicateCard.IsApplicableForDesktopUse = card.IsApplicableForDesktopUse;
            duplicateCard.IsApplicableForOnlineUse = card.IsApplicableForOnlineUse;
            duplicateCard.IsATimedCard = card.IsATimedCard;
            duplicateCard.IsAwaitingRatification = card.IsAwaitingRatification;
            duplicateCard.IsDirtyCard_PreviouslyRatified = card.IsDirtyCard_PreviouslyRatified;
            duplicateCard.IsForClientlibraryElsePublicLibrary = card.IsForClientlibraryElsePublicLibrary;
            duplicateCard.IsRatified = card.IsRatified;
            duplicateCard.IsStillInBeta_DoNotMakeAvailableForUseInGames =
                card.IsStillInBeta_DoNotMakeAvailableForUseInGames;
            duplicateCard.LastModifiedOnDateTime = currentDateTime;
            duplicateCard.ProductID = card.ProductID;

            if (card.QuestionCard != null)
            {
                var questionCard = DuplicateQuestionCard(card.QuestionCard, currentDateTime, userId, storeAssetsInLiveFolder);
                duplicateCard.QuestionCard = questionCard;
            }

            duplicateCard.RatificationApprovedOn = card.RatificationApprovedOn;
            duplicateCard.RatificationApproverNotes = card.RatificationApproverNotes;
            duplicateCard.RatificationRequestedOn = card.RatificationRequestedOn;
            duplicateCard.RatificationRequestorNotes = card.RatificationRequestorNotes;
            duplicateCard.UseCardTimeNotGameTime = card.UseCardTimeNotGameTime;
            duplicateCard.UserID_CreatedBy = userId;
            duplicateCard.UserID_LastModifiedBy = userId;
            duplicateCard.UserID_RatificationApprover = card.UserID_RatificationApprover;
            duplicateCard.UserID_RatificationRequestor = card.UserID_RatificationRequestor;
            duplicateCard.AudioTranscriptFile = card.AudioTranscriptFile;
            duplicateCard.AudioTranscriptFriendlyName = card.AudioTranscriptFriendlyName;
            duplicateCard.AudioTranscriptText = card.AudioTranscriptText;


            var cardLanguages = card.Card_Languages.Select(l => DuplicateCardLanguage(l, currentDateTime, userId));

            foreach (var cardLanguage in cardLanguages)
            {
                duplicateCard.Card_Languages.Add(cardLanguage);
            }

            var associateCards = card.AssociatedCards.Select(a => DuplicateAssociatedCard(a, currentDateTime, userId));

            foreach (var associatedCard in associateCards)
            {
                duplicateCard.AssociatedCards.Add(associatedCard);
            }

            var card2CardClass =
                card.Card2CardClassifications.Select(c => DuplicateCard2CardClassification(c, currentDateTime, userId));

            foreach (var card2CardClassification in card2CardClass)
            {
                duplicateCard.Card2CardClassifications.Add(card2CardClassification);
            }

            var referenceMaterials =
                card.CardReferenceMaterials.Select(
                    r => DuplicateCardReferenceMaterial(r, currentDateTime, userId, storeAssetsInLiveFolder));

            foreach (var cardReferenceMaterial in referenceMaterials)
            {
                duplicateCard.CardReferenceMaterials.Add(cardReferenceMaterial);
            }
                        
            return duplicateCard;
        }

        CardReferenceMaterial DuplicateCardReferenceMaterial(CardReferenceMaterial referenceMaterial, DateTime currentDateTime, Guid userId, bool storeAssetsInLiveFolder)
        {
            if (referenceMaterial == null)
            {
                return null;
            }

            var cardReferenceMaterial = new CardReferenceMaterial();
            cardReferenceMaterial.CreatedOnDateTime = currentDateTime;
            cardReferenceMaterial.DefaultLanguageID = referenceMaterial.DefaultLanguageID;
            cardReferenceMaterial.LastModifiedOnDateTime = currentDateTime;
            cardReferenceMaterial.UserID_CreatedBy = userId;
            cardReferenceMaterial.UserID_LastModifiedBy = userId;

            var cardReferenceMaterialLanguages =
                referenceMaterial.CardReferenceMaterial_Languages.Select(
                    r => DuplicateCardReferenceMaterialLanguage(r, currentDateTime, userId, storeAssetsInLiveFolder));

            foreach (var cardReferenceMaterialLanguage in cardReferenceMaterialLanguages)
            {
                cardReferenceMaterial.CardReferenceMaterial_Languages.Add(cardReferenceMaterialLanguage);
            }

            

            return cardReferenceMaterial;
        }

        CardReferenceMaterial_Language DuplicateCardReferenceMaterialLanguage(CardReferenceMaterial_Language referenceMaterialLanguage,
            DateTime currentDateTime, Guid userId, bool storeAssetInLiveFolder)
        {
            if (referenceMaterialLanguage == null)
            {
                return null;
            }

            var cardReferenceMaterialLanguage = new CardReferenceMaterial_Language();
            cardReferenceMaterialLanguage.CardReferenceMaterialDescription =
                referenceMaterialLanguage.CardReferenceMaterialDescription;

            var parts = referenceMaterialLanguage.CardReferenceMaterialUrl.Split(new[] { "." },
                                                                                    StringSplitOptions.
                                                                                        RemoveEmptyEntries);

            var filename = string.Format(@"{0}.{1}", Guid.NewGuid(), parts[parts.Length - 1]);

            var destinationPath = string.Empty;
            if (storeAssetInLiveFolder)
            {
                destinationPath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation), filename);
            }
            else
            {
                destinationPath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
            }

            var sourcePath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                                               referenceMaterialLanguage.CardReferenceMaterialUrl);

            if (filename.Length > 0 && File.Exists(sourcePath))
            {
                File.Copy(sourcePath, destinationPath);
            }

            cardReferenceMaterialLanguage.CardReferenceMaterialUrl = filename;
            cardReferenceMaterialLanguage.CreatedOnDateTime = currentDateTime;
            cardReferenceMaterialLanguage.LanguageID = referenceMaterialLanguage.LanguageID;
            cardReferenceMaterialLanguage.LastModifiedOnDateTime = currentDateTime;
            cardReferenceMaterialLanguage.UserID_CreatedBy = userId;
            cardReferenceMaterialLanguage.UserID_LastModifiedBy = userId;

            return cardReferenceMaterialLanguage;
        }

        Card2CardClassification DuplicateCard2CardClassification(Card2CardClassification classification, DateTime currentDateTime, Guid userId)
        {
            if (classification == null)
            {
                return null;
            }

            var card2CardClassification = new Card2CardClassification();
            card2CardClassification.CardClassificationGroupID = classification.CardClassificationGroupID;
            card2CardClassification.CreatedOnDateTime = currentDateTime;
            card2CardClassification.IsAPrimaryCard = classification.IsAPrimaryCard;
            card2CardClassification.LastModifiedOnDateTime = currentDateTime;
            card2CardClassification.UserID_CreateBy = userId;
            card2CardClassification.UserID_LastModifiedBy = userId;

            return card2CardClassification;
        }

        AssociatedCard DuplicateAssociatedCard(AssociatedCard card, DateTime currentDateTime, Guid userId)
        {
            if (card == null)
            {
                return null;
            }

            var associatedCard = new AssociatedCard();
            associatedCard.CardID = card.CardID;
            associatedCard.CardID_AssociatedCard = card.CardID_AssociatedCard;
            associatedCard.CreatedOnDateTime = currentDateTime;
            associatedCard.ExplanationForAssociation = card.ExplanationForAssociation;
            associatedCard.IsAssociatedToElseContextuallySimilarTo = card.IsAssociatedToElseContextuallySimilarTo;
            associatedCard.IsPrimaryAssociation = card.IsPrimaryAssociation;
            associatedCard.LastModifiedOnDateTime = currentDateTime;
            associatedCard.UserID_CreatedBy = userId;
            associatedCard.UserID_LastModifiedBy = userId;

            return associatedCard;
        }

        Card_Language DuplicateCardLanguage(Card_Language card, DateTime currentDateTime, Guid userId)
        {
            if (card == null)
            {
                return null;
            }

            var cardLanguage = new Card_Language();
            cardLanguage.CardName = card.CardName;
            cardLanguage.CardTitle = card.CardTitle;
            cardLanguage.CreatedOnDateTime = currentDateTime;
            cardLanguage.DetailedCardDescription = card.DetailedCardDescription;
            cardLanguage.FacilitatorSlideNotes = card.FacilitatorSlideNotes;
            cardLanguage.LanguageID = card.LanguageID;
            cardLanguage.LastModifiedOnDateTime = currentDateTime;
            cardLanguage.UserID_CreatedBy = userId;
            cardLanguage.UserID_LastModifiedBy = userId;

            return cardLanguage;
        }

        BranchingCard DuplicateBranchingCard(BranchingCard card, DateTime currentDateTime, Guid userId, bool storeAssetInLiveFolder)
        {
            if (card == null)
            {
                return null;
            }

            var branchingCard = new BranchingCard();

            branchingCard.MustPressButtonsInSequence = card.MustPressButtonsInSequence;
            branchingCard.MustAddBackgroundBorderToBranches = card.MustAddBackgroundBorderToBranches;
            branchingCard.MinButtonPresses = card.MinButtonPresses;
            branchingCard.ButtonOrientation = card.ButtonOrientation;
            branchingCard.CardGroupID_BranchingCompleted = card.CardGroupID_BranchingCompleted;
            branchingCard.GameID = card.GameID;
            branchingCard.UserID_CreatedBy = userId;
            branchingCard.UserID_LastModifiedBy = userId;
            branchingCard.LastModifiedOnDateTime = currentDateTime;

            var branchingCardButtons =
                card.BranchingCard_Buttons.Select(b => DuplicateBranchingCardButton(b, currentDateTime, userId, storeAssetInLiveFolder));

            foreach (var branchingCardButton in branchingCardButtons)
            {
                branchingCard.BranchingCard_Buttons.Add(branchingCardButton);
            }

            return branchingCard;
        }

        BranchingCard_Button DuplicateBranchingCardButton(BranchingCard_Button buttonCard, DateTime currentDateTime, Guid userId, bool storeAssetInLiveFolder)
        {
            if (buttonCard == null)
            {
                return null;
            }

            var branchingCardButton = new BranchingCard_Button();
            branchingCardButton.Position = buttonCard.Position;
            branchingCardButton.CardGroupID_Result = buttonCard.CardGroupID_Result;
            branchingCardButton.IsMultiClick = buttonCard.IsMultiClick;
            branchingCardButton.MustReturnToBranchingCard = buttonCard.MustReturnToBranchingCard;

            branchingCardButton.UserID_CreatedBy = userId;
            branchingCardButton.UserID_LastModifiedBy = userId;
            branchingCardButton.LastModifiedOnDateTime = currentDateTime;

            var buttonCardLanguages =
                buttonCard.BranchingCard_ButtonLanguage.Select(
                    l => DuplicateBranchingCardButtonLanguage(l, currentDateTime, userId, true));

            foreach (var branchingCardButtonLanguage in buttonCardLanguages)
            {
                branchingCardButton.BranchingCard_ButtonLanguage.Add(branchingCardButtonLanguage);
            }

            return branchingCardButton;
        }

        private BranchingCard_ButtonLanguage DuplicateBranchingCardButtonLanguage(BranchingCard_ButtonLanguage buttonCardLanguage,
            DateTime currentDateTime, Guid userId, bool storeAssetInLiveFolder)
        {
            if (buttonCardLanguage == null)
            {
                return null;
            }

            var buttonLanguage = new BranchingCard_ButtonLanguage();

            buttonLanguage.UserID_CreatedBy = userId;
            buttonLanguage.UserID_LastModifiedBy = userId;
            buttonLanguage.LastModifiedOnDateTime = currentDateTime;
            
            if (!string.IsNullOrEmpty(buttonCardLanguage.ButtonImageFriendlyName))
            {
                var assocParts = buttonCardLanguage.ButtonImageFriendlyName.Split(new[] { "." },
                                                                           StringSplitOptions.
                                                                               RemoveEmptyEntries);

                var associatedImageUrlFileName = string.Format(@"{0}.{1}", Guid.NewGuid(),
                                                               assocParts[assocParts.Length - 1]);

                var assocImageUrlDestinationPath = string.Empty;

                if (storeAssetInLiveFolder)
                {
                    assocImageUrlDestinationPath = Path.Combine(
                        HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                        associatedImageUrlFileName);
                }
                else
                {
                    assocImageUrlDestinationPath = Path.Combine(
                        HttpContext.Current.Server.MapPath(ConfigurationHelper.TemporaryUploadLocation),
                        associatedImageUrlFileName);
                }

                var assocImageUrlSourcePath = Path.Combine(
                    HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                    buttonCardLanguage.ButtonImageFriendlyName);

                if (associatedImageUrlFileName.Length > 0 && File.Exists(assocImageUrlSourcePath))
                {
                    File.Copy(assocImageUrlSourcePath, assocImageUrlDestinationPath);
                    buttonLanguage.ButtonImageMultimediaURL = associatedImageUrlFileName;
                }
            }

            buttonLanguage.ButtonImageFriendlyName = buttonCardLanguage.ButtonImageFriendlyName;
            buttonLanguage.ButtonText = buttonCardLanguage.ButtonText;
            buttonLanguage.LanguageID = buttonCardLanguage.LanguageID;

            return buttonLanguage;
        }

        QuestionCard DuplicateQuestionCard(QuestionCard card, DateTime currentDateTime, Guid userId, bool storeAssetInLiveFolder)
        {
            if (card == null)
            {
                return null;
            }

            var questionCard = new QuestionCard();

            questionCard.AssociatedImageUrl = card.AssociatedImageUrl;
            if (!string.IsNullOrEmpty(questionCard.AssociatedImageUrl))
            {
                var assocParts = card.AssociatedImageUrl.Split(new[] { "." },
                                                                           StringSplitOptions.
                                                                               RemoveEmptyEntries);

                var associatedImageUrlFileName = string.Format(@"{0}.{1}", Guid.NewGuid(),
                                                               assocParts[assocParts.Length - 1]);

                var assocImageUrlDestinationPath = string.Empty;

                if (storeAssetInLiveFolder)
                {
                    assocImageUrlDestinationPath = Path.Combine(
                        HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                        associatedImageUrlFileName);
                }
                else
                {
                    assocImageUrlDestinationPath = Path.Combine(
                        HttpContext.Current.Server.MapPath(ConfigurationHelper.TemporaryUploadLocation),
                        associatedImageUrlFileName);
                }

                var assocImageUrlSourcePath = Path.Combine(
                    HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                    card.AssociatedImageUrl);

                if (associatedImageUrlFileName.Length > 0 && File.Exists(assocImageUrlSourcePath))
                {
                    File.Copy(assocImageUrlSourcePath, assocImageUrlDestinationPath);
                    questionCard.AssociatedImageUrl = associatedImageUrlFileName;
                }
            }

            questionCard.AssociatedImageUrlFriendlyName = card.AssociatedImageUrlFriendlyName;


            questionCard.AnswerExplanationMultimediaType_ImageVideoetc =
                card.AnswerExplanationMultimediaType_ImageVideoetc;

            var answerMultimediaType = card.AnswerExplanationMultimediaType_ImageVideoetc == null ? string.Empty : card.AnswerExplanationMultimediaType_ImageVideoetc.ToLower();
            switch (answerMultimediaType)
            {
                case "wistia":
                    questionCard.AnswerExplanationMultimediaURL = card.AnswerExplanationMultimediaURL;
                    break;

                case "document":
                case "image":

                    var answerParts = card.AnswerExplanationMultimediaURL.Split(new[] { "." },
                                                                        StringSplitOptions.
                                                                            RemoveEmptyEntries);

                    var answerFileName = string.Format(@"{0}.{1}", Guid.NewGuid(), answerParts[answerParts.Length - 1]);

                    var destinationPath = string.Empty;

                    if (storeAssetInLiveFolder)
                    {
                        destinationPath = Path.Combine(
                            HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                            answerFileName);
                    }
                    else
                    {

                        destinationPath =  Path.Combine(
                            HttpContext.Current.Server.MapPath(ConfigurationHelper.TemporaryUploadLocation),
                            answerFileName);
                    }

                    var sourcePath = Path.Combine(
                        HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                        card.AnswerExplanationMultimediaURL);

                    if (answerFileName.Length > 0 && File.Exists(sourcePath))
                    {
                        File.Copy(sourcePath, destinationPath);
                        questionCard.AnswerExplanationMultimediaURL = answerFileName;
                    }

                    break;
            }

            questionCard.AnswerExplanationMultimediaURLFriendlyName = card.AnswerExplanationMultimediaURLFriendlyName;
            questionCard.AnswerIsCaseSensitive = card.AnswerIsCaseSensitive;
            questionCard.AnswerMultimediaShowPlaybackControls = card.AnswerMultimediaShowPlaybackControls;
            questionCard.AnswerMultimediaStartAutoElsePushPlayToStart =
                card.AnswerMultimediaStartAutoElsePushPlayToStart;
            questionCard.ApplyNegScoringForEachInCorrectAnswer = card.ApplyNegScoringForEachInCorrectAnswer;
            questionCard.ApplyScoreByCardElseForEachCorrectAnswer = card.ApplyScoreByCardElseForEachCorrectAnswer;            
            questionCard.CreatedOnDateTime = currentDateTime;
            questionCard.DesktopCurrentLosingScore = card.DesktopCurrentLosingScore;
            questionCard.DesktopCurrentWinningScore = card.DesktopCurrentWinningScore;
            questionCard.DesktopNonCurrentLosingScore = card.DesktopNonCurrentLosingScore;
            questionCard.DesktopNonCurrentWinningScore = card.DesktopNonCurrentWinningScore;
            questionCard.LastModifiedOnDateTime = currentDateTime;
            questionCard.OfferOptOutChoice = card.OfferOptOutChoice;
            questionCard.OfferOTHERTextOption = card.OfferOTHERTextOption;
            questionCard.OnlineLosingScore = card.OnlineLosingScore;
            questionCard.OnlineWinningScore = card.OnlineWinningScore;
            questionCard.QuestionIsOnlyCorrectIfAllAnswersAreTrue = card.QuestionIsOnlyCorrectIfAllAnswersAreTrue;
            questionCard.QuestionMultimediaShowPlaybackControls = card.QuestionMultimediaShowPlaybackControls;
            questionCard.QuestionMultimediaStartAutoElsePushPlayToStart =
                card.QuestionMultimediaStartAutoElsePushPlayToStart;
            questionCard.QuestionMultimediaType_ImageVideoetc = card.QuestionMultimediaType_ImageVideoetc;


            var questionMultimediaType = card.QuestionMultimediaType_ImageVideoetc == null ? string.Empty : card.QuestionMultimediaType_ImageVideoetc.ToLower();
            switch (questionMultimediaType)
            {
                case "wistia":
                    questionCard.QuestionMultimediaURL = card.QuestionMultimediaURL;
                    break;

                case "document":
                case "image":

                    var parts = card.QuestionMultimediaURL.Split(new[] { "." },
                                                                        StringSplitOptions.
                                                                            RemoveEmptyEntries);

                    var filename = string.Format(@"{0}.{1}", Guid.NewGuid(), parts[parts.Length - 1]);

                    var destinationPath = string.Empty;

                    if (storeAssetInLiveFolder)
                    {
                        destinationPath = Path.Combine(
                              HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                              filename);
                    }
                    else
                    {
                        destinationPath = Path.Combine(
                            HttpContext.Current.Server.MapPath(ConfigurationHelper.TemporaryUploadLocation),
                            filename);

                    }

                    var sourcePath = Path.Combine(
                        HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                        card.QuestionMultimediaURL);

                    if (filename.Length > 0 && File.Exists(sourcePath))
                    {
                        File.Copy(sourcePath, destinationPath);
                        questionCard.QuestionMultimediaURL = filename;
                    }
                    break;
            }

            questionCard.QuestionMultimediaURLFriendlyName = card.QuestionMultimediaURLFriendlyName;
            questionCard.TextOptionInputIsTextArea = card.TextOptionInputIsTextArea;
            questionCard.UsePrimaryTopics = card.UsePrimaryTopics;
            questionCard.TopNSelected = card.TopNSelected;
            questionCard.MustShowCompanyResults = card.MustShowCompanyResults;
            questionCard.MinDataPointsForCompanyResult = card.MinDataPointsForCompanyResult;
            questionCard.UserID_CreatedBy = userId;
            questionCard.UserID_LastModifiedBy = userId;

            questionCard.QuestionMultimediaThumbnailURL = card.QuestionMultimediaThumbnailURL;
            questionCard.QuestionMultimediaThumbnailURLFriendlyName = card.QuestionMultimediaThumbnailURLFriendlyName;
            questionCard.AnswerExplanationMultimediaThumbnailURL = card.AnswerExplanationMultimediaThumbnailURL;
            questionCard.AnswerExplanationMultimediaThumbnailURLFriendlyName =
                card.AnswerExplanationMultimediaThumbnailURLFriendlyName;
                

            var questionCardLanguages =
                card.QuestionCard_Languages.Select(c => DuplicateQuestionCardLanguage(c, currentDateTime, userId));

            foreach (var questionCardLanguage in questionCardLanguages)
            {
                questionCard.QuestionCard_Languages.Add(questionCardLanguage);
            }

            var questionCardStatementLanguage = card.QuestionCardStatement_Languages.Select(
                c => DuplicateQuestionCardStatementLanguage(c, currentDateTime, userId));


            foreach (var cardStatementLanguage in questionCardStatementLanguage)
            {
                questionCard.QuestionCardStatement_Languages.Add(cardStatementLanguage);
            }

            var generalAnswers =
                card.QuestionCard_GeneralAnswers.Select(
                    a => DuplicateQuestionCardGeneralAnswer(a, currentDateTime, userId, storeAssetInLiveFolder));

            foreach (var questionCardGeneralAnswer in generalAnswers)
            {
                questionCard.QuestionCard_GeneralAnswers.Add(questionCardGeneralAnswer);
            }

            return questionCard;
        }

        QuestionCard_Language DuplicateQuestionCardLanguage(QuestionCard_Language cardLanguage, DateTime currentDateTime, Guid userId)
        {
            if (cardLanguage == null)
            {
                return null;
            }

            var questionCardLanguage = new QuestionCard_Language();
            questionCardLanguage.AnswerExplanation_LongVersion = cardLanguage.AnswerExplanation_LongVersion;
            questionCardLanguage.AnswerExplanation_ShortVersion = cardLanguage.AnswerExplanation_ShortVersion;
            questionCardLanguage.CreatedOnDateTime = currentDateTime;
            questionCardLanguage.LanguageID = cardLanguage.LanguageID;
            questionCardLanguage.LastModifiedOnDateTime = currentDateTime;
            questionCardLanguage.Question_Statement_LongVersion = cardLanguage.Question_Statement_LongVersion;
            questionCardLanguage.Question_Statement_ShortVersion = cardLanguage.Question_Statement_ShortVersion;
            questionCardLanguage.StatementHeader = cardLanguage.StatementHeader;
            questionCardLanguage.UserID_CreatedBy = userId;
            questionCardLanguage.UserID_LastModifiedBy = userId;
            questionCardLanguage.StatementLocation = cardLanguage.StatementLocation;

            return questionCardLanguage;
        }

        QuestionCardStatement_Language DuplicateQuestionCardStatementLanguage(QuestionCardStatement_Language statementLanguage, DateTime currentDateTime, Guid userId)
        {
            if(statementLanguage == null)
            {
                return null;
            }

            var statementCardLanguage = new QuestionCardStatement_Language();
            statementCardLanguage.UserID_LastModifiedBy = userId;
            statementCardLanguage.UserID_CreatedBy = userId;
            statementCardLanguage.CreatedOnDateTime = currentDateTime;
            statementCardLanguage.LastModifiedOnDateTime = currentDateTime;
            statementCardLanguage.LanguageID = statementLanguage.LanguageID;
            statementCardLanguage.Statement_LongVersion = statementLanguage.Statement_LongVersion;
            statementCardLanguage.Statement_ShortVersion = statementLanguage.Statement_ShortVersion;
            statementCardLanguage.SequenceNo = statementLanguage.SequenceNo;

            return statementCardLanguage;
        }

        QuestionCard_GeneralAnswer DuplicateQuestionCardGeneralAnswer(QuestionCard_GeneralAnswer cardGeneralAnswer, DateTime currentDateTime, Guid userId, bool storeAssetsInLiveFolder)
        {
            if (cardGeneralAnswer == null)
            {
                return null;
            }

            var questionCardGeneralAnswer = new QuestionCard_GeneralAnswer();
            questionCardGeneralAnswer.CreatedOnDateTime = currentDateTime;
            questionCardGeneralAnswer.IsBetween = cardGeneralAnswer.IsBetween;
            questionCardGeneralAnswer.IsCorrectAnswer = cardGeneralAnswer.IsCorrectAnswer;
            questionCardGeneralAnswer.IsEqualTo = cardGeneralAnswer.IsEqualTo;
            questionCardGeneralAnswer.IsGreaterThan = cardGeneralAnswer.IsGreaterThan;
            questionCardGeneralAnswer.IsGreaterThanOrEqualTo = cardGeneralAnswer.IsGreaterThanOrEqualTo;
            questionCardGeneralAnswer.IsLessThan = cardGeneralAnswer.IsLessThan;
            questionCardGeneralAnswer.IsLessThanOrEqualTo = cardGeneralAnswer.IsLessThanOrEqualTo;
            questionCardGeneralAnswer.IsNotBetween = cardGeneralAnswer.IsNotBetween;
            questionCardGeneralAnswer.IsNotEqualTo = cardGeneralAnswer.IsNotEqualTo;
            questionCardGeneralAnswer.LastModifiedOnDateTime = currentDateTime;
            questionCardGeneralAnswer.SequenceNo = cardGeneralAnswer.SequenceNo;
            questionCardGeneralAnswer.UserID_CreatedBy = userId;
            questionCardGeneralAnswer.UserID_LastModifiedBy = userId;


            var generalAnswerLanguage =
                cardGeneralAnswer.QuestionCard_GeneralAnswer_Languages.Select(
                    l => DuplicateQuestionCardGeneralAnswerLanguage(l, currentDateTime, userId, storeAssetsInLiveFolder));

            foreach (var questionCardGeneralAnswerLanguage in generalAnswerLanguage)
            {
                questionCardGeneralAnswer.QuestionCard_GeneralAnswer_Languages.Add(questionCardGeneralAnswerLanguage);
            }
            
            return questionCardGeneralAnswer;
        }

        QuestionCard_GeneralAnswer_Language DuplicateQuestionCardGeneralAnswerLanguage(QuestionCard_GeneralAnswer_Language cardGeneralAnswerLanguage,
            DateTime currentDateTime, Guid userId, bool storeAssetsInLiveFolder)
        {
            if (cardGeneralAnswerLanguage == null)
            {
                return null;
            }

            var questionCardGeneralAnswerLanguage = new QuestionCard_GeneralAnswer_Language();
            questionCardGeneralAnswerLanguage.CreatedOnDateTime = currentDateTime;
            questionCardGeneralAnswerLanguage.LanguageID = cardGeneralAnswerLanguage.LanguageID;
            questionCardGeneralAnswerLanguage.LastModifiedOnDateTime = currentDateTime;
            questionCardGeneralAnswerLanguage.LHStatement_LongVersion =
                cardGeneralAnswerLanguage.LHStatement_LongVersion;
            questionCardGeneralAnswerLanguage.LHStatement_ShortVersion =
                cardGeneralAnswerLanguage.LHStatement_ShortVersion;
            questionCardGeneralAnswerLanguage.PossibleAnswer_LongVersion =
                cardGeneralAnswerLanguage.PossibleAnswer_LongVersion;
            questionCardGeneralAnswerLanguage.PossibleAnswer_ShortVersion =
                cardGeneralAnswerLanguage.PossibleAnswer_ShortVersion;
            questionCardGeneralAnswerLanguage.RHStatement_LongVersion =
                cardGeneralAnswerLanguage.RHStatement_LongVersion;
            questionCardGeneralAnswerLanguage.RHStatement_ShortVersion =
                cardGeneralAnswerLanguage.RHStatement_ShortVersion;
            questionCardGeneralAnswerLanguage.UserID_CreatedBy = userId;
            questionCardGeneralAnswerLanguage.UserID_LastModifiedBy = userId;

            if(string.IsNullOrEmpty(cardGeneralAnswerLanguage.PossibleAnswerMultimediaURL) || 
                string.IsNullOrWhiteSpace(cardGeneralAnswerLanguage.PossibleAnswerMultimediaURL))
            {
                return questionCardGeneralAnswerLanguage;    
            }

            var parts = cardGeneralAnswerLanguage.PossibleAnswerMultimediaURL.Split(new[] { "." },
                                                                        StringSplitOptions.
                                                                            RemoveEmptyEntries);

            var filename = string.Format(@"{0}.{1}", Guid.NewGuid(), parts[parts.Length - 1]);

            var destinationPath = string.Empty;

            if (storeAssetsInLiveFolder)
            {
                destinationPath = Path.Combine(
                      HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                      filename);
            }
            else
            {
                destinationPath = Path.Combine(
                    HttpContext.Current.Server.MapPath(ConfigurationHelper.TemporaryUploadLocation),
                    filename);

            }

            var sourcePath = Path.Combine(
                HttpContext.Current.Server.MapPath(ConfigurationHelper.LiveUploadLocation),
                cardGeneralAnswerLanguage.PossibleAnswerMultimediaURL);

            if (filename.Length > 0 && File.Exists(sourcePath))
            {
                File.Copy(sourcePath, destinationPath);
                questionCardGeneralAnswerLanguage.PossibleAnswerMultimediaURL = filename;
                questionCardGeneralAnswerLanguage.PossibleAnswerMultimediaURLFriendlyName =
                    cardGeneralAnswerLanguage.PossibleAnswerMultimediaURLFriendlyName;
            }


            return questionCardGeneralAnswerLanguage;
        }
    }
}
