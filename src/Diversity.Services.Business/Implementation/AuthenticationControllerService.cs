﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Authentication;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Security;
using BlueTorque.Net.Security.Web;
using Diversity.Common;
using Diversity.Common.Exceptions;
using Diversity.Data.DataAccess;
using Diversity.Data.DataAccess.SqlServer;
using Diversity.Domain.Model;

namespace Diversity.Services.Business.Implementation
{
    public class AuthenticationControllerService : IAuthenticationControllerService
    {

        private IAuthenticationRepository _authenticationRepository;

        public AuthenticationControllerService()
        {
            _authenticationRepository = new AuthenticationRepository();            
        }

        public MembershipUser AuthenticateScormUser(string username, string clientCode, string hostAddress, out bool isNewUser)
        {
            if (!_authenticationRepository.ClientExists(clientCode))
            {
                throw new InvalidClientException(Messages.InvalidClientCodeMessage);
            }

            var userId = _authenticationRepository.GetScormUserId(username, clientCode);

            var password = string.Empty;
            MembershipUser membershipUser = null;
            if (userId == Guid.Empty)
            {
                var emailAddress = string.Format(@"{0}@{1}.com", username.ToLower(), clientCode.ToLower());

                var passwordValid = false;                
                while (!passwordValid)
                {
                    password = Membership.GeneratePassword(10, Membership.MinRequiredNonAlphanumericCharacters);
                    passwordValid = Regex.IsMatch(password, Membership.PasswordStrengthRegularExpression);
                }
               
                membershipUser = Membership.CreateUser(emailAddress, password, emailAddress);
                
                userId = (Guid) membershipUser.ProviderUserKey;
                
                _authenticationRepository.AddUserToAllUsersClientUserGroup(userId, clientCode);
                _authenticationRepository.AddUserToClient(userId, clientCode);

                isNewUser = true;                
            }
            else
            {
                isNewUser = false;
            }

            if (membershipUser == null)
            {
                membershipUser = GetUser(userId);
            }

            if (membershipUser == null)
            {
                return null;
            }

            if (!membershipUser.IsApproved)
            {
                membershipUser.IsApproved = true;
                Membership.UpdateUser(membershipUser);
            }

            if (membershipUser.IsLockedOut)
            {
                membershipUser.UnlockUser();
            }

            userId = (Guid) membershipUser.ProviderUserKey;            

            ResetPassword(userId, out password);

            return Authenticate(membershipUser.UserName, password, hostAddress);

        }

        public MembershipUser Authenticate(string username, string password, string hostAddress)
        {         
            var membershipUser = AuthenticationHelper.ValidateUser(username, password, hostAddress);            
            return membershipUser;
        }

        public bool PassedMinimumPasswordRules(string password)
        {
            return Regex.IsMatch(password, Membership.PasswordStrengthRegularExpression) && password.Length >= Membership.MinRequiredPasswordLength;
        }

        public MembershipUser GetUser(string username)
        {
            return Membership.GetUser(username);
        }

        public MembershipUser GetUser(Guid userId)
        {
            return Membership.GetUser(userId);
        }

        public bool ResetPassword(Guid userId, out string password)
        {
            var membershipUser = Membership.GetUser(userId);
            var passwordValid = false;
            password = string.Empty;

            while (!passwordValid)
            {
                password = membershipUser.ResetPassword();                
                passwordValid = Regex.IsMatch(password, Membership.PasswordStrengthRegularExpression);
            }

            return password.Length > 0;
        }

        public bool ChangePassword(Guid userId, string oldPassword, string newPassword)
        {
            var memberShipUser = Membership.GetUser(userId);
                        
            if(memberShipUser == null)
            {
                return false;
            }
            
            return memberShipUser.ChangePassword(oldPassword, newPassword);
        }


        public AuthenticationTicket GetAuthenticationTicket(Guid userId)
        {
            return _authenticationRepository.GetAuthenticationTicket(userId);
        }

        public AuthenticationTicket GetAuthenticationTicket(string authToken)
        {
            return _authenticationRepository.GetAuthenticationTicket(authToken);
        }

        public bool CreateAuthenticationTicket(AuthenticationTicket ticket)
        {
            return _authenticationRepository.CreateAuthenticationTicket(ticket);
        }

        public bool RenewAuthenticationTicket(AuthenticationTicket ticket)
        {
            return _authenticationRepository.UpdateAuthenticationTicket(ticket);
        }
    }
}
