﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.Interception;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using Diversity.Common.Enumerations;
using Diversity.Common.Exceptions;
using Diversity.DataModel.SqlRepository;
using Diversity.Domain.Model;
using Diversity.Data.DataAccess;
using Diversity.Data.DataAccess.SqlServer;
using FlashAsset = Diversity.Domain.Model.FlashAsset;
using Game = Diversity.Domain.Model.Game;
using Language = Diversity.Domain.Model.Language;
using Session = Diversity.Domain.Model.Session;

namespace Diversity.Services.Business.Implementation
{
    public class GameControllerService : IGameControllerService
    {
        private readonly ILanguageRepository _languageRepository;
        private readonly IGameRepository _gameRepository;

        public GameControllerService()
        {
            _languageRepository = new SqlLanguageRepository();
            _gameRepository = new SqlGameRepository();
        }


        #region Public Laguage Methods

        public IEnumerable<Language> GetLanguages()
        {
            return _languageRepository.GetLanguages();
        }

        #endregion

        #region Public Flash Asset Methods

        public IEnumerable<FlashAsset> GetFlashAssets(string lcid, string defaultLcid)
        {
            var language = GetLanguage(lcid);                                   
            var defaultLanguage = GetLanguage(defaultLcid);

            if(language == null)
            {
                return GetFlashAssets(defaultLanguage);
            }

            if(language.Id == defaultLanguage.Id)
            {
                return GetFlashAssets(defaultLanguage);
            }

            var defaultLanguageAssets = GetFlashAssets(defaultLanguage);
            var mainLanguageAssets = GetFlashAssets(language).ToList();

            var mainLanguageAssetKeys = mainLanguageAssets.Select(a => a.Name);

            var assetsExcludedFromMainLanguage = defaultLanguageAssets.Where(a => !mainLanguageAssetKeys.Contains(a.Name));

            return mainLanguageAssets.Union(assetsExcludedFromMainLanguage);
        }

        IEnumerable<FlashAsset> GetFlashAssets(Language language)
        {
            return _gameRepository.GetFlashAssets(language.Id);
        }

        #endregion

        #region Public Game Methods

        public Session GetGameSession(Guid userId, int gameId, string lcid)
        {
            var session = new Session()
                              {
                                  Id = Guid.NewGuid()
                              };

            var language = GetLanguage(lcid);

            if (language == null)
            {
                return session;
            }

            Game game = null;

            if (_gameRepository.DataContext.Games.Any(g => g.GameID == gameId))
            {
                var gameEntry = _gameRepository.DataContext.Games.FirstOrDefault(g => g.GameID == gameId);
                var gameSettings = _gameRepository.GetGameSettings(gameEntry);

                if (gameSettings.IsTemplate)
                {
                    var inProgressGameId = _gameRepository.GetGameIdForInProgressGame(gameId, userId);

                    if(inProgressGameId.HasValue)
                    {
                        game = _gameRepository.GetGame(inProgressGameId.Value, language.Id, userId);
                        _gameRepository.IncrementGameRunCount(inProgressGameId.Value, userId);
                    }
                    else
                    {
                        if(_gameRepository.CanNewGameBePlayedByUser(gameId, language.Id, userId))
                        {
                            game = _gameRepository.GetGame(gameId, language.Id, userId);
                            game = CreateGameFromTemplate(userId, game, language.Id, true);    
                        }
                        else
                        {
                            throw new MaximumGamesPlayedException(Messages.GamePlayedMaximumNumberOfTimes);
                        }
                    }                    
                }
                else
                {
                    game = _gameRepository.GetGame(gameId, language.Id, userId);
                    _gameRepository.IncrementGameRunCount(gameId, userId);
                }
            }

            session.Game = game;


            return session;
        }

        Game CreateGameFromTemplate(Guid userId, Game game, int languageId, bool lockDownGameTemplate)
        {            
            return game.GameSettings.IsTemplate ? _gameRepository.CreateGameFromTemplateGame(game, userId, languageId, lockDownGameTemplate) : game;
        }


        public bool SaveCardResult(Guid userId, int cardSequenceId, PlayerResult result)
        {
            var xml = string.Empty;

            var cardSequence =
               _gameRepository.DataContext.GameCardSequences.FirstOrDefault(c => c.GameCardSequenceID == cardSequenceId);

            if (cardSequence == null)
            {
                throw new InvalidCardException(Messages.InvalidCardSequenceProvidedMessage);
            }

            if(result == null)
            {
                if(cardSequence.GameCardGroupSequence.IsAMandatoryCardGroup)
                {
                    return false;
                }

                return true;
            }

            var cardType = _gameRepository.GetCardTypeFromCardSequenceId(cardSequenceId);


            //set the result playerid to the userId here to ensure that
            //the logged in user is the one the answers are saved for
            //don't rely on front end on sending through correct userId
            result.PlayerId = userId;


            var isSaved = false;
            var isValid = false;

            switch (cardType)
            {
                case CardType.MultipleChoiceAuditStatementCard:
                case CardType.MultipleChoiceAuditStatementImageCard:
                case CardType.MultipleChoiceQuestionCard:
                case CardType.MultipleChoiceImageQuestionCard:
                case CardType.MultipleChoiceSurveyCard:
                case CardType.MultipleChoicePersonalActionPlanCard:
                

                    var multipleChoicePlayerResult = result as MultipleChoicePlayerResult;

                    isValid = IsMultipleChoicePlayerResultValid(multipleChoicePlayerResult, cardSequenceId);

                    if (isValid)
                    {
                        xml = SerializeObject(multipleChoicePlayerResult);
                    }

                    break;

                case CardType.ArrangeSequenceQuestionCard:
                    var arrangeSequencePlayerResult = result as ArrangeSequencePlayerResult;

                    isValid = IsArrangeSequencePlayerResultValid(arrangeSequencePlayerResult, cardSequenceId);

                    if (isValid)
                    {
                        xml = SerializeObject(arrangeSequencePlayerResult);
                    }

                    break;

                    //TODO: Complete validation on fill in the blanks
                case CardType.FillInTheBlankQuestionCard:
                    var fillInTheBlankPlayerResult = result as FillInTheBlankPlayerResult;

                    isValid = fillInTheBlankPlayerResult != null;

                    if (isValid)
                    {
                        xml = SerializeObject(fillInTheBlankPlayerResult);
                    }

                    break;


                case CardType.MatchingListQuestionCard:
                    var matchingListPlayerResult = result as MatchingListPlayerResult;

                    isValid = IsMatchingListPlayerResultValid(matchingListPlayerResult, cardSequenceId);
                    if (isValid)
                    {
                        xml = SerializeObject(matchingListPlayerResult);
                    }
                    break;

                case CardType.MultipleCheckBoxQuestionCard:
                case CardType.MultipleCheckBoxImageQuestionCard:
                case CardType.MultipleCheckBoxSurveyCard:                
                case CardType.MultipleCheckboxPersonalActionPlanCard:

                    var multipleCheckBoxPlayerResult = result as MultipleCheckBoxPlayerResult;
                    isValid = IsMultipleCheckBoxPlayerResultValid(multipleCheckBoxPlayerResult, cardSequenceId);

                    if (isValid)
                    {
                        xml = SerializeObject(multipleCheckBoxPlayerResult);
                    }

                    break;


                case CardType.NumericQuestionCard:
                    var numericPlayerResult = result as NumericPlayerResult;
                    isValid = numericPlayerResult != null;
                    {
                        if (isValid)
                        {
                            xml = SerializeObject(numericPlayerResult);
                        }
                    }
                    break;

                case CardType.TextAreaPersonalActionPlanCard:                
                case CardType.TextBoxPersonalActionPlanCard:
                case CardType.TextAreaSurveyCard:
                case CardType.TextBoxSurveyCard:
                    var textPlayerResult = result as TextPlayerResult;
                    isValid = textPlayerResult != null;
                    if (isValid)
                    {
                        xml = SerializeObject(textPlayerResult);
                    }
                    break;

                case CardType.PriorityIssueVotingCard:
                    var votingPlayerResult = result as VotingPlayerResult;

                    isValid = IsVotingPlayerResultValid(votingPlayerResult, cardSequenceId);
                    if (isValid)
                    {
                        xml = SerializeObject(votingPlayerResult);
                    }

                    break;

                case CardType.BranchingCard:
                    var branchingCardPlayerResult = result as BranchingCardPlayerResult;
                    isValid = IsBranchingCardPlayerResultValid(branchingCardPlayerResult, cardSequenceId);
                    
                    if (isValid)
                    {
                        xml = SerializeObject(branchingCardPlayerResult);
                    }

                    break;

                case CardType.MultipleChoicePriorityFeedbackCard:
                case CardType.TextAreaPriorityFeedbackCard:
                case CardType.MultipleCheckBoxPriorityFeedbackCard:
                    var votingFeedbackResults = result as VotingCardFeedbackResult;

                    isValid = IsFeedbackResultValid(votingFeedbackResults, cardSequenceId);

                    if (votingFeedbackResults != null && (votingFeedbackResults.FeedbackResult is MultipleCheckBoxPlayerResult ||
                        votingFeedbackResults.FeedbackResult is MultipleChoicePlayerResult || votingFeedbackResults.FeedbackResult is TextPlayerResult))
                    {
                        if (votingFeedbackResults.FeedbackResult is MultipleCheckBoxPlayerResult)
                        {
                            ((MultipleCheckBoxPlayerResult) votingFeedbackResults.FeedbackResult).PlayerId = userId;
                        }                        
                        else if (votingFeedbackResults.FeedbackResult is MultipleChoicePlayerResult)
                        {
                            ((MultipleChoicePlayerResult)votingFeedbackResults.FeedbackResult).PlayerId = userId;
                        }
                        else if (votingFeedbackResults.FeedbackResult is TextPlayerResult)
                        {
                            ((TextPlayerResult)votingFeedbackResults.FeedbackResult).PlayerId = userId;
                        }
                    }

                    if (isValid)
                    {
                        xml = SerializeObject(votingFeedbackResults);
                    }

                    break;

                default:
                    isValid = true;
                    xml = SerializeObject(result);
                    break;
            }


            if (!isValid)
            {
                throw new Exception(@"The result supplied failed validation as a result of being the incorrect type or containing invalid answers.");
            }


            if (string.IsNullOrEmpty(xml))
            {
                throw new Exception(
                    string.Format(@"Unable to serialize result. Incorrect result type supplied for card type {0}.", 
                    Enum.GetName(typeof(CardType), cardType)));
            }
            
            var resultXml = XElement.Parse(xml);
            isSaved = _gameRepository.SaveResult(userId, cardSequenceId, resultXml.Name.LocalName, resultXml);
            
            return isSaved;
        }

        public bool SkipCard(Guid userId, int cardSequenceId)
        {
            return _gameRepository.SkipCard(userId, cardSequenceId);
        }

        public bool SetGameAsCompleted(Guid userId, int gameId)
        {
            var game =
                _gameRepository.DataContext.Games.FirstOrDefault(g => g.GameID == gameId && g.UserID_CreatedBy == userId);

            if(game == null)
            {
                throw new GameCompletedException(string.Format(@"Could not find the game ID = {0} and game player ID = {1}", gameId, userId));
            }

            //if(_gameRepository.HasUnansweredQuestions(gameId))
            //{
            //    throw new GameCompletedException(Messages.IncompleteGameMessage);
            //}

            return _gameRepository.SetGameAsCompleted(userId, gameId);
        }

        public bool IsAPlayableGame(Guid userId, int gameId)
        {
            var game = _gameRepository.DataContext.Games.FirstOrDefault(g => g.GameID == gameId && g.UserID_CreatedBy == userId);

            if(game == null)
            {
                return false;
            }

            if(game.IsAGameTemplate)
            {
                return false;
            }

            return true;
        }

        public bool IsGameCompleted(Guid userId, int gameId)
        {
            var game = _gameRepository.DataContext.Games.FirstOrDefault(g => g.GameID == gameId && g.UserID_CreatedBy == userId);
            return game.GameCompletionDate.HasValue;
        }

        public int? GetGameIdFromExternalId(string externalId)
        {
            var loweredExternalId = externalId.ToLower();
            var game =
                _gameRepository.DataContext.Games.FirstOrDefault(
                    c => c.ExternalID == loweredExternalId && !c.GameID_Template.HasValue && !c.IsADeletedRow);

            if (game == null)
            {
                return null;
            }

            return game.GameID;
        }

        #endregion

        #region Public Dashboad Methods

        public Dashboard GetDashboard(Guid userId, string lcid)
        {
            var language = GetLanguage(lcid);
            return _gameRepository.GetDashboard(userId, language.Id);
        }

        #endregion

        #region Public Profile Methods

        public bool SavePlayerProfile(Guid userId, string firstname, string lastname)
        {
            return _gameRepository.UpdatePlayerDetails(userId, firstname, lastname);
        }

        public bool SaveScormPlayerProfile(Guid userId, string scormUserId, string firstname, string lastname, string email)
        {
            return _gameRepository.UpdateScormPlayerDetails(userId, scormUserId, firstname, lastname, email);
        }


        public bool SaveAvatarProfile(Guid userId, string avatarName, string avatarMetaData)
        {
            return _gameRepository.UpdateAvatarDetails(userId, avatarName, avatarMetaData);
        }

        public bool ScormGameExists(string externalId)
        {
            return _gameRepository.ScormGameExists(externalId);
        }

        public bool InviteUserToGame(Guid userId, int gameId, string externalId)
        {
            return _gameRepository.InviteUserToGame(userId, gameId, externalId);
        }

        public bool UserExistsInThemeClientUserGroup(Guid userId, string themeCode)
        {
            return _gameRepository.UserExistsInThemeClientUserGroup(userId, themeCode);
        }

        public bool AddUserToThemeClientUserGroup(Guid userId, string themeCode)
        {
            return _gameRepository.AddUserToThemeClientUserGroup(userId, themeCode);
        }

        #endregion
       

        #region Private Methods

        Language GetLanguage(string lcid)
        {
            return _languageRepository.GetLanguages()
                .FirstOrDefault(l => string.Compare(l.LCID, lcid, true) == 0);
        }
        
        static string SerializeObject<T>(T source)
        {
            using(var stream = new MemoryStream())
            {
                var serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(stream, source);
                return System.Text.Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        bool IsMultipleChoicePlayerResultValid(MultipleChoicePlayerResult result, int cardSequenceId)
        {
            if (result == null)
            {
                return false;
            }

            var cardType = _gameRepository.GetCardTypeFromCardSequenceId(cardSequenceId);

            if (cardType == CardType.MultipleChoiceAuditStatementCard ||
                cardType == CardType.MultipleChoiceAuditStatementImageCard ||
                cardType == CardType.MultipleChoicePersonalActionPlanCard ||
                cardType == CardType.MultipleChoiceQuestionCard ||
                cardType == CardType.MultipleChoiceImageQuestionCard ||
                cardType == CardType.MultipleChoiceSurveyCard ||
                cardType == CardType.MultipleChoicePriorityFeedbackCard)
            {

                var cardSequence =
                    _gameRepository.DataContext.GameCardSequences.FirstOrDefault(
                        c => c.GameCardSequenceID == cardSequenceId);

                if (cardSequence == null)
                {
                    return false;
                }

                var questionCard = cardSequence.Card.QuestionCard;

                if (questionCard == null)
                {
                    return false;
                }
                
                if(!result.AnswerId.HasValue)
                {
                    if(cardSequence.IsAMandatoryCard)
                    {
                        if(cardSequence.Card.IsATimedCard && cardSequence.Card.CardMaxTimeToAnswerInSeconds == result.TimeTakenToAnswerInSeconds)
                        {
                            return true;
                        }

                        throw new InvalidAnswerException(Messages.InvalidAnswerException);
                    }

                    return true;
                }

                var answerIds = questionCard.QuestionCard_GeneralAnswers.Select(g => g.QuestionCard_GeneralAnswerID);

                if(answerIds.Contains(result.AnswerId.Value))
                {
                    return true;
                }

                throw new InvalidAnswerException(Messages.InvalidAnswerException);
            }
            
            return false;
        }


        bool IsArrangeSequencePlayerResultValid(ArrangeSequencePlayerResult result, int cardSequenceId)
        {
            if (result == null)
            {
                return false;
            }

            var cardType = _gameRepository.GetCardTypeFromCardSequenceId(cardSequenceId);

            if (cardType != CardType.ArrangeSequenceQuestionCard)                
            {
                return false;
            }

            var cardSequence =
                _gameRepository.DataContext.GameCardSequences.FirstOrDefault(c => c.GameCardSequenceID == cardSequenceId);

            if (cardSequence == null)
            {
                return false;
            }

            var questionCard = cardSequence.Card.QuestionCard;

            if (questionCard == null)
            {
                return false;
            }            

            if (result.Results== null || result.Results.Count <=0)
            {
                if(cardSequence.IsAMandatoryCard)
                {
                    if (cardSequence.Card.IsATimedCard && cardSequence.Card.CardMaxTimeToAnswerInSeconds == result.TimeTakenToAnswerInSeconds)
                    {
                        return true;
                    }

                    throw new InvalidAnswerException(Messages.InvalidAnswerException);
                }

                return true;
            }

            if (result.Results == null || result.Results.Count <= 0)
            {
                throw new InvalidAnswerException(Messages.InvalidAnswerException);
            }

            var answerIds = questionCard.QuestionCard_GeneralAnswers.Select(g => g.QuestionCard_GeneralAnswerID);
            var playerAnswerIds = result.Results.Cast<ArrangeSequencePlayerResultAnswer>().Select(c => c.AnswerId);

       
            foreach (var playerAnswerId in playerAnswerIds)
            {
                if(!answerIds.Contains(playerAnswerId))
                {
                    throw new InvalidAnswerException(Messages.InvalidAnswerException);
                }
            }

            return true;
        }


        bool IsMatchingListPlayerResultValid(MatchingListPlayerResult result, int cardSequenceId)
        {
            if (result == null)
            {
                return false;
            }

            var cardType = _gameRepository.GetCardTypeFromCardSequenceId(cardSequenceId);


            if (cardType == CardType.MatchingListQuestionCard)
            {
                var cardSequence =
                    _gameRepository.DataContext.GameCardSequences.FirstOrDefault(
                        c => c.GameCardSequenceID == cardSequenceId);

                if (cardSequence == null)
                {
                    return false;
                }

                var questionCard = cardSequence.Card.QuestionCard;

                if (questionCard == null)
                {
                    return false;
                }

                if (result.Results == null || result.Results.Count <= 0)
                {
                    if (cardSequence.IsAMandatoryCard)
                    {
                        if (cardSequence.Card.IsATimedCard && cardSequence.Card.CardMaxTimeToAnswerInSeconds == result.TimeTakenToAnswerInSeconds)
                        {
                            return true;
                        }

                        throw new InvalidAnswerException(Messages.InvalidAnswerException);
                    }

                    return true;
                }

                if (result.Results == null || result.Results.Count <= 0)
                {
                    throw new InvalidAnswerException(Messages.InvalidAnswerException);
                }

                var answerIds = questionCard.QuestionCard_GeneralAnswers.Select(g => g.QuestionCard_GeneralAnswerID);
                var playerAnswerIds = result.Results.Cast<MatchingListPlayerResultAnswer>().Select(c => c.RHAnswerId);
                playerAnswerIds =
                    playerAnswerIds.Union(result.Results.Cast<MatchingListPlayerResultAnswer>()
                        .Select(c => c.LHAnswerId));

                foreach (var playerAnswerId in playerAnswerIds)
                {
                    if (!answerIds.Contains(playerAnswerId))
                    {
                        throw new InvalidAnswerException(Messages.InvalidAnswerException);
                    }
                }
               
                return true;
            }

            return false;
        }

        bool IsMultipleCheckBoxPlayerResultValid(MultipleCheckBoxPlayerResult result, int cardSequenceId)
        {
            if (result == null)
            {
                return false;
            }

            var cardType = _gameRepository.GetCardTypeFromCardSequenceId(cardSequenceId);

            if (cardType == CardType.MultipleCheckBoxPriorityFeedbackCard ||
                cardType == CardType.MultipleCheckBoxSurveyCard ||
                cardType == CardType.MultipleCheckboxPersonalActionPlanCard ||
                cardType == CardType.MultipleCheckBoxQuestionCard ||
                cardType == CardType.MultipleCheckBoxImageQuestionCard)
            {
                var cardSequence =
                    _gameRepository.DataContext.GameCardSequences.FirstOrDefault(
                        c => c.GameCardSequenceID == cardSequenceId);

                if (cardSequence == null)
                {
                    return false;
                }

                var questionCard = cardSequence.Card.QuestionCard;

                if (questionCard == null)
                {
                    return false;
                }

                if (result.Results == null || result.Results.Count <= 0)
                {
                    if(cardSequence.IsAMandatoryCard)
                    {
                        if (cardSequence.Card.IsATimedCard && cardSequence.Card.CardMaxTimeToAnswerInSeconds == result.TimeTakenToAnswerInSeconds)
                        {
                            return true;
                        }

                        throw new InvalidAnswerException(Messages.InvalidAnswerException);
                    }

                    return true;
                }

                if (result.Results == null || result.Results.Count <= 0)
                {
                    throw new InvalidAnswerException(Messages.InvalidAnswerException);
                }

                var answerIds = questionCard.QuestionCard_GeneralAnswers.Select(g => g.QuestionCard_GeneralAnswerID);
                var playerAnswerIds = result.Results.Cast<MultipleCheckBoxPlayerResultAnswer>().Select(c => c.AnswerId);

                foreach (var playerAnswerId in playerAnswerIds)
                {
                    if (!answerIds.Contains(playerAnswerId))
                    {
                        throw new InvalidAnswerException(Messages.InvalidAnswerException);
                    }
                }

                return true;
            }

            return false;
        }

        bool IsBranchingCardPlayerResultValid(BranchingCardPlayerResult result, int cardSequenceId)
        {
            if (result == null)
            {
                return false;
            }

            var cardType = _gameRepository.GetCardTypeFromCardSequenceId(cardSequenceId);

            if (cardType != CardType.BranchingCard)
            {
                return false;
            }

            var cardSequence =
                _gameRepository.DataContext.GameCardSequences.FirstOrDefault(c => c.GameCardSequenceID == cardSequenceId);

            if (cardSequence == null)
            {
                return false;
            }

            var questionCard = cardSequence.Card.QuestionCard;

            if (questionCard == null)
            {
                return false;
            }

            if (result.BranchingCardPlayerOptions == null || result.BranchingCardPlayerOptions.Count <= 0)
            {
                if (cardSequence.IsAMandatoryCard)
                {                   
                    throw new InvalidAnswerException(Messages.InvalidAnswerException);
                }

                return true;
            }

            return true;
        }


        bool IsVotingPlayerResultValid(VotingPlayerResult result, int cardSequenceId)
        {
            if (result == null)
            {
                return false;
            }

            var cardType = _gameRepository.GetCardTypeFromCardSequenceId(cardSequenceId);

            if (cardType != CardType.PriorityIssueVotingCard)
            {
                return false;
            }

            var cardSequence =
                _gameRepository.DataContext.GameCardSequences.FirstOrDefault(c => c.GameCardSequenceID == cardSequenceId);

            if (cardSequence == null)
            {
                return false;
            }

            var questionCard = cardSequence.Card.QuestionCard;

            if (questionCard == null)
            {
                return false;
            }

            if (result.Results == null || result.Results.Count <= 0)
            {
                if(cardSequence.IsAMandatoryCard)
                {
                    if (cardSequence.Card.IsATimedCard && cardSequence.Card.CardMaxTimeToAnswerInSeconds == result.TimeTakenToAnswerInSeconds)
                    {
                        return true;
                    }

                    throw new InvalidAnswerException(Messages.InvalidAnswerException);
                }

                return true;
            }

            if (result.Results == null || result.Results.Count <= 0)
            {
                throw new InvalidAnswerException(Messages.InvalidAnswerException);
            }

            var answerIds = questionCard.QuestionCard_GeneralAnswers.Select(g => g.QuestionCard_GeneralAnswerID);
            var playerAnswerIds = result.Results.Cast<VotingPlayerResultAnswer>().Select(c => c.AnswerId);

            foreach (var playerAnswerId in playerAnswerIds)
            {
                if (!answerIds.Contains(playerAnswerId))
                {
                    throw new InvalidAnswerException(Messages.InvalidAnswerException);
                }
            }

            return true;
        }

        bool IsFeedbackResultValid(VotingCardFeedbackResult result, int cardSequenceId)
        {
            if (result == null)
            {
                return false;
            }

            var cardType = _gameRepository.GetCardTypeFromCardSequenceId(cardSequenceId);

            if (cardType != CardType.MultipleChoicePriorityFeedbackCard &&
                cardType != CardType.TextAreaPriorityFeedbackCard && 
                cardType != CardType.MultipleCheckBoxPriorityFeedbackCard)
            {
                return false;
            }

            var votingCard = _gameRepository.DataContext.Cards.FirstOrDefault(c => c.CardID == result.VotingCardId);
            var votingCardType = _gameRepository.GetCardType(result.VotingCardId);

            if (votingCard == null || votingCardType != CardType.PriorityIssueVotingCard)
            {
                throw new InvalidAnswerException(Messages.InvalidVotingCardMessage);
            }

            var isAValidAnsweId = votingCard.QuestionCard.QuestionCard_GeneralAnswers.Any(
                g => g.QuestionCard_GeneralAnswerID == result.AnswerId);

            if (!isAValidAnsweId)
            {
                throw new InvalidAnswerException(Messages.InvalidVotingCardAnswerMessage);
            }

            var cardGroupSequenceId =
                _gameRepository.DataContext.GameCardSequences.Where(s => s.GameCardSequenceID == cardSequenceId)
                    .Select(s => s.GameCardGroupSequenceID).FirstOrDefault();

            var isVotingCardInSameSequence =
                _gameRepository.DataContext.GameCardSequences.Any(
                    s => s.GameCardGroupSequenceID == cardGroupSequenceId && s.CardID == result.VotingCardId);

            if (!isVotingCardInSameSequence)
            {
                throw new InvalidAnswerException(Messages.InvalidVotingCardMessage);
            }

            var gameid =
                _gameRepository.DataContext.GameCardGroupSequences.Where(
                    g => g.GameCardGroupSequenceID == cardGroupSequenceId).Select(s => s.GameID).FirstOrDefault();

            var priorityVotingCardResult =
                _gameRepository.DataContext.Results.FirstOrDefault(r => r.GameID == gameid && r.CardID == result.VotingCardId);

            if (priorityVotingCardResult == null)
            {
                throw new InvalidAnswerException(Messages.InvalidVotingCardAnswerMessage);
            }

            var votingCardAnswer = Deserialize<VotingPlayerResult>(priorityVotingCardResult.ResultXmlElement);

            if (votingCardAnswer == null)
            {
                throw new InvalidAnswerException(Messages.InvalidVotingCardAnswerMessage);
            }
            else
            {
                var votingPlayerResults = votingCardAnswer.Results.Cast<VotingPlayerResultAnswer>();

                if (votingPlayerResults == null || votingPlayerResults.All(a => a.AnswerId != result.AnswerId))
                {
                    throw new InvalidAnswerException(Messages.InvalidVotingCardAnswerMessage);
                }
            }
            

            if (cardType == CardType.MultipleChoicePriorityFeedbackCard)
            {
                var multipleChoiceResult = result.FeedbackResult as MultipleChoicePlayerResult;

                if (!IsMultipleChoicePlayerResultValid(multipleChoiceResult, cardSequenceId))
                {
                    throw new InvalidAnswerException(Messages.InvalidFeedbackResultMessage);
                }
            }
            else if (cardType == CardType.TextAreaPriorityFeedbackCard)
            {
                var textResult = result.FeedbackResult as TextPlayerResult;

                if (textResult == null)
                {
                    throw new InvalidAnswerException(Messages.InvalidFeedbackResultMessage);
                }
            }
            else if (cardType == CardType.MultipleCheckBoxPriorityFeedbackCard)
            {
                var multipleCheckboxResult = result.FeedbackResult as MultipleCheckBoxPlayerResult;

                if (!IsMultipleCheckBoxPlayerResultValid(multipleCheckboxResult, cardSequenceId))
                {
                    throw new InvalidAnswerException(Messages.InvalidFeedbackResultMessage);
                }
            }

            return true;
        }

        private static T Deserialize<T>(XElement element)
        {
            var serializer = new DataContractSerializer(typeof(T));
            return (T)serializer.ReadObject(element.CreateReader());
        }
        

        #endregion
    }
}
