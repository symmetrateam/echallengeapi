﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Domain.Model;

namespace Diversity.Services.Business
{
    public interface IErrorMessageService : IService
    {
        IEnumerable<ErrorMessage> GetErrorMessages();
        
        IEnumerable<ErrorMessage> GetErrorMessages(string lcid);
        IEnumerable<ErrorMessage> GetErrorMessages(Language language);
        IEnumerable<ErrorMessage> GetErrorMessagesByCode(string errorCode);
        ErrorMessage GetErrorMessage(string lcid, string errorCode);
        ErrorMessage GetErrorMessage(Language language, string errorCode);
    }
}
