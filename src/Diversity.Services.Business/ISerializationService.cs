﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Diversity.Services.Business
{
    public interface ISerializationService : IService
    {
        string ToXmlString(object obj);
        object FromXmlString(string xmlString);
        object FromXmlDocument(XmlDocument document);
    }
}
