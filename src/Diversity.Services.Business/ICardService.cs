﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Services.Business
{
    public interface ICardService : IService
    {
        /// <summary>
        /// Creates a duplicate card for a game 
        /// with assets associated to the card located in the temporary folders
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Card CreateDuplicateCard(int cardId, Guid userId);
        /// <summary>
        /// Creates a duplicate card for a game with assets located in either temporary of live
        /// folders. 
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="userId"></param>
        ///<param name="storeAssetsInLiveFolder"></param>
        /// <returns></returns>
        Card CreateDuplicateCard(int cardId, Guid userId, bool storeAssetsInLiveFolder);
    }
}
