﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Diversity.Data.Utility
{
    public class LazyList<T> : IList<T>
    {
        private IList<T> _inner;
        private IQueryable<T> _query;

        public LazyList()
        {
            _inner = new List<T>();
        }

        public LazyList(IQueryable<T> query)
        {
            _query = query;
        }

        public int IndexOf(T item)
        {
            return Inner.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            Inner.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            Inner.RemoveAt(index);
        }

        public T this[int index]
        {
            get { return Inner[index]; }
            set { Inner[index] = value; }
        }

        public void Add(T item)
        {
            _inner = _inner ?? new List<T>();
            Inner.Add(item);
        }

        public void Add(object ob)
        {
            throw new NotImplementedException("This is for serialization");
        }

        public void Clear()
        {
            if (_inner != null)
            {
                Inner.Clear();
            }
        }

        public bool Contains(T item)
        {
            return Inner.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Inner.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return Inner.Remove(item);
        }

        public int Count
        {
            get { return Inner.Count; }
        }

        public bool IsReadOnly
        {
            get { return Inner.IsReadOnly; }
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return Inner.GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)Inner).GetEnumerator();
        }

        public IList<T> Inner
        {
            get
            {
                if (_inner == null)
                {
                    _inner = _query.ToList();
                }
                return _inner;
            }
        }

    }
}
