﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;

namespace Diversity.Data.Utility
{
    public static class Entity
    {
        /// <summary>
        /// Clones any object and returns the new cloned object.
        /// </summary>
        /// <typeparam name="T">The type of object.</typeparam>
        /// <param name="source">The original object.</param>
        /// <returns>The clone of the object.</returns>
        public static T Clone<T>(this T source)
        {
            var dcs = new DataContractSerializer(typeof(T));
            using (var ms = new System.IO.MemoryStream())
            {
                dcs.WriteObject(ms, source);
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                return (T)dcs.ReadObject(ms);
            }
        }

        public static object Copy(object source, object destination)
        {
            System.Reflection.PropertyInfo[] sourceProps = source.GetType().GetProperties();

            System.Reflection.PropertyInfo[] destinationProps = destination.GetType().GetProperties();

            foreach (System.Reflection.PropertyInfo sourceProp in sourceProps)
            {
                ColumnAttribute column = Attribute.GetCustomAttribute(sourceProp, typeof(ColumnAttribute)) as ColumnAttribute;

                if (column != null && !column.IsPrimaryKey)
                {

                    foreach (System.Reflection.PropertyInfo destinationProp in destinationProps)
                    {

                        if (sourceProp.Name == destinationProp.Name && destinationProp.CanWrite)
                        {

                            destinationProp.SetValue(destination, sourceProp.GetValue(source, null), null);

                            break;

                        }

                    }

                }

            }

            return destination;
        }

    } 
}
