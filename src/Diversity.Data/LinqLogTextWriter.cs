﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using log4net;

namespace Diversity.Data
{
    public class LinqLogTextWriter : TextWriter
    {
        private static readonly ILog _log = LogManager.GetLogger((System.Reflection.MethodBase.GetCurrentMethod().DeclaringType));
        
        public override void Write(string value)
        {
            _log.Debug(value);
        }

        public override void Write(char[] buffer, int index, int count)
        {
            _log.Debug(new string(buffer, index, count));
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}
