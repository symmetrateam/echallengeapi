﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Domain.Model;

namespace Diversity.Data.DataAccess
{
    public interface ILanguageRepository : IRepository
    {
        IQueryable<Language> GetLanguages();
    }
}
