﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Data.DataAccess
{
    public interface IRepository
    {
        EChallengeDataContext DataContext { get; }        
    }
}
