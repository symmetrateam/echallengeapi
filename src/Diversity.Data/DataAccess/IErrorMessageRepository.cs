﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Domain.Model;

namespace Diversity.Data.DataAccess
{
    public interface IErrorMessageRepository : IRepository
    {
        /// <summary>
        /// Gets a list of all error messages
        /// </summary>
        /// <returns></returns>
        IEnumerable<ErrorMessage> GetErrorMessages();        
    }
}
