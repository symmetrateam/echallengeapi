﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Diversity.Common.Enumerations;
using Diversity.Domain.Model;

namespace Diversity.Data.DataAccess
{
    public interface IGameRepository : IRepository
    {        
        IQueryable<Game> GetGames(int languageId);
        Game GetGame(int gameId, int languageId, Guid userId);
        int? GetGameIdForInProgressGame(int gameTemplateId, Guid userId);
        Dashboard GetDashboard(Guid userId, int languageId);
        bool UpdatePlayerDetails(Guid userId, string firstname, string lastname);
        bool UpdateScormPlayerDetails(Guid userId, string scormUserId, string firstname, string lastname, string email);
        bool UpdateAvatarDetails(Guid userId, string avatarName, string avatarMetaData);

        CardType GetCardType(int cardId);
        CardType GetCardTypeFromCardSequenceId(int cardSequenceId);
        Game CreateGameFromTemplateGame(Game templateGame, Guid userId, int languageId, bool lockDownTemplate);
        bool SaveResult(Guid userId, int cardSequenceId, string resultType, XElement resultXml);
        bool SkipCard(Guid userId, int cardSequenceId);
        bool SetGameAsCompleted(Guid userId, int gameId);
        bool IncrementGameRunCount(int gameId, Guid userId);
        bool HasUnansweredQuestions(int gameId);


        IQueryable<FlashAsset> GetFlashAssets(int languageId);

        GameHeader.GameSettingsInfo GetGameSettings(DataModel.SqlRepository.Game game);
        bool CanNewGameBePlayedByUser(int gameId, int languageId, Guid userId);
        bool ScormGameExists(string externalId);
        bool InviteUserToGame(Guid userId, int gameId, string externalId);
        bool UserExistsInThemeClientUserGroup(Guid userId, string themeClientUserGroupExternalId);
        bool AddUserToThemeClientUserGroup(Guid userId, string themeClientUserGroupExternalId);
    }
}
