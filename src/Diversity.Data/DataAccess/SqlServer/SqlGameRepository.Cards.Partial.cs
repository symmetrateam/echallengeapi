﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using Diversity.Common.Enumerations;
using Diversity.Data.Utility;
using Diversity.DataModel.SqlRepository;
using Diversity.Domain.Model;
using System.Web;
using BranchingCard = Diversity.Domain.Model.BranchingCard;
using Card = Diversity.DataModel.SqlRepository.Card;
using Game = Diversity.DataModel.SqlRepository.Game;

namespace Diversity.Data.DataAccess.SqlServer
{
    public partial class SqlGameRepository
    {
        private IEnumerable<Domain.Model.Card> GetCards(int gameId, int languageId, List<int> avatarGameIds)
        {
            var cards = new List<Domain.Model.Card>();

            var avatarGames = GetAvatarGames(avatarGameIds).ToList();

            //Display only cards
            cards.AddRange(GetCompanyPolicyCards(gameId, languageId));
            cards.AddRange(GetLeadingQuestionCards(gameId, languageId));
            cards.AddRange(GetGameCompletedCards(gameId, languageId));
            cards.AddRange(GetRoundResultCards(gameId, languageId));
            cards.AddRange(GetScoreboardCards(gameId, languageId));
            cards.AddRange(GetLcdCategoryCards(gameId, languageId));
            cards.AddRange(GetCertificateCards(gameId, languageId));
            cards.AddRange(GetAwardCards(gameId, languageId));
            cards.AddRange(GetIntroSurveyCards(gameId, languageId));
            cards.AddRange(GetTransitionUpCards(gameId, languageId));
            cards.AddRange(GetTransitionDownCards(gameId, languageId));
            cards.AddRange(GetGameIntroCards(gameId, languageId));
            cards.AddRange(GetThemeIntroCards(gameId, languageId));
            cards.AddRange(GetMultiMediaQuestionCards(gameId, languageId));

            //Cards that require answers
            //cards.AddRange(GetArrangeSequenceQuestionCards(gameId, languageId, avatarGames));
            //cards.AddRange(GetFillInTheBlankQuestionCards(gameId, languageId, avatarGames));
            cards.AddRange(GetMatchingListQuestionCards(gameId, languageId, avatarGames));
            cards.AddRange(GetMultipleCheckBoxQuestionCards(gameId, languageId, avatarGames));
            cards.AddRange(GetMultipleCheckBoxSurveyCards(gameId, languageId));
            cards.AddRange(GetMultipleChoiceQuestionCards(gameId, languageId, avatarGames));
            cards.AddRange(GetMultipleChoiceImageQuestionCards(gameId, languageId, avatarGames));
            cards.AddRange(GetMultipleCheckBoxPriorityFeedbackCards(gameId, languageId));
            cards.AddRange(GetMultipleCheckBoxPersonalActionPlanCards(gameId, languageId));

            cards.AddRange(GetMultipleChoiceAuditStatementCards(gameId, languageId, avatarGames));
            cards.AddRange(GetMultipleChoiceAuditStatementImageCards(gameId, languageId, avatarGames));
            cards.AddRange(GetMultipleChoiceSurveyCards(gameId, languageId));
            //cards.AddRange(GetNumericQuestionCards(gameId, languageId, avatarGames));
            //cards.AddRange(GetPotLuckQuestionCards(gameId, languageId));
            cards.AddRange(GetTextAreaSurveyCards(gameId, languageId));
            cards.AddRange(GetTextBoxSurveyCards(gameId, languageId));
            cards.AddRange(GetMultipleChoicePriorityFeedbackCards(gameId, languageId));
            cards.AddRange(GetMultipleChoicePersonalActionPlanCards(gameId, languageId));
            cards.AddRange(GetTextAreaPersonalActionPlanCards(gameId, languageId));
            cards.AddRange(GetTextBoxPersonalActionPlanCards(gameId, languageId));
            cards.AddRange(GetTextAreaPriorityFeedbackCards(gameId, languageId));
            cards.AddRange(GetPriorityIssueVotingCards(gameId, languageId));            
            cards.AddRange(GetMultipleCheckBoxImageQuestionCards(gameId, languageId, avatarGames));
            cards.AddRange(GetBranchingCards(gameId, languageId));

            return cards.AsEnumerable().OrderBy(c => c.CardSequenceNo);
        }


        private IEnumerable<ArrangeSequenceQuestionCard> GetArrangeSequenceQuestionCards(int gameId, int languageId,
                                                                                         IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.ArrangeSequenceQuestionCard);
            var cardType = CardType.CompanyPolicyCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          languageId,
                                          //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                                          IsATimedCard = c.IsATimedCard,
                                          MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //Scoring = GetScoring(c.QuestionCard),
                                          UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<ArrangeSequenceQuestionCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<ArrangeSequencePlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));
                var card = new ArrangeSequenceQuestionCard()
                               {
                                   AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                                   IsATimedCard = result.IsATimedCard,
                                   MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   Scoring = GetScoring(result.QuestionCard),
                                   UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<CompanyPolicyCard> GetCompanyPolicyCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof (CardType), CardType.CompanyPolicyCard);
            var cardType = CardType.CompanyPolicyCard.ToString();

            var results =  (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                              {
                                  card = c,
                                  CardType = cardType,
                                  CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                  Id = c.CardID,
                                  //Title = GetCardTitle(c, languageId),
                                  CardSequenceId = gcs.GameCardSequenceID,
                                  IsATimedCard = c.IsATimedCard,
                                  CardSequenceNo = gcs.CardSequenceNo,
                                  CardViewedInGame = gcs.CardViewedInGame,
                                  CardViewedInGameOn = gcs.CardViewedInGameOn,
                                  IsAMandatoryCard = gcs.IsAMandatoryCard,
                                  UseLongVersion = gcs.UseLongVersion,
                                  CompanyPolicyAssociatedImageUrl = c.QuestionCard.AssociatedImageUrl,
                                  CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                  //Topic = GetTopic(c, languageId),
                                  //SubTopic = GetSubTopic(c, languageId),
                                  AudioTranscriptFile = c.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = c.AudioTranscriptText,
                                  QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                  QuestionMultimediaTypeImageVideoEtc =
                       c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls =
                       c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                  AnswerMultimediaTypeImageVideoEtc =
                       c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls =
                       c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                  c.QuestionCard.QuestionMultimediaThumbnailURL,
                                  c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                              }).ToList();

            var companyPolicyCards = new List<CompanyPolicyCard>();
            
            foreach (var result in results)
            {
                var card = new CompanyPolicyCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   CardSequenceNo = result.CardSequenceNo,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn =result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CompanyPolicyAssociatedImageUrl = result.CompanyPolicyAssociatedImageUrl,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                companyPolicyCards.Add(card);
            }

            return companyPolicyCards;
        }

        private IEnumerable<ThemeIntroCard> GetThemeIntroCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof (CardType), CardType.ThemeIntroCard);
            var cardType = CardType.ThemeIntroCard.ToString();

            var results =  (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                   {
                       card = c,
                       c.QuestionCard,
                       CardType = cardType,
                       CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                       Id = c.CardID,
                       //Title = GetCardTitle(c, languageId),
                       CardSequenceId = gcs.GameCardSequenceID,
                       IsATimedCard = c.IsATimedCard,
                       CardSequenceNo = gcs.CardSequenceNo,
                       CardViewedInGame = gcs.CardViewedInGame,
                       CardViewedInGameOn = gcs.CardViewedInGameOn,
                       IsAMandatoryCard = gcs.IsAMandatoryCard,
                       UseLongVersion = gcs.UseLongVersion,
                       StatementMultimediaUrl = c.QuestionCard.AssociatedImageUrl,
                       CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                       //Topic = GetTopic(c, languageId),
                       //SubTopic = GetSubTopic(c, languageId),
                       AudioTranscriptFile = c.AudioTranscriptFile,
                       AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                       AudioTranscriptText = c.AudioTranscriptText,
                       //StatementListHeader = GetStatementHeader(c, languageId),
                       //StatementListItems = GetCardStatements(c,languageId, gcs.UseLongVersion),
                       //Description = GetThemeDescription(c.QuestionCard, languageId, gcs.UseLongVersion),
                       QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                       QuestionMultimediaTypeImageVideoEtc =
            c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                       QuestionMultimediaStartAutoElsePushPlayToStart =
            c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                       QuestionMultimediaShowPlaybackControls =
            c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                       AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                       AnswerMultimediaTypeImageVideoEtc =
            c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                       AnswerMultimediaStartAutoElsePushPlayToStart =
            c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                       AnswerMultimediaShowPlaybackControls =
            c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                       c.QuestionCard.QuestionMultimediaThumbnailURL,
                       c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                   }).ToList();
            
            var cards = new List<ThemeIntroCard>();

            foreach (var result in results)
            {
                var card = new ThemeIntroCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   CardSequenceNo = result.CardSequenceNo,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   StatementMultimediaUrl = result.StatementMultimediaUrl,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   StatementListHeader = GetStatementHeader(result.card, languageId),
                                   StatementListItems = GetCardStatements(result.card, languageId, result.UseLongVersion),
                                   Description = GetThemeDescription(result.QuestionCard, languageId, result.UseLongVersion),
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<MultiMediaQuestionCard> GetMultiMediaQuestionCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultiMediaCard);
            var cardType = CardType.MultiMediaCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                           {
                               Card = c,
                               c.QuestionCard,
                               CardType = cardType,
                               CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                               Id = c.CardID,
                               //Title = GetCardTitle(c, languageId),
                               CardSequenceId = gcs.GameCardSequenceID,
                               IsATimedCard = c.IsATimedCard,
                               CardSequenceNo = gcs.CardSequenceNo,
                               //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                               CardViewedInGame = gcs.CardViewedInGame,
                               CardViewedInGameOn = gcs.CardViewedInGameOn,
                               IsAMandatoryCard = gcs.IsAMandatoryCard,
                               UseLongVersion = gcs.UseLongVersion,
                               CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                               //Topic = GetTopic(c, languageId),
                               //SubTopic = GetSubTopic(c, languageId),
                               AudioTranscriptFile = c.AudioTranscriptFile,
                               AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                               AudioTranscriptText = c.AudioTranscriptText,
                               QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                               QuestionMultimediaTypeImageVideoEtc =
                    c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                               QuestionMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                               QuestionMultimediaShowPlaybackControls =
                    c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                               AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                               AnswerMultimediaTypeImageVideoEtc =
                    c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                               AnswerMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                               AnswerMultimediaShowPlaybackControls =
                    c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                               c.QuestionCard.QuestionMultimediaThumbnailURL,
                               c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                           }).ToList();

            var cards = new List<MultiMediaQuestionCard>();

            foreach (var result in results)
            {
                var card = new MultiMediaQuestionCard()
                {
                    CardType = cardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.Card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.Card, languageId),
                    SubTopic = GetSubTopic(result.Card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<FillInTheBlankQuestionCard> GetFillInTheBlankQuestionCards(int gameId, int languageId,
                                                                                       IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.FillInTheBlankQuestionCard);
            var cardType = CardType.FillInTheBlankQuestionCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //Scoring = GetScoring(c.QuestionCard),
                                          UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          AnswerIsCaseSensitive = c.QuestionCard.AnswerIsCaseSensitive,
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<FillInTheBlankQuestionCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<FillInTheBlankPlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));
                var card = new FillInTheBlankQuestionCard()
                               {
                                   AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   Scoring = GetScoring(result.QuestionCard),
                                   UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   AnswerIsCaseSensitive = result.AnswerIsCaseSensitive,
                                   CardSequenceNo = result.CardSequenceNo,
                                   MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<BranchingCard> GetBranchingCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.BranchingCard);
            var cardType = CardType.BranchingCard.ToString();

            var results = (from c in DataContext.Cards.OfType<Diversity.DataModel.SqlRepository.BranchingCard>()
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                           {
                               Card = c,
                               c.QuestionCard,
                               CardType = cardType,
                               CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                               Id = c.CardID,
                               //Title = GetCardTitle(c, languageId),
                               CardSequenceId = gcs.GameCardSequenceID,
                               IsATimedCard = c.IsATimedCard,
                               CardSequenceNo = gcs.CardSequenceNo,
                               //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                               CardViewedInGame = gcs.CardViewedInGame,
                               CardViewedInGameOn = gcs.CardViewedInGameOn,
                               IsAMandatoryCard = gcs.IsAMandatoryCard,
                               UseLongVersion = gcs.UseLongVersion,
                               CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                               //Topic = GetTopic(c, languageId),
                               //SubTopic = GetSubTopic(c, languageId),
                               AudioTranscriptFile = c.AudioTranscriptFile,
                               AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                               AudioTranscriptText = c.AudioTranscriptText,
                               QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                               QuestionMultimediaTypeImageVideoEtc =
                    c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                               QuestionMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                               QuestionMultimediaShowPlaybackControls =
                    c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                               AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                               AnswerMultimediaTypeImageVideoEtc =
                    c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                               AnswerMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                               AnswerMultimediaShowPlaybackControls =
                    c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                               c.QuestionCard.QuestionMultimediaThumbnailURL,
                               c.QuestionCard.AnswerExplanationMultimediaThumbnailURL,
                               c.MustAddBackgroundBorderToBranches,
                               c.MustPressButtonsInSequence,
                               c.MinButtonPresses,
                               c.ButtonOrientation,
                               c.CardGroupID_BranchingCompleted
                           }).ToList();

            var cards = new List<BranchingCard>();

            foreach (var result in results)
            {
                var card = new BranchingCard()
                {
                    CardType = cardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.Card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.Card, languageId),
                    SubTopic = GetSubTopic(result.Card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL,
                    MustAddBackgroundBorderToBranches = result.MustAddBackgroundBorderToBranches,
                    MustPressButtonsInSequence = result.MustPressButtonsInSequence,
                    MinButtonPresses = result.MinButtonPresses,
                    ButtonOrientation = result.ButtonOrientation,
                    BranchingCompletedCardGroupId = result.CardGroupID_BranchingCompleted,
                    BranchingCardButtons = GetBranchingCardButtons(result.Card, languageId, result.UseLongVersion)
                };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<LeadingQuestionCard> GetLeadingQuestionCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.LeadingQuestionCard);
            var cardType = CardType.LeadingQuestionCard.ToString();

            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                              {
                                  Card = c,
                                  c.QuestionCard,
                                  CardType = cardType,
                                  CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                  Id = c.CardID,
                                  //Title = GetCardTitle(c, languageId),
                                  CardSequenceId = gcs.GameCardSequenceID,
                                  IsATimedCard = c.IsATimedCard,
                                  CardSequenceNo = gcs.CardSequenceNo,
                                  //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                  CardViewedInGame = gcs.CardViewedInGame,
                                  CardViewedInGameOn = gcs.CardViewedInGameOn,
                                  IsAMandatoryCard = gcs.IsAMandatoryCard,
                                  UseLongVersion = gcs.UseLongVersion,
                                  CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                  //Topic = GetTopic(c, languageId),
                                  //SubTopic = GetSubTopic(c, languageId),
                                  AudioTranscriptFile = c.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = c.AudioTranscriptText,
                                  QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                  QuestionMultimediaTypeImageVideoEtc =
                       c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls =
                       c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                  AnswerMultimediaTypeImageVideoEtc =
                       c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls =
                       c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                  c.QuestionCard.QuestionMultimediaThumbnailURL,
                                  c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                              }).ToList();

            var cards = new List<LeadingQuestionCard>();

            foreach (var result in results)
            {
                var card = new LeadingQuestionCard()
                               {
                                   CardType = cardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.Card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   CardSequenceNo = result.CardSequenceNo,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Topic = GetTopic(result.Card, languageId),
                                   SubTopic = GetSubTopic(result.Card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<TransitionUpCard> GetTransitionUpCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.TransitionUpCard);
            var cardType = CardType.TransitionUpCard.ToString();

            var results =  (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                              {
                                  Card = c,
                                  c.QuestionCard,
                                  CardType = cardType,
                                  CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                  Id = c.CardID,
                                  //Title = GetCardTitle(c, languageId),
                                  CardSequenceId = gcs.GameCardSequenceID,
                                  IsATimedCard = c.IsATimedCard,
                                  CardSequenceNo = gcs.CardSequenceNo,
                                  //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                  CardViewedInGame = gcs.CardViewedInGame,
                                  CardViewedInGameOn = gcs.CardViewedInGameOn,
                                  IsAMandatoryCard = gcs.IsAMandatoryCard,
                                  UseLongVersion = gcs.UseLongVersion,
                                  CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                  //Topic = GetTopic(c, languageId),
                                  //SubTopic = GetSubTopic(c, languageId),
                                  AudioTranscriptFile = c.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = c.AudioTranscriptText,
                                  QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                  QuestionMultimediaTypeImageVideoEtc =
                       c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls =
                       c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                  AnswerMultimediaTypeImageVideoEtc =
                       c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls =
                       c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                  c.QuestionCard.QuestionMultimediaThumbnailURL,
                                  c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                              }).ToList();

            var cards = new List<TransitionUpCard>();
            foreach (var result in results)
            {
                var card = new TransitionUpCard()
                              {
                                  CardType = result.CardType,
                                  CardTypeGroup = result.CardType,
                                  Id = result.Id,
                                  Title = GetCardTitle(result.Card, languageId),
                                  CardSequenceId = result.CardSequenceId,
                                  IsATimedCard = result.IsATimedCard,
                                  CardSequenceNo = result.CardSequenceNo,
                                  Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                  CardViewedInGame = result.CardViewedInGame,
                                  CardViewedInGameOn = result.CardViewedInGameOn,
                                  IsAMandatoryCard = result.IsAMandatoryCard,
                                  UseLongVersion = result.UseLongVersion,
                                  CardGroupSequenceId = result.CardGroupSequenceId,
                                  Topic = GetTopic(result.Card, languageId),
                                  SubTopic = GetSubTopic(result.Card, languageId),
                                  AudioTranscriptFile = result.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = result.AudioTranscriptText,
                                  QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                  QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                  AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                  QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                  AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                              };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<TransitionDownCard> GetTransitionDownCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.TransitionDownCard);
            var cardType = CardType.TransitionDownCard.ToString();

            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                   {
                       Card = c,
                       c.QuestionCard,
                       CardType = cardType,
                       CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                       Id = c.CardID,
                       //Title = GetCardTitle(c, languageId),
                       CardSequenceId = gcs.GameCardSequenceID,
                       IsATimedCard = c.IsATimedCard,
                       CardSequenceNo = gcs.CardSequenceNo,
                       //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                       CardViewedInGame = gcs.CardViewedInGame,
                       CardViewedInGameOn = gcs.CardViewedInGameOn,
                       IsAMandatoryCard = gcs.IsAMandatoryCard,
                       UseLongVersion = gcs.UseLongVersion,
                       CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                       //Topic = GetTopic(c, languageId),
                       //SubTopic = GetSubTopic(c, languageId),
                       AudioTranscriptFile = c.AudioTranscriptFile,
                       AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                       AudioTranscriptText = c.AudioTranscriptText,
                       QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                       QuestionMultimediaTypeImageVideoEtc =
            c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                       QuestionMultimediaStartAutoElsePushPlayToStart =
            c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                       QuestionMultimediaShowPlaybackControls =
            c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                       AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                       AnswerMultimediaTypeImageVideoEtc =
            c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                       AnswerMultimediaStartAutoElsePushPlayToStart =
            c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                       AnswerMultimediaShowPlaybackControls =
            c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                       c.QuestionCard.QuestionMultimediaThumbnailURL,
                       c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                   }).ToList();

            var cards = new List<TransitionDownCard>();

            foreach (var result in results)
            {
                var card = new TransitionDownCard()
                {
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.Card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.Card, languageId),
                    SubTopic = GetSubTopic(result.Card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<IntroSurveyCard> GetIntroSurveyCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.IntroSurveyCard);
            var cardType = CardType.IntroSurveyCard.ToString();

            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                   {
                       Card = c,
                       c.QuestionCard,
                       CardType = cardType,
                       CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                       Id = c.CardID,
                       //Title = GetCardTitle(c, languageId),
                       CardSequenceId = gcs.GameCardSequenceID,
                       IsATimedCard = c.IsATimedCard,
                       CardSequenceNo = gcs.CardSequenceNo,
                      // Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                       CardViewedInGame = gcs.CardViewedInGame,
                       CardViewedInGameOn = gcs.CardViewedInGameOn,
                       IsAMandatoryCard = gcs.IsAMandatoryCard,
                       UseLongVersion = gcs.UseLongVersion,
                       CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                       //Topic = GetTopic(c, languageId),
                       //SubTopic = GetSubTopic(c, languageId),
                       AudioTranscriptFile = c.AudioTranscriptFile,
                       AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                       AudioTranscriptText = c.AudioTranscriptText,
                       QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                       QuestionMultimediaTypeImageVideoEtc =
            c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                       QuestionMultimediaStartAutoElsePushPlayToStart =
            c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                       QuestionMultimediaShowPlaybackControls =
            c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                       AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                       AnswerMultimediaTypeImageVideoEtc =
            c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                       AnswerMultimediaStartAutoElsePushPlayToStart =
            c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                       AnswerMultimediaShowPlaybackControls =
            c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                       c.QuestionCard.QuestionMultimediaThumbnailURL,
                       c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                   }).ToList();

            var cards = new List<IntroSurveyCard>();

            foreach (var result in results)
            {
                var card = new IntroSurveyCard()
                {
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.Card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.Card, languageId),
                    SubTopic = GetSubTopic(result.Card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;

        }

        private IEnumerable<GameCompletedCard> GetGameCompletedCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.GameCompletedCard);
            var cardType = CardType.GameCompletedCard.ToString();

            var results = (from c in DataContext.Cards
                          join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                          where gcs.GameCardGroupSequence.GameID == gameId &&
                                c.Internal_CardType.Internal_CardTypeShortCode ==
                                cardShortCode
                          select new 
                                     {
                                         Card = c,
                                         c.QuestionCard,
                                         CardType = cardType,
                                         CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                         Id = c.CardID,
                                         //Title = GetCardTitle(c, languageId),
                                         CardSequenceId = gcs.GameCardSequenceID,
                                         IsATimedCard = c.IsATimedCard,
                                         CardSequenceNo = gcs.CardSequenceNo,
                                         //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                         CardViewedInGame = gcs.CardViewedInGame,
                                         CardViewedInGameOn = gcs.CardViewedInGameOn,
                                         IsAMandatoryCard = gcs.IsAMandatoryCard,
                                         UseLongVersion = gcs.UseLongVersion,
                                         CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                         //Topic = GetTopic(c, languageId),
                                         //SubTopic = GetSubTopic(c, languageId),
                                         AudioTranscriptFile = c.AudioTranscriptFile,
                                         AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                         AudioTranscriptText = c.AudioTranscriptText,
                                         QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                         QuestionMultimediaTypeImageVideoEtc =
                              c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                         QuestionMultimediaStartAutoElsePushPlayToStart =
                              c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                         QuestionMultimediaShowPlaybackControls =
                              c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                         AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                         AnswerMultimediaTypeImageVideoEtc =
                              c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                         AnswerMultimediaStartAutoElsePushPlayToStart =
                              c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                         AnswerMultimediaShowPlaybackControls =
                              c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                         c.QuestionCard.QuestionMultimediaThumbnailURL,
                                         c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                     }).ToList();

            var cards = new List<GameCompletedCard>();

            foreach (var result in results)
            {
                var card = new GameCompletedCard()
                {
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.Card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.Card, languageId),
                    SubTopic = GetSubTopic(result.Card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<GameIntroCard> GetGameIntroCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.GameIntroCard);
            var cardType = CardType.GameIntroCard.ToString();

            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                   {
                       Card = c,
                       CardType = cardType,
                       CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                       Id = c.CardID,
                       //Title = GetCardTitle(c, languageId),
                       CardSequenceId = gcs.GameCardSequenceID,
                       IsATimedCard = c.IsATimedCard,
                       CardSequenceNo = gcs.CardSequenceNo,
                       CardViewedInGame = gcs.CardViewedInGame,
                       CardViewedInGameOn = gcs.CardViewedInGameOn,
                       IsAMandatoryCard = gcs.IsAMandatoryCard,
                       UseLongVersion = gcs.UseLongVersion,
                       GameIntroAssociatedImageUrl = c.QuestionCard.AssociatedImageUrl,
                       CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                       //Topic = GetTopic(c, languageId),
                       //SubTopic = GetSubTopic(c, languageId),
                       AudioTranscriptFile = c.AudioTranscriptFile,
                       AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                       AudioTranscriptText = c.AudioTranscriptText,
                       QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                       QuestionMultimediaTypeImageVideoEtc =
            c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                       QuestionMultimediaStartAutoElsePushPlayToStart =
            c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                       QuestionMultimediaShowPlaybackControls =
            c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                       AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                       AnswerMultimediaTypeImageVideoEtc =
            c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                       AnswerMultimediaStartAutoElsePushPlayToStart =
            c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                       AnswerMultimediaShowPlaybackControls =
            c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                       c.QuestionCard.QuestionMultimediaThumbnailURL,
                       c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                   }).ToList();

            var cards = new List<GameIntroCard>();

            foreach (var result in results)
            {
                var card = new GameIntroCard()
                {
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.Card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    GameIntroAssociatedImageUrl = result.GameIntroAssociatedImageUrl,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.Card, languageId),
                    SubTopic = GetSubTopic(result.Card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }


        private IEnumerable<MatchingListQuestionCard> GetMatchingListQuestionCards(int gameId, int languageId,
                                                                                   IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MatchingListQuestionCard);
            var cardType = CardType.MatchingListQuestionCard.ToString();

            var results = (from c in DataContext.Cards
                          join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                  c.Internal_CardType.Internal_CardTypeShortCode ==
                                  cardShortCode
                          select new
                                     {
                                         Card = c,
                                         c.QuestionCard,
                                         GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                         //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                                         CardType = cardType,
                                         CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                         Id = c.CardID,
                                         //Title = GetCardTitle(c, languageId),
                                         CardSequenceId = gcs.GameCardSequenceID,
                                         IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                                         IsATimedCard = c.IsATimedCard,
                                         //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                         //Scoring = GetScoring(c.QuestionCard),
                                         UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                                         //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                         CardSequenceNo = gcs.CardSequenceNo,
                                         MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                                         ApplyScoreByCardElseForEachCorrectAnswer =
                              c.QuestionCard.ApplyScoreByCardElseForEachCorrectAnswer,
                                         QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                              c.QuestionCard.QuestionIsOnlyCorrectIfAllAnswersAreTrue,
                                         ApplyNegScoringForEachInCorrectAnswer =
                              c.QuestionCard.ApplyNegScoringForEachInCorrectAnswer,
                                         CardViewedInGame = gcs.CardViewedInGame,
                                         CardViewedInGameOn = gcs.CardViewedInGameOn,
                                         IsAMandatoryCard = gcs.IsAMandatoryCard,
                                         UseLongVersion = gcs.UseLongVersion,
                                         CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                         //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                                         //Topic = GetTopic(c, languageId),
                                         //SubTopic = GetSubTopic(c, languageId),
                                         AudioTranscriptFile = c.AudioTranscriptFile,
                                         AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                         AudioTranscriptText = c.AudioTranscriptText,
                                         QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                         QuestionMultimediaTypeImageVideoEtc =
                              c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                         QuestionMultimediaStartAutoElsePushPlayToStart =
                              c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                         QuestionMultimediaShowPlaybackControls =
                              c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                         AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                         AnswerMultimediaTypeImageVideoEtc =
                              c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                         AnswerMultimediaStartAutoElsePushPlayToStart =
                              c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                         AnswerMultimediaShowPlaybackControls =
                              c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                         c.QuestionCard.QuestionMultimediaThumbnailURL,
                                         c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                     }).ToList();

            var cards = new List<MatchingListQuestionCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MatchingListPlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.Card, avatarGames));
                var card = new MatchingListQuestionCard()
                               {
                                   AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.Card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   Scoring = GetScoring(result.QuestionCard),
                                   UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                                   PossibleAnswers = GetPossibleAnswers(result.Card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                                   ApplyScoreByCardElseForEachCorrectAnswer =
                                       result.ApplyScoreByCardElseForEachCorrectAnswer,
                                   QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                                       result.QuestionIsOnlyCorrectIfAllAnswersAreTrue,
                                   ApplyNegScoringForEachInCorrectAnswer = result.ApplyNegScoringForEachInCorrectAnswer,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Statistics = GetStatistics(result.Card).ToList(),
                                   Topic = GetTopic(result.Card, languageId),
                                   SubTopic = GetSubTopic(result.Card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<MultipleCheckBoxQuestionCard> GetMultipleCheckBoxQuestionCards(int gameId, int languageId,
                                                                                           IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxQuestionCard);
            var cardType = CardType.MultipleCheckBoxQuestionCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //Scoring = GetScoring(c.QuestionCard),
                                          UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                                          ApplyScoreByCardElseForEachCorrectAnswer =
                               c.QuestionCard.ApplyScoreByCardElseForEachCorrectAnswer,
                                          QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                               c.QuestionCard.QuestionIsOnlyCorrectIfAllAnswersAreTrue,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          ApplyNegScoringForEachInCorrectAnswer =
                               c.QuestionCard.ApplyNegScoringForEachInCorrectAnswer,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleCheckBoxQuestionCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleCheckBoxPlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));

                var card = new MultipleCheckBoxQuestionCard()
                               {
                                   AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   Scoring = GetScoring(result.QuestionCard),
                                   UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                                   ApplyScoreByCardElseForEachCorrectAnswer =
                                       result.ApplyScoreByCardElseForEachCorrectAnswer,
                                   QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                                       result.QuestionIsOnlyCorrectIfAllAnswersAreTrue,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   ApplyNegScoringForEachInCorrectAnswer = result.ApplyNegScoringForEachInCorrectAnswer,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }


        private IEnumerable<MultipleCheckBoxImageQuestionCard> GetMultipleCheckBoxImageQuestionCards(int gameId, int languageId,
                                                                                           IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxImageQuestionCard);
            var cardType = CardType.MultipleCheckBoxImageQuestionCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                           {
                               card = c,
                               c.QuestionCard,
                               GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                               //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                               CardType = cardType,
                               CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                               Id = c.CardID,
                               //Title = GetCardTitle(c, languageId),
                               CardSequenceId = gcs.GameCardSequenceID,
                               IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                               IsATimedCard = c.IsATimedCard,
                               //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                               //Scoring = GetScoring(c.QuestionCard),
                               UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                               //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                               CardSequenceNo = gcs.CardSequenceNo,
                               MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                               ApplyScoreByCardElseForEachCorrectAnswer =
                    c.QuestionCard.ApplyScoreByCardElseForEachCorrectAnswer,
                               QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                    c.QuestionCard.QuestionIsOnlyCorrectIfAllAnswersAreTrue,
                               OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                               ApplyNegScoringForEachInCorrectAnswer =
                    c.QuestionCard.ApplyNegScoringForEachInCorrectAnswer,
                               CardViewedInGame = gcs.CardViewedInGame,
                               CardViewedInGameOn = gcs.CardViewedInGameOn,
                               IsAMandatoryCard = gcs.IsAMandatoryCard,
                               UseLongVersion = gcs.UseLongVersion,
                               CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                               //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                               //Topic = GetTopic(c, languageId),
                               //SubTopic = GetSubTopic(c, languageId),
                               AudioTranscriptFile = c.AudioTranscriptFile,
                               AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                               AudioTranscriptText = c.AudioTranscriptText,
                               QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                               QuestionMultimediaTypeImageVideoEtc =
                    c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                               QuestionMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                               QuestionMultimediaShowPlaybackControls =
                    c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                               AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                               AnswerMultimediaTypeImageVideoEtc =
                    c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                               AnswerMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                               AnswerMultimediaShowPlaybackControls =
                    c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                               c.QuestionCard.QuestionMultimediaThumbnailURL,
                               c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                           }).ToList();

            var cards = new List<MultipleCheckBoxImageQuestionCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleCheckBoxPlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));

                var card = new MultipleCheckBoxImageQuestionCard()
                {
                    AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                    IsATimedCard = result.IsATimedCard,
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    Scoring = GetScoring(result.QuestionCard),
                    UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                    PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                    CardSequenceNo = result.CardSequenceNo,
                    MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                    ApplyScoreByCardElseForEachCorrectAnswer =
                        result.ApplyScoreByCardElseForEachCorrectAnswer,
                    QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                        result.QuestionIsOnlyCorrectIfAllAnswersAreTrue,
                    OfferOptOutChoice = result.OfferOptOutChoice,
                    ApplyNegScoringForEachInCorrectAnswer = result.ApplyNegScoringForEachInCorrectAnswer,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Results = answerResults.Cast<PlayerResult>().ToList(),
                    Topic = GetTopic(result.card, languageId),
                    SubTopic = GetSubTopic(result.card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<MultipleChoiceQuestionCard> GetMultipleChoiceQuestionCards(int gameId, int languageId,
                                                                                       IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoiceQuestionCard);
            var cardType = CardType.MultipleChoiceQuestionCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //Scoring = GetScoring(c.QuestionCard),
                                          UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleChoiceQuestionCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleChoicePlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));

                var card = new MultipleChoiceQuestionCard()
                               {
                                   AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   CardSequenceNo = result.CardSequenceNo,
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   IsATimedCard = result.IsATimedCard,
                                   MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Scoring = GetScoring(result.QuestionCard),
                                   UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                                   UseLongVersion = result.UseLongVersion,
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<MultipleChoiceImageQuestionCard> GetMultipleChoiceImageQuestionCards(int gameId, int languageId,
                                                                                      IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoiceImageQuestionCard);
            var cardType = CardType.MultipleChoiceImageQuestionCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                           {
                               card = c,
                               c.QuestionCard,
                               GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                               //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                               CardType = cardType,
                               CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                               Id = c.CardID,
                               //Title = GetCardTitle(c, languageId),
                               CardSequenceId = gcs.GameCardSequenceID,
                               IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                               IsATimedCard = c.IsATimedCard,
                               //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                               //Scoring = GetScoring(c.QuestionCard),
                               UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                               //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                               CardSequenceNo = gcs.CardSequenceNo,
                               MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                               OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                               CardViewedInGame = gcs.CardViewedInGame,
                               CardViewedInGameOn = gcs.CardViewedInGameOn,
                               IsAMandatoryCard = gcs.IsAMandatoryCard,
                               UseLongVersion = gcs.UseLongVersion,
                               CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                               //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                               //Topic = GetTopic(c, languageId),
                               //SubTopic = GetSubTopic(c, languageId),
                               AudioTranscriptFile = c.AudioTranscriptFile,
                               AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                               AudioTranscriptText = c.AudioTranscriptText,
                               QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                               QuestionMultimediaTypeImageVideoEtc =
                    c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                               QuestionMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                               QuestionMultimediaShowPlaybackControls =
                    c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                               AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                               AnswerMultimediaTypeImageVideoEtc =
                    c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                               AnswerMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                               AnswerMultimediaShowPlaybackControls =
                    c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                               c.QuestionCard.QuestionMultimediaThumbnailURL,
                               c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                           }).ToList();

            var cards = new List<MultipleChoiceImageQuestionCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleChoicePlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));

                var card = new MultipleChoiceImageQuestionCard()
                {
                    AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    CardSequenceNo = result.CardSequenceNo,
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    Id = result.Id,
                    Title = GetCardTitle(result.card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    IsATimedCard = result.IsATimedCard,
                    MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                    OfferOptOutChoice = result.OfferOptOutChoice,
                    PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    Results = answerResults.Cast<PlayerResult>().ToList(),
                    Scoring = GetScoring(result.QuestionCard),
                    UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                    UseLongVersion = result.UseLongVersion,
                    Topic = GetTopic(result.card, languageId),
                    SubTopic = GetSubTopic(result.card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<MultipleCheckBoxSurveyCard> GetMultipleCheckBoxSurveyCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxSurveyCard);
            var cardType = CardType.MultipleCheckBoxSurveyCard.ToString();
            
            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          OfferOtherOption = c.QuestionCard.OfferOTHERTextOption,
                                          OtherOptionIsTextBoxElseTextArea = !c.QuestionCard.TextOptionInputIsTextArea,
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleCheckBoxSurveyCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleCheckBoxPlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new MultipleCheckBoxSurveyCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   OfferOtherOption = result.OfferOtherOption,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }
            return cards;
        }

        private IEnumerable<MultipleCheckBoxPriorityFeedbackCard> GetMultipleCheckBoxPriorityFeedbackCards(int gameId,
                                                                                                           int
                                                                                                               languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxPriorityFeedbackCard);
            var cardType = CardType.MultipleCheckBoxPriorityFeedbackCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode == cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          OfferOtherOption = c.QuestionCard.OfferOTHERTextOption,
                                          OtherOptionIsTextBoxElseTextArea = !c.QuestionCard.TextOptionInputIsTextArea,
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleCheckBoxPriorityFeedbackCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<VotingCardFeedbackResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new MultipleCheckBoxPriorityFeedbackCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   OfferOtherOption = result.OfferOtherOption,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                card.IsFeedbackComplete = IsFeedbackCompleteForPriorityVotingInCardGroup(card.CardSequenceId,
                    card.Results.Count);

                cards.Add(card);
            }
            return cards;
        }


        private IEnumerable<MultipleCheckBoxPersonalActionPlanCard> GetMultipleCheckBoxPersonalActionPlanCards(
            int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleCheckboxPersonalActionPlanCard);
            var cardType = CardType.MultipleCheckboxPersonalActionPlanCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          OfferOtherOption = c.QuestionCard.OfferOTHERTextOption,
                                          OtherOptionIsTextBoxElseTextArea = !c.QuestionCard.TextOptionInputIsTextArea,
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleCheckBoxPersonalActionPlanCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleCheckBoxPlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new MultipleCheckBoxPersonalActionPlanCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   OfferOtherOption = result.OfferOtherOption,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }


        private IEnumerable<MultipleChoiceAuditStatementCard> GetMultipleChoiceAuditStatementCards(int gameId,
                                                                                                   int languageId,
                                                                                                    IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementCard);
            var cardType = CardType.MultipleChoiceAuditStatementCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          //AnswerExplain = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          //Statistics = GetStatistics(c),
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleChoiceAuditStatementCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleChoicePlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));

                var card = new MultipleChoiceAuditStatementCard()
                               {
                                   AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   Statistics = GetStatistics(result.card).ToList(),
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                card.TotalAnswerCount =
                    card.Statistics.Sum(s => ((Domain.Model.AuditStatementAnswerStatistic) s).AnswerCount);

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<MultipleChoiceAuditStatementImageCard> GetMultipleChoiceAuditStatementImageCards(int gameId,
                                                                                                  int languageId,
                                                                                                  IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementImageCard);
            var cardType = CardType.MultipleChoiceAuditStatementImageCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                           {
                               card = c,
                               c.QuestionCard,
                               GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                               //AnswerExplain = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                               CardType = cardType,
                               CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                               Id = c.CardID,
                               //Title = GetCardTitle(c, languageId),
                               CardSequenceId = gcs.GameCardSequenceID,
                               IsATimedCard = c.IsATimedCard,
                               //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                               //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                               CardSequenceNo = gcs.CardSequenceNo,
                               OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                               CardViewedInGame = gcs.CardViewedInGame,
                               CardViewedInGameOn = gcs.CardViewedInGameOn,
                               IsAMandatoryCard = gcs.IsAMandatoryCard,
                               UseLongVersion = gcs.UseLongVersion,
                               CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                               //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                               //Topic = GetTopic(c, languageId),
                               //SubTopic = GetSubTopic(c, languageId),
                               AudioTranscriptFile = c.AudioTranscriptFile,
                               AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                               AudioTranscriptText = c.AudioTranscriptText,
                               //Statistics = GetStatistics(c),
                               QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                               QuestionMultimediaTypeImageVideoEtc =
                    c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                               QuestionMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                               QuestionMultimediaShowPlaybackControls =
                    c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                               AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                               AnswerMultimediaTypeImageVideoEtc =
                    c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                               AnswerMultimediaStartAutoElsePushPlayToStart =
                    c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                               AnswerMultimediaShowPlaybackControls =
                    c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                               c.QuestionCard.QuestionMultimediaThumbnailURL,
                               c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                           }).ToList();

            var cards = new List<MultipleChoiceAuditStatementImageCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleChoicePlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));

                var card = new MultipleChoiceAuditStatementImageCard()
                {
                    AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                    CardSequenceNo = result.CardSequenceNo,
                    OfferOptOutChoice = result.OfferOptOutChoice,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Results = answerResults.Cast<PlayerResult>().ToList(),
                    Topic = GetTopic(result.card, languageId),
                    SubTopic = GetSubTopic(result.card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    Statistics = GetStatistics(result.card).ToList(),
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                card.TotalAnswerCount =
                    card.Statistics.Sum(s => ((Domain.Model.AuditStatementAnswerStatistic)s).AnswerCount);

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<MultipleChoiceSurveyCard> GetMultipleChoiceSurveyCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoiceSurveyCard);
            var cardType = CardType.MultipleChoiceSurveyCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          OfferOtherOption = c.QuestionCard.OfferOTHERTextOption,
                                          OtherOptionIsTextBoxElseTextArea = !c.QuestionCard.TextOptionInputIsTextArea,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleChoiceSurveyCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleChoicePlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new MultipleChoiceSurveyCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   OfferOtherOption = result.OfferOtherOption,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }


        private IEnumerable<MultipleChoicePriorityFeedbackCard> GetMultipleChoicePriorityFeedbackCards(int gameId,
                                                                                                       int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoicePriorityFeedbackCard);
            var cardType = CardType.MultipleChoicePriorityFeedbackCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          OfferOtherOption = c.QuestionCard.OfferOTHERTextOption,
                                          OtherOptionIsTextBoxElseTextArea = !c.QuestionCard.TextOptionInputIsTextArea,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleChoicePriorityFeedbackCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<VotingCardFeedbackResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new MultipleChoicePriorityFeedbackCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   OfferOtherOption = result.OfferOtherOption,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                card.IsFeedbackComplete = IsFeedbackCompleteForPriorityVotingInCardGroup(card.CardSequenceId,
                    card.Results.Count);

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<MultipleChoicePersonalActionPlanCard> GetMultipleChoicePersonalActionPlanCards(int gameId,
                                                                                                           int
                                                                                                               languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoicePersonalActionPlanCard);
            var cardType = CardType.MultipleChoicePersonalActionPlanCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard, 
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          OfferOtherOption = c.QuestionCard.OfferOTHERTextOption,
                                          OtherOptionIsTextBoxElseTextArea = !c.QuestionCard.TextOptionInputIsTextArea,
                                          OfferOptOutChoice = c.QuestionCard.OfferOptOutChoice,
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<MultipleChoicePersonalActionPlanCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<MultipleChoicePlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new MultipleChoicePersonalActionPlanCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   OfferOtherOption = result.OfferOtherOption,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   OfferOptOutChoice = result.OfferOptOutChoice,
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };
                cards.Add(card);
            }
            return cards;
        }


        private IEnumerable<NumericQuestionCard> GetNumericQuestionCards(int gameId, int languageId,
                                                                         IEnumerable<Game> avatarGames)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.NumericQuestionCard);
            var cardType = CardType.NumericQuestionCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard, 
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          //Scoring = GetScoring(c.QuestionCard),
                                          UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                                          ApplyScoreByCardElseForEachCorrectAnswer =
                               c.QuestionCard.ApplyScoreByCardElseForEachCorrectAnswer,
                                          QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                               c.QuestionCard.QuestionIsOnlyCorrectIfAllAnswersAreTrue,
                                          ApplyNegScoringForEachInCorrectAnswer =
                               c.QuestionCard.ApplyNegScoringForEachInCorrectAnswer,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetAllPlayerResults(gcs.GameCardGroupSequence.Game, c, avatarGames),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<NumericQuestionCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<NumericPlayerResult>(GetAllPlayerResults(result.GameCardGroupSequenceGame, result.card, avatarGames));

                var card = new NumericQuestionCard()
                               {
                                   AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = result.Title,
                                   CardSequenceId = result.CardSequenceId,
                                   IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   Scoring = GetScoring(result.QuestionCard),
                                   UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardSequenceNo = result.CardSequenceNo,
                                   MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                                   ApplyScoreByCardElseForEachCorrectAnswer =
                                       result.ApplyScoreByCardElseForEachCorrectAnswer,
                                   QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                                       result.QuestionIsOnlyCorrectIfAllAnswersAreTrue,
                                   ApplyNegScoringForEachInCorrectAnswer = result.ApplyNegScoringForEachInCorrectAnswer,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card,languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }


        private IEnumerable<PotLuckQuestionCard> GetPotLuckQuestionCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.PotLuckQuestionCard);
            var cardType = CardType.PotLuckQuestionCard.ToString();

            //TODO : Complete Pot luck question cards to return player results
            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                          select new 
                          {
                              card = c,
                              c.QuestionCard,
                              CardType = cardType,
                              CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                              Id = c.CardID,
                              //Title = GetCardTitle(c, languageId),
                              CardSequenceId = gcs.GameCardSequenceID,
                              IsATimedCard = c.IsATimedCard,
                              CardSequenceNo = gcs.CardSequenceNo,
                              //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                              //AnswerExplanation = GetAnswerExplanation(c.QuestionCard, languageId, gcs.UseLongVersion),
                              IgnoreAnyTimersOnThisCard = c.IgnoreAnyTimersOnThisCard,
                              MaximumTimeToAnswerInSeconds = c.CardMaxTimeToAnswerInSeconds,
                              //Scoring = GetScoring(c.QuestionCard),
                              UseCardTimeNotGameTime = c.UseCardTimeNotGameTime,
                              CardViewedInGame = gcs.CardViewedInGame,
                              CardViewedInGameOn = gcs.CardViewedInGameOn,
                              IsAMandatoryCard = gcs.IsAMandatoryCard,
                              UseLongVersion = gcs.UseLongVersion,
                              CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                              //Topic = GetTopic(c, languageId),
                              //SubTopic = GetSubTopic(c, languageId),
                              AudioTranscriptFile = c.AudioTranscriptFile,
                              AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                              AudioTranscriptText = c.AudioTranscriptText,
                              QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                              QuestionMultimediaTypeImageVideoEtc =
                   c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                              QuestionMultimediaStartAutoElsePushPlayToStart =
                   c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                              QuestionMultimediaShowPlaybackControls =
                   c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                              AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                              AnswerMultimediaTypeImageVideoEtc =
                   c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                              AnswerMultimediaStartAutoElsePushPlayToStart =
                   c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                              AnswerMultimediaShowPlaybackControls =
                   c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                              c.QuestionCard.QuestionMultimediaThumbnailURL,
                              c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                          }).ToList();

            var cards = new List<PotLuckQuestionCard>();

            foreach (var result in results)
            {
                var card = new PotLuckQuestionCard()
                {
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                    AnswerExplanation = GetAnswerExplanation(result.QuestionCard, languageId, result.UseLongVersion),
                    IgnoreAnyTimersOnThisCard = result.IgnoreAnyTimersOnThisCard,
                    MaximumTimeToAnswerInSeconds = result.MaximumTimeToAnswerInSeconds,
                    Scoring = GetScoring(result.QuestionCard),
                    UseCardTimeNotGameTime = result.UseCardTimeNotGameTime,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.card, languageId),
                    SubTopic = GetSubTopic(result.card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<TextAreaSurveyCard> GetTextAreaSurveyCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.TextAreaSurveyCard);
            var cardType = CardType.TextAreaSurveyCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                                 && c.QuestionCard.TextOptionInputIsTextArea

                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          OtherOptionIsTextBoxElseTextArea = c.QuestionCard.TextOptionInputIsTextArea,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<TextAreaSurveyCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<TextPlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new TextAreaSurveyCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   CardSequenceNo = result.CardSequenceNo,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }
            return cards;
        }

        private IEnumerable<TextAreaPriorityFeedbackCard> GetTextAreaPriorityFeedbackCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.TextAreaPriorityFeedbackCard);
            var cardType = CardType.TextAreaPriorityFeedbackCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          OtherOptionIsTextBoxElseTextArea = c.QuestionCard.TextOptionInputIsTextArea,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<TextAreaPriorityFeedbackCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<VotingCardFeedbackResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new TextAreaPriorityFeedbackCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   CardSequenceNo = result.CardSequenceNo,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                card.IsFeedbackComplete = IsFeedbackCompleteForPriorityVotingInCardGroup(card.CardSequenceId,
                    card.Results.Count);

                cards.Add(card);
            }
            return cards;
        }

        private IEnumerable<PriorityIssueVotingCard> GetPriorityIssueVotingCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.PriorityIssueVotingCard);
            var cardType = CardType.PriorityIssueVotingCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where
                               gcs.GameCardGroupSequence.GameID == gameId &&
                               c.Internal_CardType.Internal_CardTypeShortCode ==
                               cardShortCode
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard, 
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          SelectTopN = c.QuestionCard.TopNSelected,
                                          UseQuestionCardPrimaryTopics = c.QuestionCard.UsePrimaryTopics,
                                          //PossibleAnswers = GetPossibleAnswers(c, languageId, gcs.UseLongVersion).ToList(),
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<PriorityIssueVotingCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<VotingPlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new PriorityIssueVotingCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   CardSequenceNo = result.CardSequenceNo,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   SelectTopN = result.SelectTopN,
                                   UseQuestionCardPrimaryTopics = result.UseQuestionCardPrimaryTopics,
                                   PossibleAnswers = GetPossibleAnswers(result.card, languageId, result.UseLongVersion).ToList(),
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<TextBoxSurveyCard> GetTextBoxSurveyCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.TextBoxSurveyCard);
            var cardType = CardType.TextBoxSurveyCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                                 && !c.QuestionCard.TextOptionInputIsTextArea
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          OtherOptionIsTextBoxElseTextArea = c.QuestionCard.TextOptionInputIsTextArea,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<TextBoxSurveyCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<TextPlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new TextBoxSurveyCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   CardSequenceNo = result.CardSequenceNo,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };
                cards.Add(card);
            }
            return cards;
        }

        private IEnumerable<TextAreaPersonalActionPlanCard> GetTextAreaPersonalActionPlanCards(int gameId,
                                                                                               int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.TextAreaPersonalActionPlanCard);
            var cardType = CardType.TextAreaPersonalActionPlanCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                                 && c.QuestionCard.TextOptionInputIsTextArea

                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          OtherOptionIsTextBoxElseTextArea = c.QuestionCard.TextOptionInputIsTextArea,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<TextAreaPersonalActionPlanCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<TextPlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new TextAreaPersonalActionPlanCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   CardSequenceNo = result.CardSequenceNo,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);

            }
            return cards;
        }

        private IEnumerable<TextBoxPersonalActionPlanCard> GetTextBoxPersonalActionPlanCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.TextBoxPersonalActionPlanCard);
            var cardType = CardType.TextBoxPersonalActionPlanCard.ToString();

            var results = (from c in DataContext.Cards
                           join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                           where gcs.GameCardGroupSequence.GameID == gameId &&
                                 c.Internal_CardType.Internal_CardTypeShortCode ==
                                 cardShortCode
                                 && !c.QuestionCard.TextOptionInputIsTextArea
                           select new
                                      {
                                          card = c,
                                          c.QuestionCard,
                                          GameCardGroupSequenceGame = gcs.GameCardGroupSequence.Game,
                                          CardType = cardType,
                                          CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                          Id = c.CardID,
                                          //Title = GetCardTitle(c, languageId),
                                          CardSequenceId = gcs.GameCardSequenceID,
                                          IsATimedCard = c.IsATimedCard,
                                          CardSequenceNo = gcs.CardSequenceNo,
                                          //Question = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                          IsAMandatoryQuestion = gcs.IsAMandatoryCard,
                                          OtherOptionIsTextBoxElseTextArea = c.QuestionCard.TextOptionInputIsTextArea,
                                          CardViewedInGame = gcs.CardViewedInGame,
                                          CardViewedInGameOn = gcs.CardViewedInGameOn,
                                          IsAMandatoryCard = gcs.IsAMandatoryCard,
                                          UseLongVersion = gcs.UseLongVersion,
                                          CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                          //Results = GetCurrentPlayerResults(gcs.GameCardGroupSequence.Game, c),
                                          //Topic = GetTopic(c, languageId),
                                          //SubTopic = GetSubTopic(c, languageId),
                                          AudioTranscriptFile = c.AudioTranscriptFile,
                                          AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                          AudioTranscriptText = c.AudioTranscriptText,
                                          QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                          QuestionMultimediaTypeImageVideoEtc =
                               c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                          QuestionMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                          QuestionMultimediaShowPlaybackControls =
                               c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                          AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                          AnswerMultimediaTypeImageVideoEtc =
                               c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                          AnswerMultimediaStartAutoElsePushPlayToStart =
                               c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                          AnswerMultimediaShowPlaybackControls =
                               c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                          c.QuestionCard.QuestionMultimediaThumbnailURL,
                                          c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                                      }).ToList();

            var cards = new List<TextBoxPersonalActionPlanCard>();

            foreach (var result in results)
            {
                var answerResults = Deserialize<TextPlayerResult>(GetCurrentPlayerResults(result.GameCardGroupSequenceGame, result.card));

                var card = new TextBoxPersonalActionPlanCard()
                               {
                                   CardType = result.CardType,
                                   CardTypeGroup = result.CardTypeGroup,
                                   Id = result.Id,
                                   Title = GetCardTitle(result.card, languageId),
                                   CardSequenceId = result.CardSequenceId,
                                   IsATimedCard = result.IsATimedCard,
                                   CardSequenceNo = result.CardSequenceNo,
                                   Question = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion),
                                   IsAMandatoryQuestion = result.IsAMandatoryQuestion,
                                   OtherOptionIsTextBoxElseTextArea = result.OtherOptionIsTextBoxElseTextArea,
                                   CardViewedInGame = result.CardViewedInGame,
                                   CardViewedInGameOn = result.CardViewedInGameOn,
                                   IsAMandatoryCard = result.IsAMandatoryCard,
                                   UseLongVersion = result.UseLongVersion,
                                   CardGroupSequenceId = result.CardGroupSequenceId,
                                   Results = answerResults.Cast<PlayerResult>().ToList(),
                                   Topic = GetTopic(result.card, languageId),
                                   SubTopic = GetSubTopic(result.card, languageId),
                                   AudioTranscriptFile = result.AudioTranscriptFile,
                                   AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                   AudioTranscriptText = result.AudioTranscriptText,
                                   QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                   QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                   QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                   QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                   AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                   AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                   AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                   AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                   QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                   AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                               };

                cards.Add(card);
            }
            return cards;
        }

        private IEnumerable<ScoreboardCard> GetScoreboardCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.ScoreboardCard);
            var cardType = CardType.ScoreboardCard.ToString();

            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                              {
                                  card = c,
                                  CardType = cardType,
                                  CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                  Id = c.CardID,
                                  //Title = GetCardTitle(c, languageId),
                                  CardSequenceId = gcs.GameCardSequenceID,
                                  IsATimedCard = c.IsATimedCard,
                                  CardSequenceNo = gcs.CardSequenceNo,
                                  CardViewedInGame = gcs.CardViewedInGame,
                                  CardViewedInGameOn = gcs.CardViewedInGameOn,
                                  IsAMandatoryCard = gcs.IsAMandatoryCard,
                                  UseLongVersion = gcs.UseLongVersion,
                                  CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                  //Topic = GetTopic(c, languageId),
                                  //SubTopic = GetSubTopic(c, languageId),
                                  AudioTranscriptFile = c.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = c.AudioTranscriptText,
                                  QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                  QuestionMultimediaTypeImageVideoEtc =
                       c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls =
                       c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                  AnswerMultimediaTypeImageVideoEtc =
                       c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls =
                       c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                  c.QuestionCard.QuestionMultimediaThumbnailURL,
                                  c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                              }).ToList();

            var cards = new List<ScoreboardCard>();

            foreach (var result in results)
            {
                var card = new ScoreboardCard()
                              {
                                  CardType = result.CardType,
                                  CardTypeGroup = result.CardTypeGroup,
                                  Id = result.Id,
                                  Title = GetCardTitle(result.card, languageId),
                                  CardSequenceId = result.CardSequenceId,
                                  IsATimedCard = result.IsATimedCard,
                                  CardSequenceNo = result.CardSequenceNo,
                                  CardViewedInGame = result.CardViewedInGame,
                                  CardViewedInGameOn = result.CardViewedInGameOn,
                                  IsAMandatoryCard = result.IsAMandatoryCard,
                                  UseLongVersion = result.UseLongVersion,
                                  CardGroupSequenceId = result.CardGroupSequenceId,
                                  Topic = GetTopic(result.card, languageId),
                                  SubTopic = GetSubTopic(result.card, languageId),
                                  AudioTranscriptFile = result.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = result.AudioTranscriptText,
                                  QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                                  QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                                  AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                                  QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                                  AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                              };

                cards.Add(card);
            }

            return cards;
        }


        private IEnumerable<RoundResultCard> GetRoundResultCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.RoundResultCard);
            var cardType = CardType.RoundResultCard.ToString();

            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                              {
                                  card = c,
                                  CardType = cardType,
                                  CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                  Id = c.CardID,
                                  //Title = GetCardTitle(c, languageId),
                                  CardSequenceId = gcs.GameCardSequenceID,
                                  IsATimedCard = c.IsATimedCard,
                                  CardSequenceNo = gcs.CardSequenceNo,
                                  CardViewedInGame = gcs.CardViewedInGame,
                                  CardViewedInGameOn = gcs.CardViewedInGameOn,
                                  IsAMandatoryCard = gcs.IsAMandatoryCard,
                                  UseLongVersion = gcs.UseLongVersion,
                                  CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                  //Topic = GetTopic(c, languageId),
                                  //SubTopic = GetSubTopic(c, languageId),
                                  AudioTranscriptFile = c.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = c.AudioTranscriptText,
                                  QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                  QuestionMultimediaTypeImageVideoEtc =
                       c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls =
                       c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                  AnswerMultimediaTypeImageVideoEtc =
                       c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls =
                       c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                  c.QuestionCard.QuestionMultimediaThumbnailURL,
                                  c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                              }).ToList();

            var cards = new List<RoundResultCard>();


            foreach (var result in results)
            {
                var card = new RoundResultCard()
                {
                    CardType = cardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.card, languageId),
                    SubTopic = GetSubTopic(result.card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }


        private IEnumerable<CertificateCard> GetCertificateCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.CertificateCard);
            var cardType = CardType.CertificateCard.ToString();

            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new
                              {
                                  card = c,
                                  CardType = cardType,
                                  CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                  Id = c.CardID,
                                  //Title = GetCardTitle(c, languageId),
                                  CardSequenceId = gcs.GameCardSequenceID,
                                  IsATimedCard = c.IsATimedCard,
                                  CardSequenceNo = gcs.CardSequenceNo,
                                  CardViewedInGame = gcs.CardViewedInGame,
                                  CardViewedInGameOn = gcs.CardViewedInGameOn,
                                  IsAMandatoryCard = gcs.IsAMandatoryCard,
                                  UseLongVersion = gcs.UseLongVersion,
                                  CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                  //Topic = GetTopic(c, languageId),
                                  //SubTopic = GetSubTopic(c, languageId),
                                  AudioTranscriptFile = c.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = c.AudioTranscriptText,
                                  QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                  QuestionMultimediaTypeImageVideoEtc =
                       c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls =
                       c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                  AnswerMultimediaTypeImageVideoEtc =
                       c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls =
                       c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                  c.QuestionCard.QuestionMultimediaThumbnailURL,
                                  c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                              }).ToList();

            var cards = new List<CertificateCard>();

            foreach (var result in results)
            {
                var card = new CertificateCard()
                {
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.card, languageId),
                    SubTopic = GetSubTopic(result.card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }


            return cards;
        }

        private IEnumerable<AwardCard> GetAwardCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.AwardCard);
            var cardType = CardType.AwardCard.ToString();

            var results = (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                              {
                                  card = c,
                                  CardType = cardType,
                                  CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                  Id = c.CardID,
                                  //Title = GetCardTitle(c, languageId),
                                  CardSequenceId = gcs.GameCardSequenceID,
                                  IsATimedCard = c.IsATimedCard,
                                  CardSequenceNo = gcs.CardSequenceNo,
                                  CardViewedInGame = gcs.CardViewedInGame,
                                  CardViewedInGameOn = gcs.CardViewedInGameOn,
                                  IsAMandatoryCard = gcs.IsAMandatoryCard,
                                  UseLongVersion = gcs.UseLongVersion,
                                  CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                  //Topic = GetTopic(c, languageId),
                                  //SubTopic = GetSubTopic(c, languageId),
                                  AudioTranscriptFile = c.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = c.AudioTranscriptText,
                                  QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                  QuestionMultimediaTypeImageVideoEtc =
                       c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls =
                       c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                  AnswerMultimediaTypeImageVideoEtc =
                       c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls =
                       c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                  c.QuestionCard.QuestionMultimediaThumbnailURL,
                                  c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                              }).ToList();

            var cards = new List<AwardCard>();

            foreach (var result in results)
            {
                var card = new AwardCard()
                {
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.card, languageId),
                    SubTopic = GetSubTopic(result.card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }

        private IEnumerable<LcdCard> GetLcdCategoryCards(int gameId, int languageId)
        {
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.LcdCategoryCard);
            var cardType = CardType.LcdCategoryCard.ToString();

            var results =  (from c in DataContext.Cards
                   join gcs in DataContext.GameCardSequences on c.CardID equals gcs.CardID
                   where gcs.GameCardGroupSequence.GameID == gameId &&
                         c.Internal_CardType.Internal_CardTypeShortCode ==
                         cardShortCode
                   select new 
                              {
                                  card = c,
                                  c.QuestionCard,
                                  CardType = cardType,
                                  CardTypeGroup = c.Internal_CardType.Internal_CardTypeGroup,
                                  Id = c.CardID,
                                  //Title = GetCardTitle(c, languageId),
                                  CardSequenceId = gcs.GameCardSequenceID,
                                  IsATimedCard = c.IsATimedCard,
                                  CardSequenceNo = gcs.CardSequenceNo,
                                  CardViewedInGame = gcs.CardViewedInGame,
                                  CardViewedInGameOn = gcs.CardViewedInGameOn,
                                  IsAMandatoryCard = gcs.IsAMandatoryCard,
                                  UseLongVersion = gcs.UseLongVersion,
                                  LcdAssociatedImageUrl = c.QuestionCard.AssociatedImageUrl,
                                  //LcdCategory = GetQuestion(c.QuestionCard, languageId, gcs.UseLongVersion),
                                  CardGroupSequenceId = gcs.GameCardGroupSequenceID,
                                  //Topic = GetTopic(c, languageId),
                                  //SubTopic = GetSubTopic(c, languageId),
                                  AudioTranscriptFile = c.AudioTranscriptFile,
                                  AudioTranscriptFriendlyName = c.AudioTranscriptFriendlyName,
                                  AudioTranscriptText = c.AudioTranscriptText,
                                  QuestionMultimediaUrl = c.QuestionCard.QuestionMultimediaURL,
                                  QuestionMultimediaTypeImageVideoEtc =
                       c.QuestionCard.QuestionMultimediaType_ImageVideoetc,
                                  QuestionMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                                  QuestionMultimediaShowPlaybackControls =
                       c.QuestionCard.QuestionMultimediaShowPlaybackControls,
                                  AnswerMultimediaUrl = c.QuestionCard.AnswerExplanationMultimediaURL,
                                  AnswerMultimediaTypeImageVideoEtc =
                       c.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                                  AnswerMultimediaStartAutoElsePushPlayToStart =
                       c.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                                  AnswerMultimediaShowPlaybackControls =
                       c.QuestionCard.AnswerMultimediaShowPlaybackControls,
                                  c.QuestionCard.QuestionMultimediaThumbnailURL,
                                  c.QuestionCard.AnswerExplanationMultimediaThumbnailURL
                              }).ToList();


            var cards = new List<LcdCard>();

            foreach (var result in results)
            {
                var card = new LcdCard()
                {
                    CardType = result.CardType,
                    CardTypeGroup = result.CardTypeGroup,
                    Id = result.Id,
                    Title = GetCardTitle(result.card, languageId),
                    CardSequenceId = result.CardSequenceId,
                    IsATimedCard = result.IsATimedCard,
                    CardSequenceNo = result.CardSequenceNo,
                    CardViewedInGame = result.CardViewedInGame,
                    CardViewedInGameOn = result.CardViewedInGameOn,
                    IsAMandatoryCard = result.IsAMandatoryCard,
                    UseLongVersion = result.UseLongVersion,
                    LcdAssociatedImageUrl = result.LcdAssociatedImageUrl,
                    LcdCategory = GetQuestion(result.QuestionCard, languageId, result.UseLongVersion).QuestionStatement,
                    CardGroupSequenceId = result.CardGroupSequenceId,
                    Topic = GetTopic(result.card, languageId),
                    SubTopic = GetSubTopic(result.card, languageId),
                    AudioTranscriptFile = result.AudioTranscriptFile,
                    AudioTranscriptFriendlyName = result.AudioTranscriptFriendlyName,
                    AudioTranscriptText = result.AudioTranscriptText,
                    QuestionMultimediaUrl = result.QuestionMultimediaUrl,
                    QuestionMultimediaTypeImageVideoEtc = result.QuestionMultimediaTypeImageVideoEtc,
                    QuestionMultimediaStartAutoElsePushPlayToStart = result.QuestionMultimediaStartAutoElsePushPlayToStart,
                    QuestionMultimediaShowPlaybackControls = result.QuestionMultimediaShowPlaybackControls,
                    AnswerMultimediaUrl = result.AnswerMultimediaUrl,
                    AnswerMultimediaTypeImageVideoEtc = result.AnswerMultimediaTypeImageVideoEtc,
                    AnswerMultimediaStartAutoElsePushPlayToStart = result.AnswerMultimediaStartAutoElsePushPlayToStart,
                    AnswerMultimediaShowPlaybackControls = result.AnswerMultimediaShowPlaybackControls,
                    QuestionMultimediaThumbnailUrl = result.QuestionMultimediaThumbnailURL,
                    AnswerExplanationMultimediaThumbnailUrl = result.AnswerExplanationMultimediaThumbnailURL
                };

                cards.Add(card);
            }

            return cards;
        }

        #region Helper Methods       

        private OnlineScore GetScoring(QuestionCard questionCard)
        {
            return new OnlineScore()
                       {
                           OnlinePlayerLosing = questionCard.OnlineLosingScore,
                           OnlinePlayerWinning = questionCard.OnlineWinningScore
                       };
        }

        private List<BranchingCardButton> GetBranchingCardButtons(DataModel.SqlRepository.BranchingCard branchingCard, int languageId, bool useLongVersion)
        {
            var items = new List<BranchingCardButton>();

            foreach (var branchingCardButton in branchingCard.BranchingCard_Buttons)
            {
                var branchingCardButtonLanguage =
                    branchingCardButton.BranchingCard_ButtonLanguage.FirstOrDefault(b => b.LanguageID == languageId);
                
                if (branchingCardButtonLanguage != null)
                {
                    var button = new BranchingCardButton()
                                 {
                                     Position = branchingCardButton.Position,
                                     ButtonText = branchingCardButtonLanguage.ButtonText,
                                     ButtonImageFriendlyName = branchingCardButtonLanguage.ButtonImageFriendlyName,
                                     ButtonImageMultimediaURL = branchingCardButtonLanguage.ButtonImageMultimediaURL,
                                     LanguageId = languageId,
                                     ButtonPressedCardGroupId = branchingCardButton.CardGroupID_Result,
                                     IsMultiClick = branchingCardButton.IsMultiClick,
                                     MustReturnToBranchingCard = branchingCardButton.MustReturnToBranchingCard
                                 };

                    items.Add(button);
                }
            }

            return items;
        }

        private QuestionExplain GetQuestion(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var questionLang = questionCard.QuestionCard_Languages.FirstOrDefault(q => q.LanguageID == languageId);

            if (questionLang == null)
            {
                return null;
            }

            return new QuestionExplain()
                       {
                           QuestionMultimediaShowPlaybackControls = questionCard.QuestionMultimediaShowPlaybackControls,
                           QuestionMultimediaStartAutoElsePushPlayToStart =
                               questionCard.QuestionMultimediaStartAutoElsePushPlayToStart,
                           QuestionMultimediaTypeImageVideoEtc =
                               questionCard.QuestionMultimediaType_ImageVideoetc,
                           QuestionMultimediaUrl = questionCard.QuestionMultimediaURL,
                           QuestionStatement =
                               useLongVersion
                                   ? questionLang.Question_Statement_LongVersion
                                   : questionLang.Question_Statement_ShortVersion,
                           MustShowCompanyResults = questionCard.MustShowCompanyResults,
                           QuestionStatementPosition = questionLang.StatementLocation
                       };
        }

        private AnswerExplain GetAnswerExplanation(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var ansLang = questionCard.QuestionCard_Languages.FirstOrDefault(q => q.LanguageID == languageId);

            if (ansLang == null)
            {
                return null;
            }

            return new AnswerExplain()
                       {
                           AnswerExplanation =
                               useLongVersion
                                   ? ansLang.AnswerExplanation_LongVersion
                                   : ansLang.AnswerExplanation_ShortVersion,
                           AnswerMultimediaShowPlaybackControls = questionCard.AnswerMultimediaShowPlaybackControls,
                           AnswerMultimediaStartAutoElsePushPlayToStart =
                               questionCard.AnswerMultimediaStartAutoElsePushPlayToStart,
                           AnswerMultimediaTypeImageVideoEtc =
                               questionCard.AnswerExplanationMultimediaType_ImageVideoetc,
                           AnswerMultimediaUrl = questionCard.AnswerExplanationMultimediaURL
                       };
        }

        private string GetThemeDescription(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var cardLang = questionCard.QuestionCard_Languages.FirstOrDefault(q => q.LanguageID == languageId);

            if (cardLang == null)
            {
                return null;
            }

            return useLongVersion ? cardLang.Question_Statement_LongVersion : cardLang.Question_Statement_ShortVersion;
        }

        private string GetTopic(Card card, int languageId)
        {
            var subTopic = (from c in card.Card2CardClassifications
                            from g in c.CardClassificationGroup.CardClassificationGroup_Languages
                            where
                                g.LanguageID == languageId && c.IsAPrimaryCard &&
                                !c.CardClassificationGroup.IsAMetaTagClassificationGroup
                            select g.CardClassificationGroup1).FirstOrDefault();
            if (subTopic == null)
            {
                return null;
            }

            var result = subTopic.CardClassificationGroup1.CardClassificationGroup_Languages
                .FirstOrDefault(p => p.LanguageID == languageId);

            return result.CardClassificationGroup;
        }

        private string GetSubTopic(Card card, int languageId)
        {
            var result = from c in card.Card2CardClassifications
                         from g in c.CardClassificationGroup.CardClassificationGroup_Languages
                         where
                             g.LanguageID == languageId && c.IsAPrimaryCard &&
                             !c.CardClassificationGroup.IsAMetaTagClassificationGroup
                         select g.CardClassificationGroup;

            return result.FirstOrDefault();
        }

        private string GetCardTitle(Card card, int languageId)
        {
            var result = from c in card.Card_Languages
                         where c.LanguageID == languageId
                         select c.CardTitle;

            return result.FirstOrDefault();
        }      
 
        private string GetStatementHeader(Card card, int languageId)
        {
            var result = from c in card.QuestionCard.QuestionCard_Languages
                         where c.LanguageID == languageId
                         select c.StatementHeader;

            return result.FirstOrDefault();
        }

        private List<QuestionCardStatement> GetCardStatements(Card card, int languageId, bool useLongVersion)
        {
            var results = from c in card.QuestionCard.QuestionCard_Languages
                          join s in card.QuestionCard.QuestionCardStatement_Languages on c.QuestionCardID equals
                              s.QuestionCardID
                          where c.LanguageID == languageId
                          let statement = useLongVersion ? s.Statement_LongVersion : s.Statement_ShortVersion
                          select new QuestionCardStatement()
                                     {
                                         QuestionCardId = s.QuestionCardID,
                                         QuestionCardStatementId = s.QuestionCardStatement_LanguageID,
                                         SequenceNo = s.SequenceNo,
                                         Statement = statement
                                     };

            return results.ToList();
        }

        private bool IsFeedbackCompleteForPriorityVotingInCardGroup(int gameCardSequenceId, int resultCount)
        {
            var cardGroupSequenceId =
                DataContext.GameCardSequences.Where(s => s.GameCardSequenceID == gameCardSequenceId)
                    .Select(s => s.GameCardGroupSequenceID).FirstOrDefault();

            var cardGroup =
                DataContext.GameCardGroupSequences.FirstOrDefault(g => g.GameCardGroupSequenceID == cardGroupSequenceId);

            if (cardGroup == null)
            {
                return false;
            }

            var votingCardType = Enum.GetName(typeof (CardType), CardType.PriorityIssueVotingCard);

            var votingCard =
                cardGroup.GameCardSequences.Where(
                    s => s.Card.Internal_CardType.Internal_CardTypeShortCode == votingCardType)
                    .Select(s => s.Card).FirstOrDefault();

            if (votingCard == null)
            {
                return false;
            }

            return votingCard.QuestionCard.TopNSelected == resultCount;
        }

        private static List<T> Deserialize<T>(List<XElement> elements)
        {
            var results = new List<T>();

            foreach (var element in elements)
            {
                var result = Deserialize<T>(element);
                results.Add(result);
            }

            return results;
        }

        private IEnumerable<DataModel.SqlRepository.Game> GetAvatarGames(List<int> avatarGameIds)
        {
            var avatarGames = from g in DataContext.Games
                              where avatarGameIds.Contains(g.GameID)
                              select g;
            return avatarGames;
        }

        private static T Deserialize<T>(XElement element)
        {

            var serializer = new DataContractSerializer(typeof(T));
            return (T)serializer.ReadObject(element.CreateReader());
        }

        #endregion

    }
}
