//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diversity.DataModel.SqlRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Game
    {
        public Game()
        {
            this.Game1 = new HashSet<Game>();
            this.GameInvite = new HashSet<GameInvite>();
            this.GamePlayers = new HashSet<GamePlayer>();
            this.GameCardGroupSequences = new HashSet<GameCardGroupSequence>();
            this.Results = new HashSet<Result>();
            this.BranchingCard = new HashSet<BranchingCard>();
        }
    
        public int GameID { get; set; }
        public System.Guid UserID_CreatedBy { get; set; }
        public System.Guid UserID_LastModifiedBy { get; set; }
        public System.DateTime CreatedOnDateTime { get; set; }
        public System.DateTime LastModifiedOnDateTime { get; set; }
        public bool IsADeletedRow { get; set; }
        public bool IsAnAuditRow { get; set; }
        public Nullable<int> GameID_Template { get; set; }
        public bool IsOnlineGame { get; set; }
        public bool IsAGameTemplate { get; set; }
        public bool GameNowLockedDown { get; set; }
        public bool GameConfigCanBeModifiedAcrossInstances { get; set; }
        public bool IsTimedGame { get; set; }
        public bool UseCardTime { get; set; }
        public bool UseActualAvatarNames { get; set; }
        public bool UseHistoricAvatarAnswers { get; set; }
        public bool UseActualAvatarAnsweringTimes { get; set; }
        public Nullable<System.DateTime> AllUsersMustFinishThisGameByThisDate { get; set; }
        public bool PlayerTurn_RandomNoRepeatingUntilAllHavePlayed { get; set; }
        public bool PlayerTurn_ByPlayerSequenceNoElseRandom { get; set; }
        public Nullable<int> DefaultGameTimeInSeconds { get; set; }
        public Nullable<System.DateTime> GameCompletionDate { get; set; }
        public Nullable<System.DateTime> GameStartDate { get; set; }
        public string GameThemeData { get; set; }
        public bool CanSkipTutorial { get; set; }
        public string GameTemplateNameForDashboard { get; set; }
        public bool HasBeenDownloaded { get; set; }
        public Nullable<int> NumberOfTimesRun { get; set; }
        public int ProductID { get; set; }
        public Nullable<int> NumberOfTimesGameCanBePlayed { get; set; }
        public string ExternalID { get; set; }
        public string ThemeClientUserGroupExternalID { get; set; }
    
        public virtual User aspnet_Users { get; set; }
        public virtual User aspnet_Users1 { get; set; }
        public virtual ICollection<Game> Game1 { get; set; }
        public virtual Game Game2 { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<GameInvite> GameInvite { get; set; }
        public virtual ICollection<GamePlayer> GamePlayers { get; set; }
        public virtual ICollection<GameCardGroupSequence> GameCardGroupSequences { get; set; }
        public virtual ICollection<Result> Results { get; set; }
        public virtual ICollection<BranchingCard> BranchingCard { get; set; }
    }
}
