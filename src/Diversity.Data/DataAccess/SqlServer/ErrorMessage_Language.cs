//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diversity.DataModel.SqlRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class ErrorMessage_Language
    {
        public int ErrorMessage_LanguageID { get; set; }
        public int ErrorMessageID { get; set; }
        public int LanguageID { get; set; }
        public string ErrorMessageDescription { get; set; }
    
        public virtual ErrorMessage ErrorMessage { get; set; }
        public virtual Language Language { get; set; }
    }
}
