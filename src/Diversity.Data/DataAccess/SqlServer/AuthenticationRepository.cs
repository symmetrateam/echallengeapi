﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using Diversity.Common;
using Diversity.Common.Constants;
using Diversity.DataModel.SqlRepository;
using Diversity.Domain.Model;

namespace Diversity.Data.DataAccess.SqlServer
{
    public class AuthenticationRepository : SqlRepository, IAuthenticationRepository
    {
        public AuthenticationTicket GetAuthenticationTicket(Guid userId)
        {
            var session = DataContext.Sessions.FirstOrDefault(s => s.UserID == userId);

            if(session == null)
            {
                return null;
            }

            var user = DataContext
                .Users
                .FirstOrDefault(u => u.UserId == userId);

            var clientTheme = DataContext.User2Clients
                .Where(c => c.UserID == userId && c.Client.Theme != null)
                .Select(c => new
                             {
                                 c.ClientID,
                                 Theme = c.Client.ThemeID.HasValue ? c.Client.Theme : null,
                             })
                .FirstOrDefault();
            
                

            var requiresFirstLoginDetails = string.IsNullOrWhiteSpace(user.UserLastName) ||
                                            string.IsNullOrWhiteSpace(user.UserLastName) ||
                                            string.IsNullOrWhiteSpace(user.PlayerAvatarData) ||
                                            string.IsNullOrWhiteSpace(user.PlayerAvatarName);



            return new AuthenticationTicket()
                       {
                           ExpiresAt = session.ExpiresAt,
                           Token = session.AuthenticationToken,
                           UserId = session.UserID,
                           IsFirstLogin = requiresFirstLoginDetails,
                           ThemeKey = clientTheme == null ? null : clientTheme.Theme.ThemeCode
                       };
        }

        public bool CreateAuthenticationTicket(AuthenticationTicket ticket)
        {
            var result = false;

            using(var dataContext = new EChallengeDataContext(ticket.UserId))
            {
                using (var transaction = DataContext.Database.BeginTransaction())
                {
                    try
                    {
                        var session = new DataModel.SqlRepository.Session()
                        {
                            AuthenticationToken = ticket.Token,
                            ExpiresAt = ticket.ExpiresAt,
                            UserID = ticket.UserId
                        };
                        dataContext.Sessions.Add(session);

                        dataContext.SaveChanges();
                        transaction.Commit();
                        result = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return result;
        }

        public bool UpdateAuthenticationTicket(AuthenticationTicket ticket)
        {
            using (var dataContext = new EChallengeDataContext(ticket.UserId))
            {
                var storedSession = dataContext.Sessions.FirstOrDefault(s => s.UserID == ticket.UserId);

                if (storedSession != null)
                {

                    using (var transaction = DataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            storedSession.AuthenticationToken = ticket.Token;
                            storedSession.ExpiresAt = ticket.ExpiresAt;   

                            dataContext.SaveChanges();
                            transaction.Commit();

                            return true;
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }                    
                }
            }

            return false;   
        }


        public AuthenticationTicket GetAuthenticationTicket(string authenticationToken)
        {
            var session = DataContext.Sessions.FirstOrDefault(s => s.AuthenticationToken == authenticationToken);

            if (session == null)
            {
                return null;
            }

            var user = DataContext
                .Users
                .FirstOrDefault(u => u.UserId == session.UserID);

            var clientTheme = DataContext.User2Clients
                .Where(c => c.UserID == session.UserID && c.Client.Theme != null)
                .Select(c => new
                             {
                                 c.ClientID,
                                 Theme = c.Client.ThemeID.HasValue ? c.Client.Theme : null,
                             })
                .FirstOrDefault();
            

            var requiresFirstLoginDetails = string.IsNullOrWhiteSpace(user.UserLastName) ||
                                            string.IsNullOrWhiteSpace(user.UserLastName) ||
                                            string.IsNullOrWhiteSpace(user.PlayerAvatarData) ||
                                            string.IsNullOrWhiteSpace(user.PlayerAvatarName);

            return new AuthenticationTicket()
            {
                ExpiresAt = session.ExpiresAt,
                Token = session.AuthenticationToken,
                UserId = session.UserID,
                IsFirstLogin = requiresFirstLoginDetails,
                ThemeKey = clientTheme == null ? null : clientTheme.Theme.ThemeCode
            };
        }

        public Guid GetScormUserId(string username, string clientCode)
        {
            var loweredUserName = username.ToLower();
            var loweredClientCode = clientCode.ToLower();

            var user = DataContext.Users
                .Where(c => c.ScormUserID.ToLower() == loweredUserName)
                .SelectMany(c => c.Clients)
                .Where(c => c.Client.ExternalID.ToLower() == loweredClientCode)
                .Select(c => c.UserID)
                .FirstOrDefault();

            return user;
        }

        public bool ClientExists(string clientCode)
        {
            var loweredClientCode = clientCode.ToLower();
            return DataContext.Clients.Any(c => c.ExternalID.ToLower() == loweredClientCode && !c.IsADeletedRow);
        }

        public bool AddUserToAllUsersClientUserGroup(Guid userId, string clientCode)
        {
            var added = false;
            var loweredClientCode = clientCode.ToLower();
            var loweredAllUsersUserGroupName = SecurityGroupResources.ClientUserGroupNameAllUsers;

            var clientUserGroup =
                DataContext
                    .ClientUserGroups
                    .FirstOrDefault(
                        c =>
                            c.Client.ExternalID.ToLower() == loweredClientCode && !c.IsADeletedRow &&
                            !c.Client.IsADeletedRow && c.ClientUserGroupName.ToLower() == loweredAllUsersUserGroupName);

            var loweredAdminAccountUserName = ConfigurationHelper.AdminAccountName.ToLower();
            var adminUser = DataContext.Users.First(c => c.LoweredUserName == loweredAdminAccountUserName);
            
            
            if (DataContext.Users.Any(c => c.UserId == userId && !c.IsADeletedRow) && clientUserGroup != null
                &&
                !DataContext.User2ClientUserGroups.Any(
                    c => c.ClientUserGroup.ClientID == clientUserGroup.ClientID && c.UserID == userId && !c.IsADeletedRow && 
                        c.ClientUserGroup.ClientUserGroupName.ToLower() == loweredAllUsersUserGroupName))
            {
                using (var dataContext = new EChallengeDataContext(adminUser.UserId))
                {
                    using (var transaction = DataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var createdOn = DateTime.Now;
                            var mapping =
                                new User2ClientUserGroup()
                                {
                                    CreatedOnDateTime = createdOn,
                                    LastModifiedOnDateTime = createdOn,
                                    UserID_CreatedBy = adminUser.UserId,
                                    ClientUserGroupID = clientUserGroup.ClientUserGroupID,
                                    UserID = userId,
                                    UserID_LastModifiedBy = adminUser.UserId                                    
                                };

                            dataContext.User2ClientUserGroups.Add(mapping);
                            dataContext.SaveChanges();
                            transaction.Commit();
                            added = true;
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }

            return added;
        }

        public bool AddUserToClient(Guid userId, string clientCode)
        {
            var added = false;
            var loweredClientCode = clientCode.ToLower();
            
            var client =
                DataContext.Clients.FirstOrDefault(c => c.ExternalID.ToLower() == loweredClientCode && !c.IsADeletedRow);

            var loweredAdminAccountUserName = ConfigurationHelper.AdminAccountName.ToLower();
            var adminUser = DataContext.Users.First(c => c.LoweredUserName == loweredAdminAccountUserName);
            if (DataContext.Users.Any(c => c.UserId == userId && !c.IsADeletedRow) && client != null
                &&
                !DataContext.User2Clients.Any(
                    c => c.ClientID == client.ClientID && c.UserID == userId && !c.IsADeletedRow))
            {
                using (var dataContext = new EChallengeDataContext(adminUser.UserId))
                {
                    using (var transaction = DataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var createdOn = DateTime.Now;
                            var mapping =
                                new User2Client()
                                {
                                    CreatedOnDateTime = createdOn,
                                    LastModifiedDateTime = createdOn,
                                    UserID_CreatedBy = adminUser.UserId,
                                    ClientID = client.ClientID,
                                    UserID = userId,
                                    UserID_LastModifiedBy = adminUser.UserId
                                };

                            dataContext.User2Clients.Add(mapping);
                            dataContext.SaveChanges();
                            transaction.Commit();
                            added = true;
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }

            return added;
        }
    }
}
