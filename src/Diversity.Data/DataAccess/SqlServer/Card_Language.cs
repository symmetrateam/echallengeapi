//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diversity.DataModel.SqlRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Card_Language
    {
        public int Card_LanguageID { get; set; }
        public System.Guid UserID_CreatedBy { get; set; }
        public System.Guid UserID_LastModifiedBy { get; set; }
        public System.DateTime CreatedOnDateTime { get; set; }
        public System.DateTime LastModifiedOnDateTime { get; set; }
        public bool IsADeletedRow { get; set; }
        public bool IsAnAuditRow { get; set; }
        public int CardID { get; set; }
        public int LanguageID { get; set; }
        public string CardName { get; set; }
        public string CardTitle { get; set; }
        public string DetailedCardDescription { get; set; }
        public string FacilitatorSlideNotes { get; set; }
    
        public virtual User aspnet_Users { get; set; }
        public virtual User aspnet_Users1 { get; set; }
        public virtual Card Card { get; set; }
        public virtual Language Language { get; set; }
    }
}
