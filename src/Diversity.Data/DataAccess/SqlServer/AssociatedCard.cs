//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diversity.DataModel.SqlRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class AssociatedCard
    {
        public int AssociatedCardID { get; set; }
        public System.Guid UserID_CreatedBy { get; set; }
        public System.Guid UserID_LastModifiedBy { get; set; }
        public System.DateTime CreatedOnDateTime { get; set; }
        public System.DateTime LastModifiedOnDateTime { get; set; }
        public bool IsADeletedRow { get; set; }
        public bool IsAnAuditRow { get; set; }
        public int CardID { get; set; }
        public int CardID_AssociatedCard { get; set; }
        public bool IsAssociatedToElseContextuallySimilarTo { get; set; }
        public bool IsPrimaryAssociation { get; set; }
        public string ExplanationForAssociation { get; set; }
    
        public virtual User aspnet_Users { get; set; }
        public virtual User aspnet_Users1 { get; set; }
        public virtual Card Card { get; set; }
    }
}
