﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Diversity.DataModel.SqlRepository
{    
    public partial class Result
    {
        [System.ComponentModel.DataAnnotations.Schema.NotMappedAttribute]
        public XElement ResultXmlElement
        {
            get { return XElement.Parse(ResultXml); }
            set { ResultXml = value.ToString(); }
        }
    }
}
