//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diversity.DataModel.SqlRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Distributor
    {
        public Distributor()
        {
            this.Client2Distributors = new HashSet<Client2Distributor>();
            this.Product2Distributors = new HashSet<Product2Distributor>();
            this.User2Distributors = new HashSet<User2Distributor>();
        }
    
        public int DistributorID { get; set; }
        public System.Guid UserID_CreatedBy { get; set; }
        public System.Guid UserID_LastModifiedBy { get; set; }
        public System.DateTime CreatedOnDateTime { get; set; }
        public System.DateTime LastModifiedOnDateTime { get; set; }
        public bool IsADeletedRow { get; set; }
        public bool IsAnAuditRow { get; set; }
        public string PhysicalAddress1 { get; set; }
        public string PhysicalAddress2 { get; set; }
        public string PhysicalPostalCode { get; set; }
        public string PhysicalStateProvince { get; set; }
        public string PhysicalCountry { get; set; }
        public string PostalAddress1 { get; set; }
        public string PostalAddress2 { get; set; }
        public string PostalPostalCode { get; set; }
        public string PostalStateProvince { get; set; }
        public string PostalCountry { get; set; }
        public string DistributorName { get; set; }
        public string DistributorTel1 { get; set; }
        public string DistributorEmailAddress { get; set; }
        public bool DistributorCanCreateProducts { get; set; }
        public string DistributorNotes { get; set; }
        public int MaxNoOfProductsDistributorCanCreate { get; set; }
    
        public virtual User aspnet_Users { get; set; }
        public virtual User aspnet_Users1 { get; set; }
        public virtual ICollection<Client2Distributor> Client2Distributors { get; set; }
        public virtual ICollection<Product2Distributor> Product2Distributors { get; set; }
        public virtual ICollection<User2Distributor> User2Distributors { get; set; }
    }
}
