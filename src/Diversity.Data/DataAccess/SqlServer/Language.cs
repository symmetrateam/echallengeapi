//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diversity.DataModel.SqlRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Language
    {
        public Language()
        {
            this.Card = new HashSet<Card>();
            this.Card_Language = new HashSet<Card_Language>();
            this.CardClassificationGroup_Language = new HashSet<CardClassificationGroup_Language>();
            this.CardReferenceMaterial = new HashSet<CardReferenceMaterial>();
            this.CardReferenceMaterial_Language = new HashSet<CardReferenceMaterial_Language>();
            this.CardReferenceMaterial_Language1 = new HashSet<CardReferenceMaterial_Language>();
            this.ErrorMessage_Language = new HashSet<ErrorMessage_Language>();
            this.FlashAsset_Language = new HashSet<FlashAsset_Language>();
            this.Product = new HashSet<Product>();
            this.Product_Language = new HashSet<Product_Language>();
            this.QuestionCard_GeneralAnswer_Language = new HashSet<QuestionCard_GeneralAnswer_Language>();
            this.QuestionCard_Language = new HashSet<QuestionCard_Language>();
            this.QuestionCardStatement_Language = new HashSet<QuestionCardStatement_Language>();
            this.BranchingCard_ButtonLanguage = new HashSet<BranchingCard_ButtonLanguage>();
        }
    
        public int LanguageID { get; set; }
        public string Language1 { get; set; }
        public bool IsLeftToRight { get; set; }
        public string LCID { get; set; }
    
        public virtual ICollection<Card> Card { get; set; }
        public virtual ICollection<Card_Language> Card_Language { get; set; }
        public virtual ICollection<CardClassificationGroup_Language> CardClassificationGroup_Language { get; set; }
        public virtual ICollection<CardReferenceMaterial> CardReferenceMaterial { get; set; }
        public virtual ICollection<CardReferenceMaterial_Language> CardReferenceMaterial_Language { get; set; }
        public virtual ICollection<CardReferenceMaterial_Language> CardReferenceMaterial_Language1 { get; set; }
        public virtual ICollection<ErrorMessage_Language> ErrorMessage_Language { get; set; }
        public virtual ICollection<FlashAsset_Language> FlashAsset_Language { get; set; }
        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<Product_Language> Product_Language { get; set; }
        public virtual ICollection<QuestionCard_GeneralAnswer_Language> QuestionCard_GeneralAnswer_Language { get; set; }
        public virtual ICollection<QuestionCard_Language> QuestionCard_Language { get; set; }
        public virtual ICollection<QuestionCardStatement_Language> QuestionCardStatement_Language { get; set; }
        public virtual ICollection<BranchingCard_ButtonLanguage> BranchingCard_ButtonLanguage { get; set; }
    }
}
