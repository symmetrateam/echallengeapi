//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diversity.DataModel.SqlRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuestionCard_GeneralAnswer_Language
    {
        public int QuestionCard_GeneralAnswer_LanguageID { get; set; }
        public System.Guid UserID_CreatedBy { get; set; }
        public System.Guid UserID_LastModifiedBy { get; set; }
        public System.DateTime CreatedOnDateTime { get; set; }
        public System.DateTime LastModifiedOnDateTime { get; set; }
        public bool IsADeletedRow { get; set; }
        public bool IsAnAuditRow { get; set; }
        public int QuestionCard_GeneralAnswerID { get; set; }
        public int LanguageID { get; set; }
        public string PossibleAnswer_LongVersion { get; set; }
        public string PossibleAnswer_ShortVersion { get; set; }
        public string LHStatement_LongVersion { get; set; }
        public string RHStatement_LongVersion { get; set; }
        public string LHStatement_ShortVersion { get; set; }
        public string RHStatement_ShortVersion { get; set; }
        public string PossibleAnswerMultimediaURL { get; set; }
        public string PossibleAnswerMultimediaURLFriendlyName { get; set; }
    
        public virtual User aspnet_Users { get; set; }
        public virtual User aspnet_Users1 { get; set; }
        public virtual Language Language { get; set; }
        public virtual QuestionCard_GeneralAnswer QuestionCard_GeneralAnswer { get; set; }
    }
}
