﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Linq;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Linq;
using Diversity.Common;
using Diversity.DataModel.SqlRepository;
using Diversity.Common.Enumerations;
using Diversity.Data.Utility;
using Diversity.Domain.Model;
using log4net;
using Card = Diversity.Domain.Model.Card;
using Client = Diversity.Domain.Model.Client;
using Game = Diversity.Domain.Model.Game;
using FlashAsset = Diversity.Domain.Model.FlashAsset;

namespace Diversity.Data.DataAccess.SqlServer
{
    public partial class SqlGameRepository : SqlRepository, IGameRepository
    {
        private static readonly ILog _log = LogManager.GetLogger((System.Reflection.MethodBase.GetCurrentMethod().DeclaringType));

        public Game GetGame(int gameId, int languageId, Guid userId)
        {
            var game = GetGameById(languageId, gameId, userId);
            return game;
        }

        public bool CanNewGameBePlayedByUser(int gameId, int languageId, Guid userId)
        {
            var dbGame = (from g in DataContext.Games
                          where g.GameID == gameId
                          select g).FirstOrDefault();

            if(dbGame == null)
            {
                return false;
            }

            if(dbGame.NumberOfTimesGameCanBePlayed == 0 || 
                (dbGame.GameID_Template.HasValue && GetGameIdForInProgressGame(gameId,userId).HasValue))
            {
                return true;
            }

            var gameCount = (from g in DataContext.Games
                             where g.GameID_Template == gameId
                                   && g.UserID_CreatedBy == userId && !g.IsADeletedRow && !g.IsAnAuditRow
                             select g).Count();

            return !(gameCount > dbGame.NumberOfTimesGameCanBePlayed);
        }

        public IQueryable<FlashAsset> GetFlashAssets(int languageId)
        {
            var assets = from f in DataContext.FlashAssets
                   join l in DataContext.FlashAsset_Languages on f.FlashAssetID equals l.FlashAssetID
                   where l.LanguageID == languageId
                   select new 
                              {
                                  Name = f.Key,
                                  l.Value,
                                  Lcid = l.Language.LCID
                              };

            var results = (from a in assets
                           select new FlashAsset()
                                      {
                                          Name = a.Name,
                                          Value = a.Value,
                                          Lcid = a.Lcid
                                      });

            return results;
        }

        public int? GetGameIdForInProgressGame(int gameTemplateId, Guid userId)
        {
            var gamePlayer = DataContext.GamePlayers
                .FirstOrDefault
                (
                    p =>
                    p.UserID == userId && !p.IsAvatar && p.Game.GameID_Template == gameTemplateId &&
                    p.Game.GameCompletionDate == null
                );
                       
            if(gamePlayer == null)
            {
                return null;
            }

            return gamePlayer.GameID;
        }

        DataModel.SqlRepository.Game DuplicateGameFromTemplate(Game templateGame, Guid userId, int languageId, bool lockDownTemplate)
        {
            var currentDateTime = DateTime.Now;
            var duplicateGame = new DataModel.SqlRepository.Game();

            duplicateGame.UserID_CreatedBy = userId;
            duplicateGame.UserID_LastModifiedBy = userId;
            duplicateGame.CreatedOnDateTime = currentDateTime;
            duplicateGame.LastModifiedOnDateTime = currentDateTime;
            duplicateGame.GameID_Template = templateGame.Id;
            duplicateGame.IsOnlineGame = templateGame.GameSettings.IsOnline;
            duplicateGame.GameNowLockedDown = lockDownTemplate;
            duplicateGame.GameConfigCanBeModifiedAcrossInstances =
                templateGame.GameSettings.GameConfigCanBeModifiedAcrossInstances;
            duplicateGame.IsTimedGame = templateGame.GameSettings.IsTimed;
            duplicateGame.UseCardTime = templateGame.GameSettings.UseCardTime;
            duplicateGame.UseActualAvatarNames = templateGame.AvatarSettings.UseActualName;
            duplicateGame.UseHistoricAvatarAnswers = templateGame.AvatarSettings.UseHistoricAnswers;
            duplicateGame.UseActualAvatarAnsweringTimes = templateGame.AvatarSettings.UseActualAnsweringTimes;
            duplicateGame.AllUsersMustFinishThisGameByThisDate =
                templateGame.GameSettings.AllUsersMustFinishThisGameBy;
            duplicateGame.PlayerTurn_RandomNoRepeatingUntilAllHavePlayed =
                templateGame.PlayerTurnSettings.RandomNoRepeatingUntilAllHavePlayed;
            duplicateGame.PlayerTurn_ByPlayerSequenceNoElseRandom =
                templateGame.PlayerTurnSettings.BySequenceNoElseRandom;
            duplicateGame.DefaultGameTimeInSeconds = templateGame.GameSettings.DefaultGameTimeInSeconds;
            duplicateGame.GameThemeData = templateGame.GameSettings.GameThemeData;
            duplicateGame.CanSkipTutorial = templateGame.GameSettings.CanSkipTutorial;
            duplicateGame.GameTemplateNameForDashboard = templateGame.GameSettings.GameTemplateNameForDashboard;
            duplicateGame.ExternalID = templateGame.CourseCode;

            //set values here to create a new instance game
            duplicateGame.HasBeenDownloaded = true;
            duplicateGame.NumberOfTimesRun = 1;
            duplicateGame.GameStartDate = DateTime.Now;
            duplicateGame.IsAGameTemplate = false;
            duplicateGame.GameCompletionDate = null;

            duplicateGame.ProductID = templateGame.ProductId;

            //add the real game player to the game
            var gamePlayer = new GamePlayer();
            gamePlayer.CreatedOnDateTime = currentDateTime;
            gamePlayer.IsAvatar = false;
            gamePlayer.LastModifiedOnDateTime = currentDateTime;
            gamePlayer.UserID = userId;
            gamePlayer.UserID_CreatedBy = userId;
            gamePlayer.UserID_LastModifiedBy = userId;

            duplicateGame.GamePlayers.Add(gamePlayer);

            foreach (var cardGroup in templateGame.CardGroups)
            {
                var duplicateCardGroupSequence = new GameCardGroupSequence();
                duplicateCardGroupSequence.CardGroupSequenceNo = cardGroup.CardGroupSequenceNo;
                duplicateCardGroupSequence.GameID = duplicateGame.GameID;
                duplicateCardGroupSequence.IsAMandatoryCardGroup = cardGroup.IsMandatoryGroup;
                duplicateCardGroupSequence.CreatedOnDateTime = currentDateTime;
                duplicateCardGroupSequence.LastModifiedOnDateTime = currentDateTime;
                duplicateCardGroupSequence.UserID_CreatedBy = userId;
                duplicateCardGroupSequence.UserID_LastModifiedBy = userId;

                foreach (Card card in cardGroup.Cards)
                {
                    var duplicateGameCardSequence = new GameCardSequence();
                    duplicateGameCardSequence.CardID = card.Id;
                    duplicateGameCardSequence.CardSequenceNo = card.CardSequenceNo;
                    duplicateGameCardSequence.CardViewedInGame = false;
                    duplicateGameCardSequence.CardViewedInGameOn = null;
                    duplicateGameCardSequence.GameCardGroupSequenceID =
                        duplicateCardGroupSequence.GameCardGroupSequenceID;
                    duplicateGameCardSequence.IsAMandatoryCard = card.IsAMandatoryCard;
                    duplicateGameCardSequence.UseLongVersion = card.UseLongVersion;

                    duplicateGameCardSequence.CreatedOnDateTime = currentDateTime;
                    duplicateGameCardSequence.LastModifiedOnDateTime = currentDateTime;
                    duplicateGameCardSequence.UserID_CreatedBy = userId;
                    duplicateGameCardSequence.UserID_LastModifiedBy = userId;

                    duplicateCardGroupSequence.GameCardSequences.Add(duplicateGameCardSequence);
                }

                duplicateGame.GameCardGroupSequences.Add(duplicateCardGroupSequence);
            }

            return duplicateGame;
        }

        List<DataModel.SqlRepository.Game> GetSimulatedPlayerGames(Game templateGame, int languageId, bool lockDownTemplate)
        {
            var player1 =
                DataContext.Users.FirstOrDefault(u => u.LoweredUserName == ConfigurationHelper.SystemPlayer1.ToLower());

            var player1Game = DuplicateGameFromTemplate(templateGame, player1.UserId, languageId, lockDownTemplate);            

            var player2 =
                DataContext.Users.FirstOrDefault(u => u.LoweredUserName == ConfigurationHelper.SystemPlayer2.ToLower());

            var player2Game = DuplicateGameFromTemplate(templateGame, player2.UserId, languageId, lockDownTemplate);

            var player3 =
                DataContext.Users.FirstOrDefault(u => u.LoweredUserName == ConfigurationHelper.SystemPlayer3.ToLower());

            var player3Game = DuplicateGameFromTemplate(templateGame, player3.UserId, languageId, lockDownTemplate);

            var results = new List<DataModel.SqlRepository.Game>();
            results.Add(player1Game);
            results.Add(player2Game);
            results.Add(player3Game);
            return results;
        }

        List<Result> GetSimulatedAnswersForGame(DataModel.SqlRepository.Game game, Guid lastModifiedByUserId)
        {
            var results = new List<Result>();

            var avatar = game.GamePlayers.FirstOrDefault();

            foreach(var cardSequence in game.GameCardGroupSequences.SelectMany(c=>c.GameCardSequences))
            {
                var card = DataContext.Cards.FirstOrDefault(c=>c.CardID == cardSequence.CardID);

                if(card.Internal_CardType.Internal_CardTypeShortCode.Equals(Enum.GetName(typeof(CardType), CardType.MultipleChoiceQuestionCard)) ||
                    card.Internal_CardType.Internal_CardTypeShortCode.Equals(Enum.GetName(typeof(CardType), CardType.MultipleChoiceImageQuestionCard)) ||
                    card.Internal_CardType.Internal_CardTypeShortCode.Equals(Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementCard)) ||
                    card.Internal_CardType.Internal_CardTypeShortCode.Equals(Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementImageCard))
                    )
                {
                    var random = new Random();

                    var answerNo = random.Next(1, card.QuestionCard.QuestionCard_GeneralAnswers.Count);
                    var answers = card.QuestionCard.QuestionCard_GeneralAnswers.ToList();

                    var timeToAnswer = card.CardMaxTimeToAnswerInSeconds > 0
                                           ? random.Next(1, card.CardMaxTimeToAnswerInSeconds)
                                           : 0;                                       

                    var playerResult = new MultipleChoicePlayerResult()
                                           {
                                               AnswerId = answers[answerNo].QuestionCard_GeneralAnswerID,
                                               PlayerId = avatar.UserID,
                                               TimeTakenToAnswerInSeconds = timeToAnswer
                                           };

                    var xml = SerializeObject(playerResult);

                    if(string.IsNullOrEmpty(xml))
                    {
                        continue;
                    }

                    var resultXml = XElement.Parse(xml);


                    var currentDateTime = DateTime.Now;
                    var result = new Result()
                    {
                        CardID = card.CardID,
                        CreatedOnDateTime = currentDateTime,
                        GameID = game.GameID,
                        Internal_CardTypeID = card.Internal_CardTypeID,
                        LastModifiedOnDateTime = currentDateTime,
                        ResultType = resultXml.Name.LocalName,
                        ResultXmlElement = resultXml,
                        UserID_CreatedBy = lastModifiedByUserId,
                        UserID_LastModifiedBy = lastModifiedByUserId
                    };
                    
                    results.Add(result);
                }
                else if (card.Internal_CardType.Internal_CardTypeShortCode.Equals(Enum.GetName(typeof(CardType), CardType.MatchingListQuestionCard)))
                {
                    var random = new Random();
                    
                    var answers = card.QuestionCard.QuestionCard_GeneralAnswers.ToList();

                    var timeToAnswer = card.CardMaxTimeToAnswerInSeconds > 0
                                          ? random.Next(1, card.CardMaxTimeToAnswerInSeconds)
                                          : 0;

                    var playerResult = new MatchingListPlayerResult()
                    {
                        PlayerId = avatar.UserID,
                        TimeTakenToAnswerInSeconds = timeToAnswer,
                        Results = new MatchingListPlayerResultAnswer[answers.Count]
                    };

                    int rhAnswerNo;
                    int lhAnswerNo;

                    for (int i = 0; i < playerResult.Results.Count; i++)
                    {
                        rhAnswerNo = random.Next(0, answers.Count);
                        lhAnswerNo = random.Next(0, answers.Count);

                        playerResult.Results[i] = new MatchingListPlayerResultAnswer()
                                                  {
                                                      AnswerSequenceNo = 0,
                                                      RHAnswerId = answers[rhAnswerNo].QuestionCard_GeneralAnswerID,
                                                      LHAnswerId = answers[lhAnswerNo].QuestionCard_GeneralAnswerID
                                                  };
                    }

                    var xml = SerializeObject(playerResult);

                    if (string.IsNullOrEmpty(xml))
                    {
                        continue;
                    }

                    var resultXml = XElement.Parse(xml);

                    var currentDateTime = DateTime.Now;
                    var result = new Result()
                    {
                        CardID = card.CardID,
                        CreatedOnDateTime = currentDateTime,
                        GameID = game.GameID,
                        Internal_CardTypeID = card.Internal_CardTypeID,
                        LastModifiedOnDateTime = currentDateTime,
                        ResultType = resultXml.Name.LocalName,
                        ResultXmlElement = resultXml,
                        UserID_CreatedBy = lastModifiedByUserId,
                        UserID_LastModifiedBy = lastModifiedByUserId
                    };

                    results.Add(result);

                }
                else if (card.Internal_CardType.Internal_CardTypeShortCode.Equals(Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxQuestionCard)) ||
                    card.Internal_CardType.Internal_CardTypeShortCode.Equals(Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxImageQuestionCard)))
                {
                    var random = new Random();

                    var answers = card.QuestionCard.QuestionCard_GeneralAnswers.Where(c => c.IsCorrectAnswer).ToList();

                    var timeToAnswer = card.CardMaxTimeToAnswerInSeconds > 0
                                           ? random.Next(1, card.CardMaxTimeToAnswerInSeconds)
                                           : 0;

                    var playerResult = new MultipleCheckBoxPlayerResult()
                    {
                        PlayerId = avatar.UserID,
                        TimeTakenToAnswerInSeconds = timeToAnswer,
                        Results = new MultipleCheckBoxPlayerResultAnswer[answers.Count]
                    };

                    for (int i = 0; i < playerResult.Results.Count; i++)
                    {
                        var answerNo = random.Next(0, answers.Count);

                        playerResult.Results[i] = new MultipleCheckBoxPlayerResultAnswer()
                        {
                            AnswerId = answers[answerNo].QuestionCard_GeneralAnswerID
                        };
                    }

                    var xml = SerializeObject(playerResult);

                    if (string.IsNullOrEmpty(xml))
                    {
                        continue;
                    }

                    var resultXml = XElement.Parse(xml);


                    var currentDateTime = DateTime.Now;
                    var result = new Result()
                    {
                        CardID = card.CardID,
                        CreatedOnDateTime = currentDateTime,
                        GameID = game.GameID,
                        Internal_CardTypeID = card.Internal_CardTypeID,
                        LastModifiedOnDateTime = currentDateTime,
                        ResultType = resultXml.Name.LocalName,
                        ResultXmlElement = resultXml,
                        UserID_CreatedBy = lastModifiedByUserId,
                        UserID_LastModifiedBy = lastModifiedByUserId
                    };

                    results.Add(result);
                }
            }
            return results;
        }

        static string SerializeObject<T>(T source)
        {
            using (var stream = new MemoryStream())
            {
                var serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(stream, source);
                return System.Text.Encoding.UTF8.GetString(stream.ToArray());
            }
        }


        public Game CreateGameFromTemplateGame(Game templateGame, Guid userId, int languageId, bool lockDownTemplate)
        {
            //select random players for avatars
            var avatars = GetRandomAvatarsForOnlineGame(templateGame.Id, userId);

            //first check if there are any avatars available
            //if no avatars available for template game then the system
            //avatars need to be used to simulate playing the game               
            var createdGameId = -1;
            var currentDateTime = DateTime.Now;
            using (var dataContext = new EChallengeDataContext())
            {
                var duplicateGame = DuplicateGameFromTemplate(templateGame, userId, languageId, lockDownTemplate);

                var gameInvite =
                    dataContext.GameInvites.FirstOrDefault(c => c.UserID == userId && c.GameID == templateGame.Id);

                if (gameInvite != null)
                {
                    duplicateGame.ThemeClientUserGroupExternalID = gameInvite.ThemeClientUserGroupExternalID;
                }

                dataContext.Games.Add(duplicateGame);               
            
                if (avatars.Count <= 0)
                {
                    var games = GetSimulatedPlayerGames(templateGame, languageId, lockDownTemplate);
                    

                    foreach (var game in games)
                    {
                        var currentPlayer = game.GamePlayers.FirstOrDefault();
                        var player = new GamePlayer()
                                             {
                                                 UserID = currentPlayer.UserID,
                                                 IsAvatar = true
                                             };
                        avatars.Add(player);               

                        foreach (var result in GetSimulatedAnswersForGame(game,userId))
                        {
                            result.GamePlayer = player;
                            game.Results.Add(result);
                        }

                        game.GameCompletionDate = DateTime.Now;
                        dataContext.Games.Add(game);                                                            
                    }                                       
                }

                var gamePlayer = duplicateGame.GamePlayers.FirstOrDefault();

                //now fill populate other information for avatars;
                foreach (var avatar in avatars)
                {
                    avatar.IsAvatar = true;
                    avatar.GameID = duplicateGame.GameID;
                    avatar.CreatedOnDateTime = currentDateTime;
                    avatar.UserID_CreatedBy = userId;
                    avatar.LastModifiedOnDateTime = currentDateTime;
                    avatar.UserID_LastModifiedBy = userId;
                }

                //generate random player sequences for avatar and player
                avatars.Add(gamePlayer);

                var shuffleAvatars = avatars.OrderBy(a => Guid.NewGuid()).ToList();
                for (var i = 0; i < shuffleAvatars.Count(); i++)
                {
                    var player = shuffleAvatars[i];
                    player.PlayerTurnSequenceNo = i + 1;
                }

                if (shuffleAvatars.Count > 0)
                {
                    foreach (var shuffleAvatar in shuffleAvatars.Where(a => a.IsAvatar))
                    {
                        duplicateGame.GamePlayers.Add(shuffleAvatar);
                    }

                    var player = shuffleAvatars.FirstOrDefault(a => a.UserID == gamePlayer.UserID);
                    gamePlayer.PlayerTurnSequenceNo = player.PlayerTurnSequenceNo;

                    var list = duplicateGame.GamePlayers.ToList();
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    //as a new game is now based of the template game we need to lock down the template
                    var currentTemplateGame = dataContext.Games.FirstOrDefault(g => g.GameID == templateGame.Id);
                    currentTemplateGame.GameNowLockedDown = lockDownTemplate;

                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();
                        createdGameId = duplicateGame.GameID;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }

            var createdGame = GetGame(createdGameId, languageId, userId);
            createdGame.IsResumed = false;
            return createdGame;
        }

        public bool SaveResult(Guid userId, int cardSequenceId, string resultType, XElement resultXml)
        {
            var saved = false;

            using (var dataContext = new EChallengeDataContext())
            {
                var cardSequence =
                    dataContext.GameCardSequences.FirstOrDefault(s => s.GameCardSequenceID == cardSequenceId);

                if (cardSequence == null)
                {
                    return false;
                }

                var cardId = cardSequence.CardID;
                var gameId = cardSequence.GameCardGroupSequence.GameID;

                if (dataContext.Results.Any(r => r.GameID == gameId && r.CardID == cardId))
                {
                    var existingResult = dataContext.Results.FirstOrDefault(r => r.GameID == gameId && r.CardID == cardId);

                    if(existingResult != null)
                    {
                        var cardType = GetCardTypeFromCardSequenceId(cardSequenceId);

                        if (cardType != CardType.MultipleChoicePriorityFeedbackCard &&
                            cardType != CardType.TextAreaPriorityFeedbackCard &&
                            cardType != CardType.MultipleCheckBoxPriorityFeedbackCard)
                        {
                            dataContext.Results.Remove(existingResult);   
                        }
                        else
                        {
                            var feedbackResult = Deserialize<VotingCardFeedbackResult>(existingResult.ResultXmlElement);

                            if (feedbackResult != null)
                            {
                                var currentFeedbackResult = Deserialize<VotingCardFeedbackResult>(resultXml);

                                if (currentFeedbackResult != null && 
                                    feedbackResult.AnswerId == currentFeedbackResult.AnswerId)
                                {
                                    dataContext.Results.Remove(existingResult);  
                                }
                            }
                        }                        
                    }                   
                }

                bool isUpdateOnly = false;

                var currentDateTime = DateTime.Now;

                cardSequence.LastModifiedOnDateTime = currentDateTime;
                cardSequence.UserID_LastModifiedBy = userId;
                cardSequence.CardViewedInGame = true;
                cardSequence.CardViewedInGameOn = currentDateTime;

                Result result = null;
                if (string.Compare(resultType, typeof(PlayerResult).Name, true) == 0)
                {
                    isUpdateOnly = true;
                }
                else
                {
                    var card = dataContext.Cards.FirstOrDefault(c => c.CardID == cardId);

                    if (card == null)
                    {
                        return false;
                    }

                    var gamePlayer =
                        dataContext.GamePlayers.FirstOrDefault(g => g.GameID == gameId && g.UserID == userId);

                    if(gamePlayer == null)
                    {
                        throw new Exception(@"The result could no be saved as the Game Player could not be found for this game.");
                    }

                    result = new Result()
                                     {
                                         CardID = cardId,
                                         CreatedOnDateTime = currentDateTime,
                                         GameID = gameId,
                                         Internal_CardTypeID = card.Internal_CardTypeID,
                                         LastModifiedOnDateTime = currentDateTime,
                                         ResultType = resultType,
                                         ResultXmlElement = resultXml,
                                         UserID_CreatedBy = userId,
                                         UserID_LastModifiedBy = userId,
                                         GamePlayerID = gamePlayer.GamePlayerID
                                     };

                    dataContext.Results.Add(result);                    
                }

                dataContext.SaveChanges();

                if(isUpdateOnly)
                {
                    saved = true;
                }
                else
                {
                    saved = result.ResultID > 0;
                }

            }
            return saved;
        }

        public bool SkipCard(Guid userId, int cardSequenceId)
        {
            var saved = false;

            using (var dataContext = new EChallengeDataContext())
            {
                var cardSequence =
                    dataContext.GameCardSequences.FirstOrDefault(s => s.GameCardSequenceID == cardSequenceId);

                if (cardSequence == null)
                {
                    return false;
                }

                var currentDateTime = DateTime.Now;

                cardSequence.LastModifiedOnDateTime = currentDateTime;
                cardSequence.UserID_LastModifiedBy = userId;
                cardSequence.CardViewedInGame = true;
                cardSequence.CardViewedInGameOn = currentDateTime;

                dataContext.SaveChanges();
                saved = true;
            }
            return saved;

        }

        public bool SetGameAsCompleted(Guid userId, int gameId)
        {
            var saved = false;

            using (var dataContext = new EChallengeDataContext())
            {
                var game =
                    dataContext.Games.FirstOrDefault(s => s.GameID == gameId && s.UserID_CreatedBy == userId);

                if (game == null)
                {
                    return false;
                }

                var currentDateTime = DateTime.Now;

                game.LastModifiedOnDateTime = currentDateTime;
                game.UserID_LastModifiedBy = userId;
                game.GameCompletionDate = DateTime.Now;

                var gameCompletedShortCode = Enum.GetName(typeof (CardType), CardType.GameCompletedCard);

                var gameCompletedCard = dataContext.GameCardSequences.FirstOrDefault(
                    c =>
                    c.Card.Internal_CardType.Internal_CardTypeShortCode ==
                    gameCompletedShortCode && c.GameCardGroupSequence.GameID == gameId);

                if (gameCompletedCard != null)
                {
                    gameCompletedCard.LastModifiedOnDateTime = currentDateTime;
                    gameCompletedCard.UserID_LastModifiedBy = userId;
                    gameCompletedCard.CardViewedInGame = true;
                    gameCompletedCard.CardViewedInGameOn = game.GameCompletionDate;
                }

                dataContext.SaveChanges();
                saved = true;
            }
            return saved;
        }

        public bool HasUnansweredQuestions(int gameId)
        {
            var excludedCardTypes = new List<string>();
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.AwardCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.CertificateCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.CompanyPolicyCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.GameIntroCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.GameCompletedCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.LeadingQuestionCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.LcdCategoryCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.IntroSurveyCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.RoundResultCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.ScoreboardCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.TransitionUpCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.TransitionDownCard));
            excludedCardTypes.Add(Enum.GetName(typeof(CardType), CardType.ThemeIntroCard));

            var cardsequences = DataContext.GameCardGroupSequences
                .Where(g => g.GameID == gameId)
                .SelectMany(g => g.GameCardSequences);

            _log.Debug(@"Start Has Unanswered Questions");

            var gameCompletedShortCode = Enum.GetName(typeof (CardType), CardType.GameCompletedCard);
            var noOfCardsRequiredToBeViewed = cardsequences.Where(
                c =>
                c.IsAMandatoryCard && c.Card.Internal_CardType.Internal_CardTypeShortCode != gameCompletedShortCode).Distinct().Count();

            _log.DebugFormat(@"Cards Required to be Viewed = {0}", noOfCardsRequiredToBeViewed);

            var noOfCardsViewInGame = cardsequences.Where(
                c => c.IsAMandatoryCard && c.CardViewedInGame && c.Card.Internal_CardType.Internal_CardTypeShortCode != gameCompletedShortCode).Distinct().Count();

            _log.DebugFormat(@"Cards Viewed = {0}", noOfCardsViewInGame);

            if(noOfCardsRequiredToBeViewed != noOfCardsViewInGame)
            {
                return true;
            }

            var noOfCardsRequiredToBeAnswered = cardsequences.Where(
                c =>
                //validate mandatory cards
                c.IsAMandatoryCard &&
                !excludedCardTypes.Contains(c.Card.Internal_CardType.Internal_CardTypeShortCode))
                .Distinct().Count();

            _log.DebugFormat(@"Cards Required to be Answered = {0}", noOfCardsRequiredToBeAnswered);

            var results = DataContext.Results.Where(r=>r.GameID == gameId).Select(r=>r.CardID);

            var cardSequencesToAnswer =(DataContext.GameCardGroupSequences
                .Where(g => g.GameID == gameId)                
                .SelectMany(g => g.GameCardSequences))
                .Where(c=>c.IsAMandatoryCard && !excludedCardTypes.Contains(c.Card.Internal_CardType.Internal_CardTypeShortCode));


            var noOfCardsAnswered = (from c in cardSequencesToAnswer
                           where results.Contains(c.CardID)
                           select c).Count();

            _log.DebugFormat(@"Cards Answered = {0}", noOfCardsAnswered);

            //var noOfCardsAnswered = (from grp in DataContext.GameCardGroupSequences
            //                         from cs in grp.GameCardSequences
            //                         where grp.GameID == gameId && cs.IsAMandatoryCard && !excludedCardTypes.Contains(cs.Card.Internal_CardType.Internal_CardTypeShortCode)
            //                         && results.Contains(cs.CardID)
            //                         select cs).Count();


                                        
            
            if(noOfCardsAnswered == noOfCardsRequiredToBeAnswered)
            {
                return false;
            }

            return true;
        }

        public bool IncrementGameRunCount(int gameId, Guid userId)
        {
            var saved = false;

            using (var dataContext = new EChallengeDataContext())
            {
                var game =
                    dataContext.Games.FirstOrDefault(s => s.GameID == gameId && s.UserID_CreatedBy == userId);

                if (game == null)
                {
                    return false;
                }

                var currentDateTime = DateTime.Now;

                game.LastModifiedOnDateTime = currentDateTime;
                game.UserID_LastModifiedBy = userId;
                var numberOfTimesRun = game.NumberOfTimesRun;
                numberOfTimesRun++;
                game.NumberOfTimesRun = numberOfTimesRun;

                dataContext.SaveChanges();
                saved = true;
            }
            return saved;
        }

        public CardType GetCardTypeFromCardSequenceId(int cardSequenceId)
        {
            var cardSequence = DataContext.GameCardSequences.FirstOrDefault(s => s.GameCardSequenceID == cardSequenceId);

            if(cardSequence == null)
            {
                return CardType.Unknown;                
            }            
            return GetCardType(cardSequence.CardID);
        }        

        public CardType GetCardType(int cardId)
        {
            var card = DataContext.Cards.FirstOrDefault(c => c.CardID == cardId);

            if (card == null)
            {
                return CardType.Unknown;
            }

            var result = CardType.Unknown;

            if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.ArrangeSequenceQuestionCard))
            {
                result = CardType.ArrangeSequenceQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.FillInTheBlankQuestionCard))
            {
                result = CardType.FillInTheBlankQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MatchingListQuestionCard))
            {
                result = CardType.MatchingListQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.NumericQuestionCard))
            {
                result = CardType.NumericQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.PotLuckQuestionCard))
            {
                result = CardType.PotLuckQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceQuestionCard))
            {
                result = CardType.MultipleChoiceQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceImageQuestionCard))
            {
                result = CardType.MultipleChoiceImageQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxQuestionCard))
            {
                result = CardType.MultipleCheckBoxQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.LcdCategoryCard))
            {
                result = CardType.LcdCategoryCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementCard))
            {
                result = CardType.MultipleChoiceAuditStatementCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementImageCard))
            {
                result = CardType.MultipleChoiceAuditStatementImageCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.CompanyPolicyCard))
            {
                result = CardType.CompanyPolicyCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.LeadingQuestionCard))
            {
                result = CardType.LeadingQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.TransitionUpCard))
            {
                result = CardType.TransitionUpCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.TransitionDownCard))
            {
                result = CardType.TransitionDownCard;
            }
            else if(card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.GameCompletedCard))
            {
                result = CardType.GameCompletedCard;                
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceSurveyCard))
            {
                result = CardType.MultipleChoiceSurveyCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxSurveyCard))
            {
                result = CardType.MultipleCheckBoxSurveyCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxPriorityFeedbackCard))
            {
                result = CardType.MultipleCheckBoxPriorityFeedbackCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoicePriorityFeedbackCard))
            {
                result = CardType.MultipleChoicePriorityFeedbackCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.TextAreaPriorityFeedbackCard))
            {
                result = CardType.TextAreaPriorityFeedbackCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.PriorityIssueVotingCard))
            {
                result = CardType.PriorityIssueVotingCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.TextAreaSurveyCard))
            {
                result = CardType.TextAreaSurveyCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.TextBoxSurveyCard))
            {
                result = CardType.TextBoxSurveyCard;
            }
            else if(card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.TextAreaPersonalActionPlanCard))
            {
                result = CardType.TextAreaPersonalActionPlanCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.TextBoxPersonalActionPlanCard))
            {
                result = CardType.TextBoxPersonalActionPlanCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoicePersonalActionPlanCard))
            {
                result = CardType.MultipleChoicePersonalActionPlanCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckboxPersonalActionPlanCard))
            {
                result = CardType.MultipleCheckboxPersonalActionPlanCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.ScoreboardCard))
            {
                result = CardType.ScoreboardCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.RoundResultCard))
            {
                result = CardType.RoundResultCard;
            }
            else if(card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.IntroSurveyCard))
            {
                result = CardType.IntroSurveyCard;                
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.GameIntroCard))
            {
                result = CardType.GameIntroCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.ThemeIntroCard))
            {
                result = CardType.ThemeIntroCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MatchingListQuestionCard))
            {
                result = CardType.MatchingListQuestionCard;
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.BranchingCard))
            {
                result = CardType.BranchingCard;
            }

            return result;
        }

        public Dashboard GetDashboard(Guid userId, int languageId)
        {
            return
                new Dashboard()
                {
                    Games = GetGamesForDashboard(userId, languageId).ToList(),
                    Player = GetPlayer(userId)
                };
        }


        IEnumerable<GameTemplateInstance> GetGamesForDashboard(Guid userId, int languageId)
        {
            var dbGames = from g in DataContext.Games
                   join i in DataContext.GameInvites on g.GameID equals i.GameID
                   where g.IsAGameTemplate && i.UserID == userId
                   select new 
                              {
                                  Id = g.GameID,
                                  game = g,
                                  NumberOfTimesGameCanBePlayed = g.NumberOfTimesGameCanBePlayed.HasValue ? g.NumberOfTimesGameCanBePlayed.Value : 1
                              };


            var games = new List<GameTemplateInstance>();

            foreach (var dbGame in dbGames)
            {
                var game = new GameTemplateInstance()
                               {
                                   Id = dbGame.Id,
                                   GameSettings = GetGameSettings(dbGame.game),
                                   Games = GetGameInstancesForTemplate(dbGame.Id, userId),
                                   NumberOfTimesGameCanBePlayed = dbGame.NumberOfTimesGameCanBePlayed
                               };

                games.Add(game);
            }

            return games;
        }

        List<GameInstance> GetGameInstancesForTemplate(int gameTemplateId, Guid userId)
        {
            var results = (from g in DataContext.Games
                    join p in DataContext.GamePlayers on g.GameID equals p.GameID
                    where g.GameID_Template == gameTemplateId && !g.IsAGameTemplate
                        && p.UserID == userId && g.UserID_CreatedBy == userId 
                        // Use UserID_CreatedBy above as a user will only be able to create his own game instances.
                        // This also prevents bringing up games in other peoples dashboard as the UserID may be used 
                        // as a simulated player in another game and so checking the player UserID only is not sufficient
                    select new 
                               {
                                   Id = g.GameID,
                                   game = g,
                                   ProductId = g.ProductID,
                               }).Distinct().ToList();

            var gameInstances = new List<GameInstance>();

            foreach (var result in results)
            {
                var gameInstance = new GameInstance()
                                       {
                                           Id = result.Id,
                                           GameSettings = GetGameSettings(result.game),
                                           ProductId = result.ProductId,
                                           PercentCompleted = GetPercentCompleted(result.Id)
                                       };

                SetGameHeaderCardTotals(gameInstance);

                gameInstances.Add(gameInstance);
            }

            return gameInstances;
        }

        public IQueryable<Game> GetGames(int languageId)
        {
            var games = from g in DataContext.Games
                          select g;

            var results = (from g in games
                           select new Game()
                                      {
                                          Id = g.GameID,
                                          ProductId = g.ProductID,
                                          GameSettings = GetGameSettings(g),
                                          AvatarSettings = GetAvatarSettings(g),
                                          PlayerTurnSettings = GetPlayerTurnSettings(g),
                                          Players = new List<Player>(GetGamePlayers(g.GameID)),
                                          CardGroups = GetCardGroups(g.GameID, languageId).ToList(),
                                          PercentCompleted = g.IsAGameTemplate ? 0 : GetPercentCompleted(g.GameID),
                                          IsResumed = true
                                          //All set to true here. When new games are created from templates, 
                                          //after receving the game from db this value is set to false to indicate a new game
                                      });

            foreach (var result in results)
            {
                SetGameHeaderCardTotals(result);
            }
          
            return results;
        }

        public Game GetGameById(int languageId, int gameId, Guid userId)
        {
            var dbGame = (from g in DataContext.Games
                       where g.GameID == gameId
                          select g).FirstOrDefault();

            var theme = DataContext.ThemeClientUserGroup
                .Where(c => c.ExternalID.ToLower() == dbGame.ThemeClientUserGroupExternalID.ToLower())
                .Select(c => c.Theme)
                .FirstOrDefault();

            var newGame = new Game()
                              {
                                  Id = dbGame.GameID,
                                  ProductId = dbGame.ProductID,
                                  PercentCompleted = dbGame.IsAGameTemplate ? 0 : GetPercentCompleted(dbGame.GameID),
                                  IsResumed = true, //All set to true here. When new games are created from templates, 
                                  //after receving the game from db this value is set to false to indicate a new game
                                  ThemeKey = theme == null ? string.Empty : theme.ThemeCode
                              };

            newGame.GameSettings = GetGameSettings(dbGame);
            newGame.AvatarSettings = GetAvatarSettings(dbGame);
            newGame.PlayerTurnSettings = GetPlayerTurnSettings(dbGame);
            newGame.Players = new List<Player>(GetGamePlayers(dbGame.GameID));
            newGame.CardGroups = GetCardGroups(dbGame.GameID, languageId).ToList();
            newGame.Client = GetGameClient(userId);
            newGame.CourseCode = dbGame.ExternalID;
            
            SetGameHeaderCardTotals(newGame);

            return newGame;
        }

        private void SetGameHeaderCardTotals(GameHeader gameHeader)
        {
            var cardsequences = DataContext.GameCardSequences
                .Where(
                    g =>
                    g.GameCardGroupSequence.GameID == gameHeader.Id)
                .ToList();

            gameHeader.TotalCardsInGame = cardsequences.Count;
            gameHeader.TotalCardsViewed = cardsequences.Count(c => c.CardViewedInGame);
        }

        private int GetPercentCompleted(int gameId)
        {
            _log.Debug(@"Start Get percent game completed");
            var game = DataContext.Games.FirstOrDefault(g => g.GameID == gameId);

            if(game.GameCompletionDate.HasValue)
            {
                return 100;
            }

            var noAnswerCardTypes = new List<string>
                                        {
                                            Enum.GetName(typeof (CardType), CardType.RoundResultCard),
                                            Enum.GetName(typeof (CardType), CardType.ScoreboardCard),
                                            Enum.GetName(typeof (CardType), CardType.GameCompletedCard),
                                            Enum.GetName(typeof (CardType), CardType.AwardCard),
                                            Enum.GetName(typeof (CardType), CardType.CertificateCard),
                                            Enum.GetName(typeof (CardType), CardType.CompanyPolicyCard),
                                            Enum.GetName(typeof (CardType), CardType.LcdCategoryCard),
                                            Enum.GetName(typeof(CardType), CardType.GameIntroCard),
                                            Enum.GetName(typeof(CardType), CardType.LeadingQuestionCard),            
                                            Enum.GetName(typeof(CardType), CardType.IntroSurveyCard),
                                            Enum.GetName(typeof(CardType), CardType.TransitionUpCard),
                                            Enum.GetName(typeof(CardType), CardType.TransitionDownCard),
                                            Enum.GetName(typeof(CardType), CardType.ThemeIntroCard)
                                        };

            var cardsequences = DataContext.GameCardSequences
                .Where(
                    g =>
                    g.GameCardGroupSequence.GameID == gameId)
                .ToList();                                   

            var totalCardsViewedInGame = cardsequences.Count(c => c.CardViewedInGame);
            _log.DebugFormat(@"Cards Viewed = {0}", totalCardsViewedInGame);

            var results = DataContext.Results.Where(r => r.GameID == gameId).Select(r => r.CardID).ToList();

            var cardSequencesToAnswer = (DataContext.GameCardGroupSequences
                .Where(g => g.GameID == gameId)
                .SelectMany(g => g.GameCardSequences))
                .Where(c => c.IsAMandatoryCard && !noAnswerCardTypes.Contains(c.Card.Internal_CardType.Internal_CardTypeShortCode)).ToList();

            var totalCardsViewedNoAnswerRequired = cardsequences.Count(c => !cardSequencesToAnswer.Contains(c) && c.CardViewedInGame);

            var noAnsweredCards = (from c in cardSequencesToAnswer
                                     where results.Contains(c.CardID)
                                     select c).Count();            

            var totalCardsForGameCompletion = cardsequences.Count;

            var percentCompleted = 0.00;

            if (totalCardsViewedInGame > 0)
            {
                percentCompleted = 100*
                                   ((Convert.ToDouble(totalCardsViewedNoAnswerRequired) + Convert.ToDouble(noAnsweredCards))/
                                    Convert.ToDouble(totalCardsForGameCompletion));
            }

            return Convert.ToInt32(percentCompleted);
        }

        public Client GetGameClient(Guid userId)
        {
            var dbClient = (from u in DataContext.User2Clients
                            join c in DataContext.Clients on u.ClientID equals c.ClientID
                          where u.UserID == userId && !c.IsADeletedRow && !u.IsADeletedRow
                          select c).FirstOrDefault();

            if(dbClient == null)
            {
                return new Client();
            }

            return new Client
                       {
                           ClientLogoImageUrl = dbClient.ClientLogoImageURL,
                           ClientCode = dbClient.ExternalID
                       };
        }


        public bool UpdateScormPlayerDetails(Guid userId, string scormUserId, string firstname, string lastname, string email)
        {
            var result = false;

            var user = DataContext.Users.Where(u => u.UserId == userId).FirstOrDefault();

            if (user != null)
            {

                using (var transaction = DataContext.Database.BeginTransaction())
                {
                    try
                    {
                        user.UserFirstName = firstname;
                        user.UserLastName = lastname;

                        var loweredEmailAddress = email.ToLower();
                        user.UserPersonalEmail1 = loweredEmailAddress;
                        user.Email = loweredEmailAddress;
                        user.LoweredEmail = loweredEmailAddress;
                        user.LoweredUserName = loweredEmailAddress;
                        user.UserName = loweredEmailAddress;
                        user.ScormUserID = scormUserId;

                        user.LastModifiedOnDateTime = DateTime.Now;
                        user.UserID_LastModifiedBy = userId;

                        DataContext.SaveChanges();
                        transaction.Commit();
                        result = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return result;
        }

        public bool UpdatePlayerDetails(Guid userId, string firstname, string lastname)
        {
            var result = false;

            var user = DataContext.Users.Where(u => u.UserId == userId).FirstOrDefault();

            if (user != null)
            {

                using (var transaction = DataContext.Database.BeginTransaction())
                {
                    try
                    {
                        user.UserFirstName = firstname;
                        user.UserLastName = lastname;

                        user.LastModifiedOnDateTime = DateTime.Now;
                        user.UserID_LastModifiedBy = userId;

                        DataContext.SaveChanges();
                        transaction.Commit();
                        result = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return result;
        }

        public bool UpdateAvatarDetails(Guid userId, string avatarName, string avatarMetaData)
        {
            var result = false;

            var user = DataContext.Users.Where(u => u.UserId == userId).FirstOrDefault();

            if (user != null)
            {
                using (var transaction = DataContext.Database.BeginTransaction())
                {
                    try
                    {
                        user.PlayerAvatarName = avatarName;
                        user.PlayerAvatarData = avatarMetaData;

                        user.LastModifiedOnDateTime = DateTime.Now;
                        user.UserID_LastModifiedBy = userId;

                        DataContext.SaveChanges();
                        transaction.Commit();
                        result = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return result;
        }

        public bool ScormGameExists(string externalId)
        {
            var loweredExternalId = externalId.ToLower();

            return
                DataContext.Games.Any(
                    c => c.ExternalID.ToLower() == loweredExternalId && !c.GameID_Template.HasValue && !c.IsADeletedRow);
        }

        public bool UserExistsInThemeClientUserGroup(Guid userId, string themeClientUserGroupExternalId)
        {
            var loweredThemeCode = themeClientUserGroupExternalId.ToLower();

            return DataContext
                .User2ClientUserGroups
                .Where(c => c.UserID == userId)
                .SelectMany(c => c.ClientUserGroup.Themes)
                .Any(c => c.ExternalID.ToLower() == loweredThemeCode);
        }

        public bool AddUserToThemeClientUserGroup(Guid userId, string themeClientUserGroupExternalId)
        {
            var added = false;

            var loweredThemeCode = themeClientUserGroupExternalId.ToLower();

            var themeClientUserGroup = DataContext
                .ThemeClientUserGroup
                .FirstOrDefault(c => c.ExternalID.ToLower() == loweredThemeCode);

            var exists = UserExistsInThemeClientUserGroup(userId, loweredThemeCode);
           
            if (themeClientUserGroup != null && !exists)
            {
                var adminAccountUserName = ConfigurationHelper.AdminAccountName.ToLower();

                var adminUser = DataContext
                    .Users.FirstOrDefault(c => c.LoweredUserName == adminAccountUserName);            

                var clientUserGroup = DataContext.ClientUserGroups.FirstOrDefault(c => c.ClientUserGroupID == themeClientUserGroup.ClientUserGroupID);

                using (var transaction = DataContext.Database.BeginTransaction())
                {
                    try
                    {
                        var createdOn = DateTime.Now;

                        var mapping =
                            new User2ClientUserGroup()
                            {
                                ClientUserGroupID = themeClientUserGroup.ClientUserGroupID,
                                CreatedOnDateTime = createdOn,                                
                                LastModifiedOnDateTime = createdOn,                                
                                UserID = userId,
                                UserID_CreatedBy = adminUser.UserId,
                                UserID_LastModifiedBy = adminUser.UserId
                            };

                        if (clientUserGroup != null)
                        {
                            clientUserGroup.User2ClientUserGroups.Add(mapping);
                        }
                        added = true;
                        DataContext.SaveChanges();
                        transaction.Commit();                        
                    }
                    catch (Exception)
                    {
                        added = false;
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return added;
        }

        public bool InviteUserToGame(Guid userId, int gameId, string externalId)
        {
            var invited =
                DataContext
                    .GameInvites
                    .Any(c => c.GameID == gameId && c.UserID == userId && !c.IsADeletedRow);

            if (!invited)
            {
                var adminAccountUserName = ConfigurationHelper.AdminAccountName.ToLower();

                var adminUser = DataContext
                    .Users.FirstOrDefault(c => c.LoweredUserName == adminAccountUserName);

                var game = DataContext.Games.FirstOrDefault(c => c.GameID == gameId);

                using (var transaction = DataContext.Database.BeginTransaction())
                {
                    try
                    {
                        var createdOn = DateTime.Now;

                        var invite =
                            new GameInvite()
                            {
                                EmailContent = "Scorm - automatic invite",
                                EmailSubject = "Scorm - automatic invite",
                                ThemeClientUserGroupExternalID = externalId,
                                GameID = gameId,                                
                                InviteDate = createdOn,
                                CreatedOnDateTime = createdOn,
                                LastModifiedOnDateTime = createdOn,
                                LastReminderDate = createdOn,
                                UserID = userId,
                                UserID_CreatedBy = adminUser.UserId,
                                UserID_LastModifiedBy = adminUser.UserId                                                                
                            };
                        
                        game.GameInvite.Add(invite);
                        DataContext.SaveChanges();
                        transaction.Commit();
                        invited = true;                        
                    }
                    catch (Exception)
                    {
                        invited = false;
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return invited;
        }

        #region Private Methods

        #region Game

        public GameHeader.GameSettingsInfo GetGameSettings(DataModel.SqlRepository.Game game)
        {
            return new GameHeader.GameSettingsInfo()
                       {
                           AllUsersMustFinishThisGameBy = game.AllUsersMustFinishThisGameByThisDate,                           
                           CanSkipTutorial = game.CanSkipTutorial,                           
                           CompletionDate = game.GameCompletionDate,                                                      
                           DefaultGameTimeInSeconds = game.DefaultGameTimeInSeconds,
                           GameConfigCanBeModifiedAcrossInstances = game.GameConfigCanBeModifiedAcrossInstances,
                           GameNowLockedDown = game.GameNowLockedDown,                           
                           GameTemplateNameForDashboard = game.GameTemplateNameForDashboard,
                           GameThemeData = game.GameThemeData,
                           HasBeenDownloaded = game.HasBeenDownloaded,
                           IsOnline = game.IsOnlineGame,                           
                           IsTemplate = game.IsAGameTemplate,                           
                           IsTimed = game.IsTimedGame,
                           NumberOfTimesRun = game.NumberOfTimesRun,                           
                           StartDate = game.GameStartDate,                           
                           UseCardTime = game.UseCardTime
                       };
        }

        Game.AvatarTurnSettingsInfo GetAvatarSettings(DataModel.SqlRepository.Game game)
        {
            return new Game.AvatarTurnSettingsInfo()
                       {
                           UseActualAnsweringTimes = game.UseActualAvatarAnsweringTimes,
                           UseActualName = game.UseActualAvatarNames,
                           UseHistoricAnswers = game.UseHistoricAvatarAnswers
                       };
        }

        Game.PlayerTurnSettingsInfo GetPlayerTurnSettings(DataModel.SqlRepository.Game game)
        {
            return new Game.PlayerTurnSettingsInfo()
                       {
                           BySequenceNoElseRandom = game.PlayerTurn_ByPlayerSequenceNoElseRandom,
                           RandomNoRepeatingUntilAllHavePlayed = game.PlayerTurn_RandomNoRepeatingUntilAllHavePlayed
                       };
        }

        #endregion

        #region Cards

        IEnumerable<CardGroup> GetCardGroups(int gameId, int languageId)
        {


            var dbCardGroups = DataContext.GameCardGroupSequences.Where(g => g.GameID == gameId).ToList();
            var currentGameTemplateId = dbCardGroups.FirstOrDefault().Game.GameID_Template;

            var avatarPlayers = dbCardGroups.FirstOrDefault().Game.GamePlayers.Where(g=>g.IsAvatar);

            List<int> randomAvatarGameIds = new List<int>();
            foreach (var avatarPlayer in avatarPlayers)
            {
                var games = DataContext.GamePlayers
                    .Where(gp => gp.UserID == avatarPlayer.UserID && 
                        gp.Game.GameID_Template == currentGameTemplateId && 
                        gp.Game.Results.Count > 0 && 
                        gp.GameID != gameId && !gp.IsAvatar && gp.Game.GameCompletionDate != null)                    
                    .Select(gp => gp.Game.GameID).ToList();

                if(games.Count <=0)
                {
                    continue;                    
                }

                var random = new Random();
                int index = random.Next(0, games.Count - 1);
                var avatarGameId = games[index];
                randomAvatarGameIds.Add(avatarGameId);               
            }

            var cards = GetCards(gameId, languageId, randomAvatarGameIds).ToList();

            var cardGroups =
                (from g in dbCardGroups
                 where g.GameID == gameId
                 select new CardGroup()
                            {
                                Id = g.GameCardGroupSequenceID,
                                CardGroupSequenceNo = g.CardGroupSequenceNo,
                                Cards = cards
                                    .Where(c => c.CardGroupSequenceId == g.GameCardGroupSequenceID).OrderBy(
                                        c => c.CardSequenceNo).ToList(),
                                IsMandatoryGroup = g.IsAMandatoryCardGroup
                            }).OrderBy(c => c.CardGroupSequenceNo).ToList();
                         
            return cardGroups;
        }
        

        IQueryable<Domain.Model.CardReferenceMaterial> GetReferenceMaterial(int cardId)
        {
            return from r in DataContext.CardReferenceMaterials
                   where r.CardID == cardId
                   select new Domain.Model.CardReferenceMaterial
                   {
                       //Url = r.CardReferenceMaterialUrl
                   };
        }

        #endregion

        #region Players

        Player GetPlayer(Guid userId)
        {
            return DataContext.Users
                .Where(u => u.UserId == userId)
                .Select(p => new Player
                                 {
                                     AvatarData = p.PlayerAvatarData,
                                     AvatarGender = p.PlayerAvatarGender,
                                     AvatarName = p.PlayerAvatarName,
                                     HasViewedGameTutorial = false,
                                     Id = p.UserId,
                                     IsSimulated = false,
                                     RealName = p.UserFirstName + " " + p.UserLastName,
                                     PlayerFirstName = p.UserFirstName,
                                     PlayerLastName = p.UserLastName
                                 }).FirstOrDefault();
        }

        IQueryable<Player> GetGamePlayers(int gameId)
        {
            return (from p in DataContext.GamePlayers
                    join u in DataContext.Users on p.UserID equals u.UserId
                    where p.GameID == gameId
                    select
                        new Player
                            {
                                Id = p.UserID,
                                AvatarName = u.PlayerAvatarName,
                                AvatarData = u.PlayerAvatarData,
                                AvatarGender = u.PlayerAvatarGender,
                                HasViewedGameTutorial = u.HasViewedGameTutorial,
                                IsSimulated = p.IsAvatar,
                                RealName = u.UserFirstName + " " + u.UserLastName,
                                PlayerFirstName = u.UserFirstName,
                                PlayerLastName = u.UserLastName
                            }).Distinct().OrderBy(p => p.IsSimulated);
        }


        List<GamePlayer> GetRandomAvatarsForOnlineGame(int templateGameId, Guid excludeUserId)
        {
            var results = new List<GamePlayer>();

            var avatarPlayers = (from gp in DataContext.GamePlayers
                                 join g in DataContext.Games on gp.GameID equals g.GameID
                                 where g.GameID_Template == templateGameId && gp.UserID != excludeUserId && g.GameCompletionDate != null
                                 select gp)
                .Distinct()
                .OrderBy(a => Guid.NewGuid()); //used to create some randomness in order

            var count = avatarPlayers.Count();           

            if(count <= 0)
            {
                return results;
            }

            while (results.Count <= count && results.Count < 3)
            {                                            
                var index = new Random().Next(count); //used to create even more randomness

                var tempPlayer = avatarPlayers.Skip(index).First();
                var newPlayer = new GamePlayer()
                                    {
                                        UserID = tempPlayer.UserID,                                        
                                        IsAvatar = true
                                    };

                if(results.Any(p=> p.UserID == newPlayer.UserID))
                {
                    continue;                    
                }

                results.Add
                    (
                        new GamePlayer()
                            {
                                UserID = tempPlayer.UserID,
                            }
                    );                
            }

            return results;
        }

        #endregion

        #endregion

    }
}
