﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.DataModel.SqlRepository;
using Language = Diversity.Domain.Model.Language;

namespace Diversity.Data.DataAccess.SqlServer
{
    public class SqlLanguageRepository : SqlRepository, ILanguageRepository
    {
        
        public IQueryable<Language> GetLanguages()
        {
            return from l in DataContext.Languages
                   select new Language
                              {
                                  Id = l.LanguageID,
                                  IsLeftToRight = l.IsLeftToRight,
                                  LCID = l.LCID,
                                  Name = l.Language1
                              };
        }
    }
}
