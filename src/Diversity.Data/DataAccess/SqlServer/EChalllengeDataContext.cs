﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Diversity.Common;

namespace Diversity.DataModel.SqlRepository
{
    public partial class EChallengeDataContext
    {
        private Guid _contextUserId;
        
        public EChallengeDataContext(Guid userId): base(ConfigurationHelper.ConnectionString)
        {
            _contextUserId = userId;
            Database.Connection.StateChange += OnConnectionStateChange;
        }

        private void OnConnectionStateChange(object sender, StateChangeEventArgs e)
        {
            if (e.OriginalState == ConnectionState.Open || e.CurrentState != ConnectionState.Open)
            {
                return;
            }

            var connection = sender as IDbConnection;

            var command = connection.CreateCommand();
            command.CommandText = "upContextInfoSet";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("contextUserID", _contextUserId));
            command.ExecuteNonQuery();
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                //Get the error messages.
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(c => c.ValidationErrors)
                    .Select(c => c.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);             
            }            
        }
    }
}
