﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Common.Enumerations;
using Diversity.Domain.Model;
using Card = Diversity.DataModel.SqlRepository.Card;

namespace Diversity.Data.DataAccess.SqlServer
{
    public partial class SqlGameRepository
    {
        private IQueryable<Statistic> GetStatistics(Card card)
        {
            IQueryable<Statistic> results = null;

            if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementCard) ||
               card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementImageCard))
            {
                return GetAuditStatementStatistics(card);
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MatchingListQuestionCard))
            {
                return GetMatchingListStatistics(card);
            }

            return results;
        }

        public IQueryable<Statistic> GetAuditStatementStatistics(Card card)
        {
            var stats = (from c in DataContext.AuditStatementAnswerStatistics
                         where c.CardID == card.CardID
                         select new AuditStatementAnswerStatistic()
                                    {
                                        AnswerCount = c.AnswerCount ?? 0,
                                        CardId = c.CardID,
                                        GameTemplateId = c.GameID_Template ?? 0,
                                        GeneralAnswerId = c.QuestionCard_GeneralAnswerID
                                    }).ToList();

            var totalAnswers = 0;

            if (!stats.Any() || !card.QuestionCard.MustShowCompanyResults)
            {
                return new List<AuditStatementAnswerStatistic>().AsQueryable();
            }

            totalAnswers = stats.Sum(s => s.AnswerCount);

            if (totalAnswers <= 0 || totalAnswers < card.QuestionCard.MinDataPointsForCompanyResult)
            {
                return new List<AuditStatementAnswerStatistic>().AsQueryable();
            }


            foreach (var auditStatementAnswerStatistic in stats)
            {
                auditStatementAnswerStatistic.AnswerPercentage =
                    Convert.ToDouble(auditStatementAnswerStatistic.AnswerCount) / Convert.ToDouble(totalAnswers);
            }


            return stats.AsQueryable();
        }

        public IQueryable<Statistic> GetMatchingListStatistics(Card card)
        {
            var resultStats = new List<MatchingListAnswerStatistic>();

            var stats = (from c in DataContext.MatchingListAnswerStatistic
                         where c.CardID == card.CardID
                         select new 
                         {
                             AnswerCount = c.AnswerCount ?? 0,
                             CardId = c.CardID,
                             GameTemplateId = c.GameID_Template ?? 0,
                             GeneralAnswerId = c.QuestionCard_GeneralAnswerID,
                             LHAnswerId = c.LHAnswerId,
                             RHAnswerId = c.RHAnswerId
                         }).ToList();

            var answerIds = card.QuestionCard.QuestionCard_GeneralAnswers.Select(g => g.QuestionCard_GeneralAnswerID).ToList();            

            if (!stats.Any() || !answerIds.Any() || !card.QuestionCard.MustShowCompanyResults)
            {
                return new List<MatchingListAnswerStatistic>().AsQueryable();    
            }

            var answerCount = answerIds.Count();
            var gameTemplateId = stats.Select(s => s.GameTemplateId).FirstOrDefault();
            var totalAnswerCount = stats.Sum(s => s.AnswerCount);

            if (totalAnswerCount <= 0 || totalAnswerCount < card.QuestionCard.MinDataPointsForCompanyResult)
            {
                return new List<MatchingListAnswerStatistic>().AsQueryable();
            }

            for (int i = 0; i < answerCount; i++)
            {
                var answerId = answerIds[i];

                for (int j = 0; j < answerCount; j++)
                {
                    var result = new MatchingListAnswerStatistic()
                                 {
                                     AnswerCount = 0,
                                     AnswerPercentage = 0,
                                     CardId = card.CardID,
                                     GameTemplateId = gameTemplateId,
                                     GeneralAnswerId = answerId,
                                     LHAnswerId = answerIds[j],
                                     RHAnswerId = answerId
                                 };

                    if (stats.Any(s => s.LHAnswerId == result.LHAnswerId && s.RHAnswerId == result.RHAnswerId))
                    {
                        var stat =
                            stats.FirstOrDefault(
                                s => s.LHAnswerId == result.LHAnswerId && s.RHAnswerId == result.RHAnswerId);

                        result.AnswerCount = stat.AnswerCount;
                        result.AnswerPercentage = Convert.ToDouble(stat.AnswerCount) / Convert.ToDouble(totalAnswerCount);
                    }

                    resultStats.Add(result);
                }
            }

            return resultStats.AsQueryable();
        }
    }
}
