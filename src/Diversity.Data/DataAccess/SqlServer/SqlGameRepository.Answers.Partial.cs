﻿using System;
using System.Linq;
using Diversity.DataModel.SqlRepository;
using Diversity.Common.Enumerations;
using Diversity.Domain.Model;
using Card = Diversity.DataModel.SqlRepository.Card;


namespace Diversity.Data.DataAccess.SqlServer
{
    public partial class SqlGameRepository
    {
        IQueryable<Answer> GetPossibleAnswers(Card card, int languageId, bool useLongVersion)
        {
            IQueryable<Answer> results = null;


            //if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.ArrangeSequenceQuestionCard))
            //{
            //    results = GetArrangeSequenceAnswers(card.QuestionCard, languageId, useLongVersion);
            //}
            //else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.FillInTheBlankQuestionCard))
            //{
            //    results = GetFillInTheBlankAnswers(card.QuestionCard, languageId, useLongVersion);
            //}
            //else 
            if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MatchingListQuestionCard))
            {
                results = GetMatchingListAnswers(card.QuestionCard, languageId, useLongVersion);
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxQuestionCard))
            {
                results = GetMultipleCheckBoxAnswers(card.QuestionCard, languageId, useLongVersion);
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceQuestionCard))
            {
                results = GetMultipleChoiceAnswers(card.QuestionCard, languageId, useLongVersion);
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceImageQuestionCard) ||
                card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementImageCard))
            {
                results = GetMultipleChoiceMultimediaAnswers(card.QuestionCard, languageId, useLongVersion);
            }
            //else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.NumericQuestionCard))
            //{
            //    results = GetNumericAnswers(card.QuestionCard, languageId);
            //}
            else if ((card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceSurveyCard)) ||
                (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxSurveyCard)) ||
                (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxPriorityFeedbackCard)) ||
                (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoicePriorityFeedbackCard)) ||
                (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.PriorityIssueVotingCard)) ||
                (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoicePersonalActionPlanCard)) ||
                (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckboxPersonalActionPlanCard)) ||
                (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementCard)))
            {
                results = GetStatementAnswers(card.QuestionCard, languageId, useLongVersion);
            }
            else if (card.Internal_CardType.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxImageQuestionCard))
            {
                results = GetMultipleCheckBoxImageAnswers(card.QuestionCard, languageId, useLongVersion);
            }

            return results;
        }

        IQueryable<ArrangeSequenceAnswer> GetArrangeSequenceAnswers(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let possibleAnswer =
                              useLongVersion ? al.PossibleAnswer_LongVersion : al.PossibleAnswer_ShortVersion
                          select new ArrangeSequenceAnswer()
                          {
                              AnswerSequenceNo = a.SequenceNo,
                              Id = a.QuestionCard_GeneralAnswerID,
                              IsCorrectAnswer = a.IsCorrectAnswer,
                              PossibleAnswer = possibleAnswer
                          };

            return results.AsQueryable();
        }

        IQueryable<FillInTheBlankAnswer> GetFillInTheBlankAnswers(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let possibleAnswer =
                              useLongVersion ? al.PossibleAnswer_LongVersion : al.PossibleAnswer_ShortVersion
                          select new FillInTheBlankAnswer()
                          {
                              Id = a.QuestionCard_GeneralAnswerID,
                              IsCorrectAnswer = a.IsCorrectAnswer,
                              PossibleAnswer = possibleAnswer
                          };

            return results.AsQueryable();
        }

        IQueryable<MatchingListAnswer> GetMatchingListAnswers(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let rhStatement = useLongVersion ? al.RHStatement_LongVersion : al.RHStatement_ShortVersion
                          let lhStatement =
                              useLongVersion ? al.LHStatement_LongVersion : al.LHStatement_ShortVersion
                          select new MatchingListAnswer()
                          {
                              Id = a.QuestionCard_GeneralAnswerID,
                              IsCorrectAnswer = a.IsCorrectAnswer,
                              LHStatement = lhStatement,
                              RHStatement = rhStatement
                          };

            return results.AsQueryable();
        }


        IQueryable<MultipleCheckBoxAnswer> GetMultipleCheckBoxAnswers(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let possibleAnswer = useLongVersion ? al.PossibleAnswer_LongVersion : al.PossibleAnswer_ShortVersion
                          select new MultipleCheckBoxAnswer()
                          {
                              Id = a.QuestionCard_GeneralAnswerID,
                              IsCorrectAnswer = a.IsCorrectAnswer,
                              PossibleAnswer = possibleAnswer
                          };

            return results.AsQueryable();
        }

        IQueryable<MultipleChoiceAnswer> GetMultipleChoiceAnswers(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let possibleAnswer = useLongVersion ? al.PossibleAnswer_LongVersion : al.PossibleAnswer_ShortVersion
                          select new MultipleChoiceAnswer()
                          {
                              Id = a.QuestionCard_GeneralAnswerID,
                              IsCorrectAnswer = a.IsCorrectAnswer,
                              PossibleAnswer = possibleAnswer
                          };

            return results.AsQueryable();

        }

        IQueryable<MultipleCheckBoxImageAnswer> GetMultipleCheckBoxImageAnswers(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let possibleAnswer = useLongVersion ? al.PossibleAnswer_LongVersion : al.PossibleAnswer_ShortVersion
                          select new MultipleCheckBoxImageAnswer()
                          {
                              Id = a.QuestionCard_GeneralAnswerID,
                              IsCorrectAnswer = a.IsCorrectAnswer,
                              PossibleAnswer = possibleAnswer,
                              PossibleAnswerMultimediaURL = al.PossibleAnswerMultimediaURL,
                              PossibleAnswerMultimediaURLFriendlyName = al.PossibleAnswerMultimediaURLFriendlyName
                          };

            return results.AsQueryable();
        }
            
        IQueryable<MultipleChoiceMultimediaAnswer> GetMultipleChoiceMultimediaAnswers(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let possibleAnswer = useLongVersion ? al.PossibleAnswer_LongVersion : al.PossibleAnswer_ShortVersion
                          select new MultipleChoiceMultimediaAnswer()
                          {
                              Id = a.QuestionCard_GeneralAnswerID,
                              IsCorrectAnswer = a.IsCorrectAnswer,
                              PossibleAnswer = possibleAnswer,
                              PossibleAnswerMultimediaURL = al.PossibleAnswerMultimediaURL,
                              PossibleAnswerMultimediaURLFriendlyName = al.PossibleAnswerMultimediaURLFriendlyName
                          };

            return results.AsQueryable();

        }

        IQueryable<NumericAnswer> GetNumericAnswers(QuestionCard questionCard, int languageId)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let operand = GetOperand(a)
                          select new NumericAnswer()
                          {
                              Id = a.QuestionCard_GeneralAnswerID,
                              IsCorrectAnswer = a.IsCorrectAnswer,
                              Operand = operand,
                              Value1 = Convert.ToSingle(al.PossibleAnswer_LongVersion),
                              Value2 = Convert.ToSingle(al.PossibleAnswer_ShortVersion)
                          };

            return results.AsQueryable();
        }

        IQueryable<StatementAnswer> GetStatementAnswers(QuestionCard questionCard, int languageId, bool useLongVersion)
        {
            var results = from a in questionCard.QuestionCard_GeneralAnswers
                          from al in a.QuestionCard_GeneralAnswer_Languages
                          where al.LanguageID == languageId
                          let possibleAnswer = useLongVersion ? al.PossibleAnswer_LongVersion : al.PossibleAnswer_ShortVersion
                          select new StatementAnswer()
                          {
                              Id = a.QuestionCard_GeneralAnswerID,
                              PossibleAnswer = possibleAnswer
                          };

            return results.AsQueryable();
        }


        string GetOperand(QuestionCard_GeneralAnswer answer)
        {
            var operand = string.Empty;

            if (answer.IsBetween)
            {
                operand = "IsBetween";
            }
            else if (answer.IsEqualTo)
            {
                operand = "IsEqualTo";
            }
            else if (answer.IsGreaterThan)
            {
                operand = "IsGreaterThan";
            }
            else if (answer.IsGreaterThanOrEqualTo)
            {
                operand = "IsGreaterThanOrEqualTo";
            }
            else if (answer.IsLessThan)
            {
                operand = "IsLessThan";
            }
            else if (answer.IsLessThanOrEqualTo)
            {
                operand = "IsLessThanOrEqualTo";
            }
            else if (answer.IsNotBetween)
            {
                operand = "IsNotBetween";
            }
            else if (answer.IsNotEqualTo)
            {
                operand = "IsNotEqualTo";
            }
            return operand;
        }

    }
}
