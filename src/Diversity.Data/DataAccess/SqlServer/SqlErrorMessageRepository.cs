﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Domain.Model;

namespace Diversity.Data.DataAccess.SqlServer
{
    public class SqlErrorMessageRepository : SqlRepository, IErrorMessageRepository
    {
        
        public IEnumerable<ErrorMessage> GetErrorMessages()
        {
            return from e in DataContext.ErrorMessages
                   join el in DataContext.ErrorMessage_Languages on e.ErrorMessageID equals el.ErrorMessageID
                   join l in DataContext.Languages on el.LanguageID equals l.LanguageID
                   select new ErrorMessage()
                              {
                                  Code = e.ErrorCode,
                                  Description = el.ErrorMessageDescription,
                                  Id = e.ErrorMessageID,
                                  Language = new Language()
                                                 {
                                                     Id = l.LanguageID,
                                                     IsLeftToRight = l.IsLeftToRight,
                                                     LCID = l.LCID,
                                                     Name = l.Language1
                                                 }
                              };
        }        
    }
}
