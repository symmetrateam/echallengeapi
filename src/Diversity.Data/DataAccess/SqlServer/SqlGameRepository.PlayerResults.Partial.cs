﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using Diversity.Common.Enumerations;
using Diversity.Domain.Model;

namespace Diversity.Data.DataAccess.SqlServer
{
    public partial class SqlGameRepository
    {
        List<XElement> GetAllPlayerResults(DataModel.SqlRepository.Game game, DataModel.SqlRepository.Card card, IEnumerable<DataModel.SqlRepository.Game> avatarGames)
        {
            var playerResults = 
                 (from r in game.Results
                    where r.CardID == card.CardID
                    select r.ResultXmlElement).ToList();

            var avatarResults = (from g in avatarGames
                                 from r in g.Results
                                 where r.CardID == card.CardID
                                 select r.ResultXmlElement).ToList();
                        
            return playerResults.Union(avatarResults).ToList();            
        }

        List<XElement> GetCurrentPlayerResults(DataModel.SqlRepository.Game game, DataModel.SqlRepository.Card card)
        {
            var results =
                 (from r in game.Results
                  where r.CardID == card.CardID
                  select r.ResultXmlElement).ToList();

            return results;
        }
    }
}
