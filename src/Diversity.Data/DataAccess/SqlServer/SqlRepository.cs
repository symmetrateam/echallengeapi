﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Common;
using Diversity.DataModel.SqlRepository;
using System.Data;

namespace Diversity.Data.DataAccess.SqlServer
{
    /// <summary>
    /// The base class for all repositories
    /// </summary>
    public abstract class SqlRepository : IRepository
    {
        private EChallengeDataContext _dataContext;

        protected SqlRepository()
        {
            _dataContext = new EChallengeDataContext();           
        }

        public EChallengeDataContext DataContext
        {
            get { return _dataContext; }
        }

        //public EChallengeDataContext CreateContext()
        //{
        //    return new EChallengeDataContext();
        //}

        //public EChallengeDataContext CreateContext(IDbConnection connection)
        //{
        //    _dataContext = new EChallengeDataContext(connection);
        //    return _dataContext;
        //}

        //public EChallengeDataContext GetContext()
        //{
        //    return _dataContext;
        //}
    }
}
