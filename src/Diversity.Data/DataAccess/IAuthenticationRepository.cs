﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.Domain.Model;

namespace Diversity.Data.DataAccess
{
    public interface IAuthenticationRepository
    {
        AuthenticationTicket GetAuthenticationTicket(Guid userId);
        AuthenticationTicket GetAuthenticationTicket(string authenticationToken);
        bool CreateAuthenticationTicket(AuthenticationTicket ticket);
        bool UpdateAuthenticationTicket(AuthenticationTicket ticket);
        Guid GetScormUserId(string username, string clientCode);
        bool AddUserToClient(Guid userId, string clientCode);
        bool AddUserToAllUsersClientUserGroup(Guid userId, string clientCode);
        bool ClientExists(string clientCode);

    }
}
