﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Data.DataAccess
{
    public interface IDistributorRepository
    {
        IQueryable<Distributor> GetDistributors();
        IQueryable<Distributor> GetDistributorsForUser(Guid userId);
    }
}
