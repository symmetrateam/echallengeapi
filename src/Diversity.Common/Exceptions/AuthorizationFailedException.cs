﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Common.Exceptions
{
    [DataContract(Namespace = "")]
    public class AuthorizationFailedException
    {
        public AuthorizationFailedException() { }

        public AuthorizationFailedException(string message)
        {
            Message = message;
        }

        [DataMember]
        public string Message { get; set; }
    }
}
