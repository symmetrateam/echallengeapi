﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Common.Exceptions
{
    [DataContract(Namespace = "")]
    public class RequiredValidationException
    {
        public RequiredValidationException() { }

        public RequiredValidationException(string message)
        {
            Message = message;
        }

        public RequiredValidationException(string parameterName, string message)
        {
            Message = string.Format(@"parameter {0}: {1}", parameterName, message);
        }          
       
        [DataMember]
        public string Message { get; set; }   
    }
}
