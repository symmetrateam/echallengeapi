﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diversity.Common.Exceptions
{
    public class InvalidAnswerException : Exception
    {
        public InvalidAnswerException() { }

        public InvalidAnswerException(string message) : base(message) { }

        public InvalidAnswerException(string message, Exception innerException) : base(message, innerException) { }
    }
}
