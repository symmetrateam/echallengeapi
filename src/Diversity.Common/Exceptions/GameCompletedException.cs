﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diversity.Common.Exceptions
{
    public class GameCompletedException : Exception
    {
        public GameCompletedException() { }

        public GameCompletedException(string message) : base(message) { }

        public GameCompletedException(string message, Exception innerException) : base(message, innerException) { }
    }
}
