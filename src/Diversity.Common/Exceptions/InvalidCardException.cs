﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diversity.Common.Exceptions
{
    public class InvalidCardException : Exception
    {
        public InvalidCardException() { }

        public InvalidCardException(string message) : base(message) { }

        public InvalidCardException(string message, Exception innerException) : base(message, innerException) { }
    }
}
