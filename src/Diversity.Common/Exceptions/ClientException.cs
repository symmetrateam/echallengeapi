﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Diversity.Common.Exceptions
{
    
        [DataContract(Namespace = "")]
        public class ClientException
        {
            public ClientException()
            {
            }

            public ClientException(string message)
            {
                Message = message;
            }

            [DataMember]
            public string Message { get; set; }
        }
    
}
