﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Common.Exceptions
{
    [DataContract(Namespace = "")]
    public class CustomValidationException
    {
        public CustomValidationException() { }

        public CustomValidationException(string message)
        {
            Message = message;
        }

        public CustomValidationException(string parameterName, string message)
        {
            Message = string.Format(@"parameter {0}: {1}", parameterName, message);
        }          
       
        [DataMember]
        public string Message { get; set; }
    }
}
