﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diversity.Common.Exceptions
{
    public enum ErrorCodes
    {
        None,
        Login_Failed,
        Login_Failed_Scorm,
        Account_Not_Activated,
        Account_Locked,
        Unauthorized_Forbidden,
        String_Greater_Than_Zero,
        String_Equal_To_One,
        Argument_Null_Exception,
        Argument_Out_Of_Range,
        Unhandled_Exception,
        Minimum_Password_Requirement,
        Password_Change_Old_Password_Failed,
        Password_Reset_Email_Failed,
        Password_Reset_Email_Sent,
        Invalid_Game_Provided,
        Invalid_Theme_Code,
        Unable_To_Complete_Game,

        Invalid_Username,
        Invalid_Email,
        Invalid_CardSequence_Number_Provided,        
        Invalid_Answer_Provided,
        Invalid_Client_Code,
        Change_Password_Failed,
        Session_Timeout_Exception,
        Unable_To_Retrieve_Game_Session,
        Unable_To_Retrieve_Dashboard,
        Unable_To_Save_Player_Profile,
        Unable_To_Save_Avatar_Profile,
        Unable_To_Save_Card_Result,
        Unable_To_Skip_Card,
        Game_Cannot_Be_Played,
        Unable_To_Set_Game_As_Completed,
        Game_Already_Completed,
        Maximum_Games_Played,
        Unable_To_Auto_Invite_User,
        Unable_To_Create_User
    }
}
