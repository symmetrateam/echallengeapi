﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Common.Exceptions
{
    [DataContract(Namespace = "")]
    public class SessionTimeoutException
    {
        public SessionTimeoutException() { }

        public SessionTimeoutException(string message)
        {
            Message = message;
        }

        [DataMember]
        public string Message { get; set; }
    }
}
