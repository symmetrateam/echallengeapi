﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Common.Exceptions
{
    [DataContract(Namespace = "")]
    public class AuthenticationFailedException
    {
        public AuthenticationFailedException() { }

        public AuthenticationFailedException(string message)
        {
            Message = message;
        }

        [DataMember]
        public string Message { get; set; }
    }
}
