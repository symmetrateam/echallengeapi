﻿using System.Runtime.Serialization;

namespace Diversity.Common.Enumerations
{
    [DataContract]
    public enum CardType
    {
        [EnumMember]
        Unknown = 0,

        [EnumMember]
        ArrangeSequenceQuestionCard = 1,

        [EnumMember]
        FillInTheBlankQuestionCard = 2,

        [EnumMember]
        MatchingListQuestionCard = 3,

        [EnumMember]
        NumericQuestionCard = 4,

        [EnumMember]
        PotLuckQuestionCard = 5,

        [EnumMember]
        MultipleChoiceQuestionCard = 6,

        [EnumMember]
        MultipleCheckBoxQuestionCard = 7,

        [EnumMember]
        LcdCategoryCard = 8,

        [EnumMember]
        MultipleChoiceAuditStatementCard = 9,

        [EnumMember]
        CompanyPolicyCard = 10,

        [EnumMember]
        LeadingQuestionCard = 11, //display card

        [EnumMember]
        TextAreaPriorityFeedbackCard = 12,

        [EnumMember]
        PriorityIssueVotingCard = 13,

        [EnumMember]
        MultipleChoiceSurveyCard = 14,

        [EnumMember]
        MultipleCheckBoxSurveyCard = 15,

        [EnumMember]
        TextBoxSurveyCard = 16,

        [EnumMember]
        TextAreaSurveyCard = 17,

        [EnumMember]
        MultipleChoicePriorityFeedbackCard = 18,

        [EnumMember]
        MultipleCheckBoxPriorityFeedbackCard = 19,

        [EnumMember]
        MultipleChoicePersonalActionPlanCard = 20,

        [EnumMember]
        MultipleCheckboxPersonalActionPlanCard = 21,

        [EnumMember]
        TextBoxPersonalActionPlanCard = 22,

        [EnumMember]
        TextAreaPersonalActionPlanCard = 23,

        [EnumMember]
        RoundResultCard = 24, //display

        [EnumMember]
        ScoreboardCard = 25, //display

        [EnumMember]
        AwardCard = 26, //display

        [EnumMember]
        CertificateCard = 27, //display

        [EnumMember]
        GameCompletedCard = 28,

        [EnumMember]
        IntroSurveyCard = 29, //display

        [EnumMember]
        TransitionUpCard = 30, //display

        [EnumMember]
        TransitionDownCard = 31, //display

        [EnumMember]
        MultipleChoiceImageQuestionCard = 32,

        [EnumMember]
        MultipleChoiceAuditStatementImageCard = 33,

        [EnumMember]
        GameIntroCard = 34,  //display

        [EnumMember]
        ThemeIntroCard = 35,

        [EnumMember]
        MultiMediaCard = 36,

        [EnumMember]
        MultipleCheckBoxImageQuestionCard = 37,

        [EnumMember]
        BranchingCard = 38
    }
}