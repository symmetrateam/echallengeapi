﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diversity.Common.Enumerations
{
    public enum MultimediaType
    {
        Image,
        Video
    }
}
