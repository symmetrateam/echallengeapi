﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Diversity.Common
{
    public class ConfigurationHelper
    {
        private const string LIVE_UPLOAD_FOLDER = "~/Upload/Live";
        private const string TEMP_UPLOAD_FOLDER = "~/Upload/Temp";
        private const string EMAIL_VALIDATION_REGEX = "";

        public static bool SmtpEnableSsl
        {
            get { return bool.Parse(ConfigurationManager.AppSettings["SmtpEnableSsl"]); }
        }

        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["EChallengeDataContext"].ConnectionString; }
        }

        public static string TemporaryUploadLocation
        {
            get { return TEMP_UPLOAD_FOLDER; }
        }

        public static string LiveUploadLocation
        {
            get { return LIVE_UPLOAD_FOLDER; }
        }

        public static string DefaultCultureLanguage
        {
            get { return ConfigurationManager.AppSettings["DefaultCultureLanguage"]; }   
        }

        public static string SecurityServiceUrl
        {
            get { return ConfigurationManager.AppSettings["SecurityServiceUrl"]; }
        }

        public static string GameServiceUrl
        {
            get { return ConfigurationManager.AppSettings["GameServiceUrl"]; }
        }

        public static string GameUrl
        {
            get { return ConfigurationManager.AppSettings["GameUrl"]; }
        }

        public static string SystemPlayer1
        {
            get { return ConfigurationManager.AppSettings["SystemPlayer1"]; }
        }

        public static string SystemPlayer2
        {
            get { return ConfigurationManager.AppSettings["SystemPlayer2"]; }
        }

        public static string SystemPlayer3
        {
            get { return ConfigurationManager.AppSettings["SystemPlayer3"]; }
        }

        public static string AdminAccountName
        {
            get { return ConfigurationManager.AppSettings["AdminAccountName"]; }
        }

        public static int TimeoutInMinutes
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["TimeoutInMinutes"]); }
        }

        public static string EmailValidationRegex
        {
            get { return EMAIL_VALIDATION_REGEX; }
        }
    }
}
