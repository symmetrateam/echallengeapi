﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Diversity.Common.Extensions
{
    public static class DateTimeExtension
    {
        /// <summary>
        /// Sets the datetime to the beginning of the day
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToDayStart(this DateTime dateTime)
        {
            return dateTime.Date;
        }

        /// <summary>
        /// Sets the datetime to the end of the day
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToDayEnd(this DateTime dateTime)
        {
            return dateTime.Date.AddDays(1).AddMilliseconds(-1);
        }

        /// <summary>
        /// Returns the start of the week based on the start date
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="startOfWeek"></param>
        /// <returns></returns>
        public static DateTime ToWeekStart(this DateTime dateTime, DayOfWeek startOfWeek)
        {
            int diff = dateTime.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dateTime.AddDays(-1 * diff).Date;
        }

        public static DateTime ToWeekEnd(this DateTime dateTime, DayOfWeek startOfWeek)
        {
            var weekStartDate = dateTime.ToWeekStart(startOfWeek);

            return weekStartDate.AddDays(7).AddMilliseconds(-1);
        }

        public static DateTime ToWeekEnd(this DateTime dateTime, CultureInfo cultureInfo)
        {
            return ToWeekEnd(dateTime, cultureInfo.DateTimeFormat.FirstDayOfWeek);
        }

        public static DateTime ToWeekEnd(this DateTime dateTime)
        {
            var cultureInfo = CultureInfo.CurrentCulture;
            return ToWeekEnd(dateTime, cultureInfo.DateTimeFormat.FirstDayOfWeek);
        }


        public static DateTime ToWeekStart(this DateTime dateTime, CultureInfo cultureInfo)
        {
            return ToWeekStart(dateTime, cultureInfo.DateTimeFormat.FirstDayOfWeek);
        }

        public static DateTime ToWeekStart(this DateTime dateTime)
        {
            var cultureInfo = CultureInfo.CurrentCulture;
            return ToWeekStart(dateTime, cultureInfo.DateTimeFormat.FirstDayOfWeek);
        }


        /// <summary>
        /// Gets the start of the month
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToMonthStart(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        /// <summary>
        /// Gets the end of the month
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToMonthEnd(this DateTime dateTime)
        {
            var firstDayOfMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            return firstDayOfMonth.AddMonths(1).AddMilliseconds(-1);
        }

        public static DateTime RoundToSeconds(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
        }

        public static string ToMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTime.Month);
        }

        public static string ToShortMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(dateTime.Month);
        }


        public static DateTime FloorToMilliseconds(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, 0);
        }
    }
}
