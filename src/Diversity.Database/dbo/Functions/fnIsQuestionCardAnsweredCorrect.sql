﻿CREATE FUNCTION dbo.fnIsQuestionCardAnsweredCorrect 
(
	@ResultID INT
)
RETURNS BIT
AS
BEGIN	

DECLARE @IsCorrect BIT
DECLARE @ResultType NVARCHAR(50)
DECLARE @CardID INT
DECLARE @CardTypeGroup NVARCHAR(50)
DECLARE @Xml XML


SELECT @ResultType = ResultType
    , @CardID = CardID
    , @CardTypeGroup = Internal_CardTypeGroup
    , @Xml = ResultXml
FROM Result 
INNER JOIN Internal_CardType ON Result.Internal_CardTypeID = Internal_CardType.Internal_CardTypeID
WHERE ResultID = @ResultID


IF @CardTypeGroup != 'Question'
BEGIN
    SET @IsCorrect = NULL
    RETURN @IsCorrect
END

IF @ResultType = 'MultipleChoicePlayerResult'
BEGIN
	   SET @IsCorrect = 0
	   
	   DECLARE @AnswerID INT
    
	   SELECT @AnswerID = QuestionCard_GeneralAnswerID
	   FROM QuestionCard_GeneralAnswer
	   WHERE QuestionCardID = @CardID AND IsCorrectAnswer = 1

	   DECLARE @UserAnswerID INT

	   SELECT @UserAnswerID = @xml.value('(/MultipleChoicePlayerResult//AnswerId/node())[1]', 'int')

	   IF @AnswerID = @UserAnswerID
	   BEGIN
		  SET @IsCorrect = 1
	   END
END

IF @ResultType = 'MultipleCheckBoxPlayerResult'
BEGIN 
	   SET @IsCorrect = 0
	   DECLARE @CountAnswers INT
	   SELECT @CountAnswers = COUNT(*)
	   FROM QuestionCard_GeneralAnswer
	   WHERE QuestionCardID = @CardID AND IsCorrectAnswer = 1

	   DECLARE @UserCountAnswers INT

	   SELECT @UserCountAnswers =  COUNT(*)
	   FROM @Xml.nodes('/MultipleCheckBoxPlayerResult/Results/PlayerResultAnswer/AnswerId') As Answers(pref)

	   IF @CountAnswers = @UserCountAnswers
	   BEGIN

		  DECLARE @Total INT
		  SELECT @Total =  COUNT(*)
		  FROM
		  (
			 SELECT QuestionCard_GeneralAnswerID AnswerID
			 FROM QuestionCard_GeneralAnswer
			 WHERE QuestionCardID = @CardID AND IsCorrectAnswer = 1
			 INTERSECT
			 SELECT pref.value('(node())[1]', 'int') AS AnswerId
			 FROM @Xml.nodes('/MultipleCheckBoxPlayerResult/Results/PlayerResultAnswer/AnswerId') AS Answers(pref)			
		  ) A

		  IF @Total = @CountAnswers
		  BEGIN
			 SET @IsCorrect = 1
		  END
		  
	   END
END

IF @ResultType = 'MatchingListPlayerResult'
BEGIN
     SET @IsCorrect = 0
	DECLARE @UserAnswerCorrect BIT
     SELECT @UserAnswerCorrect = @xml.value('(/MatchingListPlayerResult/IsAnswerCorrect/node())[1]', 'bit')
	
	SET @IsCorrect = @UserAnswerCorrect
END

RETURN @IsCorrect

END
