﻿CREATE VIEW vwReportAnswers
AS
SELECT DISTINCT
	 Client.ClientID
	,Client.ClientName
	,ClientUserGroup.ClientUserGroupID
	,ClientUserGroup.ClientUserGroupName
	,aspnet_Users.UserId
	,aspnet_users.UserFirstName + ' ' + aspnet_users.UserLastName AS 'User'	
	,result.GameID
	,game.GameID_Template
	,ResultType
	,Internal_CardType.Internal_CardTypeName
	,replace(REPLACE(REPLACE(Question_Statement_ShortVersion, CHAR(13) + CHAR(10), ''), CHAR(9), ''), CHAR(10), '') AS Question_Statement_ShortVersion
	,result.ResultID	
	,CASE 
		WHEN ResultType = 'MultipleChoicePlayerResult'
			THEN (
					SELECT CASE 
							WHEN Internal_CardType.Internal_CardTypeShortCode = 'MultipleChoiceImageQuestionCard'
								THEN REPLACE(REPLACE(REPLACE(PossibleAnswerMultimediaURLFriendlyName, CHAR(13) + CHAR(10), ''), CHAR(9), ''), CHAR(10), '')
							ELSE REPLACE(REPLACE(REPLACE(PossibleAnswer_ShortVersion, CHAR(13) + CHAR(10), ''), CHAR(9), ''), CHAR(10), '')
							END
					FROM QuestionCard_GeneralAnswer_Language
					WHERE QuestionCard_GeneralAnswerID = resultxml.value('(/MultipleChoicePlayerResult//AnswerId/node())[1]', 'nvarchar(max)')
					)
		WHEN ResultType = 'TextPlayerResult'
			THEN REPLACE(REPLACE(REPLACE(resultxml.value('(/TextPlayerResult//Answer/node())[1]', 'nvarchar(max)'), CHAR(13) + CHAR(10), ''), CHAR(9), ''), CHAR(10), '')
		WHEN ResultType = 'VotingPlayerResult'
			THEN STUFF((
						SELECT ', ' + REPLACE(REPLACE(REPLACE(PossibleAnswer_ShortVersion, CHAR(13) + CHAR(10), ''), CHAR(9), ''), CHAR(10), '')
						FROM QuestionCard_GeneralAnswer_Language
						INNER JOIN (
							SELECT ROW_NUMBER() OVER (
									ORDER BY (
											SELECT 1
											)
									) AS RowNumber
								,pref.value('(node())[1]', 'nvarchar(max)') AS AnswerId
							FROM Result
							CROSS APPLY ResultXml.nodes('/VotingPlayerResult/Results/PlayerResultAnswer/AnswerId') AS Answers(pref)
							WHERE gameID = Game.GameID
								AND ResultType = 'VotingPlayerResult'
							) VotingAnswers ON QuestionCard_GeneralAnswer_Language.QuestionCard_GeneralAnswerID = VotingAnswers.AnswerId
						ORDER BY VotingAnswers.RowNumber
						FOR XML PATH('')
						), 1, 2, '')
		  WHEN ResultType = 'MultipleCheckBoxPlayerResult'
			THEN STUFF((
						SELECT '| ' + 
							CASE 
								WHEN Internal_CardType.Internal_CardTypeShortCode = 'MultipleCheckBoxImageQuestionCard'
								    THEN REPLACE(REPLACE(REPLACE(PossibleAnswerMultimediaURLFriendlyName, CHAR(13) + CHAR(10), ''), CHAR(9), ''), CHAR(10), '')
								ELSE REPLACE(REPLACE(REPLACE(PossibleAnswer_ShortVersion, CHAR(13) + CHAR(10), ''), CHAR(9), ''), CHAR(10), '')
							END																		
						FROM QuestionCard_GeneralAnswer_Language
						INNER JOIN (
							SELECT ROW_NUMBER() OVER (
									ORDER BY (
											SELECT 1
											)
									) AS RowNumber
								,pref.value('(node())[1]', 'nvarchar(max)') AS AnswerId
							FROM Result
							CROSS APPLY ResultXml.nodes('/MultipleCheckBoxPlayerResult/Results/PlayerResultAnswer/AnswerId') AS Answers(pref)
							WHERE gameID = Game.GameID
								AND ResultType = 'MultipleCheckBoxPlayerResult'
							) MultiCheckBoxAnswers ON QuestionCard_GeneralAnswer_Language.QuestionCard_GeneralAnswerID = MultiCheckBoxAnswers.AnswerId
						ORDER BY MultiCheckBoxAnswers.RowNumber
						FOR XML PATH('')
						), 1, 2, '')
		  ELSE NULL /* No card type answer definied*/
		END AS [Answer]
     ,CASE WHEN Internal_CardTypeGroup = 'Question' 
	   THEN dbo.fnIsQuestionCardAnsweredCorrect(result.ResultID)  
	   ELSE NULL
	 END AnsweredCorrectly
	,game.CreatedOnDateTime
	,game.GameCompletionDate	
FROM result
INNER JOIN game ON game.gameid = result.gameid
INNER JOIN card ON result.cardid = card.CardID
INNER JOIN Internal_CardType ON Internal_CardType.Internal_CardTypeID = card.Internal_CardTypeID
INNER JOIN QuestionCard ON QuestionCard.QuestionCardID = card.QuestionCardID
LEFT JOIN QuestionCard_Language ON QuestionCard_Language.QuestionCardid = questioncard.QuestionCardID
INNER JOIN aspnet_users ON aspnet_users.userid = game.UserID_CreatedBy
INNER JOIN User2ClientUserGroup on aspnet_Users.UserId = User2ClientUserGroup.UserID
INNER JOIN ClientUserGroup ON User2ClientUserGroup.ClientUserGroupID = ClientUserGroup.ClientUserGroupID
INNER JOIN Client ON ClientUserGroup.ClientID = Client.ClientID