﻿




CREATE VIEW [dbo].[vwMatchingListAnswerStatistic]
AS
select an.CardID, an.QuestionCard_GeneralAnswerID, an.GameID_Template, res.AnswerSequenceNo, res.RHAnswerId, res.LHAnswerId, count(*) as AnswerCount from 
	(SELECT 
		g.GameID_Template, 
		r.CardID,		
		rr.value('(AnswerSequenceNo/text())[1]', 'int') as AnswerSequenceNo, 
		rr.value('(LHAnswerId/text())[1]', 'int') as LHAnswerId,
		rr.value('(RHAnswerId/text())[1]', 'int') as RHAnswerId
	FROM            
		Result AS r 
		CROSS APPLY 
			ResultXml.nodes('/MatchingListPlayerResult/Results/PlayerResultAnswer') as rowResult(rr)
		INNER JOIN
			dbo.Game AS g ON r.GameID = g.GameID 
		INNER JOIN 
			dbo.GamePlayer gp on r.GamePlayerID = gp.GamePlayerID and g.GameID = gp.GameID and gp.IsAvatar = 0
	WHERE 
		r.Internal_CardTypeID IN
				(SELECT        Internal_CardTypeID
					FROM            dbo.Internal_CardType
					WHERE        (Internal_CardTypeName = 'QuestionCard_MatchingTwoLists'))
	) res RIGHT OUTER JOIN
                             (SELECT DISTINCT g.GameID_Template, c.CardID, ga.QuestionCard_GeneralAnswerID, c.Internal_CardTypeID
                               FROM            dbo.Game AS g INNER JOIN
                                                         dbo.GameCardGroupSequence AS gcgs ON g.GameID = gcgs.GameID INNER JOIN
                                                         dbo.GameCardSequence AS gcs ON gcgs.GameCardGroupSequenceID = gcs.GameCardGroupSequenceID INNER JOIN
                                                         dbo.Card AS c ON gcs.CardID = c.CardID INNER JOIN
                                                         dbo.QuestionCard_GeneralAnswer AS ga ON c.QuestionCardID = ga.QuestionCardID
                               WHERE        (g.GameID_Template IS NOT NULL) AND (g.IsADeletedRow = 0) AND (gcgs.IsADeletedRow = 0) AND (gcs.IsADeletedRow = 0) AND (c.IsADeletedRow = 0) AND (ga.IsADeletedRow = 0) AND 
                                                         (c.Internal_CardTypeID IN
                                                             (SELECT        Internal_CardTypeID
                                                               FROM            dbo.Internal_CardType AS Internal_CardType_1
                                                               WHERE        (Internal_CardTypeName = 'QuestionCard_MatchingTwoLists')))) AS an ON an.QuestionCard_GeneralAnswerID = res.LHAnswerId AND an.CardID = res.CardID AND 
                         res.GameID_Template = an.GameID_Template
where res.LHAnswerId is not null and res.RHAnswerId is not null
GROUP BY an.CardID, an.QuestionCard_GeneralAnswerID, an.GameID_Template, res.AnswerSequenceNo, res.RHAnswerId, res.LHAnswerId