﻿


CREATE VIEW [dbo].[vwAuditStatementAnswerStatistic]
AS
SELECT        an.GameID_Template, an.CardID, COUNT(res.GameID_Template) AS AnswerCount, an.QuestionCard_GeneralAnswerID
FROM            (SELECT        g.GameID_Template, r.CardID,
                                                        (SELECT        r.ResultXml.value('(/MultipleChoicePlayerResult//AnswerId/node())[1]', 'nvarchar(max)') AS Expr1) AS Answer
                          FROM            dbo.Result AS r INNER JOIN
                                                    dbo.Game AS g ON r.GameID = g.GameID INNER JOIN 
													dbo.GamePlayer gp on r.GamePlayerID = gp.GamePlayerID and g.GameID = gp.GameID and gp.IsAvatar = 0
                          WHERE        (g.GameID_Template IS NOT NULL) AND (r.Internal_CardTypeID IN
                                                        (SELECT        Internal_CardTypeID
                                                          FROM            dbo.Internal_CardType
                                                          WHERE        (Internal_CardTypeName = 'Card_AuditStatement') OR
                                                                                    (Internal_CardTypeName = 'Card_AuditStatementImage')))) AS res RIGHT OUTER JOIN
                             (SELECT DISTINCT g.GameID_Template, c.CardID, ga.QuestionCard_GeneralAnswerID, c.Internal_CardTypeID
                               FROM            dbo.Game AS g INNER JOIN
                                                         dbo.GameCardGroupSequence AS gcgs ON g.GameID = gcgs.GameID INNER JOIN
                                                         dbo.GameCardSequence AS gcs ON gcgs.GameCardGroupSequenceID = gcs.GameCardGroupSequenceID INNER JOIN
                                                         dbo.Card AS c ON gcs.CardID = c.CardID INNER JOIN
                                                         dbo.QuestionCard_GeneralAnswer AS ga ON c.QuestionCardID = ga.QuestionCardID
                               WHERE        (g.GameID_Template IS NOT NULL) AND (g.IsADeletedRow = 0) AND (gcgs.IsADeletedRow = 0) AND (gcs.IsADeletedRow = 0) AND (c.IsADeletedRow = 0) AND (ga.IsADeletedRow = 0) AND 
                                                         (c.Internal_CardTypeID IN
                                                             (SELECT        Internal_CardTypeID
                                                               FROM            dbo.Internal_CardType AS Internal_CardType_1
                                                               WHERE        (Internal_CardTypeName = 'Card_AuditStatement') OR
                                                                                         (Internal_CardTypeName = 'Card_AuditStatementImage')))) AS an ON an.QuestionCard_GeneralAnswerID = res.Answer AND an.CardID = res.CardID AND 
                         res.GameID_Template = an.GameID_Template
GROUP BY an.CardID, an.QuestionCard_GeneralAnswerID, an.GameID_Template
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwAuditStatementAnswerStatistic';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "res"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "an"
            Begin Extent = 
               Top = 6
               Left = 262
               Bottom = 136
               Right = 519
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwAuditStatementAnswerStatistic';

