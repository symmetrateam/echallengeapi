﻿
CREATE PROCEDURE [dbo].[upDeleteDistributor](@distributorId int, @contextUserId uniqueidentifier)
AS 
BEGIN
	
	exec dbo.upContextInfoSet @contextUserId
	
	IF NOT EXISTS (SELECT * FROM Distributor WHERE DistributorID = @distributorId)
	BEGIN
		RETURN
	END

	declare @clientTable table (Position int, ClientId int) 

	insert into @clientTable
	select ROW_NUMBER() OVER (order by clientid asc),clientid from Client2Distributor where DistributorID = @distributorId

	select * from @clientTable

	declare @totalRows int
	select @totalRows=count(*) from @clientTable

	declare @currentPosition int = 1
	declare @clientId int

	WHILE @currentPosition <= @totalRows
	BEGIN
	
		select @clientId = ClientId from @clientTable where Position = @currentPosition

		IF (SELECT COUNT(*) FROM Client2Distributor where ClientID = @clientId) > 1
		BEGIN
			PRINT 'DELETE ROW ENTRY ONLY'
			DELETE FROM DistributorClient2DistributorProduct WHERE Client2DistributorID in (SELECT Client2DistributorID FROM Client2Distributor WHERE ClientID = @clientId and DistributorID = @distributorId)
			DELETE FROM Client2Distributor WHERE ClientID = @clientId and DistributorID = @distributorId		
		END
		ELSE
		BEGIN
			PRINT 'DELETE ENTIRE CLIENT'
			DELETE FROM DistributorClient2DistributorProduct WHERE Client2DistributorID in (SELECT Client2DistributorID FROM Client2Distributor WHERE ClientID = @clientId and DistributorID = @distributorId)
			DELETE FROM Client2Distributor WHERE ClientID = @clientId and DistributorID = @distributorId
			exec dbo.upDeleteClient @clientId, @contextUserId
		END

		SET @currentPosition = @currentPosition + 1
	END

	DECLARE @productTable table (position int, productId int)

	insert into @productTable
	select ROW_NUMBER() OVER (order by productID asc),productID from Product2Distributor where DistributorID = @distributorId

	select * from @productTable

	select @totalRows=count(*) from @productTable

	set @currentPosition = 1
	declare @productId int

	WHILE @currentPosition <= @totalRows
	BEGIN
	
		select @productId = ProductID from @productTable where Position = @currentPosition

		IF (SELECT COUNT(*) FROM Product2Distributor where ProductID = @productId) > 1
		BEGIN
			PRINT 'DELETE ROW ENTRY ONLY'
			DELETE FROM DistributorClient2DistributorProduct WHERE Distributor2ProductID in (SELECT Distributor2ProductID FROM Product2Distributor WHERE ProductID = @productId and DistributorID = @distributorId)
			DELETE FROM Product2Distributor WHERE ProductID = @productId and DistributorID = @distributorId		
		END
		ELSE
		BEGIN
			PRINT 'DELETE ENTIRE PRODUCT'
			DELETE FROM DistributorClient2DistributorProduct WHERE Distributor2ProductID in (SELECT Distributor2ProductID FROM Product2Distributor WHERE ProductID = @productId and DistributorID = @distributorId)
			DELETE FROM Product2Distributor WHERE ProductID = @productId and DistributorID = @distributorId
			EXEC dbo.upDeleteProduct @productId, @contextUserId
		END

		SET @currentPosition = @currentPosition + 1
	END

	DECLARE @userTable table (position int, userId uniqueidentifier)

	insert into @userTable
	select ROW_NUMBER() OVER (order by UserID asc),UserID from User2Distributor where DistributorID = @distributorId and UserID != @contextUserId

	select * from @userTable


	select @totalRows=count(*) from @userTable

	set @currentPosition = 1
	declare @userId uniqueidentifier

	WHILE @currentPosition <= @totalRows
	BEGIN
	
		select @userId = userId from @userTable where Position = @currentPosition

		IF (SELECT COUNT(*) FROM User2Distributor where UserID = @userId) > 1
		BEGIN
			PRINT 'DELETE ROW ENTRY ONLY'
			DELETE FROM User2Distributor WHERE UserID = @userId and DistributorID = @distributorId
		END
		ELSE
		BEGIN
			PRINT 'DELETE ENTIRE USER'
			DELETE FROM User2Distributor WHERE UserID = @userId and DistributorID = @distributorId
			exec dbo.upDeleteUser @userId, @contextUserId
		END

		SET @currentPosition = @currentPosition + 1
	END

	DELETE FROM Distributor WHERE DistributorID = @distributorId
END