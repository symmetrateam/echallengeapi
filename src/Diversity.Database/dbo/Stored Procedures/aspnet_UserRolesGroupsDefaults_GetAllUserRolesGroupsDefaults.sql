﻿CREATE Procedure [dbo].[aspnet_UserRolesGroupsDefaults_GetAllUserRolesGroupsDefaults]
		@ApplicationName		nvarchar(256),
		@UserRolesGroupName		nvarchar(256)
AS
BEGIN
	DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

	DECLARE @UserRolesGroupId uniqueidentifier
	SELECT @UserRolesGroupId = NULL
	SELECT @UserRolesGroupId = UserRolesGroupId FROM aspnet_UserRolesGroups WHERE LOWER(@UserRolesGroupName) = LoweredUserRolesGroupName
    
    IF(  @UserRolesGroupId IS NULL)
		RETURN
    
    SELECT RoleName
    FROM   dbo.aspnet_Roles r, dbo.aspnet_UserRolesGroupsDefaults d
    WHERE r.RoleId = d.RoleId and d.UserRolesGroupId = @UserRolesGroupId
    ORDER BY RoleName

END