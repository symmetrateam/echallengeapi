﻿CREATE Procedure [dbo].[aspnet_UserRolesGroups_CreateUserRolesGroup]
		@ApplicationName	nvarchar(256),
		@UserRolesGroupName nvarchar(256),
		@UserRolesGroupId	uniqueidentifier OUTPUT
AS
BEGIN
				
	DECLARE @ApplicationId uniqueidentifier
	SELECT @ApplicationId = NULL
		
	DECLARE @ErrorCode int
	SET @ErrorCode = 0

	DECLARE @TranStarted bit
	SET @TranStarted = 0
	
	IF( @@TRANCOUNT = 0)
	BEGIN		
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END
	ELSE
		SET @TranStarted = 0

	EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT
		
	IF( @@ERROR  <> 0)
	BEGIN
		SET @ErrorCode = -1
		GOTO Cleanup
	END
	
	SELECT  @UserRolesGroupId = UserRolesGroupId FROM dbo.aspnet_UserRolesGroups WHERE LOWER(@UserRolesGroupName) = LoweredUserRolesGroupName AND @ApplicationId = ApplicationId

	IF( @UserRolesGroupId IS NULL)
	BEGIN
	
		SELECT @UserRolesGroupId = NEWID()
		INSERT INTO dbo.aspnet_UserRolesGroups
								(ApplicationId, UserRolesGroupId, UserRolesGroupName, LowereduserRolesGroupName)
						VALUES	(@ApplicationId, @UserRolesGroupId, @UserRolesGroupName, LOWER(@UserRolesGroupName))
		
		IF( @@ERROR  <> 0)
		BEGIN
			SET @ErrorCode = -1
			GOTO Cleanup
		END
		
	END
	ELSE
	BEGIN
			SET @ErrorCode = 1
			GOTO CleanUp
	END
		
	IF( @TranStarted = 1 )
	BEGIN
		SET @TranStarted = 0
		COMMIT TRANSACTION
	END

	RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode


END