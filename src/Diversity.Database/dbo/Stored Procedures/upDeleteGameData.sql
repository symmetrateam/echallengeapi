﻿
CREATE PROCEDURE [dbo].[upDeleteGameData](@gameId int, @contextUserId uniqueidentifier)
AS 
BEGIN
	
	exec dbo.upContextInfoSet @contextUserId

	DELETE
	FROM Result
	WHERE GameID in (SELECT GameID from game where GameID_Template = @gameId)
	
	DELETE FROM GameCardSequence WHERE GameCardGroupSequenceID IN (SELECT GameCardGroupSequenceID FROM GameCardGroupSequence WHERE GameID in (SELECT GameID from game where GameID_Template = @gameId))
	DELETE FROM GameCardGroupSequence WHERE GameID in (SELECT GameID from game where GameID_Template = @gameId)
	DELETE FROM GamePlayer WHERE GameID  in (SELECT GameID from game where GameID_Template = @gameId)
	DELETE FROM GAME WHERE GameID in (SELECT GameID from game where GameID_Template = @gameId)

END