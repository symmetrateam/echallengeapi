﻿
CREATE PROCEDURE [dbo].[upDeleteGame](@gameId int, @contextUserId uniqueidentifier)
AS 
BEGIN
	
	DECLARE @IsGameTemplate bit = 0

	SELECT @IsGameTemplate=IsAGameTemplate from game where GameID = @gameId

	exec dbo.upContextInfoSet @contextUserId

	IF @IsGameTemplate = 1
	BEGIN		
		DELETE
		FROM Result
		WHERE GameID in (SELECT GameID from game where gameid = @GameID or GameID_Template = @gameId)

		DECLARE @cardTable table (position int, cardID int)
		INSERT INTO @cardTable
		Select ROW_NUMBER() OVER (Order by CardID),CardID FROM Card WHERE IsForGame = 1 AND CardID in (
			SELECT CardID FROM GameCardSequence WHERE GameCardGroupSequenceID IN (SELECT GameCardGroupSequenceID FROM GameCardGroupSequence WHERE GameID in (SELECT GameID from game where gameid = @GameID or GameID_Template = @gameId)))

		DECLARE @total int
		SELECT @total = count(*) from @cardTable

		DECLARE @count int = 0
		DECLARE @cardId int 

		WHILE @count <= @total
		BEGIN
			SELECT @cardId = cardID from @cardTable where position = @count

			exec dbo.upDeleteCard @cardId, @contextUserId

			SET @count = @count + 1
		END
		
		DELETE FROM GameCardSequence WHERE GameCardGroupSequenceID IN (SELECT GameCardGroupSequenceID FROM GameCardGroupSequence WHERE GameID in (SELECT GameID from game where gameid = @GameID or GameID_Template = @gameId))
		DELETE FROM GameCardGroupSequence WHERE GameID in (SELECT GameID from game where gameid = @GameID or GameID_Template = @gameId)
		DELETE FROM GamePlayer WHERE GameID  in (SELECT GameID from game where gameid = @GameID or GameID_Template = @gameId)
		DELETE FROM GAME WHERE GameID in (SELECT GameID from game where GameID_Template = @gameId)
	END
	ELSE
	BEGIN
		
		DELETE
		FROM Result
		WHERE GameID in (SELECT GameID from game where gameid = @GameID)
		
		DELETE FROM GameCardSequence WHERE GameCardGroupSequenceID IN (SELECT GameCardGroupSequenceID FROM GameCardGroupSequence WHERE GameID in (SELECT GameID from game where gameid = @GameID))
		DELETE FROM GameCardGroupSequence WHERE GameID in (SELECT GameID from game where gameid = @GameID)
		DELETE FROM GamePlayer WHERE GameID  in (SELECT GameID from game where gameid = @GameID)
	END

	DELETE FROM Game WHERE GameID = @GameID
END