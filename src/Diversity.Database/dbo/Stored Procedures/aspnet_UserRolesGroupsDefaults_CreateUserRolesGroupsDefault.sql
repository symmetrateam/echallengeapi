﻿CREATE Procedure [dbo].[aspnet_UserRolesGroupsDefaults_CreateUserRolesGroupsDefault]
	@ApplicationName		nvarchar(256),
	@UserRolesGroupName		nvarchar(256),
	@RoleName				nvarchar(256)
AS			
BEGIN
		
	DECLARE @UserRolesGroupId uniqueidentifier
	SET @UserRolesGroupId = NULL
	
	DECLARE @RoleId uniqueidentifier
	SET @RoleId = NULL
	
	DECLARE @ErrorCode int
	SET @ErrorCode = 0

	DECLARE @TranStarted bit
	SET @TranStarted = 0
	
	IF( @@TRANCOUNT = 0)
	BEGIN		
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END
	ELSE
		SET @TranStarted = 0
	
	SELECT @RoleId = RoleId FROM dbo.aspnet_Roles WHERE LOWER(@RoleName) = LoweredRoleName
	
	IF( @RoleId IS NULL )
	BEGIN
		SET @ErrorCode = 0
		GOTO Cleanup
	END
		
	EXEC dbo.aspnet_UserRolesGroups_CreateUserRolesGroup @ApplicationName, @UserRolesGroupName, @UserRolesGroupId OUTPUT
	
	IF( @@ERROR  <> 0)
	BEGIN
		SET @ErrorCode = -1
		GOTO Cleanup
	END
		
	INSERT INTO dbo.aspnet_UserRolesGroupsDefaults
							(UserRolesGroupId, RoleId)
					VALUES	(@UserRolesGroupId, @RoleId)
	
	IF( @@ERROR  <> 0)
	BEGIN
		SET @ErrorCode = -1
		GOTO Cleanup
	END
	
	IF( @TranStarted = 1 )
	BEGIN
		SET @TranStarted = 0
		COMMIT TRANSACTION
	END
	
	RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

	
END