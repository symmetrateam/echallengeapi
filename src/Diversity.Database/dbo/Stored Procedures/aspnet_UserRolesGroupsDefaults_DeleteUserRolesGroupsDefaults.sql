﻿CREATE Procedure [dbo].[aspnet_UserRolesGroupsDefaults_DeleteUserRolesGroupsDefaults]
	@UserRolesGroupId		uniqueidentifier,
	@RoleId					uniqueidentifier
AS		
BEGIN	
	DECLARE @ErrorCode int
	SET @ErrorCode = 0

	DECLARE @TranStarted bit
	SET @TranStarted = 0
	
	IF( @@TRANCOUNT = 0)
	BEGIN		
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END
	ELSE
		SET @TranStarted = 0

	IF(NOT EXISTS(SELECT UserRolesGroupId FROM UserRolesGroupsDefaults WHERE @UserRolesGroupId = UserRolesGroupId AND @RoleId = RoleId))
	BEGIN
		SET @ErrorCode = 1
		GOTO Cleanup
	END
	
	DELETE FROM dbo.aspnet_UserRolesGroupsDefaults
		WHERE UserRolesGroupId = @UserRolesGroupId AND RoleId = @RoleId					
	
	IF( @@ERROR  <> 0)
	BEGIN
		SET @ErrorCode = -1
		GOTO Cleanup
	END
	
	IF( @TranStarted = 1 )
	BEGIN
		SET @TranStarted = 0
		COMMIT TRANSACTION
	END
	
	RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode
END