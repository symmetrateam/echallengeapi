﻿
CREATE PROCEDURE [dbo].[upDeleteUser](@userId uniqueidentifier, @contextUserId uniqueidentifier)
AS 
BEGIN
	
	exec dbo.upContextInfoSet @contextUserId

	IF NOT EXISTS (SELECT * FROM aspnet_Users where userid = @userId)
	BEGIN
		RETURN
	END 

	declare @gameTable table (
		Position int,
		GameId int
	)

	insert into @gameTable
	select ROW_NUMBER() over(order by GameID) as Position, GameID 
	from game where UserID_CreatedBy = @userId and GameID_Template IS NOT NULL

	DECLARE @gameId int 
	DECLARE @count int = 1
	DECLARE @total  int
	select @total=max(Position) from @gameTable	
	print @total

	select * from @gameTable

	while @count <= @total
	begin
		select @gameId=GameID from @gameTable where Position = @count

		exec dbo.upDeleteGame @gameId, @contextUserId

		print CONVERT(NVARCHAR,@count) + ' - ' + CONVERT(NVARCHAR,@gameId)
		SET @count = @count+1		
	end

	DELETE FROM User2Distributor where userid = @userId	
	DELETE FROM User2ClientUserGroup where userid = @userId
	DELETE FROM User2Client where UserID = @userId		
	DELETE FROM Session where UserID = @userId

	declare @username nvarchar(255)
	select @username = username from aspnet_users where userid = @userId

	declare @NumTablesDeletedFrom int
	EXEC dbo.aspnet_Users_DeleteUser '/Diversity', @username, 15, @NumTablesDeletedFrom
END