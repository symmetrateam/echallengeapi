﻿
CREATE PROCEDURE [dbo].[upDeleteClient](@clientId int, @contextUserId uniqueidentifier)
AS 
BEGIN
	
	exec dbo.upContextInfoSet @contextUserId

	IF NOT EXISTS (SELECT * FROM Client where ClientID = @clientId)
	BEGIN
		RETURN
	END 

	DELETE FROM DistributorClient2DistributorProduct where Client2DistributorID in 
			(SELECT Client2DistributorID FROM Client2Distributor where ClientId = @clientId )

	DELETE FROM Client2Distributor where ClientID = @clientId

	declare @userTable table (
		Position int,
		UserId uniqueidentifier
	)

	insert into @userTable
	select ROW_NUMBER() OVER (order by UserID) as Position, UserId
	from aspnet_users where userid in (select UserID from User2Client where ClientID = @clientId)
	and userid not in (select UserID from User2Distributor) and userid != @contextUserId


	DECLARE @userId uniqueidentifier
	DECLARE @count int = 1
	DECLARE @total  int
	select @total=max(Position) from @userTable	
	

	while @count <= @total
	begin
		select @userId=UserId from @userTable where Position = @count
		
		exec dbo.upDeleteUser @userId, @contextUserId		

		SET @count = @count+1		
	end

	DELETE FROM User2ClientUserGroup WHERE ClientUserGroupID in (SELECT ClientUserGroupID from ClientUserGroup where ClientID = @clientId)
	DELETE from ClientUserGroup where ClientID = @clientId
	DELETE FROM User2Client WHERE ClientID = @clientId
	UPDATE Card SET ClientID_ForLibrary = null WHERE ClientID_ForLibrary = @clientId

	DELETE FROM Client WHERE ClientID = @clientId
END