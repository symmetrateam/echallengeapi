﻿

CREATE PROCEDURE [dbo].[upDeleteProduct](@productId int, @contextUserId uniqueidentifier)
AS 
BEGIN
	
	exec dbo.upContextInfoSet @contextUserId

	IF NOT EXISTS (SELECT * FROM Product where ProductID = @productId)
	BEGIN
		RETURN
	END 

	DELETE FROM Product_Language where ProductId = @productId
	DELETE FROM CardClassificationGroup_Language WHERE CardClassificationGroupID in (SELECT CardClassificationGroupID FROM CardClassificationGroup WHERE ProductID = @productId)
	DELETE FROM Card2CardClassification where CardClassificationGroupID in (SELECT CardClassificationGroupID FROM CardClassificationGroup WHERE ProductID = @productId)
	DELETE FROM CardClassificationGroup where ProductID = @productId
	DELETE FROM DistributorClient2DistributorProduct Where Distributor2ProductID in (SELECT Distributor2ProductID FROM Product2Distributor where ProductID = @productId)
	DELETE FROM Product2Distributor where ProductID = @productId

	DECLARE @gameTable table (Position int, GameId int)
	
	insert into @gameTable
	select ROW_NUMBER() OVER (ORDER BY GameID), GameID from Game where ProductID = @productId

	DECLARE @total int
	SELECT @total = count(*) from @gameTable

	Declare @position int = 1
	Declare @gameId int

	WHILE @position <= @total
	BEGIN
		SELECT @gameId=gameid from @gameTable where Position = @position

		EXEC dbo.upDeleteGame @gameId, @contextUserId
		SET @position = @position + 1
	END

	DECLARE @cardTable table (Position int, CardID int)
	
	insert into @cardTable
	select ROW_NUMBER() OVER (ORDER BY CardID), CardID from Card where ProductID = @productId

	select @total = count(*) from @cardTable

	set @position = 1
	DECLARE @cardId int

	while @position <= @total
	BEGIN		
		select @cardId = CardID from @cardTable where Position = @position

		exec dbo.upDeleteCard @cardId, @contextUserId

		SET @position = @position + 1
	END
	
	DELETE FROM Product WHERE ProductID = @productId

END