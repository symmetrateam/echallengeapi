﻿
/*
	This procedure allows the application to pass context information into the database
	context. 

	For this application the primary use is to enable the user information to be passed
	into the audit process so that the last modified user can be set on a deleted
	audit row.	
*/
CREATE PROCEDURE [dbo].[upContextInfoSet](@contextUserId nvarchar(50))
AS 
BEGIN
	DECLARE @m binary(128)
	set @m = CAST(@contextUserId as binary(128))
	set CONTEXT_INFO @m
END