﻿CREATE PROCEDURE [dbo].[upDeleteTheme](@themeId int, @contextUserId uniqueidentifier)
AS 
BEGIN
	
	exec dbo.upContextInfoSet @contextUserId
	
	IF NOT EXISTS (SELECT * FROM Theme WHERE ThemeID = @themeId)
	BEGIN
		RETURN
	END

	DELETE FROM Theme WHERE ThemeID = @themeId

	RETURN
END