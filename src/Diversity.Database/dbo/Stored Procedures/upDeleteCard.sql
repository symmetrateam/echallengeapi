﻿CREATE PROCEDURE [dbo].[upDeleteCard](@cardId int, @contextUserId uniqueidentifier, @isGameCard bit = 0)
AS 
BEGIN
	
	exec dbo.upContextInfoSet @contextUserId

	DECLARE @questionCardTable table (questionCardID int)
	INSERT INTO @questionCardTable
	SELECT QuestionCardID from Card WHERE IsForGame = @isGameCard AND CardID = @cardId

	DELETE FROM QuestionCardStatement_Language where QuestionCardID in (
		select questionCardID from @questionCardTable
	)

	DELETE FROM QuestionCard_GeneralAnswer_Language where QuestionCard_GeneralAnswerID in (
		SELECT QuestionCard_GeneralAnswerID FROM QuestionCard_GeneralAnswer where QuestionCardID in (
			select questionCardID from @questionCardTable
		)
	)
	
	DELETE FROM QuestionCard_GeneralAnswer where QuestionCardID in (
		select questionCardID from @questionCardTable
	)

	DELETE FROM QuestionCardStatement_Language where QuestionCardID in (
		select questionCardID from @questionCardTable
	)	

	DELETE FROM QuestionCard_Language where QuestionCardID in (
		select questionCardID from @questionCardTable
	)

	Delete from Result where CardID = @cardId

	IF @isGameCard = 1
	BEGIN		
		DECLARE @gameCardGroupSequenceTable table (Position int,GameCardGroupSequenceID int)

		insert into @gameCardGroupSequenceTable
		SELECT ROW_NUMBER() OVER (ORDER BY GameCardGroupSequenceID), GameCardGroupSequenceID FROM
		(SELECT DISTINCT GameCardGroupSequenceID FROM GameCardSequence WHERE CardID = @cardId) base

		DELETE FROM GameCardSequence WHERE CardID = @cardId

		DECLARE @total int
		SELECT @total=count(*) from @gameCardGroupSequenceTable

		DECLARE @position int = 1
		DECLARE @gameCardGroupSequenceID int
		WHILE @position <= @total
		BEGIN
			select @gameCardGroupSequenceID=GameCardGroupSequenceID from @gameCardGroupSequenceTable where Position = @position

			IF (SELECT count(*) from GameCardSequence where GameCardGroupSequenceID = @gameCardGroupSequenceID) = 0
			BEGIN
				DELETE FROM GameCardGroupSequence where GameCardGroupSequenceID = @gameCardGroupSequenceID
			END
		
			SET @position = @position + 1
		END

		
	END

	DELETE FROM Card WHERE CardID = @cardId

	DELETE FROM QuestionCard where QuestionCardID in (
		select questionCardID from @questionCardTable
	)
END