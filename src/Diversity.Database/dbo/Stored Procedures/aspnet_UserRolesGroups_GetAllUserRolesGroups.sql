﻿CREATE Procedure [dbo].[aspnet_UserRolesGroups_GetAllUserRolesGroups]
		@ApplicationName	nvarchar(256)

AS
BEGIN
	DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN
    SELECT UserRolesGroupName
    FROM   dbo.aspnet_UserRolesGroups WHERE ApplicationId = @ApplicationId
    ORDER BY UserRolesGroupName
END