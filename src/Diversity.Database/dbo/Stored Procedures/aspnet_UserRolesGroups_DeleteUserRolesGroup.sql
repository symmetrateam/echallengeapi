﻿CREATE Procedure [dbo].[aspnet_UserRolesGroups_DeleteUserRolesGroup]
	@ApplicationName	nvarchar(256),
	@UserRolesGroupName nvarchar(256)	
AS
BEGIN
	DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
        
        
	DECLARE @ErrorCode int
	SET @ErrorCode = 0

	DECLARE @TranStarted bit
	SET @TranStarted = 0
	
	IF( @@TRANCOUNT = 0)
	BEGIN		
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END
	ELSE
		SET @TranStarted = 0
		
	DECLARE @UserRolesGroupId uniqueidentifier
	SELECT @UserRolesGroupId = NULL
	SELECT @UserRolesGroupId = UserRolesGroupId FROM dbo.aspnet_UserRolesGroups 
		WHERE LOWER(@UserRolesGroupName) = LoweredUserRolesGroupName AND @ApplicationId = ApplicationId

	IF( @UserRolesGroupId IS NULL )
	BEGIN
		SET @ErrorCode = 1
		GOTO Cleanup
	END
	
	DELETE FROM dbo.aspnet_UserRolesGroupsDefaults WHERE UserRolesGroupId = @UserRolesGroupId
	
	IF( @@ERROR  <> 0)
	BEGIN
		SET @ErrorCode = -1
		GOTO Cleanup
	END
	
	DELETE FROM dbo.aspnet_UserRolesGroups
		WHERE  UserRolesGroupId = @UserRolesGroupId
	
	IF( @@ERROR  <> 0)
	BEGIN
		SET @ErrorCode = -1
		GOTO Cleanup
	END
	
	IF( @TranStarted = 1 )
	BEGIN
		SET @TranStarted = 0
		COMMIT TRANSACTION
	END
	
	RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode


END