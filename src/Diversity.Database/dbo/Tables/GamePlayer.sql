﻿CREATE TABLE [dbo].[GamePlayer] (
    [GamePlayerID]             INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]        DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]   DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]            BIT              CONSTRAINT [DF_GamePlayer_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]             BIT              CONSTRAINT [DF_GamePlayer_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [UserID]                   UNIQUEIDENTIFIER NOT NULL,
    [GameID]                   INT              NOT NULL,
    [DesktopTeamIconName]      NVARCHAR (255)   NULL,
    [DesktopTeamName]          NVARCHAR (255)   NULL,
    [PlayerTurnSequenceNo]     INT              NOT NULL,
    [NoOfPlayersIfThisIsATeam] INT              NULL,
    [IsAvatar]                 BIT              CONSTRAINT [DF_GamePlayer_IsAvatar] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_GamePlayer] PRIMARY KEY CLUSTERED ([GamePlayerID] ASC),
    CONSTRAINT [FK_GamePlayer_aspnet_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[aspnet_Users] ([UserId]) ON DELETE CASCADE,
    CONSTRAINT [FK_GamePlayer_Game] FOREIGN KEY ([GameID]) REFERENCES [dbo].[Game] ([GameID]) ON DELETE CASCADE
);






GO
CREATE TRIGGER [tgAuditGamePlayer] 
		   ON  [dbo].[GamePlayer] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditGamePlayer
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GamePlayerID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [GameID],  [DesktopTeamIconName],  [DesktopTeamName],  [PlayerTurnSequenceNo],  [NoOfPlayersIfThisIsATeam],  [IsAvatar]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditGamePlayer
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GamePlayerID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [GameID],  [DesktopTeamIconName],  [DesktopTeamName],  [PlayerTurnSequenceNo],  [NoOfPlayersIfThisIsATeam],  [IsAvatar]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditGamePlayer
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GamePlayerID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [GameID],  [DesktopTeamIconName],  [DesktopTeamName],  [PlayerTurnSequenceNo],  [NoOfPlayersIfThisIsATeam],  [IsAvatar]
				FROM deleted
			END
		END
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This could be null if the row represents a team instead of a single player', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'GamePlayer', @level2type = N'COLUMN', @level2name = N'UserID';

