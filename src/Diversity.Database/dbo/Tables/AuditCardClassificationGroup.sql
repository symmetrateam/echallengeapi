﻿CREATE TABLE [dbo].[AuditCardClassificationGroup] (
    [ID]                               INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                        CHAR (1)         NOT NULL,
    [RecordedAtDateTime]               DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [CardClassificationGroupID]        INT              NULL,
    [UserID_CreatedBy]                 UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]            UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]           DATETIME2 (7)    NULL,
    [IsADeletedRow]                    BIT              NULL,
    [IsAnAuditRow]                     BIT              NULL,
    [ProductID]                        INT              NULL,
    [CardClassificationGroupID_Parent] INT              NULL,
    [CardClassificationGroupImageURL]  NVARCHAR (MAX)   NULL,
    [IsAMetaTagClassificationGroup]    BIT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



