﻿CREATE TABLE [dbo].[CardReferenceMaterial] (
    [CardReferenceMaterialID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]       DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]  DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]           BIT              CONSTRAINT [DF_CardReferenceMaterial_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]            BIT              CONSTRAINT [DF_CardReferenceMaterial_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [CardID]                  INT              NOT NULL,
    [DefaultLanguageID]       INT              NOT NULL,
    CONSTRAINT [PK_CardReferenceMaterial] PRIMARY KEY CLUSTERED ([CardReferenceMaterialID] ASC),
    CONSTRAINT [FK_CardReferenceMaterial_Card] FOREIGN KEY ([CardID]) REFERENCES [dbo].[Card] ([CardID]) ON DELETE CASCADE,
    CONSTRAINT [FK_CardReferenceMaterial_Language] FOREIGN KEY ([DefaultLanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);




GO
CREATE TRIGGER [tgAuditCardReferenceMaterial] 
		   ON  [dbo].[CardReferenceMaterial] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditCardReferenceMaterial
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardReferenceMaterialID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [DefaultLanguageID]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditCardReferenceMaterial
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardReferenceMaterialID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [DefaultLanguageID]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditCardReferenceMaterial
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardReferenceMaterialID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [DefaultLanguageID]
				FROM deleted
			END
		END