﻿CREATE TABLE [dbo].[QuestionCard_Language] (
    [QuestionCard_LanguageID]         INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]               DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]          DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                   BIT              CONSTRAINT [DF_QuestionCard_Language_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                    BIT              CONSTRAINT [DF_QuestionCard_Language_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [QuestionCardID]                  INT              NOT NULL,
    [LanguageID]                      INT              NOT NULL,
    [Question_Statement_LongVersion]  NVARCHAR (MAX)   NULL,
    [Question_Statement_ShortVersion] NVARCHAR (MAX)   NULL,
    [AnswerExplanation_LongVersion]   NVARCHAR (MAX)   NULL,
    [AnswerExplanation_ShortVersion]  NVARCHAR (MAX)   NULL,
    [StatementHeader]                 NVARCHAR (MAX)   NULL,
    [StatementLocation]               NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_QuestionCard_Language] PRIMARY KEY CLUSTERED ([QuestionCard_LanguageID] ASC),
    CONSTRAINT [FK_QuestionCard_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_QuestionCard_Language_QuestionCard] FOREIGN KEY ([QuestionCardID]) REFERENCES [dbo].[QuestionCard] ([QuestionCardID])
);








GO
CREATE TRIGGER [tgAuditQuestionCard_Language] 
		   ON  [dbo].[QuestionCard_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditQuestionCard_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCardID],  [LanguageID],  [Question_Statement_LongVersion],  [Question_Statement_ShortVersion],  [AnswerExplanation_LongVersion],  [AnswerExplanation_ShortVersion],  [StatementHeader],  [StatementLocation]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditQuestionCard_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCardID],  [LanguageID],  [Question_Statement_LongVersion],  [Question_Statement_ShortVersion],  [AnswerExplanation_LongVersion],  [AnswerExplanation_ShortVersion],  [StatementHeader],  [StatementLocation]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditQuestionCard_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCardID],  [LanguageID],  [Question_Statement_LongVersion],  [Question_Statement_ShortVersion],  [AnswerExplanation_LongVersion],  [AnswerExplanation_ShortVersion],  [StatementHeader],  [StatementLocation]
				FROM deleted
			END
		END