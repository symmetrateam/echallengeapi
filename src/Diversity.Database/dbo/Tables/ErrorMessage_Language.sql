﻿CREATE TABLE [dbo].[ErrorMessage_Language] (
    [ErrorMessage_LanguageID] INT            IDENTITY (1, 1) NOT NULL,
    [ErrorMessageID]          INT            NOT NULL,
    [LanguageID]              INT            NOT NULL,
    [ErrorMessageDescription] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ErrorMessage_Language] PRIMARY KEY CLUSTERED ([ErrorMessage_LanguageID] ASC),
    CONSTRAINT [FK_ErrorMessage_Language_ErrorMessage] FOREIGN KEY ([ErrorMessageID]) REFERENCES [dbo].[ErrorMessage] ([ErrorMessageID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ErrorMessage_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]) ON DELETE CASCADE
);




GO
CREATE TRIGGER [tgAuditErrorMessage_Language] 
		   ON  [dbo].[ErrorMessage_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditErrorMessage_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ErrorMessage_LanguageID],  [ErrorMessageID],  [LanguageID],  [ErrorMessageDescription]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditErrorMessage_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ErrorMessage_LanguageID],  [ErrorMessageID],  [LanguageID],  [ErrorMessageDescription]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditErrorMessage_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ErrorMessage_LanguageID],  [ErrorMessageID],  [LanguageID],  [ErrorMessageDescription]
				FROM deleted
			END
		END