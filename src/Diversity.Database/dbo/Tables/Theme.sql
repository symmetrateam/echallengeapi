﻿CREATE TABLE [dbo].[Theme] (
    [ThemeID]                INT              IDENTITY (1, 1) NOT NULL,
    [ThemeCode]              NVARCHAR (10)    NOT NULL,
    [ThemeName]              NVARCHAR (50)    NOT NULL,
    [Description]            NVARCHAR (255)   NULL,
    [UserID_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]      DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime] DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]          BIT              CONSTRAINT [DF_Theme_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]           BIT              CONSTRAINT [DF_Theme_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Theme] PRIMARY KEY CLUSTERED ([ThemeID] ASC),
    CONSTRAINT [UK_Theme_ThemeCode] UNIQUE NONCLUSTERED ([ThemeCode] ASC)
);


GO
CREATE TRIGGER [tgAuditTheme] 
				   ON  [dbo].[Theme] 
				   AFTER INSERT, UPDATE, DELETE
				AS 
				BEGIN
					SET NOCOUNT ON;

					IF(SELECT COUNT(*) FROM inserted) > 0
					BEGIN
		
						IF(SELECT COUNT(*) FROM deleted) > 0
						BEGIN
							--Update
							INSERT INTO AuditTheme
							SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ThemeID],  [ThemeCode],  [ThemeName],  [Description],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow]
							FROM inserted 
						END
						ELSE
						BEGIN
							--Insert
							INSERT INTO AuditTheme
							SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ThemeID],  [ThemeCode],  [ThemeName],  [Description],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow]
							FROM inserted 
						END
					END
					ELSE
					BEGIN
						INSERT INTO AuditTheme
						SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ThemeID],  [ThemeCode],  [ThemeName],  [Description],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow]
						FROM deleted
					END
				END