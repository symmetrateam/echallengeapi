﻿CREATE TABLE [dbo].[FlashAsset_Language] (
    [FlashAsset_LanguageID] INT            IDENTITY (1, 1) NOT NULL,
    [FlashAssetID]          INT            NOT NULL,
    [LanguageID]            INT            NOT NULL,
    [Value]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FlashAsset_Language] PRIMARY KEY CLUSTERED ([FlashAsset_LanguageID] ASC),
    CONSTRAINT [FK_FlashAsset_Language_FlashAsset] FOREIGN KEY ([FlashAssetID]) REFERENCES [dbo].[FlashAsset] ([FlashAssetID]) ON DELETE CASCADE,
    CONSTRAINT [FK_FlashAsset_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]) ON DELETE CASCADE
);




GO
CREATE TRIGGER [tgAuditFlashAsset_Language] 
		   ON  [dbo].[FlashAsset_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditFlashAsset_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [FlashAsset_LanguageID],  [FlashAssetID],  [LanguageID],  [Value]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditFlashAsset_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [FlashAsset_LanguageID],  [FlashAssetID],  [LanguageID],  [Value]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditFlashAsset_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [FlashAsset_LanguageID],  [FlashAssetID],  [LanguageID],  [Value]
				FROM deleted
			END
		END