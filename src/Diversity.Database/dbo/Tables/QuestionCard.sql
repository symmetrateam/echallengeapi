﻿CREATE TABLE [dbo].[QuestionCard] (
    [QuestionCardID]                                      INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                                    UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]                               UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                                   DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]                              DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                                       BIT              CONSTRAINT [DF_QuestionCard_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                                        BIT              CONSTRAINT [DF_QuestionCard_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [AnswerIsCaseSensitive]                               BIT              CONSTRAINT [DF_QuestionCard_AnswerIsCaseSensitive] DEFAULT ((0)) NOT NULL,
    [QuestionIsOnlyCorrectIfAllAnswersAreTrue]            BIT              CONSTRAINT [DF_QuestionCard_QuestionIsOnlyCorrectIfAllAnswersAreTrue] DEFAULT ((0)) NOT NULL,
    [AssociatedImageUrl]                                  NVARCHAR (MAX)   NULL,
    [QuestionMultimediaStartAutoElsePushPlayToStart]      BIT              CONSTRAINT [DF_QuestionCard_CompanyScorecardAnimationStartAutoElsePushPlayToStart] DEFAULT ((0)) NOT NULL,
    [QuestionMultimediaShowPlaybackControls]              BIT              CONSTRAINT [DF_QuestionCard_CompanyScorecardShowPlaybackControls] DEFAULT ((0)) NOT NULL,
    [AnswerMultimediaStartAutoElsePushPlayToStart]        BIT              CONSTRAINT [DF_QuestionCard_AnswerMultimediaStartAutoElsePushPlayToStart] DEFAULT ((0)) NOT NULL,
    [AnswerMultimediaShowPlaybackControls]                BIT              CONSTRAINT [DF_QuestionCard_AnswerMultimediaShowPlaybackControls] DEFAULT ((0)) NOT NULL,
    [QuestionMultimediaURLFriendlyName]                   NVARCHAR (MAX)   NULL,
    [QuestionMultimediaURL]                               NVARCHAR (MAX)   NULL,
    [QuestionMultimediaType_ImageVideoetc]                NVARCHAR (100)   NULL,
    [AnswerExplanationMultimediaURLFriendlyName]          NVARCHAR (MAX)   NULL,
    [AnswerExplanationMultimediaURL]                      NVARCHAR (MAX)   NULL,
    [AnswerExplanationMultimediaType_ImageVideoetc]       NVARCHAR (100)   NULL,
    [DesktopCurrentWinningScore]                          INT              NOT NULL,
    [DesktopCurrentLosingScore]                           INT              NOT NULL,
    [DesktopNonCurrentWinningScore]                       INT              NOT NULL,
    [DesktopNonCurrentLosingScore]                        INT              NOT NULL,
    [OnlineWinningScore]                                  INT              NOT NULL,
    [OnlineLosingScore]                                   INT              NOT NULL,
    [OfferOTHERTextOption]                                BIT              CONSTRAINT [DF_QuestionCard_OfferOTHERTextOption] DEFAULT ((0)) NOT NULL,
    [TextOptionInputIsTextArea]                           BIT              CONSTRAINT [DF_QuestionCard_OTHERTextOptionIsTextArea] DEFAULT ((0)) NOT NULL,
    [ApplyScoreByCardElseForEachCorrectAnswer]            BIT              CONSTRAINT [DF_QuestionCard_ApplyScoreByCardElseForEachCorrectAnswer] DEFAULT ((0)) NOT NULL,
    [OfferOptOutChoice]                                   BIT              CONSTRAINT [DF_QuestionCard_OfferOptOutChoice] DEFAULT ((0)) NULL,
    [ApplyNegScoringForEachInCorrectAnswer]               BIT              CONSTRAINT [DF_QuestionCard_ApplyNegScoringForEachInCorrectAnswer_1] DEFAULT ((0)) NOT NULL,
    [UsePrimaryTopics]                                    BIT              CONSTRAINT [DF_QuestionCard_UsePrimaryTopics] DEFAULT ((0)) NOT NULL,
    [TopNSelected]                                        INT              CONSTRAINT [DF_QuestionCard_TopNSelected] DEFAULT ((0)) NOT NULL,
    [AssociatedImageUrlFriendlyName]                      NVARCHAR (50)    NULL,
    [MinDataPointsForCompanyResult]                       INT              CONSTRAINT [DF_QuestionCard_MinDataPointsForCompanyResult] DEFAULT ((30)) NOT NULL,
    [QuestionMultimediaThumbnailURL]                      NVARCHAR (MAX)   NULL,
    [QuestionMultimediaThumbnailURLFriendlyName]          NVARCHAR (MAX)   NULL,
    [AnswerExplanationMultimediaThumbnailURL]             NVARCHAR (MAX)   NULL,
    [AnswerExplanationMultimediaThumbnailURLFriendlyName] NVARCHAR (MAX)   NULL,
    [MustShowCompanyResults]                              BIT              CONSTRAINT [DF_QuestionCard_MustShowCompanyResults] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_QuestionCard] PRIMARY KEY CLUSTERED ([QuestionCardID] ASC)
);






GO
CREATE TRIGGER [tgAuditQuestionCard] 
		   ON  [dbo].[QuestionCard] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditQuestionCard
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [AnswerIsCaseSensitive],  [QuestionIsOnlyCorrectIfAllAnswersAreTrue],  [AssociatedImageUrl],  [QuestionMultimediaStartAutoElsePushPlayToStart],  [QuestionMultimediaShowPlaybackControls],  [AnswerMultimediaStartAutoElsePushPlayToStart],  [AnswerMultimediaShowPlaybackControls],  [QuestionMultimediaURLFriendlyName],  [QuestionMultimediaURL],  [QuestionMultimediaType_ImageVideoetc],  [AnswerExplanationMultimediaURLFriendlyName],  [AnswerExplanationMultimediaURL],  [AnswerExplanationMultimediaType_ImageVideoetc],  [DesktopCurrentWinningScore],  [DesktopCurrentLosingScore],  [DesktopNonCurrentWinningScore],  [DesktopNonCurrentLosingScore],  [OnlineWinningScore],  [OnlineLosingScore],  [OfferOTHERTextOption],  [TextOptionInputIsTextArea],  [ApplyScoreByCardElseForEachCorrectAnswer],  [OfferOptOutChoice],  [ApplyNegScoringForEachInCorrectAnswer],  [UsePrimaryTopics],  [TopNSelected],  [AssociatedImageUrlFriendlyName],  [MinDataPointsForCompanyResult],  [QuestionMultimediaThumbnailURL],  [QuestionMultimediaThumbnailURLFriendlyName],  [AnswerExplanationMultimediaThumbnailURL],  [AnswerExplanationMultimediaThumbnailURLFriendlyName],  [MustShowCompanyResults]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditQuestionCard
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [AnswerIsCaseSensitive],  [QuestionIsOnlyCorrectIfAllAnswersAreTrue],  [AssociatedImageUrl],  [QuestionMultimediaStartAutoElsePushPlayToStart],  [QuestionMultimediaShowPlaybackControls],  [AnswerMultimediaStartAutoElsePushPlayToStart],  [AnswerMultimediaShowPlaybackControls],  [QuestionMultimediaURLFriendlyName],  [QuestionMultimediaURL],  [QuestionMultimediaType_ImageVideoetc],  [AnswerExplanationMultimediaURLFriendlyName],  [AnswerExplanationMultimediaURL],  [AnswerExplanationMultimediaType_ImageVideoetc],  [DesktopCurrentWinningScore],  [DesktopCurrentLosingScore],  [DesktopNonCurrentWinningScore],  [DesktopNonCurrentLosingScore],  [OnlineWinningScore],  [OnlineLosingScore],  [OfferOTHERTextOption],  [TextOptionInputIsTextArea],  [ApplyScoreByCardElseForEachCorrectAnswer],  [OfferOptOutChoice],  [ApplyNegScoringForEachInCorrectAnswer],  [UsePrimaryTopics],  [TopNSelected],  [AssociatedImageUrlFriendlyName],  [MinDataPointsForCompanyResult],  [QuestionMultimediaThumbnailURL],  [QuestionMultimediaThumbnailURLFriendlyName],  [AnswerExplanationMultimediaThumbnailURL],  [AnswerExplanationMultimediaThumbnailURLFriendlyName],  [MustShowCompanyResults]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditQuestionCard
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [AnswerIsCaseSensitive],  [QuestionIsOnlyCorrectIfAllAnswersAreTrue],  [AssociatedImageUrl],  [QuestionMultimediaStartAutoElsePushPlayToStart],  [QuestionMultimediaShowPlaybackControls],  [AnswerMultimediaStartAutoElsePushPlayToStart],  [AnswerMultimediaShowPlaybackControls],  [QuestionMultimediaURLFriendlyName],  [QuestionMultimediaURL],  [QuestionMultimediaType_ImageVideoetc],  [AnswerExplanationMultimediaURLFriendlyName],  [AnswerExplanationMultimediaURL],  [AnswerExplanationMultimediaType_ImageVideoetc],  [DesktopCurrentWinningScore],  [DesktopCurrentLosingScore],  [DesktopNonCurrentWinningScore],  [DesktopNonCurrentLosingScore],  [OnlineWinningScore],  [OnlineLosingScore],  [OfferOTHERTextOption],  [TextOptionInputIsTextArea],  [ApplyScoreByCardElseForEachCorrectAnswer],  [OfferOptOutChoice],  [ApplyNegScoringForEachInCorrectAnswer],  [UsePrimaryTopics],  [TopNSelected],  [AssociatedImageUrlFriendlyName],  [MinDataPointsForCompanyResult],  [QuestionMultimediaThumbnailURL],  [QuestionMultimediaThumbnailURLFriendlyName],  [AnswerExplanationMultimediaThumbnailURL],  [AnswerExplanationMultimediaThumbnailURLFriendlyName],  [MustShowCompanyResults]
				FROM deleted
			END
		END