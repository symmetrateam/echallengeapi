﻿CREATE TABLE [dbo].[Product] (
    [ProductID]              INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]      DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime] DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]          BIT              CONSTRAINT [DF_Product_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]           BIT              CONSTRAINT [DF_Product_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [DefaultLanguageID]      INT              NOT NULL,
    [DefaultProductTheme]    NVARCHAR (255)   NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductID] ASC),
    CONSTRAINT [FK_Product_Language] FOREIGN KEY ([DefaultLanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);






GO
CREATE TRIGGER [tgAuditProduct] 
		   ON  [dbo].[Product] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditProduct
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ProductID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DefaultLanguageID],  [DefaultProductTheme]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditProduct
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ProductID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DefaultLanguageID],  [DefaultProductTheme]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditProduct
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ProductID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DefaultLanguageID],  [DefaultProductTheme]
				FROM deleted
			END
		END