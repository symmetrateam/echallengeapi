﻿CREATE TABLE [dbo].[AuditBranchingCard_Button] (
    [ID]                        INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                 CHAR (1)         NOT NULL,
    [RecordedAtDateTime]        DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [BranchingCard_ButtonID]    INT              NULL,
    [UserID_CreatedBy]          UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]     UNIQUEIDENTIFIER NULL,
    [LastModifiedOnDateTime]    DATETIME2 (7)    NULL,
    [IsADeletedRow]             BIT              NULL,
    [IsAnAuditRow]              BIT              NULL,
    [BranchingCardID]           INT              NULL,
    [Position]                  INT              NULL,
    [CardGroupID_Result]        INT              NULL,
    [IsMultiClick]              BIT              NULL,
    [MustReturnToBranchingCard] BIT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);





