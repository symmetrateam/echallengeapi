﻿CREATE TABLE [dbo].[BranchingCard] (
    [CardID]                            INT           NOT NULL,
    [GameID]                            INT           NOT NULL,
    [MustPressButtonsInSequence]        BIT           CONSTRAINT [DF_BranchingCard_MustPressButtonsInSequenct] DEFAULT ((0)) NOT NULL,
    [MustAddBackgroundBorderToBranches] BIT           CONSTRAINT [DF_BranchingCard_MustAddBackgroundBorderToBranches] DEFAULT ((0)) NOT NULL,
    [MinButtonPresses]                  INT           NOT NULL,
    [ButtonOrientation]                 NVARCHAR (50) NOT NULL,
    [CardGroupID_BranchingCompleted]    INT           NOT NULL,
    CONSTRAINT [PK_Card_BranchingCard_1] PRIMARY KEY CLUSTERED ([CardID] ASC),
    CONSTRAINT [FK_BranchingCard_Card] FOREIGN KEY ([CardID]) REFERENCES [dbo].[Card] ([CardID]),
    CONSTRAINT [FK_BranchingCard_Game] FOREIGN KEY ([GameID]) REFERENCES [dbo].[Game] ([GameID])
);




GO
CREATE TRIGGER [tgAuditBranchingCard] 
		   ON  [dbo].[BranchingCard] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditBranchingCard
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardID],  [GameID],  [MustPressButtonsInSequence],  [MustAddBackgroundBorderToBranches],  [MinButtonPresses],  [ButtonOrientation],  [CardGroupID_BranchingCompleted]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditBranchingCard
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardID],  [GameID],  [MustPressButtonsInSequence],  [MustAddBackgroundBorderToBranches],  [MinButtonPresses],  [ButtonOrientation],  [CardGroupID_BranchingCompleted]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditBranchingCard
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardID],  [GameID],  [MustPressButtonsInSequence],  [MustAddBackgroundBorderToBranches],  [MinButtonPresses],  [ButtonOrientation],  [CardGroupID_BranchingCompleted]
				FROM deleted
			END
		END