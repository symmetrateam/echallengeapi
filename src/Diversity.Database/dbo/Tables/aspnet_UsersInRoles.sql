﻿CREATE TABLE [dbo].[aspnet_UsersInRoles] (
    [UserId] UNIQUEIDENTIFIER NOT NULL,
    [RoleId] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC),
    FOREIGN KEY ([RoleId]) REFERENCES [dbo].[aspnet_Roles] ([RoleId]),
    CONSTRAINT [FK__aspnet_Us__UserI__448B0BA5] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
);




GO
CREATE NONCLUSTERED INDEX [aspnet_UsersInRoles_index]
    ON [dbo].[aspnet_UsersInRoles]([RoleId] ASC);


GO
CREATE TRIGGER [tgAuditaspnet_UsersInRoles] 
		   ON  [dbo].[aspnet_UsersInRoles] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO Auditaspnet_UsersInRoles
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [UserId],  [RoleId]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO Auditaspnet_UsersInRoles
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [UserId],  [RoleId]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO Auditaspnet_UsersInRoles
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [UserId],  [RoleId]
				FROM deleted
			END
		END