﻿CREATE TABLE [dbo].[BranchingCard_ButtonLanguage] (
    [BranchingCard_ButtonLanguageID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]               UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]          UNIQUEIDENTIFIER NOT NULL,
    [LastModifiedOnDateTime]         DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                  BIT              CONSTRAINT [DF_BranchingCard_ButtonLanguage_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                   BIT              CONSTRAINT [DF_BranchingCard_ButtonLanguage_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [BranchingCard_ButtonID]         INT              NOT NULL,
    [LanguageID]                     INT              NOT NULL,
    [ButtonText]                     NVARCHAR (50)    NOT NULL,
    [ButtonImageMultimediaURL]       NVARCHAR (MAX)   NULL,
    [ButtonImageFriendlyName]        NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_BranchingCard_ButtonLanguage] PRIMARY KEY CLUSTERED ([BranchingCard_ButtonLanguageID] ASC),
    CONSTRAINT [FK_BranchingCard_ButtonLanguage_BranchingCard_Button] FOREIGN KEY ([BranchingCard_ButtonID]) REFERENCES [dbo].[BranchingCard_Button] ([BranchingCard_ButtonID]),
    CONSTRAINT [FK_BranchingCard_ButtonLanguage_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);














GO
CREATE TRIGGER [tgAuditBranchingCard_ButtonLanguage] 
		   ON  [dbo].[BranchingCard_ButtonLanguage] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditBranchingCard_ButtonLanguage
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [BranchingCard_ButtonLanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [BranchingCard_ButtonID],  [LanguageID],  [ButtonText],  [ButtonImageMultimediaURL],  [ButtonImageFriendlyName]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditBranchingCard_ButtonLanguage
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [BranchingCard_ButtonLanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [BranchingCard_ButtonID],  [LanguageID],  [ButtonText],  [ButtonImageMultimediaURL],  [ButtonImageFriendlyName]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditBranchingCard_ButtonLanguage
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [BranchingCard_ButtonLanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [BranchingCard_ButtonID],  [LanguageID],  [ButtonText],  [ButtonImageMultimediaURL],  [ButtonImageFriendlyName]
				FROM deleted
			END
		END