﻿CREATE TABLE [dbo].[AuditSession] (
    [ID]                  INT              IDENTITY (1, 1) NOT NULL,
    [Operation]           CHAR (1)         NOT NULL,
    [RecordedAtDateTime]  DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [UserID]              UNIQUEIDENTIFIER NULL,
    [ExpiresAt]           DATETIME2 (7)    NULL,
    [AuthenticationToken] NVARCHAR (255)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



