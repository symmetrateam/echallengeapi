﻿CREATE TABLE [dbo].[AuditClient] (
    [ID]                          INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                   CHAR (1)         NOT NULL,
    [RecordedAtDateTime]          DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [ClientID]                    INT              NULL,
    [UserID_CreatedBy]            UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]       UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]           DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]      DATETIME2 (7)    NULL,
    [IsADeletedRow]               BIT              NULL,
    [IsAnAuditRow]                BIT              NULL,
    [ClientName]                  NVARCHAR (255)   NULL,
    [PhysicalAddress1]            NVARCHAR (255)   NULL,
    [PhysicalAddress2]            NVARCHAR (255)   NULL,
    [PhysicalPostalCode]          NVARCHAR (255)   NULL,
    [PhysicalStateProvince]       NVARCHAR (255)   NULL,
    [PhysicalCountry]             NVARCHAR (255)   NULL,
    [PostalAddress1]              NVARCHAR (255)   NULL,
    [PostalAddress2]              NVARCHAR (255)   NULL,
    [PostalPostalCode]            NVARCHAR (255)   NULL,
    [PostalStateProvince]         NVARCHAR (255)   NULL,
    [PostalCountry]               NVARCHAR (255)   NULL,
    [ClientTel1]                  NVARCHAR (255)   NULL,
    [ClientTel2]                  NVARCHAR (255)   NULL,
    [ClientWebsite]               NVARCHAR (255)   NULL,
    [ClientNotes]                 NVARCHAR (MAX)   NULL,
    [ClientIndustrySector]        NVARCHAR (255)   NULL,
    [ClientNoOfEmployees]         INT              NULL,
    [ClientAnnualTurnover]        MONEY            NULL,
    [ClientLogoImageURL]          NVARCHAR (MAX)   NULL,
    [ClientLogoImageFriendlyName] NVARCHAR (MAX)   NULL,
    [ExternalID]                  NVARCHAR (50)    NULL,
    [ThemeID]                     INT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);







