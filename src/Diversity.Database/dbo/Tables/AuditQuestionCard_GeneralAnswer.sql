﻿CREATE TABLE [dbo].[AuditQuestionCard_GeneralAnswer] (
    [ID]                           INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                    CHAR (1)         NOT NULL,
    [RecordedAtDateTime]           DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [QuestionCard_GeneralAnswerID] INT              NULL,
    [UserID_CreatedBy]             UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]        UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]            DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]       DATETIME2 (7)    NULL,
    [IsADeletedRow]                BIT              NULL,
    [IsAnAuditRow]                 BIT              NULL,
    [QuestionCardID]               INT              NULL,
    [IsCorrectAnswer]              BIT              NULL,
    [SequenceNo]                   INT              NULL,
    [IsEqualTo]                    BIT              NULL,
    [IsBetween]                    BIT              NULL,
    [IsGreaterThan]                BIT              NULL,
    [IsGreaterThanOrEqualTo]       BIT              NULL,
    [IsLessThan]                   BIT              NULL,
    [IsLessThanOrEqualTo]          BIT              NULL,
    [IsNotEqualTo]                 BIT              NULL,
    [IsNotBetween]                 BIT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



