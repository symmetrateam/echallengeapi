﻿CREATE TABLE [dbo].[Game] (
    [GameID]                                         INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                               UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]                          UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                              DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]                         DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                                  BIT              CONSTRAINT [DF_Game_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                                   BIT              CONSTRAINT [DF_Game_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [GameID_Template]                                INT              NULL,
    [IsOnlineGame]                                   BIT              CONSTRAINT [DF_Game_IsOnlilneGame] DEFAULT ((0)) NOT NULL,
    [IsAGameTemplate]                                BIT              CONSTRAINT [DF_Game_IsATemplate] DEFAULT ((0)) NOT NULL,
    [GameNowLockedDown]                              BIT              CONSTRAINT [DF_Game_GameNowLockedDown] DEFAULT ((0)) NOT NULL,
    [GameConfigCanBeModifiedAcrossInstances]         BIT              CONSTRAINT [DF_Game_GameConfigCanBeModifiedAcrossInstances] DEFAULT ((0)) NOT NULL,
    [IsTimedGame]                                    BIT              CONSTRAINT [DF_Game_IsTimedGame] DEFAULT ((0)) NOT NULL,
    [UseCardTime]                                    BIT              CONSTRAINT [DF_Game_UseCardTime] DEFAULT ((0)) NOT NULL,
    [UseActualAvatarNames]                           BIT              CONSTRAINT [DF_Game_UseActualAvatarNames] DEFAULT ((0)) NOT NULL,
    [UseHistoricAvatarAnswers]                       BIT              CONSTRAINT [DF_Game_UseHistoricAvatarAnswers] DEFAULT ((0)) NOT NULL,
    [UseActualAvatarAnsweringTimes]                  BIT              CONSTRAINT [DF_Game_UseActualAvatarAnsweringTimes] DEFAULT ((0)) NOT NULL,
    [AllUsersMustFinishThisGameByThisDate]           DATETIME2 (7)    NULL,
    [PlayerTurn_RandomNoRepeatingUntilAllHavePlayed] BIT              CONSTRAINT [DF_Game_PlayerTurn_RandomNoRepeatingUntilAllHavePlayed] DEFAULT ((0)) NOT NULL,
    [PlayerTurn_ByPlayerSequenceNoElseRandom]        BIT              CONSTRAINT [DF_Game_PlayerTurn_ByPlayerSequenceNo] DEFAULT ((0)) NOT NULL,
    [DefaultGameTimeInSeconds]                       INT              NULL,
    [GameCompletionDate]                             DATETIME2 (7)    NULL,
    [GameStartDate]                                  DATETIME2 (7)    NULL,
    [GameThemeData]                                  NVARCHAR (MAX)   NULL,
    [CanSkipTutorial]                                BIT              CONSTRAINT [DF_Game_CanSkipTutorial] DEFAULT ((1)) NOT NULL,
    [GameTemplateNameForDashboard]                   NVARCHAR (255)   NOT NULL,
    [HasBeenDownloaded]                              BIT              CONSTRAINT [DF_Game_HasBeenDownloaded] DEFAULT ((0)) NOT NULL,
    [NumberOfTimesRun]                               INT              NULL,
    [ProductID]                                      INT              NOT NULL,
    [NumberOfTimesGameCanBePlayed]                   INT              CONSTRAINT [DF_Game_NumberOfTimesGameCanBePlayed] DEFAULT ((1)) NULL,
    [ExternalID]                                     NVARCHAR (50)    NOT NULL,
    [ThemeClientUserGroupExternalID]                 NVARCHAR (50)    NULL,
    CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED ([GameID] ASC),
    CONSTRAINT [FK_Game_CreatedByUser] FOREIGN KEY ([UserID_CreatedBy]) REFERENCES [dbo].[aspnet_Users] ([UserId]),
    CONSTRAINT [FK_Game_Game] FOREIGN KEY ([GameID_Template]) REFERENCES [dbo].[Game] ([GameID]),
    CONSTRAINT [FK_Game_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Product] ([ProductID])
);










GO

CREATE TRIGGER [dbo].[tgAuditGame] 
				   ON  [dbo].[Game] 
				   AFTER INSERT, UPDATE, DELETE
				AS 
				BEGIN
					SET NOCOUNT ON;

					IF(SELECT COUNT(*) FROM inserted) > 0
					BEGIN
		
						IF(SELECT COUNT(*) FROM deleted) > 0
						BEGIN
							--Update
							INSERT INTO AuditGame
							SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [GameID_Template],  [IsOnlineGame],  [IsAGameTemplate],  [GameNowLockedDown],  [GameConfigCanBeModifiedAcrossInstances],  [IsTimedGame],  [UseCardTime],  [UseActualAvatarNames],  [UseHistoricAvatarAnswers],  [UseActualAvatarAnsweringTimes],  [AllUsersMustFinishThisGameByThisDate],  [PlayerTurn_RandomNoRepeatingUntilAllHavePlayed],  [PlayerTurn_ByPlayerSequenceNoElseRandom],  [DefaultGameTimeInSeconds],  [GameCompletionDate],  [GameStartDate],  [GameThemeData],  [CanSkipTutorial],  [GameTemplateNameForDashboard],  [HasBeenDownloaded],  [NumberOfTimesRun],  [ProductID],  [NumberOfTimesGameCanBePlayed],  [ExternalID], [ThemeClientUserGroupExternalID]
							FROM inserted 
						END
						ELSE
						BEGIN
							--Insert
							INSERT INTO AuditGame
							SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [GameID_Template],  [IsOnlineGame],  [IsAGameTemplate],  [GameNowLockedDown],  [GameConfigCanBeModifiedAcrossInstances],  [IsTimedGame],  [UseCardTime],  [UseActualAvatarNames],  [UseHistoricAvatarAnswers],  [UseActualAvatarAnsweringTimes],  [AllUsersMustFinishThisGameByThisDate],  [PlayerTurn_RandomNoRepeatingUntilAllHavePlayed],  [PlayerTurn_ByPlayerSequenceNoElseRandom],  [DefaultGameTimeInSeconds],  [GameCompletionDate],  [GameStartDate],  [GameThemeData],  [CanSkipTutorial],  [GameTemplateNameForDashboard],  [HasBeenDownloaded],  [NumberOfTimesRun],  [ProductID],  [NumberOfTimesGameCanBePlayed],  [ExternalID], [ThemeClientUserGroupExternalID]
							FROM inserted 
						END
					END
					ELSE
					BEGIN
						INSERT INTO AuditGame
						SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [GameID_Template],  [IsOnlineGame],  [IsAGameTemplate],  [GameNowLockedDown],  [GameConfigCanBeModifiedAcrossInstances],  [IsTimedGame],  [UseCardTime],  [UseActualAvatarNames],  [UseHistoricAvatarAnswers],  [UseActualAvatarAnsweringTimes],  [AllUsersMustFinishThisGameByThisDate],  [PlayerTurn_RandomNoRepeatingUntilAllHavePlayed],  [PlayerTurn_ByPlayerSequenceNoElseRandom],  [DefaultGameTimeInSeconds],  [GameCompletionDate],  [GameStartDate],  [GameThemeData],  [CanSkipTutorial],  [GameTemplateNameForDashboard],  [HasBeenDownloaded],  [NumberOfTimesRun],  [ProductID],  [NumberOfTimesGameCanBePlayed],  [ExternalID], [ThemeClientUserGroupExternalID]
						FROM deleted
					END
				END