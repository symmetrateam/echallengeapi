﻿CREATE TABLE [dbo].[AuditCard2CardClassification] (
    [ID]                        INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                 CHAR (1)         NOT NULL,
    [RecordedAtDateTime]        DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [Card2CardClassificationID] INT              NULL,
    [UserID_CreateBy]           UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]     UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]         DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]    DATETIME2 (7)    NULL,
    [IsADeletedRow]             BIT              NULL,
    [IsAnAuditRow]              BIT              NULL,
    [CardID]                    INT              NULL,
    [CardClassificationGroupID] INT              NULL,
    [IsAPrimaryCard]            BIT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



