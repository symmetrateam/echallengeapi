﻿CREATE TABLE [dbo].[AuditDistributor] (
    [ID]                                  INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                           CHAR (1)         NOT NULL,
    [RecordedAtDateTime]                  DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [DistributorID]                       INT              NULL,
    [UserID_CreatedBy]                    UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]               UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                   DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]              DATETIME2 (7)    NULL,
    [IsADeletedRow]                       BIT              NULL,
    [IsAnAuditRow]                        BIT              NULL,
    [PhysicalAddress1]                    NVARCHAR (255)   NULL,
    [PhysicalAddress2]                    NVARCHAR (255)   NULL,
    [PhysicalPostalCode]                  NVARCHAR (255)   NULL,
    [PhysicalStateProvince]               NVARCHAR (255)   NULL,
    [PhysicalCountry]                     NVARCHAR (255)   NULL,
    [PostalAddress1]                      NVARCHAR (255)   NULL,
    [PostalAddress2]                      NVARCHAR (255)   NULL,
    [PostalPostalCode]                    NVARCHAR (255)   NULL,
    [PostalStateProvince]                 NVARCHAR (255)   NULL,
    [PostalCountry]                       NVARCHAR (255)   NULL,
    [DistributorName]                     NVARCHAR (255)   NULL,
    [DistributorTel1]                     NVARCHAR (255)   NULL,
    [DistributorEmailAddress]             NVARCHAR (255)   NULL,
    [DistributorCanCreateProducts]        BIT              NULL,
    [DistributorNotes]                    NVARCHAR (MAX)   NULL,
    [MaxNoOfProductsDistributorCanCreate] INT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



