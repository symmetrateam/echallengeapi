﻿CREATE TABLE [dbo].[ErrorMessage] (
    [ErrorMessageID] INT           IDENTITY (1, 1) NOT NULL,
    [ErrorCode]      NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ErrorMessage] PRIMARY KEY CLUSTERED ([ErrorMessageID] ASC)
);




GO
CREATE TRIGGER [tgAuditErrorMessage] 
		   ON  [dbo].[ErrorMessage] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditErrorMessage
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ErrorMessageID],  [ErrorCode]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditErrorMessage
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ErrorMessageID],  [ErrorCode]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditErrorMessage
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ErrorMessageID],  [ErrorCode]
				FROM deleted
			END
		END