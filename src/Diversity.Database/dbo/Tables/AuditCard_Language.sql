﻿CREATE TABLE [dbo].[AuditCard_Language] (
    [ID]                      INT              IDENTITY (1, 1) NOT NULL,
    [Operation]               CHAR (1)         NOT NULL,
    [RecordedAtDateTime]      DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [Card_LanguageID]         INT              NULL,
    [UserID_CreatedBy]        UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]   UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]       DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]  DATETIME2 (7)    NULL,
    [IsADeletedRow]           BIT              NULL,
    [IsAnAuditRow]            BIT              NULL,
    [CardID]                  INT              NULL,
    [LanguageID]              INT              NULL,
    [CardName]                NVARCHAR (255)   NULL,
    [CardTitle]               NVARCHAR (255)   NULL,
    [DetailedCardDescription] NVARCHAR (MAX)   NULL,
    [FacilitatorSlideNotes]   NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



