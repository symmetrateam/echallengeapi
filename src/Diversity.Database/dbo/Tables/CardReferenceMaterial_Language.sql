﻿CREATE TABLE [dbo].[CardReferenceMaterial_Language] (
    [CardReferenceMaterial_LanguageID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                 UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]           DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                    BIT              CONSTRAINT [DF_CardReferenceMaterial_Language_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                     BIT              CONSTRAINT [DF_CardReferenceMaterial_Language_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [CardReferenceMaterialID]          INT              NOT NULL,
    [LanguageID]                       INT              NOT NULL,
    [CardReferenceMaterialDescription] NVARCHAR (256)   NOT NULL,
    [CardReferenceMaterialUrl]         NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_CardReferenceMaterial_Language] PRIMARY KEY CLUSTERED ([CardReferenceMaterial_LanguageID] ASC),
    CONSTRAINT [FK_CardReferenceMaterial_Language_CardReferenceMaterial] FOREIGN KEY ([CardReferenceMaterialID]) REFERENCES [dbo].[CardReferenceMaterial] ([CardReferenceMaterialID]) ON DELETE CASCADE,
    CONSTRAINT [FK_CardReferenceMaterial_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_CardReferenceMaterial_Language_Language1] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);






GO
CREATE TRIGGER [tgAuditCardReferenceMaterial_Language] 
		   ON  [dbo].[CardReferenceMaterial_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditCardReferenceMaterial_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardReferenceMaterial_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardReferenceMaterialID],  [LanguageID],  [CardReferenceMaterialDescription],  [CardReferenceMaterialUrl]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditCardReferenceMaterial_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardReferenceMaterial_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardReferenceMaterialID],  [LanguageID],  [CardReferenceMaterialDescription],  [CardReferenceMaterialUrl]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditCardReferenceMaterial_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardReferenceMaterial_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardReferenceMaterialID],  [LanguageID],  [CardReferenceMaterialDescription],  [CardReferenceMaterialUrl]
				FROM deleted
			END
		END