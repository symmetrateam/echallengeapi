﻿CREATE TABLE [dbo].[AuditQuestionCard_Language] (
    [ID]                              INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                       CHAR (1)         NOT NULL,
    [RecordedAtDateTime]              DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [QuestionCard_LanguageID]         INT              NULL,
    [UserID_CreatedBy]                UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]           UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]               DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]          DATETIME2 (7)    NULL,
    [IsADeletedRow]                   BIT              NULL,
    [IsAnAuditRow]                    BIT              NULL,
    [QuestionCardID]                  INT              NULL,
    [LanguageID]                      INT              NULL,
    [Question_Statement_LongVersion]  NVARCHAR (MAX)   NULL,
    [Question_Statement_ShortVersion] NVARCHAR (MAX)   NULL,
    [AnswerExplanation_LongVersion]   NVARCHAR (MAX)   NULL,
    [AnswerExplanation_ShortVersion]  NVARCHAR (MAX)   NULL,
    [StatementHeader]                 NVARCHAR (MAX)   NULL,
    [StatementLocation]               NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);





