﻿CREATE TABLE [dbo].[DistributorClient2DistributorProduct] (
    [DistributorClient2DistributorProductID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                       UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]                  UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                      DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]                 DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                          BIT              CONSTRAINT [DF_DistributorClient2DistributorProduct_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                           BIT              CONSTRAINT [DF_DistributorClient2DistributorProduct_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [Client2DistributorID]                   INT              NOT NULL,
    [Distributor2ProductID]                  INT              NOT NULL,
    [QuestionMix]                            NVARCHAR (50)    CONSTRAINT [DF_DistributorClient2DistributorProduct_QuestionMix_RatifiedOnly] DEFAULT ((0)) NULL,
    [OnlyShowClientCustomAvatars]            BIT              CONSTRAINT [DF_DistributorClient2DistributorProduct_OnlyShowClientCustomAvatars] DEFAULT ((0)) NOT NULL,
    [ClientSpecificProductTheme]             NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_DistributorClient2DistributorProduct] PRIMARY KEY CLUSTERED ([DistributorClient2DistributorProductID] ASC),
    CONSTRAINT [FK_DistributorClient2DistributorProduct_Distributor2Client] FOREIGN KEY ([Client2DistributorID]) REFERENCES [dbo].[Client2Distributor] ([Client2DistributorID]) ON DELETE CASCADE,
    CONSTRAINT [FK_DistributorClient2DistributorProduct_Distributor2Product] FOREIGN KEY ([Distributor2ProductID]) REFERENCES [dbo].[Product2Distributor] ([Product2DistributorID])
);






GO
CREATE TRIGGER [tgAuditDistributorClient2DistributorProduct] 
		   ON  [dbo].[DistributorClient2DistributorProduct] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditDistributorClient2DistributorProduct
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [DistributorClient2DistributorProductID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [Client2DistributorID],  [Distributor2ProductID],  [QuestionMix],  [OnlyShowClientCustomAvatars],  [ClientSpecificProductTheme]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditDistributorClient2DistributorProduct
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [DistributorClient2DistributorProductID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [Client2DistributorID],  [Distributor2ProductID],  [QuestionMix],  [OnlyShowClientCustomAvatars],  [ClientSpecificProductTheme]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditDistributorClient2DistributorProduct
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [DistributorClient2DistributorProductID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [Client2DistributorID],  [Distributor2ProductID],  [QuestionMix],  [OnlyShowClientCustomAvatars],  [ClientSpecificProductTheme]
				FROM deleted
			END
		END