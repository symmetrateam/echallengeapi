﻿CREATE TABLE [dbo].[Result] (
    [ResultID]               INT              IDENTITY (1, 1) NOT NULL,
    [GameID]                 INT              NOT NULL,
    [CardID]                 INT              NOT NULL,
    [Internal_CardTypeID]    INT              NOT NULL,
    [ResultType]             NVARCHAR (100)   NOT NULL,
    [ResultXml]              XML              NOT NULL,
    [UserID_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]      DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime] DATETIME2 (7)    NOT NULL,
    [GamePlayerID]           INT              NOT NULL,
    CONSTRAINT [PK_Result] PRIMARY KEY CLUSTERED ([ResultID] ASC),
    CONSTRAINT [FK_Result_aspnet_Users] FOREIGN KEY ([UserID_CreatedBy]) REFERENCES [dbo].[aspnet_Users] ([UserId]),
    CONSTRAINT [FK_Result_Card] FOREIGN KEY ([CardID]) REFERENCES [dbo].[Card] ([CardID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Result_Game] FOREIGN KEY ([GameID]) REFERENCES [dbo].[Game] ([GameID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Result_GamePlayer] FOREIGN KEY ([GamePlayerID]) REFERENCES [dbo].[GamePlayer] ([GamePlayerID]),
    CONSTRAINT [FK_Result_Internal_CardType] FOREIGN KEY ([Internal_CardTypeID]) REFERENCES [dbo].[Internal_CardType] ([Internal_CardTypeID]) ON DELETE CASCADE
);










GO
CREATE TRIGGER [tgAuditResult] 
		   ON  [dbo].[Result] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditResult
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ResultID],  [GameID],  [CardID],  [Internal_CardTypeID],  [ResultType],  [ResultXml],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [GamePlayerID]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditResult
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ResultID],  [GameID],  [CardID],  [Internal_CardTypeID],  [ResultType],  [ResultXml],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [GamePlayerID]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditResult
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ResultID],  [GameID],  [CardID],  [Internal_CardTypeID],  [ResultType],  [ResultXml],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [GamePlayerID]
				FROM deleted
			END
		END