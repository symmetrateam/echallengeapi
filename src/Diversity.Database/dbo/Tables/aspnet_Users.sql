﻿CREATE TABLE [dbo].[aspnet_Users] (
    [ApplicationId]               UNIQUEIDENTIFIER NOT NULL,
    [UserId]                      UNIQUEIDENTIFIER CONSTRAINT [DF__aspnet_Us__UserI__096A45D7] DEFAULT (newid()) NOT NULL,
    [UserName]                    NVARCHAR (256)   NOT NULL,
    [LoweredUserName]             NVARCHAR (256)   NOT NULL,
    [MobileAlias]                 NVARCHAR (16)    CONSTRAINT [DF__aspnet_Us__Mobil__0A5E6A10] DEFAULT (NULL) NULL,
    [IsAnonymous]                 BIT              CONSTRAINT [DF__aspnet_Us__IsAno__0B528E49] DEFAULT ((0)) NOT NULL,
    [LastActivityDate]            DATETIME         NOT NULL,
    [UserID_CreatedBy]            UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]       UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]           DATETIME         NULL,
    [LastModifiedOnDateTime]      DATETIME         NULL,
    [IsADeletedRow]               BIT              CONSTRAINT [DF_aspnet_Users_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                BIT              CONSTRAINT [DF_aspnet_Users_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [User2ClientID]               INT              NULL,
    [PlayerAvatarData]            NVARCHAR (MAX)   NULL,
    [PlayerAvatarName]            NVARCHAR (255)   NULL,
    [PlayerAvatarGender]          NVARCHAR (1)     NULL,
    [IsAnonymousUser]             BIT              CONSTRAINT [DF_aspnet_Users_IsAnonymousUser] DEFAULT ((0)) NOT NULL,
    [UserFirstName]               NVARCHAR (255)   NULL,
    [UserLastName]                NVARCHAR (255)   NULL,
    [UserGender]                  NVARCHAR (1)     NULL,
    [UserDateOfBirth]             DATETIME         NULL,
    [UserPersonalEmail1]          NVARCHAR (255)   NULL,
    [UserPersonalMobile]          NVARCHAR (255)   NULL,
    [UserWorkEmail]               NVARCHAR (255)   NULL,
    [UserWorkTel1]                NVARCHAR (255)   NULL,
    [UserWorkTel2]                NVARCHAR (255)   NULL,
    [UserWorkMobile]              NVARCHAR (255)   NULL,
    [UserWorkNotes]               NVARCHAR (MAX)   NULL,
    [ClientUserGroup]             NVARCHAR (255)   NULL,
    [ClientBusinessUnit]          NVARCHAR (255)   NULL,
    [ClientLocation]              NVARCHAR (255)   NULL,
    [ClientCostCentre]            NVARCHAR (255)   NULL,
    [ClientDivision]              NVARCHAR (255)   NULL,
    [ClientJobTitle]              NVARCHAR (255)   NULL,
    [ClientUniqueSalaryNo]        NVARCHAR (255)   NULL,
    [ClientSubsidiaryCompanyName] NVARCHAR (255)   NULL,
    [HasViewedGameTutorial]       BIT              CONSTRAINT [DF_aspnet_Users_HasViewedGameTutorial] DEFAULT ((0)) NOT NULL,
    [ScormUserID]                 NVARCHAR (50)    NULL,
    CONSTRAINT [PK__aspnet_U__1788CC4D068DD92C] PRIMARY KEY NONCLUSTERED ([UserId] ASC),
    CONSTRAINT [FK__aspnet_Us__Appli__0876219E] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId]),
    CONSTRAINT [FK_aspnet_Users_User2Client] FOREIGN KEY ([User2ClientID]) REFERENCES [dbo].[User2Client] ([User2ClientID])
);








GO
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2]
    ON [dbo].[aspnet_Users]([ApplicationId] ASC, [LastActivityDate] ASC);


GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index]
    ON [dbo].[aspnet_Users]([ApplicationId] ASC, [LoweredUserName] ASC);


GO
CREATE TRIGGER [tgAuditaspnet_Users] 
				   ON  [dbo].[aspnet_Users] 
				   AFTER INSERT, UPDATE, DELETE
				AS 
				BEGIN
					SET NOCOUNT ON;

					IF(SELECT COUNT(*) FROM inserted) > 0
					BEGIN
		
						IF(SELECT COUNT(*) FROM deleted) > 0
						BEGIN
							--Update
							INSERT INTO Auditaspnet_Users
							SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [UserId],  [UserName],  [LoweredUserName],  [MobileAlias],  [IsAnonymous],  [LastActivityDate],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [User2ClientID],  [PlayerAvatarData],  [PlayerAvatarName],  [PlayerAvatarGender],  [IsAnonymousUser],  [UserFirstName],  [UserLastName],  [UserGender],  [UserDateOfBirth],  [UserPersonalEmail1],  [UserPersonalMobile],  [UserWorkEmail],  [UserWorkTel1],  [UserWorkTel2],  [UserWorkMobile],  [UserWorkNotes],  [ClientUserGroup],  [ClientBusinessUnit],  [ClientLocation],  [ClientCostCentre],  [ClientDivision],  [ClientJobTitle],  [ClientUniqueSalaryNo],  [ClientSubsidiaryCompanyName],  [HasViewedGameTutorial],  [ScormUserID]
							FROM inserted 
						END
						ELSE
						BEGIN
							--Insert
							INSERT INTO Auditaspnet_Users
							SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [UserId],  [UserName],  [LoweredUserName],  [MobileAlias],  [IsAnonymous],  [LastActivityDate],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [User2ClientID],  [PlayerAvatarData],  [PlayerAvatarName],  [PlayerAvatarGender],  [IsAnonymousUser],  [UserFirstName],  [UserLastName],  [UserGender],  [UserDateOfBirth],  [UserPersonalEmail1],  [UserPersonalMobile],  [UserWorkEmail],  [UserWorkTel1],  [UserWorkTel2],  [UserWorkMobile],  [UserWorkNotes],  [ClientUserGroup],  [ClientBusinessUnit],  [ClientLocation],  [ClientCostCentre],  [ClientDivision],  [ClientJobTitle],  [ClientUniqueSalaryNo],  [ClientSubsidiaryCompanyName],  [HasViewedGameTutorial],  [ScormUserID]
							FROM inserted 
						END
					END
					ELSE
					BEGIN
						INSERT INTO Auditaspnet_Users
						SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [UserId],  [UserName],  [LoweredUserName],  [MobileAlias],  [IsAnonymous],  [LastActivityDate],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [User2ClientID],  [PlayerAvatarData],  [PlayerAvatarName],  [PlayerAvatarGender],  [IsAnonymousUser],  [UserFirstName],  [UserLastName],  [UserGender],  [UserDateOfBirth],  [UserPersonalEmail1],  [UserPersonalMobile],  [UserWorkEmail],  [UserWorkTel1],  [UserWorkTel2],  [UserWorkMobile],  [UserWorkNotes],  [ClientUserGroup],  [ClientBusinessUnit],  [ClientLocation],  [ClientCostCentre],  [ClientDivision],  [ClientJobTitle],  [ClientUniqueSalaryNo],  [ClientSubsidiaryCompanyName],  [HasViewedGameTutorial],  [ScormUserID]
						FROM deleted
					END
				END