﻿CREATE TABLE [dbo].[Card] (
    [CardID]                                        INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                              UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]                         UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                             DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]                        DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                                 BIT              CONSTRAINT [DF_Card_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditedRow]                                BIT              CONSTRAINT [DF_Card_IsAnAuditedRow] DEFAULT ((0)) NOT NULL,
    [Internal_CardTypeID]                           INT              NOT NULL,
    [CardID_LCDCategoryCard]                        INT              NULL,
    [ClientID_ForLibrary]                           INT              NULL,
    [ProductID]                                     INT              NOT NULL,
    [QuestionCardID]                                INT              NULL,
    [CardShortcutCode]                              NVARCHAR (255)   NOT NULL,
    [IsForClientlibraryElsePublicLibrary]           BIT              CONSTRAINT [DF_Card_IsForPublicLibrary] DEFAULT ((0)) NOT NULL,
    [IgnoreAnyTimersOnThisCard]                     BIT              CONSTRAINT [DF_Card_IgnoreTimerOnThisCard] DEFAULT ((0)) NOT NULL,
    [IsStillInBeta_DoNotMakeAvailableForUseInGames] BIT              CONSTRAINT [DF_Card_IsStillInBeta_DoNotMakeAvailableForUseInGames] DEFAULT ((0)) NOT NULL,
    [IsATimedCard]                                  BIT              CONSTRAINT [DF_Card_IsATimedCard] DEFAULT ((0)) NOT NULL,
    [CardMaxTimeToAnswerInSeconds]                  INT              CONSTRAINT [DF_Card_CardMaxTimeToAnswerInSeconds] DEFAULT ((0)) NOT NULL,
    [IsApplicableForOnlineUse]                      BIT              CONSTRAINT [DF_Card_IsApplicableForOnlineUse] DEFAULT ((0)) NOT NULL,
    [IsApplicableForDesktopUse]                     BIT              CONSTRAINT [DF_Card_IsApplicableForDesktopUse] DEFAULT ((0)) NOT NULL,
    [FacilitatorSlideNotes]                         NVARCHAR (MAX)   NULL,
    [UseCardTimeNotGameTime]                        BIT              CONSTRAINT [DF_Card_UseCardTimeNotGameTime] DEFAULT ((0)) NOT NULL,
    [UserID_RatificationRequestor]                  UNIQUEIDENTIFIER NULL,
    [RatificationRequestedOn]                       DATETIME2 (7)    NULL,
    [RatificationRequestorNotes]                    NVARCHAR (MAX)   NULL,
    [UserID_RatificationApprover]                   UNIQUEIDENTIFIER NULL,
    [RatificationApprovedOn]                        DATETIME2 (7)    NULL,
    [RatificationApproverNotes]                     NVARCHAR (MAX)   NULL,
    [IsAwaitingRatification]                        BIT              CONSTRAINT [DF_Card_IsAwaitingRatification] DEFAULT ((0)) NOT NULL,
    [IsRatified]                                    BIT              CONSTRAINT [DF_Card_IsRatified] DEFAULT ((0)) NOT NULL,
    [IsDirtyCard_PreviouslyRatified]                BIT              CONSTRAINT [DF_Card_IsDirtyCard_PreviouslyRatified] DEFAULT ((0)) NOT NULL,
    [CardContentNextRefreshDate]                    DATETIME2 (7)    NULL,
    [DefaultLanguageID]                             INT              NOT NULL,
    [IsForGame]                                     BIT              CONSTRAINT [DF_Card_IsForGame] DEFAULT ((0)) NOT NULL,
    [AudioTranscriptFile]                           NVARCHAR (255)   NULL,
    [AudioTranscriptFriendlyName]                   NVARCHAR (255)   NULL,
    [AudioTranscriptText]                           NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED ([CardID] ASC),
    CONSTRAINT [FK_Card_Card] FOREIGN KEY ([CardID_LCDCategoryCard]) REFERENCES [dbo].[Card] ([CardID]),
    CONSTRAINT [FK_Card_Client] FOREIGN KEY ([ClientID_ForLibrary]) REFERENCES [dbo].[Client] ([ClientID]),
    CONSTRAINT [FK_Card_Internal_CardType] FOREIGN KEY ([Internal_CardTypeID]) REFERENCES [dbo].[Internal_CardType] ([Internal_CardTypeID]),
    CONSTRAINT [FK_Card_Language] FOREIGN KEY ([DefaultLanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_Card_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Product] ([ProductID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Card_QuestionCard] FOREIGN KEY ([QuestionCardID]) REFERENCES [dbo].[QuestionCard] ([QuestionCardID])
);






GO
CREATE TRIGGER [tgAuditCard] 
		   ON  [dbo].[Card] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditCard
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditedRow],  [Internal_CardTypeID],  [CardID_LCDCategoryCard],  [ClientID_ForLibrary],  [ProductID],  [QuestionCardID],  [CardShortcutCode],  [IsForClientlibraryElsePublicLibrary],  [IgnoreAnyTimersOnThisCard],  [IsStillInBeta_DoNotMakeAvailableForUseInGames],  [IsATimedCard],  [CardMaxTimeToAnswerInSeconds],  [IsApplicableForOnlineUse],  [IsApplicableForDesktopUse],  [FacilitatorSlideNotes],  [UseCardTimeNotGameTime],  [UserID_RatificationRequestor],  [RatificationRequestedOn],  [RatificationRequestorNotes],  [UserID_RatificationApprover],  [RatificationApprovedOn],  [RatificationApproverNotes],  [IsAwaitingRatification],  [IsRatified],  [IsDirtyCard_PreviouslyRatified],  [CardContentNextRefreshDate],  [DefaultLanguageID],  [IsForGame],  [AudioTranscriptFile],  [AudioTranscriptFriendlyName],  [AudioTranscriptText]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditCard
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditedRow],  [Internal_CardTypeID],  [CardID_LCDCategoryCard],  [ClientID_ForLibrary],  [ProductID],  [QuestionCardID],  [CardShortcutCode],  [IsForClientlibraryElsePublicLibrary],  [IgnoreAnyTimersOnThisCard],  [IsStillInBeta_DoNotMakeAvailableForUseInGames],  [IsATimedCard],  [CardMaxTimeToAnswerInSeconds],  [IsApplicableForOnlineUse],  [IsApplicableForDesktopUse],  [FacilitatorSlideNotes],  [UseCardTimeNotGameTime],  [UserID_RatificationRequestor],  [RatificationRequestedOn],  [RatificationRequestorNotes],  [UserID_RatificationApprover],  [RatificationApprovedOn],  [RatificationApproverNotes],  [IsAwaitingRatification],  [IsRatified],  [IsDirtyCard_PreviouslyRatified],  [CardContentNextRefreshDate],  [DefaultLanguageID],  [IsForGame],  [AudioTranscriptFile],  [AudioTranscriptFriendlyName],  [AudioTranscriptText]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditCard
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditedRow],  [Internal_CardTypeID],  [CardID_LCDCategoryCard],  [ClientID_ForLibrary],  [ProductID],  [QuestionCardID],  [CardShortcutCode],  [IsForClientlibraryElsePublicLibrary],  [IgnoreAnyTimersOnThisCard],  [IsStillInBeta_DoNotMakeAvailableForUseInGames],  [IsATimedCard],  [CardMaxTimeToAnswerInSeconds],  [IsApplicableForOnlineUse],  [IsApplicableForDesktopUse],  [FacilitatorSlideNotes],  [UseCardTimeNotGameTime],  [UserID_RatificationRequestor],  [RatificationRequestedOn],  [RatificationRequestorNotes],  [UserID_RatificationApprover],  [RatificationApprovedOn],  [RatificationApproverNotes],  [IsAwaitingRatification],  [IsRatified],  [IsDirtyCard_PreviouslyRatified],  [CardContentNextRefreshDate],  [DefaultLanguageID],  [IsForGame],  [AudioTranscriptFile],  [AudioTranscriptFriendlyName],  [AudioTranscriptText]
				FROM deleted
			END
		END