﻿CREATE TABLE [dbo].[AuditResult] (
    [ID]                     INT              IDENTITY (1, 1) NOT NULL,
    [Operation]              CHAR (1)         NOT NULL,
    [RecordedAtDateTime]     DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [ResultID]               INT              NULL,
    [GameID]                 INT              NULL,
    [CardID]                 INT              NULL,
    [Internal_CardTypeID]    INT              NULL,
    [ResultType]             NVARCHAR (100)   NULL,
    [ResultXml]              XML              NULL,
    [UserID_CreatedBy]       UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]  UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]      DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime] DATETIME2 (7)    NULL,
    [GamePlayerID]           INT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);





