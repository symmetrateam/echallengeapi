﻿CREATE TABLE [dbo].[AuditGameInvite] (
    [ID]                             INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                      CHAR (1)         NOT NULL,
    [RecordedAtDateTime]             DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [GameInviteID]                   INT              NULL,
    [UserID]                         UNIQUEIDENTIFIER NULL,
    [GameID]                         INT              NULL,
    [InviteDate]                     DATETIME2 (7)    NULL,
    [LastReminderDate]               DATETIME2 (7)    NULL,
    [UserID_CreatedBy]               UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]              DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]         DATETIME2 (7)    NULL,
    [UserID_LastModifiedBy]          UNIQUEIDENTIFIER NULL,
    [IsADeletedRow]                  BIT              NULL,
    [IsAnAuditRow]                   BIT              NULL,
    [EmailSubject]                   NVARCHAR (255)   NULL,
    [EmailContent]                   NVARCHAR (MAX)   NULL,
    [ThemeClientUserGroupExternalID] NVARCHAR (50)    NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);





