﻿CREATE TABLE [dbo].[Product_Language] (
    [Product_LanguageID]     INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]      DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime] DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]          BIT              CONSTRAINT [DF_Product_Language_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]           BIT              CONSTRAINT [DF_Product_Language_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [ProductID]              INT              NOT NULL,
    [LanguageID]             INT              NOT NULL,
    [ProductName]            NVARCHAR (100)   NOT NULL,
    [ProductDescription]     NVARCHAR (255)   NOT NULL,
    CONSTRAINT [PK_Product_Language] PRIMARY KEY CLUSTERED ([Product_LanguageID] ASC),
    CONSTRAINT [FK_Product_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_Product_Language_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Product] ([ProductID])
);






GO
CREATE TRIGGER [tgAuditProduct_Language] 
		   ON  [dbo].[Product_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditProduct_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Product_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ProductID],  [LanguageID],  [ProductName],  [ProductDescription]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditProduct_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Product_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ProductID],  [LanguageID],  [ProductName],  [ProductDescription]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditProduct_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Product_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ProductID],  [LanguageID],  [ProductName],  [ProductDescription]
				FROM deleted
			END
		END