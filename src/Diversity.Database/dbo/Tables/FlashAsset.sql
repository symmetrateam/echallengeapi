﻿CREATE TABLE [dbo].[FlashAsset] (
    [FlashAssetID] INT           IDENTITY (1, 1) NOT NULL,
    [Key]          NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_FlashAsset] PRIMARY KEY CLUSTERED ([FlashAssetID] ASC)
);




GO
CREATE TRIGGER [tgAuditFlashAsset] 
		   ON  [dbo].[FlashAsset] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditFlashAsset
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [FlashAssetID],  [Key]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditFlashAsset
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [FlashAssetID],  [Key]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditFlashAsset
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [FlashAssetID],  [Key]
				FROM deleted
			END
		END