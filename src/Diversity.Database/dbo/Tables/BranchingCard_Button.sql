﻿CREATE TABLE [dbo].[BranchingCard_Button] (
    [BranchingCard_ButtonID]    INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]          UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]     UNIQUEIDENTIFIER NOT NULL,
    [LastModifiedOnDateTime]    DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]             BIT              CONSTRAINT [DF_BranchingCard_Button_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]              BIT              CONSTRAINT [DF_BranchingCard_Button_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [BranchingCardID]           INT              NOT NULL,
    [Position]                  INT              NOT NULL,
    [CardGroupID_Result]        INT              NOT NULL,
    [IsMultiClick]              BIT              CONSTRAINT [DF_BranchingCard_Button_IsMultiClick] DEFAULT ((0)) NOT NULL,
    [MustReturnToBranchingCard] BIT              CONSTRAINT [DF_BranchingCard_Button_MustReturnToBranchingCard] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BranchingCard_Button] PRIMARY KEY CLUSTERED ([BranchingCard_ButtonID] ASC),
    CONSTRAINT [FK_BranchingCard_Button_BranchingCard] FOREIGN KEY ([BranchingCardID]) REFERENCES [dbo].[BranchingCard] ([CardID])
);








GO
CREATE TRIGGER [tgAuditBranchingCard_Button] 
		   ON  [dbo].[BranchingCard_Button] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditBranchingCard_Button
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [BranchingCard_ButtonID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [BranchingCardID],  [Position],  [CardGroupID_Result],  [IsMultiClick],  [MustReturnToBranchingCard]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditBranchingCard_Button
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [BranchingCard_ButtonID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [BranchingCardID],  [Position],  [CardGroupID_Result],  [IsMultiClick],  [MustReturnToBranchingCard]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditBranchingCard_Button
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [BranchingCard_ButtonID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [BranchingCardID],  [Position],  [CardGroupID_Result],  [IsMultiClick],  [MustReturnToBranchingCard]
				FROM deleted
			END
		END