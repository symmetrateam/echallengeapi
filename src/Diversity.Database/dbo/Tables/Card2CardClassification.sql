﻿CREATE TABLE [dbo].[Card2CardClassification] (
    [Card2CardClassificationID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreateBy]           UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]         DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]    DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]             BIT              CONSTRAINT [DF_Card2CardClassification_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]              BIT              CONSTRAINT [DF_Card2CardClassification_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [CardID]                    INT              NOT NULL,
    [CardClassificationGroupID] INT              NOT NULL,
    [IsAPrimaryCard]            BIT              CONSTRAINT [DF_Card2CardClassification_IsAPrimaryCard] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Card2CardClassification] PRIMARY KEY CLUSTERED ([Card2CardClassificationID] ASC),
    CONSTRAINT [FK_Card2CardClassification_Card] FOREIGN KEY ([CardID]) REFERENCES [dbo].[Card] ([CardID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Card2CardClassification_CardClassificationGroup] FOREIGN KEY ([CardClassificationGroupID]) REFERENCES [dbo].[CardClassificationGroup] ([CardClassificationGroupID])
);






GO
CREATE TRIGGER [tgAuditCard2CardClassification] 
		   ON  [dbo].[Card2CardClassification] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditCard2CardClassification
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Card2CardClassificationID],  [UserID_CreateBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [CardClassificationGroupID],  [IsAPrimaryCard]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditCard2CardClassification
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Card2CardClassificationID],  [UserID_CreateBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [CardClassificationGroupID],  [IsAPrimaryCard]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditCard2CardClassification
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Card2CardClassificationID],  [UserID_CreateBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [CardClassificationGroupID],  [IsAPrimaryCard]
				FROM deleted
			END
		END