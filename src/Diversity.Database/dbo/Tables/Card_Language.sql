﻿CREATE TABLE [dbo].[Card_Language] (
    [Card_LanguageID]         INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]       DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]  DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]           BIT              CONSTRAINT [DF_Card_Language_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]            BIT              CONSTRAINT [DF_Card_Language_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [CardID]                  INT              NOT NULL,
    [LanguageID]              INT              NOT NULL,
    [CardName]                NVARCHAR (255)   NOT NULL,
    [CardTitle]               NVARCHAR (255)   NULL,
    [DetailedCardDescription] NVARCHAR (MAX)   NOT NULL,
    [FacilitatorSlideNotes]   NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_Card_Language] PRIMARY KEY CLUSTERED ([Card_LanguageID] ASC),
    CONSTRAINT [FK_Card_Language_Card] FOREIGN KEY ([CardID]) REFERENCES [dbo].[Card] ([CardID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Card_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);






GO
CREATE TRIGGER [tgAuditCard_Language] 
		   ON  [dbo].[Card_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditCard_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Card_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [LanguageID],  [CardName],  [CardTitle],  [DetailedCardDescription],  [FacilitatorSlideNotes]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditCard_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Card_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [LanguageID],  [CardName],  [CardTitle],  [DetailedCardDescription],  [FacilitatorSlideNotes]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditCard_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Card_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [LanguageID],  [CardName],  [CardTitle],  [DetailedCardDescription],  [FacilitatorSlideNotes]
				FROM deleted
			END
		END