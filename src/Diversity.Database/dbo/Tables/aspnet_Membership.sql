﻿CREATE TABLE [dbo].[aspnet_Membership] (
    [ApplicationId]                          UNIQUEIDENTIFIER NOT NULL,
    [UserId]                                 UNIQUEIDENTIFIER NOT NULL,
    [Password]                               NVARCHAR (128)   NOT NULL,
    [PasswordFormat]                         INT              DEFAULT ((0)) NOT NULL,
    [PasswordSalt]                           NVARCHAR (128)   NOT NULL,
    [MobilePIN]                              NVARCHAR (16)    NULL,
    [Email]                                  NVARCHAR (256)   NULL,
    [LoweredEmail]                           NVARCHAR (256)   NULL,
    [PasswordQuestion]                       NVARCHAR (256)   NULL,
    [PasswordAnswer]                         NVARCHAR (128)   NULL,
    [IsApproved]                             BIT              NOT NULL,
    [IsLockedOut]                            BIT              NOT NULL,
    [CreateDate]                             DATETIME         NOT NULL,
    [LastLoginDate]                          DATETIME         NOT NULL,
    [LastPasswordChangedDate]                DATETIME         NOT NULL,
    [LastLockoutDate]                        DATETIME         NOT NULL,
    [FailedPasswordAttemptCount]             INT              NOT NULL,
    [FailedPasswordAttemptWindowStart]       DATETIME         NOT NULL,
    [FailedPasswordAnswerAttemptCount]       INT              NOT NULL,
    [FailedPasswordAnswerAttemptWindowStart] DATETIME         NOT NULL,
    [Comment]                                NVARCHAR (MAX)   NULL,
    PRIMARY KEY NONCLUSTERED ([UserId] ASC),
    FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId]),
    CONSTRAINT [FK__aspnet_Me__UserI__1D713E84] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
);





GO
EXECUTE sp_tableoption @TableNamePattern = N'[dbo].[aspnet_Membership]', @OptionName = N'text in row', @OptionValue = N'3000';


GO
CREATE CLUSTERED INDEX [aspnet_Membership_index]
    ON [dbo].[aspnet_Membership]([ApplicationId] ASC, [LoweredEmail] ASC);


GO
CREATE TRIGGER [tgAuditaspnet_Membership] 
		   ON  [dbo].[aspnet_Membership] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO Auditaspnet_Membership
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [UserId],  [Password],  [PasswordFormat],  [PasswordSalt],  [MobilePIN],  [Email],  [LoweredEmail],  [PasswordQuestion],  [PasswordAnswer],  [IsApproved],  [IsLockedOut],  [CreateDate],  [LastLoginDate],  [LastPasswordChangedDate],  [LastLockoutDate],  [FailedPasswordAttemptCount],  [FailedPasswordAttemptWindowStart],  [FailedPasswordAnswerAttemptCount],  [FailedPasswordAnswerAttemptWindowStart],  [Comment]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO Auditaspnet_Membership
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [UserId],  [Password],  [PasswordFormat],  [PasswordSalt],  [MobilePIN],  [Email],  [LoweredEmail],  [PasswordQuestion],  [PasswordAnswer],  [IsApproved],  [IsLockedOut],  [CreateDate],  [LastLoginDate],  [LastPasswordChangedDate],  [LastLockoutDate],  [FailedPasswordAttemptCount],  [FailedPasswordAttemptWindowStart],  [FailedPasswordAnswerAttemptCount],  [FailedPasswordAnswerAttemptWindowStart],  [Comment]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO Auditaspnet_Membership
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [UserId],  [Password],  [PasswordFormat],  [PasswordSalt],  [MobilePIN],  [Email],  [LoweredEmail],  [PasswordQuestion],  [PasswordAnswer],  [IsApproved],  [IsLockedOut],  [CreateDate],  [LastLoginDate],  [LastPasswordChangedDate],  [LastLockoutDate],  [FailedPasswordAttemptCount],  [FailedPasswordAttemptWindowStart],  [FailedPasswordAnswerAttemptCount],  [FailedPasswordAnswerAttemptWindowStart],  [Comment]
				FROM deleted
			END
		END