﻿CREATE TABLE [dbo].[AuditQuestionCard_GeneralAnswer_Language] (
    [ID]                                      INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                               CHAR (1)         NOT NULL,
    [RecordedAtDateTime]                      DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [QuestionCard_GeneralAnswer_LanguageID]   INT              NULL,
    [UserID_CreatedBy]                        UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]                   UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                       DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]                  DATETIME2 (7)    NULL,
    [IsADeletedRow]                           BIT              NULL,
    [IsAnAuditRow]                            BIT              NULL,
    [QuestionCard_GeneralAnswerID]            INT              NULL,
    [LanguageID]                              INT              NULL,
    [PossibleAnswer_LongVersion]              NVARCHAR (MAX)   NULL,
    [PossibleAnswer_ShortVersion]             NVARCHAR (MAX)   NULL,
    [LHStatement_LongVersion]                 NVARCHAR (MAX)   NULL,
    [RHStatement_LongVersion]                 NVARCHAR (MAX)   NULL,
    [LHStatement_ShortVersion]                NVARCHAR (MAX)   NULL,
    [RHStatement_ShortVersion]                NVARCHAR (MAX)   NULL,
    [PossibleAnswerMultimediaURL]             NVARCHAR (255)   NULL,
    [PossibleAnswerMultimediaURLFriendlyName] NVARCHAR (255)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



