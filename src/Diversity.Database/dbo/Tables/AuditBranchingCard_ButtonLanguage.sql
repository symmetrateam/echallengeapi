﻿CREATE TABLE [dbo].[AuditBranchingCard_ButtonLanguage] (
    [ID]                             INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                      CHAR (1)         NOT NULL,
    [RecordedAtDateTime]             DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [BranchingCard_ButtonLanguageID] INT              NULL,
    [UserID_CreatedBy]               UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]          UNIQUEIDENTIFIER NULL,
    [LastModifiedOnDateTime]         DATETIME2 (7)    NULL,
    [IsADeletedRow]                  BIT              NULL,
    [IsAnAuditRow]                   BIT              NULL,
    [BranchingCard_ButtonID]         INT              NULL,
    [LanguageID]                     INT              NULL,
    [ButtonText]                     NVARCHAR (50)    NULL,
    [ButtonImageMultimediaURL]       NVARCHAR (MAX)   NULL,
    [ButtonImageFriendlyName]        NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);







