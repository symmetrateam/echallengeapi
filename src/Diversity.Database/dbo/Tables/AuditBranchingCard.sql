﻿CREATE TABLE [dbo].[AuditBranchingCard] (
    [ID]                                INT           IDENTITY (1, 1) NOT NULL,
    [Operation]                         CHAR (1)      NOT NULL,
    [RecordedAtDateTime]                DATETIME2 (7) DEFAULT (getutcdate()) NOT NULL,
    [CardID]                            INT           NULL,
    [GameID]                            INT           NULL,
    [MustPressButtonsInSequence]        BIT           NULL,
    [MustAddBackgroundBorderToBranches] BIT           NULL,
    [MinButtonPresses]                  INT           NULL,
    [ButtonOrientation]                 NVARCHAR (50) NULL,
    [CardGroupID_BranchingCompleted]    INT           NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



