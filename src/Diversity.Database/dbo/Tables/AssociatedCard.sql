﻿CREATE TABLE [dbo].[AssociatedCard] (
    [AssociatedCardID]                        INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                        UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]                   UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                       DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]                  DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                           BIT              CONSTRAINT [DF_AssociatedCard_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                            BIT              CONSTRAINT [DF_AssociatedCard_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [CardID]                                  INT              NOT NULL,
    [CardID_AssociatedCard]                   INT              NOT NULL,
    [IsAssociatedToElseContextuallySimilarTo] BIT              CONSTRAINT [DF_AssociatedCard_IsAssociatedToElseContextuallySimilarTo] DEFAULT ((0)) NOT NULL,
    [IsPrimaryAssociation]                    BIT              CONSTRAINT [DF_AssociatedCard_IsPrimaryAssociation] DEFAULT ((0)) NOT NULL,
    [ExplanationForAssociation]               NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_AssociatedCard] PRIMARY KEY CLUSTERED ([AssociatedCardID] ASC),
    CONSTRAINT [FK_AssociatedCard_Card] FOREIGN KEY ([CardID_AssociatedCard]) REFERENCES [dbo].[Card] ([CardID]) ON DELETE CASCADE
);






GO
CREATE TRIGGER [tgAuditAssociatedCard] 
		   ON  [dbo].[AssociatedCard] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditAssociatedCard
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [AssociatedCardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [CardID_AssociatedCard],  [IsAssociatedToElseContextuallySimilarTo],  [IsPrimaryAssociation],  [ExplanationForAssociation]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditAssociatedCard
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [AssociatedCardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [CardID_AssociatedCard],  [IsAssociatedToElseContextuallySimilarTo],  [IsPrimaryAssociation],  [ExplanationForAssociation]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditAssociatedCard
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [AssociatedCardID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardID],  [CardID_AssociatedCard],  [IsAssociatedToElseContextuallySimilarTo],  [IsPrimaryAssociation],  [ExplanationForAssociation]
				FROM deleted
			END
		END