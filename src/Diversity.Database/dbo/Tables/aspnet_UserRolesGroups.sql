﻿CREATE TABLE [dbo].[aspnet_UserRolesGroups] (
    [ApplicationId]             UNIQUEIDENTIFIER NOT NULL,
    [UserRolesGroupId]          UNIQUEIDENTIFIER NOT NULL,
    [UserRolesGroupName]        NVARCHAR (256)   NOT NULL,
    [LoweredUserRolesGroupName] NVARCHAR (256)   NOT NULL,
    CONSTRAINT [PK_aspnet_UserRolesGroups] PRIMARY KEY CLUSTERED ([UserRolesGroupId] ASC),
    CONSTRAINT [FK_aspnet_UserRolesGroups_aspnet_Applications] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
);

