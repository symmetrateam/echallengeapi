﻿CREATE TABLE [dbo].[ClientUserGroup] (
    [ClientUserGroupID]          INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]      UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]          DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]     DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]              BIT              CONSTRAINT [DF_ClientUserGroup_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]               BIT              CONSTRAINT [DF_ClientUserGroup_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [ClientUserGroupName]        NVARCHAR (255)   NOT NULL,
    [ClientID]                   INT              NOT NULL,
    [ClientUserGroupDescription] NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_UserList] PRIMARY KEY CLUSTERED ([ClientUserGroupID] ASC),
    CONSTRAINT [FK_UserList_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
);






GO
CREATE TRIGGER [tgAuditClientUserGroup] 
		   ON  [dbo].[ClientUserGroup] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditClientUserGroup
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ClientUserGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ClientUserGroupName],  [ClientID],  [ClientUserGroupDescription]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditClientUserGroup
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ClientUserGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ClientUserGroupName],  [ClientID],  [ClientUserGroupDescription]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditClientUserGroup
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ClientUserGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ClientUserGroupName],  [ClientID],  [ClientUserGroupDescription]
				FROM deleted
			END
		END