﻿CREATE TABLE [dbo].[AuditQuestionCardStatement_Language] (
    [ID]                               INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                        CHAR (1)         NOT NULL,
    [RecordedAtDateTime]               DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [QuestionCardStatement_LanguageID] INT              NULL,
    [QuestionCardID]                   INT              NULL,
    [LanguageID]                       INT              NULL,
    [UserID_CreatedBy]                 UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]            UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]           DATETIME2 (7)    NULL,
    [SequenceNo]                       INT              NULL,
    [Statement_ShortVersion]           NVARCHAR (MAX)   NULL,
    [Statement_LongVersion]            NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



