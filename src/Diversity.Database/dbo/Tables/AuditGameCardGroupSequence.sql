﻿CREATE TABLE [dbo].[AuditGameCardGroupSequence] (
    [ID]                      INT              IDENTITY (1, 1) NOT NULL,
    [Operation]               CHAR (1)         NOT NULL,
    [RecordedAtDateTime]      DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [GameCardGroupSequenceID] INT              NULL,
    [UserID_CreatedBy]        UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]   UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]       DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]  DATETIME2 (7)    NULL,
    [IsADeletedRow]           BIT              NULL,
    [IsAnAuditRow]            BIT              NULL,
    [GameID]                  INT              NULL,
    [CardGroupSequenceNo]     INT              NULL,
    [IsAMandatoryCardGroup]   BIT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



