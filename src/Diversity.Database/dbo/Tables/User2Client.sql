﻿CREATE TABLE [dbo].[User2Client] (
    [User2ClientID]         INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy] UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]     DATETIME2 (7)    NOT NULL,
    [LastModifiedDateTime]  DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]         BIT              CONSTRAINT [DF_User2Client_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]          BIT              CONSTRAINT [DF_User2Client_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [UserID]                UNIQUEIDENTIFIER NOT NULL,
    [ClientID]              INT              NOT NULL,
    CONSTRAINT [PK_User2ClientID] PRIMARY KEY CLUSTERED ([User2ClientID] ASC),
    CONSTRAINT [FK_User2Client_aspnet_Users2] FOREIGN KEY ([UserID]) REFERENCES [dbo].[aspnet_Users] ([UserId]),
    CONSTRAINT [FK_User2Client_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
);






GO
CREATE TRIGGER [tgAuditUser2Client] 
		   ON  [dbo].[User2Client] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditUser2Client
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2ClientID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [ClientID]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditUser2Client
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2ClientID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [ClientID]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditUser2Client
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2ClientID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [ClientID]
				FROM deleted
			END
		END