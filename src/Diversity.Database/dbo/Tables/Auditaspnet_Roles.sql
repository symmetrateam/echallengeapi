﻿CREATE TABLE [dbo].[Auditaspnet_Roles] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [Operation]          CHAR (1)         NOT NULL,
    [RecordedAtDateTime] DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [ApplicationId]      UNIQUEIDENTIFIER NULL,
    [RoleId]             UNIQUEIDENTIFIER NULL,
    [RoleName]           NVARCHAR (256)   NULL,
    [LoweredRoleName]    NVARCHAR (256)   NULL,
    [Description]        NVARCHAR (256)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



