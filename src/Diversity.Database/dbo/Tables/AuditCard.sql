﻿CREATE TABLE [dbo].[AuditCard] (
    [ID]                                            INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                                     CHAR (1)         NOT NULL,
    [RecordedAtDateTime]                            DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [CardID]                                        INT              NULL,
    [UserID_CreatedBy]                              UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]                         UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                             DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]                        DATETIME2 (7)    NULL,
    [IsADeletedRow]                                 BIT              NULL,
    [IsAnAuditedRow]                                BIT              NULL,
    [Internal_CardTypeID]                           INT              NULL,
    [CardID_LCDCategoryCard]                        INT              NULL,
    [ClientID_ForLibrary]                           INT              NULL,
    [ProductID]                                     INT              NULL,
    [QuestionCardID]                                INT              NULL,
    [CardShortcutCode]                              NVARCHAR (255)   NULL,
    [IsForClientlibraryElsePublicLibrary]           BIT              NULL,
    [IgnoreAnyTimersOnThisCard]                     BIT              NULL,
    [IsStillInBeta_DoNotMakeAvailableForUseInGames] BIT              NULL,
    [IsATimedCard]                                  BIT              NULL,
    [CardMaxTimeToAnswerInSeconds]                  INT              NULL,
    [IsApplicableForOnlineUse]                      BIT              NULL,
    [IsApplicableForDesktopUse]                     BIT              NULL,
    [FacilitatorSlideNotes]                         NVARCHAR (MAX)   NULL,
    [UseCardTimeNotGameTime]                        BIT              NULL,
    [UserID_RatificationRequestor]                  UNIQUEIDENTIFIER NULL,
    [RatificationRequestedOn]                       DATETIME2 (7)    NULL,
    [RatificationRequestorNotes]                    NVARCHAR (MAX)   NULL,
    [UserID_RatificationApprover]                   UNIQUEIDENTIFIER NULL,
    [RatificationApprovedOn]                        DATETIME2 (7)    NULL,
    [RatificationApproverNotes]                     NVARCHAR (MAX)   NULL,
    [IsAwaitingRatification]                        BIT              NULL,
    [IsRatified]                                    BIT              NULL,
    [IsDirtyCard_PreviouslyRatified]                BIT              NULL,
    [CardContentNextRefreshDate]                    DATETIME2 (7)    NULL,
    [DefaultLanguageID]                             INT              NULL,
    [IsForGame]                                     BIT              NULL,
    [AudioTranscriptFile]                           NVARCHAR (255)   NULL,
    [AudioTranscriptFriendlyName]                   NVARCHAR (255)   NULL,
    [AudioTranscriptText]                           NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



