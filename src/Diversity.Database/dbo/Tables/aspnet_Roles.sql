﻿CREATE TABLE [dbo].[aspnet_Roles] (
    [ApplicationId]   UNIQUEIDENTIFIER NOT NULL,
    [RoleId]          UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [RoleName]        NVARCHAR (256)   NOT NULL,
    [LoweredRoleName] NVARCHAR (256)   NOT NULL,
    [Description]     NVARCHAR (256)   NULL,
    PRIMARY KEY NONCLUSTERED ([RoleId] ASC),
    FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
);




GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1]
    ON [dbo].[aspnet_Roles]([ApplicationId] ASC, [LoweredRoleName] ASC);


GO
CREATE TRIGGER [tgAuditaspnet_Roles] 
		   ON  [dbo].[aspnet_Roles] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO Auditaspnet_Roles
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [RoleId],  [RoleName],  [LoweredRoleName],  [Description]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO Auditaspnet_Roles
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [RoleId],  [RoleName],  [LoweredRoleName],  [Description]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO Auditaspnet_Roles
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ApplicationId],  [RoleId],  [RoleName],  [LoweredRoleName],  [Description]
				FROM deleted
			END
		END