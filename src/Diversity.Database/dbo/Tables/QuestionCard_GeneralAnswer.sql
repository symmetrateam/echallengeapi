﻿CREATE TABLE [dbo].[QuestionCard_GeneralAnswer] (
    [QuestionCard_GeneralAnswerID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]             UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]            DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]       DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                 BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [QuestionCardID]               INT              NOT NULL,
    [IsCorrectAnswer]              BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsCorrectAnswer] DEFAULT ((0)) NOT NULL,
    [SequenceNo]                   INT              NULL,
    [IsEqualTo]                    BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsEqualTo] DEFAULT ((0)) NOT NULL,
    [IsBetween]                    BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsBetween] DEFAULT ((0)) NOT NULL,
    [IsGreaterThan]                BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsGreaterThan] DEFAULT ((0)) NOT NULL,
    [IsGreaterThanOrEqualTo]       BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsGreaterThanOrEqualTo] DEFAULT ((0)) NOT NULL,
    [IsLessThan]                   BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsLessThan] DEFAULT ((0)) NOT NULL,
    [IsLessThanOrEqualTo]          BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsLessThanOrEqualTo] DEFAULT ((0)) NOT NULL,
    [IsNotEqualTo]                 BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsNotEqualTo] DEFAULT ((0)) NOT NULL,
    [IsNotBetween]                 BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_IsNotBetween] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_QuestionCard_GeneralAnswer] PRIMARY KEY CLUSTERED ([QuestionCard_GeneralAnswerID] ASC),
    CONSTRAINT [FK_QuestionCard_GeneralAnswer_QuestionCard] FOREIGN KEY ([QuestionCardID]) REFERENCES [dbo].[QuestionCard] ([QuestionCardID])
);






GO
CREATE TRIGGER [tgAuditQuestionCard_GeneralAnswer] 
		   ON  [dbo].[QuestionCard_GeneralAnswer] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditQuestionCard_GeneralAnswer
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_GeneralAnswerID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCardID],  [IsCorrectAnswer],  [SequenceNo],  [IsEqualTo],  [IsBetween],  [IsGreaterThan],  [IsGreaterThanOrEqualTo],  [IsLessThan],  [IsLessThanOrEqualTo],  [IsNotEqualTo],  [IsNotBetween]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditQuestionCard_GeneralAnswer
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_GeneralAnswerID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCardID],  [IsCorrectAnswer],  [SequenceNo],  [IsEqualTo],  [IsBetween],  [IsGreaterThan],  [IsGreaterThanOrEqualTo],  [IsLessThan],  [IsLessThanOrEqualTo],  [IsNotEqualTo],  [IsNotBetween]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditQuestionCard_GeneralAnswer
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_GeneralAnswerID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCardID],  [IsCorrectAnswer],  [SequenceNo],  [IsEqualTo],  [IsBetween],  [IsGreaterThan],  [IsGreaterThanOrEqualTo],  [IsLessThan],  [IsLessThanOrEqualTo],  [IsNotEqualTo],  [IsNotBetween]
				FROM deleted
			END
		END