﻿CREATE TABLE [dbo].[AuditProduct2Distributor] (
    [ID]                        INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                 CHAR (1)         NOT NULL,
    [RecordedAtDateTime]        DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [Product2DistributorID]     INT              NULL,
    [UserID_CreatedBy]          UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]     UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]         DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]    DATETIME2 (7)    NULL,
    [IsADeletedRow]             BIT              NULL,
    [IsAnAuditRow]              BIT              NULL,
    [DistributorID]             INT              NULL,
    [ProductID]                 INT              NULL,
    [DistributorIsProductOwner] BIT              NULL,
    [QuestionMix]               NVARCHAR (50)    NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



