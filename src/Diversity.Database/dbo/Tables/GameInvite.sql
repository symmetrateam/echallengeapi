﻿CREATE TABLE [dbo].[GameInvite] (
    [GameInviteID]                   INT              IDENTITY (1, 1) NOT NULL,
    [UserID]                         UNIQUEIDENTIFIER NOT NULL,
    [GameID]                         INT              NOT NULL,
    [InviteDate]                     DATETIME2 (7)    NOT NULL,
    [LastReminderDate]               DATETIME2 (7)    NOT NULL,
    [UserID_CreatedBy]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]              DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]         DATETIME2 (7)    NOT NULL,
    [UserID_LastModifiedBy]          UNIQUEIDENTIFIER NOT NULL,
    [IsADeletedRow]                  BIT              CONSTRAINT [DF_GameInvite_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                   BIT              CONSTRAINT [DF_GameInvite_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [EmailSubject]                   NVARCHAR (255)   NULL,
    [EmailContent]                   NVARCHAR (MAX)   NULL,
    [ThemeClientUserGroupExternalID] NVARCHAR (50)    NULL,
    CONSTRAINT [PK_GameInvite] PRIMARY KEY CLUSTERED ([GameInviteID] ASC),
    CONSTRAINT [FK_GameInvite_aspnet_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[aspnet_Users] ([UserId]) ON DELETE CASCADE,
    CONSTRAINT [FK_GameInvite_Game] FOREIGN KEY ([GameID]) REFERENCES [dbo].[Game] ([GameID]) ON DELETE CASCADE,
    CONSTRAINT [FK_GameInvite_ThemeClientUserGroup] FOREIGN KEY ([ThemeClientUserGroupExternalID]) REFERENCES [dbo].[ThemeClientUserGroup] ([ExternalID])
);








GO

CREATE TRIGGER [dbo].[tgAuditGameInvite] 
				   ON  [dbo].[GameInvite] 
				   AFTER INSERT, UPDATE, DELETE
				AS 
				BEGIN
					SET NOCOUNT ON;

					IF(SELECT COUNT(*) FROM inserted) > 0
					BEGIN
		
						IF(SELECT COUNT(*) FROM deleted) > 0
						BEGIN
							--Update
							INSERT INTO AuditGameInvite
							SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameInviteID],  [UserID],  [GameID],  [InviteDate],  [LastReminderDate],  [UserID_CreatedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [UserID_LastModifiedBy],  [IsADeletedRow],  [IsAnAuditRow],  [EmailSubject],  [EmailContent], [ThemeClientUserGroupExternalID]
							FROM inserted 
						END
						ELSE
						BEGIN
							--Insert
							INSERT INTO AuditGameInvite
							SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameInviteID],  [UserID],  [GameID],  [InviteDate],  [LastReminderDate],  [UserID_CreatedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [UserID_LastModifiedBy],  [IsADeletedRow],  [IsAnAuditRow],  [EmailSubject],  [EmailContent], [ThemeClientUserGroupExternalID]
							FROM inserted 
						END
					END
					ELSE
					BEGIN
						INSERT INTO AuditGameInvite
						SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameInviteID],  [UserID],  [GameID],  [InviteDate],  [LastReminderDate],  [UserID_CreatedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [UserID_LastModifiedBy],  [IsADeletedRow],  [IsAnAuditRow],  [EmailSubject],  [EmailContent], [ThemeClientUserGroupExternalID]
						FROM deleted
					END
				END