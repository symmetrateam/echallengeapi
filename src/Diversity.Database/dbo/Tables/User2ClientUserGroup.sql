﻿CREATE TABLE [dbo].[User2ClientUserGroup] (
    [User2ClientUserGroupID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]      DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime] DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]          BIT              CONSTRAINT [DF_User2ClientUserGroup_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]           BIT              CONSTRAINT [DF_User2ClientUserGroup_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [UserID]                 UNIQUEIDENTIFIER NOT NULL,
    [ClientUserGroupID]      INT              NOT NULL,
    CONSTRAINT [PK_User2UserList] PRIMARY KEY CLUSTERED ([User2ClientUserGroupID] ASC),
    CONSTRAINT [FK_User2ClientUserGroup_aspnet_Users2] FOREIGN KEY ([UserID]) REFERENCES [dbo].[aspnet_Users] ([UserId]),
    CONSTRAINT [FK_User2UserList_UserList] FOREIGN KEY ([ClientUserGroupID]) REFERENCES [dbo].[ClientUserGroup] ([ClientUserGroupID]) ON DELETE CASCADE
);






GO
CREATE TRIGGER [tgAuditUser2ClientUserGroup] 
		   ON  [dbo].[User2ClientUserGroup] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditUser2ClientUserGroup
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2ClientUserGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [ClientUserGroupID]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditUser2ClientUserGroup
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2ClientUserGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [ClientUserGroupID]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditUser2ClientUserGroup
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2ClientUserGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [ClientUserGroupID]
				FROM deleted
			END
		END