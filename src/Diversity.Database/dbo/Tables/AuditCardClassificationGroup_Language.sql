﻿CREATE TABLE [dbo].[AuditCardClassificationGroup_Language] (
    [ID]                                 INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                          CHAR (1)         NOT NULL,
    [RecordedAtDateTime]                 DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [CardClassificationGroup_LanguageID] INT              NULL,
    [UserID_CreatedBy]                   UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]              UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                  DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]             DATETIME2 (7)    NULL,
    [IsADeletedRow]                      BIT              NULL,
    [IsAnAuditRow]                       BIT              NULL,
    [CardClassificationGroupID]          INT              NULL,
    [LanguageID]                         INT              NULL,
    [CardClassificationGroup]            NVARCHAR (100)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



