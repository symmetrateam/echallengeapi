﻿CREATE TABLE [dbo].[CardClassificationGroup] (
    [CardClassificationGroupID]        INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                 UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]           DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                    BIT              CONSTRAINT [DF_CardClassificationGroup_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                     BIT              CONSTRAINT [DF_CardClassificationGroup_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [ProductID]                        INT              NOT NULL,
    [CardClassificationGroupID_Parent] INT              NULL,
    [CardClassificationGroupImageURL]  NVARCHAR (MAX)   NOT NULL,
    [IsAMetaTagClassificationGroup]    BIT              CONSTRAINT [DF_CardClassificationGroup_IsAMetaTagClassificationGroup] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CardClassificationGroup] PRIMARY KEY CLUSTERED ([CardClassificationGroupID] ASC),
    CONSTRAINT [FK_CardClassificationGroup_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Product] ([ProductID])
);






GO
CREATE TRIGGER [tgAuditCardClassificationGroup] 
		   ON  [dbo].[CardClassificationGroup] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditCardClassificationGroup
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardClassificationGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ProductID],  [CardClassificationGroupID_Parent],  [CardClassificationGroupImageURL],  [IsAMetaTagClassificationGroup]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditCardClassificationGroup
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardClassificationGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ProductID],  [CardClassificationGroupID_Parent],  [CardClassificationGroupImageURL],  [IsAMetaTagClassificationGroup]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditCardClassificationGroup
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardClassificationGroupID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ProductID],  [CardClassificationGroupID_Parent],  [CardClassificationGroupImageURL],  [IsAMetaTagClassificationGroup]
				FROM deleted
			END
		END