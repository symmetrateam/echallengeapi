﻿CREATE TABLE [dbo].[Client2Distributor] (
    [Client2DistributorID]     INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]        DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]   DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]            BIT              CONSTRAINT [DF_Distributor2Client_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]             BIT              CONSTRAINT [DF_Distributor2Client_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [DistributorID]            INT              NOT NULL,
    [ClientID]                 INT              NOT NULL,
    [DistributorIsClientOwner] BIT              CONSTRAINT [DF_Client2Distributor_DistributorIsClientOwner] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Distributor2Client] PRIMARY KEY CLUSTERED ([Client2DistributorID] ASC),
    CONSTRAINT [FK_Distributor2Client_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID]) ON DELETE CASCADE,
    CONSTRAINT [FK_Distributor2Client_Distributor] FOREIGN KEY ([DistributorID]) REFERENCES [dbo].[Distributor] ([DistributorID])
);






GO
CREATE TRIGGER [tgAuditClient2Distributor] 
		   ON  [dbo].[Client2Distributor] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditClient2Distributor
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Client2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DistributorID],  [ClientID],  [DistributorIsClientOwner]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditClient2Distributor
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Client2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DistributorID],  [ClientID],  [DistributorIsClientOwner]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditClient2Distributor
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Client2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DistributorID],  [ClientID],  [DistributorIsClientOwner]
				FROM deleted
			END
		END