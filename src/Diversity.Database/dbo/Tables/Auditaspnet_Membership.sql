﻿CREATE TABLE [dbo].[Auditaspnet_Membership] (
    [ID]                                     INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                              CHAR (1)         NOT NULL,
    [RecordedAtDateTime]                     DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [ApplicationId]                          UNIQUEIDENTIFIER NULL,
    [UserId]                                 UNIQUEIDENTIFIER NULL,
    [Password]                               NVARCHAR (128)   NULL,
    [PasswordFormat]                         INT              NULL,
    [PasswordSalt]                           NVARCHAR (128)   NULL,
    [MobilePIN]                              NVARCHAR (16)    NULL,
    [Email]                                  NVARCHAR (256)   NULL,
    [LoweredEmail]                           NVARCHAR (256)   NULL,
    [PasswordQuestion]                       NVARCHAR (256)   NULL,
    [PasswordAnswer]                         NVARCHAR (128)   NULL,
    [IsApproved]                             BIT              NULL,
    [IsLockedOut]                            BIT              NULL,
    [CreateDate]                             DATETIME         NULL,
    [LastLoginDate]                          DATETIME         NULL,
    [LastPasswordChangedDate]                DATETIME         NULL,
    [LastLockoutDate]                        DATETIME         NULL,
    [FailedPasswordAttemptCount]             INT              NULL,
    [FailedPasswordAttemptWindowStart]       DATETIME         NULL,
    [FailedPasswordAnswerAttemptCount]       INT              NULL,
    [FailedPasswordAnswerAttemptWindowStart] DATETIME         NULL,
    [Comment]                                NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



