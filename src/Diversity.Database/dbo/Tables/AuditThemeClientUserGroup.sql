﻿CREATE TABLE [dbo].[AuditThemeClientUserGroup] (
    [ID]                 INT           IDENTITY (1, 1) NOT NULL,
    [Operation]          CHAR (1)      NOT NULL,
    [RecordedAtDateTime] DATETIME2 (7) NOT NULL,
    [ThemeID]            INT           NULL,
    [ClientUserGroupID]  INT           NULL,
    [ExternalID]         NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

