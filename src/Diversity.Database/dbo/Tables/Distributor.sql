﻿CREATE TABLE [dbo].[Distributor] (
    [DistributorID]                       INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                    UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                   DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]              DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                       BIT              CONSTRAINT [DF_Distributor_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                        BIT              CONSTRAINT [DF_Distributor_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [PhysicalAddress1]                    NVARCHAR (255)   NULL,
    [PhysicalAddress2]                    NVARCHAR (255)   NULL,
    [PhysicalPostalCode]                  NVARCHAR (255)   NULL,
    [PhysicalStateProvince]               NVARCHAR (255)   NULL,
    [PhysicalCountry]                     NVARCHAR (255)   NULL,
    [PostalAddress1]                      NVARCHAR (255)   NULL,
    [PostalAddress2]                      NVARCHAR (255)   NULL,
    [PostalPostalCode]                    NVARCHAR (255)   NULL,
    [PostalStateProvince]                 NVARCHAR (255)   NULL,
    [PostalCountry]                       NVARCHAR (255)   NULL,
    [DistributorName]                     NVARCHAR (255)   NOT NULL,
    [DistributorTel1]                     NVARCHAR (255)   NULL,
    [DistributorEmailAddress]             NVARCHAR (255)   NULL,
    [DistributorCanCreateProducts]        BIT              CONSTRAINT [DF_Distributor_DistributorCanCreateProducts] DEFAULT ((0)) NOT NULL,
    [DistributorNotes]                    NVARCHAR (MAX)   NULL,
    [MaxNoOfProductsDistributorCanCreate] INT              CONSTRAINT [DF_Distributor_MaxNoOfProductsDistributorCanCreate] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Distributor] PRIMARY KEY CLUSTERED ([DistributorID] ASC)
);






GO
CREATE TRIGGER [tgAuditDistributor] 
		   ON  [dbo].[Distributor] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditDistributor
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [PhysicalAddress1],  [PhysicalAddress2],  [PhysicalPostalCode],  [PhysicalStateProvince],  [PhysicalCountry],  [PostalAddress1],  [PostalAddress2],  [PostalPostalCode],  [PostalStateProvince],  [PostalCountry],  [DistributorName],  [DistributorTel1],  [DistributorEmailAddress],  [DistributorCanCreateProducts],  [DistributorNotes],  [MaxNoOfProductsDistributorCanCreate]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditDistributor
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [PhysicalAddress1],  [PhysicalAddress2],  [PhysicalPostalCode],  [PhysicalStateProvince],  [PhysicalCountry],  [PostalAddress1],  [PostalAddress2],  [PostalPostalCode],  [PostalStateProvince],  [PostalCountry],  [DistributorName],  [DistributorTel1],  [DistributorEmailAddress],  [DistributorCanCreateProducts],  [DistributorNotes],  [MaxNoOfProductsDistributorCanCreate]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditDistributor
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [PhysicalAddress1],  [PhysicalAddress2],  [PhysicalPostalCode],  [PhysicalStateProvince],  [PhysicalCountry],  [PostalAddress1],  [PostalAddress2],  [PostalPostalCode],  [PostalStateProvince],  [PostalCountry],  [DistributorName],  [DistributorTel1],  [DistributorEmailAddress],  [DistributorCanCreateProducts],  [DistributorNotes],  [MaxNoOfProductsDistributorCanCreate]
				FROM deleted
			END
		END