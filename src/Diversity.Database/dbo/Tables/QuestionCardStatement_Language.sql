﻿CREATE TABLE [dbo].[QuestionCardStatement_Language] (
    [QuestionCardStatement_LanguageID] INT              IDENTITY (1, 1) NOT NULL,
    [QuestionCardID]                   INT              NOT NULL,
    [LanguageID]                       INT              NOT NULL,
    [UserID_CreatedBy]                 UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]           DATETIME2 (7)    NOT NULL,
    [SequenceNo]                       INT              NOT NULL,
    [Statement_ShortVersion]           NVARCHAR (MAX)   NOT NULL,
    [Statement_LongVersion]            NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_QuestionCardStatement_Language] PRIMARY KEY CLUSTERED ([QuestionCardStatement_LanguageID] ASC),
    CONSTRAINT [FK_QuestionCardStatement_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_QuestionCardStatement_Language_QuestionCard] FOREIGN KEY ([QuestionCardID]) REFERENCES [dbo].[QuestionCard] ([QuestionCardID])
);






GO
CREATE TRIGGER [tgAuditQuestionCardStatement_Language] 
		   ON  [dbo].[QuestionCardStatement_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditQuestionCardStatement_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCardStatement_LanguageID],  [QuestionCardID],  [LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [SequenceNo],  [Statement_ShortVersion],  [Statement_LongVersion]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditQuestionCardStatement_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCardStatement_LanguageID],  [QuestionCardID],  [LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [SequenceNo],  [Statement_ShortVersion],  [Statement_LongVersion]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditQuestionCardStatement_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCardStatement_LanguageID],  [QuestionCardID],  [LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [SequenceNo],  [Statement_ShortVersion],  [Statement_LongVersion]
				FROM deleted
			END
		END