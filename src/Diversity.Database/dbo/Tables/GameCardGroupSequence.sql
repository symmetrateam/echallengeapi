﻿CREATE TABLE [dbo].[GameCardGroupSequence] (
    [GameCardGroupSequenceID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]       DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]  DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]           BIT              CONSTRAINT [DF_GameCardSequence_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]            BIT              CONSTRAINT [DF_GameCardSequence_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [GameID]                  INT              NOT NULL,
    [CardGroupSequenceNo]     INT              NOT NULL,
    [IsAMandatoryCardGroup]   BIT              CONSTRAINT [DF_GameCardSequence_IsAMandatoryQuestion] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_GameSequence] PRIMARY KEY CLUSTERED ([GameCardGroupSequenceID] ASC),
    CONSTRAINT [FK_GameSequence_Game] FOREIGN KEY ([GameID]) REFERENCES [dbo].[Game] ([GameID]) ON DELETE CASCADE
);






GO
CREATE TRIGGER [tgAuditGameCardGroupSequence] 
		   ON  [dbo].[GameCardGroupSequence] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditGameCardGroupSequence
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameCardGroupSequenceID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [GameID],  [CardGroupSequenceNo],  [IsAMandatoryCardGroup]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditGameCardGroupSequence
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameCardGroupSequenceID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [GameID],  [CardGroupSequenceNo],  [IsAMandatoryCardGroup]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditGameCardGroupSequence
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameCardGroupSequenceID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [GameID],  [CardGroupSequenceNo],  [IsAMandatoryCardGroup]
				FROM deleted
			END
		END