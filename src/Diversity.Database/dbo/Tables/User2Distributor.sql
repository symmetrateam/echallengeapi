﻿CREATE TABLE [dbo].[User2Distributor] (
    [User2DistributorID]    INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy] UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]     DATETIME2 (7)    NOT NULL,
    [LastModifiedDateTime]  DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]         BIT              CONSTRAINT [DF_User2Distributor_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]          BIT              CONSTRAINT [DF_User2Distributor_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [UserID]                UNIQUEIDENTIFIER NOT NULL,
    [DistributorID]         INT              NOT NULL,
    CONSTRAINT [PK_User2Distributor] PRIMARY KEY CLUSTERED ([User2DistributorID] ASC),
    CONSTRAINT [FK_User2Distributor_aspnet_Users2] FOREIGN KEY ([UserID]) REFERENCES [dbo].[aspnet_Users] ([UserId]),
    CONSTRAINT [FK_User2Distributor_Distributor] FOREIGN KEY ([DistributorID]) REFERENCES [dbo].[Distributor] ([DistributorID]) ON DELETE CASCADE
);






GO
CREATE TRIGGER [tgAuditUser2Distributor] 
		   ON  [dbo].[User2Distributor] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditUser2Distributor
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [DistributorID]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditUser2Distributor
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [DistributorID]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditUser2Distributor
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [User2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [UserID],  [DistributorID]
				FROM deleted
			END
		END