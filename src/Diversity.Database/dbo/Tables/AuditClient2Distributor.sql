﻿CREATE TABLE [dbo].[AuditClient2Distributor] (
    [ID]                       INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                CHAR (1)         NOT NULL,
    [RecordedAtDateTime]       DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [Client2DistributorID]     INT              NULL,
    [UserID_CreatedBy]         UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]    UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]        DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]   DATETIME2 (7)    NULL,
    [IsADeletedRow]            BIT              NULL,
    [IsAnAuditRow]             BIT              NULL,
    [DistributorID]            INT              NULL,
    [ClientID]                 INT              NULL,
    [DistributorIsClientOwner] BIT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



