﻿CREATE TABLE [dbo].[AuditFlashAsset] (
    [ID]                 INT           IDENTITY (1, 1) NOT NULL,
    [Operation]          CHAR (1)      NOT NULL,
    [RecordedAtDateTime] DATETIME2 (7) DEFAULT (getutcdate()) NOT NULL,
    [FlashAssetID]       INT           NULL,
    [Key]                NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



