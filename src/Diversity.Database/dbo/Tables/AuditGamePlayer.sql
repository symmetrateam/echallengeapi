﻿CREATE TABLE [dbo].[AuditGamePlayer] (
    [ID]                       INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                CHAR (1)         NOT NULL,
    [RecordedAtDateTime]       DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [GamePlayerID]             INT              NULL,
    [UserID_CreatedBy]         UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]    UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]        DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]   DATETIME2 (7)    NULL,
    [IsADeletedRow]            BIT              NULL,
    [IsAnAuditRow]             BIT              NULL,
    [UserID]                   UNIQUEIDENTIFIER NULL,
    [GameID]                   INT              NULL,
    [DesktopTeamIconName]      NVARCHAR (255)   NULL,
    [DesktopTeamName]          NVARCHAR (255)   NULL,
    [PlayerTurnSequenceNo]     INT              NULL,
    [NoOfPlayersIfThisIsATeam] INT              NULL,
    [IsAvatar]                 BIT              NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



