﻿CREATE TABLE [dbo].[AuditInternal_CardType] (
    [ID]                         INT            IDENTITY (1, 1) NOT NULL,
    [Operation]                  CHAR (1)       NOT NULL,
    [RecordedAtDateTime]         DATETIME2 (7)  DEFAULT (getutcdate()) NOT NULL,
    [Internal_CardTypeID]        INT            NULL,
    [Internal_CardTypeName]      NVARCHAR (255) NULL,
    [Internal_CardTypeShortCode] NVARCHAR (255) NULL,
    [Internal_CardTypeGroup]     NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



