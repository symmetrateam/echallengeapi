﻿CREATE TABLE [dbo].[Internal_CardType] (
    [Internal_CardTypeID]        INT            IDENTITY (1, 1) NOT NULL,
    [Internal_CardTypeName]      NVARCHAR (255) NOT NULL,
    [Internal_CardTypeShortCode] NVARCHAR (255) NOT NULL,
    [Internal_CardTypeGroup]     NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Internal_CardType] PRIMARY KEY CLUSTERED ([Internal_CardTypeID] ASC)
);




GO
CREATE TRIGGER [tgAuditInternal_CardType] 
		   ON  [dbo].[Internal_CardType] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditInternal_CardType
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Internal_CardTypeID],  [Internal_CardTypeName],  [Internal_CardTypeShortCode],  [Internal_CardTypeGroup]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditInternal_CardType
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Internal_CardTypeID],  [Internal_CardTypeName],  [Internal_CardTypeShortCode],  [Internal_CardTypeGroup]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditInternal_CardType
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Internal_CardTypeID],  [Internal_CardTypeName],  [Internal_CardTypeShortCode],  [Internal_CardTypeGroup]
				FROM deleted
			END
		END