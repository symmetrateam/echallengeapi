﻿CREATE TABLE [dbo].[AuditGame] (
    [ID]                                             INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                                      CHAR (1)         NOT NULL,
    [RecordedAtDateTime]                             DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [GameID]                                         INT              NULL,
    [UserID_CreatedBy]                               UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]                          UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                              DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]                         DATETIME2 (7)    NULL,
    [IsADeletedRow]                                  BIT              NULL,
    [IsAnAuditRow]                                   BIT              NULL,
    [GameID_Template]                                INT              NULL,
    [IsOnlineGame]                                   BIT              NULL,
    [IsAGameTemplate]                                BIT              NULL,
    [GameNowLockedDown]                              BIT              NULL,
    [GameConfigCanBeModifiedAcrossInstances]         BIT              NULL,
    [IsTimedGame]                                    BIT              NULL,
    [UseCardTime]                                    BIT              NULL,
    [UseActualAvatarNames]                           BIT              NULL,
    [UseHistoricAvatarAnswers]                       BIT              NULL,
    [UseActualAvatarAnsweringTimes]                  BIT              NULL,
    [AllUsersMustFinishThisGameByThisDate]           DATETIME2 (7)    NULL,
    [PlayerTurn_RandomNoRepeatingUntilAllHavePlayed] BIT              NULL,
    [PlayerTurn_ByPlayerSequenceNoElseRandom]        BIT              NULL,
    [DefaultGameTimeInSeconds]                       INT              NULL,
    [GameCompletionDate]                             DATETIME2 (7)    NULL,
    [GameStartDate]                                  DATETIME2 (7)    NULL,
    [GameThemeData]                                  NVARCHAR (MAX)   NULL,
    [CanSkipTutorial]                                BIT              NULL,
    [GameTemplateNameForDashboard]                   NVARCHAR (255)   NULL,
    [HasBeenDownloaded]                              BIT              NULL,
    [NumberOfTimesRun]                               INT              NULL,
    [ProductID]                                      INT              NULL,
    [NumberOfTimesGameCanBePlayed]                   INT              NULL,
    [ExternalID]                                     NVARCHAR (50)    NULL,
    [ThemeClientUserGroupExternalID]                 NVARCHAR (50)    NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);





