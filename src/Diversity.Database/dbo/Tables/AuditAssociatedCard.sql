﻿CREATE TABLE [dbo].[AuditAssociatedCard] (
    [ID]                                      INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                               CHAR (1)         NOT NULL,
    [RecordedAtDateTime]                      DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [AssociatedCardID]                        INT              NULL,
    [UserID_CreatedBy]                        UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]                   UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                       DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]                  DATETIME2 (7)    NULL,
    [IsADeletedRow]                           BIT              NULL,
    [IsAnAuditRow]                            BIT              NULL,
    [CardID]                                  INT              NULL,
    [CardID_AssociatedCard]                   INT              NULL,
    [IsAssociatedToElseContextuallySimilarTo] BIT              NULL,
    [IsPrimaryAssociation]                    BIT              NULL,
    [ExplanationForAssociation]               NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



