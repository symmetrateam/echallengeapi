﻿CREATE TABLE [dbo].[aspnet_UserRolesGroupsDefaults] (
    [UserRolesGroupId] UNIQUEIDENTIFIER NOT NULL,
    [RoleId]           UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_aspnet_UserRolesGroupsDefaults] PRIMARY KEY CLUSTERED ([UserRolesGroupId] ASC, [RoleId] ASC),
    CONSTRAINT [FK_aspnet_UserRolesGroupsDefaults_aspnet_UserRolesGroups] FOREIGN KEY ([UserRolesGroupId]) REFERENCES [dbo].[aspnet_UserRolesGroups] ([UserRolesGroupId])
);

