﻿CREATE TABLE [dbo].[AuditErrorMessage_Language] (
    [ID]                      INT            IDENTITY (1, 1) NOT NULL,
    [Operation]               CHAR (1)       NOT NULL,
    [RecordedAtDateTime]      DATETIME2 (7)  DEFAULT (getutcdate()) NOT NULL,
    [ErrorMessage_LanguageID] INT            NULL,
    [ErrorMessageID]          INT            NULL,
    [LanguageID]              INT            NULL,
    [ErrorMessageDescription] NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



