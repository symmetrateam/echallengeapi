﻿CREATE TABLE [dbo].[GameCardSequence] (
    [GameCardSequenceID]      INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]       DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]  DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]           BIT              CONSTRAINT [DF_GameCardSequence_IsADeletedRow_1] DEFAULT ((0)) NOT NULL,
    [IsAnAuditedRow]          BIT              CONSTRAINT [DF_GameCardSequence_IsAnAuditedRow] DEFAULT ((0)) NOT NULL,
    [GameCardGroupSequenceID] INT              NOT NULL,
    [CardID]                  INT              NOT NULL,
    [CardSequenceNo]          INT              NOT NULL,
    [UseLongVersion]          BIT              CONSTRAINT [DF_GameCardSequence_UseLongVersion_1] DEFAULT ((0)) NOT NULL,
    [IsAMandatoryCard]        BIT              CONSTRAINT [DF_GameCardSequence_IsAMandatoryCard] DEFAULT ((0)) NOT NULL,
    [CardViewedInGame]        BIT              CONSTRAINT [DF_GameCardSequence_CardViewedInGame] DEFAULT ((0)) NOT NULL,
    [CardViewedInGameOn]      DATETIME2 (7)    NULL,
    CONSTRAINT [PK_GameCardSequence] PRIMARY KEY CLUSTERED ([GameCardSequenceID] ASC),
    CONSTRAINT [FK_GameCardSequence_Card] FOREIGN KEY ([CardID]) REFERENCES [dbo].[Card] ([CardID]) ON DELETE CASCADE,
    CONSTRAINT [FK_GameCardSequence_GameCardGroupSequence] FOREIGN KEY ([GameCardGroupSequenceID]) REFERENCES [dbo].[GameCardGroupSequence] ([GameCardGroupSequenceID]) ON DELETE CASCADE
);






GO
CREATE TRIGGER [tgAuditGameCardSequence] 
		   ON  [dbo].[GameCardSequence] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditGameCardSequence
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameCardSequenceID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditedRow],  [GameCardGroupSequenceID],  [CardID],  [CardSequenceNo],  [UseLongVersion],  [IsAMandatoryCard],  [CardViewedInGame],  [CardViewedInGameOn]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditGameCardSequence
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameCardSequenceID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditedRow],  [GameCardGroupSequenceID],  [CardID],  [CardSequenceNo],  [UseLongVersion],  [IsAMandatoryCard],  [CardViewedInGame],  [CardViewedInGameOn]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditGameCardSequence
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [GameCardSequenceID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditedRow],  [GameCardGroupSequenceID],  [CardID],  [CardSequenceNo],  [UseLongVersion],  [IsAMandatoryCard],  [CardViewedInGame],  [CardViewedInGameOn]
				FROM deleted
			END
		END