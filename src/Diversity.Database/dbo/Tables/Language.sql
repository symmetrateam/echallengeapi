﻿CREATE TABLE [dbo].[Language] (
    [LanguageID]    INT            IDENTITY (1, 1) NOT NULL,
    [Language]      NVARCHAR (MAX) NOT NULL,
    [IsLeftToRight] BIT            CONSTRAINT [DF_Language_IsLeftToRight] DEFAULT ((1)) NOT NULL,
    [LCID]          NVARCHAR (10)  NOT NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([LanguageID] ASC)
);




GO
CREATE TRIGGER [tgAuditLanguage] 
		   ON  [dbo].[Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditLanguage
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [LanguageID],  [Language],  [IsLeftToRight],  [LCID]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditLanguage
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [LanguageID],  [Language],  [IsLeftToRight],  [LCID]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditLanguage
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [LanguageID],  [Language],  [IsLeftToRight],  [LCID]
				FROM deleted
			END
		END