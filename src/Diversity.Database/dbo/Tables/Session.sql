﻿CREATE TABLE [dbo].[Session] (
    [UserID]              UNIQUEIDENTIFIER NOT NULL,
    [ExpiresAt]           DATETIME2 (7)    NOT NULL,
    [AuthenticationToken] NVARCHAR (255)   NOT NULL,
    CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED ([UserID] ASC),
    CONSTRAINT [FK_Session_aspnet_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[aspnet_Users] ([UserId])
);




GO
CREATE TRIGGER [tgAuditSession] 
		   ON  [dbo].[Session] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditSession
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [UserID],  [ExpiresAt],  [AuthenticationToken]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditSession
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [UserID],  [ExpiresAt],  [AuthenticationToken]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditSession
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [UserID],  [ExpiresAt],  [AuthenticationToken]
				FROM deleted
			END
		END