﻿CREATE TABLE [dbo].[ThemeClientUserGroup] (
    [ThemeID]           INT           NOT NULL,
    [ClientUserGroupID] INT           NOT NULL,
    [ExternalID]        NVARCHAR (50) CONSTRAINT [DF_ThemeClientUserGroup_ExternalID] DEFAULT (replace(newid(),'-','')) NOT NULL,
    CONSTRAINT [PK_ThemeClientUserGroup] PRIMARY KEY CLUSTERED ([ThemeID] ASC, [ClientUserGroupID] ASC),
    CONSTRAINT [FK_ThemeClientUserGroup_ClientUserGroup] FOREIGN KEY ([ClientUserGroupID]) REFERENCES [dbo].[ClientUserGroup] ([ClientUserGroupID]),
    CONSTRAINT [FK_ThemeClientUserGroup_Theme] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Theme] ([ThemeID]),
    CONSTRAINT [UX_ThemeClientUserGroup_ExternalID] UNIQUE NONCLUSTERED ([ExternalID] ASC)
);








GO
CREATE TRIGGER [tgAuditThemeClientUserGroup] 
				   ON  [dbo].[ThemeClientUserGroup] 
				   AFTER INSERT, UPDATE, DELETE
				AS 
				BEGIN
					SET NOCOUNT ON;

					IF(SELECT COUNT(*) FROM inserted) > 0
					BEGIN
		
						IF(SELECT COUNT(*) FROM deleted) > 0
						BEGIN
							--Update
							INSERT INTO AuditThemeClientUserGroup
							SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ThemeID],  [ClientUserGroupID],  [ExternalID]
							FROM inserted 
						END
						ELSE
						BEGIN
							--Insert
							INSERT INTO AuditThemeClientUserGroup
							SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ThemeID],  [ClientUserGroupID],  [ExternalID]
							FROM inserted 
						END
					END
					ELSE
					BEGIN
						INSERT INTO AuditThemeClientUserGroup
						SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ThemeID],  [ClientUserGroupID],  [ExternalID]
						FROM deleted
					END
				END