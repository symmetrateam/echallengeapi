﻿CREATE TABLE [dbo].[AuditDistributorClient2DistributorProduct] (
    [ID]                                     INT              IDENTITY (1, 1) NOT NULL,
    [Operation]                              CHAR (1)         NOT NULL,
    [RecordedAtDateTime]                     DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [DistributorClient2DistributorProductID] INT              NULL,
    [UserID_CreatedBy]                       UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]                  UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]                      DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]                 DATETIME2 (7)    NULL,
    [IsADeletedRow]                          BIT              NULL,
    [IsAnAuditRow]                           BIT              NULL,
    [Client2DistributorID]                   INT              NULL,
    [Distributor2ProductID]                  INT              NULL,
    [QuestionMix]                            NVARCHAR (50)    NULL,
    [OnlyShowClientCustomAvatars]            BIT              NULL,
    [ClientSpecificProductTheme]             NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



