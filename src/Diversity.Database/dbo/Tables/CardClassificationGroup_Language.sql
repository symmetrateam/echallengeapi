﻿CREATE TABLE [dbo].[CardClassificationGroup_Language] (
    [CardClassificationGroup_LanguageID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                   UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                  DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]             DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                      BIT              CONSTRAINT [DF_CardClassificationGroup_Language_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                       BIT              CONSTRAINT [DF_CardClassificationGroup_Language_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [CardClassificationGroupID]          INT              NOT NULL,
    [LanguageID]                         INT              NOT NULL,
    [CardClassificationGroup]            NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_CardClassificationGroup_Language] PRIMARY KEY CLUSTERED ([CardClassificationGroup_LanguageID] ASC),
    CONSTRAINT [FK_CardClassificationGroup_Language_CardClassificationGroup] FOREIGN KEY ([CardClassificationGroupID]) REFERENCES [dbo].[CardClassificationGroup] ([CardClassificationGroupID]),
    CONSTRAINT [FK_CardClassificationGroup_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);






GO
CREATE TRIGGER [tgAuditCardClassificationGroup_Language] 
		   ON  [dbo].[CardClassificationGroup_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditCardClassificationGroup_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardClassificationGroup_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardClassificationGroupID],  [LanguageID],  [CardClassificationGroup]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditCardClassificationGroup_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardClassificationGroup_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardClassificationGroupID],  [LanguageID],  [CardClassificationGroup]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditCardClassificationGroup_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [CardClassificationGroup_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [CardClassificationGroupID],  [LanguageID],  [CardClassificationGroup]
				FROM deleted
			END
		END