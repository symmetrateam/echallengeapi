﻿CREATE TABLE [dbo].[QuestionCard_GeneralAnswer_Language] (
    [QuestionCard_GeneralAnswer_LanguageID]   INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]                        UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]                   UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]                       DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]                  DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]                           BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_Language_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                            BIT              CONSTRAINT [DF_QuestionCard_GeneralAnswer_Language_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [QuestionCard_GeneralAnswerID]            INT              NOT NULL,
    [LanguageID]                              INT              NOT NULL,
    [PossibleAnswer_LongVersion]              NVARCHAR (MAX)   NULL,
    [PossibleAnswer_ShortVersion]             NVARCHAR (MAX)   NULL,
    [LHStatement_LongVersion]                 NVARCHAR (MAX)   NULL,
    [RHStatement_LongVersion]                 NVARCHAR (MAX)   NULL,
    [LHStatement_ShortVersion]                NVARCHAR (MAX)   NULL,
    [RHStatement_ShortVersion]                NVARCHAR (MAX)   NULL,
    [PossibleAnswerMultimediaURL]             NVARCHAR (255)   NULL,
    [PossibleAnswerMultimediaURLFriendlyName] NVARCHAR (255)   NULL,
    CONSTRAINT [PK_QuestionCard_GeneralAnswer_Language] PRIMARY KEY CLUSTERED ([QuestionCard_GeneralAnswer_LanguageID] ASC),
    CONSTRAINT [FK_QuestionCard_GeneralAnswer_Language_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_QuestionCard_GeneralAnswer_Language_QuestionCard_GeneralAnswer] FOREIGN KEY ([QuestionCard_GeneralAnswerID]) REFERENCES [dbo].[QuestionCard_GeneralAnswer] ([QuestionCard_GeneralAnswerID])
);






GO
CREATE TRIGGER [tgAuditQuestionCard_GeneralAnswer_Language] 
		   ON  [dbo].[QuestionCard_GeneralAnswer_Language] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditQuestionCard_GeneralAnswer_Language
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_GeneralAnswer_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCard_GeneralAnswerID],  [LanguageID],  [PossibleAnswer_LongVersion],  [PossibleAnswer_ShortVersion],  [LHStatement_LongVersion],  [RHStatement_LongVersion],  [LHStatement_ShortVersion],  [RHStatement_ShortVersion],  [PossibleAnswerMultimediaURL],  [PossibleAnswerMultimediaURLFriendlyName]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditQuestionCard_GeneralAnswer_Language
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_GeneralAnswer_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCard_GeneralAnswerID],  [LanguageID],  [PossibleAnswer_LongVersion],  [PossibleAnswer_ShortVersion],  [LHStatement_LongVersion],  [RHStatement_LongVersion],  [LHStatement_ShortVersion],  [RHStatement_ShortVersion],  [PossibleAnswerMultimediaURL],  [PossibleAnswerMultimediaURLFriendlyName]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditQuestionCard_GeneralAnswer_Language
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [QuestionCard_GeneralAnswer_LanguageID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [QuestionCard_GeneralAnswerID],  [LanguageID],  [PossibleAnswer_LongVersion],  [PossibleAnswer_ShortVersion],  [LHStatement_LongVersion],  [RHStatement_LongVersion],  [LHStatement_ShortVersion],  [RHStatement_ShortVersion],  [PossibleAnswerMultimediaURL],  [PossibleAnswerMultimediaURLFriendlyName]
				FROM deleted
			END
		END