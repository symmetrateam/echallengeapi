﻿CREATE TABLE [dbo].[Product2Distributor] (
    [Product2DistributorID]     INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]          UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]         DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]    DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]             BIT              CONSTRAINT [DF_Distributor2Product_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]              BIT              CONSTRAINT [DF_Distributor2Product_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [DistributorID]             INT              NOT NULL,
    [ProductID]                 INT              NOT NULL,
    [DistributorIsProductOwner] BIT              CONSTRAINT [DF_Product2Distributor_DistributorIsProductOwner] DEFAULT ((0)) NOT NULL,
    [QuestionMix]               NVARCHAR (50)    CONSTRAINT [DF_Distributor2Product_QuestionMix_RatifiedOnly] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Distributor2Product] PRIMARY KEY CLUSTERED ([Product2DistributorID] ASC),
    CONSTRAINT [FK_Distributor2Product_Distributor] FOREIGN KEY ([DistributorID]) REFERENCES [dbo].[Distributor] ([DistributorID]),
    CONSTRAINT [FK_Distributor2Product_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Product] ([ProductID]) ON DELETE CASCADE
);






GO
CREATE TRIGGER [tgAuditProduct2Distributor] 
		   ON  [dbo].[Product2Distributor] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO AuditProduct2Distributor
					SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Product2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DistributorID],  [ProductID],  [DistributorIsProductOwner],  [QuestionMix]
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO AuditProduct2Distributor
					SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Product2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DistributorID],  [ProductID],  [DistributorIsProductOwner],  [QuestionMix]
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO AuditProduct2Distributor
				SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [Product2DistributorID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [DistributorID],  [ProductID],  [DistributorIsProductOwner],  [QuestionMix]
				FROM deleted
			END
		END