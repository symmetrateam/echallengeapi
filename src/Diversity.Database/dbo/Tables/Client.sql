﻿CREATE TABLE [dbo].[Client] (
    [ClientID]                    INT              IDENTITY (1, 1) NOT NULL,
    [UserID_CreatedBy]            UNIQUEIDENTIFIER NOT NULL,
    [UserID_LastModifiedBy]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnDateTime]           DATETIME2 (7)    NOT NULL,
    [LastModifiedOnDateTime]      DATETIME2 (7)    NOT NULL,
    [IsADeletedRow]               BIT              CONSTRAINT [DF_Client_IsADeletedRow] DEFAULT ((0)) NOT NULL,
    [IsAnAuditRow]                BIT              CONSTRAINT [DF_Client_IsAnAuditRow] DEFAULT ((0)) NOT NULL,
    [ClientName]                  NVARCHAR (255)   NOT NULL,
    [PhysicalAddress1]            NVARCHAR (255)   NULL,
    [PhysicalAddress2]            NVARCHAR (255)   NULL,
    [PhysicalPostalCode]          NVARCHAR (255)   NULL,
    [PhysicalStateProvince]       NVARCHAR (255)   NULL,
    [PhysicalCountry]             NVARCHAR (255)   NULL,
    [PostalAddress1]              NVARCHAR (255)   NULL,
    [PostalAddress2]              NVARCHAR (255)   NULL,
    [PostalPostalCode]            NVARCHAR (255)   NULL,
    [PostalStateProvince]         NVARCHAR (255)   NULL,
    [PostalCountry]               NVARCHAR (255)   NULL,
    [ClientTel1]                  NVARCHAR (255)   NULL,
    [ClientTel2]                  NVARCHAR (255)   NULL,
    [ClientWebsite]               NVARCHAR (255)   NULL,
    [ClientNotes]                 NVARCHAR (MAX)   NULL,
    [ClientIndustrySector]        NVARCHAR (255)   NULL,
    [ClientNoOfEmployees]         INT              NULL,
    [ClientAnnualTurnover]        MONEY            NULL,
    [ClientLogoImageURL]          NVARCHAR (MAX)   NULL,
    [ClientLogoImageFriendlyName] NVARCHAR (MAX)   NULL,
    [ExternalID]                  NVARCHAR (50)    NOT NULL,
    [ThemeID]                     INT              NULL,
    CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED ([ClientID] ASC),
    CONSTRAINT [FK_Client_Theme] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Theme] ([ThemeID])
);












GO
CREATE TRIGGER [dbo].[tgAuditClient] 
				   ON  [dbo].[Client] 
				   AFTER INSERT, UPDATE, DELETE
				AS 
				BEGIN
					SET NOCOUNT ON;

					IF(SELECT COUNT(*) FROM inserted) > 0
					BEGIN
		
						IF(SELECT COUNT(*) FROM deleted) > 0
						BEGIN
							--Update
							INSERT INTO AuditClient
							SELECT 'U' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ClientID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ClientName],  [PhysicalAddress1],  [PhysicalAddress2],  [PhysicalPostalCode],  [PhysicalStateProvince],  [PhysicalCountry],  [PostalAddress1],  [PostalAddress2],  [PostalPostalCode],  [PostalStateProvince],  [PostalCountry],  [ClientTel1],  [ClientTel2],  [ClientWebsite],  [ClientNotes],  [ClientIndustrySector],  [ClientNoOfEmployees],  [ClientAnnualTurnover],  [ClientLogoImageURL],  [ClientLogoImageFriendlyName],  [ExternalID], [ThemeID]
							FROM inserted 
						END
						ELSE
						BEGIN
							--Insert
							INSERT INTO AuditClient
							SELECT 'I' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ClientID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ClientName],  [PhysicalAddress1],  [PhysicalAddress2],  [PhysicalPostalCode],  [PhysicalStateProvince],  [PhysicalCountry],  [PostalAddress1],  [PostalAddress2],  [PostalPostalCode],  [PostalStateProvince],  [PostalCountry],  [ClientTel1],  [ClientTel2],  [ClientWebsite],  [ClientNotes],  [ClientIndustrySector],  [ClientNoOfEmployees],  [ClientAnnualTurnover],  [ClientLogoImageURL],  [ClientLogoImageFriendlyName],  [ExternalID], [ThemeID]
							FROM inserted 
						END
					END
					ELSE
					BEGIN
						INSERT INTO AuditClient
						SELECT 'D' AS Operation, GETUTCDATE() AS RecordedAtDateTime,   [ClientID],  [UserID_CreatedBy],  [UserID_LastModifiedBy],  [CreatedOnDateTime],  [LastModifiedOnDateTime],  [IsADeletedRow],  [IsAnAuditRow],  [ClientName],  [PhysicalAddress1],  [PhysicalAddress2],  [PhysicalPostalCode],  [PhysicalStateProvince],  [PhysicalCountry],  [PostalAddress1],  [PostalAddress2],  [PostalPostalCode],  [PostalStateProvince],  [PostalCountry],  [ClientTel1],  [ClientTel2],  [ClientWebsite],  [ClientNotes],  [ClientIndustrySector],  [ClientNoOfEmployees],  [ClientAnnualTurnover],  [ClientLogoImageURL],  [ClientLogoImageFriendlyName],  [ExternalID], [ThemeID]
						FROM deleted
					END
				END