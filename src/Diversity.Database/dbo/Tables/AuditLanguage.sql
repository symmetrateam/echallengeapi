﻿CREATE TABLE [dbo].[AuditLanguage] (
    [ID]                 INT            IDENTITY (1, 1) NOT NULL,
    [Operation]          CHAR (1)       NOT NULL,
    [RecordedAtDateTime] DATETIME2 (7)  DEFAULT (getutcdate()) NOT NULL,
    [LanguageID]         INT            NULL,
    [Language]           NVARCHAR (MAX) NULL,
    [IsLeftToRight]      BIT            NULL,
    [LCID]               NVARCHAR (10)  NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



