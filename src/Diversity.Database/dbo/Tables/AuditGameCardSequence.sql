﻿CREATE TABLE [dbo].[AuditGameCardSequence] (
    [ID]                      INT              IDENTITY (1, 1) NOT NULL,
    [Operation]               CHAR (1)         NOT NULL,
    [RecordedAtDateTime]      DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [GameCardSequenceID]      INT              NULL,
    [UserID_CreatedBy]        UNIQUEIDENTIFIER NULL,
    [UserID_LastModifiedBy]   UNIQUEIDENTIFIER NULL,
    [CreatedOnDateTime]       DATETIME2 (7)    NULL,
    [LastModifiedOnDateTime]  DATETIME2 (7)    NULL,
    [IsADeletedRow]           BIT              NULL,
    [IsAnAuditedRow]          BIT              NULL,
    [GameCardGroupSequenceID] INT              NULL,
    [CardID]                  INT              NULL,
    [CardSequenceNo]          INT              NULL,
    [UseLongVersion]          BIT              NULL,
    [IsAMandatoryCard]        BIT              NULL,
    [CardViewedInGame]        BIT              NULL,
    [CardViewedInGameOn]      DATETIME2 (7)    NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



