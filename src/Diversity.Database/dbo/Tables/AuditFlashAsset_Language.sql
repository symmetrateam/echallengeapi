﻿CREATE TABLE [dbo].[AuditFlashAsset_Language] (
    [ID]                    INT            IDENTITY (1, 1) NOT NULL,
    [Operation]             CHAR (1)       NOT NULL,
    [RecordedAtDateTime]    DATETIME2 (7)  DEFAULT (getutcdate()) NOT NULL,
    [FlashAsset_LanguageID] INT            NULL,
    [FlashAssetID]          INT            NULL,
    [LanguageID]            INT            NULL,
    [Value]                 NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CHECK ([Operation]='D' OR [Operation]='U' OR [Operation]='I')
);



