﻿/*
	This script recreates the audit tables to ensure ID, Operation and RecordedAtDateTime appear 
	as the first columns in the Audit tables. Any existing data is preserved. All the audit triggers
	are recreated to ensure that they insert correctly by incorporating the three aforementioned columns
	at the start of the insert. This now allows for columns to be added at the end of the tables using SQL
	without affecting the audit triggers.
*/

/***************************************
Change the database name BELOW
****************************************/

DECLARE @tableCatalog nvarchar(50)
SET @tableCatalog = 'Echallenge'



DECLARE @tableExclusions nvarchar(max)
SET @tableExclusions = 'sysdiagrams,aspnet_InvalidCredentialsLog,aspnet_Applications,aspnet_Paths,aspnet_PersonalizationAllUsers,aspnet_PersonalizationPerUser,aspnet_Profile,aspnet_SchemaVersions,aspnet_UserRolesGroups,aspnet_UserRolesGroupsDefaults,aspnet_WebEvent_Events,__RefactorLog'


DECLARE @tableName nvarchar(max)
DECLARE @schema nvarchar(max)

DECLARE TableCursor CURSOR FOR
SELECT TABLE_SCHEMA, TABLE_NAME 
FROM information_schema.tables 	
WHERE TABLE_CATALOG = @tableCatalog
	AND TABLE_TYPE = 'BASE TABLE'
	AND TABLE_NAME NOT IN (SELECT Item FROM dbo.fnDelimitedSplit8K(@tableExclusions, ','))
	AND TABLE_NAME NOT LIKE 'Audit%'
	AND TABLE_NAME NOT LIKE 'TEMP_Audit%'
ORDER BY TABLE_NAME

OPEN TableCursor

FETCH NEXT FROM TableCursor
INTO @schema, @tableName

WHILE @@FETCH_STATUS = 0
BEGIN

		DECLARE @done bit 
		SET @done = 0 

		DECLARE @crlf nvarchar(max)
		SET @crlf = char(10)
		DECLARE @sql nvarchar(max)
		DECLARE @tableColumns nvarchar(max) = N' '


		SET @sql = 'IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[' + @schema + '].[TEMP_Audit'+ @tableName +']'') AND type in (N''U''))
		DROP TABLE [dbo].[TEMP_Audit' + @tableName +']' + @crlf 

		SET @sql = @sql + 'CREATE TABLE [' + @schema + '].[TEMP_Audit' + @tableName + ']' + @crlf
		SET @sql = @sql + '(' + @crlf		
		SET @sql = @sql + ' [ID] int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED, ' + @crlf
		SET @sql = @sql + ' [Operation] char(1) NOT NULL CHECK([Operation] IN (''I'',''U'',''D'')),' + @crlf
		SET @sql = @sql + ' [RecordedAtDateTime] datetime2(7) DEFAULT(GETUTCDATE()) NOT NULL,' + @crlf

		DECLARE @columnID int
		SET @columnID = 0
		DECLARE @columnName nvarchar(max)
		DECLARE @columnType nvarchar(max)
		DECLARE @columnSize nvarchar(max)

		WHILE @done=0   
		BEGIN            
			SELECT TOP 1
			@columnID=clmns.column_id,
			@columnName=clmns.name ,
			@columnType=usrt.name ,
			@columnSize= 
							CASE 
								WHEN baset.name IN (N'nchar', N'nvarchar') AND clmns.max_length <> -1 
									THEN CONVERT(nvarchar(max), clmns.max_length/2)
								WHEN baset.name IN (N'nchar', N'nvarchar', N'varbinary') AND clmns.max_length = -1 
									THEN 'max'
								WHEN baset.name IN (N'datetime2')
									THEN '7'
								ELSE CONVERT(nvarchar(max), clmns.max_length)
							END
			FROM
			sys.tables AS tbl
			INNER JOIN sys.all_columns AS clmns 
				ON clmns.object_id=tbl.object_id
			LEFT OUTER JOIN sys.types AS usrt 
				ON usrt.user_type_id = clmns.user_type_id
			LEFT OUTER JOIN sys.types AS baset 
				ON baset.user_type_id = clmns.system_type_id and 
					baset.user_type_id = baset.system_type_id
			WHERE
			(tbl.name=@tableName and SCHEMA_NAME(tbl.schema_id)=@schema)
			and clmns.column_id > @columnID
			ORDER BY
			clmns.column_id ASC

			IF @@ROWCOUNT=0  
			BEGIN   
				SET @done=1  
			END
			ELSE
			BEGIN
				SET @tableColumns = @tableColumns + ' [' + @columnName + '], '

				SET @sql= @sql + ' [' + @columnName + '] '
				
				IF (@columnType IN ('timestamp')) 
				BEGIN	
					SET @sql = @sql + 'varbinary(8) '
				END
				ELSE
				BEGIN
					SET @sql = @sql + '[' + @columnType +'] '
				END
				
				IF (@columnType IN ('nchar', 'nvarchar', 'char', 'varchar', 'datetime2', 'varbinary')) 
				BEGIN	
					SET @sql= @sql + '('+ rtrim(ltrim(@columnSize)) + ')'
				END
				
				SET @sql = @sql +' NULL,' + @crlf
			END
		END

		SET @sql = SUBSTRING(@sql, 1, LEN(@sql) - 2)
		
		SET @sql = @sql + ')' + @crlf
		
		PRINT @sql
		PRINT ' ' 
		EXEC (@sql)
		
		SET @sql = N'SELECT Operation AS Operation_NEW, RecordedAtDateTime AS RecordedAtDateTime_NEW, * INTO #TempTable FROM ' + N'[' + @schema + '].[Audit'+ @tableName +']   ' + @crlf
		SET @sql = @sql + N'ALTER TABLE #TempTable DROP COLUMN Operation, RecordedAtDateTime, ID   ' + @crlf
		SET @sql = @sql + N'EXEC TEMPDB..SP_RENAME ''TEMPDB.DBO.#TempTable.Operation_NEW'', ''Operation'', ''COLUMN''' + @crlf
		SET @sql = @sql + N'EXEC TEMPDB..SP_RENAME ''TEMPDB.DBO.#TempTable.RecordedAtDateTime_NEW'', ''RecordedAtDateTime'', ''COLUMN''' + @crlf
		SET @sql = @sql + N'INSERT INTO ' + N'[' + @schema + '].[TEMP_Audit'+ @tableName +']  ' + @crlf		
		SET @sql = @sql + N'SELECT * FROM #TempTable ' + @crlf
		SET @sql = @sql + N'DROP TABLE #TempTable  ' + @crlf
		SET @sql = @sql + N'DROP TABLE ' + N'[' + @schema + '].[Audit'+ @tableName +']  ' + @crlf

		PRINT @sql
		PRINT ' ' 
		EXEC (@sql)

		SET @sql = N'DBCC CHECKIDENT (''TEMP_Audit'+ @tableName + ''')' + @crlf		
		
		PRINT @sql
		PRINT ' ' 
		EXEC (@sql)

		SET @sql = N'EXEC SP_RENAME ''' + N'TEMP_Audit' + @tableName  + N''',''Audit'+ @tableName + '''' + @crlf		
		
		PRINT @sql
		PRINT ' ' 
		EXEC (@sql)

		
		SET @sql = 'IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N''[dbo].[tgAudit' + @tableName+']''))
		DROP TRIGGER [dbo].[tgAudit' + @tableName + ']'
		PRINT @sql

		EXEC(@sql)

		SET @tableColumns = SUBSTRING(@tableColumns, 1, LEN(@tableColumns) -1)
		PRINT @tableColumns

		IF EXISTS(select clmns.name
			from
			sys.tables AS tbl
			INNER JOIN sys.all_columns AS clmns 
				ON clmns.object_id=tbl.object_id
			LEFT OUTER JOIN sys.types AS usrt 
				ON usrt.user_type_id = clmns.user_type_id
			LEFT OUTER JOIN sys.types AS baset 
				ON baset.user_type_id = clmns.system_type_id and 
					baset.user_type_id = baset.system_type_id
			WHERE
			(tbl.name=@tableName and SCHEMA_NAME(tbl.schema_id)=@schema)
			and clmns.name = 'LastModifiedBy_UserID' )
		BEGIN
			SET @sql='CREATE TRIGGER [tgAudit'+ @tableName + '] 
			   ON  [' + @schema + '].['+ @tableName + '] 
			   AFTER INSERT, UPDATE, DELETE
			AS 
			BEGIN
				SET NOCOUNT ON;

				IF(SELECT COUNT(*) FROM inserted) > 0
				BEGIN
		
					IF(SELECT COUNT(*) FROM deleted) > 0
					BEGIN
						--Update
						INSERT INTO Audit' + @tableName + '
						SELECT ''U'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
						FROM inserted 
					END
					ELSE
					BEGIN
						--Insert
						INSERT INTO Audit' + @tableName + '
						SELECT ''I'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
						FROM inserted 
					END
				END
				ELSE
				BEGIN
					DECLARE @contextUserID nvarchar(50)
					SELECT @contextUserID = CAST(CONTEXT_INFO() AS nvarchar(50))

					IF LEN(@contextUserID) <=0
					BEGIN
						SELECT @contextUserID = SUSER_SNAME()
					END

					INSERT INTO Audit' + @tableName + '
					SELECT ''D'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
					FROM deleted

					DECLARE @ID int
					SET @ID = SCOPE_IDENTITY()

					UPDATE Audit' + @tableName + '
					SET LastModifiedBy_UserID = @contextUserID
					WHERE [ID] = @ID
				END
			END

			'
		END
		ELSE
		BEGIN
			SET @sql='CREATE TRIGGER [tgAudit'+ @tableName + '] 
		   ON  [' + @schema + '].['+ @tableName + '] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO Audit' + @tableName + '
					SELECT ''U'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO Audit' + @tableName + '
					SELECT ''I'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO Audit' + @tableName + '
				SELECT ''D'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
				FROM deleted
			END
		END

		'
		END


		PRINT @sql

		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		EXEC (@sql)
		
		FETCH NEXT FROM TableCursor
		INTO @schema, @tableName
END
CLOSE TableCursor;
DEALLOCATE TableCursor;
GO
