﻿DECLARE @TransStarted BIT

IF (@@TRANCOUNT = 0)
BEGIN
	BEGIN TRANSACTION
		
	SET @TransStarted = 1
END
ELSE
	SET @TransStarted = 0
	

	
	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END


IF (@TransStarted = 1)
BEGIN
	SET @TransStarted = 0

	COMMIT TRANSACTION
END

RETURN

Cleanup:

IF (@TransStarted = 1)
BEGIN
	SET @TransStarted = 0

	ROLLBACK TRANSACTION
END
