﻿/*
	This script consists of 3 parts:
	
	PART ONE: Performs changes to the underlying database schema

	PART TWO: Refreshes the triggers on the database

	PART THREE: Performs operations on data, insert, update and delete	
	
*/


IF OBJECT_ID('tempdb.dbo.#ScriptManager') IS NULL
BEGIN
	CREATE TABLE #ScriptManager 
	(
		VariableName nvarchar(30),
		Value nvarchar(30)
	)

	INSERT INTO #ScriptManager 
	SELECT 'Counter', '1'
END
GO

/*	PART ONE:
	
	PURPOSE:
	Amend the table structures in preparation for data manipulation in preparation for PART THREE
*/

DECLARE @Counter int
SELECT TOP 1 @Counter = CONVERT(int, Value) FROM #ScriptManager WHERE VariableName = 'Counter'

IF @Counter = 1	
BEGIN
	DECLARE @TransStarted BIT
	IF (@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRANSACTION

		SET @TransStarted = 1
				

		IF @@ERROR <> 0
		BEGIN
			GOTO Cleanup
		END

	END
	ELSE
		SET @TransStarted = 0

		IF @@ERROR <> 0
		BEGIN
			GOTO Cleanup
		END

	
	IF (@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0

		COMMIT TRANSACTION
	
		UPDATE #ScriptManager SET Value = CONVERT(NVARCHAR(50), @Counter + 1) WHERE VariableName = 'Counter'
		RAISERROR('Completed PART ONE' ,0, 1) WITH NOWAIT;

	END


	RETURN

	Cleanup:

	IF (@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0

		ROLLBACK TRANSACTION
	END
END
GO

/*	PART TWO:
	
	PURPOSE:
	Refresh the triggers in the database 
*/

DECLARE @Counter int
SELECT TOP 1 @Counter = CONVERT(int, Value) FROM #ScriptManager WHERE VariableName = 'Counter'

IF @Counter = 2	
BEGIN

DECLARE @TransStarted BIT
	IF (@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRANSACTION

		SET @TransStarted = 1
	END
	ELSE
		SET @TransStarted = 0

		DECLARE @tableCatalog nvarchar(128)
		SELECT @tableCatalog = DB_NAME()


		DECLARE @tableExclusions nvarchar(max)
		SET @tableExclusions = 'sysdiagrams,aspnet_InvalidCredentialsLog,aspnet_Applications,aspnet_Paths,aspnet_PersonalizationAllUsers,aspnet_PersonalizationPerUser,aspnet_Profile,aspnet_SchemaVersions,aspnet_UserRolesGroups,aspnet_UserRolesGroupsDefaults,aspnet_WebEvent_Events,__RefactorLog'


		DECLARE @tableName nvarchar(max)
		DECLARE @schema nvarchar(max)

		DECLARE TableCursor CURSOR FOR
		SELECT TABLE_SCHEMA, TABLE_NAME 
		FROM information_schema.tables 	
		WHERE TABLE_CATALOG = @tableCatalog
			AND TABLE_TYPE = 'BASE TABLE'
			AND TABLE_NAME NOT IN (SELECT Item FROM dbo.fnDelimitedSplit8K(@tableExclusions, ','))
			AND TABLE_NAME NOT LIKE 'Audit%'
			AND TABLE_NAME NOT LIKE 'TEMP_Audit%'
		ORDER BY TABLE_NAME

		OPEN TableCursor

		FETCH NEXT FROM TableCursor
		INTO @schema, @tableName

		WHILE @@FETCH_STATUS = 0
		BEGIN

				DECLARE @done bit 
				SET @done = 0 

				DECLARE @crlf nvarchar(max)
				SET @crlf = char(10)
				DECLARE @sql nvarchar(max)
				DECLARE @tableColumns nvarchar(max) = N' '


				DECLARE @columnID int
				SET @columnID = 0
				DECLARE @columnName nvarchar(max)

				WHILE @done=0   
				BEGIN            
					SELECT TOP 1			
					@columnID=clmns.column_id,
					@columnName=clmns.name 			
					FROM
					sys.tables AS tbl
					INNER JOIN sys.all_columns AS clmns 
						ON clmns.object_id=tbl.object_id
					LEFT OUTER JOIN sys.types AS usrt 
						ON usrt.user_type_id = clmns.user_type_id
					LEFT OUTER JOIN sys.types AS baset 
						ON baset.user_type_id = clmns.system_type_id and 
							baset.user_type_id = baset.system_type_id
					WHERE
					(tbl.name=@tableName and SCHEMA_NAME(tbl.schema_id)=@schema)
					and clmns.column_id > @columnID
					ORDER BY
					clmns.column_id ASC

					IF @@ROWCOUNT=0  
					BEGIN   
						SET @done=1  
					END
					ELSE
					BEGIN
						SET @tableColumns = @tableColumns + ' [' + @columnName + '], '
					END
				END
					
		
				SET @sql = 'IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N''[dbo].[tgAudit' + @tableName+']''))
				DROP TRIGGER [dbo].[tgAudit' + @tableName + ']'
				PRINT @sql

				EXEC(@sql)

				SET @tableColumns = SUBSTRING(@tableColumns, 1, LEN(@tableColumns) -1)
				PRINT @tableColumns

				IF EXISTS(select clmns.name
					from
					sys.tables AS tbl
					INNER JOIN sys.all_columns AS clmns 
						ON clmns.object_id=tbl.object_id
					LEFT OUTER JOIN sys.types AS usrt 
						ON usrt.user_type_id = clmns.user_type_id
					LEFT OUTER JOIN sys.types AS baset 
						ON baset.user_type_id = clmns.system_type_id and 
							baset.user_type_id = baset.system_type_id
					WHERE
					(tbl.name=@tableName and SCHEMA_NAME(tbl.schema_id)=@schema)
					and clmns.name = 'LastModifiedBy_UserID' )
				BEGIN
					SET @sql='CREATE TRIGGER [tgAudit'+ @tableName + '] 
					   ON  [' + @schema + '].['+ @tableName + '] 
					   AFTER INSERT, UPDATE, DELETE
					AS 
					BEGIN
						SET NOCOUNT ON;

						IF(SELECT COUNT(*) FROM inserted) > 0
						BEGIN
		
							IF(SELECT COUNT(*) FROM deleted) > 0
							BEGIN
								--Update
								INSERT INTO Audit' + @tableName + '
								SELECT ''U'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
								FROM inserted 
							END
							ELSE
							BEGIN
								--Insert
								INSERT INTO Audit' + @tableName + '
								SELECT ''I'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
								FROM inserted 
							END
						END
						ELSE
						BEGIN
							DECLARE @contextUserID nvarchar(50)
							SELECT @contextUserID = CAST(CONTEXT_INFO() AS nvarchar(50))

							IF LEN(@contextUserID) <=0
							BEGIN
								SELECT @contextUserID = SUSER_SNAME()
							END

							INSERT INTO Audit' + @tableName + '
							SELECT ''D'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
							FROM deleted

							DECLARE @ID int
							SET @ID = SCOPE_IDENTITY()

							UPDATE Audit' + @tableName + '
							SET LastModifiedBy_UserID = @contextUserID
							WHERE [ID] = @ID
						END
					END

					'
				END
				ELSE
				BEGIN
					SET @sql='CREATE TRIGGER [tgAudit'+ @tableName + '] 
				   ON  [' + @schema + '].['+ @tableName + '] 
				   AFTER INSERT, UPDATE, DELETE
				AS 
				BEGIN
					SET NOCOUNT ON;

					IF(SELECT COUNT(*) FROM inserted) > 0
					BEGIN
		
						IF(SELECT COUNT(*) FROM deleted) > 0
						BEGIN
							--Update
							INSERT INTO Audit' + @tableName + '
							SELECT ''U'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
							FROM inserted 
						END
						ELSE
						BEGIN
							--Insert
							INSERT INTO Audit' + @tableName + '
							SELECT ''I'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
							FROM inserted 
						END
					END
					ELSE
					BEGIN
						INSERT INTO Audit' + @tableName + '
						SELECT ''D'' AS Operation, GETUTCDATE() AS RecordedAtDateTime, ' + @tableColumns + '
						FROM deleted
					END
				END

				'
				END


				PRINT @sql

				SET ANSI_NULLS ON
				SET QUOTED_IDENTIFIER ON
				EXEC (@sql)
		
				FETCH NEXT FROM TableCursor
				INTO @schema, @tableName
		END
		CLOSE TableCursor;
		DEALLOCATE TableCursor;

		IF @@ERROR <> 0
			BEGIN
				GOTO Cleanup
			END
	

	IF (@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0

		COMMIT TRANSACTION

		UPDATE #ScriptManager SET Value = CONVERT(NVARCHAR(50), @Counter + 1) WHERE VariableName = 'Counter'
		RAISERROR('Completed PART TWO' ,0, 1) WITH NOWAIT;
	END


	RETURN

	Cleanup:

	IF (@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0

		ROLLBACK TRANSACTION
	END

END
GO



/*	PART THREE:
	
	PURPOSE:
	Perform data operations to insert, update or delete necessary data in prepartion for the SCHEMA COMPARE
*/
DECLARE @Counter int
SELECT TOP 1 @Counter = CONVERT(int, Value) FROM #ScriptManager WHERE VariableName = 'Counter'	

IF @Counter = 3
BEGIN
	DECLARE @TransStarted BIT

	IF (@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRANSACTION

		SET @TransStarted = 1
		
		IF @@ERROR <> 0
		BEGIN
			GOTO Cleanup
		END

		
	END
	ELSE
		SET @TransStarted = 0

		IF @@ERROR <> 0
		BEGIN
			GOTO Cleanup
		END

	IF (@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0

		COMMIT TRANSACTION

		UPDATE #ScriptManager SET Value = CONVERT(NVARCHAR(50), @Counter + 1) WHERE VariableName = 'Counter'
		RAISERROR('Completed PART THREE' ,0, 1) WITH NOWAIT;
	END


	RETURN

	Cleanup:

	IF (@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0

		ROLLBACK TRANSACTION
	END

END
GO


DECLARE @Counter int
SELECT TOP 1 @Counter = CONVERT(int, Value) FROM #ScriptManager WHERE VariableName = 'Counter'

IF @Counter >= 4 
BEGIN
	DROP TABLE #ScriptManager
	RAISERROR('Script Completed successfully.',0 ,1) WITH NOWAIT
END
ELSE
BEGIN
	DECLARE @Msg nvarchar(max)
	SET @Msg = 'Last part to complete successfully was PART ' + CONVERT(NVARCHAR(MAX), @Counter)
	RAISERROR(@Msg, 0, 1) WITH NOWAIT
END
GO
