﻿DECLARE @TransStarted bit

IF(@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRANSACTION
		SET @TransStarted = 1
	END
	ELSE
		SET @TransStarted = 0

	exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageGames'

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	exec aspnet_Roles_CreateRole '/diversity', 'PageView_ImportUsers'

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	exec aspnet_Roles_CreateRole '/diversity', 'PageView_Reporting'

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	DECLARE @defaultLanguageID int
	SELECT @defaultLanguageID = LanguageID 
	FROM Language
	WHERE LCID = 'en-US'

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	DECLARE @errorMessageID int
	INSERT INTO ErrorMessage(ErrorCode) VALUES ('MAXIMUM_GAMES_PLAYED')
	
	SELECT @errorMessageID = SCOPE_IDENTITY()
	INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'This game has already been played the maximum amount of times by this user.')

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	update result
	set 
		GamePlayerID = updatedResults.GamePlayerID
	from result
	inner join 
	(
	select ResultId,
			ResultXml, 
		   ResultXml.value('(/*/PlayerId)[1]', 'uniqueidentifier') as userId,
		   gp.GameID,
		   gp.GamePlayerID
	from Result r
	inner join
	GamePlayer gp on r.GameID = gp.GameID and r.ResultXml.value('(/*/PlayerId)[1]', 'uniqueidentifier') = gp.UserID) updatedResults
	on result.ResultID = updatedResults.ResultID

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

IF (@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0
		COMMIT TRANSACTION
	END

RETURN

Cleanup:
	
	IF(@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0
		ROLLBACK TRANSACTION
	END