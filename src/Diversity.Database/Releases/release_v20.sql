﻿DECLARE @TransStarted bit

IF(@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRANSACTION
		SET @TransStarted = 1
	END
	ELSE
		SET @TransStarted = 0


	INSERT INTO [dbo].[Internal_CardType]
           ([Internal_CardTypeName]
           ,[Internal_CardTypeShortCode]
           ,[Internal_CardTypeGroup])
     VALUES
           ('Card_TransitionUp'
           ,'TransitionUpCard'
           ,'Transition')

	
	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	INSERT INTO [dbo].[Internal_CardType]
           ([Internal_CardTypeName]
           ,[Internal_CardTypeShortCode]
           ,[Internal_CardTypeGroup])
     VALUES
           ('Card_TransitionDown'
           ,'TransitionDownCard'
           ,'Transition')

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	INSERT INTO [dbo].[Internal_CardType]
           ([Internal_CardTypeName]
           ,[Internal_CardTypeShortCode]
           ,[Internal_CardTypeGroup])
     VALUES
           ('QuestionCard_MultipleChoiceImage'
           ,'MultipleChoiceImageQuestionCard'
           ,'Question')

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END
	
	INSERT INTO [dbo].[Internal_CardType]
           ([Internal_CardTypeName]
           ,[Internal_CardTypeShortCode]
           ,[Internal_CardTypeGroup])
     VALUES
           ('Card_AuditStatementImage'
           ,'MultipleChoiceAuditStatementImageCard'
           ,'AuditStatement')

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END


	INSERT INTO [dbo].[Internal_CardType]
           ([Internal_CardTypeName]
           ,[Internal_CardTypeShortCode]
           ,[Internal_CardTypeGroup])
     VALUES
           ('Card_GameIntroCard'
           ,'GameIntroCard'
           ,'Intro')

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	INSERT INTO [dbo].[Internal_CardType]
           ([Internal_CardTypeName]
           ,[Internal_CardTypeShortCode]
           ,[Internal_CardTypeGroup])
     VALUES
           ('Card_ThemeIntroCard'
           ,'ThemeIntroCard'
           ,'Intro')

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	update Internal_CardType
	set Internal_CardTypeName = 'Survey_Intro'
	where Internal_CardTypeShortCode = 'IntroSurveyCard' and Internal_CardTypeGroup = 'Survey'

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END


IF (@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0
		COMMIT TRANSACTION
	END

RETURN

Cleanup:
	
	IF(@TransStarted = 1)
	BEGIN
		SET @TransStarted = 0
		ROLLBACK TRANSACTION
	END