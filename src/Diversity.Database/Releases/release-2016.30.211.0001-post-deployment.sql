﻿DECLARE @TransStarted BIT

IF (@@TRANCOUNT = 0)
BEGIN
	BEGIN TRANSACTION

	SET @TransStarted = 1
END
ELSE
	SET @TransStarted = 0
	
	DECLARE @ApplicationID uniqueidentifier
	SELECT @ApplicationID = ApplicationID
	FROM aspnet_Applications 
	WHERE LoweredApplicationName = '/diversity'

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	INSERT INTO aspnet_Roles (ApplicationId, RoleId, LoweredRoleName, RoleName) VALUES (@ApplicationID, NEWID(), 'pageview_managethemes', 'PageView_ManageThemes')
	
	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END
	
	INSERT INTO aspnet_Roles (ApplicationId, RoleId, LoweredRoleName, RoleName) VALUES (@ApplicationID, NEWID(), 'pageedit_managethemes', 'PageEdit_ManageThemes')
	
	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END
	

	DECLARE @LanguageID INT 
	SELECT @LanguageID = LanguageID
	FROM Language
	WHERE LCID = 'en-US'

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END
		
	DECLARE @ErrorMessageID INT
	INSERT INTO ErrorMessage (ErrorCode) VALUES ('Invalid_Theme_Code')
	SELECT @ErrorMessageID = SCOPE_IDENTITY()

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

	INSERT INTO ErrorMessage_Language (ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES (@ErrorMessageID, @LanguageID, 'The theme code supplied is invalid.')

	IF @@ERROR <> 0
	BEGIN
		GOTO Cleanup
	END

IF (@TransStarted = 1)
BEGIN
	SET @TransStarted = 0

	COMMIT TRANSACTION
END

RETURN

Cleanup:

IF (@TransStarted = 1)
BEGIN
	SET @TransStarted = 0

	ROLLBACK TRANSACTION
END
