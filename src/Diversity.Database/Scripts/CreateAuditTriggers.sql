﻿USE EChallenge
GO

IF OBJECT_ID (N'[dbo].fnDelimitedSplit8K') IS NOT NULL
    DROP FUNCTION dbo.fnDelimitedSplit8K
GO 


CREATE FUNCTION [dbo].[fnDelimitedSplit8K]
--===== Define I/O parameters
        (@pString NVARCHAR(MAX), @pDelimiter NCHAR(1))
RETURNS TABLE WITH SCHEMABINDING AS
 RETURN
--===== "Inline" CTE Driven "Tally Table" produces values from 0 up to 10,000...
     -- enough to cover VARCHAR(8000)
  WITH E1(N) AS (
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL 
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL 
                 SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1 UNION ALL SELECT 1
                ),                          --10E+1 or 10 rows
       E2(N) AS (SELECT 1 FROM E1 a, E1 b), --10E+2 or 100 rows
       E4(N) AS (SELECT 1 FROM E2 a, E2 b), --10E+4 or 10,000 rows max
 cteTally(N) AS (--==== This provides the "zero base" and limits the number of rows right up front
                     -- for both a performance gain and prevention of accidental "overruns"
                 SELECT 0 UNION ALL
                 SELECT TOP (DATALENGTH(ISNULL(@pString,1))) ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) FROM E4
                ),
cteStart(N1) AS (--==== This returns N+1 (starting position of each "element" just once for each delimiter)
                 SELECT t.N+1
                   FROM cteTally t
                  WHERE (SUBSTRING(@pString,t.N,1) = @pDelimiter OR t.N = 0) 
                )
--===== Do the actual split. The ISNULL/NULLIF combo handles the length for the final element when no delimiter is found.
 SELECT ItemNumber = ROW_NUMBER() OVER(ORDER BY s.N1),
        Item       = SUBSTRING(@pString,s.N1,ISNULL(NULLIF(CHARINDEX(@pDelimiter,@pString,s.N1),0)-s.N1,LEN(@pString)))
   FROM cteStart s
;
GO

DECLARE @tableCatalog nvarchar(50)
SET @tableCatalog = 'EChallenge'
DECLARE @tableExclusions nvarchar(max)
SET @tableExclusions = 'sysdiagrams,aspnet_InvalidCredentialsLog,aspnet_Applications,aspnet_Paths,aspnet_PersonalizationAllUsers,aspnet_PersonalizationPerUser,aspnet_Profile,aspnet_SchemaVersions,aspnet_UserRolesGroups,aspnet_UserRolesGroupsDefaults,aspnet_WebEvent_Events'


DECLARE @tableName nvarchar(max)
DECLARE @schema nvarchar(max)

DECLARE TableCursor CURSOR FOR
SELECT TABLE_SCHEMA, TABLE_NAME 
FROM information_schema.tables 	
WHERE TABLE_CATALOG = @tableCatalog
	AND TABLE_TYPE = 'BASE TABLE'
	AND TABLE_NAME NOT IN (SELECT Item FROM dbo.fnDelimitedSplit8K(@tableExclusions, ','))
	AND TABLE_NAME NOT LIKE 'Audit%'
ORDER BY TABLE_NAME

OPEN TableCursor

FETCH NEXT FROM TableCursor
INTO @schema, @tableName

WHILE @@FETCH_STATUS = 0
BEGIN

		DECLARE @done bit 
		SET @done = 0 

		DECLARE @crlf nvarchar(max)
		SET @crlf = char(10)
		DECLARE @sql nvarchar(max)


		SET @sql = 'IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[' + @schema + '].[Audit'+ @tableName +']'') AND type in (N''U''))
		DROP TABLE [dbo].[Audit' + @tableName +']' + @crlf 

		SET @sql = @sql + 'CREATE TABLE [' + @schema + '].[Audit' + @tableName + ']' + @crlf
		SET @sql = @sql + '(' + @crlf
		SET @sql = @sql + ' [ID] int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED, ' + @crlf

		DECLARE @columnID int
		SET @columnID = 0
		DECLARE @columnName nvarchar(max)
		DECLARE @columnType nvarchar(max)
		DECLARE @columnSize nvarchar(max)

		WHILE @done=0   
		BEGIN            
			SELECT TOP 1
			@columnID=clmns.column_id,
			@columnName=clmns.name ,
			@columnType=usrt.name ,
			@columnSize= 
							CASE 
								WHEN baset.name IN (N'nchar', N'nvarchar') AND clmns.max_length <> -1 
									THEN CONVERT(nvarchar(max), clmns.max_length/2)
								WHEN baset.name IN (N'nchar', N'nvarchar') AND clmns.max_length = -1 
									THEN 'max'
								WHEN baset.name IN (N'datetime2')
									THEN '7'
								ELSE CONVERT(nvarchar(max), clmns.max_length)
							END
			FROM
			sys.tables AS tbl
			INNER JOIN sys.all_columns AS clmns 
				ON clmns.object_id=tbl.object_id
			LEFT OUTER JOIN sys.types AS usrt 
				ON usrt.user_type_id = clmns.user_type_id
			LEFT OUTER JOIN sys.types AS baset 
				ON baset.user_type_id = clmns.system_type_id and 
					baset.user_type_id = baset.system_type_id
			WHERE
			(tbl.name=@tableName and SCHEMA_NAME(tbl.schema_id)=@schema)
			and clmns.column_id > @columnID
			ORDER BY
			clmns.column_id ASC

			IF @@ROWCOUNT=0  
			BEGIN   
				SET @done=1  
			END
			ELSE
			BEGIN
				SET @sql= @sql + ' [' + @columnName + '] '
				
				IF (@columnType IN ('timestamp')) 
				BEGIN	
					SET @sql = @sql + 'varbinary(8) '
				END
				ELSE
				BEGIN
					SET @sql = @sql + '[' + @columnType +'] '
				END
				
				IF (@columnType IN ('nchar', 'nvarchar', 'char', 'varchar', 'datetime2')) 
				BEGIN	
					SET @sql= @sql + '('+ rtrim(ltrim(@columnSize)) + ')'
				END
				
				SET @sql = @sql +' NULL, ' + @crlf
			END
		END

		SET @sql = @sql + ' [Operation] char(1) NOT NULL CHECK([Operation] IN (''I'',''U'',''D'')),' + @crlf
		SET @sql = @sql + ' [RecordedAtDateTime] datetime2(7) DEFAULT(GETUTCDATE()) NOT NULL' + @crlf
		SET @sql = @sql + ')' + @crlf
		
		PRINT @sql
		PRINT ' ' 
		EXEC (@sql)

		SET @sql = 'IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N''[dbo].[tgAudit' + @tableName+']''))
		DROP TRIGGER [dbo].[tgAudit' + @tableName + ']'
		PRINT @sql

		EXEC(@sql)

		IF EXISTS(select clmns.name
			from
			sys.tables AS tbl
			INNER JOIN sys.all_columns AS clmns 
				ON clmns.object_id=tbl.object_id
			LEFT OUTER JOIN sys.types AS usrt 
				ON usrt.user_type_id = clmns.user_type_id
			LEFT OUTER JOIN sys.types AS baset 
				ON baset.user_type_id = clmns.system_type_id and 
					baset.user_type_id = baset.system_type_id
			WHERE
			(tbl.name=@tableName and SCHEMA_NAME(tbl.schema_id)=@schema)
			and clmns.name = 'LastModifiedBy_UserID' )
		BEGIN
			SET @sql='CREATE TRIGGER [tgAudit'+ @tableName + '] 
			   ON  [' + @schema + '].['+ @tableName + '] 
			   AFTER INSERT, UPDATE, DELETE
			AS 
			BEGIN
				SET NOCOUNT ON;

				IF(SELECT COUNT(*) FROM inserted) > 0
				BEGIN
		
					IF(SELECT COUNT(*) FROM deleted) > 0
					BEGIN
						--Update
						INSERT INTO Audit' + @tableName + '
						SELECT *, ''U'', GETUTCDATE()
						FROM inserted 
					END
					ELSE
					BEGIN
						--Insert
						INSERT INTO Audit' + @tableName + '
						SELECT *, ''I'', GETUTCDATE()
						FROM inserted 
					END
				END
				ELSE
				BEGIN
					DECLARE @contextUserID nvarchar(50)
					SELECT @contextUserID = CAST(CONTEXT_INFO() AS nvarchar(50))

					IF LEN(@contextUserID) <=0
					BEGIN
						SELECT @contextUserID = SUSER_SNAME()
					END

					INSERT INTO Audit' + @tableName + '
					SELECT *, ''D'', GETUTCDATE()
					FROM deleted

					DECLARE @ID int
					SET @ID = SCOPE_IDENTITY()

					UPDATE Audit' + @tableName + '
					SET LastModifiedBy_UserID = @contextUserID
					WHERE [ID] = @ID
				END
			END

			'
		END
		ELSE
		BEGIN
			SET @sql='CREATE TRIGGER [tgAudit'+ @tableName + '] 
		   ON  [' + @schema + '].['+ @tableName + '] 
		   AFTER INSERT, UPDATE, DELETE
		AS 
		BEGIN
			SET NOCOUNT ON;

			IF(SELECT COUNT(*) FROM inserted) > 0
			BEGIN
		
				IF(SELECT COUNT(*) FROM deleted) > 0
				BEGIN
					--Update
					INSERT INTO Audit' + @tableName + '
					SELECT *, ''U'', GETUTCDATE()
					FROM inserted 
				END
				ELSE
				BEGIN
					--Insert
					INSERT INTO Audit' + @tableName + '
					SELECT *, ''I'', GETUTCDATE()
					FROM inserted 
				END
			END
			ELSE
			BEGIN
				INSERT INTO Audit' + @tableName + '
				SELECT *, ''D'', GETUTCDATE()
				FROM deleted
			END
		END

		'
		END


		PRINT @sql

		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		EXEC (@sql)

		FETCH NEXT FROM TableCursor
		INTO @schema, @tableName
END
CLOSE TableCursor;
DEALLOCATE TableCursor;
GO

-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'upContextInfoSet' 
)
   DROP PROCEDURE dbo.upContextInfoSet
GO

/*
	This procedure allows the application to pass context information into the database
	context. 

	For this application the primary use is to enable the user information to be passed
	into the audit process so that the last modified user can be set on a deleted
	audit row.	
*/
CREATE PROCEDURE [dbo].[upContextInfoSet](@contextUserId nvarchar(50))
AS 
BEGIN
	DECLARE @m binary(128)
	set @m = CAST(@contextUserId as binary(128))
	set CONTEXT_INFO @m
END
GO