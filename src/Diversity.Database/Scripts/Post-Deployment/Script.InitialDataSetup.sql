﻿Use eChallenge
GO

/* Add roles */
exec aspnet_Roles_CreateRole '/diversity', 'Administrators'
exec aspnet_Roles_CreateRole '/diversity', 'LeastPrivilegeUser'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageCardClassificationGroups'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageCards'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageClients'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageClientUserGroups'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageDistributors'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageFlashAsset'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageProducts'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageRoleGroups'
exec aspnet_Roles_CreateRole '/diversity', 'PageEdit_ManageUsers'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageCardClassificationGroups'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageCards'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageClients'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageClientUserGroups'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageDistributors'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageFlashAsset'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageLogViewer'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageProducts'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageRoleGroups'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageUsers'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ManageGames'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_ImportUsers'
exec aspnet_Roles_CreateRole '/diversity', 'PageView_Reporting'
exec aspnet_Roles_CreateRole '/diversity', 'Users'
GO

/* Add the internal cardtypes */
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('QuestionCard_ArrangeSequence','ArrangeSequenceQuestionCard','Question')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('QuestionCard_FillInTheBlank','FillInTheBlankQuestionCard','Question')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('QuestionCard_MatchingTwoLists','MatchingListQuestionCard','Question')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('QuestionCard_Numeric','NumericQuestionCard','Question')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('QuestionCard_PotLuck','PotLuckQuestionCard','Question')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('QuestionCard_MultipleChoice','MultipleChoiceQuestionCard','Question')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('QuestionCard_MultipleCheckboxSelection','MultipleCheckBoxQuestionCard','Question')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_LCDCategory','LcdCategoryCard','Category')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_AuditStatement','MultipleChoiceAuditStatementCard','AuditStatement')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_CompanyScoreCard','CompanyPolicyCard','CompanyPolicy')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_LeadingQuestion','LeadingQuestionCard','LeadingQuestion')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_PriorityFeedback_TextArea','TextAreaPriorityFeedbackCard','VotingFeedback')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_PriorityIssue_Voting','PriorityIssueVotingCard','Voting')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Survey_MultipleChoice','MultipleChoiceSurveyCard','Survey')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Survey_MultipleCheckboxSelection','MultipleCheckBoxSurveyCard','Survey')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Survey_TextBox','TextBoxSurveyCard','Survey')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Survey_TextArea','TextAreaSurveyCard','Survey')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_ImageCard','ImageCard','Image')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_PriorityFeedback_MultipleChoice','MultipleChoicePriorityFeedbackCard','VotingFeedback')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_PriorityFeedback_MultipleCheckBoxSelection','MultipleCheckBoxPriorityFeedbackCard','PriorityFeedback')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('PersonalActionPlan_MultipleChoice','MultipleChoicePersonalActionPlanCard','PersonalActionPlan')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('PersonalActionPlan_MultipleCheckboxSelection','MultipleCheckboxPersonalActionPlanCard','PersonalActionPlan')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('PersonalActionPlan_TextBox','TextBoxPersonalActionPlanCard','PersonalActionPlan')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('PersonalActionPlan_TextArea','TextAreaPersonalActionPlanCard','PersonalActionPlan')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('RoundResults','RoundResultCard','RoundResult')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Scoreboard','ScoreboardCard','Scoreboard')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Award','AwardCard','Award')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Certificate','CertificateCard','Certificate')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('GameCompleted','GameCompletedCard','GameCompleted')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('IntroSurveyCard','IntroSurveyCard','Survey')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_TransitionUp','TransitionUpCard','Transition')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_TransitionDown','TransitionDownCard','Transition')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('QuestionCard_MultipleChoiceImage','MultipleChoiceImageQuestionCard','Question')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_AuditStatementImage','MultipleChoiceAuditStatementImageCard','AuditStatement')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_GameIntroCard','GameIntroCard','Intro')
INSERT INTO Internal_CardType (Internal_CardTypeName, Internal_CardTypeShortCode, Internal_CardTypeGroup) VALUES ('Card_ThemeIntroCard','ThemeIntroCard','Intro')
GO

/* Add Languages */
INSERT INTO Language (Language, IsLeftToRight, LCID) VALUES ('English', 1, 'en-US')
GO

DECLARE @defaultLanguageID int
SELECT @defaultLanguageID = LanguageID 
FROM Language
WHERE LCID = 'en-US'

DECLARE @errorMessageID int

INSERT INTO ErrorMessage(ErrorCode) VALUES ('ACCOUNT_LOCKED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Your account has been locked. Please contact your administrator.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('ACCOUNT_NOT_ACTIVATED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Your account has not been activated. Please contact your administrator.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('ARGUMENT_NULL_EXCEPTION')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Argument is null.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('ARGUMENT_OUT_OF_RANGE')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Argument is out of range.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('CHANGE_PASSWORD_FAILED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Unable to change password')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('GAME_ALREADY_COMPLETED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'This game has already been completed.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('GAME_CANNOT_BE_PLAYED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'This is not a playable game.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('INVALID_ANSWER_PROVIDED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Please provide an answer.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('INVALID_CARDSEQUENCE_NUMBER_PROVIDED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Invalid card sequence number provided')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('INVALID_EMAIL')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Not a valid email address')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('INVALID_GAME_PROVIDED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Invalid game information provided.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('INVALID_USERNAME')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Username is invalid')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('LOGIN_FAILED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Username and/or password is incorrect. Please try again')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('MAXIMUM_GAMES_PLAYED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'This game has already been played the maximum amount of times by this user.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('MINIMUM_PASSWORD_REQUIREMENT')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Your password does not meet the minimum requirements. A password must have 1 number, 1 special character (!@#$%&*) and be 7 or more characters in length.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('PASSWORD_CHANGE_OLD_PASSWORD_FAILED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'We could not change your password. Please ensure you have entered the correct password.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('PASSWORD_RESET_EMAIL_FAILED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Unable to send password reset email.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('PASSWORD_RESET_EMAIL_SENT')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'An email has been sent to you with your new login details.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('SESSION_TIMEOUT_EXCEPTION')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Your session has timed out. Please login to continue.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('STRING_EQUAL_TO_ONE')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'One or more characters is required.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('STRING_GREATER_THAN_ZERO')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'One or more characters is required.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNABLE_TO_COMPLETE_GAME')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'A mandatory component of this game has not been completed. Please go to the dashboard and RESUME this game to go to the incomplete section.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNABLE_TO_RETRIEVE_DASHBOARD')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Unable to load dashboard')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNABLE_TO_RETRIEVE_GAME_SESSION')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Unable to retrieve your session.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNABLE_TO_SAVE_AVATAR_PROFILE')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Unable to save your avatar profile')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNABLE_TO_SAVE_CARD_RESULT')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Unable to save your answer.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNABLE_TO_SAVE_PLAYER_PROFILE')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'Unable to save your profile')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNABLE_TO_SET_GAME_AS_COMPLETED')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'An error has occurred. Unable to complete the game.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNABLE_TO_SKIP_CARD')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'An error has occurred. We are unable to skip this card.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNAUTHORIZED_FORBIDDEN')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'You are not authorized to invoke this service.')

INSERT INTO ErrorMessage(ErrorCode) VALUES ('UNHANDLED_EXCEPTION')
SELECT @errorMessageID = SCOPE_IDENTITY()
INSERT INTO ErrorMessage_Language(ErrorMessageID, LanguageID, ErrorMessageDescription) VALUES(@errorMessageID, @defaultLanguageID, 'An unhandled exception was thrown.')
GO

/*
SELECT 'INSERT INTO FlashAsset([Key]) VALUES(''' + [Key] +''')'
FROM FlashAsset
ORDER BY [Key]

*/
/* Add Flash Assets */
INSERT INTO FlashAsset([Key]) VALUES('AvatarProfile_AvatarDisclaimer')
INSERT INTO FlashAsset([Key]) VALUES('AvatarProfile_AvatarNameLabel')
INSERT INTO FlashAsset([Key]) VALUES('AvatarProfile_Description')
INSERT INTO FlashAsset([Key]) VALUES('AvatarProfile_Title')
INSERT INTO FlashAsset([Key]) VALUES('CardTitle_AuditStatementCards')
INSERT INTO FlashAsset([Key]) VALUES('CardTitle_CompanyPolicyCards')
INSERT INTO FlashAsset([Key]) VALUES('CardTitle_FeedbackCards')
INSERT INTO FlashAsset([Key]) VALUES('CardTitle_LeadingQuestionCards')
INSERT INTO FlashAsset([Key]) VALUES('CardTitle_PersonalActionPlanCards')
INSERT INTO FlashAsset([Key]) VALUES('CardTitle_PriorityVoting')
INSERT INTO FlashAsset([Key]) VALUES('CardTitle_QuestionCards')
INSERT INTO FlashAsset([Key]) VALUES('CardTitle_SurveyCards')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ChangePassword_ConfirmPasswordText')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ChangePassword_NewPasswordText')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ChangePassword_OldPasswordText')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ChangePasswordDescription')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ChangePasswordTitle')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_Label_CompletedGames')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_Label_ContinueGame')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_Label_NewGame')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_Label_SavedGames')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_Label_StartGame')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_Profile_SavedMessage')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ProfileChangeAvatarNameText')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ProfileChangeAvatarText')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ProfileChangeMyNameText')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ProfileChangePasswordText')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_ProfileSettingText')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_WelcomeMessage')
INSERT INTO FlashAsset([Key]) VALUES('Dashboard_WelcomeNote')
INSERT INTO FlashAsset([Key]) VALUES('Error_NotAllRequiredFieldsHaveBeenEntered')
INSERT INTO FlashAsset([Key]) VALUES('Error_PasswordNotSame')
INSERT INTO FlashAsset([Key]) VALUES('Error_PleaseSelectAllSelection')
INSERT INTO FlashAsset([Key]) VALUES('Error_PleaseSelectAValue')
INSERT INTO FlashAsset([Key]) VALUES('Error_PleaseSelectYourLanguage')
INSERT INTO FlashAsset([Key]) VALUES('ErrorPage_ActionButtonText')
INSERT INTO FlashAsset([Key]) VALUES('ErrorPage_FirstTitle_Red')
INSERT INTO FlashAsset([Key]) VALUES('ErrorPage_Message')
INSERT INTO FlashAsset([Key]) VALUES('ErrorPage_SecondTitle')
INSERT INTO FlashAsset([Key]) VALUES('Game_MultipleChoiceImage_ResultStatement')
INSERT INTO FlashAsset([Key]) VALUES('Game_MultipleChoiceImageAudit_OrganisationResponse')
INSERT INTO FlashAsset([Key]) VALUES('Game_MultipleChoiceText_ResultStatement')
INSERT INTO FlashAsset([Key]) VALUES('Game_MultipleChoiceTextAudit_OrganisationResponse')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_CategoryCard_Opening_IntroText1')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_CategoryCard_Opening_IntroText2')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_PriorityFeedback_AnswerInstructionText')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_PriorityFeedback_FollowUpQuestionText')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_PriorityFeedback_InstructionText')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_PriorityVoting_InstructionText1')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_PriorityVoting_InstructionText2')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_PriorityVotingQuestionMsg')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_Question_ReadQuestionAgainText')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_Question_ReasonBehindTheAnswerText')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_RoundResult_Title')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_ScoreBoardResult_Title')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_ThankYou_ExitGameText')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_ThankYou_ThankYouDescriptionText')
INSERT INTO FlashAsset([Key]) VALUES('GameCard_ThankYou_ThankYouTitle')
INSERT INTO FlashAsset([Key]) VALUES('GameCardScoreBoardRoundNameText')
INSERT INTO FlashAsset([Key]) VALUES('GameCardScoreBoardTotalScoreText')
INSERT INTO FlashAsset([Key]) VALUES('GameLeftNav_Home')
INSERT INTO FlashAsset([Key]) VALUES('GameLeftNav_Narration')
INSERT INTO FlashAsset([Key]) VALUES('GameLeftNav_Score')
INSERT INTO FlashAsset([Key]) VALUES('GameLeftNav_Sound')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_DashboardConfirmationMessage')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Label_Answer')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Label_MultiMedia')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Label_MultipleCboxAnswerInstructionText')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Label_MultipleChoiceAnswerInstructionText')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Label_MultipleChoiceAuditInstructionText')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Label_Progress')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Label_Question')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Label_TextBoxAnswerInstructionText')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Score_lblRoundTopics')
INSERT INTO FlashAsset([Key]) VALUES('GamePlay_Score_lblTotalScores')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonCancelClose')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonCancelContinue')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonCancelText')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonGoNext')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonNext')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonProceedText')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonResetPassword')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonSavePassword')
INSERT INTO FlashAsset([Key]) VALUES('Global_ButtonSkip')
INSERT INTO FlashAsset([Key]) VALUES('Global_lblLoadingText1')
INSERT INTO FlashAsset([Key]) VALUES('Global_lblLoadingText2')
INSERT INTO FlashAsset([Key]) VALUES('Global_lblLoadingText3')
INSERT INTO FlashAsset([Key]) VALUES('Global_TextBoxPlaceHolderText')
INSERT INTO FlashAsset([Key]) VALUES('Login_ButtonLoginText')
INSERT INTO FlashAsset([Key]) VALUES('Login_ForgotPasswordDescription')
INSERT INTO FlashAsset([Key]) VALUES('Login_ForgotPasswordLabel')
INSERT INTO FlashAsset([Key]) VALUES('Login_ForgotPasswordTitle')
INSERT INTO FlashAsset([Key]) VALUES('Login_PasswordLabel')
INSERT INTO FlashAsset([Key]) VALUES('Login_PlayerLoginDescription')
INSERT INTO FlashAsset([Key]) VALUES('Login_PlayerLoginTitle')
INSERT INTO FlashAsset([Key]) VALUES('Login_ProfileDescription')
INSERT INTO FlashAsset([Key]) VALUES('Login_ProfileFirstNameLabel')
INSERT INTO FlashAsset([Key]) VALUES('Login_ProfileLastNameLabel')
INSERT INTO FlashAsset([Key]) VALUES('Login_ProfileSaveButtonText')
INSERT INTO FlashAsset([Key]) VALUES('Login_ProfileTitle')
INSERT INTO FlashAsset([Key]) VALUES('Login_ResetPasswordButtonText')
INSERT INTO FlashAsset([Key]) VALUES('Login_UserNameLabel')
INSERT INTO FlashAsset([Key]) VALUES('Opening_ChooseYourLanguageDescription')
INSERT INTO FlashAsset([Key]) VALUES('Opening_ChooseYourLanguageTitle')
INSERT INTO FlashAsset([Key]) VALUES('Opening_IntroText')
INSERT INTO FlashAsset([Key]) VALUES('Opening_StartButtonLabel')
INSERT INTO FlashAsset([Key]) VALUES('PlayerSelect_Description')
INSERT INTO FlashAsset([Key]) VALUES('PlayerSelect_SelectButtonText')
INSERT INTO FlashAsset([Key]) VALUES('PlayerSelect_Title')
GO
/*  INSERT Flash Asset Language */
/* 
select
	'INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, ' +
	'(SELECT LanguageID FROM Language WHERE Language=''' + l.Language + '''),
		''' + REPLACE(fl.Value, '''','''''')+ '''
	 FROM FlashAsset WHERE [KEY] = '''+convert(nvarchar(max),fa.[Key])+''''
from FlashAsset_Language fl
inner join FlashAsset fa on fl.FlashAssetID = fa.FlashAssetID
inner join Language l on fl.LanguageID = l.LanguageID
*/
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Loading...'
	 FROM FlashAsset WHERE [KEY] = 'Global_lblLoadingText1'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Loading game logic'
	 FROM FlashAsset WHERE [KEY] = 'Global_lblLoadingText2'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Loading assets'
	 FROM FlashAsset WHERE [KEY] = 'Global_lblLoadingText3'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'START THE GAME NOW'
	 FROM FlashAsset WHERE [KEY] = 'Opening_StartButtonLabel'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Choose your language'
	 FROM FlashAsset WHERE [KEY] = 'Opening_ChooseYourLanguageTitle'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please select you preferred language below'
	 FROM FlashAsset WHERE [KEY] = 'Opening_ChooseYourLanguageDescription'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Cancel'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonCancelText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Proceed'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonProceedText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please select your language'
	 FROM FlashAsset WHERE [KEY] = 'Error_PleaseSelectYourLanguage'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Username'
	 FROM FlashAsset WHERE [KEY] = 'Login_UserNameLabel'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Password'
	 FROM FlashAsset WHERE [KEY] = 'Login_PasswordLabel'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Forgot Password'
	 FROM FlashAsset WHERE [KEY] = 'Login_ForgotPasswordLabel'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Login now'
	 FROM FlashAsset WHERE [KEY] = 'Login_ButtonLoginText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Login'
	 FROM FlashAsset WHERE [KEY] = 'Login_PlayerLoginTitle'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please enter your email address below'
	 FROM FlashAsset WHERE [KEY] = 'Login_ForgotPasswordDescription'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Email address:'
	 FROM FlashAsset WHERE [KEY] = 'Login_ResetPasswordButtonText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Enter your details below to login'
	 FROM FlashAsset WHERE [KEY] = 'Login_PlayerLoginDescription'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Profile'
	 FROM FlashAsset WHERE [KEY] = 'Login_ProfileTitle'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please enter your details below'
	 FROM FlashAsset WHERE [KEY] = 'Login_ProfileDescription'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'First Name'
	 FROM FlashAsset WHERE [KEY] = 'Login_ProfileFirstNameLabel'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Last Name'
	 FROM FlashAsset WHERE [KEY] = 'Login_ProfileLastNameLabel'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Next step'
	 FROM FlashAsset WHERE [KEY] = 'Login_ProfileSaveButtonText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Player Select'
	 FROM FlashAsset WHERE [KEY] = 'PlayerSelect_Title'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please select your avatar below'
	 FROM FlashAsset WHERE [KEY] = 'PlayerSelect_Description'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Your Avatar name may be viewed by other people. Do not use your real name or the real name of another person. Do not use any offensive or inappropriate language.'
	 FROM FlashAsset WHERE [KEY] = 'AvatarProfile_AvatarDisclaimer'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Name'
	 FROM FlashAsset WHERE [KEY] = 'AvatarProfile_AvatarNameLabel'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please complete the form below'
	 FROM FlashAsset WHERE [KEY] = 'AvatarProfile_Description'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Avatar Profile'
	 FROM FlashAsset WHERE [KEY] = 'AvatarProfile_Title'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Reset'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonResetPassword'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Putting the fun into online learning'
	 FROM FlashAsset WHERE [KEY] = 'Opening_IntroText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Forgot Password'
	 FROM FlashAsset WHERE [KEY] = 'Login_ForgotPasswordTitle'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Select avatar'
	 FROM FlashAsset WHERE [KEY] = 'PlayerSelect_SelectButtonText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Change your password'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ChangePasswordTitle'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please complete the form below'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ChangePasswordDescription'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please select from one of the options below. '
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_WelcomeNote'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Profile Settings'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ProfileSettingText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Change Avatar'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ProfileChangeAvatarText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Change Avatar Name'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ProfileChangeAvatarNameText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Change Password'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ProfileChangePasswordText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Change My Name'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ProfileChangeMyNameText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Welcome to your dashboard '
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_WelcomeMessage'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'New Game'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_Label_NewGame'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Saved Game'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_Label_SavedGames'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Completed Games'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_Label_CompletedGames'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Start Game'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_Label_StartGame'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Continue Game'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_Label_ContinueGame'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Save Password'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonSavePassword'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Old Password'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ChangePassword_OldPasswordText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'New Password'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ChangePassword_NewPasswordText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Confirm Password'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_ChangePassword_ConfirmPasswordText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Your profile has been changed'
	 FROM FlashAsset WHERE [KEY] = 'Dashboard_Profile_SavedMessage'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Company Policies'
	 FROM FlashAsset WHERE [KEY] = 'CardTitle_CompanyPolicyCards'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Personal action plan'
	 FROM FlashAsset WHERE [KEY] = 'CardTitle_PersonalActionPlanCards'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Did you know...'
	 FROM FlashAsset WHERE [KEY] = 'CardTitle_LeadingQuestionCards'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Quiz Round'
	 FROM FlashAsset WHERE [KEY] = 'CardTitle_QuestionCards'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Survey'
	 FROM FlashAsset WHERE [KEY] = 'CardTitle_SurveyCards'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Audit Statement'
	 FROM FlashAsset WHERE [KEY] = 'CardTitle_AuditStatementCards'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Priority Voting'
	 FROM FlashAsset WHERE [KEY] = 'CardTitle_PriorityVoting'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Feedback'
	 FROM FlashAsset WHERE [KEY] = 'CardTitle_FeedbackCards'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Progress'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Label_Progress'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'THANK YOU!!'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_ThankYou_ThankYouTitle'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Congratulations, you have completed the game.'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_ThankYou_ThankYouDescriptionText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Exit the game'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_ThankYou_ExitGameText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Next'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonNext'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Skip this question'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonSkip'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Select items by dragging the discs to the appropriate spot'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_PriorityVoting_InstructionText1'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Drag the number into the circle on the left'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_PriorityVoting_InstructionText2'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Pick a card to choose your category for the next question round'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_CategoryCard_Opening_IntroText1'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Click Start to choose your card!'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_CategoryCard_Opening_IntroText2'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Answer:'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Label_Answer'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Question:'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Label_Question'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'You have selected:'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_PriorityFeedback_InstructionText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please make your selection below:'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_PriorityFeedback_AnswerInstructionText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'The follow up question:'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_PriorityFeedback_FollowUpQuestionText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Next'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonGoNext'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please select one option below:'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Label_MultipleChoiceAnswerInstructionText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please select one of the below:'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Label_MultipleChoiceAuditInstructionText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please type your answer below:'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Label_TextBoxAnswerInstructionText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Review question'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_Question_ReadQuestionAgainText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Your result for this round is:'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_RoundResult_Title'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Your total score so far is:'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_ScoreBoardResult_Title'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Round Name'
	 FROM FlashAsset WHERE [KEY] = 'GameCardScoreBoardRoundNameText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Total Score'
	 FROM FlashAsset WHERE [KEY] = 'GameCardScoreBoardTotalScoreText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Enter text here...'
	 FROM FlashAsset WHERE [KEY] = 'Global_TextBoxPlaceHolderText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Select the {0} areas where you think your organisation can give better support in addressing inappropriate behaviour:'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_PriorityVotingQuestionMsg'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Passwords do not match. Please re-enter your password.'
	 FROM FlashAsset WHERE [KEY] = 'Error_PasswordNotSame'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please select a value'
	 FROM FlashAsset WHERE [KEY] = 'Error_PleaseSelectAValue'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please make all {0} selections'
	 FROM FlashAsset WHERE [KEY] = 'Error_PleaseSelectAllSelection'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Not all required fields have been entered'
	 FROM FlashAsset WHERE [KEY] = 'Error_NotAllRequiredFieldsHaveBeenEntered'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Please select from the options below:'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Label_MultipleCboxAnswerInstructionText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Click the icon below to view multimedia'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Label_MultiMedia'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Do you want to go back to the Dashboard screen?'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_DashboardConfirmationMessage'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Reason behind the Answer:'
	 FROM FlashAsset WHERE [KEY] = 'GameCard_Question_ReasonBehindTheAnswerText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Topic'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Score_lblRoundTopics'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Total scores'
	 FROM FlashAsset WHERE [KEY] = 'GamePlay_Score_lblTotalScores'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Close'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonCancelClose'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Continue'
	 FROM FlashAsset WHERE [KEY] = 'Global_ButtonCancelContinue'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Take me back to login Page'
	 FROM FlashAsset WHERE [KEY] = 'ErrorPage_ActionButtonText'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'This may be due to a long period of inactivity. Please log in again.'
	 FROM FlashAsset WHERE [KEY] = 'ErrorPage_Message'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Sorry, '
	 FROM FlashAsset WHERE [KEY] = 'ErrorPage_FirstTitle_Red'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'We couldn''t process your request at this time'
	 FROM FlashAsset WHERE [KEY] = 'ErrorPage_SecondTitle'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'How did others in your organisation respond?'
	 FROM FlashAsset WHERE [KEY] = 'Game_MultipleChoiceImage_ResultStatement'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'How did others in your organisation respond?'
	 FROM FlashAsset WHERE [KEY] = 'Game_MultipleChoiceText_ResultStatement'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Flash text Exit'
	 FROM FlashAsset WHERE [KEY] = 'GameLeftNav_Home'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Flash text Scoreboard'
	 FROM FlashAsset WHERE [KEY] = 'GameLeftNav_Score'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Flash text Mute / Unmute'
	 FROM FlashAsset WHERE [KEY] = 'GameLeftNav_Sound'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'Flash text Narration / Transcript'
	 FROM FlashAsset WHERE [KEY] = 'GameLeftNav_Narration'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'({0} responses)'
	 FROM FlashAsset WHERE [KEY] = 'Game_MultipleChoiceImageAudit_OrganisationResponse'
INSERT INTO FlashAsset_Language(FlashAssetID, LanguageID, Value)
	SELECT FlashAssetID, (SELECT LanguageID FROM Language WHERE Language='English'),
		'({0} responses)'
	 FROM FlashAsset WHERE [KEY] = 'Game_MultipleChoiceTextAudit_OrganisationResponse'
GO

DECLARE @RC int
DECLARE @ApplicationId uniqueidentifier
DECLARE @UserName nvarchar(256) = 'admin@symmetra.com.au'
DECLARE @IsUserAnonymous bit = 0
DECLARE @LastActivityDate datetime = GETDATE()
DECLARE @UserId uniqueidentifier

Select @ApplicationId = ApplicationID from aspnet_Applications where ApplicationName = '/diversity'

EXECUTE @RC = [dbo].[aspnet_Users_CreateUser] 
   @ApplicationId
  ,@UserName
  ,@IsUserAnonymous
  ,@LastActivityDate
  ,@UserId OUTPUT
GO

USE [EChallenge]
GO

DECLARE @RC int
DECLARE @ApplicationName nvarchar(256) = '/diversity'
DECLARE @UserName nvarchar(256) = 'admin@symmetra.com.au'
DECLARE @Password nvarchar(128) = 'Nj3rHoPn3vNF4I9L9JoWwbXi3FQ='
DECLARE @PasswordSalt nvarchar(128) = '7J0P1z9UtLqXlYALISNyHg=='
DECLARE @Email nvarchar(256) = 'kevin.gounden@teamery.com'
DECLARE @PasswordQuestion nvarchar(256)
DECLARE @PasswordAnswer nvarchar(128)
DECLARE @IsApproved bit = 1
DECLARE @CurrentTimeUtc datetime = GETUTCDATE()
DECLARE @CreateDate datetime = GETUTCDATE()
DECLARE @UniqueEmail int
DECLARE @PasswordFormat int = 1
DECLARE @UserId uniqueidentifier

select @UserId=userid from aspnet_users where UserName = 'admin@symmetra.com.au'

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[aspnet_Membership_CreateUser] 
   @ApplicationName
  ,@UserName
  ,@Password
  ,@PasswordSalt
  ,@Email
  ,@PasswordQuestion
  ,@PasswordAnswer
  ,@IsApproved
  ,@CurrentTimeUtc
  ,@CreateDate
  ,@UniqueEmail
  ,@PasswordFormat
  ,@UserId OUTPUT
GO

USE [EChallenge]
GO

DECLARE @RC int
DECLARE @ApplicationName nvarchar(256) = '/diversity'
DECLARE @UserNames nvarchar(4000) = 'admin@symmetra.com.au'
DECLARE @RoleNames nvarchar(4000) = 'Administrators,Users'
DECLARE @CurrentTimeUtc datetime = GETUTCDATE()

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[aspnet_UsersInRoles_AddUsersToRoles] 
   @ApplicationName
  ,@UserNames
  ,@RoleNames
  ,@CurrentTimeUtc
GO

DECLARE @RC int
DECLARE @ApplicationId uniqueidentifier
DECLARE @IsUserAnonymous bit = 0
DECLARE @LastActivityDate datetime = GETDATE()
DECLARE @UserId uniqueidentifier

Select @ApplicationId = ApplicationID from aspnet_Applications where ApplicationName = '/diversity'

EXECUTE @RC = [dbo].[aspnet_Users_CreateUser] 
   @ApplicationId
  ,'avatar1@mail.com'
  ,@IsUserAnonymous
  ,@LastActivityDate
  ,@UserId OUTPUT

DECLARE @ApplicationName nvarchar(256) = '/diversity'
DECLARE @UserName nvarchar(256) = 'avatar1@mail.com'
DECLARE @Password nvarchar(128) = 'Nj3rHoPn3vNF4I9L9JoWwbXi3FQ='
DECLARE @PasswordSalt nvarchar(128) = '7J0P1z9UtLqXlYALISNyHg=='
DECLARE @Email nvarchar(256) = 'avatar1@mail.com'
DECLARE @PasswordQuestion nvarchar(256)
DECLARE @PasswordAnswer nvarchar(128)
DECLARE @IsApproved bit = 1
DECLARE @CurrentTimeUtc datetime = GETUTCDATE()
DECLARE @CreateDate datetime = GETUTCDATE()
DECLARE @UniqueEmail int
DECLARE @PasswordFormat int = 1

select @UserId=userid from aspnet_users where UserName = 'avatar1@mail.com'

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[aspnet_Membership_CreateUser] 
   @ApplicationName
  ,@UserName
  ,@Password
  ,@PasswordSalt
  ,@Email
  ,@PasswordQuestion
  ,@PasswordAnswer
  ,@IsApproved
  ,@CurrentTimeUtc
  ,@CreateDate
  ,@UniqueEmail
  ,@PasswordFormat
  ,@UserId OUTPUT
GO

DECLARE @RC int
DECLARE @ApplicationId uniqueidentifier
DECLARE @IsUserAnonymous bit = 0
DECLARE @LastActivityDate datetime = GETDATE()
DECLARE @UserId uniqueidentifier

Select @ApplicationId = ApplicationID from aspnet_Applications where ApplicationName = '/diversity'

EXECUTE @RC = [dbo].[aspnet_Users_CreateUser] 
   @ApplicationId
  ,'avatar2@mail.com'
  ,@IsUserAnonymous
  ,@LastActivityDate
  ,@UserId OUTPUT

EXECUTE @RC = [dbo].[aspnet_Users_CreateUser] 
   @ApplicationId
  ,'avatar1@mail.com'
  ,@IsUserAnonymous
  ,@LastActivityDate
  ,@UserId OUTPUT

DECLARE @ApplicationName nvarchar(256) = '/diversity'
DECLARE @UserName nvarchar(256) = 'avatar2@mail.com'
DECLARE @Password nvarchar(128) = 'Nj3rHoPn3vNF4I9L9JoWwbXi3FQ='
DECLARE @PasswordSalt nvarchar(128) = '7J0P1z9UtLqXlYALISNyHg=='
DECLARE @Email nvarchar(256) = 'avatar2@mail.com'
DECLARE @PasswordQuestion nvarchar(256)
DECLARE @PasswordAnswer nvarchar(128)
DECLARE @IsApproved bit = 1
DECLARE @CurrentTimeUtc datetime = GETUTCDATE()
DECLARE @CreateDate datetime = GETUTCDATE()
DECLARE @UniqueEmail int
DECLARE @PasswordFormat int = 1

select @UserId=userid from aspnet_users where UserName = 'avatar2@mail.com'

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[aspnet_Membership_CreateUser] 
   @ApplicationName
  ,@UserName
  ,@Password
  ,@PasswordSalt
  ,@Email
  ,@PasswordQuestion
  ,@PasswordAnswer
  ,@IsApproved
  ,@CurrentTimeUtc
  ,@CreateDate
  ,@UniqueEmail
  ,@PasswordFormat
  ,@UserId OUTPUT

GO

DECLARE @RC int
DECLARE @ApplicationId uniqueidentifier
DECLARE @IsUserAnonymous bit = 0
DECLARE @LastActivityDate datetime = GETDATE()
DECLARE @UserId uniqueidentifier

Select @ApplicationId = ApplicationID from aspnet_Applications where ApplicationName = '/diversity'

EXECUTE @RC = [dbo].[aspnet_Users_CreateUser] 
   @ApplicationId
  ,'avatar3@mail.com'
  ,@IsUserAnonymous
  ,@LastActivityDate
  ,@UserId OUTPUT

 EXECUTE @RC = [dbo].[aspnet_Users_CreateUser] 
   @ApplicationId
  ,'avatar1@mail.com'
  ,@IsUserAnonymous
  ,@LastActivityDate
  ,@UserId OUTPUT

DECLARE @ApplicationName nvarchar(256) = '/diversity'
DECLARE @UserName nvarchar(256) = 'avatar3@mail.com'
DECLARE @Password nvarchar(128) = 'Nj3rHoPn3vNF4I9L9JoWwbXi3FQ='
DECLARE @PasswordSalt nvarchar(128) = '7J0P1z9UtLqXlYALISNyHg=='
DECLARE @Email nvarchar(256) = 'avatar3@mail.com'
DECLARE @PasswordQuestion nvarchar(256)
DECLARE @PasswordAnswer nvarchar(128)
DECLARE @IsApproved bit = 1
DECLARE @CurrentTimeUtc datetime = GETUTCDATE()
DECLARE @CreateDate datetime = GETUTCDATE()
DECLARE @UniqueEmail int
DECLARE @PasswordFormat int = 1

select @UserId=userid from aspnet_users where UserName = 'avatar3@mail.com'

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[aspnet_Membership_CreateUser] 
   @ApplicationName
  ,@UserName
  ,@Password
  ,@PasswordSalt
  ,@Email
  ,@PasswordQuestion
  ,@PasswordAnswer
  ,@IsApproved
  ,@CurrentTimeUtc
  ,@CreateDate
  ,@UniqueEmail
  ,@PasswordFormat
  ,@UserId OUTPUT
GO

UPDATE aspnet_Users SET PlayerAvatarData = 'avatar1', PlayerAvatarName='Sandy', 
UserFirstName='System1', UserLastName='User', UserPersonalEmail1='avatar1@mail.com'
where UserName = 'avatar1@mail.com'
GO

UPDATE aspnet_Users SET PlayerAvatarData = 'avatar2', PlayerAvatarName='Terry',
UserFirstName='System2', UserLastName='User', UserPersonalEmail1='avatar2@mail.com'
where UserName = 'avatar2@mail.com'
GO

UPDATE aspnet_Users SET PlayerAvatarData = 'avatar3', PlayerAvatarName='Danny',
UserFirstName='System3', UserLastName='User', UserPersonalEmail1='avatar3@mail.com'
where UserName = 'avatar3@mail.com'
GO

use msdb
GO

EXEC dbo.sp_start_job N'ExecuteAspnetRegSql'
GO