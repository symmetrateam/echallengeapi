﻿DECLARE @AdminID uniqueidentifier;
SELECT @AdminID = UserID FROM aspnet_Users WHERE LoweredUserName = 'admin@symmetra.com.au'

DELETE FROM aspnet_UsersInRoles 
WHERE UserID != @AdminID

TRUNCATE TABLE AssociatedCard

TRUNCATE TABLE [dbo].[Result]
TRUNCATE TABLE [dbo].[GameCardSequence]
TRUNCATE TABLE [dbo].[GameCardSequence]

TRUNCATE TABLE [dbo].[QuestionCard_GeneralAnswer_Language]
DELETE FROM [dbo].[QuestionCard_GeneralAnswer]
TRUNCATE TABLE [dbo].[QuestionCardStatement_Language]
DELETE FROM [dbo].[QuestionCard_Language]
DELETE FROM Card
DELETE FROM QuestionCard
DELETE FROM GameInvite
DELETE FROM GamePlayer
DELETE FROM Game
DELETE FROM [dbo].[CardReferenceMaterial_Language]
DELETE FROM [dbo].[CardReferenceMaterial]
DELETE FROM [dbo].[CardClassificationGroup_Language]
DELETE FROM [dbo].[CardClassificationGroup]
DELETE FROM [dbo].[Card2CardClassification]
DELETE FROM [dbo].[Card_Language]
DELETE FROM Card
DELETE FROM QuestionCard

DELETE FROM [dbo].[User2Client]
DELETE FROM [dbo].[User2ClientUserGroup]
DELETE FROM [dbo].[User2Distributor]
DELETE FROM [dbo].[Client2Distributor]
DELETE FROM [dbo].[DistributorClient2DistributorProduct]
DELETE FROM [dbo].[Product2Distributor]
DELETE FROM [dbo].[ClientUserGroup]
DELETE FROM [dbo].[Client]
DELETE FROM [dbo].[Distributor]
DELETE FROM [dbo].[Product_Language]
DELETE FROM [dbo].[Product]
DELETE FROM [dbo].[Session]
DELETE FROM [dbo].[aspnet_Profile]
DELETE FROM [dbo].[aspnet_Membership] WHERE UserID != @AdminID
DELETE FROM [dbo].[aspnet_Users] WHERE UserID != @AdminID


TRUNCATE TABLE [dbo].[Auditaspnet_Users]
TRUNCATE TABLE [dbo].[AuditAssociatedCard]
TRUNCATE TABLE [dbo].[AuditCard]
TRUNCATE TABLE [dbo].[AuditCard_Language]
TRUNCATE TABLE [dbo].[AuditCard2CardClassification]
TRUNCATE TABLE [dbo].[AuditCardClassificationGroup]
TRUNCATE TABLE [dbo].[AuditCardClassificationGroup_Language]
TRUNCATE TABLE [dbo].[AuditCardReferenceMaterial]
TRUNCATE TABLE [dbo].[AuditCardReferenceMaterial_Language]
TRUNCATE TABLE [dbo].[AuditClient]
TRUNCATE TABLE [dbo].[AuditClient2Distributor]
TRUNCATE TABLE [dbo].[AuditClientUserGroup]
TRUNCATE TABLE [dbo].[AuditDistributor]
TRUNCATE TABLE [dbo].[AuditDistributorClient2DistributorProduct]
TRUNCATE TABLE [dbo].[AuditGame]
TRUNCATE TABLE [dbo].[AuditGameCardGroupSequence]
TRUNCATE TABLE [dbo].[AuditGameCardSequence]
TRUNCATE TABLE [dbo].[AuditGameInvite]
TRUNCATE TABLE [dbo].[AuditGamePlayer]
TRUNCATE TABLE [dbo].[AuditProduct]
TRUNCATE TABLE [dbo].[AuditProduct_Language]
TRUNCATE TABLE [dbo].[AuditProduct2Distributor]
TRUNCATE TABLE [dbo].[AuditQuestionCard]
TRUNCATE TABLE [dbo].[aspnet_InvalidCredentialsLog]
TRUNCATE TABLE [dbo].[AuditQuestionCard]
TRUNCATE TABLE [dbo].[AuditQuestionCard_GeneralAnswer]
TRUNCATE TABLE [dbo].[AuditQuestionCard_GeneralAnswer_Language]
TRUNCATE TABLE [dbo].[AuditQuestionCard_Language]
TRUNCATE TABLE [dbo].[AuditQuestionCardStatement_Language]
TRUNCATE TABLE [dbo].[AuditResult]
TRUNCATE TABLE [dbo].[AuditUser2Client]
TRUNCATE TABLE [dbo].[AuditUser2ClientUserGroup]
TRUNCATE TABLE [dbo].[AuditUser2Distributor]


SELECT 
    t.NAME AS TableName,
    i.name as indexName,
    p.[Rows],
    sum(a.total_pages) as TotalPages, 
    sum(a.used_pages) as UsedPages, 
    sum(a.data_pages) as DataPages,
    (sum(a.total_pages) * 8) / 1024 as TotalSpaceMB, 
    (sum(a.used_pages) * 8) / 1024 as UsedSpaceMB, 
    (sum(a.data_pages) * 8) / 1024 as DataSpaceMB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
WHERE 
    t.NAME NOT LIKE 'dt%' AND
    i.OBJECT_ID > 255 AND   
    i.index_id <= 1
GROUP BY 
    t.NAME, i.object_id, i.index_id, i.name, p.[Rows]
ORDER BY 
    object_name(i.object_id) 