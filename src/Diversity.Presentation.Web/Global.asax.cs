﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;

namespace Diversity.Presentation.Web
{
    public class Global : System.Web.HttpApplication
    {
        private static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();         
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = HttpContext.Current.Server.GetLastError();

            while(exception.InnerException != null)
            {
                exception = exception.InnerException;
            }

            if(exception is HttpException)
            {
                var httpException = exception as HttpException;

                if(httpException.ErrorCode == 404)
                {
                    _log.Warn(@"Page not found - 404", exception);
                }
                else
                {
                    _log.Fatal(@"Unhandled exception.", exception);
                }
            }
            else if(exception is UIMessageException)
            {                
                var code = (exception as UIMessageException).MessageCode;
                Server.ClearError();
                var path =
                    string.Format("{0}?code={1}",
                                  VirtualPathUtility.ToAbsolute(@"~/MyResources/Errors/UIInformationMessage.aspx"), code);
                
                Response.Redirect(path);
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
         
        }
    }
}