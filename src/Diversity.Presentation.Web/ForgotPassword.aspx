﻿<%@ page language="C#" autoeventwireup="true" codebehind="ForgotPassword.aspx.cs" title="Forgot Password"
    inherits="Diversity.Presentation.Web.ForgotPassword" masterpagefile="~/AuthenticationPage.Master" %>

<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<%@ register src="UserControls/PasswordRecoveryControl.ascx" tagname="PasswordRecoveryControl"
    tagprefix="uc1" %>
<asp:content id="Content1" contentplaceholderid="head" runat="server">
</asp:content>
<asp:content id="Content2" contentplaceholderid="MainContentPlaceHolder" runat="server">
    <uc1:passwordrecoverycontrol id="PasswordRecoveryControl1" runat="server" />
</asp:content>
