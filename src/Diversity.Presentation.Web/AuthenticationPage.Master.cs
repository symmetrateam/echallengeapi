﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Diversity.Presentation.Web
{
    public partial class AuthenticationPage : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override string PageIconUrl
        {
            get { return TitleImage.ImageUrl; }
            set { TitleImage.ImageUrl = ResolveUrl(string.Format(@"~/App_Themes/Aqua/Icons/{0}", value)); }
        }
    }
}