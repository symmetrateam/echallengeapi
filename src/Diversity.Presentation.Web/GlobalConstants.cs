﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diversity.Presentation.Web
{
    public class GlobalConstants
    {
        public const string ADMINISTRATORS_ROLE = "Administrators";
        public const string USERS_ROLE = "Users";

        //PAGE VIEW PERMISSIONS
        public const string PAGEVIEW_MANAGEDISTRIBUTORS_ROLE = "PageView_ManageDistributors";
        public const string PAGEVIEW_MANAGECLIENTS_ROLE = "PageView_ManageClients";
        public const string PAGEVIEW_MANAGEUSERS_ROLE = "PageView_ManageUsers";
        public const string PAGEVIEW_MANAGEROLEGROUPS_ROLE = "PageView_ManageRoleGroups";
        public const string PAGEVIEW_MANAGECLIENTUSERGROUPS_ROLE = "PageView_ManageClientUserGroups";
        public const string PAGEVIEW_MANAGEPRODUCTS_ROLE = "PageView_ManageProducts";
        public const string PAGEVIEW_MANAGECARDCLASSIFICATIONGROUPS_ROLE = "PageView_ManageCardClassificationGroups";
        public const string PAGEVIEW_MANAGECARDS_ROLE = "PageView_ManageCards";

        //PAGE EDIT PERMISSIONS
        public const string PAGEEDIT_MANAGEDISTRIBUTORS_ROLE = "PageEdit_ManageDistributors";
        public const string PAGEEDIT_MANAGECLIENTS_ROLE = "PageEdit_ManageClients";
        public const string PAGEEDIT_MANAGEUSERS_ROLE = "PageEdit_ManageUsers";
        public const string PAGEEDIT_MANAGEROLEGROUPS_ROLE = "PageEdit_ManageRoleGroups";
        public const string PAGEEDIT_MANAGECLIENTUSERGROUPS_ROLE = "PageEdit_ManageClientUserGroups";
        public const string PAGEEDIT_MANAGEPRODUCTS_ROLE = "PageEdit_ManageProducts";
        public const string PAGEEDIT_MANAGECARDCLASSIFICATIONGROUPS_ROLE = "PageEdit_ManageCardClassificationGroups";
        public const string PAGEEDIT_MANAGECARDS_ROLE = "PageEdit_ManageCards";

        public const string PAGEVIEW_MANAGEGAMES = "PageView_ManageGames";
        public const string PAGEVIEW_IMPORTUSERS = "PageView_ImportUsers";
        public const string PAGEVIEW_REPORTING = "PageView_Reporting";

    }
}