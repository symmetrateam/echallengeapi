﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Web;
using Diversity.Common;

namespace Diversity.Presentation.Web
{
    public class EmailHelper
    {
        const string ACCOUNT_CREATED_EMAIL = @"~/MyResources/Emails/AccountCreated.txt";
        const string FORGOT_PASSWORD_EMAIL = @"~/MyResources/Emails/ForgotPassword.txt";
        const string GAME_INVITE = @"~/MyResources/Emails/GameInvitation.txt";

        public static void SendAccountCreatedEmail(string username, string password, string emailAddress)
        {
            var addresses = new List<MailAddress>();
            addresses.Add(new MailAddress(emailAddress));

            var replacements = new Dictionary<string, string>();
            replacements.Add("<%Username%>", username);
            replacements.Add("<%Password%>", password);

            SendMessage(Resources.Diversity.AccountCreatedEmailSubject, replacements, addresses, ACCOUNT_CREATED_EMAIL, false);
        }

        public static void SendPasswordResetEmail(string username, string password, string emailAddress)
        {
            var addresses = new List<MailAddress>();
            addresses.Add(new MailAddress(emailAddress));

            var replacements = new Dictionary<string, string>();
            replacements.Add("<%UserName%>", username);
            replacements.Add("<%Password%>", password);

            SendMessage(Resources.Diversity.AccountCreatedEmailSubject, replacements, addresses, FORGOT_PASSWORD_EMAIL, false);

        }

        public static MailMessage GetGameInvitationalEmailMessage(string address, string subject, string body, bool isHtml,
            string firstname, string lastname, string footer, string passwordText)
        {
            var message = new MailMessage();
            message.IsBodyHtml = isHtml;
            message.Subject = subject;

            var path = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute(GAME_INVITE));

            var bodyText = string.Empty;
            using (var reader = new StreamReader(path))
            {
                bodyText = reader.ReadToEnd();
            }

            var url = ConfigurationHelper.GameUrl;

            var replacements = new Dictionary<string, string>();
            replacements.Add("<%FIRSTNAME%>", firstname);
            replacements.Add("<%LASTNAME%>", lastname);
            replacements.Add("<%LOGINURL%>", url);
            replacements.Add("<%BODY%>", body);
            replacements.Add("<%EMAILADDRESS%>", address);
            replacements.Add("<%PASSWORDTEXT%>", passwordText);
            replacements.Add("<%FOOTER%>", footer);
            
            foreach (var item in replacements)
            {
                bodyText = bodyText.Replace(item.Key, item.Value);
            }

            message.Body = bodyText;
            message.To.Add(new MailAddress(address));

            return message;
        }


        public static void SendGameInvitationEmail(IEnumerable<MailAddress> addresses, string subject, string body, bool isHtml)
        {
            SendMessage(subject, addresses, body, isHtml);
        }

        static void SendMessage(string subject, Dictionary<string, string> replacements, IEnumerable<MailAddress> addresses, string emailPath, bool isHtml)
        {
            var client = new SmtpClient();
            client.EnableSsl = ConfigurationHelper.SmtpEnableSsl;

            var message = new MailMessage();
            message.IsBodyHtml = isHtml;

            foreach (var address in addresses)
            {
                message.To.Add(address);
            }

            var path = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute(emailPath));

            message.Subject = subject;

            var body = string.Empty;
            using (var reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }

            foreach (var item in replacements)
            {
                body = body.Replace(item.Key, item.Value);
            }

            message.Body = body;

            client.Send(message);
        }

        static void SendMessage(string subject, IEnumerable<MailAddress> addresses, string body, bool isHtml)
        {
            var client = new SmtpClient();
            client.EnableSsl = ConfigurationHelper.SmtpEnableSsl;

            var message = new MailMessage();
            message.IsBodyHtml = isHtml;

            foreach (var address in addresses)
            {
                message.To.Add(address);
            }

            message.Subject = subject;

            message.Body = body;

            client.Send(message);
        }
    }
}