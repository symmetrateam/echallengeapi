﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using Diversity.DataModel.SqlRepository;
using log4net;

namespace Diversity.Presentation.Web
{
    public class AccountProfile : ProfileBase 
    {
        static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Gets the currently logged in users id
        /// </summary>
        [SettingsAllowAnonymous(false)]
        public Guid UserIdentifier
        {
            get
            {
                var userId = Guid.NewGuid();

                try
                {
                    var user = Membership.GetUser();

                    if (user != null)
                    {
                        userId = new Guid(user.ProviderUserKey.ToString());
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(@"Error retrieving user", ex);
                }

                return userId;
            }
        }              

        [SettingsAllowAnonymous(false)]
        public string Culture 
        {
            get { return base["Culture"] as string; }
            set { base["Culture"] =  value; Save(); }
        }
        
        [SettingsAllowAnonymous(false)]
        public string PageLayoutDirection
        {
            get { return base["PageLayoutDirection"] as string; }
            set { base["PageLayoutDirection"] = value; Save(); }
        }

        public static AccountProfile GetProfile(string userName)
        {
            return Create(userName) as AccountProfile;            
        }

        public static AccountProfile GetProfile()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return null;
            }

            return GetProfile(Membership.GetUser().UserName);
        }
    }
}