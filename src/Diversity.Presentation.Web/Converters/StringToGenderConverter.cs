﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FileHelpers;

namespace Diversity.Presentation.Web.Converters
{
    public class StringToGenderConverter : ConverterBase
    {
        private const string GenderMale = "M";
        private const string GenderFemale = "F";

        public override object StringToField(string from)
        {
            if (from == null || from.Trim().Length == 0)
            {
                return null;
            }

            if (from.ToLower().Equals(GenderFemale.ToLower()) ||
                from.ToLower().Equals(GenderMale.ToLower()))
            {
                return from.ToUpper();
            }

            throw new ArgumentException();
        }

        public override string FieldToString(object fieldValue)
        {
            if (fieldValue == null)
            {
                return string.Empty;
            }

            if (fieldValue is string)
            {
                var stringValue = (string)fieldValue;

                if (stringValue.ToLower().Equals(GenderFemale.ToLower()) ||
                    stringValue.ToLower().Equals(GenderMale.ToLower()))
                {
                    return stringValue.ToUpper();
                }
            }

            return string.Empty;
        }
    }
}