﻿<%@ page language="C#" autoeventwireup="true" codebehind="Logout.aspx.cs" inherits="Diversity.Presentation.Web.Authentication.Logout"
    masterpagefile="~/AuthenticationPage.Master" title="Signed Out" %>

<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<asp:content id="Content1" contentplaceholderid="head" runat="server">
</asp:content>
<asp:content id="Content2" contentplaceholderid="MainContentPlaceHolder" runat="server">
    <div id="loginLinks">
        <dx:aspxhyperlink id="LoginHyperLink" runat="server" navigateurl="~/Login.aspx" text="Return to sign in page">
        </dx:aspxhyperlink>
    </div>
</asp:content>
