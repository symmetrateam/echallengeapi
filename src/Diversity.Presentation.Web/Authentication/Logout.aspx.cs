﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Diversity.Presentation.Web.Authentication
{
    public partial class Logout : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                SignOut();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            Master.PageIconUrl = "SignOut.png";

            base.OnInit(e);
        }

        private void SignOut()
        {
            //Sign out from existing session
            FormsAuthentication.SignOut();

            //destroy current cookie
            Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now;

            /* Create new session ticket that expires immediately */
            var ticket =
                new FormsAuthenticationTicket(
                    1,
                    HttpContext.Current.User.Identity.Name,
                    DateTime.Now,
                    DateTime.Now,
                    false,
                    Guid.NewGuid().ToString());

            /* Encrypt the ticket */
            var encryptedticket = FormsAuthentication.Encrypt(ticket);

            /* Create cookie */
            var cookie = new HttpCookie(
                FormsAuthentication.FormsCookieName,
                encryptedticket);

            /* Add cookie */
            Response.Cookies.Add(cookie);

            /* Abandon session object to destroy all session variables */
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();

            HttpContext.Current.User = null;
        }     

    }
}