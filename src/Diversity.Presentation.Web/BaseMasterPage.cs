﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Diversity.Presentation.Web
{
    public abstract class BaseMasterPage : MasterPage
    {
        public abstract string PageIconUrl
        {
            get;
            set;
        }        
    }
}