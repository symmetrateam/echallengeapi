﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Diversity.Presentation.Web
{
    public class UIMessageException : Exception
    {
        public string MessageCode { get; set; }

        public UIMessageException(string message): base(message)
        {            
            
        }

        public UIMessageException(string format, params object[] args): base(string.Format(format,args))
        {

        }

        public UIMessageException(string message, Exception innerException): base(message, innerException)
        {
            
        }

        public UIMessageException(string format, Exception innerException, params object[] args): base(string.Format(format, args), innerException)
        {
            
        }

        protected UIMessageException(SerializationInfo info, StreamingContext context): base(info, context)
        {
            
        }
    }
}