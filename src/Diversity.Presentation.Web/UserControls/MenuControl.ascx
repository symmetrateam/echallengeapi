﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuControl.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.MenuControl" %>
<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<dx:aspxsitemapdatasource id="WebSiteMap" runat="server"  enableroles="true">
</dx:aspxsitemapdatasource>

<dx:aspxmenu id="ASPxMenu1" runat="server" datasourceid="WebSiteMap" Width="90%" ShowPopOutImages="True">        
</dx:aspxmenu>

