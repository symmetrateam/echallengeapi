﻿<%@ control language="C#" autoeventwireup="true" codebehind="DualListBoxControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.DualListBoxControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>

<!--    
        Script is place here instead of .js as devexpress controls can't pick up server side 
        script files on a callback.
-->
<script id="dxss_DualListBoxControlScript" type="text/javascript">
    function UpdateButtons() {
        AddSelectedButton.SetEnabled(LeftListBox.GetSelectedItem() != null);
        AddAllButton.SetEnabled(LeftListBox.GetItemCount() > 0);
        RemoveSelectedButton.SetEnabled(RightListBox.GetSelectedItem() != null);
        RemoveAllButton.SetEnabled(RightListBox.GetItemCount() > 0);
    }
    function MoveItems(lb1, lb2) {
        lb2.BeginUpdate();
        var itemCount = lb1.GetItemCount();
        for (var i = 0; i < itemCount; i++) {
            var item = lb1.GetItem(0);
            lb2.InsertItem(GetCurrentIndex(item.value, lb2), item.text, item.value);
            lb1.RemoveItem(0);
        }
        lb2.EndUpdate();
        UpdateButtons();
    }
    function MoveSelectedItem(lb1, lb2) {
        var item = lb1.GetSelectedItem();
        if (item != null) {
            lb2.InsertItem(GetCurrentIndex(item.value, lb2), item.text, item.value);
            lb1.RemoveItem(item.index);
        }
        UpdateButtons();
    }

    function SelectOption() {
        MoveSelectedItem(LeftListBox, RightListBox);
    }
    function SelectAllOptions() {
        MoveItems(LeftListBox, RightListBox);
    }
    function UnselectOption() {
        MoveSelectedItem(RightListBox, LeftListBox);
    }
    function UnselectAllOptions() {
        MoveItems(RightListBox, LeftListBox);
    }
    function GetPrimaryIndex(value) {
        var options = GetPrimaryOptions();
        for (var i = 0; i < options.length; i++)
            if (options[i] == value)
                return i;
    }
    function GetCurrentIndex(value, lbDestination) {
        var options = GetPrimaryOptions();
        for (var i = (GetPrimaryIndex(value) - 1); i >= 0; i--) {
            var neighborIndex = GetItemIndexByValue(options[i], lbDestination);
            if (neighborIndex != -1)
                return neighborIndex + 1;
        }
        return 0;
    }
    function GetItemIndexByValue(value, listBox) {
        var itemCount = listBox.GetItemCount();
        for (var i = 0; i < itemCount; i++)
            if (listBox.GetItem(i).value == value)
                return i;
        return -1;
    }
    function GetPrimaryOptions() {
        return hiddenField.Get("options");
    }

</script>
<asp:panel id="ContainerPanel" runat="server" 
    >
    <table width="510px">
        <tr>
            <td>
                <dx:aspxlabel id="UnSelectedRoles" runat="server" text="Un-selected" 
                     />
            </td>
            <td>
            </td>
            <td>
                <dx:aspxlabel id="SelectedRoles" runat="server" text="Selected" 
                    />
            </td>
        </tr>
        <tr>
            <td align="center">
                <dx:aspxlistbox id="LeftListBox" clientinstancename="LeftListBox" runat="server"
                    enableviewstate="False" width="250px" height="350px" 
                    >
                    <clientsideevents selectedindexchanged="UpdateButtons" init="UpdateButtons" />
                </dx:aspxlistbox>
            </td>
            <td align="center">
                <dx:aspxbutton id="RemoveSelectedButton" clientinstancename="RemoveSelectedButton"
                    runat="server" autopostback="False" text="<" 
                    >
                    <clientsideevents click="UnselectOption" />
                </dx:aspxbutton>
                <dx:aspxbutton id="RemoveAllButton" clientinstancename="RemoveAllButton" runat="server"
                    autopostback="False" text="<<" >
                    <clientsideevents click="UnselectAllOptions" />
                </dx:aspxbutton>
                <dx:aspxbutton id="AddAllButton" clientinstancename="AddAllButton" runat="server"
                    autopostback="False" text=">>" >
                    <clientsideevents click="SelectAllOptions" />
                </dx:aspxbutton>
                <dx:aspxbutton id="AddSelectedButton" clientinstancename="AddSelectedButton" runat="server"
                    autopostback="False" text=">" 
                    >
                    <clientsideevents click="SelectOption" />
                </dx:aspxbutton>
            </td>
            <td align="center">
                <dx:aspxlistbox id="RightListBox" clientinstancename="RightListBox" runat="server"
                    enableviewstate="False" width="250px" height="350px" 
                    >                    
                    <clientsideevents selectedindexchanged="UpdateButtons"/>
                </dx:aspxlistbox>
            </td>
        </tr>
    </table>
    <dx:aspxhiddenfield id="hiddenField" clientinstancename="hiddenField" runat="server"
        syncwithserver="False">
    </dx:aspxhiddenfield>
</asp:panel>
