﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls
{
    public partial class LoginStatusControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            var fullNameLabel = LoginStatusCtl.FindControl(@"FullNameLabel") as Label;

            if (fullNameLabel != null)
            {
                var dataContext = new EChallengeDataContext();
                var userId = (Page as BasePage).CurrentContext.UserIdentifier;
                var user = (from u in dataContext.Users
                           where u.UserId == userId
                           select u).FirstOrDefault();

                var fullname = string.Empty;

                if (user != null && !string.IsNullOrEmpty(user.UserFirstName) && !string.IsNullOrEmpty(user.UserLastName))
                {
                    fullname = string.Format(@" {0} {1}", user.UserFirstName, user.UserLastName);
                }

                if(string.IsNullOrEmpty(fullname))
                {
                    fullname = string.Format(@" {0}", Page.User.Identity.Name.ToLower());
                }
                fullNameLabel.Text = fullname;
            }
        }

        protected void LanguageDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            CultureHelper.SetupCulture(LanguageDropDownList.SelectedItem.Value.ToString());
            Response.Redirect(Request.Url.PathAndQuery, true);
        }

        protected void LanguageDropDownList_DataBound(object sender, EventArgs e)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            if(string.IsNullOrEmpty(lcid) || string.Compare(lcid, "Auto", true) == 0)
            {
                var item = LanguageDropDownList.Items.FindByValue("en-US");
                if(item != null)
                {
                    item.Selected = true;
                    
                }
            }
            else
            {
                LanguageDropDownList.Items.FindByValue(lcid).Selected = true;   
            }
        }
    }
}