﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePasswordControl.ascx.cs"
    Inherits="Diversity.Presentation.Web.UserControls.ChangePasswordControl" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<script id="dxis_ChangePasswordControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/ChangePasswordControl.js") %>'>

</script>
<asp:ChangePassword ID="ChangePassControl" runat="server" ContinueDestinationPageUrl="~/Admin/Default.aspx"
    CancelDestinationPageUrl="~/Admin/Default.aspx" ChangePasswordFailureText="Your current password is incorrect or your new password does not meet the minimum requirements."
    >
    <ChangePasswordTemplate>
        <div>
            <dx:ASPxLabel ID="PasswordRequirementLabel" runat="server" Text="NB. A password must have 1 number, 1 special character (!@#$%^&amp;*) and be 7 or more characters in length."
                />
            <br />
            <dx:ASPxLabel ID="FailureText" runat="server" EnableViewState="False" ForeColor="Red">
            </dx:ASPxLabel>
        </div>
        <table>
            <tr>
                <td>
                    <dx:ASPxLabel ID="CurrentPasswordLabel" runat="server" 
                        Text="Current Password">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="CurrentPassword" runat="server" textmode="Password" 
                        Password="true">
                        <ValidationSettings ValidationGroup="ChangePassword1" SetFocusOnError="true" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField ErrorText="Password is required" IsRequired="true" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="NewPasswordLabel" runat="server" 
                        Text="New Password">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="NewPassword" runat="server" 
                        ClientInstanceName="txtNewPassword" Password="true">
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true" ValidationGroup="ChangePassword1">
                            <RequiredField IsRequired="true" ErrorText="New password is required" />
                            <RegularExpression ValidationExpression="(?=^.{6,}$)(?=.*\d)(?=.*\W+)(?![.\n]).*$"
                                ErrorText="The new password must have 1 number, 1 special character (!@#$%^&amp;*) and be 7 or more characters in length" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ConfirmNewPasswordLabel" runat="server" 
                        Text="Confirm New Password">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ConfirmNewPassword" runat="server" 
                        ClientInstanceName="txtConfirmNewPassword" Password="true">
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true" ErrorText="New and confirm passwords must match"
                            ValidationGroup="ChangePassword1">
                            <RequiredField IsRequired="true" ErrorText="Confirm new password is required" />
                        </ValidationSettings>
                        <ClientSideEvents Validation="function(s,e){ConfirmNewPasswordValidate(s,e);}" />
                    </dx:ASPxTextBox>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword"
                                    Text="Change Password" ValidationGroup="ChangePassword1" />
                            </td>
                            <td>
                                <dx:ASPxButton ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                    AutoPostBack="false" Text="Cancel" >
                                    <ClientSideEvents Click="function(s,e){ASPxClientEdit.ClearEditorsInContainerById('ChangePassword1');}" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ChangePasswordTemplate>
    <SuccessTemplate>
        <table>
            <tr>
                <td>
                    <dx:ASPxLabel ID="SuccessMessageLiteral" runat="server" Text="Your password has been successfully changed"
                         />
                </td>
            </tr>
            <%--            <tr>
                <td>
                    <dx:aspxbutton id="ContinuePushButton" runat="server" causesvalidation="False" text="Continue to home page"
                        commandname="Continue" />
                </td>
            </tr>
            --%>
        </table>
    </SuccessTemplate>
</asp:ChangePassword>
