﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Profile;
using System.Web.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BlueTorque.Net.Security.Web;
using DevExpress.Web;

namespace Diversity.Presentation.Web.UserControls
{
    public partial class LoginUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoginControl_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var loginUserName = LoginControl.UserName;
            var loginPassword = LoginControl.Password;
            var errorMessage = string.Empty;

            bool authenticated = AuthenticationHelper.ValidateUser(loginUserName,
                loginPassword,
                HttpContext.Current.Request.UserHostAddress,
                out errorMessage
                );

            e.Authenticated = authenticated;
            if (!authenticated)
            {
                LoginControl.FailureText = errorMessage;
            }

        }

        protected void LoginControl_LoggedIn(object sender, EventArgs e)
        {
            var languageDropDownList = LoginControl.FindControl(@"LanguageDropDownList") as ASPxComboBox;
            var languageLcid = languageDropDownList == null ? string.Empty : languageDropDownList.SelectedItem.Value.ToString();
            CultureHelper.SetupCulture(LoginControl.UserName, languageLcid);
        }

        protected void LanguageDropDownList_DataBound(object sender, EventArgs e)
        {
            var dropDownList = sender as ASPxComboBox;
            var item = dropDownList.Items.FindByTextWithTrim(@"English");

            if(item != null)
            {
                item.Selected = true;
            }
            else
            {
                dropDownList.SelectedIndex = 0;
            }
        }
    }
}