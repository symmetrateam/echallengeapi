﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.Common;
using log4net;

namespace Diversity.Presentation.Web.UserControls
{
    public partial class PasswordRecoveryControl : System.Web.UI.UserControl
    {
        ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ResetPwd_SendingMail(object sender, MailMessageEventArgs e)
        {
            _log.Debug("Forgot Password Email");

            var client = new SmtpClient();
            client.EnableSsl = ConfigurationHelper.SmtpEnableSsl;

            _log.DebugFormat("SSL Enabled={0}", client.EnableSsl.ToString());

            try
            {
                _log.Debug("Sending email");
                client.Send(e.Message);
                _log.Debug("Email sent");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            e.Cancel = true;
        }

        protected void PaswordRecoveryControl_SendMailError(object sender, SendMailErrorEventArgs e)
        {
            _log.Error(@"Forgot Password Email Error", e.Exception);

            e.Handled = false;
        }

        protected void PaswordRecoveryControl_UserLookupError(object sender, EventArgs e)
        {

            _log.Error(@"Unable to find user");

            
        }

        protected void PaswordRecoveryControl_VerifyingUser(object sender, LoginCancelEventArgs e)
        {
            _log.Debug(@"Veryfying user");           
            e.Cancel = false;
        }
    }
}