﻿
/*Handle the product selection process on page load*/
function ProductComboBox_SelectedIndexChanged(s, e) {
    if (e.index < 0) {
        ConfirmProductButton.SetEnabled(false);
    }
    else {
        ConfirmProductButton.SetEnabled(true);
    }
}

function ConfirmProductButton_Click(s, e) {
    var param = ProductComboBox.GetValue().toString();
    ProductSelectionCallback.PerformCallback(param);
    ConfirmProductButton.SetEnabled(false);
}

function ProductSelectionCallback_CallbackComplete(s, e) {
    if (e.result == null) {
        ProductSelectionPopupControl.Hide();
        ProductCallbackPanel.PerformCallback();
    }
}

function GameLibraryComboBox_SelectedIndexChanged(s, e) {
    var librarySelected = s.GetValue();
    if (librarySelected != null && librarySelected.toString() == "Client") {
        ClientLibraryComboBox.SetEnabled(true);
        ClientLibraryComboBox.PerformCallback();
    }
    else {
        ClientLibraryComboBox.SetEnabled(false);
        ClientLibraryComboBox.ClearItems();
    }
}

function SearchButton_Click(s, e) {

    //we need to get the selected nodes of the treelist first and process this
    //once the callback on the treelist is complete

    CardClassificationTreeList.GetSelectedNodeValues("CardClassificationGroupID", ProcessSearchResults);        
}


function ProcessSearchResults(values){
    
    var version = "";
    var cardType = "";
    var library = "";
    var classifications = "";

    if (values.length > 0) {

        for (var i = 0; i < values.length; i++) {
            if (i == values.length-1) {
                classifications = classifications + values[i].toString()
            }
            else {
                classifications = classifications + values[i].toString() + ";"
            }
        }
    }


    if (CardTypeListBox.GetSelectedIndex() >= 0) {
        cardType = CardTypeListBox.GetValue();
    }

    var searchText = SearchTextBox.GetText();
    var parameters = cardType + "|" + version + "|" + library + "|" + classifications + "|" + searchText;

    SearchResultGridView.PerformCallback(parameters);
}

//used to know which combobox in the master or detail 
//to populate from the selected search result
var _comboBoxToPopulate = "";

function SelectCardComboBox_Click(s, e) {
    CardFinderPopupControl.Show();
    _comboBoxToPopulate = "SelectCardComboBox";
}

function SelectDetailCardComboBox_Click(s, e) {
    CardFinderPopupControl.Show();
    _comboBoxToPopulate = "SelectDetailCardComboBox";
}

function CardFinderPopupControl_CloseButtonClick(s, e) {
    _comboBoxToPopulate = "";    
}

function SelectCardButton_Click(s, e) {
    if (_comboBoxToPopulate.length > 0 && SearchResultGridView.GetSelectedRowCount() > 0) {
        var columns = "CardId;CardName"

        SearchResultGridView.GetSelectedFieldValues(columns, OnProcessSearchResultsSelection);
    }
}

function CancelCardButton_Click(s, e) {
    _comboBoxToPopulate = "";
    CardFinderPopupControl.Hide();
}

function OnProcessSearchResultsSelection(values){
    
    var comboBox = GetComboBoxToPopulate(_comboBoxToPopulate);
    comboBox.ClearItems();
    
    if(values.length == 0) {
        return;
    }

    for(var i=0; i < values.length; i++){
        
        var val = values[i][0]; //CardID
        var text = values[i][1]; // CardName

        comboBox.AddItem(text, val);
    }

    if (comboBox.GetItemCount() > 0) {
        comboBox.SetSelectedIndex(0);
    }
    SearchResultGridView.UnselectRows();
    CardFinderPopupControl.Hide();
}

function GetComboBoxToPopulate(comboBoxName) {
    return eval(comboBoxName);
}

function GameCardGroupGridViewUpdateLinkButton_Click() {
    if (ASPxClientEdit.ValidateGroup('EditForm', false)) {
        GameCardGroupGridView.UpdateEdit();
    }
}

function SaveGameButton_Click(s, e) {
    SaveGameCallback.PerformCallback();
}

function SaveGameCallback_CallbackComplete(s, e) {
    if (e.result == null) {
        SetInformationSuccessLabelVisible(true);
    }
    else {
        SetInformationSuccessLabelVisible(false);
    }

    InformationMessagePopupControl.Show();
}

function OkButton_Click(s, e) {
    InformationMessagePopupControl.Hide();
}

function SetInformationSuccessLabelVisible(visible) {
    SuccessLabel.SetVisible(visible);
    FailLabel.SetVisible(!visible);
}

var _command = "";
function GameCardGridView_BeginCallback(s, e) {
    _command = e.command;
}

function GameCardGridView_EndCallback(s, e) {
    if (_command == "DELETEROW") {
        GameCardGroupGridView.Refresh();
    }    
}

function GameCardGridViewAddNewRow(s, e, gv) {
    gv.AddNewRow();
}



function DeleteGameDataCallback_CallbackComplete(s,e) {
    LoadingPanel.Hide();
    var response = e.result;
    DeleteCompleteLabel.SetText(response);
    DeleteCompletePopupControl.Show();
}

function DeleteGameDataButton_Click(s,e) {
    DeleteNotificationPopupControl.Show();
}

function DeleteDataButton_Click(s, e) {
    DeleteNotificationPopupControl.Hide();
    LoadingPanel.Show();
    DeleteGameDataCallback.PerformCallback();
}

function CancelDataGameButton_Click(s,e) {
    DeleteNotificationPopupControl.Hide();
}

function CloseButton_Click(s,e) {
    DeleteCompletePopupControl.Hide();
}
