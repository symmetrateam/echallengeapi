﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Diversity.Common.Enumerations;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class GameStatusControl : System.Web.UI.UserControl
    {
        public class GameStatusEntry
        {
            public Guid UserId { get; set; }
            public int GameID { get; set; }
            public string GameTemplateNameForDashboard { get; set; }
            public string UserFirstName { get; set; }
            public string UserLastName { get; set; }
            public string ClientName { get; set; }
            public int GameCount { get; set; }
            public int GameCompletedCount { get; set; }
            public int GameIncompleteCount { get; set; }
            public double PercentageComplete { get; set; }
            public DateTime? CompletionDate { get; set; }
        }

        int GameId
        {
            get
            {
                var v = Request.QueryString.Get("id");
                return v == null ? -1 : Convert.ToInt32(v);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (Request.UrlReferrer == null)
            {
                Response.Redirect(ResolveUrl(@"~/Default.aspx"));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void UserStatusDataSource_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            if (GameId == -1)
            {
                throw new Exception(Resources.Exceptions.GameDoesNotExist);
            }

            var dataContext = new EChallengeDataContext();

            var game = dataContext.Games.FirstOrDefault(g => g.GameID == GameId);

            if (game == null)
            {
                throw new Exception(Resources.Exceptions.GameDoesNotExist);
            }

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var userItems = (from g in dataContext.GameInvites
                             join tg in dataContext.Games on g.GameID equals tg.GameID
                             join u in dataContext.Users on g.UserID equals u.UserId
                             join u2c in dataContext.User2Clients on u.UserId equals u2c.UserID
                             join c in dataContext.Clients on u2c.ClientID equals c.ClientID
                             join pg in dataContext.Games on u.UserId equals pg.UserID_CreatedBy into ug
                             from playerGames in (from pg in ug
                                                      where pg.GameID_Template == GameId
                                                  select pg).DefaultIfEmpty()
                             where g.GameID == GameId && !g.IsAnAuditRow && !g.IsADeletedRow 
                             select new
                             {
                                 tg.GameTemplateNameForDashboard,
                                 g.GameID,
                                 u.UserId,
                                 u.UserFirstName,
                                 u.UserLastName,
                                 c.ClientName,
                                 HasGame = playerGames == null ? 0 : 1,
                                 IsGameComplete = playerGames == null ? 0 : playerGames.GameCompletionDate.HasValue ? 1 : 0,
                                 GameCompletionDate = playerGames == null ? 
                                                    null : playerGames.GameCompletionDate.HasValue ? 
                                                                        playerGames.GameCompletionDate : null
                             }).ToList();

            var results = from u in userItems
                          group u by new {u.UserId, u.GameID, u.GameTemplateNameForDashboard, u.UserFirstName, u.UserLastName, u.ClientName}
                          into g
                              select new GameStatusEntry
                                     {
                                         UserId = g.Key.UserId,
                                         GameID = g.Key.GameID,
                                         GameTemplateNameForDashboard =g.Key.GameTemplateNameForDashboard,
                                         UserFirstName = g.Key.UserFirstName,
                                         UserLastName = g.Key.UserLastName,
                                         ClientName = g.Key.ClientName,
                                         GameCount = g.Sum(u => u.HasGame),
                                         GameCompletedCount = g.Sum(u => u.IsGameComplete),
                                         GameIncompleteCount = g.Sum(u => u.HasGame) - g.Sum(u => u.IsGameComplete),
                                         PercentageComplete = g.Sum(u => u.HasGame) == 0 ? 0: ((g.Sum(u => u.HasGame) - g.Sum(u => u.IsGameComplete)) > 0 ? 0:100),
                                         CompletionDate = g.Sum(u => u.IsGameComplete) == 0 ? null : g.Where(f => f.IsGameComplete == 1).Max(f => f.GameCompletionDate)
                                     };


            foreach (var result in results)
            {
                if(result.GameIncompleteCount == 0)
                {
                    continue;
                }

                var incompleteGames =
                    dataContext.Games.Where(
                        g =>
                        g.GameID_Template.HasValue && g.GameID_Template.Value == result.GameID &&
                        g.UserID_CreatedBy == result.UserId && !g.GameCompletionDate.HasValue).Select(g => g.GameID).FirstOrDefault();

                if(incompleteGames > 0)
                {
                    result.PercentageComplete = GetPercentCompleted(result.GameID, dataContext);
                }
            }

            
            e.Result = results;

        }

        private double GetPercentCompleted(int gameId, EChallengeDataContext dataContext)
        {
            var game = dataContext.Games.FirstOrDefault(g => g.GameID == gameId);

            if(game.GameCompletionDate.HasValue)
            {
                return 100;
            }

            var noAnswerCardTypes = new List<string>
                                        {
                                            Enum.GetName(typeof (CardType), CardType.RoundResultCard),
                                            Enum.GetName(typeof (CardType), CardType.ScoreboardCard),
                                            Enum.GetName(typeof (CardType), CardType.GameCompletedCard),
                                            Enum.GetName(typeof (CardType), CardType.AwardCard),
                                            Enum.GetName(typeof (CardType), CardType.CertificateCard),
                                            Enum.GetName(typeof (CardType), CardType.CompanyPolicyCard),
                                            Enum.GetName(typeof (CardType), CardType.LcdCategoryCard),
                                            Enum.GetName(typeof(CardType), CardType.GameIntroCard),
                                            Enum.GetName(typeof(CardType), CardType.LeadingQuestionCard),            
                                            Enum.GetName(typeof(CardType), CardType.IntroSurveyCard),
                                            Enum.GetName(typeof(CardType), CardType.TransitionUpCard),
                                            Enum.GetName(typeof(CardType), CardType.TransitionDownCard),
                                            Enum.GetName(typeof(CardType), CardType.ThemeIntroCard)
                                        };

            var cardsequences = dataContext.GameCardSequences
                .Where(
                    g =>
                    g.GameCardGroupSequence.GameID == gameId)
                .ToList();                                   

            var totalCardsViewedInGame = cardsequences.Count(c => c.CardViewedInGame);
            
            var results = dataContext.Results.Where(r => r.GameID == gameId).Select(r => r.CardID).ToList();

            var cardSequencesToAnswer = (dataContext.GameCardGroupSequences
                .Where(g => g.GameID == gameId)
                .SelectMany(g => g.GameCardSequences))
                .Where(c => c.IsAMandatoryCard && !noAnswerCardTypes.Contains(c.Card.Internal_CardType.Internal_CardTypeShortCode)).ToList();

            var totalCardsViewedNoAnswerRequired = cardsequences.Count(c => !cardSequencesToAnswer.Contains(c) && c.CardViewedInGame);

            var noAnsweredCards = (from c in cardSequencesToAnswer
                                     where results.Contains(c.CardID)
                                     select c).Count();            

            var totalCardsForGameCompletion = cardsequences.Count;

            var percentCompleted = 0.00;

            if (totalCardsViewedInGame > 0)
            {
                percentCompleted = 100*
                                   ((Convert.ToDouble(totalCardsViewedNoAnswerRequired) + Convert.ToDouble(noAnsweredCards))/
                                    Convert.ToDouble(totalCardsForGameCompletion));
            }

            return percentCompleted;
        }

        protected void PercentageCompleteProgressBar_OnInit(object sender, EventArgs e)
        {
            var progressBar = sender as ASPxProgressBar;

            if (progressBar == null)
            {
                return;
            }

            progressBar.ShowPosition = true;
            progressBar.ForeColor = Color.White;
        }
    }
}