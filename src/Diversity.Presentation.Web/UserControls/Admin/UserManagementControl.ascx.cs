﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using BlueTorque.Net.Security.Web;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Data;
using Diversity.Common;
using Diversity.Common.Constants;
using Diversity.DataModel.SqlRepository;
using System.Transactions;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class UserManagementControl : System.Web.UI.UserControl
    {
        private const string USERNAME_SESSION_KEY = "EditingUserName";
        private const string USERID_SESSION_KEY = "EditingUserId";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SelectedRoles = null;
                EditingUserId = null;
                EditingUserName = null;
            }
        }

        protected void UserGridView_CustomButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCustomButtonEventArgs e)
        {
            if (e.CellType == GridViewTableCommandCellType.Data)
            {
                var applicationId = new Guid(Convert.ToString(UserGridView.GetRowValues(e.VisibleIndex, new string[] { "ApplicationId" })));
                var userId = new Guid(Convert.ToString(UserGridView.GetRowValues(e.VisibleIndex, new string[] { "UserId" })));                

                var dataContext = new EChallengeDataContext();
                var memberShip = (from m in dataContext.Users
                                  where m.ApplicationId == applicationId && m.UserId == userId
                                  select m).FirstOrDefault();

                if (memberShip == null)
                {
                    return;
                }

                if (e.ButtonID == "UnlockUserButton")
                {
                    if (!memberShip.IsLockedOut)
                    {
                        e.Text = Resources.Diversity.AuthenticationAccountUnlockedText;
                        e.Image.Url = ResolveUrl(@"~/App_Themes/Aqua/Icons/UnlockDisabled.png");
                        e.Enabled = false;
                    }
                }

                if (memberShip.IsApproved)
                {
                    if (e.ButtonID == "ActivateUserButton")
                    {
                        e.Visible = DefaultBoolean.False;
                    }
                    else if (e.ButtonID == "DeActivateUserButton")
                    {
                        e.Visible = DefaultBoolean.True;
                    }
                }
                else
                {
                    if (e.ButtonID == "DeActivateUserButton")
                    {
                        e.Visible = DefaultBoolean.False;
                    }
                    else if (e.ButtonID == "ActivateUserButton")
                    {
                        e.Visible = DefaultBoolean.True;
                    }
                }
            }
            else
            {
                if (e.ButtonID == "DeActivateUserButton"
                    || e.ButtonID == "ActivateUserButton"
                    || e.ButtonID == "UnlockUserButton")
                {
                    e.Visible = DefaultBoolean.False;
                }
            }
        }

        protected void UserGridView_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            var userName = Convert.ToString(UserGridView.GetRowValues(e.VisibleIndex, new string[] { "UserName" }));

            if (!string.IsNullOrEmpty(userName) && userName != (Page as BasePage).User.Identity.Name)
            {
                if (e.ButtonID == "ActivateUserButton")
                {
                    ActivateUser(userName, true);
                }
                else if (e.ButtonID == "DeActivateUserButton")
                {
                    ActivateUser(userName, false);
                }
                else if (e.ButtonID == "UnlockUserButton")
                {
                    UnlockUser(userName);
                }
            }

            UserGridView.DataBind();
        }

        protected void RolesDualListBox_Init(object sender, EventArgs e)
        {
            var rolesDualListBoxControl = sender as DualListBoxControl;

            SetSelectedRoles(rolesDualListBoxControl);
        }

        private void SetSelectedRoles(DualListBoxControl rolesDualListBoxControl)
        {
            var userName = EditingUserName;

            var completeList = GetAllRoles();

            var userRoles = Roles.GetRolesForUser(userName);
            var selected = userRoles
                .Where(r => r != GlobalConstants.USERS_ROLE)
                .Select(role => new ListEditItem(role, role)).ToList();

            if (SelectedRoles != null)
            {
                selected.AddRange(SelectedRoles.Select(r => new ListEditItem(r, r)));
            }

            rolesDualListBoxControl.SetInitialValues(completeList, selected);
        }


        #region Private Methods

        private void ActivateUser(string userName, bool activate)
        {
            var user = Membership.GetUser(userName);
            user.IsApproved = activate;
            Membership.UpdateUser(user);
        }

        private void UnlockUser(string username)
        {
            var user = Membership.GetUser(username);
            user.UnlockUser();
        }

        public List<string> GetAllDefaultRoleGroups()
        {
            var roleGroups = new List<string>();

            var results = RolesGroups.GetRoleGroups();

            if (results != null && results.Count > 0)
            {
                roleGroups.Add("Select...");

                foreach (var group in results)
                {
                    roleGroups.Add(group.GroupName);
                }
            }

            return roleGroups;
        }


        private List<ListEditItem> GetAllRoles()
        {
            var allRoles = Roles.GetAllRoles();
            var resultRoles = new List<ListEditItem>();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                resultRoles.AddRange(allRoles
                    .Where(r => r != GlobalConstants.USERS_ROLE)
                    .Select(role => new ListEditItem(role, role)));
            }
            else
            {
                var loggedInUserRoles = Roles.GetRolesForUser(Page.User.Identity.Name);
                resultRoles.AddRange(loggedInUserRoles
                    .Where(r => r != GlobalConstants.USERS_ROLE)
                    .Select(role => new ListEditItem(role, role)));
            }

            return resultRoles;
        }


        #endregion

        /// <summary>
        /// Gets the newly selected roles
        /// </summary>
        protected List<string> SelectedRoles
        {
            get { return Session["SelectedRoles"] == null ? null : Session["SelectedRoles"] as List<string>; }
            set { Session["SelectedRoles"] = value; }
        }

        protected string EditingUserName
        {
            get
            {
                if (Session[USERNAME_SESSION_KEY] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return Session[USERNAME_SESSION_KEY].ToString();
                }
            }
            set { Session[USERNAME_SESSION_KEY] = value; }
        }

        protected Guid? EditingUserId
        {
            get
            {
                if (Session[USERID_SESSION_KEY] == null)
                {
                    return null;
                }
                else
                {
                    return (Guid)Session[USERID_SESSION_KEY];
                }
            }
            set { Session[USERID_SESSION_KEY] = value; }
        }

        protected void UserGridView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            SelectedRoles = null;
        }

        protected void UserGridView_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            SelectedRoles = null;

            var dataContext = new EChallengeDataContext();
            var userId = new Guid(e.EditingKeyValue.ToString());

            var user = dataContext.Users.FirstOrDefault(u => u.UserId == userId);

            EditingUserName = user.UserName;
            EditingUserId = user.UserId;
        }

        protected void RolesDualListBoxCallbackPanel_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            var panel = sender as ASPxCallbackPanel;
            var dualListBoxControl = panel.FindControl(@"RolesDualListBox") as DualListBoxControl;            
            string selectedGroup = e.Parameter;

            //get the default roles for the selected rolesgroups template
            if (!selectedGroup.Contains("Select..."))
            {
                var roles = RolesGroups.GetRoleGroupRoles(selectedGroup);
                var selected = new List<ListEditItem>();
                SelectedRoles = null;
                foreach (string role in roles)
                {
                    if (SelectedRoles == null)
                    {
                        SelectedRoles = new List<string>();
                    }
                    SelectedRoles.Add(role);
                }
            }

            SetSelectedRoles(dualListBoxControl);
        }

        public static string RemoveSpecialCharacters(string str)
        {
            var sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        protected void UserGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var userFirstName = GetUserFirstName().Trim();
            var userLastName = GetUserLastName().Trim();
            var personalEmail = GetUserPersonalEmail1().Trim();
            var editingUser = (Page as BasePage).CurrentContext.UserIdentifier;

            var tempUser = Membership.GetUser(personalEmail);
            if (tempUser != null)
            {
                throw new Exception(Resources.Exceptions.UsernameAlreadyExists);
            }

            //var validateRoles = GetSelectedUserRoles();

            //if (validateRoles.Count <= 0)
            //{
            //    throw new Exception(Resources.Exceptions.AtLeastOneRoleMustBeSelected);
            //}


            var dataContext = new EChallengeDataContext();

            using (var transaction = dataContext.Database.BeginTransaction())
            {
                try
                {
                    var password = string.Empty;
                    var passwordValid = false;
                    while (!passwordValid)
                    {
                        password = Membership.GeneratePassword(15,
                                                               Membership.MinRequiredNonAlphanumericCharacters);

                        passwordValid = Regex.IsMatch(password, Membership.PasswordStrengthRegularExpression);
                    }

                    //we need to create a membership user            
                    MembershipUser membershipUser = Membership.CreateUser(personalEmail, password, personalEmail);
                    membershipUser.IsApproved = true;
                    Membership.UpdateUser(membershipUser);

                    var currentRoles = Roles.GetRolesForUser(personalEmail);
                    var selectedRoles = GetSelectedUserRoles();

                    //first delete roles
                    foreach (var role in currentRoles.Where(role => !selectedRoles.Contains(role)))
                    {
                        Roles.RemoveUserFromRole(personalEmail, role);
                    }

                    //now add new roles
                    //first add to default user role
                    Roles.AddUserToRole(personalEmail, GlobalConstants.USERS_ROLE);
                    foreach (var role in selectedRoles.Where(role => !Roles.IsUserInRole(personalEmail, role)))
                    {
                        Roles.AddUserToRole(personalEmail, role);
                    }

                    var userId = new Guid(membershipUser.ProviderUserKey.ToString());

                    var user = (from u in dataContext.Users
                                where u.UserId == userId
                                select u).FirstOrDefault();

                    var currentDateTime = DateTime.Now;

                    //set fields 
                    user.ClientBusinessUnit = GetClientBusinessUnit();
                    user.ClientCostCentre = GetClientCostCentre();
                    user.ClientDivision = GetClientDivision();
                    user.ClientJobTitle = GetClientJobTitle();
                    user.ClientLocation = GetClientLocation();
                    user.ClientSubsidiaryCompanyName = GetClientSubsidiaryCompanyName();
                    user.ClientUniqueSalaryNo = GetClientUniqueSalaryNo();
                    user.ClientUserGroup = GetClientUserGroup();
                    user.CreatedOnDateTime = currentDateTime;
                    user.UserID_CreatedBy = editingUser;
                    user.IsADeletedRow = false;
                    user.IsAnAuditRow = false;
                    user.LastModifiedOnDateTime = currentDateTime;
                    user.UserID_LastModifiedBy = editingUser;
                    user.UserDateOfBirth = GetUserDateOfBirth();
                    user.UserFirstName = userFirstName;
                    user.UserGender = GetUserGender();
                    user.UserID_CreatedBy = userId;
                    user.UserID_LastModifiedBy = userId;
                    user.UserLastName = userLastName;
                    user.UserPersonalEmail1 = personalEmail;
                    user.UserPersonalMobile = GetUserPersonalMobile();
                    user.UserWorkEmail = GetUserWorkEmail();
                    user.UserWorkMobile = GetUserWorkMobile();
                    user.UserWorkNotes = GetUserWorkNotes();
                    user.UserWorkTel1 = GetUserWorkTel1();
                    user.UserWorkTel2 = GetUserWorkTel2();
                    user.PlayerAvatarData = GetAvatarData();
                    user.PlayerAvatarName = GetAvatarName();
                    var clientId = GetClientSelectedValue();

                    if (clientId.HasValue)
                    {
                        user.Clients.Add
                            (
                                new User2Client()
                                {
                                    ClientID = clientId.Value,
                                    CreatedOnDateTime = currentDateTime,
                                    LastModifiedDateTime = currentDateTime,
                                    UserID = user.UserId,
                                    UserID_CreatedBy = editingUser,
                                    UserID_LastModifiedBy = editingUser
                                }
                            );

                        var clientUserGroup = dataContext.ClientUserGroups.FirstOrDefault(
                            c =>
                                c.ClientID == clientId.Value &&
                                c.ClientUserGroupName == SecurityGroupResources.ClientUserGroupNameAllUsers);

                        if (clientUserGroup != null)
                        {
                            if (
                                !user.ClientUserGroups.Any(
                                    c =>
                                        c.ClientUserGroupID == clientUserGroup.ClientUserGroupID &&
                                        c.UserID == user.UserId))
                            {
                                user.ClientUserGroups.Add(
                                    new User2ClientUserGroup()
                                    {
                                        ClientUserGroupID = clientUserGroup.ClientUserGroupID,
                                        CreatedOnDateTime = currentDateTime,
                                        LastModifiedOnDateTime = currentDateTime,
                                        UserID = user.UserId,
                                        UserID_CreatedBy = user.UserId,
                                        UserID_LastModifiedBy = user.UserId
                                    });
                            }
                        }
                    }

                    var selectedDistributors = GetSelectedDistributors();
                    foreach (var distributor in selectedDistributors)
                    {
                        var mapping = new User2Distributor()
                        {
                            CreatedOnDateTime = currentDateTime,
                            DistributorID = distributor,
                            LastModifiedDateTime = currentDateTime,
                            UserID = user.UserId,
                            UserID_CreatedBy = editingUser,
                            UserID_LastModifiedBy = editingUser
                        };
                        user.Distributors.Add(mapping);
                    }

                    dataContext.SaveChanges();                  

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }

            e.Cancel = true;
            UserGridView.CancelEdit();
            UserGridView.DataBind();
            SelectedRoles = null;

        }

        #region Get Control Values Methods

        protected string GetUserFirstName()
        {
            return GetASPxTextBoxValue("FirstNameTextBox");
        }

        protected string GetUserLastName()
        {
            return GetASPxTextBoxValue("LastNameTextBox");
        }

        protected DateTime? GetUserDateOfBirth()
        {
            return GetASPxDateEditValue("DOBDateEdit");
        }

        protected string GetUserGender()
        {
            return GetASPxRadioButtonListValue("GenderRadioButtonList") as string;
        }

        protected int? GetClientSelectedValue()
        {
            var selectedItem = GetASPxComboBoxSelectedItem("CompanyComboBox");

            return selectedItem == null ? null : (int?)selectedItem.Value;
        }


        protected string GetUserPersonalEmail1()
        {
            return GetASPxTextBoxValue("PersonalEmailTextBox").Trim();
        }

        protected string GetUserPersonalMobile()
        {
            return GetASPxTextBoxValue("PersonalMobileTextBox").Trim();
        }

        protected string GetClientJobTitle()
        {
            return GetASPxTextBoxValue("JobTitleTextBox").Trim();
        }

        protected string GetClientUniqueSalaryNo()
        {
            return GetASPxTextBoxValue("SalaryNoTextBox").Trim();
        }

        protected string GetClientSubsidiaryCompanyName()
        {
            return GetASPxTextBoxValue("SubsidiaryTextBox").Trim();
        }

        protected string GetUserWorkEmail()
        {
            return GetASPxTextBoxValue("WorkEmailTextBox").Trim();
        }

        protected string GetClientBusinessUnit()
        {
            return GetASPxTextBoxValue("BusinessUnitTextBox").Trim();
        }

        protected string GetUserWorkTel1()
        {
            return GetASPxTextBoxValue("WorkTel1TextBox").Trim();
        }

        protected string GetClientLocation()
        {
            return GetASPxTextBoxValue("LocationTextBox").Trim();
        }

        protected string GetUserWorkTel2()
        {
            return GetASPxTextBoxValue("WorkTel2TextBox").Trim();
        }

        protected string GetClientCostCentre()
        {
            return GetASPxTextBoxValue("CostCentreTextBox").Trim();
        }

        protected string GetUserWorkMobile()
        {
            return GetASPxTextBoxValue("WorkMobileTextBox").Trim();
        }

        protected string GetClientDivision()
        {
            return GetASPxTextBoxValue("DivisionTextBox").Trim();
        }

        protected string GetClientUserGroup()
        {
            return GetASPxTextBoxValue("UserGroupTextBox").Trim();
        }

        protected string GetUserWorkNotes()
        {
            return GetASPxMemoValue("WorkNotesMemo").Trim();
        }

        protected string GetAvatarData()
        {
            return GetASPxTextBoxValue("AvatarDataTextBox").Trim();
        }

        protected string GetAvatarName()
        {
            return GetASPxTextBoxValue("AvatarNameTextBox").Trim();
        }

        //protected string GetUserName()
        //{
        //    return GetASPxTextBoxValue("UserNameTextBox").Trim();
        //}

        protected IEnumerable<int> GetSelectedDistributors()
        {
            var pageControl = UserGridView.FindEditFormTemplateControl("UserPageControl") as ASPxPageControl;
            var listBox = pageControl.FindControl("DistributorAccessListBox") as ASPxListBox;
            return listBox.SelectedValues.Cast<int>();
        }

        protected List<string> GetSelectedUserRoles()
        {
            var pageControl = UserGridView.FindEditFormTemplateControl("UserPageControl") as ASPxPageControl;
            var panel = pageControl.FindControl("RolesDualListBoxCallbackPanel") as ASPxCallbackPanel;
            var dualListBox = panel.FindControl(@"RolesDualListBox") as DualListBoxControl;
            var selectedRoles =
                dualListBox.SelectedItems.ConvertAll<string>(new Converter<ListEditItem, string>(c => c.Value.ToString()));

            return selectedRoles;

        }



        private string GetASPxTextBoxValue(string controlName)
        {
            var pageControl = UserGridView.FindEditFormTemplateControl("UserPageControl") as ASPxPageControl;
            var textBox = pageControl.FindControl(controlName) as ASPxTextBox;
            return textBox.Text;
        }

        private string GetASPxMemoValue(string controlName)
        {
            var pageControl = UserGridView.FindEditFormTemplateControl("UserPageControl") as ASPxPageControl;
            var memo = pageControl.FindControl(controlName) as ASPxMemo;
            return memo.Text;
        }

        private DateTime? GetASPxDateEditValue(string controlName)
        {
            var pageControl = UserGridView.FindEditFormTemplateControl("UserPageControl") as ASPxPageControl;
            var dateEdit = pageControl.FindControl(controlName) as ASPxDateEdit;
            return dateEdit.Value as DateTime?;
        }

        private object GetASPxRadioButtonListValue(string controlName)
        {
            var pageControl = UserGridView.FindEditFormTemplateControl("UserPageControl") as ASPxPageControl;
            var radioButtonList = pageControl.FindControl(controlName) as ASPxRadioButtonList;
            return radioButtonList.Value;
        }

        private ListEditItem GetASPxComboBoxSelectedItem(string controlName)
        {
            var pageControl = UserGridView.FindEditFormTemplateControl("UserPageControl") as ASPxPageControl;
            var comboBox = pageControl.FindControl(controlName) as ASPxComboBox;
            return comboBox.SelectedItem;
        }

        private void SetASPxComboBoxSelectedItem(string controlName, object value)
        {
            var pageControl = UserGridView.FindEditFormTemplateControl("UserPageControl") as ASPxPageControl;
            var comboBox = pageControl.FindControl(controlName) as ASPxComboBox;
            var item = comboBox.Items.FindByValue(value);
            if (item != null)
            {
                item.Selected = true;
            }
        }

        #endregion

        protected void UserGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var editingUser = (Page as BasePage).CurrentContext.UserIdentifier;
            var personalEmail = GetUserPersonalEmail1().Trim();

            if (string.IsNullOrEmpty(personalEmail))
            {
                throw new Exception(Resources.Exceptions.UsernameIsARequiredValue);
            }

            var dataContext = new EChallengeDataContext();
            var tempUser = Membership.GetUser(personalEmail);

            if (tempUser != null)
            {
                var tempUserId = (Guid)tempUser.ProviderUserKey;
                if (EditingUserId != tempUserId)
                {
                    throw new Exception(Resources.Exceptions.UsernameAlreadyExists);
                }
            }

            var userFirstName = GetUserFirstName();
            var userLastName = GetUserLastName();


            var lastModifiedOn = (DateTime)e.OldValues["LastModifiedOnDateTime"];

            using (var transaction = dataContext.Database.BeginTransaction())
            {
                try
                {           
                    var oldUserName = Membership.GetUser(new Guid(e.Keys[0].ToString())).UserName;
                    var membershipUser = Membership.GetUser(oldUserName);

                    var userId = new Guid(membershipUser.ProviderUserKey.ToString());

                    var user = (from u in dataContext.Users
                                where u.UserId == userId
                                select u).FirstOrDefault();

                    var currentDateTime = DateTime.Now;

                    if (lastModifiedOn != user.LastModifiedOnDateTime)
                    {
                        throw new Exception(Resources.Exceptions.ConcurrencyException);
                    }


                    //set fields             
                    if (string.Compare(oldUserName, personalEmail, true) != 0)
                    {
                        user.UserName = personalEmail;
                        user.LoweredUserName = personalEmail.ToLower();
                    }

                    user.ClientBusinessUnit = GetClientBusinessUnit();
                    user.ClientCostCentre = GetClientCostCentre();
                    user.ClientDivision = GetClientDivision();
                    user.ClientJobTitle = GetClientJobTitle();
                    user.ClientLocation = GetClientLocation();
                    user.ClientSubsidiaryCompanyName = GetClientSubsidiaryCompanyName();
                    user.ClientUniqueSalaryNo = GetClientUniqueSalaryNo();
                    user.ClientUserGroup = GetClientUserGroup();
                    user.IsADeletedRow = false;
                    user.IsAnAuditRow = false;
                    user.LastModifiedOnDateTime = currentDateTime;
                    user.UserID_LastModifiedBy = editingUser;
                    user.UserDateOfBirth = GetUserDateOfBirth();
                    user.UserFirstName = userFirstName;
                    user.UserGender = GetUserGender();
                    user.UserID_LastModifiedBy = userId;
                    user.UserLastName = userLastName;
                    user.UserPersonalEmail1 = personalEmail;
                    user.UserPersonalMobile = GetUserPersonalMobile();
                    user.UserWorkEmail = GetUserWorkEmail();
                    user.UserWorkMobile = GetUserWorkMobile();
                    user.UserWorkNotes = GetUserWorkNotes();
                    user.UserWorkTel1 = GetUserWorkTel1();
                    user.UserWorkTel2 = GetUserWorkTel2();
                    user.PlayerAvatarData = GetAvatarData();
                    user.PlayerAvatarName = GetAvatarName();
                    var clientId = GetClientSelectedValue();

                    if (clientId.HasValue)
                    {
                        if (user.Clients.All(c => c.ClientID != clientId.Value))
                        {
                            dataContext.User2Clients.Add(
                                new User2Client()
                                    {
                                        ClientID = clientId.Value,
                                        CreatedOnDateTime = currentDateTime,
                                        LastModifiedDateTime = currentDateTime,
                                        UserID = user.UserId,
                                        UserID_CreatedBy = editingUser,
                                        UserID_LastModifiedBy = editingUser
                                    }
                                );
                        }

                        var clientsToRemove = user.Clients.Where(c => c.ClientID != clientId.Value).ToList();

                        foreach (var user2Client in clientsToRemove)
                        {
                            user.Clients.Remove(user2Client);
                            dataContext.User2Clients.Remove(user2Client);
                        }
                    }
                    else
                    {
                        var clientMappings = user.Clients.ToList();
                        foreach (var clientMapping in clientMappings)
                        {
                            user.Clients.Remove(clientMapping);
                            dataContext.User2Clients.Remove(clientMapping);
                        }
                    }



                    var currentDistributors = user.Distributors.ToList();
                    var selectedDistributors = GetSelectedDistributors();

                    //delete distributors that are no longer selected
                    foreach (
                        var distributor in
                            currentDistributors.Where(
                                distributor => !selectedDistributors.Contains(distributor.DistributorID)))
                    {
                        user.Distributors.Remove(distributor);
                        dataContext.User2Distributors.Remove(distributor);
                    }

                    //now add new distributors
                    foreach (var distributor in selectedDistributors)
                    {
                        var exists = from d in user.Distributors
                                     where d.DistributorID == distributor
                                     select d;

                        if (!exists.Any())
                        {
                            dataContext.User2Distributors.Add(
                                new User2Distributor()
                                {
                                    CreatedOnDateTime = currentDateTime,
                                    DistributorID = distributor,
                                    LastModifiedDateTime = currentDateTime,
                                    UserID = user.UserId,
                                    UserID_CreatedBy = editingUser,
                                    UserID_LastModifiedBy = editingUser
                                }
                                );
                        }
                    }

                    membershipUser.Email = user.UserPersonalEmail1;
                    Membership.UpdateUser(membershipUser);

                    var currentRoles = Roles.GetRolesForUser(oldUserName);
                    var selectedRoles = GetSelectedUserRoles();

                    //first delete roles
                    foreach (var role in currentRoles.Where(role => !selectedRoles.Contains(role) && string.Compare(role,GlobalConstants.USERS_ROLE, StringComparison.OrdinalIgnoreCase) != 0))
                    {
                        Roles.RemoveUserFromRole(oldUserName, role);
                    }

                    //now add new roles
                    foreach (var role in selectedRoles.Where(role => !Roles.IsUserInRole(oldUserName, role)))
                    {
                        Roles.AddUserToRole(oldUserName, role);
                    }

                    dataContext.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }

            UserGridView.CancelEdit();
            UserGridView.DataBind();
            SelectedRoles = null;
            e.Cancel = true;
        }

        protected void UserGridView_CancelRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            SelectedRoles = null;
            EditingUserId = null;

            UserGridView.DataBind();
        }

        protected void DistributorDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var dataContext = new EChallengeDataContext();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = from d in dataContext.Distributors
                           orderby d.DistributorName
                           select d;
            }
            else
            {
                e.Result = from d in dataContext.Distributors
                           from u in d.User2Distributors
                           where u.UserID == userId
                           select d;
            }
        }

        protected void DistributorAccessListBox_DataBound(object sender, EventArgs e)
        {
            var listBox = sender as ASPxListBox;      

            if (EditingUserId == null)
            {
                return;
            }

            var userId = EditingUserId;

            var dataContext = new EChallengeDataContext();
            var user = (from u in dataContext.Users
                        where u.UserId == userId
                        select u).FirstOrDefault();

            foreach (var mapping in user.Distributors)
            {
                var item = listBox.Items.FindByValue(mapping.DistributorID);
                if (item != null)
                {
                    item.Selected = true;
                }
            }
        }

        protected void UserDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var dataContext = new EChallengeDataContext();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = from u in dataContext.Users
                           where u.UserId != userId
                           select u;
            }
            else
            {
                var distributors = from d in dataContext.User2Distributors
                                   where d.UserID == userId
                                   select d.DistributorID;

                e.Result = (from u in dataContext.Users
                            join d in dataContext.User2Distributors on u.UserId equals d.UserID
                            where u.UserId != userId && distributors.Contains(d.DistributorID)
                            select u).Distinct();
            }
        }

        protected void ClientDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = (from c in dataContext.Clients
                            select c).Distinct();
            }
            else
            {
                e.Result = (from c in dataContext.Clients
                            from d in c.Client2Distributors
                            join u in dataContext.User2Distributors on d.DistributorID equals u.DistributorID
                            where u.UserID == userId
                            select c).Distinct();
            }
        }

        protected void CompanyComboBox_DataBound(object sender, EventArgs e)
        {
            var comboBox = sender as ASPxComboBox;

            if (EditingUserId == null)
            {
                comboBox.ClientEnabled = true;
                return;
            }

            var userId = EditingUserId;
            var dataContext = new EChallengeDataContext();

            var user = dataContext.Users
                .Where(u => u.UserId == userId)
                .FirstOrDefault();

            var mapping = user.Clients.FirstOrDefault();

            if (mapping != null)
            {
                SetASPxComboBoxSelectedItem(@"CompanyComboBox", mapping.ClientID);
            }

            comboBox.ClientEnabled = false;
        }

        protected void DeleteUserCallback_OnCallback(object source, CallbackEventArgs e)
        {
            var userIdString = e.Parameter;
            var userId = new Guid(userIdString);
            e.Result = null;

            using (var dataContext = new EChallengeDataContext())
            {
                var userExists =
                    dataContext.Users
                        .Any(u => u.UserId == userId);
                if (userExists)
                {
                    var loggedInUserId = (Page as BasePage).CurrentContext.UserIdentifier;
                    using (var transaction = dataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            dataContext.upDeleteUser(userId, loggedInUserId);
                            dataContext.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
        }

        protected void DeleteButton_OnInit(object sender, EventArgs e)
        {
            var link = sender as ASPxHyperLink;

            using (var container = link.NamingContainer as GridViewDataItemTemplateContainer)
            {
                link.ClientSideEvents.Click = "function (s,e) { DeleteButton_Click(s,'" + container.KeyValue + "');}";
            }
        }
    }
}