﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class CardClassificationManagementControl : System.Web.UI.UserControl
    {
        private const string EDITING_PRODUCT_ID_KEY = "EditingProductID";

        public class CardClassificationGroupLanguage
        {
            public int CardClassificationGroupID { get; set; }
            public int ProductId { get; set; }
            public string CardClassificationGroupImageURL { get; set; }
            public bool IsAMetaTagClassificationGroup { get; set; }
            public int? CardClassificationGroupID_Parent { get; set; }
            public int LanguageId { get; set; }
            public string CardClassificationGroup { get; set; }
            public DateTime? LanguageLastModifiedOnDateTime { get; set; }
            public DateTime? ParentLastModifiedOnDateTime { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                EditingProductId = -1;
            }
        }

        protected void CardClassificationGroupDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var results = GetCardClassifcationGroups(false);

            if (results == null)
            {
                e.Result = new List<CardClassificationGroupLanguage>().AsQueryable();
            }
            else
            {
                e.Result = results;
            }

        }

        protected void CardClassificationMetaTagDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var results = GetCardClassifcationGroups(true);

            if (results == null)
            {
                e.Result = new List<CardClassificationGroupLanguage>().AsQueryable();
            }
            else
            {
                e.Result = results;
            }
        }

        private IQueryable<CardClassificationGroupLanguage> GetCardClassifcationGroups(bool isMetaTagClassificationGroup)
        {           
            var productId = EditingProductId;

            var dataContext = new EChallengeDataContext();

            var lcid = (Page as BasePage).CurrentContext.Culture;
            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            return  from c in dataContext.CardClassificationGroups
                      from l in c.CardClassificationGroup_Languages
                      where l.LanguageID == language.LanguageID && c.ProductID == productId
                            && c.IsAMetaTagClassificationGroup == isMetaTagClassificationGroup
                      select new CardClassificationGroupLanguage()
                                 {
                                     CardClassificationGroup = l.CardClassificationGroup,
                                     CardClassificationGroupID = c.CardClassificationGroupID,
                                     CardClassificationGroupImageURL = c.CardClassificationGroupImageURL,
                                     CardClassificationGroupID_Parent = c.CardClassificationGroupID_Parent,
                                     IsAMetaTagClassificationGroup = c.IsAMetaTagClassificationGroup,
                                     LanguageId = l.LanguageID,
                                     ProductId = c.ProductID,
                                     LanguageLastModifiedOnDateTime = l.LastModifiedOnDateTime,
                                     ParentLastModifiedOnDateTime = c.LastModifiedOnDateTime
                                 };
        }

        int EditingProductId
        {
            get
            {
                if (Session[EDITING_PRODUCT_ID_KEY] == null)
                {
                    return -1;
                }
                else
                {
                    return (int)Session[EDITING_PRODUCT_ID_KEY];
                }
            }
            set { Session[EDITING_PRODUCT_ID_KEY] = value; }

        }

        protected void ProductDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = (from p in dataContext.Products
                            from currentLanguageProduct in dataContext.Product_Languages
                                .Where(d => d.ProductID == p.ProductID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            from pl in p.Product_Languages
                                .Where(d => d.ProductID == p.ProductID && d.LanguageID == p.DefaultLanguageID)
                            select new
                                       {
                                           p.ProductID,                                           
                                           ProductName =
                                currentLanguageProduct == null ? pl.ProductName : currentLanguageProduct.ProductName
                                       })
                    .OrderBy(r => r.ProductName);
            }
            else
            {
                e.Result = (from p in dataContext.Products
                            from pd in p.Product2Distributors
                            from currentLanguageProduct in dataContext.Product_Languages
                                .Where(d => d.ProductID == p.ProductID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            from pl in p.Product_Languages
                                .Where(d => d.ProductID == p.ProductID && d.LanguageID == p.DefaultLanguageID)
                            let distributors = dataContext.User2Distributors
                                .Where(d => d.UserID == userId)
                                .Select(d => d.DistributorID)
                            where distributors.Contains(pd.DistributorID)
                            select new
                                       {
                                           p.ProductID,                                    
                                           ProductName =
                                currentLanguageProduct == null ? pl.ProductName : currentLanguageProduct.ProductName
                                       })
                    .OrderBy(r => r.ProductName);
            }

        }

        private void Insert(string cardClassificationGroupName, bool isMetaTagClassificationGroup, int? cardClassificationGroupParentId)
        {
            var productId = EditingProductId;

            using (var dataContext = new EChallengeDataContext())
            {

                var currentDateTime = DateTime.Now;
                var userId = (Page as BasePage).CurrentContext.UserIdentifier;
                var lcid = (Page as BasePage).CurrentContext.Culture;

                var language = (from l in dataContext.Languages
                                where l.LCID == lcid
                                select l).FirstOrDefault();

                var cardClassificationGroup = new CardClassificationGroup()
                                                  {
                                                      CreatedOnDateTime = currentDateTime,
                                                      IsADeletedRow = false,
                                                      IsAnAuditRow = false,
                                                      LastModifiedOnDateTime = currentDateTime,
                                                      UserID_CreatedBy = userId,
                                                      UserID_LastModifiedBy = userId,
                                                      CardClassificationGroupImageURL = string.Empty,
                                                      IsAMetaTagClassificationGroup = isMetaTagClassificationGroup,
                                                      ProductID = productId,
                                                      CardClassificationGroupID_Parent = cardClassificationGroupParentId
                                                  };

                var cgLanguage = new CardClassificationGroup_Language()
                                     {
                                         LanguageID = language.LanguageID,
                                         UserID_CreatedBy = userId,
                                         UserID_LastModifiedBy = userId,
                                         CardClassificationGroup = cardClassificationGroupName.Trim(),
                                         LastModifiedOnDateTime = currentDateTime,
                                         CreatedOnDateTime = currentDateTime
                                     };


                cardClassificationGroup.CardClassificationGroup_Languages.Add(cgLanguage);

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.CardClassificationGroups.Add(cardClassificationGroup);
                        dataContext.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }

        private void Update(ASPxTreeList treeList, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e, int cardClassificationGroupId)
        {            
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var currentDateTime = DateTime.Now;
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var node = treeList.FindNodeByKeyValue(cardClassificationGroupId.ToString());
            var languageLastModifiedOn = (DateTime)node.GetValue("LanguageLastModifiedOnDateTime");
            var parentLastModifiedOn = (DateTime)node.GetValue("ParentLastModifiedOnDateTime");


            using (var dataContext = new EChallengeDataContext())
            {

                var cardClassificationGroup = (from c in dataContext.CardClassificationGroups
                                               from cl in c.CardClassificationGroup_Languages
                                               join l in dataContext.Languages on cl.LanguageID equals l.LanguageID
                                               where
                                                   c.CardClassificationGroupID == cardClassificationGroupId &&
                                                   l.LCID == lcid
                                               select cl).FirstOrDefault();
                
                if(languageLastModifiedOn != cardClassificationGroup.LastModifiedOnDateTime || 
                    parentLastModifiedOn != cardClassificationGroup.CardClassificationGroup1.LastModifiedOnDateTime)                
                {
                    throw new Exception(Resources.Exceptions.ConcurrencyException);
                }

                if (e.NewValues["CardClassificationGroup"] != null)
                {
                    cardClassificationGroup.CardClassificationGroup =
                        e.NewValues["CardClassificationGroup"].ToString().Trim();
                }

                if (e.NewValues["CardClassificationGroupID_Parent"] != null)
                {
                    int? parentId = null;
                    if (e.NewValues["CardClassificationGroupID_Parent"] != null &&
                        !string.IsNullOrEmpty(e.NewValues["CardClassificationGroupID_Parent"].ToString()))
                    {
                        parentId = Convert.ToInt32(e.NewValues["CardClassificationGroupID_Parent"].ToString());
                    }
                    cardClassificationGroup.CardClassificationGroup1.CardClassificationGroupID_Parent = parentId;
                }

                cardClassificationGroup.UserID_LastModifiedBy = userId;
                cardClassificationGroup.LastModifiedOnDateTime = currentDateTime;

                cardClassificationGroup.CardClassificationGroup1.LastModifiedOnDateTime = currentDateTime;
                cardClassificationGroup.CardClassificationGroup1.UserID_LastModifiedBy = userId;

                if (e.NewValues["CardClassificationGroupImageURL"] != null)
                {
                    cardClassificationGroup.CardClassificationGroup1.CardClassificationGroupImageURL =
                        e.NewValues["CardClassificationGroupImageURL"].ToString().Trim();
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }

        protected void CardClassificationTreeList_NodeInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var treeList = sender as ASPxTreeList;

            var groupName = Convert.ToString(e.NewValues["CardClassificationGroup"]);

            int? parentId = null;
            if (e.NewValues["CardClassificationGroupID_Parent"] != null && !string.IsNullOrEmpty(e.NewValues["CardClassificationGroupID_Parent"].ToString()))
            {
                parentId = Convert.ToInt32(e.NewValues["CardClassificationGroupID_Parent"].ToString());
            }

            try
            {
                Insert(groupName, false, parentId);
                treeList.CancelEdit();
                e.Cancel = true;
                treeList.DataBind();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void CardClassificationTreeList_NodeUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var treeList = sender as ASPxTreeList;

            var cardClassificationGroupId = Convert.ToInt32(Session["EditCardClassificationGroupID"]);

            try
            {
                Update(treeList, e, cardClassificationGroupId);
                treeList.CancelEdit();
                e.Cancel = true;
                treeList.DataBind();
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        protected void CardClassificationTreeList_StartNodeEditing(object sender, TreeListNodeEditingEventArgs e)
        {
            Session["EditCardClassificationGroupID"] = Convert.ToInt32(e.NodeKey);
        }

        protected void CardClassificationTreeList_CancelNodeEditing(object sender, TreeListNodeEditingEventArgs e)
        {
            Session.Remove("EditCardClassificationGroupID");
        }

        protected void CardMetaTagsTreeList_CancelNodeEditing(object sender, TreeListNodeEditingEventArgs e)
        {
            Session.Remove("EditMetaCardClassificationGroupID");
        }

        protected void CardMetaTagsTreeList_StartNodeEditing(object sender, TreeListNodeEditingEventArgs e)
        {
            Session["EditMetaCardClassificationGroupID"] = Convert.ToInt32(e.NodeKey);
        }

        protected void CardMetaTagsTreeList_NodeInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var treeList = sender as ASPxTreeList;

            var groupName = Convert.ToString(e.NewValues["CardClassificationGroup"]).Trim();

            int? parentId = null;
            if (e.NewValues["CardClassificationGroupID_Parent"] != null && !string.IsNullOrEmpty(e.NewValues["CardClassificationGroupID_Parent"].ToString()))
            {
                parentId = Convert.ToInt32(e.NewValues["CardClassificationGroupID_Parent"].ToString());
            }

            try
            {
                Insert(groupName, true, parentId);
                treeList.CancelEdit();
                e.Cancel = true;
                treeList.DataBind();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }

        protected void CardMetaTagsTreeList_NodeUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var treeList = sender as ASPxTreeList;

            var cardClassificationGroupId = Convert.ToInt32(Session["EditMetaCardClassificationGroupID"]);

            try
            {
                Update(treeList, e, cardClassificationGroupId);
                treeList.CancelEdit();
                e.Cancel = true;
                treeList.DataBind();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }

        protected void CardClassificationTreeList_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            var treeList = sender as ASPxTreeList;
            SetMaximumNodeLevel(treeList, e);
        }

        protected void CardMetaTagsTreeList_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            var treeList = sender as ASPxTreeList;
            SetMaximumNodeLevel(treeList, e);
        }

        void SetMaximumNodeLevel(ASPxTreeList treeList, TreeListCommandColumnButtonEventArgs e)
        {
            if (e.NodeKey != null)
            {
                var nodeLevel = treeList.FindNodeByKeyValue(e.NodeKey).Level;

                if (nodeLevel == 2)
                {
                    if (e.ButtonType == TreeListCommandColumnButtonType.New)
                    {
                        e.Visible = DefaultBoolean.False;
                    }
                }
            }
        }

        protected void CardClassificationTreeList_Init(object sender, EventArgs e)
        {
            EditingProductId = (int) GetMasterRowKeyValue(sender as ASPxTreeList);
        }

        protected void CardMetaTagsTreeList_Init(object sender, EventArgs e)
        {
            EditingProductId = (int)GetMasterRowKeyValue(sender as ASPxTreeList);
        }

        protected object GetMasterRowKeyValue(ASPxTreeList treeList)
        {
            GridViewBaseRowTemplateContainer container = null;
            Control control = treeList;
            while(control.Parent != null)
            {
                container = control.Parent as GridViewBaseRowTemplateContainer;
                if(container != null)
                {
                    break;
                }
                control = control.Parent;
            }
            return container.KeyValue;
        }
    }
}