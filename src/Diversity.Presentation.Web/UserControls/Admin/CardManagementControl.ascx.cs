﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using DevExpress.Data.PivotGrid;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using Diversity.Common;
using Diversity.Common.Enumerations;
using Diversity.DataModel.SqlRepository;
using Diversity.Services.Business;
using Diversity.Services.Business.Implementation;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class CardManagementControl : System.Web.UI.UserControl
    {
        private const string CARD_REFERENCE_MATERIAL_ID_KEY = "3B1223FF-4809-4502-87CB-E85DC1C61526";

        #region Classes

        public class CardManagement
        {
            public int CardID { get; set; }
            public Guid CreatedBy { get; set; }
            public Guid LastModifiedBy { get; set; }
            public DateTime CreatedOn { get; set; }
            public DateTime LastModifiedOn { get; set; }
            public int InternalCardTypeID { get; set; }
            public int? CardID_LCDCategoryCard { get; set; }
            public int PrimaryCardClassificationGroupID { get; set; }
            public int? ClientIDForLibrary { get; set; }
            public int ProductID { get; set; }
            public int? QuestionCardID { get; set; }
            public string CardShortcutCode { get; set; }
            public bool IsForClientLibraryElsePublicLibrary { get; set; }
            public bool IgnoreAnyTimersOnThisCard { get; set; }
            public bool IsStillInBeta { get; set; }
            public bool IsATimedCard { get; set; }
            public int CardMaxTimeToAnswerInSeconds { get; set; }
            public bool IsApplicableForOnlineUse { get; set; }
            public bool IsApplicableForDesktopUse { get; set; }
            public string FacilitatorSlideNotes { get; set; }
            public bool UseCardTimeNotGameTime { get; set; }
            public int LanguageID { get; set; }
            public string CardName { get; set; }
            public string CardTitle { get; set; }
            public string CardDescription { get; set; }
        }

        public class CardClassification
        {
            public int CardClassificationGroupID { get; set; }
            public int ProductId { get; set; }
            public string CardClassificationGroupImageURL { get; set; }
            public bool IsAMetaTagClassificationGroup { get; set; }
            public int? CardClassificationGroupID_Parent { get; set; }
            public int LanguageId { get; set; }
            public string CardClassificationGroup { get; set; }
            public bool IsPrimaryCard { get; set; }
            public bool IsClassificationSelected { get; set; }
        }

        public class CardLanguageReferenceMaterial
        {
            public int CardReferenceMaterialID { get; set; }
            public int CardReferenceMaterialLanguageID { get; set; }
            public string CardReferenceMaterialUrl { get; set; }
            public string CardReferenceMaterialDescription { get; set; }
            public int DefaultLanguageID { get; set; }
            public int LanguageID { get; set; }
        }
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (IsPostBack)
            {
                SaveState();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);
            if (!CurrentCardType.HasValue)
            {
                throw new Exception(Resources.Exceptions.UseMenuToCreateUpdateDeleteCard);
            }
        }


        protected override void OnInit(EventArgs e)
        {
            ProductComboBox.DataBind();

            base.OnInit(e);

            if (Request.UrlReferrer == null)
            {
                Response.Redirect(ResolveUrl(@"~/Default.aspx"));
            }

            if (!Page.IsPostBack)
            {
                EditingCardReferenceMaterialID = null;
                CardLanguageReferenceMaterialData = null;
                HiddenNodeList = new List<string>();
                PrimaryNodeKey = null;
                RemovedNodesTemp = new List<string>();
                ChangedPrimaryCardTemp = null;

                PopulateContextuallySimilarAssociatedCardData();
                PopulateLeadingQuestionAssociatedCardData();
                PopulateAuditStatementAssociatedCardData();
                PopulateAuditStatementImageAssociatedCardData();

                if (CurrentCardId.HasValue)
                {
                    BindControls();

                    CardLanguageReferenceMaterialData = GetCardReferenceMaterialData();
                }
                else
                {
                    var questionVisible = (QuestionMultimediaTypeComboBox.SelectedItem == null ||
                                   string.Compare(QuestionMultimediaTypeComboBox.SelectedItem.Value.ToString(), "wistia", true) == 0);

                    QuestionMultimediaUrlTextBox.ClientVisible = questionVisible;
                    QuestionMultimediaBrowseButton.ClientVisible = !questionVisible;

                    var answerVisible = (AnswerMultimediaTypeComboBox.SelectedItem == null ||
                                         string.Compare(AnswerMultimediaTypeComboBox.SelectedItem.Value.ToString(), "wistia", true) ==
                                         0);

                    AnswerMultimediaUrlTextBox.ClientVisible = answerVisible;
                    AnswerMultimediaBrowseButton.ClientVisible = !answerVisible;

                    CardLanguageReferenceMaterialData = new List<CardLanguageReferenceMaterial>();

                }
            }

            Page.Title = GetPageTitle();
        }

        public int? CurrentCardId
        {
            get
            {
                if (Request.QueryString["id"] == null)
                {
                    return null;
                }
                return Convert.ToInt32(Request.QueryString["id"]);
            }
        }

        int? EditingCardReferenceMaterialID
        {
            get
            {
                if (Session[CARD_REFERENCE_MATERIAL_ID_KEY] == null)
                {
                    return null;
                }
                else
                {
                    return (int)Session[CARD_REFERENCE_MATERIAL_ID_KEY];
                }
            }
            set { Session[CARD_REFERENCE_MATERIAL_ID_KEY] = value; }
        }

        public CardType? CurrentCardType
        {
            get
            {
                if (Request.QueryString["type"] == null)
                {
                    return null;
                }

                int cardTypeId;
                CardType type;
                if (Int32.TryParse(Request.QueryString["type"], out cardTypeId))
                {
                    type = (CardType)cardTypeId;
                }
                else
                {
                    type = (CardType)Enum.Parse(typeof(CardType), Request.QueryString["type"], true);
                }

                return type;
            }
        }

        public List<CardLanguageReferenceMaterial> CardLanguageReferenceMaterialData
        {
            get
            {
                return Session["CD7B60A3-D2FF-4DC4-900E-7CB87AA01501"] == null ? null : Session["CD7B60A3-D2FF-4DC4-900E-7CB87AA01501"] as List<CardLanguageReferenceMaterial>;
            }

            set
            {
                Session["CD7B60A3-D2FF-4DC4-900E-7CB87AA01501"] = value;
            }
        }

        string GetPageTitle()
        {
            if (!CurrentCardType.HasValue)
            {
                return Page.Title;
            }

            var dataContext = new EChallengeDataContext();

            var cardTypeShortCode = Enum.GetName(typeof(CardType), CurrentCardType.Value);

            var cardType = dataContext.Internal_CardTypes
                .FirstOrDefault(i => i.Internal_CardTypeShortCode == cardTypeShortCode);
            return string.Format(@"{0} - {1}", Page.Title, cardType.Internal_CardTypeName);
        }

        List<CardLanguageReferenceMaterial> GetCardReferenceMaterialData()
        {
            var dataContext = new EChallengeDataContext();
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();
            return
            (from c in dataContext.CardReferenceMaterials
             from currentCardReferenceMaterial in dataContext.CardReferenceMaterial_Languages
                 .Where(x => x.CardReferenceMaterialID == c.CardReferenceMaterialID && x.LanguageID == language.LanguageID)
                 .DefaultIfEmpty()
             from defaultCardReferenceMaterial in c.CardReferenceMaterial_Languages
                 .Where(x => x.CardReferenceMaterialID == c.CardReferenceMaterialID && x.LanguageID == c.DefaultLanguageID)
             where c.CardID == CurrentCardId
             select new CardLanguageReferenceMaterial()
             {
                 CardReferenceMaterialDescription = currentCardReferenceMaterial == null ? defaultCardReferenceMaterial.CardReferenceMaterialDescription : currentCardReferenceMaterial.CardReferenceMaterialDescription,
                 CardReferenceMaterialID = c.CardReferenceMaterialID,
                 CardReferenceMaterialLanguageID = currentCardReferenceMaterial == null ? defaultCardReferenceMaterial.CardReferenceMaterial_LanguageID : currentCardReferenceMaterial.CardReferenceMaterial_LanguageID,
                 CardReferenceMaterialUrl = currentCardReferenceMaterial == null ? defaultCardReferenceMaterial.CardReferenceMaterialUrl : currentCardReferenceMaterial.CardReferenceMaterialUrl,
                 DefaultLanguageID = c.DefaultLanguageID,
                 LanguageID = currentCardReferenceMaterial == null ? defaultCardReferenceMaterial.LanguageID : currentCardReferenceMaterial.LanguageID
             }).Distinct().ToList();
        }

        protected void ProductDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {

                e.Result = from p in dataContext.Products
                           from pl in p.Product_Languages
                           join l in dataContext.Languages on pl.LanguageID equals l.LanguageID
                           where l.LCID == lcid
                           orderby pl.ProductName
                           select new { p.ProductID, pl.ProductName };
            }
            else
            {
                var distributors = from d in dataContext.User2Distributors
                                   where d.UserID == userId
                                   select d.DistributorID;

                e.Result = from p in dataContext.Products
                           from pl in p.Product_Languages
                           join pd in dataContext.Product2Distributors on p.ProductID equals pd.ProductID
                           join l in dataContext.Languages on pl.LanguageID equals l.LanguageID
                           where l.LCID == lcid && distributors.Contains(pd.DistributorID)
                           orderby pl.ProductName
                           select new { p.ProductID, pl.ProductName };
            }
        }

        protected void CardReferenceMaterialDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = CardLanguageReferenceMaterialData;
        }

        protected void CardLanguageReferenceMaterialDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            e.Result = from m in CardLanguageReferenceMaterialData
                       where m.CardReferenceMaterialID == EditingCardReferenceMaterialID
                             && m.LanguageID == language.LanguageID
                       select m;
        }


        protected void ClientDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = from uc in dataContext.User2Clients
                           orderby uc.Client.ClientName
                           select uc.Client;
            }
            else
            {
                e.Result = from c in dataContext.Clients
                           from uc in c.User2Clients
                           where uc.UserID == userId
                           orderby c.ClientName
                           select c;
            }
        }

        protected void CardClassificationDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var result = GetCardClassifcationGroups(false);
            var list = new List<CardClassification>();
            if (result != null)
            {
                list = result.ToList();
            }

            e.Result = list;
        }

        protected void CardMetaTagDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var result = GetCardClassifcationGroups(true);
            var list = new List<CardClassification>();
            if (result != null)
            {
                list = result.ToList();
            }

            e.Result = list;
        }


        private IQueryable<CardClassification> GetCardClassifcationGroups(bool isMetaTagClassificationGroup)
        {
            IQueryable<CardClassification> results = null;

            if (ProductComboBox.SelectedItem != null)
            {
                var productId = Convert.ToInt32(ProductComboBox.SelectedItem.Value);


                if (CurrentCardId.HasValue)
                {
                    results = GetCardClassifications(CurrentCardId, productId, isMetaTagClassificationGroup);

                }
                else
                {
                    results = GetCardClassifications(-1, productId, isMetaTagClassificationGroup);
                }
            }

            return results;
        }

        IQueryable<CardClassification> GetCardClassifications(int? cardId, int productId, bool isMetaTagClassificationGroup)
        {
            var dataContext = new EChallengeDataContext();

            var lcid = (Page as BasePage).CurrentContext.Culture;
            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var results = (from c in dataContext.CardClassificationGroups
                           from l in c.CardClassificationGroup_Languages
                           from cc in dataContext.Card2CardClassifications
                               .Where(x => x.CardClassificationGroupID == c.CardClassificationGroupID && x.CardID == cardId)
                               .DefaultIfEmpty()
                           where l.LanguageID == language.LanguageID && c.ProductID == productId
                                 && c.IsAMetaTagClassificationGroup == isMetaTagClassificationGroup
                           select new CardClassification()
                                      {
                                          CardClassificationGroup = l.CardClassificationGroup,
                                          CardClassificationGroupID = c.CardClassificationGroupID,
                                          CardClassificationGroupImageURL = c.CardClassificationGroupImageURL,
                                          CardClassificationGroupID_Parent = c.CardClassificationGroupID_Parent,
                                          IsAMetaTagClassificationGroup = c.IsAMetaTagClassificationGroup,
                                          LanguageId = l.LanguageID,
                                          ProductId = c.ProductID,
                                          IsPrimaryCard = cc.CardClassificationGroup == null ? false : cc.IsAPrimaryCard,
                                          IsClassificationSelected = cc.CardClassificationGroup == null ? false : cc.CardID == cardId
                                      }
                          ).Distinct();

            return results;
        }

        protected void CardMetaTagTreeList_DataBound(object sender, EventArgs e)
        {
            var treeList = sender as ASPxTreeList;
            var nodes = treeList.GetAllNodes();

            foreach (var node in nodes)
            {
                var cardClassification = node.DataItem as CardClassification;

                if (cardClassification.IsClassificationSelected)
                {
                    node.Selected = true;
                   
                    if(node.ParentNode != null)
                    {
                        node.ParentNode.Expanded = true;
                    }
                    node.Expanded = true;
                }

            }
            SetNodeSelectionSettings(treeList);
        }

        protected void CardClassificationTreeList_DataBound(object sender, EventArgs e)
        {
            var treeList = sender as ASPxTreeList;

            var nodes = treeList.GetAllNodes();

            foreach (var node in nodes)
            {
                var cardClassification = node.DataItem as CardClassification;

                if (cardClassification.IsPrimaryCard)
                {
                    if (string.IsNullOrEmpty(ChangedPrimaryCardTemp))
                    {
                        PrimaryNodeKey = node.Key;
                    }
                    else
                    {
                        PrimaryNodeKey = ChangedPrimaryCardTemp;
                    }
                    
                    if(node.ParentNode != null)
                    {
                        node.ParentNode.Expanded = true;
                    }
                    node.Expanded = true;                    
                }

                if (cardClassification.IsClassificationSelected)
                {
                    if (RemovedNodesTemp.Contains(node.Key))
                    {
                        continue;
                    }

                    if (!HiddenNodeList.Contains(node.Key))
                    {                        
                        HiddenNodeList.Add(node.Key);

                        if (RemovedNodesTemp.Contains(node.Key))
                        {
                            RemovedNodesTemp.Remove(node.Key);
                        }
                    }

                    if (node.ParentNode != null)
                    {
                        node.ParentNode.Expanded = true;
                    }
                    node.Expanded = true;
                }
            }

        }

        void SetNodeSelectionSettings(ASPxTreeList treeList)
        {
            var iterator = treeList.CreateNodeIterator();

            TreeListNode node;
            while (true)
            {
                node = iterator.GetNext();

                if (node == null)
                {
                    break;
                }

                if (node.Level <= 1)
                {
                    node.AllowSelect = false;
                }
            }
        }

        protected void UpdateButton_Click(object sender, EventArgs e)
        {
            if (CurrentCardId.HasValue)
            {
                SaveState();
                Update();
            }
            else
            {
                SaveState();
                Insert();
            }
        }


        void Insert()
        {
            var cardId = 0;

            using (var dataContext = new EChallengeDataContext())
            {
                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        var lcid = (Page as BasePage).CurrentContext.Culture;

                        var language = (from l in dataContext.Languages
                                        where l.LCID == lcid
                                        select l).FirstOrDefault();

                        var currentDateTime = DateTime.Now;
                        var userId = (Page as BasePage).CurrentContext.UserIdentifier;

                        Card card;

                        if (CurrentCardType.HasValue && CurrentCardType.Value == CardType.BranchingCard)
                        {
                            card = CreateNewBranchingCard(language.LanguageID, currentDateTime, userId);
                        }
                        else
                        {
                            card = CreateNewCard(language.LanguageID, currentDateTime, userId);
                        }
                        

                        card.QuestionCard = CreateNewQuestionCard(currentDateTime, userId, language.LanguageID);

                        dataContext.Cards.Add(card);
                        dataContext.SaveChanges();
                        transaction.Commit();

                        cardId = card.CardID;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw e;
                    }                    
                }
            }

            if (cardId > 0)
            {
                Response.Redirect(string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", cardId, (int)CurrentCardType));
            }
        }

        void Update()
        {
            using (var dataContext = new EChallengeDataContext())
            {
                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        var lcid = (Page as BasePage).CurrentContext.Culture;

                        var language = (from l in dataContext.Languages
                                        where l.LCID == lcid
                                        select l).FirstOrDefault();
                        var currentDateTime = DateTime.Now;

                        var card = dataContext.Cards.FirstOrDefault(c => c.CardID == CurrentCardId);

                        var userId = (Page as BasePage).CurrentContext.UserIdentifier;


                        if (card is BranchingCard)
                        {
                            var branchingCard = (BranchingCard)card;
                            var itemsToRemove = new List<BranchingCard_Button>();

                            UpdateBranchingCard(branchingCard, currentDateTime, userId, language.LanguageID, out itemsToRemove);

                            foreach (var branchingCardButton in itemsToRemove)
                            {
                                dataContext.BranchingCard_ButtonLanguage.RemoveRange(
                                    branchingCardButton.BranchingCard_ButtonLanguage);

                                dataContext.BranchingCard_Button.Remove(branchingCardButton);
                            }
                        }
                        
                        SetCardValues(card, language.LanguageID, currentDateTime, userId);

                        UpdateQuestionCard(card.QuestionCard, currentDateTime, userId, language.LanguageID);

                        dataContext.Card2CardClassifications.RemoveRange(card.Card2CardClassifications);

                        CreateCard2CardClassificationMappings(card, currentDateTime, userId);

                        var cardLanguage =
                            card.Card_Languages.FirstOrDefault(c => c.CardID == card.CardID && c.LanguageID == language.LanguageID);

                        if (cardLanguage == null)
                        {
                            cardLanguage = new Card_Language
                                               {
                                                   CardID = card.CardID,
                                                   CreatedOnDateTime = currentDateTime,
                                                   UserID_CreatedBy = userId,
                                                   LanguageID = language.LanguageID
                                               };

                            card.Card_Languages.Add(cardLanguage);
                        }

                        SetCardLanguageValues(card, cardLanguage, currentDateTime, userId, language.LanguageID);

                        UpdateCardAssociations(card, currentDateTime, userId);

                        UpdateCardReferenceMaterials(card, currentDateTime, userId, language.LanguageID);

                        dataContext.SaveChanges();
                        transaction.Commit();

                        RemovedNodesTemp = null;
                        ChangedPrimaryCardTemp = null;
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }

            if (CurrentCardId > 0)
            {
                Response.Redirect(string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", CurrentCardId, (int)CurrentCardType));
            }
        }

        BranchingCard CreateNewBranchingCard(int languageID, DateTime currentDateTime, Guid userId)
        {
            var branchingCard = new BranchingCard();

            UpdateNewCardData(languageID, currentDateTime, userId, branchingCard);
            BranchingCardControl.PopulateBranchingCardDetails(branchingCard);

            var branchingCardButtons = BranchingCardControl.GetBranchingCardButtons();

            if (branchingCardButtons.Any())
            {
                foreach (var branchingCardButton in branchingCardButtons)
                {
                    var button = new BranchingCard_Button();
                    TransferBranchingCardButtonValues(button, branchingCardButton, currentDateTime, userId, true);

                    foreach (var buttonLanguage in branchingCardButton.BranchingCard_ButtonLanguage)
                    {
                        var newButtonLanguage = new BranchingCard_ButtonLanguage();
                        TransferBranchingCardButtonLanguageValues(newButtonLanguage, buttonLanguage, currentDateTime,
                            userId, languageID, branchingCardButton.BranchingCard_ButtonID, true);

                        button.BranchingCard_ButtonLanguage.Add(newButtonLanguage);
                    }

                    branchingCard.BranchingCard_Buttons.Add(button);
                }
            }

            BranchingCardControl.MoveMultimediaFilesToLiveFolder();
            
            return branchingCard;
        }

        Card CreateNewCard(int languageID, DateTime currentDateTime, Guid userId)
        {
            var card = new Card();

            UpdateNewCardData(languageID, currentDateTime, userId, card);

            return card;
        }

        private void UpdateNewCardData(int languageID, DateTime currentDateTime, Guid userId, Card card)
        {
            SetCardValues(card, languageID, currentDateTime, userId);

            CreateCard2CardClassificationMappings(card, currentDateTime, userId);

            var cardLanguage = new Card_Language();

            SetCardLanguageValues(card, cardLanguage, currentDateTime, userId, languageID);

            card.Card_Languages.Add(cardLanguage);

            CreateCardAssociations(card, currentDateTime, userId);

            UpdateCardReferenceMaterials(card, currentDateTime, userId, languageID);
        }

        void CreateCardAssociations(Card card, DateTime currentDateTime, Guid userId)
        {
            //Contextually similar cards
            if (ContextuallySimilarAssociatedCardData.Count > 0)
            {
                foreach (var association in ContextuallySimilarAssociatedCardData)
                {
                    var associatedCard = new AssociatedCard();
                    associatedCard.CardID = association.CardId;
                    associatedCard.CreatedOnDateTime = currentDateTime;
                    associatedCard.ExplanationForAssociation = string.Empty;
                    associatedCard.IsAssociatedToElseContextuallySimilarTo = false;
                    associatedCard.IsPrimaryAssociation = association.IsPrimaryAssociation;
                    associatedCard.LastModifiedOnDateTime = currentDateTime;
                    associatedCard.UserID_CreatedBy = userId;
                    associatedCard.UserID_LastModifiedBy = userId;

                    card.AssociatedCards.Add(associatedCard);
                }
            }

            if (LeadingQuestionAssociatedCardData.Count > 0)
            {
                foreach (var association in LeadingQuestionAssociatedCardData)
                {
                    var associatedCard = new AssociatedCard();
                    associatedCard.CardID = association.CardId;
                    associatedCard.CreatedOnDateTime = currentDateTime;
                    associatedCard.ExplanationForAssociation = string.Empty;
                    associatedCard.IsAssociatedToElseContextuallySimilarTo = true;
                    associatedCard.IsPrimaryAssociation = association.IsPrimaryAssociation;
                    associatedCard.LastModifiedOnDateTime = currentDateTime;
                    associatedCard.UserID_CreatedBy = userId;
                    associatedCard.UserID_LastModifiedBy = userId;

                    card.AssociatedCards.Add(associatedCard);
                }
            }

            if (AuditStatementAssociatedCardData.Count > 0)
            {
                foreach (var association in AuditStatementAssociatedCardData)
                {
                    var associatedCard = new AssociatedCard();
                    associatedCard.CardID = association.CardId;
                    associatedCard.CreatedOnDateTime = currentDateTime;
                    associatedCard.ExplanationForAssociation = string.Empty;
                    associatedCard.IsAssociatedToElseContextuallySimilarTo = true;
                    associatedCard.IsPrimaryAssociation = association.IsPrimaryAssociation;
                    associatedCard.LastModifiedOnDateTime = currentDateTime;
                    associatedCard.UserID_CreatedBy = userId;
                    associatedCard.UserID_LastModifiedBy = userId;

                    card.AssociatedCards.Add(associatedCard);
                }
            }

            if(AuditStatementImageAssociatedCardData.Count > 0)
            {
                foreach (var association in AuditStatementImageAssociatedCardData)
                {
                    var associatedCard = new AssociatedCard();
                    associatedCard.CardID = association.CardId;
                    associatedCard.CreatedOnDateTime = currentDateTime;
                    associatedCard.ExplanationForAssociation = string.Empty;
                    associatedCard.IsAssociatedToElseContextuallySimilarTo = true;
                    associatedCard.IsPrimaryAssociation = association.IsPrimaryAssociation;
                    associatedCard.LastModifiedOnDateTime = currentDateTime;
                    associatedCard.UserID_CreatedBy = userId;
                    associatedCard.UserID_LastModifiedBy = userId;

                    card.AssociatedCards.Add(associatedCard);
                }   
            }

        }

        void CreateCard2CardClassificationMappings(Card card, DateTime currentDateTime, Guid userId)
        {
            for (var i = 0; i < HiddenNodeList.Count; i++)
            {
                var cardClassificationMapping = new Card2CardClassification();

                var isAPrimaryCard = Convert.ToInt32(HiddenNodeList[i]) == Convert.ToInt32(PrimaryNodeKey);

                SetCard2CardClassificationValues(card,
                                                 cardClassificationMapping, Convert.ToInt32(HiddenNodeList[i]), isAPrimaryCard, currentDateTime, userId);

                card.Card2CardClassifications.Add(cardClassificationMapping);
            }

            if (CardMetaTagTreeList.SelectionCount > 0)
            {
                var nodes = CardMetaTagTreeList.GetSelectedNodes(false);

                foreach (var node in nodes)
                {

                    var cardClassificationMapping = new Card2CardClassification();

                    SetCard2CardClassificationValues(card,
                                                     cardClassificationMapping, Convert.ToInt32(node.Key), false,
                                                     currentDateTime, userId);

                    card.Card2CardClassifications.Add(cardClassificationMapping);
                }
            }
        }

        void UpdateCardAssociations(Card card, DateTime currentDateTime, Guid userId)
        {
            var currentAssociatedCards = card.AssociatedCards.ToList();

            foreach (var association in currentAssociatedCards)
            {
                card.AssociatedCards.Remove(association);
            }

            CreateCardAssociations(card, currentDateTime, userId);
        }

        void UpdateCardReferenceMaterials(Card card, DateTime currentDateTime, Guid userId, int languageId)
        {
            //need to determine records for insert and update
            //records to insert at cardmaterial level all contain negative id's
            var insertData = CardLanguageReferenceMaterialData.Where(c => c.CardReferenceMaterialID < 0);

            //records to update cardreferencematieral ids > 0
            var updateData = CardLanguageReferenceMaterialData.Where(c => c.CardReferenceMaterialID > 0);

            foreach (var referenceMaterial in insertData)
            {
                var newReferenceMaterial = new CardReferenceMaterial();
                TransferCardReferenceMaterialValues(newReferenceMaterial, card, currentDateTime, referenceMaterial, userId);
                card.CardReferenceMaterials.Add(newReferenceMaterial);
            }

            var processedCardReferenceMaterialIds = new List<int>();
            foreach (var referenceMaterial in updateData)
            {
                if (!processedCardReferenceMaterialIds.Contains(referenceMaterial.CardReferenceMaterialID))
                {
                    var currentCardReferenceMaterial =
                        card.CardReferenceMaterials.Where(
                            c => c.CardReferenceMaterialID == referenceMaterial.CardReferenceMaterialID).FirstOrDefault();

                    var allAssociatedLangForReference =
                        updateData.Where(c => c.CardReferenceMaterialID == referenceMaterial.CardReferenceMaterialID);

                    currentCardReferenceMaterial.DefaultLanguageID = referenceMaterial.DefaultLanguageID;

                    foreach (var langRef in allAssociatedLangForReference)
                    {
                        //insert
                        if (langRef.CardReferenceMaterialLanguageID < 0)
                        {
                            var newReferenceMaterialLanguage = new CardReferenceMaterial_Language();
                            TransferCardReferenceMaterialLanguages(newReferenceMaterialLanguage,
                                                                   currentCardReferenceMaterial.CardReferenceMaterialID,
                                                                   currentDateTime, langRef, userId);
                            currentCardReferenceMaterial.CardReferenceMaterial_Languages.Add(
                                newReferenceMaterialLanguage);
                        }
                        else
                        {
                            var currentCardLang =
                                currentCardReferenceMaterial.CardReferenceMaterial_Languages.Where(
                                    l => l.CardReferenceMaterial_LanguageID == langRef.CardReferenceMaterialLanguageID).
                                    FirstOrDefault();

                            currentCardLang.CardReferenceMaterialDescription = langRef.CardReferenceMaterialDescription;

                            if (string.Compare(langRef.CardReferenceMaterialUrl, currentCardLang.CardReferenceMaterialUrl, true) != 0)
                            {
                                currentCardLang.CardReferenceMaterialUrl = langRef.CardReferenceMaterialUrl;
                                MoveReferenceMaterialsFromTempToLive(langRef.CardReferenceMaterialUrl);
                            }

                            currentCardLang.LastModifiedOnDateTime = currentDateTime;
                            currentCardLang.UserID_LastModifiedBy = userId;
                        }
                    }

                    processedCardReferenceMaterialIds.Add(referenceMaterial.CardReferenceMaterialID);
                }
            }
        }

        void TransferCardReferenceMaterialValues(CardReferenceMaterial newReferenceMaterial, Card card, DateTime currentDateTime, CardLanguageReferenceMaterial oldReferenceMaterial, Guid userId)
        {
            newReferenceMaterial.CardID = card.CardID;
            newReferenceMaterial.CreatedOnDateTime = currentDateTime;
            newReferenceMaterial.DefaultLanguageID = oldReferenceMaterial.DefaultLanguageID;
            newReferenceMaterial.LastModifiedOnDateTime = currentDateTime;
            newReferenceMaterial.UserID_CreatedBy = userId;
            newReferenceMaterial.UserID_LastModifiedBy = userId;

            var newReferenceMaterialLanguage = new CardReferenceMaterial_Language();
            TransferCardReferenceMaterialLanguages(newReferenceMaterialLanguage, newReferenceMaterial.CardReferenceMaterialID, currentDateTime, oldReferenceMaterial, userId);
            newReferenceMaterial.CardReferenceMaterial_Languages.Add(newReferenceMaterialLanguage);
        }

        void TransferCardReferenceMaterialLanguages(CardReferenceMaterial_Language newReferenceMaterialLanguage, int cardReferenceMaterialId, DateTime currentDateTime, CardLanguageReferenceMaterial oldReferenceMaterial, Guid userId)
        {

            newReferenceMaterialLanguage.CardReferenceMaterialDescription =
                oldReferenceMaterial.CardReferenceMaterialDescription;
            newReferenceMaterialLanguage.CardReferenceMaterialID = cardReferenceMaterialId;
            newReferenceMaterialLanguage.CardReferenceMaterialUrl = oldReferenceMaterial.CardReferenceMaterialUrl;
            newReferenceMaterialLanguage.CreatedOnDateTime = currentDateTime;
            newReferenceMaterialLanguage.LanguageID = oldReferenceMaterial.LanguageID;
            newReferenceMaterialLanguage.LastModifiedOnDateTime = currentDateTime;
            newReferenceMaterialLanguage.UserID_CreatedBy = userId;
            newReferenceMaterialLanguage.UserID_LastModifiedBy = userId;

            MoveReferenceMaterialsFromTempToLive(oldReferenceMaterial.CardReferenceMaterialUrl);
        }

        private void MoveReferenceMaterialsFromTempToLive(string filename)
        {
            var sourcePath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
            var destinationPath = Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                               filename);

            if (filename.Length > 0 && File.Exists(sourcePath))
            {
                File.Move(sourcePath, destinationPath);

                if (File.Exists(sourcePath))
                {
                    File.Delete(sourcePath);
                }
            }
        }


        void SetCard2CardClassificationValues(Card card, Card2CardClassification card2CardClassification, int cardClassificationGroupId, bool isAPrimaryCard,
            DateTime currentDateTime, Guid userId)
        {
            if (CurrentCardId.HasValue)
            {
                card2CardClassification.UserID_CreateBy = card.UserID_CreatedBy;
                card2CardClassification.CreatedOnDateTime = card.CreatedOnDateTime;
            }
            else
            {
                card2CardClassification.UserID_CreateBy = userId;
                card2CardClassification.CreatedOnDateTime = currentDateTime;
            }

            card2CardClassification.CardClassificationGroupID = cardClassificationGroupId;
            card2CardClassification.CardID = card.CardID;
            card2CardClassification.IsAPrimaryCard = isAPrimaryCard;


            card2CardClassification.LastModifiedOnDateTime = currentDateTime;
            card2CardClassification.UserID_LastModifiedBy = userId;
        }


        void SetCardValues(Card card, int languageID, DateTime currentDateTime, Guid userId)
        {
            if (!CurrentCardId.HasValue)
            {
                card.CreatedOnDateTime = currentDateTime;
                card.UserID_CreatedBy = userId;
            }


            card.LastModifiedOnDateTime = currentDateTime;
            card.UserID_LastModifiedBy = userId;


            card.ProductID = Convert.ToInt32(ProductComboBox.SelectedItem.Value);

            var cardType = (from i in ListHelper.CardTypes
                            where i.Internal_CardTypeShortCode == Enum.GetName(typeof(CardType), CurrentCardType)
                            select i).FirstOrDefault();


            card.Internal_CardTypeID = cardType.Internal_CardTypeID;
            card.CardID_LCDCategoryCard = CategorySelectorComboBox.SelectedItem == null
                                              ? null
                                              : (int?)CategorySelectorComboBox.SelectedItem.Value;


            card.ClientID_ForLibrary = ClientRadioButton.Checked
                                           ? (ClientComboBox.SelectedItem == null
                                                  ? null
                                                  : (int?)ClientComboBox.SelectedItem.Value)
                                           : null;

            card.CardShortcutCode = ShortcutCodeTextBox.Text;
            card.IsForClientlibraryElsePublicLibrary = ClientRadioButton.Checked ? true : false;
            card.IgnoreAnyTimersOnThisCard = IgnoreTimerCheckBox.Checked;
            card.IsStillInBeta_DoNotMakeAvailableForUseInGames = StillInBetaCheckBox.Checked;
            card.IsATimedCard = IsATimedCardCheckBox.Checked;
            card.DefaultLanguageID = Convert.ToInt32(DefaultLanguageComboBox.SelectedItem.Value);

            card.CardMaxTimeToAnswerInSeconds = Convert.ToInt32(TimeToAnswerMinuteSpinEdit.Value) * 60 +
                                                Convert.ToInt32(TimeToAnswerSecondSpinEdit.Value);

            card.IsApplicableForOnlineUse = UseOnlineCheckBox.Checked;
            card.IsApplicableForDesktopUse = UseDesktopCheckBox.Checked;
            card.FacilitatorSlideNotes = FacilitatorNoteMemo.Text;
            card.UseCardTimeNotGameTime = UseCardTimerCheckBox.Checked;
            card.AudioTranscriptText = TranscriptTextMemo.Text;

            if (HiddenField.Contains("AudioNarrativeMediaHyperLinkNavigateUrl"))
            {
                var audioNavigateUrl =
                                    HiddenField.Get("AudioNarrativeMediaHyperLinkNavigateUrl") as string;

                if (!string.IsNullOrEmpty(audioNavigateUrl))
                {
                    var audioFileName = HiddenField.Get("AudioNarrativeMediaHyperLinkText") as string;
                    var parts = audioNavigateUrl.Split(new[] { "/" },
                                                       StringSplitOptions.
                                                           RemoveEmptyEntries);

                    if (parts.Count() > 0)
                    {
                        var filename = parts[parts.Length - 1];
                        if (string.Compare(filename, card.AudioTranscriptFile, true) != 0)
                        {
                            var oldAudioFilename = card.AudioTranscriptFile;

                            card.AudioTranscriptFile = filename;
                            card.AudioTranscriptFriendlyName = audioFileName;
                            var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);

                            var destinationPath = Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                               filename);

                            var oldFileDeletePath = string.IsNullOrEmpty(oldAudioFilename)
                                                        ? string.Empty
                                                        : Path.Combine(
                                                            MapPath(ConfigurationHelper.LiveUploadLocation),
                                                            oldAudioFilename);

                            File.Move(tempPath, destinationPath);

                            if (!string.IsNullOrEmpty(oldAudioFilename) && File.Exists(oldFileDeletePath))
                            {
                                File.Delete(oldFileDeletePath);
                            }
                        }
                    }
                }
            }
            else if (!string.IsNullOrEmpty(card.AudioTranscriptFile) && !string.IsNullOrWhiteSpace(card.AudioTranscriptFile))
            {
                card.AudioTranscriptFriendlyName = null;
                card.AudioTranscriptFile = null;
            }

        }

        void SetCardLanguageValues(Card card, Card_Language cardLanguage, DateTime currentDateTime, Guid userId, int languageID)
        {
            if (!CurrentCardId.HasValue)
            {
                cardLanguage.CreatedOnDateTime = currentDateTime;
                cardLanguage.UserID_CreatedBy = userId;
            }

            cardLanguage.CardID = card.CardID;
            cardLanguage.CardName = CardNameTextBox.Text;
            cardLanguage.CardTitle = CardTitleTextBox.Text;
            cardLanguage.DetailedCardDescription = CardDescriptionMemo.Text;
            cardLanguage.LastModifiedOnDateTime = currentDateTime;
            cardLanguage.UserID_LastModifiedBy = userId;
            cardLanguage.LanguageID = languageID;
        }

        void BindCardDetailControl(Card card)
        {
            switch (CurrentCardType)
            {
                case CardType.MultipleChoiceQuestionCard:
                    QuestionMultipleChoiceCardControl.Populate(card);
                    break;

                case CardType.MultipleChoiceImageQuestionCard:
                    MultipleChoiceImageQuestionControl.Populate(card);
                    break;
                case CardType.MultipleCheckBoxQuestionCard:
                    QuestionMultipleCheckBoxCardControl.Populate(card);
                    break;

                case CardType.TextAreaSurveyCard:
                case CardType.LeadingQuestionCard:
                case CardType.IntroSurveyCard:
                case CardType.TextAreaPriorityFeedbackCard:
                case CardType.TextAreaPersonalActionPlanCard:
                case CardType.GameCompletedCard:
                case CardType.TransitionUpCard:
                case CardType.TransitionDownCard:
                    TextAreaIputControl.Populate(card);
                    break;

                case CardType.TextBoxSurveyCard:
                case CardType.TextBoxPersonalActionPlanCard:
                    TextBoxInputControl.Populate(card);
                    break;

                case CardType.MultipleCheckBoxSurveyCard:
                    SurveyCardMultipleCheckBoxControl.Populate(card);
                    break;

                case CardType.MultipleChoiceSurveyCard:
                    SurveryCardMultipleChoiceControl.Populate(card);
                    break;

                case CardType.MultipleChoiceAuditStatementCard:
                    AuditStatementCardMultipleChoiceControl.Populate(card);
                    break;

                case CardType.MultipleChoiceAuditStatementImageCard:
                    AuditStatementImageCardControl.Populate(card);
                    break;

                case CardType.CompanyPolicyCard:
                case CardType.LcdCategoryCard:
                case CardType.GameIntroCard:
                    ImageAndTextCardControl.Populate(card);
                    break;

                case CardType.PriorityIssueVotingCard:
                    PriorityIssueVotingCardControl.Populate(card);
                    break;

                case CardType.MultipleCheckBoxPriorityFeedbackCard:
                    PriorityFeedbackMultipleCheckBoxCardControl.Populate(card);
                    break;

                case CardType.MultipleChoicePriorityFeedbackCard:
                    PriorityFeedbackMultipleChoiceCardControl.Populate(card);
                    break;

                case CardType.MultipleChoicePersonalActionPlanCard:
                    MultipleChoicePersonalActionPlanCardControl.Populate(card);
                    break;

                case CardType.MultipleCheckboxPersonalActionPlanCard:
                    MultipleCheckBoxPersonalActionPlanCardControl.Populate(card);
                    break;

                case CardType.ThemeIntroCard:
                    StatementListCardControl.Populate(card);
                    break;
                case CardType.MatchingListQuestionCard:
                    MatchingListCardControl.Populate(card);
                    break;
                case CardType.MultiMediaCard:
                    TextAreaLocationCardControl.Populate(card);
                    break;
                case CardType.MultipleCheckBoxImageQuestionCard:
                    MultipleCheckBoxImageCardControl.Populate(card);
                    break;
                case CardType.BranchingCard:
                    BranchingCardControl.Populate(card);
                    break;
            }
        }

        void BindControls()
        {
            if (Request.QueryString["id"] != null)
            {
                var cardId = Convert.ToInt32(Request.QueryString["id"]);
                var dataContext = new EChallengeDataContext();

                var lcid = (Page as BasePage).CurrentContext.Culture;

                var card = (from c in dataContext.Cards
                            where c.CardID == cardId
                            select c).FirstOrDefault();

                var selectProduct = ProductComboBox.Items.FindByValue(card.ProductID);
                if (selectProduct == null)
                {
                    throw new Exception(Resources.Exceptions.UnableToCreateCardProductNotAvailableInLanguage);
                }

                selectProduct.Selected = true;

                DefaultLanguageComboBox.DataBind();

                var defaultLanguage = DefaultLanguageComboBox.Items.FindByValue(card.DefaultLanguageID);
                if (defaultLanguage != null)
                {
                    defaultLanguage.Selected = true;
                }

                BindCardDetailControl(card);

                TranscriptTextMemo.Text = card.AudioTranscriptText;

                if (!string.IsNullOrEmpty(card.AudioTranscriptFile) && !string.IsNullOrWhiteSpace(card.AudioTranscriptFile))
                {
                    AudioNarrativeMediaBrowseButton.ClientVisible = false;
                    AudioNarrativeMediaRemoveHyperlink.ClientVisible = true;
                    AudioNarrativeMediaHyperLink.ClientVisible = true;

                    AudioNarrativeMediaHyperLink.NavigateUrl =
                               ResolveUrl(string.Format(@"{0}/{1}",
                                                        ConfigurationHelper.LiveUploadLocation,
                                                        card.AudioTranscriptFile));

                    AudioNarrativeMediaHyperLink.Text = card.AudioTranscriptFriendlyName;
                }
                else
                {
                    AudioNarrativeMediaBrowseButton.ClientVisible = true;
                    AudioNarrativeMediaRemoveHyperlink.ClientVisible = false;
                    AudioNarrativeMediaHyperLink.ClientVisible = false;
                }


                QuestionMultimediaTypeComboBox.SelectedItem =
                    string.IsNullOrEmpty(card.QuestionCard.QuestionMultimediaType_ImageVideoetc)
                        ? null
                        : QuestionMultimediaTypeComboBox.Items.FindByValue(
                            card.QuestionCard.QuestionMultimediaType_ImageVideoetc);

                if (string.IsNullOrEmpty(card.QuestionCard.QuestionMultimediaType_ImageVideoetc))
                {
                    QuestionMultimediaUrlTextBox.ClientVisible = false;
                    QuestionMultimediaBrowseButton.ClientVisible = false;
                }
                else
                {
                    switch (card.QuestionCard.QuestionMultimediaType_ImageVideoetc.ToLower())
                    {
                        case "wistia":
                            QuestionMultimediaUrlTextBox.Text = card.QuestionCard.QuestionMultimediaURL;
                            QuestionMultimediaUrlTextBox.ClientVisible = true;
                            QuestionMultimediaBrowseButton.ClientVisible = false;
                            break;

                        case "image":
                        case "document":

                            QuestionMultimediaUrlTextBox.ClientVisible = false;

                            if (string.IsNullOrEmpty(card.QuestionCard.QuestionMultimediaURL))
                            {
                                QuestionMultimediaBrowseButton.ClientVisible = true;
                                QuestionMultimediaRemoveHyperLink.ClientVisible = false;
                                break;
                            }

                            QuestionMultimediaDocumentHyperLink.NavigateUrl =
                                ResolveUrl(string.Format(@"{0}/{1}",
                                                         ConfigurationHelper.LiveUploadLocation,
                                                         card.QuestionCard.
                                                             QuestionMultimediaURL));

                            QuestionMultimediaDocumentHyperLink.Text =
                                card.QuestionCard.QuestionMultimediaURLFriendlyName;

                            HiddenField.Set("QuestionMultimediaDocumentHyperLinkNavigateUrl",
                                            QuestionMultimediaDocumentHyperLink.NavigateUrl);
                            HiddenField.Set("QuestionMultimediaDocumentHyperLinkText",
                                            QuestionMultimediaDocumentHyperLink.Text);

                            QuestionMultimediaRemoveHyperLink.ClientVisible = true;
                            QuestionMultimediaBrowseButton.ClientVisible = false;

                            break;                                                 
                    }
                }

                if (card.QuestionCard.QuestionMultimediaThumbnailURL == null || string.IsNullOrEmpty(card.QuestionCard.QuestionMultimediaThumbnailURL) ||
                    string.IsNullOrWhiteSpace(card.QuestionCard.QuestionMultimediaThumbnailURL))
                {
                    QuestionThumbnailHyperLink.ClientVisible = false;
                    QuestionThumnailRemoveHyperLink.ClientVisible = false;
                    QuestionThumbnailBrowseButton.ClientVisible = true;
                }
                else
                {
                    QuestionThumbnailHyperLink.ClientVisible = true;
                    QuestionThumnailRemoveHyperLink.ClientVisible = true;
                    QuestionThumbnailBrowseButton.ClientVisible = false;

                    QuestionThumbnailHyperLink.NavigateUrl =
                                ResolveUrl(string.Format(@"{0}/{1}",
                                                         ConfigurationHelper.LiveUploadLocation,
                                                         card.QuestionCard.
                                                             QuestionMultimediaThumbnailURL));

                    QuestionThumbnailHyperLink.Text =
                        card.QuestionCard.QuestionMultimediaThumbnailURLFriendlyName;

                    HiddenField.Set("QuestionThumbnailHyperLinkNavigateUrl",
                                    QuestionThumbnailHyperLink.NavigateUrl);
                    HiddenField.Set("QuestionThumbnailHyperLinkText",
                                    QuestionThumbnailHyperLink.Text);
                }
                

                QuestionStartMultimediaAutoCheckBox.Checked = card.QuestionCard.QuestionMultimediaStartAutoElsePushPlayToStart;
                QuestionShowPlaybackControlCheckBox.Checked = card.QuestionCard.QuestionMultimediaShowPlaybackControls;

                AnswerMultimediaTypeComboBox.SelectedItem =
                    string.IsNullOrEmpty(card.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc)
                        ? null
                        : AnswerMultimediaTypeComboBox.Items.FindByValue(
                            card.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc);

                if (string.IsNullOrEmpty(card.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc))
                {
                    AnswerMultimediaUrlTextBox.ClientVisible = false;
                    AnswerMultimediaBrowseButton.ClientVisible = false;
                }
                else
                {

                    switch (card.QuestionCard.AnswerExplanationMultimediaType_ImageVideoetc.ToLower())
                    {
                        case "wistia":
                            AnswerMultimediaUrlTextBox.Text = card.QuestionCard.AnswerExplanationMultimediaURL;
                            AnswerMultimediaUrlTextBox.ClientVisible = true;
                            AnswerMultimediaBrowseButton.ClientVisible = false;
                            break;

                        case "image":
                        case "document":

                            AnswerMultimediaUrlTextBox.ClientVisible = false;

                            if (string.IsNullOrEmpty(card.QuestionCard.AnswerExplanationMultimediaURL))
                            {
                                AnswerMultimediaBrowseButton.ClientVisible = true;
                                AnswerMultimediaRemoveHyperLink.ClientVisible = false;
                                break;
                            }

                            AnswerMultimediaDocumentHyperLink.NavigateUrl = ResolveUrl(string.Format(@"{0}/{1}",
                                                                                                     ConfigurationHelper.LiveUploadLocation,
                                                                                                     card.QuestionCard.
                                                                                                         AnswerExplanationMultimediaURL));

                            AnswerMultimediaDocumentHyperLink.Text =
                                card.QuestionCard.AnswerExplanationMultimediaURLFriendlyName;

                            HiddenField.Set("AnswerMultimediaDocumentHyperLinkNavigateUrl",
                                            AnswerMultimediaDocumentHyperLink.NavigateUrl);
                            HiddenField.Set("AnswerMultimediaDocumentHyperLinkText",
                                            AnswerMultimediaDocumentHyperLink.Text);

                            AnswerMultimediaRemoveHyperLink.ClientVisible = true;
                            AnswerMultimediaBrowseButton.ClientVisible = false;

                            break;                        
                    }
                }

                if (card.QuestionCard.AnswerExplanationMultimediaThumbnailURL == null || string.IsNullOrEmpty(card.QuestionCard.AnswerExplanationMultimediaThumbnailURL) ||
                    string.IsNullOrWhiteSpace(card.QuestionCard.AnswerExplanationMultimediaThumbnailURL))
                {
                    AnswerThumbnailHyperLink.ClientVisible = false;
                    AnswerThumnailRemoveHyperLink.ClientVisible = false;
                    AnswerThumbnailBrowseButton.ClientVisible = true;
                }
                else
                {
                    AnswerThumbnailHyperLink.ClientVisible = true;
                    AnswerThumnailRemoveHyperLink.ClientVisible = true;
                    AnswerThumbnailBrowseButton.ClientVisible = false;

                    AnswerThumbnailHyperLink.NavigateUrl =
                                ResolveUrl(string.Format(@"{0}/{1}",
                                                         ConfigurationHelper.LiveUploadLocation,
                                                         card.QuestionCard.
                                                             AnswerExplanationMultimediaThumbnailURL));

                    AnswerThumbnailHyperLink.Text =
                        card.QuestionCard.AnswerExplanationMultimediaThumbnailURLFriendlyName;

                    HiddenField.Set("AnswerThumbnailHyperLinkNavigateUrl",
                                    AnswerThumbnailHyperLink.NavigateUrl);
                    HiddenField.Set("AnswerThumbnailHyperLinkText",
                                    AnswerThumbnailHyperLink.Text);
                }

                AnswerStartMultimediaAutoCheckBox.Checked = card.QuestionCard.AnswerMultimediaStartAutoElsePushPlayToStart;
                AnswerShowPlaynackControlCheckBox.Checked = card.QuestionCard.AnswerMultimediaShowPlaybackControls;

                ShortcutCodeTextBox.Text = card.CardShortcutCode;
                StillInBetaCheckBox.Checked = card.IsStillInBeta_DoNotMakeAvailableForUseInGames;
                UseOnlineCheckBox.Checked = card.IsApplicableForOnlineUse;
                UseDesktopCheckBox.Checked = card.IsApplicableForDesktopUse;
                PublicRadioButton.Checked = !card.IsForClientlibraryElsePublicLibrary;
                ClientRadioButton.Checked = card.IsForClientlibraryElsePublicLibrary;
                ClientComboBox.SelectedItem = card.ClientID_ForLibrary == null
                                                  ? null
                                                  : ClientComboBox.Items.FindByValue(card.ClientID_ForLibrary);
                TimeToAnswerMinuteSpinEdit.Value = card.CardMaxTimeToAnswerInSeconds / 60;
                TimeToAnswerSecondSpinEdit.Value = card.CardMaxTimeToAnswerInSeconds % 60;
                IgnoreTimerCheckBox.Checked = card.IgnoreAnyTimersOnThisCard;
                IsATimedCardCheckBox.Checked = card.IsATimedCard;
                UseCardTimerCheckBox.Checked = card.UseCardTimeNotGameTime;

                CurrentPlayerLoseSpinEdit.Value = card.QuestionCard.DesktopCurrentLosingScore;
                CurrentPlayerWinSpinEdit.Value = card.QuestionCard.DesktopCurrentWinningScore;
                NonCurrentPlayerLoseSpinEdit.Value = card.QuestionCard.DesktopNonCurrentLosingScore;
                NonCurrentPlayerWinSpinEdit.Value = card.QuestionCard.DesktopNonCurrentWinningScore;
                OnlinePlayerLoseSpinEdit.Value = card.QuestionCard.OnlineLosingScore;
                OnlinePlayerWinSpinEdit.Value = card.QuestionCard.OnlineWinningScore;

                FacilitatorNoteMemo.Text = card.FacilitatorSlideNotes;


                var cardLanguage = (from cl in card.Card_Languages
                                    join l in dataContext.Languages on cl.LanguageID equals l.LanguageID
                                    where l.LCID == lcid
                                    select cl).FirstOrDefault();

                if (cardLanguage != null)
                {
                    CardNameTextBox.Text = cardLanguage.CardName;
                    CardTitleTextBox.Text = cardLanguage.CardTitle;
                    CardDescriptionMemo.Text = cardLanguage.DetailedCardDescription;
                }
            }
        }


        public string HiddenListSessionKey
        {
            get { return string.Format(@"{0}HiddenList", CardClassificationTreeList.ClientID); }

        }

        protected List<string> HiddenNodeList
        {
            get
            {
                var list = Session[HiddenListSessionKey] as List<string>;
                if (list == null)
                {
                    list = new List<string>();
                    Session[HiddenListSessionKey] = list;                    
                }
                return list;
            }
            set { Session[HiddenListSessionKey] = value; }
        }

        public string PrimaryNodeSessionKey
        {
            get { return string.Format(@"{0}PrimaryNode", CardClassificationTreeList.ClientID); }
        }

        public string PrimaryNodeKey
        {
            get
            {
                var value = Session[PrimaryNodeSessionKey];
                if (value != null)
                {
                    return value.ToString();
                }
                return null;
            }
            set
            {
                Session[PrimaryNodeSessionKey] = value;
            }
        }


        public string ChangedPrimaryCardTempSessionKey
        {
            get { return string.Format(@"{0}ChangedPrimaryCardTempSessionKey", CardClassificationTreeList.ClientID); }
        }

        string ChangedPrimaryCardTemp
        {
            get { return Session[ChangedPrimaryCardTempSessionKey] as string; }                    
            set { Session[ChangedPrimaryCardTempSessionKey] = value; }
        }

        public string RemovedNodesTempSessionKey
        {
            get { return string.Format(@"{0}RemovedNodesList", CardClassificationTreeList.ClientID); }
        }

        List<string> RemovedNodesTemp
        {
            get
            {
                var list = Session[RemovedNodesTempSessionKey] as List<string>;
                if (list == null)
                {
                    list = new List<string>();
                    Session[RemovedNodesTempSessionKey] = list;
                }
                return list;
            }
            set { Session[RemovedNodesTempSessionKey] = value; }
        }

        void SaveState()
        {
            const string prefix = "mycheck_";
            const string myradio = "myradio";
            foreach (string key in Request.Params)
            {
                var value = Request.Params[key];

                if (key.StartsWith(prefix) || key.StartsWith(myradio))
                {
                    if (key == myradio)
                    {
                        PrimaryNodeKey = value;
                        ChangedPrimaryCardTemp = value;

                        if (!HiddenNodeList.Contains(value))
                        {
                            HiddenNodeList.Add(value);
                        }
                    }

                    if (key.StartsWith(prefix) && !string.IsNullOrEmpty(value))
                    {
                        var nodeKey = key.Substring(prefix.Length);
                        if (value == "H")
                        {
                            if (!HiddenNodeList.Contains(nodeKey))
                            {
                                HiddenNodeList.Add(nodeKey);

                                if (RemovedNodesTemp.Contains(nodeKey))
                                {
                                    RemovedNodesTemp.Remove(nodeKey);
                                }
                            }
                        }
                        else if (value == "V")
                        {                            
                            HiddenNodeList.Remove(nodeKey);
                            
                            RemovedNodesTemp.Add(nodeKey);
                        }                        
                    }
                }
            }

            CardClassificationTreeList.DataBind();
        }

        protected void CardClassificationTreeList_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (e.NodeKey == PrimaryNodeKey)
            {
                e.Row.BackColor = ColorTranslator.FromHtml("#FFBD69");
            }

            if (HiddenNodeList.Contains(e.NodeKey))
            {
                e.Row.Font.Bold = true;
            }
        }


        protected void CardClassificationTreeList_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            //emtpy handler required to sync state of radiobutton checkbox treelist
        }

        protected void DefaultLanguageDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var productId = (int?)e.SelectParameters["ProductID"];

            if (productId.HasValue)
            {
                e.Result = (from p in dataContext.Products
                            from pl in p.Product_Languages
                            where p.ProductID == productId
                            select pl.Language).Distinct();
            }
            else
            {
                e.Cancel = true;
            }

        }


        void FillDefaultLanguageComboBox(string productId)
        {
            if (string.IsNullOrEmpty(productId))
            {
                return;
            }

            DefaultLanguageDataSource.SelectParameters["ProductID"].DefaultValue = productId;
            DefaultLanguageComboBox.DataBind();

            if (!CurrentCardId.HasValue)
            {
                var lcid = (Page as BasePage).CurrentContext.Culture;
                var dataContext = new EChallengeDataContext();
                var language = dataContext.Languages.Where(l => l.LCID == lcid).FirstOrDefault();

                var selectedItem = DefaultLanguageComboBox.Items.FindByValue(language.LanguageID);
                if (selectedItem != null)
                {
                    selectedItem.Selected = true;
                }
            }
        }

        protected void ProductComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var productId = (int)ProductComboBox.SelectedItem.Value;
            FillDefaultLanguageComboBox(productId.ToString());
        }

        protected void UploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            var splitOn = new[] { "." };
            var parts = e.UploadedFile.FileName.Split(splitOn, StringSplitOptions.RemoveEmptyEntries);
            var extension = parts.Length > 1 ? string.Format(".{0}", parts[parts.Length - 1]) : string.Empty;
            var filename = string.Format("{0}{1}", Guid.NewGuid(), extension);
            var fullPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
            e.UploadedFile.SaveAs(fullPath, false);
            var url = ResolveUrl(string.Format("{0}/{1}", ConfigurationHelper.TemporaryUploadLocation, filename));

            e.CallbackData = string.Format("{0};{1}", url, e.UploadedFile.FileName);
        }

        protected void ReferenceMaterialGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var descriptionTextBox =
                gridView.FindEditFormTemplateControl(@"CardReferenceMaterialDescriptionTextBox") as ASPxTextBox;
            var navigateUrl = HiddenField.Get("CardReferenceMaterialDocumentHyperLinkNavigateUrl") as string;

            var parts = navigateUrl.Split('/');
            var filename = parts[parts.Length - 1];

            var cardReferenceMaterialId = GetNextCardReferenceMaterialId();
            var cardReferenceMaterialLanguageId = GetNextCardReferenceMaterialLanguageId();

            CardLanguageReferenceMaterialData.Add(
                new CardLanguageReferenceMaterial()
                    {
                        CardReferenceMaterialDescription = descriptionTextBox.Text.Trim(),
                        CardReferenceMaterialID = cardReferenceMaterialId,
                        CardReferenceMaterialLanguageID = cardReferenceMaterialLanguageId,
                        CardReferenceMaterialUrl = filename,
                        DefaultLanguageID = language.LanguageID,
                        LanguageID = language.LanguageID
                    }
                );

            gridView.CancelEdit();
            e.Cancel = true;
            gridView.DataBind();
        }

        protected void ReferenceMaterialGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var descriptionTextBox =
                gridView.FindEditFormTemplateControl(@"CardReferenceMaterialDescriptionTextBox") as ASPxTextBox;
            var navigateUrl = HiddenField.Get("CardReferenceMaterialDocumentHyperLinkNavigateUrl") as string;

            var parts = navigateUrl.Split('/');
            var filename = parts[parts.Length - 1];


            var cardReferenceMaterialId = (int)e.Keys[0];

            var data =
                CardLanguageReferenceMaterialData.Where(
                    c => c.CardReferenceMaterialID == cardReferenceMaterialId && c.LanguageID == language.LanguageID).
                    FirstOrDefault();

            if (data != null)
            {
                data.CardReferenceMaterialDescription = descriptionTextBox.Text.Trim();
                data.CardReferenceMaterialUrl = filename;
            }

            gridView.CancelEdit();
            e.Cancel = true;
            gridView.DataBind();
        }

        protected void DetailReferenceMaterialGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var gridView = sender as ASPxGridView;

            var descriptionTextBox =
                gridView.FindEditFormTemplateControl(@"DetailReferenceDescriptionTextBox") as ASPxTextBox;
            var navigateUrl = HiddenField.Get("DetailReferenceDocumentHyperLinkNavigateUrl") as string;
            var parts = navigateUrl.Split('/');
            var filename = parts[parts.Length - 1];

            var defaultLanguageCheckBox =
                gridView.FindEditFormTemplateControl(@"IsDefaultLanguageForReferenceMaterialCheckBox") as ASPxCheckBox;

            var cardReferenceMaterialLanguageId = GetNextCardReferenceMaterialLanguageId();

            var currentDefaultLanguage =
                CardLanguageReferenceMaterialData.Where(c => c.CardReferenceMaterialID == EditingCardReferenceMaterialID)
                    .FirstOrDefault();

            if (defaultLanguageCheckBox.Checked)
            {
                currentDefaultLanguage.DefaultLanguageID = language.LanguageID;
            }

            CardLanguageReferenceMaterialData.Add(
                new CardLanguageReferenceMaterial()
                    {
                        CardReferenceMaterialDescription = descriptionTextBox.Text.Trim(),
                        CardReferenceMaterialID = EditingCardReferenceMaterialID.Value,
                        CardReferenceMaterialLanguageID = cardReferenceMaterialLanguageId,
                        CardReferenceMaterialUrl = filename,
                        DefaultLanguageID = defaultLanguageCheckBox.Checked ? language.LanguageID : currentDefaultLanguage.DefaultLanguageID,
                        LanguageID = language.LanguageID
                    }
                );

            gridView.CancelEdit();
            e.Cancel = true;
            gridView.DataBind();
        }

        protected void DetailReferenceMaterialGridView_BeforePerformDataSelect(object sender, EventArgs e)
        {
            EditingCardReferenceMaterialID = Convert.ToInt32((sender as ASPxGridView).GetMasterRowKeyValue());
        }

        int GetNextCardReferenceMaterialId()
        {
            var id = -1;

            if (CardLanguageReferenceMaterialData.Count > 0)
            {
                var tempId = CardLanguageReferenceMaterialData.Min(c => c.CardReferenceMaterialID);

                if (tempId < 0)
                {
                    id = tempId - 1;
                }
            }

            return id;
        }

        int GetNextCardReferenceMaterialLanguageId()
        {
            var id = -1;

            if (CardLanguageReferenceMaterialData.Count > 0)
            {
                var tempId = CardLanguageReferenceMaterialData.Min(c => c.CardReferenceMaterialLanguageID);

                if (tempId < 0)
                {
                    id = tempId - 1;
                }
            }

            return id;
        }

        protected void ReferenceMaterialGridView_DetailRowGetButtonVisibility(object sender, ASPxGridViewDetailRowButtonEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var rowLanguageId = (int)gridView.GetRowValues(e.VisibleIndex, "LanguageID");

            if (language.LanguageID == rowLanguageId)
            {
                e.ButtonState = GridViewDetailRowButtonState.Hidden;
            }
            else
            {
                e.ButtonState = GridViewDetailRowButtonState.Visible;
            }
        }

        protected void DocumentHyperLink_Init(object sender, EventArgs e)
        {
            var hyperLink = sender as ASPxHyperLink;
            var container = hyperLink.NamingContainer as GridViewDataItemTemplateContainer;

            var documentUrl = (string)container.Grid.GetRowValues(container.VisibleIndex, "CardReferenceMaterialUrl");
            var referenceMaterialLanguageId =
                (int)container.Grid.GetRowValues(container.VisibleIndex, "CardReferenceMaterialLanguageID");

            if (string.IsNullOrEmpty(documentUrl))
            {
                return;
            }

            var tempDocumentLocation = ResolveUrl(string.Format(@"{0}/{1}", ConfigurationHelper.TemporaryUploadLocation,
                                                                documentUrl));

            var liveDocumentLocation = ResolveUrl(string.Format(@"{0}/ReferenceMaterial/{1}", ConfigurationHelper.LiveUploadLocation,
                                                                   documentUrl));

            hyperLink.NavigateUrl = File.Exists(MapPath(tempDocumentLocation))
                                        ? tempDocumentLocation
                                        : liveDocumentLocation;
        }

        protected void DetailReferenceMaterialGridViewNewButton_Init(object sender, EventArgs e)
        {
            var hyperLink = sender as ASPxHyperLink;
            var container = hyperLink.NamingContainer as GridViewHeaderTemplateContainer;
            hyperLink.ClientVisible = !(container.Grid.VisibleRowCount > 0);
        }

        protected void DetailReferenceMaterialGridView_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            ReferenceMaterialGridView.DataBind();
        }

        protected void ReferenceMaterialGridView_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            if (e.ButtonType == ColumnCommandButtonType.Edit)
            {
                var languageId = (int)gridView.GetRowValues(e.VisibleIndex, "LanguageID");

                var lcid = (Page as BasePage).CurrentContext.Culture;
                var dataContext = new EChallengeDataContext();
                var language = dataContext.Languages.Where(l => l.LCID == lcid).FirstOrDefault();

                if (language.LanguageID != languageId)
                {
                    e.Visible = false;
                }
                else
                {
                    e.Visible = true;
                }
            }
        }

        void MoveQuestionSpecificMultimedia(Card card)
        {
            if(card == null || card.QuestionCard == null)
            {
                return;
            }

           if(Enum.GetName(typeof(CardType), CardType.GameIntroCard) == card.Internal_CardType.Internal_CardTypeShortCode ||
               Enum.GetName(typeof(CardType), CardType.CompanyPolicyCard) == card.Internal_CardType.Internal_CardTypeShortCode ||
               Enum.GetName(typeof(CardType), CardType.LcdCategoryCard) == card.Internal_CardType.Internal_CardTypeShortCode ||
               Enum.GetName(typeof(CardType), CardType.ThemeIntroCard) == card.Internal_CardType.Internal_CardTypeShortCode)
           {
               if(string.IsNullOrEmpty(card.QuestionCard.AssociatedImageUrl) || string.IsNullOrWhiteSpace(card.QuestionCard.AssociatedImageUrl))
               {
                   return;
               }

               MoveMediaFromTempToLive(card.QuestionCard.AssociatedImageUrl, card.QuestionCard.AssociatedImageUrl);
           }
           else if (Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementImageCard) == card.Internal_CardType.Internal_CardTypeShortCode ||
               Enum.GetName(typeof(CardType), CardType.MultipleChoiceImageQuestionCard) == card.Internal_CardType.Internal_CardTypeShortCode ||
               Enum.GetName(typeof(CardType), CardType.MultipleCheckBoxImageQuestionCard) == card.Internal_CardType.Internal_CardTypeShortCode
               )
           {
               foreach (var questionCardGeneralAnswer in card.QuestionCard.QuestionCard_GeneralAnswers)
               {
                   foreach (var answerLanguage in questionCardGeneralAnswer.QuestionCard_GeneralAnswer_Languages)
                   {
                       if(string.IsNullOrEmpty(answerLanguage.PossibleAnswerMultimediaURL) || string.IsNullOrWhiteSpace(answerLanguage.PossibleAnswerMultimediaURL))
                       {
                           continue;
                       }

                       MoveMediaFromTempToLive(answerLanguage.PossibleAnswerMultimediaURL,
                                               answerLanguage.PossibleAnswerMultimediaURL);
                   }
               }
           }
        }


        void MoveMediaFromTempToLive(string tempFileName, string liveFileName)
        {
            if(string.IsNullOrEmpty(tempFileName) || string.IsNullOrWhiteSpace(tempFileName))
            {
                return;
            }

            if (string.IsNullOrEmpty(liveFileName) || string.IsNullOrWhiteSpace(liveFileName))
            {
                return;
            }

            var sourcePath = Path.Combine(MapPath(
                    ConfigurationHelper.TemporaryUploadLocation),
                    tempFileName);

            if(File.Exists(sourcePath))
            {
                var destinationPath = Path.Combine(
                        MapPath(ConfigurationHelper.LiveUploadLocation),
                        liveFileName);

                File.Move(sourcePath, destinationPath);

                if (File.Exists(sourcePath))
                {
                    File.Delete(sourcePath);
                }
            }
        }


        void MoveQuestionOrAnswerMultimediaFileToLive(string sourceFilename, string imageVideoEtc)
        {
            if (string.IsNullOrEmpty(sourceFilename))
            {
                return;
            }

            if (string.IsNullOrEmpty(imageVideoEtc))
            {
                return;
            }
            
            var sourcePath = Path.Combine(MapPath(
                    ConfigurationHelper.TemporaryUploadLocation),
                    sourceFilename);

            var destinationPath = Path.Combine(
                    MapPath(ConfigurationHelper.LiveUploadLocation),
                    sourceFilename);

            File.Move(sourcePath, destinationPath);

            if (File.Exists(sourcePath))
            {
                File.Delete(sourcePath);
            }
        }

        protected void DuplicateCardCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            var parameters = e.Parameter.Split(new[] { "~;" }, StringSplitOptions.RemoveEmptyEntries);

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var cardService = new CardService() as ICardService;
            var card = cardService.CreateDuplicateCard(CurrentCardId.Value, userId);
            var cardId = -1;

            using (var dataContext = new EChallengeDataContext())
            {
                card.CardShortcutCode = parameters[0].Trim();
                var lcid = (Page as BasePage).CurrentContext.Culture;

                var language = dataContext.Languages.Where(l => l.LCID == lcid).FirstOrDefault();
                var cardLanguage = card.Card_Languages.Where(l => l.LanguageID == language.LanguageID).FirstOrDefault();
                cardLanguage.CardName = parameters[1].Trim();
                cardLanguage.CardTitle = parameters[2].Trim();

                var currentCard = dataContext.Cards.Where(c => c.CardID == CurrentCardId).FirstOrDefault();

                if (currentCard == null)
                {
                    e.Result = "The card has been removed. Please reload the card and try again";
                }

                var shortCodeExists = dataContext.Cards.Where(c => c.CardShortcutCode == card.CardShortcutCode);

                if (shortCodeExists.Count() > 0)
                {
                    e.Result =
                        string.Format("A card with the shortcode {0} already exists. Please try another short code",
                                      card.CardShortcutCode);

                    return;
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.Cards.Add(card);
                        dataContext.SaveChanges();

                        MoveQuestionSpecificMultimedia(card);

                        //move question and answer multimedia over to live folder
                        MoveQuestionOrAnswerMultimediaFileToLive(card.QuestionCard.AnswerExplanationMultimediaURL,
                                                                 card.QuestionCard.
                                                                     AnswerExplanationMultimediaType_ImageVideoetc);
                        MoveQuestionOrAnswerMultimediaFileToLive(card.QuestionCard.QuestionMultimediaURL,
                                                                 card.QuestionCard.QuestionMultimediaType_ImageVideoetc);

                        var referenceMaterials = from c in card.CardReferenceMaterials
                                                 from rf in c.CardReferenceMaterial_Languages
                                                 select rf;

                        foreach (var referenceMaterial in referenceMaterials)
                        {
                            MoveReferenceMaterialsFromTempToLive(referenceMaterial.CardReferenceMaterialUrl);
                        }

                        transaction.Commit();

                        cardId = card.CardID;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        e.Result = ex.Message;
                    }                    
                }
            }

            if (cardId > 0)
            {
                ASPxWebControl.RedirectOnCallback(string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", cardId,
                                                                (int)CurrentCardType));
            }
        }



    }
}