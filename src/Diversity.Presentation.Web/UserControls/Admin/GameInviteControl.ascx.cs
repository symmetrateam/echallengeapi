﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Diversity.DataModel.SqlRepository;
using Diversity.Common;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class GameInviteControl : System.Web.UI.UserControl
    {
        private const string CLIENT_ID_SELECTED = "228A4393-49F8-49CF-88D8-D3326443C0E3";
        private const string CLIENT_USER_GROUP_ID = "BF741B02-1B19-4E4A-A0A2-EAE342982613";

        public class EmailMessage
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailAddress { get; set; }
            public MailMessage MailMessage { get; set; }
        }

        int ClientId
        {

            get
            {
                if (Session[CLIENT_ID_SELECTED] == null)
                {
                    Session[CLIENT_ID_SELECTED] = -1;
                }

                return Convert.ToInt32(Session[CLIENT_ID_SELECTED]);
            }

            set { Session[CLIENT_ID_SELECTED] = value; }
        }

        int ClientUserGroupId
        {

            get
            {
                if (Session[CLIENT_USER_GROUP_ID] == null)
                {
                    Session[CLIENT_USER_GROUP_ID] = -1;
                }

                return Convert.ToInt32(Session[CLIENT_USER_GROUP_ID]);
            }

            set { Session[CLIENT_USER_GROUP_ID] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CleanUp();
            }
        }

        void CleanUp()
        {
            ClientUserGroupId = -1;
            ClientId = -1;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var count = GetClients().Count();

            if (count <= 0)
            {
                var exception = new UIMessageException(Resources.Exceptions.ZeroClientAssignedToProfile);
                exception.MessageCode = "ZeroClientAssignedToProfile";
                throw exception;
            }
        }

        protected void ClientDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = GetClients();
        }

        IEnumerable<Client> GetClients()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var dataContext = new EChallengeDataContext();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                return (from c in dataContext.Clients
                        select c).Distinct();
            }

            return (from c in dataContext.Clients
                    from d in c.Client2Distributors
                    join u in dataContext.User2Distributors on d.DistributorID equals u.DistributorID
                    where u.UserID == userId
                    select c).Distinct();
        }

        protected void ClientSelectionCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            ClientId = Convert.ToInt32(e.Parameter);
        }

        protected void ClientUserGroupDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            if (ClientId > 0)
            {

                var dataContext = new EChallengeDataContext();
                e.Result = (from c in dataContext.ClientUserGroups
                            where c.ClientID == ClientId
                            select c).ToList();
            }
            else
            {
                e.Result = new List<ClientUserGroup>();
            }
        }

        protected void UserGridView_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            ClientUserGroupId = string.IsNullOrEmpty(e.Parameters) ? -1 : Convert.ToInt32(e.Parameters);
            UserGridView.Selection.UnselectAll();
            UserGridView.DataBind();
        }

        protected void UserDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            if (ClientUserGroupId <= 0)
            {
                e.Result = new List<User>();
            }
            else
            {
                var dataContext = new EChallengeDataContext();

                e.Result = (from u in dataContext.Users
                            join uc in dataContext.User2ClientUserGroups on u.UserId equals uc.UserID
                            where uc.ClientUserGroupID == ClientUserGroupId
                            select u).Distinct().ToList();

            }
        }

        protected void ClientUserGroupComboBox_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            ClientUserGroupComboBox.DataBind();
            ClientUserGroupComboBox.SelectedIndex = -1;
        }

        protected void GameDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = (from g in dataContext.Games
                            join p in dataContext.Products on g.ProductID equals p.ProductID
                            let gamePlayedCount = dataContext.Games.Count(gp => gp.GameID_Template == g.GameID)
                            where g.IsAGameTemplate
                            //&& gamePlayedCount >= 3
                            select g).Distinct();
            }
            else
            {
                e.Result = (from g in dataContext.Games
                            join p in dataContext.Products on g.ProductID equals p.ProductID
                            join pd in dataContext.Product2Distributors on p.ProductID equals pd.ProductID
                            join ud in dataContext.User2Distributors on pd.DistributorID equals ud.DistributorID
                            let gamePlayedCount = dataContext.Games.Count(gp => gp.GameID_Template == g.GameID)
                            where g.IsAGameTemplate && ud.UserID == userId
                            //&& gamePlayedCount >= 3
                            select g).Distinct();
            }
        }

        string GetShortFormattedUserEmailString(string firstname, string lastname, string email)
        {
            return string.Format(@"{0} {1} [{2}]", firstname, lastname, email);
        }

        protected void SaveCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            var recipientEmailFailures = new List<string>();

            var selectedRowValues = UserGridView.GetSelectedFieldValues(
                new[]
                    {
                        "UserId",
                        "UserPersonalEmail1",
                        "UserFirstName",
                        "UserLastName"
                    }
                );

            var ids = e.Parameter.Split(';');            
            var games = ids.Select(id => Convert.ToInt32(id)).ToList();

            var emails = new List<EmailMessage>();
                        
            var themeClientUserGroupExternalId = (string)ThemeComboBox.Value;
            if (!games.Any())
            {
                return;
            }

            foreach (object[] columns in selectedRowValues)
            {
                var userId = (Guid)columns[0];
                var email = columns[1].ToString();
                var firstname = columns[2].ToString();
                var lastname = columns[3].ToString();

                var user = Membership.GetUser(userId);

                if (user == null)
                {
                    continue;
                }

                using (var dataContext = new EChallengeDataContext())
                {
                    var isNewUser = false;
                    var resetPasswordString = string.Empty;

                    var hasInvites =
                        dataContext.GameInvites.Any(c => c.UserID == userId);

                    if (user.LastLoginDate == user.CreationDate && !hasInvites)
                    {                        
                        resetPasswordString = user.ResetPassword();
                        isNewUser = true;
                    }

                    var passwordText = string.Empty;

                    if(isNewUser)
                    {
                        passwordText = string.Format(@"{0}", resetPasswordString);
                    }
                    else
                    {
                        passwordText = "Your existing password";
                    }

                    var message = EmailHelper.GetGameInvitationalEmailMessage(user.Email, SubjectTextBox.Text, 
                            EmailBodyHtmlEditor.Html, true, firstname, lastname, EmailFooterHtmlEditor.Html, passwordText);

                    emails.Add(new EmailMessage()
                    {
                        FirstName = firstname,
                        LastName = lastname,
                        EmailAddress = email,
                        MailMessage = message
                    });



                    foreach (var gameId in games)
                    {
                        var gameInvite =
                            dataContext.GameInvites.FirstOrDefault(c => c.UserID == userId && c.GameID == gameId);

                        var currentDateTime = DateTime.Now;
                        var editingUserId = (Page as BasePage).CurrentContext.UserIdentifier;

                        if (gameInvite == null)
                        {
                            gameInvite = new GameInvite()
                            {
                                CreatedOnDateTime = currentDateTime,
                                GameID = gameId,
                                InviteDate = currentDateTime,
                                LastModifiedOnDateTime = currentDateTime,
                                LastReminderDate = currentDateTime,
                                UserID = userId,
                                UserID_CreatedBy = editingUserId,
                                UserID_LastModifiedBy = editingUserId,
                                EmailContent = message.Body,
                                EmailSubject = message.Subject,
                                ThemeClientUserGroupExternalID = themeClientUserGroupExternalId
                            };

                            dataContext.GameInvites.Add(gameInvite);
                        }
                        else
                        {
                            gameInvite.LastModifiedOnDateTime = currentDateTime;
                            gameInvite.UserID_LastModifiedBy = editingUserId;
                            gameInvite.LastReminderDate = currentDateTime;
                            gameInvite.EmailContent = message.Body;
                            gameInvite.EmailSubject = message.Subject;
                        }
                    }


                    using (var transaction = dataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            dataContext.SaveChanges();
                            transaction.Commit();

                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            emails.Clear();
                            throw;
                        }
                    }

                }
            }


            if (EmailCheckBox.Checked)
            {
                return;                
            }

            if (emails.Any())
            {
                var client = new SmtpClient
                {
                    EnableSsl = ConfigurationHelper.SmtpEnableSsl
                };

                foreach (var email in emails)
                {
                    try
                    {
                        client.Send(email.MailMessage);
                    }
                    catch
                    {
                        recipientEmailFailures.Add(GetShortFormattedUserEmailString(email.FirstName, email.LastName,
                            email.EmailAddress));
                    }
                }

                client.Dispose();
            }

            if (recipientEmailFailures.Any())
            {
                //format and send text result to client
                var outputBuilder = new StringBuilder();
                foreach (var recipient in recipientEmailFailures)
                {
                    outputBuilder.AppendLine(recipient);
                }
                e.Result = outputBuilder.ToString();
            }
        }

        protected void CancelClientButton_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect(ResolveUrl(@"~/Default.aspx"));
        }

        protected void ThemeComboBox_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            var comboBox = sender as ASPxComboBox;
            if (comboBox == null)
            {
                return;
            }

            var clientUserGroupID = Convert.ToInt32(e.Parameter);
            

            using (var dataContext = new EChallengeDataContext())
            {
                var themes = dataContext.ThemeClientUserGroup
                    .Where(c => c.ClientUserGroupID == clientUserGroupID)
                    .Select(c => new
                                 {
                                     c.ExternalID,
                                     c.ThemeID,
                                     c.Theme.ThemeName
                                 })
                    .OrderBy(c => c.ThemeName)
                    .ToList();

                comboBox.DataSource = themes;
                comboBox.DataBind();
            }            
        }

        
    }
}