﻿function OnRoleGroupSelectedIndexChanged(s, e) {
    var selectedValue = s.GetValue().toString();
    RolesDualListBoxCallbackPanel.PerformCallback(selectedValue);
}

function OnUpdateButtonClick() {
    if (ASPxClientEdit.ValidateGroup("dxValidationGroup", true)) {
        UserGridView.UpdateEdit();
    }
    else {
        UserPageControl.SetActiveTab(UserPageControl.GetTab(0));
    }
}

var _userId;
function DeleteUserButton_Click(s, e) {
    DeleteUserCallback.PerformCallback(_userId);
    DeleteNotificationPopupControl.Hide();
    LoadingPanel.Show();
}

function DeleteButton_Click(s,e) {
    _userId = e;
    DeleteNotificationPopupControl.Show();
}

function CancelDeleteUserButton_Click(s, e) {
    DeleteNotificationPopupControl.Hide();
    _userId = null;
}

function DeleteUserCallback_CallbackComplete(s, e) {
    var message = e.result;

    LoadingPanel.Hide();
    if (message == null) {
        PopupLabel.SetText(null);
        NotificationPopupControl.Hide();
        UserGridView.Refresh();    
    }
    else {
        PopupLabel.SetText(message);
        NotificationPopupControl.Show();
    }
}