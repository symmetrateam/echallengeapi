﻿<%@ control language="C#" autoeventwireup="true" codebehind="ClientUserGroupManagementControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.ClientUserGroupManagementControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>


<script id="dxis_ClientUserGroupManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/ClientUserGroupManagementControl.js") %>'>

</script>
<dx:aspxgridview id="ClientGridView" runat="server" autogeneratecolumns="False" datasourceid="ClientDataSource"
    keyfieldname="ClientID" width="929px">
    <settingsdetail allowonlyonemasterrowexpanded="true" showdetailrow="true" />
    <columns>
        <dx:gridviewdatatextcolumn fieldname="ClientID" visibleindex="0" visible="false">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="ClientName" visibleindex="1">
        </dx:gridviewdatatextcolumn>
    </columns>
    <templates>
        <detailrow>
            <dx:aspxgridview id="ClientUserGroupGridView" runat="server" autogeneratecolumns="False"
                datasourceid="ClientUserGroupDataSource" keyfieldname="ClientUserGroupID" width="100%"
                onstartrowediting="ClientUserGroupGridView_StartRowEditing" clientinstancename="ClientUserGroupGridView"
                oncancelrowediting="ClientUserGroupGridView_CancelRowEditing" onrowinserting="ClientUserGroupGridView_RowInserting"
                onrowupdating="ClientUserGroupGridView_RowUpdating"
                onrowvalidating="ClientUserGroupGridView_RowValidating" 
                onbeforeperformdataselect="ClientUserGroupGridView_BeforePerformDataSelect">
                <columns>
                    <dx:gridviewcommandcolumn visibleindex="0" width="100px" ShowEditButton="True" ShowNewButton="False" ShowDeleteButton="True" ShowClearFilterButton="True">
                        <headercaptiontemplate>
                            <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="ClientUserGroupGridView.AddNewRow();return false;" text="New"/>
                        </headercaptiontemplate>
                    </dx:gridviewcommandcolumn>
                    <dx:gridviewdatatextcolumn fieldname="ClientUserGroupID" readonly="True" visible="false"
                        visibleindex="1">
                        <editformsettings visible="False" />
                    </dx:gridviewdatatextcolumn>
                    <dx:gridviewdatatextcolumn fieldname="ClientUserGroupName" visibleindex="2" caption=" Group Name"
                        width="30%">
                    </dx:gridviewdatatextcolumn>
                    <dx:gridviewdatatextcolumn caption="Description" fieldname="ClientUserGroupDescription"
                        visibleindex="3">
                    </dx:gridviewdatatextcolumn>
                    <dx:gridviewdatatextcolumn caption="No of Members" fieldname="TotalUsers"
                        visibleindex="4">
                    </dx:gridviewdatatextcolumn>
                </columns>
                <settingsbehavior columnresizemode="NextColumn" enablerowhottrack="True" />
                <settingspager alwaysshowpager="True">
                    <allbutton visible="True">
                        <image tooltip="Show All User Groups">
                        </image>
                    </allbutton>
                </settingspager>
                <settings showfilterrow="True" showgrouppanel="True" />
                <templates>
                    <editform>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="GroupNameLabel" runat="server" text="Group Name" />
                                </td>
                                <td>
                                    <dx:aspxtextbox id="GroupNameTextBox" runat="server" text='<%# Bind("ClientUserGroupName") %>'
                                        validationsettings-validationgroup='<%# Container.ValidationGroup %>'>
                                        <validationsettings setfocusonerror="true" errordisplaymode="ImageWithTooltip">
                                            <requiredfield isrequired="true" errortext="Group name is a required field" />
                                        </validationsettings>
                                    </dx:aspxtextbox>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <dx:aspxlabel id="DescriptionLabel" runat="server" text="Description" />
                                </td>
                                <td colspan="2">
                                    <dx:aspxmemo id="DescriptionMemo" runat="server" text='<%# Bind("ClientUserGroupDescription") %>'
                                        rows="5" width="550px">
                                    </dx:aspxmemo>
                                </td>
                            </tr>
                        </table>
                        <br></br>
                        <dx:aspxgridview id="UserGridView" runat="server" autogeneratecolumns="false" clientinstancename="UserGridView" datasourceid="UserDataSource"
                            keyfieldname="UserId" width="100%" onload="UserGridView_Load">
                            <settings HorizontalScrollBarMode="Auto" UseFixedTableLayout="True" showfilterbar="Visible" showgrouppanel="true" showfilterrow="True" />
                            <settingspager pagesize="50"></settingspager>
                            <columns>
                                <dx:gridviewcommandcolumn showselectcheckbox="true" visibleindex="0" ShowClearFilterButton="True">
                                    <headertemplate>
                                        <dx:aspxcheckbox id="UserGridSelectAllCheckBox" runat="server" clientinstancename="UserGridSelectAllCheckBox" tooltip="Select/Unselect all rows on page">
                                            <clientsideevents checkedchanged="UserGridSelectAllCheckBox_CheckChanged"/>
                                        </dx:aspxcheckbox>
                                    </headertemplate>
                                </dx:gridviewcommandcolumn>                                
                                <dx:gridviewdatatextcolumn fieldname="UserFirstName" caption="Firstname">
                                </dx:gridviewdatatextcolumn>
                                <dx:gridviewdatatextcolumn fieldname="UserLastName" caption="Lastname">
                                </dx:gridviewdatatextcolumn>
                                <dx:gridviewdatatextcolumn fieldname="UserPersonalEmail1" caption="Personal/Login Email" Width="140">
                                </dx:gridviewdatatextcolumn>                                
                                <dx:gridviewdatatextcolumn fieldname="ClientLocation" caption="Location">
                                </dx:gridviewdatatextcolumn>
                                <dx:gridviewdatatextcolumn fieldname="ClientJobTitle" caption="Job Title">
                                </dx:gridviewdatatextcolumn>
                                <dx:gridviewdatatextcolumn fieldname="ClientUniqueSalaryNo" caption="Salary No">
                                </dx:gridviewdatatextcolumn>                                
                                <dx:gridviewdatatextcolumn fieldname="ClientSubsidiaryCompanyName" caption="Subsidiary Company Name">
                                </dx:gridviewdatatextcolumn>
                                <dx:gridviewdatatextcolumn fieldname="ClientBusinessUnit" caption="Business Unit">
                                </dx:gridviewdatatextcolumn>
                                <dx:gridviewdatatextcolumn fieldname="ClientCostCentre" caption="Cost Centre">
                                </dx:gridviewdatatextcolumn>
                                <dx:gridviewdatatextcolumn fieldname="ClientDivision" caption="Division">
                                </dx:gridviewdatatextcolumn>
                                <dx:gridviewdatatextcolumn fieldname="ClientUserGroup" caption="User Group">
                                </dx:gridviewdatatextcolumn>
                            </columns>
                        </dx:aspxgridview>
                        <div id="aspxGridViewUpdateCancelButtonBar">
                            <div id="aspxGridViewCancelButton">
                                <dx:aspxgridviewtemplatereplacement id="CancelButton" replacementtype="EditFormCancelButton"
                                    runat="server" columnid="" tabindex="19" />
                            </div>
                            &nbsp;&nbsp;
                            <dx:aspxgridviewtemplatereplacement id="UpdateButton" replacementtype="EditFormUpdateButton"
                                runat="server" columnid="" tabindex="18" />

                                <dx:aspxdateedit id="LastModifiedOnDateTimeDateEdit" runat="server" clientvisible="false" 
                                    value='<%#Bind("LastModifiedOnDateTime") %>' readonly="true"> 
                                </dx:aspxdateedit>
                        </div>
                    </editform>
                </templates>
            </dx:aspxgridview>
        </detailrow>
    </templates>
</dx:aspxgridview>
<asp:linqdatasource id="ClientDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" orderby="ClientName" tablename="Clients" onselecting="ClientDataSource_Selecting">
</asp:linqdatasource>
<asp:linqdatasource id="ClientUserGroupDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" orderby="ClientUserGroupName" tablename="ClientUserGroups"
    onselecting="ClientUserGroupDataSource_Selecting">
</asp:linqdatasource>
<asp:linqdatasource id="UserDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" tablename="Users" onselecting="UserDataSource_Selecting">
</asp:linqdatasource>
