﻿<%@ control language="C#" autoeventwireup="true" codebehind="CardListControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.CardListControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>

<dx:aspxgridview id="CardGridView" runat="server" autogeneratecolumns="False" datasourceid="CardDataSource"
    keyfieldname="CardID" width="925px" OnRowDeleting="CardGridView_OnRowDeleting">
    <settingsbehavior columnresizemode="NextColumn" ConfirmDelete="True" enablerowhottrack="True" />
    <SettingsText ConfirmDelete="Are you sure you want to delete this card?"></SettingsText>
    <settingspager alwaysshowpager="True">
        <allbutton visible="True">
            <image tooltip="Show All Products">
            </image>
        </allbutton>
    </settingspager>
    <settings showfilterrow="True" showgrouppanel="True" />
    <columns>
        <dx:gridviewdatatextcolumn fieldname="CardID" readonly="True" visibleindex="0" visible="false">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn caption="" visibleindex="1">
            <dataitemtemplate>
                <dx:aspxhyperlink id="hyperLink" runat="server" text="Edit" navigateurl='<%#Eval("Url")%>'
                    clientvisible='<%#Eval("ShowEdit")%>'>
                </dx:aspxhyperlink>
            </dataitemtemplate>
        </dx:gridviewdatatextcolumn>
        <dx:GridViewCommandColumn ShowDeleteButton="True" VisibleIndex="2">            
        </dx:GridViewCommandColumn>
        <dx:gridviewdatatextcolumn fieldname="ProductName" caption="Product"
            visibleindex="3">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="CardShortcutCode" caption="Shortcut Code" visibleindex="4">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="Internal_CardTypeName" caption="Card Type"
            visibleindex="5">
        </dx:gridviewdatatextcolumn>        
        <dx:gridviewdatatextcolumn fieldname="DisplayCardName" caption="Card Name" visibleindex="6">
        </dx:gridviewdatatextcolumn>
    </columns>
</dx:aspxgridview>
<asp:linqdatasource id="CardDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" onselecting="CardDataSource_Selecting" tablename="Cards">
</asp:linqdatasource>
