﻿function OnUsePhysicalCheckBoxCheckChanged(s, e) {

    var checked = UsePhysicalCheckBox.GetValue();

    if (checked != null) {
        if (checked) {

            PostalAddress1TextBox.SetText(PhysicalAddress1TextBox.GetText());
            PostalAddress2TextBox.SetText(PhysicalAddress2TextBox.GetText());
            PostalStateProvinceTextBox.SetText(PhysicalStateProvinceTextBox.GetText());
            PostalCountryTextBox.SetText(PhysicalCountryTextBox.GetText());
            PostalPostCodeTextBox.SetText(PhysicalPostCodeTextBox.GetText());
        }
        else {
            PostalAddress1TextBox.SetText("");
            PostalAddress2TextBox.SetText("");
            PostalStateProvinceTextBox.SetText("");
            PostalCountryTextBox.SetText("");
            PostalPostCodeTextBox.SetText("");
        }
    }
}

var _updatePerformed = false;
function UpdateProductAccess() {
    _updatePerformed = true;
    ProductAccessGridView.PerformCallback("Update");
}

function CancelProductAccess() {
    ClientGridView.CollapseAllDetailRows();
}

function ProductAccessGridView_EndCallback(s, e) {
    if (_updatePerformed) {
        ClientGridView.CollapseAllDetailRows();
        _updatePerformed = false;
    }
}
function OnGetSelectedFieldValues(selectedValues) {
    if (selectedValues.length == 0) {
        return;
    }
    s = "";
    for (j = 0; j < selectedValues.length; j++) {
        s = s + selectedValues[i][j] + ";";
    }
    _selectedValuesRetrieved = true;

    return s;
}

function AnnualTuroverTextBox_Validate(s, e) {
    var annualTurnover = e.value;
    if (annualTurnover == null || annualTurnover == "") {
        return;
    }

//    if (!isNumber(annualTurnover)) {
//        e.isValid = false;
//    }

    var digits = "0123456789";
    for (var i = 0; i < annualTurnover.length; i++) {
        if (digits.indexOf(annualTurnover.charAt(i)) == -1) {
            e.isValid = false;
            break;
        }
    }

    if (e.isValid && annualTurnover.charAt(0) == "0") {
        annualTurnover = annualTurnover.replace(/^0+/, "");
        if (annualTurnover.length == 0) {
            e.vallue = null;
        }
    }
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
} 


function IsAccessGrantedCheckBox_ProductAccess_CheckChanged(s, e) {
    var isChecked = s.GetChecked();
    var clientInstanceName = s.cp_ClientInstanceName;
    var visibleIndex = GetVisibleIndex(clientInstanceName);
    var onlyShowClientCustomAvatarsCheckBox = GetOnlyShowClientCustomAvatarsCheckBox(visibleIndex);
    var clientSpecificProductThemeTextBox = GetClientSpecificProductThemeTextBox(visibleIndex);
    var questionMixComboBox = GetQuestionMixComboBox(visibleIndex);

    onlyShowClientCustomAvatarsCheckBox.SetEnabled(isChecked);
    clientSpecificProductThemeTextBox.SetEnabled(isChecked);
    questionMixComboBox.SetEnabled(isChecked);

    if (!isChecked) {                
        onlyShowClientCustomAvatarsCheckBox.SetChecked(false);
        questionMixComboBox.SetSelectedIndex(-1);
        clientSpecificProductThemeTextBox.SetText("");
    }
}

function OnUpdateButtonClick() {
    var count = DistributorAccessGridView.GetSelectedRowCount();

    var messageElement = DistributorAccessValidateMessageLabel.GetMainElement();
    if (count == 0) {
        messageElement.style.color = 'Red';
    }
    else {
        messageElement.style.color = 'Black';
    }

    if (ASPxClientEdit.ValidateGroup('EditForm', true) && count > 0) {
            ClientGridView.UpdateEdit();
    }    
}

function DistributorAccessGridView_Validate(s,e) {
    var count = DistributorAccessGridView.GetSelectedRowCount();

    if (count <= 0) {
        e.isValid = false;
        DistributorAccessValidateMessageLabel.SetVisible(true);
    }
    else {
        e.isValid = true;
        DistributorAccessValidateMessageLabel.SetVisible(false);
    }
}

function DistributorAccessGridView_SelectionChanged(s, e) {
    if (e.isSelected) {
        var isAccessGrantedCheckBox = GetDistributorIsAccessGrantedCheckBox(e.visibleIndex);
        isAccessGrantedCheckBox.SetChecked(true);
    }
}

function IsAccessGrantedCheckBox_DistributorAccess_CheckChanged(s, e) {
    var isChecked = s.GetChecked();
    var count = DistributorAccessGridView.GetSelectedRowCount();

    if (isChecked) {
        if (count == 0) {
            DistributorAccessGridView.SelectRowOnPage(s.cp_VisibleIndex);
        }
    }
    else {
        if (count > 0) {
            var selectedKeys = DistributorAccessGridView.GetSelectedKeysOnPage();
            //as we know this is a single select grid
            var rowKey = selectedKeys[0];            
            if (rowKey == s.cp_DistributorId) {
                s.SetChecked(true);
            }            
        }        
    }
}

function GetVisibleIndex(clientInstanceName) {
    //I assume that the clientinstancename is in the format NameObject_1 where 1 is the row number
    var startIndex = clientInstanceName.length - 1;
    var result = clientInstanceName.slice(startIndex);
    return result;
}

function GetDistributorIsAccessGrantedCheckBox(visibleIndex) {
    return eval("DistributorIsAccessGrantedCheckBox_" + visibleIndex);
}

function GetOnlyShowClientCustomAvatarsCheckBox(visibleIndex) {
    return eval("OnlyShowClientCustomAvatarsCheckBox_" + visibleIndex);
}

function GetClientSpecificProductThemeTextBox(visibleIndex) {
    return eval("ClientSpecificProductThemeTextBox_" + visibleIndex);
}

function GetQuestionMixComboBox(visibleIndex) {
    return eval("QuestionMixComboBox_" + visibleIndex);
}

var _hasCheckedSession = false;
function UploadControl_FileUploadStart(s, e) {
    if (!_hasCheckedSession) {
        e.cancel = true;
        CheckSessionCallback.PerformCallback();
    }    
}


function CheckSessionCallback_CallbackComplete(s, e) {
    _hasCheckedSession = true;
    UploadControl.Upload();
}

//Methods relating to the UploadPopup Control
var _uploadTYpe = "";
function UploadControl_FileUploadComplete(s, e) {
    var data = e.callbackData.split(";");

    if (data != 'undefined' || data.length > 0) {
        if (_uploadTYpe == "ClientLogo") {
            var editingId = data[2];

            var logoHyperLink = GetControl("ClientLogoMediaHyperLink_" + editingId);
            logoHyperLink.SetNavigateUrl(data[0]);
            logoHyperLink.SetText(data[1]);
            logoHyperLink.SetVisible(true);

            var browseButton = GetControl("ClientLogoBrowseButton_" + editingId);
            browseButton.SetVisible(false);

            var removeHyperLink = GetControl("ClientLogoMediaHyperRemoveHyperlink_" + editingId);
            removeHyperLink.SetVisible(true);

            var logoUrlText = GetControl("ClientLogoImageUrl_" + editingId);
            logoUrlText.SetText(data[3]);

            var logoFriendlyText = GetControl("ClientLogoImageFriendlyName_" + editingId);
            logoFriendlyText.SetText(data[1]);
        }

        UploadPopupControl.Hide();
    }
}

function UploadPopupControl_Closing(s, e) {
    _uploadTYpe = "";
    UploadControl.ClearText();
    UploadButton.SetEnabled(UploadControl.GetText(0) != "");
}

function UploadControl_TextChanged(s, e) {
    UploadButton.SetEnabled(UploadControl.GetText(0) != "");
    _hasCheckedSession = false;
}

function ShowUploadWindow(uploadType) {
    _uploadTYpe = uploadType;
    UploadPopupControl.Show();
}

function UploadButton_Click(s, e) {
    UploadControl.Upload();
}

function ClientLogoMediaHyperRemoveHyperlink_Click(s, e, editingId) {

    var logoHyperLink = GetControl("ClientLogoMediaHyperLink_" + editingId);
    logoHyperLink.SetNavigateUrl("#");
    logoHyperLink.SetText("");    
    
    var browseButton = GetControl("ClientLogoBrowseButton_" + editingId);
    browseButton.SetVisible(true);
    
    var logoUrlText = GetControl("ClientLogoImageUrl_" + editingId);
    logoUrlText.SetText(null);

    var logoFriendlyText = GetControl("ClientLogoImageFriendlyName_" + editingId);
    logoFriendlyText.SetText(null);
    
    s.SetVisible(false);
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function GetControl(controlName) {
    return eval(controlName);
}

var clientId;
function CustomButtonClick(s, e) {
    if (e.buttonID == 'DeleteButton') {
        var rowVisibleIndex = e.visibleIndex;
        clientId = ClientGridView.GetRowKey(rowVisibleIndex);
        DeleteNotificationPopupControl.Show();
    }
    else {
        clientId = -1;
    }
}
function DeleteClientButton_Click(s, e) {
    DeleteClientCallback.PerformCallback(clientId);
    DeleteNotificationPopupControl.Hide();
    LoadingPanel.Show();
}

function CancelDeleteClientButton_Click(s, e) {
    DeleteNotificationPopupControl.Hide();
    clientId = null;
}

function DeleteClientCallback_CallbackComplete(s, e) {
    var message = e.result;
    LoadingPanel.Hide();
    
    if (message == null) {
        PopupLabel.SetText(null);
        NotificationPopupControl.Hide();
        ClientGridView.Refresh();        
    }
    else {
        PopupLabel.SetText(message);
        NotificationPopupControl.Show();
    }    
}

