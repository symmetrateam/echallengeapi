﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ComLib;
using DevExpress.Web;
using Diversity.Common;
using Diversity.DataModel.SqlRepository;
using Diversity.Presentation.Web.Converters;
using FileHelpers;
using Diversity.Common.Constants;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class BulkUserImportControl : System.Web.UI.UserControl
    {      
        [DelimitedRecord(",")]
        class User
        {
            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _firstname;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _lastname;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _email;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            [FieldConverter(ConverterKind.Date, "yyyy/MM/dd")] 
            private DateTime? _dateOfBirth;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            [FieldConverter(typeof(StringToGenderConverter))]
            private string _userGender;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _mobile;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _workEmail;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _telNo1;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _telNo2;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _workMobile;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _jobTitle;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _salaryNo;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _subsidiary;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _businessUnit;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _location;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _costCentre;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _division;

            [FieldTrim(TrimMode.Both)]
            [FieldQuoted('"', QuoteMode.OptionalForBoth)]
            private string _userGroup;

            public string Firstname { get { return _firstname; } set { _firstname = value; } }
            public string Lastname { get { return _lastname; } set { _lastname = value; } }
            public string Email { get { return _email; } set { _email = value; } }
            public DateTime? DateOfBirth { get { return _dateOfBirth; } set { _dateOfBirth = value; } }
            public string UserGender { get { return _userGender; } set { _userGender = value; } }
            public string Mobile { get { return _mobile; } set { _mobile = value; } }
            public string WorkEmail { get { return _workEmail; } set { _workEmail = value; } }
            public string TelNo1 { get { return _telNo1; } set { _telNo1 = value; } }
            public string TelNo2 { get { return _telNo2; } set { _telNo2 = value; } }
            public string WorkMobile { get { return _workMobile; } set { _workMobile = value; } }
            public string JobTitle { get { return _jobTitle; } set { _jobTitle = value;} }
            public string SalaryNo { get { return _salaryNo; } set { _salaryNo = value;} }
            public string Subsidiary { get { return _subsidiary; } set { _subsidiary = value;} }
            public string BusinessUnit { get { return _businessUnit; } set { _businessUnit = value;} }
            public string Location { get { return _location; } set { _location = value;} }
            public string CostCentre { get { return _costCentre; } set { _costCentre = value;} }
            public string Division { get { return _division; } set { _division = value;} }
            public string UserGroup { get { return _userGroup; } set { _userGroup = value;} }
        }

        private const string VALID_USER_DATA = "C1856174-B588-4840-9FB9-659B433FCE31";
        private const string INVALID_USER_DATA = "98E428B4-4FE8-4D17-B1BC-530393B0AD0D";

        List<User> ValidUserData
        {
            get
            {
                if (Session[VALID_USER_DATA] == null)
                {
                    Session[VALID_USER_DATA] = CreateUserData();
                }

                return Session[VALID_USER_DATA] as List<User>;
            }
            set { Session[VALID_USER_DATA] = value; }
        }

        List<User> InvalidUserData
        {
            get
            {
                if (Session[INVALID_USER_DATA] == null)
                {
                    Session[INVALID_USER_DATA] = CreateUserData();
                }

                return Session[INVALID_USER_DATA] as List<User>;
            }
            set { Session[INVALID_USER_DATA] = value; }
        }

        private List<User> CreateUserData()
        {
            return new List<User>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);

            TemplateDownloadHyperLink.NavigateUrl = ResolveUrl("~/MyResources/Templates/BulkImportTemplate.csv");
            if (!Page.IsPostBack)
            {
                CleanUp();
            }
        }

        private void CleanUp()
        {
            InvalidUserData = null;
            ValidUserData = null;
        }

        protected void UserUploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            if (!e.IsValid)
            {
                return;
            }

            using (var stream = new MemoryStream(e.UploadedFile.FileBytes))
            {
                ValidUserData.Clear();
                InvalidUserData.Clear();
                var validData = new List<User>();
                var invalidData = new List<User>();

                using (var reader = new StreamReader(stream))
                {
                    var fileHelper = new FileHelperEngine<User>();
                    fileHelper.Options.IgnoreFirstLines = 1;
                    fileHelper.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
                    var users = fileHelper.ReadStream(reader);

                    if (fileHelper.ErrorManager.HasErrors)
                    {
                        var builder = new StringBuilder();
                        var errorLineNumbers = string.Empty;
                        foreach (var error in fileHelper.ErrorManager.Errors)
                        {
                            builder.Append(error.LineNumber);
                            builder.Append(", ");
                        }

                        if (builder.Length > 0)
                        {
                            errorLineNumbers = builder.ToString();
                            errorLineNumbers = errorLineNumbers.Substring(0, errorLineNumbers.Length - 2);
                        }

                        e.IsValid = false;
                        throw new Exception(string.Format("{0}{1} {2}", Resources.Exceptions.ImportFileErrorsOnLine, Resources.Diversity.ColonText , errorLineNumbers));
                    }

                    foreach (var user in users)
                    {
                        if (user.Firstname.Length > 0 &&
                            user.Lastname.Length > 0 &&
                            user.Email.Length > 0 &&
                            Regex.IsMatch(user.Email, ConfigurationHelper.EmailValidationRegex))
                        {
                            var contains = validData.Where(c => c.Email.ToLower() == user.Email.ToLower());
                            if (!contains.Any())
                            {
                                validData.Add(user);
                            }
                        }
                        else
                        {
                            var contains = invalidData.Where(c => c.Email.ToLower() == user.Email.ToLower());

                            if (!contains.Any())
                            {
                                invalidData.Add(user);
                            }
                        }
                    }
                    reader.Close();
                }

                e.IsValid = true;
                ValidUserData = validData;
                InvalidUserData = invalidData;
                e.CallbackData = string.Format(@"{0};{1}", ValidUserData.Count, InvalidUserData.Count);
                stream.Close();
                
            }
        }

        protected void UserExceptionDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = InvalidUserData;
        }

        protected void UserExceptionGridView_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            UserExceptionGridView.DataBind();
        }

        protected void UserExceptionGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var email = Convert.ToString(e.Keys[0]);

            if (email.Length < 0)
            {
                return;
            }

            var user = InvalidUserData.FirstOrDefault(u => u.Email.ToLower() == email.ToLower());
            InvalidUserData.Remove(user);

            e.Cancel = true;
            gridView.CancelEdit();
            gridView.DataBind();
        }

        protected void UserExceptionGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var email = Convert.ToString(e.Keys[0]);

            var user = InvalidUserData.FirstOrDefault(u => u.Email.ToLower() == email.ToLower());

            var newEmail = string.Empty;

            if (e.NewValues["Email"] != null)
            {
                newEmail = e.NewValues["Email"] as string;
            }

            var exists = ValidUserData.Where(u => u.Email.ToLower() == user.Email.ToLower() || u.Email.ToLower() == newEmail.ToLower());

            if (exists.Any())
            {
                throw new Exception(Resources.Exceptions.UserAlreadyExists);
            }

            if (e.NewValues["Firstname"] != null)
            {
                user.Firstname = e.NewValues["Firstname"] as string;
            }

            if (e.NewValues["Lastname"] != null)
            {
                user.Lastname = e.NewValues["Lastname"] as string;
            }

            user.Email = newEmail;

            if (e.NewValues["DateOfBirth"] != null)
            {
                user.DateOfBirth = e.NewValues["DateOfBirth"] as DateTime?;
            }

            if (e.NewValues["UserGender"] != null)
            {
                user.UserGender = e.NewValues["UserGender"] as string;
            }

            if (e.NewValues["Mobile"] != null)
            {
                user.Mobile = e.NewValues["Mobile"] as string;
            }

            if (e.NewValues["WorkEmail"] != null)
            {
                user.WorkEmail = e.NewValues["WorkEmail"] as string;
            }

            if (e.NewValues["TelNo1"] != null)
            {
                user.TelNo1 = e.NewValues["TelNo1"] as string;
            }

            if (e.NewValues["TelNo2"] != null)
            {
                user.TelNo2 = e.NewValues["TelNo2"] as string;
            }

            if (e.NewValues["WorkMobile"] != null)
            {
                user.WorkMobile = e.NewValues["WorkMobile"] as string;
            }

            if (e.NewValues["JobTitle"] != null)
            {
                user.JobTitle = e.NewValues["JobTitle"] as string;
            }

            if (e.NewValues["SalaryNo"] != null)
            {
                user.SalaryNo = e.NewValues["SalaryNo"] as string;
            }

            if (e.NewValues["Subsidiary"] != null)
            {
                user.Subsidiary = e.NewValues["Subsidiary"] as string;
            }

            if (e.NewValues["BusinessUnit"] != null)
            {
                user.BusinessUnit = e.NewValues["BusinessUnit"] as string;
            }
            
            if (e.NewValues["Location"] != null)
            {
                user.Location = e.NewValues["Location"] as string;
            }

            if (e.NewValues["CostCentre"] != null)
            {
                user.CostCentre = e.NewValues["CostCentre"] as string;
            }

            if (e.NewValues["Division"] != null)
            {
                user.Division = e.NewValues["Division"] as string;
            }

            if (e.NewValues["UserGroup"] != null)
            {
                user.UserGroup = e.NewValues["UserGroup"] as string;
            }


            ValidUserData.Add(user);
            InvalidUserData.Remove(user);

            e.Cancel = true;
            gridView.CancelEdit();
            gridView.DataBind();
        }

        protected void SaveCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            var callback = source as ASPxCallback;
            callback.JSProperties["cpTotal"] = null;
            callback.JSProperties["cpNew"] = null;
            callback.JSProperties["cpChanged"] = null;
            callback.JSProperties["cpUnChanged"] = null;

            var amendedUsers = new List<User>();
            var newUsers = 0;
            var editingUser = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;

            var dataContext = new EChallengeDataContext();

            try
            {

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        var clientId = (int?)ClientComboBox.SelectedItem.Value;
                        var clientUserGroup = dataContext.ClientUserGroups.FirstOrDefault(
                            c =>
                                c.ClientID == clientId.Value &&
                                c.ClientUserGroupName == SecurityGroupResources.ClientUserGroupNameAllUsers);

                        foreach (var validUser in ValidUserData)
                        {
                            var username = Membership.GetUserNameByEmail(validUser.Email);
                            

                            if (!string.IsNullOrEmpty(username))
                            {
                                var mappings = from uc in dataContext.User2Clients
                                               join u in dataContext.Users on uc.UserID equals u.UserId
                                               where uc.ClientID == clientId && u.LoweredUserName == username.ToLower()
                                               select uc;

                                if (!mappings.Any())
                                {
                                    var existingUser =
                                        dataContext.Users.FirstOrDefault(u => u.LoweredUserName == username.ToLower());

                                    if (clientId.HasValue)
                                    {
                                        existingUser.Clients.Add
                                            (
                                                new User2Client()
                                                {
                                                    ClientID = clientId.Value,
                                                    CreatedOnDateTime = currentDateTime,
                                                    LastModifiedDateTime = currentDateTime,
                                                    UserID = existingUser.UserId,
                                                    UserID_CreatedBy = editingUser,
                                                    UserID_LastModifiedBy = editingUser
                                                }
                                            );

                                        if (clientUserGroup != null)
                                        {
                                            AddUserToClientUserGroup(existingUser, clientUserGroup, currentDateTime,
                                                editingUser);
                                        }

                                        amendedUsers.Add(validUser);
                                    }
                                }

                                continue;
                            }

                            var password = string.Empty;
                            var passwordValid = false;
                            while (!passwordValid)
                            {
                                password = Membership.GeneratePassword(15,
                                                                       Membership.MinRequiredNonAlphanumericCharacters);

                                passwordValid = Regex.IsMatch(password, Membership.PasswordStrengthRegularExpression);
                            }

                            //we need to create a membership user            
                            MembershipUser membershipUser = Membership.CreateUser(validUser.Email, password, validUser.Email);
                            membershipUser.IsApproved = true;
                            Membership.UpdateUser(membershipUser);

                            //now add new roles
                            //first add to default user role
                            Roles.AddUserToRole(validUser.Email, GlobalConstants.USERS_ROLE);


                            var userId = new Guid(membershipUser.ProviderUserKey.ToString());

                            var user = dataContext.Users.FirstOrDefault(u => u.UserId == userId);                                       

                            //set fields                 
                            user.UserFirstName = validUser.Firstname;
                            user.UserLastName = validUser.Lastname;
                            user.UserPersonalEmail1 = validUser.Email;
                            user.UserPersonalMobile = validUser.Mobile;
                            user.UserDateOfBirth = validUser.DateOfBirth;
                            user.UserGender = validUser.UserGender;
                            user.UserWorkEmail = validUser.WorkEmail;
                            user.UserWorkTel1 = validUser.TelNo1;
                            user.UserWorkTel2 = validUser.TelNo2;
                            user.UserWorkMobile = validUser.WorkMobile;
                            user.ClientJobTitle = validUser.JobTitle;
                            user.ClientUniqueSalaryNo = validUser.SalaryNo;
                            user.ClientSubsidiaryCompanyName = validUser.Subsidiary;
                            user.ClientBusinessUnit = validUser.BusinessUnit;
                            user.ClientLocation = validUser.Location;
                            user.ClientCostCentre = validUser.CostCentre;
                            user.ClientDivision = validUser.Division;
                            user.ClientUserGroup = validUser.UserGroup;

                            user.UserID_CreatedBy = editingUser;
                            user.CreatedOnDateTime = currentDateTime;

                            user.UserID_LastModifiedBy = editingUser;
                            user.LastModifiedOnDateTime = currentDateTime;



                            if (clientId.HasValue)
                            {
                                user.Clients.Add
                                    (
                                        new User2Client()
                                        {
                                            ClientID = clientId.Value,
                                            CreatedOnDateTime = currentDateTime,
                                            LastModifiedDateTime = currentDateTime,
                                            UserID = user.UserId,
                                            UserID_CreatedBy = editingUser,
                                            UserID_LastModifiedBy = editingUser
                                        }
                                    );

                                if (clientUserGroup != null)
                                {
                                    AddUserToClientUserGroup(user, clientUserGroup, currentDateTime,
                                                editingUser);
                                }
                            }

                            //var selectedDistributors = GetSelectedDistributors();
                            //foreach (var distributor in selectedDistributors)
                            //{
                            //    var mapping = new User2Distributor()
                            //                      {
                            //                          CreatedOnDateTime = currentDateTime,
                            //                          DistributorID = distributor,
                            //                          LastModifiedDateTime = currentDateTime,
                            //                          UserID = user.UserId,
                            //                          UserID_CreatedBy = editingUser,
                            //                          UserID_LastModifiedBy = editingUser
                            //                      };
                            //    user.User2Distributors.Add(mapping);
                            //}          

                            newUsers++;
                        }

                        dataContext.SaveChanges();
                        transaction.Commit();

                        callback.JSProperties["cpTotal"] = ValidUserData.Count;
                        callback.JSProperties["cpNew"] = newUsers;
                        callback.JSProperties["cpChanged"] = amendedUsers.Count;
                        callback.JSProperties["cpUnChanged"] = ValidUserData.Count - amendedUsers.Count;

                        CleanUp();
                    }
                    catch(Exception ex)
                    {
                        transaction.Rollback();
                        e.Result = "Failed";
                        throw;        
                    }
                    
                }
            }
            catch (Exception)
            {
                e.Result = "Failed";
                throw;
            }
        }

        void AddUserToClientUserGroup(DataModel.SqlRepository.User user, ClientUserGroup clientUserGroup, DateTime lastModifiedDateTime, Guid lastModifiedByUserId)
        {
            if (!user.ClientUserGroups.Any(
                                                   c =>
                                                       c.ClientUserGroupID == clientUserGroup.ClientUserGroupID &&
                                                       c.UserID == user.UserId))
            {
                user.ClientUserGroups.Add(
                    new User2ClientUserGroup()
                    {
                        ClientUserGroupID = clientUserGroup.ClientUserGroupID,
                        CreatedOnDateTime = lastModifiedDateTime,
                        LastModifiedOnDateTime = lastModifiedDateTime,
                        UserID = user.UserId,
                        UserID_CreatedBy = lastModifiedByUserId,
                        UserID_LastModifiedBy = lastModifiedByUserId
                    });
            }
        }

        protected void ClientDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = (from c in dataContext.Clients
                            select c).Distinct();
            }
            else
            {
                e.Result = (from c in dataContext.Clients
                            from d in c.Client2Distributors
                            join u in dataContext.User2Distributors on d.DistributorID equals u.DistributorID
                            where u.UserID == userId
                            select c).Distinct();
            }
        }

        protected void UserExceptionGridView_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            foreach(GridViewColumn column in gridView.Columns)
            {
                var dataColumn = column as GridViewDataColumn;
                
                if(dataColumn == null)
                {
                    continue;                    
                }

                if ((String.Compare(dataColumn.FieldName, "Email", System.StringComparison.Ordinal) == 0 ||
                    String.Compare(dataColumn.FieldName, "Lastname", System.StringComparison.Ordinal) == 0 ||
                    String.Compare(dataColumn.FieldName, "Firstname", System.StringComparison.Ordinal) == 0)
                    && e.NewValues[dataColumn.FieldName] == null)
                {
                    e.Errors[dataColumn] = Resources.Exceptions.ThisIsARequiredField;
                }
            }

            if(e.Errors.Count > 0)
            {
                e.RowError = Resources.Exceptions.CompleteAllFields;
            }

            if(e.NewValues["Firstname"] != null && Convert.ToString(e.NewValues["Firstname"]).Trim().Length <=0)
            {
                AddError(e.Errors, gridView.Columns["Firstname"], Resources.Exceptions.ThisIsARequiredField);
            }

            if (e.NewValues["Lastname"] != null && Convert.ToString(e.NewValues["Lastname"]).Trim().Length <= 0)
            {
                AddError(e.Errors, gridView.Columns["Lastname"], Resources.Exceptions.ThisIsARequiredField);
            }

            if (e.NewValues["Email"] != null && Convert.ToString(e.NewValues["Email"]).Trim().Length <= 0)
            {
                AddError(e.Errors, gridView.Columns["Email"], Resources.Exceptions.ThisIsARequiredField);
            }

            if (e.NewValues["Email"] != null && Convert.ToString(e.NewValues["Email"]).Trim().Length > 0 && !Regex.IsMatch(Convert.ToString(e.NewValues["Email"]).Trim(), ConfigurationHelper.EmailValidationRegex))
            {
                AddError(e.Errors, gridView.Columns["Email"], Resources.Exceptions.InvalidEmailAddress);
            }

            if (e.NewValues["WorkEmail"] != null && Convert.ToString(e.NewValues["WorkEmail"]).Trim().Length > 0 && !Regex.IsMatch(Convert.ToString(e.NewValues["WorkEmail"]).Trim(), ConfigurationHelper.EmailValidationRegex))
            {
                AddError(e.Errors, gridView.Columns["WorkEmail"], Resources.Exceptions.InvalidEmailAddress);
            }
        }

        void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if(errors.ContainsKey(column))
            {
                return;
            }
            errors[column] = errorText;
        }

        protected void UserExceptionGridView_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            if(!gridView.IsNewRowEditing)
            {
                gridView.DoRowValidation();
            }
        }       

        protected void ValidUsersGridView_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {            
            ValidUsersGridView.DataBind();
        }

        protected void ValidUsersGridView_OnDataBinding(object sender, EventArgs e)
        {
            ValidUsersGridView.DataSource = ValidUserData;
        }
    }
}