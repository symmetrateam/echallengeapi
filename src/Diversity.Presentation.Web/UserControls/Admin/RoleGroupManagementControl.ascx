﻿<%@ control language="C#" autoeventwireup="true" codebehind="RoleGroupManagementControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.RoleGroupManagementControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>

<%@ register src="../DualListBoxControl.ascx" tagname="DualListBoxControl" tagprefix="uc1" %>
<dx:aspxgridview id="RoleGroupGridView" runat="server" autogeneratecolumns="False"
    keyfieldname="UserRolesGroupName" width="100%" datasourceid="RoleGroupDataSource"
    oninitnewrow="RoleGroupGridView_InitNewRow" onrowinserting="RoleGroupGridView_RowInserting"
    onstartrowediting="RoleGroupGridView_StartRowEditing" clientinstancename="RoleGroupGridView"
    onrowupdating="RoleGroupGridView_RowUpdating" onrowvalidating="RoleGroupGridView_RowValidating">
    <columns>
        <dx:gridviewcommandcolumn visibleindex="0" width="100px" ShowEditButton="true" ShowNewButton="false" ShowDeleteButton="true" ShowClearFilterButton="True">
            <headercaptiontemplate>
                <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="AddNewRow(RoleGroupGridView);return false;" text="New"/>
            </headercaptiontemplate>
        </dx:gridviewcommandcolumn>
        <dx:gridviewdatatextcolumn fieldname="UserRolesGroupName" caption="Group Name" visibleindex="1">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn caption="Description" visibleindex="2">
        </dx:gridviewdatatextcolumn>
    </columns>
    <settingsbehavior enablerowhottrack="True" columnresizemode="NextColumn" />
    <settingspager alwaysshowpager="True">
        <allbutton visible="True">
            <image tooltip="Show All Role Groups">
            </image>
        </allbutton>
    </settingspager>
    <settings showfilterrow="True" showgrouppanel="True" />
    <styles>
        <alternatingrow enabled="True">
        </alternatingrow>
    </styles>
    <templates>
        <editform>
            <table>
                <tr>
                    <td>
                        <dx:aspxlabel id="GroupNameLabel" runat="server" text="Group Name" />
                    </td>
                    <td>
                        <dx:aspxtextbox id="GroupNameTextBox" runat="server" text='<%# Bind("UserRolesGroupName") %>'
                            validationsettings-validationgroup='<%# Container.ValidationGroup %>'>
                            <validationsettings setfocusonerror="true">
                                <requiredfield isrequired="true" errortext="Group name is a required field" />
                            </validationsettings>
                        </dx:aspxtextbox>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <uc1:duallistboxcontrol id="RolesDualListBox" runat="server" ondatabinding="RolesDualListBox_DataBinding" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="aspxGridViewUpdateCancelButtonBar">
                            <div id="aspxGridViewCancelButton">
                                <dx:aspxgridviewtemplatereplacement id="CancelButton" replacementtype="EditFormCancelButton"
                                    runat="server" columnid=""  tabindex="19" />
                            </div>
                            &nbsp;&nbsp;
                            <dx:aspxgridviewtemplatereplacement id="UpdateButton" replacementtype="EditFormUpdateButton"
                                runat="server" columnid=""  tabindex="18" />
                        </div>
                    </td>
                </tr>
            </table>
        </editform>
    </templates>
</dx:aspxgridview>
<asp:linqdatasource id="RoleGroupDataSource" runat="server" onselecting="RoleGroupDataSource_Selecting">
</asp:linqdatasource>
