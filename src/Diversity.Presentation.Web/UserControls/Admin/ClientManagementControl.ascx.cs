﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.XtraRichEdit.Model;
using Diversity.Common;
using Diversity.Common.Constants;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class ClientManagementControl : System.Web.UI.UserControl
    {
        private const string EDITING_CLIENT_ID_KEY = "EditingClientId";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                EditingClientID = null;
            }
        }

        int? EditingClientID
        {
            get
            {
                if (Session[EDITING_CLIENT_ID_KEY] == null)
                {
                    return -1;
                }
                else
                {
                    return (int)Session[EDITING_CLIENT_ID_KEY];
                }
            }
            set { Session[EDITING_CLIENT_ID_KEY] = value; }
        }


        protected void ClientDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = (from c in dataContext.Clients
                            select c).Distinct();
            }
            else
            {
                e.Result = (from c in dataContext.Clients
                            from d in c.Client2Distributors
                            join u in dataContext.User2Distributors on d.DistributorID equals u.DistributorID
                            where u.UserID == userId
                            select c).Distinct();
            }
        }

        protected void ClientGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;
            var clientGridView = sender as ASPxGridView;

            using (var dataContext = new EChallengeDataContext())
            {

                var externalId = Guid.NewGuid().ToString();
                externalId = externalId.Replace("-", string.Empty);

                var client = new Client()
                                 {
                                     ClientAnnualTurnover = e.NewValues["ClientAnnualTurnover"] as decimal?,
                                     ClientIndustrySector = e.NewValues["ClientIndustrySector"] == null ? null : (e.NewValues["ClientIndustrySector"] as string).Trim(),
                                     ClientName = e.NewValues["ClientName"] == null ? null : (e.NewValues["ClientName"] as string).Trim(),
                                     ClientNoOfEmployees = e.NewValues["ClientNoOfEmployees"] as int?,
                                     ClientNotes = e.NewValues["ClientNotes"] == null ? null : (e.NewValues["ClientNotes"] as string).Trim(),
                                     ClientTel1 = e.NewValues["ClientTel1"] == null ? null : (e.NewValues["ClientTel1"] as string).Trim(),
                                     ClientTel2 = e.NewValues["ClientTel2"] == null ? null : (e.NewValues["ClientTel2"] as string).Trim(),
                                     ClientWebsite = e.NewValues["ClientWebsite"] == null ? null : (e.NewValues["ClientWebsite"] as string).Trim(),
                                     CreatedOnDateTime = modifiedOn,
                                     LastModifiedOnDateTime = modifiedOn,
                                     PhysicalAddress1 = e.NewValues["PhysicalAddress1"] == null ? null : (e.NewValues["PhysicalAddress1"] as string).Trim(),
                                     PhysicalAddress2 = e.NewValues["PhysicalAddress2"] == null ? null : (e.NewValues["PhysicalAddress2"] as string).Trim(),
                                     PhysicalCountry = e.NewValues["PhysicalCountry"] == null ? null : (e.NewValues["PhysicalCountry"] as string).Trim(),
                                     PhysicalPostalCode = e.NewValues["PhysicalPostalCode"] == null ? null : (e.NewValues["PhysicalPostalCode"] as string).Trim(),
                                     PhysicalStateProvince = e.NewValues["PhysicalStateProvince"] == null ? null : (e.NewValues["PhysicalStateProvince"] as string).Trim(),
                                     PostalAddress1 = e.NewValues["PostalAddress1"] == null ? null : (e.NewValues["PostalAddress1"] as string).Trim(),
                                     PostalAddress2 = e.NewValues["PostalAddress2"] == null ? null : (e.NewValues["PostalAddress2"] as string).Trim(),
                                     PostalCountry = e.NewValues["PostalCountry"] == null ? null : (e.NewValues["PostalCountry"] as string).Trim(),
                                     PostalPostalCode = e.NewValues["PostalPostalCode"] == null ? null : (e.NewValues["PostalPostalCode"] as string).Trim(),
                                     PostalStateProvince = e.NewValues["PostalStateProvince"] == null ? null : (e.NewValues["PostalStateProvince"] as string).Trim(),
                                     ThemeID = e.NewValues["ThemeID"] as int?,
                                     ExternalID = externalId,
                                     UserID_CreatedBy = userId,
                                     UserID_LastModifiedBy = userId
                                 };

                if (e.NewValues["ClientLogoImageURL"] != null)
                {
                    var logoUrl = (e.NewValues["ClientLogoImageURL"] as string).Trim();
                    var logoFriendlyName = (e.NewValues["ClientLogoImageFriendlyName"] as string).Trim();

                    if (!string.IsNullOrEmpty(logoUrl) && !string.IsNullOrWhiteSpace(logoUrl))
                    {

                        client.ClientLogoImageURL = logoUrl;
                        client.ClientLogoImageFriendlyName = logoFriendlyName;
                        var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), logoUrl);
                        var destinationPath = Path.Combine(
                                                      MapPath(ConfigurationHelper.LiveUploadLocation),
                                                      logoUrl);

                        File.Move(tempPath, destinationPath);
                    }
                }

                //Add the default client user group 
                var clientUserGroup = new ClientUserGroup()
                                      {
                                          LastModifiedOnDateTime = modifiedOn,
                                          UserID_LastModifiedBy = userId,
                                          UserID_CreatedBy = userId,
                                          CreatedOnDateTime = modifiedOn,
                                          ClientID = client.ClientID,
                                          ClientUserGroupName = SecurityGroupResources.ClientUserGroupNameAllUsers,
                                          ClientUserGroupDescription = SecurityGroupResources.ClientUserGroupDescAllUsers
                                      };

                client.ClientUserGroups.Add(clientUserGroup);

                /*Insert distributor access data*/
                var distributorGrid =
                    clientGridView.FindEditFormTemplateControl(@"DistributorAccessGridView") as ASPxGridView;

                int start = distributorGrid.PageIndex * distributorGrid.SettingsPager.PageSize;
                int end = (distributorGrid.PageIndex + 1) * distributorGrid.SettingsPager.PageSize;

                var isGrantedColumn = distributorGrid.Columns["IsGranted"] as GridViewDataColumn;

                for (var i = start; i < end; i++)
                {
                    var isGrantedCheckBox =
                        distributorGrid.FindRowCellTemplateControl(i, isGrantedColumn, "IsAcessGrantedCheckBox") as
                        ASPxCheckBox;

                    var id = Convert.ToInt32(distributorGrid.GetRowValues(i, distributorGrid.KeyFieldName));
                    var isOwner = distributorGrid.Selection.IsRowSelected(i);

                    if (isGrantedCheckBox != null && isGrantedCheckBox.Checked)
                    {
                        var mapping = new Client2Distributor()
                                          {
                                              ClientID = client.ClientID,
                                              CreatedOnDateTime = modifiedOn,
                                              DistributorID = id,
                                              DistributorIsClientOwner = isOwner,
                                              LastModifiedOnDateTime = modifiedOn,
                                              UserID_CreatedBy = userId,
                                              UserID_LastModifiedBy = userId
                                          };

                        client.Client2Distributors.Add(mapping);
                    }
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.Clients.Add(client);
                        dataContext.SaveChanges();

                        transaction.Commit();
                        e.Cancel = true;
                        clientGridView.CancelEdit();
                        clientGridView.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        protected void ClientGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;
            var clientId = (int)e.Keys[0];
            var clientGridView = sender as ASPxGridView;

            var lastModifiedOn = (DateTime)e.OldValues["LastModifiedOnDateTime"];


            using (var dataContext = new EChallengeDataContext())
            {
                var client = (from c in dataContext.Clients
                              where c.ClientID == clientId
                              select c).FirstOrDefault();

                if (client == null)
                {
                    return;
                }

                if (lastModifiedOn != client.LastModifiedOnDateTime)
                {
                    throw new Exception(Resources.Exceptions.ConcurrencyException);
                }

                if (e.NewValues["ClientAnnualTurnover"] != null)
                {
                    client.ClientAnnualTurnover = e.NewValues["ClientAnnualTurnover"] as decimal?;
                }

                if (e.NewValues["ClientIndustrySector"] != null)
                {
                    client.ClientIndustrySector = (e.NewValues["ClientIndustrySector"] as string).Trim();
                }

                if (e.NewValues["ClientName"] != null)
                {
                    client.ClientName = (e.NewValues["ClientName"] as string).Trim();
                }

                if (e.NewValues["ClientNoOfEmployees"] != null)
                {
                    client.ClientNoOfEmployees = e.NewValues["ClientNoOfEmployees"] as int?;
                }

                if (e.NewValues["ClientNotes"] != null)
                {
                    client.ClientNotes = (e.NewValues["ClientNotes"] as string).Trim();
                }

                if (e.NewValues["ClientTel1"] != null)
                {
                    client.ClientTel1 = (e.NewValues["ClientTel1"] as string).Trim();
                }

                if (e.NewValues["ClientTel2"] != null)
                {
                    client.ClientTel2 = (e.NewValues["ClientTel2"] as string).Trim();
                }

                if (e.NewValues["ClientWebsite"] != null)
                {
                    client.ClientWebsite = (e.NewValues["ClientWebsite"] as string).Trim();
                }

                if (e.NewValues["PhysicalAddress1"] != null)
                {
                    client.PhysicalAddress1 = (e.NewValues["PhysicalAddress1"] as string).Trim();
                }

                if (e.NewValues["PhysicalAddress2"] != null)
                {
                    client.PhysicalAddress2 = (e.NewValues["PhysicalAddress2"] as string).Trim();
                }

                if (e.NewValues["PhysicalCountry"] != null)
                {
                    client.PhysicalCountry = (e.NewValues["PhysicalCountry"] as string).Trim();
                }

                if (e.NewValues["PhysicalPostalCode"] != null)
                {
                    client.PhysicalPostalCode = (e.NewValues["PhysicalPostalCode"] as string).Trim();
                }

                if (e.NewValues["PhysicalStateProvince"] != null)
                {
                    client.PhysicalStateProvince = (e.NewValues["PhysicalStateProvince"] as string).Trim();
                }


                if (e.NewValues["PostalAddress1"] != null)
                {
                    client.PostalAddress1 = (e.NewValues["PostalAddress1"] as string).Trim();
                }

                if (e.NewValues["PostalAddress2"] != null)
                {
                    client.PostalAddress2 = (e.NewValues["PostalAddress2"] as string).Trim();
                }

                if (e.NewValues["PostalCountry"] != null)
                {
                    client.PostalCountry = (e.NewValues["PostalCountry"] as string).Trim();
                }

                if (e.NewValues["PostalPostalCode"] != null)
                {
                    client.PostalPostalCode = (e.NewValues["PostalPostalCode"] as string).Trim();
                }

                if (e.NewValues["PostalStateProvince"] != null)
                {
                    client.PostalStateProvince = (e.NewValues["PostalStateProvince"] as string).Trim();
                }

                if (e.NewValues["ThemeID"] != null)
                {
                    client.ThemeID = e.NewValues["ThemeID"] as int?;
                }

                if (e.NewValues["ClientLogoImageURL"] != null)
                {
                    var logoUrl = (e.NewValues["ClientLogoImageURL"] as string).Trim();
                    var logoFriendlyName = (e.NewValues["ClientLogoImageFriendlyName"] as string).Trim();

                    if (!string.IsNullOrEmpty(logoUrl) && !string.IsNullOrWhiteSpace(logoUrl))
                    {
                        if (string.Compare(logoUrl, client.ClientLogoImageURL, true) != 0)
                        {
                            var oldFilename = client.ClientLogoImageURL;

                            client.ClientLogoImageURL = logoUrl;
                            client.ClientLogoImageFriendlyName = logoFriendlyName;
                            var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), logoUrl);
                            var destinationPath = Path.Combine(
                                                          MapPath(ConfigurationHelper.LiveUploadLocation),
                                                          logoUrl);

                            var oldFileDeletePath = string.IsNullOrEmpty(oldFilename)
                                                        ? string.Empty
                                                        : Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                                  oldFilename);

                            File.Move(tempPath, destinationPath);

                            if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                            {
                                File.Delete(oldFileDeletePath);
                            }
                        }

                    }
                    else if (!string.IsNullOrEmpty(client.ClientLogoImageURL) && !string.IsNullOrWhiteSpace(client.ClientLogoImageURL))
                    {
                        RemoveLogoFromClient(client);
                    }
                }
                else if (!string.IsNullOrEmpty(client.ClientLogoImageURL) && !string.IsNullOrWhiteSpace(client.ClientLogoImageURL))
                {
                    RemoveLogoFromClient(client);
                }

                client.UserID_LastModifiedBy = userId;
                client.LastModifiedOnDateTime = modifiedOn;

                var distributorGrid =
                    clientGridView.FindEditFormTemplateControl(@"DistributorAccessGridView") as ASPxGridView;

                int start = distributorGrid.PageIndex * distributorGrid.SettingsPager.PageSize;
                int end = (distributorGrid.PageIndex + 1) * distributorGrid.SettingsPager.PageSize;

                var isGrantedColumn = distributorGrid.Columns["IsGranted"] as GridViewDataColumn;

                for (var i = start; i < end; i++)
                {
                    var isGrantedCheckBox =
                        distributorGrid.FindRowCellTemplateControl(i, isGrantedColumn, "IsAcessGrantedCheckBox") as
                        ASPxCheckBox;

                    var id = Convert.ToInt32(distributorGrid.GetRowValues(i, distributorGrid.KeyFieldName));
                    var isOwner = distributorGrid.Selection.IsRowSelected(i);

                    if (isGrantedCheckBox != null)
                    {
                        if (isGrantedCheckBox.Checked)
                        {
                            var mapping = client.Client2Distributors.Where(c => c.DistributorID == id).FirstOrDefault();

                            if (mapping == null)
                            {

                                mapping = new Client2Distributor()
                                              {
                                                  ClientID = client.ClientID,
                                                  CreatedOnDateTime = modifiedOn,
                                                  DistributorID = id,
                                                  DistributorIsClientOwner = isOwner,
                                                  LastModifiedOnDateTime = modifiedOn,
                                                  UserID_CreatedBy = userId,
                                                  UserID_LastModifiedBy = userId
                                              };

                                client.Client2Distributors.Add(mapping);
                            }
                            else
                            {
                                mapping.DistributorIsClientOwner = isOwner;
                            }
                        }
                        else
                        {
                            var mapping = client.Client2Distributors.Where(c => c.DistributorID == id).FirstOrDefault();

                            if (mapping != null)
                            {
                                client.Client2Distributors.Remove(mapping);
                            }
                        }
                    }
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();

                        e.Cancel = true;
                        clientGridView.CancelEdit();
                        clientGridView.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        private void RemoveLogoFromClient(Client client)
        {
            var oldFilename = client.ClientLogoImageURL;
            var oldFileDeletePath = string.IsNullOrEmpty(oldFilename)
                                        ? string.Empty
                                        : Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                       oldFilename);

            client.ClientLogoImageURL = null;
            client.ClientLogoImageFriendlyName = null;

            if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
            {
                File.Delete(oldFileDeletePath);
            }
        }

        protected void ClientGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;

            e.Values["UserID_LastModifiedBy"] = userId;
            e.Values["LastModifiedOnDateTime"] = modifiedOn;
        }

        protected void DistributorAccessDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var clientId = EditingClientID;
            var dataContext = new EChallengeDataContext();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = from d in dataContext.Distributors
                           from cd in dataContext.Client2Distributors
                               .Where(c => d.DistributorID == c.DistributorID && c.ClientID == clientId)
                               .DefaultIfEmpty()
                           orderby d.DistributorName
                           select new
                           {
                               d.DistributorID,
                               d.DistributorName,
                               IsOwner = cd.DistributorIsClientOwner == null ? false : cd.DistributorIsClientOwner,
                               IsGranted = cd.DistributorID == null ? false : true
                           };
            }
            else
            {
                e.Result = from d in dataContext.Distributors
                           from u in d.User2Distributors
                           from cd in dataContext.Client2Distributors
                            .Where(c => d.DistributorID == c.DistributorID && c.ClientID == clientId)
                            .DefaultIfEmpty()
                           where u.UserID == userId
                           orderby d.DistributorName
                           select new
                                      {
                                          d.DistributorID,
                                          d.DistributorName,
                                          IsOwner = cd.DistributorIsClientOwner == null ? false : cd.DistributorIsClientOwner,
                                          IsGranted = cd.DistributorID == null ? false : true
                                      };
            }

        }

        protected void ProductAccessDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var clientId = EditingClientID ?? -1;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var language = dataContext.Languages.Where(l => l.LCID == lcid).FirstOrDefault();

            var currentLanguageProducts = from p in dataContext.Products
                                          from pl in p.Product_Languages
                                          where pl.LanguageID == language.LanguageID
                                          select pl;

            var defaultLanguageProducts = from p in dataContext.Products
                                          from pl in p.Product_Languages
                                          where p.DefaultLanguageID == pl.LanguageID
                                          select pl;

            IQueryable<int> distributors = null;

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                distributors = from d in dataContext.Distributors
                               from m in d.Client2Distributors
                               where m.ClientID == clientId
                               select d.DistributorID;
            }
            else
            {
                distributors = from d in dataContext.Distributors
                               from u in d.User2Distributors
                               from m in d.Client2Distributors
                               where u.UserID == userId && m.ClientID == clientId
                               select d.DistributorID;
            }


            if (distributors.Count() > 0)
            {
                var result = (from p in dataContext.Products
                              from pd in p.Product2Distributors
                              join defaultProduct in defaultLanguageProducts on p.ProductID equals
                                  defaultProduct.ProductID
                              from currentLanguageProduct in currentLanguageProducts
                                  .Where(c => c.ProductID == p.ProductID).DefaultIfEmpty()
                              let client2Distributor = (dataContext.Client2Distributors.Where(c => c.ClientID == clientId && c.DistributorID == pd.DistributorID).FirstOrDefault())
                              let mapping =
                                (
                                  (from dd in dataContext.DistributorClient2DistributorProducts
                                   where dd.Distributor2ProductID == pd.Product2DistributorID &&
                                       dd.Client2DistributorID == client2Distributor.Client2DistributorID
                                   select dd).FirstOrDefault()
                                )
                              where distributors.Contains(pd.DistributorID) && pd.DistributorIsProductOwner
                              select new
                                         {
                                             p.ProductID,
                                             OnlyShowClientCustomAvatars = mapping == null ? false : mapping.OnlyShowClientCustomAvatars,
                                             ProductName =
                                  currentLanguageProduct == null
                                      ? defaultProduct.ProductName
                                      : currentLanguageProduct.ProductName,
                                             ClientSpecificProductTheme = mapping == null ? null : mapping.ClientSpecificProductTheme,
                                             pd.Product2DistributorID,
                                             pd.DistributorID,
                                             pd.Distributor.DistributorName,
                                             IsSelected = mapping == null ? false : true,
                                             QuestionMix = mapping == null ? null : mapping.QuestionMix
                                         }).Distinct();


                e.Result = from r in result
                           orderby r.ProductName ascending
                           select r;
            }
            else
            {
                e.Cancel = true;
            }
        }

        DistributorClient2DistributorProduct IsAccessGranted(int product2DistributorId, int clientId)
        {
            var dataContext = new EChallengeDataContext();

            var product2Distributor =
                dataContext.Product2Distributors.Where(p => p.Product2DistributorID == product2DistributorId).
                    FirstOrDefault();

            var client2Distributor =
                dataContext.Client2Distributors.Where(
                    c => c.ClientID == clientId && c.DistributorID == product2Distributor.DistributorID).
                    FirstOrDefault();

            if (client2Distributor == null)
            {
                return null;
            }

            return
                dataContext.DistributorClient2DistributorProducts.Where(
                    c =>
                    c.Client2DistributorID == client2Distributor.Client2DistributorID &&
                    c.Distributor2ProductID == product2DistributorId).
                    FirstOrDefault();
        }

        protected void DistributorAccessGridView_DataBound(object sender, EventArgs e)
        {
            var distributorGrid = sender as ASPxGridView;
            distributorGrid.Selection.UnselectAll();

            for (var i = 0; i < distributorGrid.VisibleRowCount; i++)
            {
                if (distributorGrid.GetRowValues(i, "IsOwner") != null)
                {
                    if (Convert.ToBoolean(distributorGrid.GetRowValues(i, "IsOwner")))
                    {
                        distributorGrid.Selection.SelectRow(i);
                    }
                }
            }
        }

        protected void ProductAccessGridView_DataBound(object sender, EventArgs e)
        {
            var productAccessGridView = sender as ASPxGridView;
            productAccessGridView.Selection.UnselectAll();

            var commandColumn = productAccessGridView.Columns["CommandColumn"] as GridViewCommandColumn;
            var updateLinkButton = productAccessGridView.FindFooterCellTemplateControl(commandColumn, @"UpdateLinkButton") as LinkButton;
            var cancelLinkButton = productAccessGridView.FindFooterCellTemplateControl(commandColumn, @"CancelLinkButton") as LinkButton;

            if (updateLinkButton != null)
            {
                updateLinkButton.Visible = productAccessGridView.VisibleRowCount > 0 ? true : false;
            }

            if (cancelLinkButton != null)
            {
                cancelLinkButton.Visible = productAccessGridView.VisibleRowCount > 0 ? true : false;
            }
        }


        protected void ClientGridView_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            EditingClientID = (int)e.EditingKeyValue;
        }

        protected void ClientGridView_CancelRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            EditingClientID = null;
        }

        protected void ClientGridView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            EditingClientID = -1;
        }

        protected void ProductAccessGridView_BeforePerformDataSelect(object sender, EventArgs e)
        {
            EditingClientID = (int)(sender as ASPxGridView).GetMasterRowKeyValue();
        }

        protected void ProductAccessGridView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;
            var productAccessGridView = sender as ASPxGridView;
            var action = e.Parameters;

            if (string.Compare(action, "Update", true) == 0)
            {
                using (var dataContext = new EChallengeDataContext())
                {
                    var selectedKeys = productAccessGridView.GetSelectedFieldValues(@"Product2DistributorID");

                    var client = dataContext.Clients.Where(c => c.ClientID == EditingClientID).FirstOrDefault();

                    int start = productAccessGridView.PageIndex * productAccessGridView.SettingsPager.PageSize;
                    int end = (productAccessGridView.PageIndex + 1) * productAccessGridView.SettingsPager.PageSize;

                    var isGrantedAccessColumn =
                        productAccessGridView.Columns["GrantAccess"] as GridViewDataColumn;
                    var onlyShowClientCustomAvatarsColumn =
                        productAccessGridView.Columns["OnlyShowClientCustomAvatars"] as GridViewDataColumn;
                    var clientSpecificProductThemeColumn =
                        productAccessGridView.Columns["ClientSpecificProductTheme"] as GridViewDataColumn;
                    var questionMixColumn = productAccessGridView.Columns["QuestionMix"] as GridViewDataColumn;

                    for (var i = start; i < end; i++)
                    {
                        var isGrantedAccessCheckBox = productAccessGridView.FindRowCellTemplateControl(i,
                                                                                           isGrantedAccessColumn,
                                                                                           @"IsAcessGrantedCheckBox")
                                          as ASPxCheckBox;
                        var onlyShowClientCustomAvatarsCheckBox = productAccessGridView.FindRowCellTemplateControl(i,
                                                                                                                   onlyShowClientCustomAvatarsColumn,
                                                                                                                   @"OnlyShowClientCustomAvatarsCheckBox")
                                                                  as ASPxCheckBox;
                        var clientSpecificProductThemeTextBox =
                            productAccessGridView.FindRowCellTemplateControl(i, clientSpecificProductThemeColumn,
                                                                             @"ClientSpecificProductThemeTextBox") as
                            ASPxTextBox;
                        var questionMixComboBox =
                            productAccessGridView.FindRowCellTemplateControl(i, questionMixColumn,
                                                                             @"QuestionMixComboBox")
                            as ASPxComboBox;

                        if (onlyShowClientCustomAvatarsCheckBox != null &&
                            clientSpecificProductThemeTextBox != null &&
                            questionMixComboBox != null && isGrantedAccessCheckBox != null)
                        {

                            var id =
                                Convert.ToInt32(productAccessGridView.GetRowValues(i, productAccessGridView.KeyFieldName));


                            var distributorId = Convert.ToInt32(productAccessGridView.GetRowValues(i, "DistributorID"));

                            var client2DistributorMapping =
                                client.Client2Distributors.Where(c => c.DistributorID == distributorId).FirstOrDefault();


                            var mapping = client2DistributorMapping.DistributorClient2DistributorProducts.Where(
                                c => c.Distributor2ProductID == id &&
                                     c.Client2DistributorID == client2DistributorMapping.Client2DistributorID).
                                FirstOrDefault();

                            if (isGrantedAccessCheckBox.Checked)
                            {
                                if (mapping == null)
                                {
                                    mapping = new DistributorClient2DistributorProduct()
                                                  {
                                                      Client2DistributorID =
                                                          client2DistributorMapping.Client2DistributorID,
                                                      ClientSpecificProductTheme =
                                                          clientSpecificProductThemeTextBox.Text.Trim(),
                                                      CreatedOnDateTime = currentDateTime,
                                                      Distributor2ProductID = id,
                                                      LastModifiedOnDateTime = currentDateTime,
                                                      OnlyShowClientCustomAvatars =
                                                          onlyShowClientCustomAvatarsCheckBox.Checked,
                                                      QuestionMix =
                                                          questionMixComboBox.SelectedItem == null
                                                              ? null
                                                              : questionMixComboBox.SelectedItem.Value.ToString(),
                                                      UserID_CreatedBy = userId,
                                                      UserID_LastModifiedBy = userId
                                                  };

                                    client2DistributorMapping.DistributorClient2DistributorProducts.Add(mapping);
                                }
                                else
                                {
                                    mapping.ClientSpecificProductTheme = clientSpecificProductThemeTextBox.Text.Trim();
                                    mapping.LastModifiedOnDateTime = currentDateTime;
                                    mapping.OnlyShowClientCustomAvatars = onlyShowClientCustomAvatarsCheckBox.Checked;
                                    mapping.QuestionMix = questionMixComboBox.SelectedItem == null
                                                              ? null
                                                              : questionMixComboBox.SelectedItem.Value.ToString();
                                    mapping.UserID_LastModifiedBy = userId;
                                }
                            }
                            else
                            {
                                if (mapping != null)
                                {
                                    client2DistributorMapping.DistributorClient2DistributorProducts.Remove(mapping);
                                }
                            }
                        }
                    }

                    using (var transaction = dataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            dataContext.SaveChanges();
                            transaction.Commit();

                            productAccessGridView.DataBind();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }

                    }
                }
            }
            else if (string.Compare(action, "Cancel", true) == 0)
            {
                productAccessGridView.Selection.UnselectAll();
                productAccessGridView.DataBind();
            }
        }

        protected void ClientGridView_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            using (var dataContext = new EChallengeDataContext())
            {
                var clientName = (e.NewValues["ClientName"] as string).Trim().ToLower();

                var client =
                    dataContext.Clients.Where(d => d.ClientName.ToLower() == clientName).FirstOrDefault();

                if (e.IsNewRow)
                {
                    if (client != null)
                    {
                        e.RowError = Resources.Exceptions.ClientNameAlreadyExists;
                    }
                }
                else
                {
                    if (client != null)
                    {
                        var editingClientId = (int)e.Keys[0];
                        if (client.ClientID != editingClientId)
                        {
                            e.RowError = Resources.Exceptions.ClientNameAlreadyExists;
                        }
                    }
                }
            }
        }

        protected void IsAcessGrantedCheckBox_Init(object sender, EventArgs e)
        {
            var checkBox = sender as ASPxCheckBox;
            var container = checkBox.NamingContainer as GridViewDataItemTemplateContainer;
            checkBox.ClientInstanceName = string.Format(@"DistributorIsAccessGrantedCheckBox_{0}",
                                                        container.VisibleIndex);
            checkBox.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            var distributorId = (int)container.Grid.GetRowValues(container.VisibleIndex, "DistributorID");
            checkBox.JSProperties["cp_DistributorId"] = distributorId;
        }

        protected void IsAcessGrantedCheckBox_ProductAccess_Init(object sender, EventArgs e)
        {
            var checkBox = sender as ASPxCheckBox;
            var container = checkBox.NamingContainer as GridViewDataItemTemplateContainer;
            checkBox.ClientInstanceName = string.Format(@"ProductAccessIsAccessGrantedCheckBox_{0}",
                                                        container.VisibleIndex);
            checkBox.JSProperties["cp_ClientInstanceName"] = checkBox.ClientInstanceName;
        }

        protected void OnlyShowClientCustomAvatarsCheckBox_Init(object sender, EventArgs e)
        {
            var checkBox = sender as ASPxCheckBox;
            var container = checkBox.NamingContainer as GridViewDataItemTemplateContainer;
            checkBox.ClientInstanceName = string.Format(@"OnlyShowClientCustomAvatarsCheckBox_{0}",
                                                        container.VisibleIndex);
        }

        protected void ClientSpecificProductThemeTextBox_Init(object sender, EventArgs e)
        {
            var textBox = sender as ASPxTextBox;
            var container = textBox.NamingContainer as GridViewDataItemTemplateContainer;
            textBox.ClientInstanceName = string.Format(@"ClientSpecificProductThemeTextBox_{0}",
                                                        container.VisibleIndex);
        }

        protected void QuestionMixComboBox_Init(object sender, EventArgs e)
        {
            var comboBox = sender as ASPxComboBox;
            var container = comboBox.NamingContainer as GridViewDataItemTemplateContainer;
            comboBox.ClientInstanceName = string.Format(@"QuestionMixComboBox_{0}",
                                                        container.VisibleIndex);
        }

        protected void UploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            var splitOn = new[] { "." };
            var parts = e.UploadedFile.FileName.Split(splitOn, StringSplitOptions.RemoveEmptyEntries);
            var extension = parts.Length > 1 ? string.Format(".{0}", parts[parts.Length - 1]) : string.Empty;
            var filename = string.Format("{0}{1}", Guid.NewGuid(), extension);
            var fullPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
            e.UploadedFile.SaveAs(fullPath, false);
            var url = ResolveUrl(string.Format("{0}/{1}", ConfigurationHelper.TemporaryUploadLocation, filename));

            var editingIdForControl = EditingClientID;

            if (editingIdForControl == -1)
            {
                editingIdForControl = 0;
            }

            e.CallbackData = string.Format("{0};{1};{2};{3}", url, e.UploadedFile.FileName, editingIdForControl, filename);
        }

        protected void ClientLogoMediaHyperLink_OnInit(object sender, EventArgs e)
        {
            var link = sender as ASPxHyperLink;

            using (var container = link.NamingContainer as GridViewEditFormTemplateContainer)
            {
                if (container.KeyValue == null)
                {
                    link.ClientInstanceName = "ClientLogoMediaHyperLink_0";
                    link.ClientVisible = false;
                    return;
                }

                var clientId = (int)container.KeyValue;
                // DO NOT CHANGE THIS NAME AS IT IS USED IN THE JAVASCRIPT AND FRONT END
                link.ClientInstanceName = "ClientLogoMediaHyperLink_" + clientId;
                using (var dataContext = new EChallengeDataContext())
                {
                    var client = (from c in dataContext.Clients
                                  where c.ClientID == clientId
                                  select c).FirstOrDefault();

                    if (client == null)
                    {
                        link.ClientVisible = false;
                        return;
                    }
                    var clientLogoUrl = client.ClientLogoImageURL;

                    if (string.IsNullOrEmpty(clientLogoUrl) || string.IsNullOrWhiteSpace(clientLogoUrl))
                    {
                        link.ClientVisible = false;
                    }
                    else
                    {
                        link.ClientVisible = true;
                        link.NavigateUrl =
                              ResolveUrl(string.Format(@"{0}/{1}",
                                                       ConfigurationHelper.LiveUploadLocation,
                                                       clientLogoUrl));

                        link.Text = client.ClientLogoImageFriendlyName;
                    }
                }
            }
        }

        protected void ClientLogoBrowseButton_OnInit(object sender, EventArgs e)
        {
            var button = sender as ASPxButton;

            using (var container = button.NamingContainer as GridViewEditFormTemplateContainer)
            {
                if (container.KeyValue == null)
                {
                    button.ClientInstanceName = "ClientLogoBrowseButton_0";
                    button.ClientVisible = true;
                    return;
                }

                var clientId = (int)container.KeyValue;
                // DO NOT CHANGE THIS NAME AS IT IS USED IN THE JAVASCRIPT AND FRONT END
                button.ClientInstanceName = "ClientLogoBrowseButton_" + clientId;
                using (var dataContext = new EChallengeDataContext())
                {
                    var client = (from c in dataContext.Clients
                                  where c.ClientID == clientId
                                  select c).FirstOrDefault();

                    if (client == null)
                    {
                        button.ClientVisible = true;
                        return;
                    }
                    var clientLogoUrl = client.ClientLogoImageURL;

                    if (string.IsNullOrEmpty(clientLogoUrl) || string.IsNullOrWhiteSpace(clientLogoUrl))
                    {
                        button.ClientVisible = true;
                    }
                    else
                    {
                        button.ClientVisible = false;
                    }
                }
            }
        }

        protected void ClientLogoMediaHyperRemoveHyperlink_OnInit(object sender, EventArgs e)
        {
            var link = sender as ASPxHyperLink;

            using (var container = link.NamingContainer as GridViewEditFormTemplateContainer)
            {
                if (container.KeyValue == null)
                {
                    link.ClientInstanceName = "ClientLogoMediaHyperRemoveHyperlink_0";
                    link.ClientSideEvents.Click = "function (s,e) { ClientLogoMediaHyperRemoveHyperlink_Click(s,e,0);}";
                    link.ClientVisible = false;
                    return;
                }

                var clientId = (int)container.KeyValue;
                // DO NOT CHANGE THIS NAME AS IT IS USED IN THE JAVASCRIPT AND FRONT END
                link.ClientInstanceName = "ClientLogoMediaHyperRemoveHyperlink_" + clientId;
                link.ClientSideEvents.Click = "function (s,e) { ClientLogoMediaHyperRemoveHyperlink_Click(s,e," +
                                              clientId +
                                              ");}";
                using (var dataContext = new EChallengeDataContext())
                {
                    var client = (from c in dataContext.Clients
                                  where c.ClientID == clientId
                                  select c).FirstOrDefault();

                    if (client == null)
                    {
                        link.ClientVisible = false;
                        return;
                    }

                    var clientLogoUrl = client.ClientLogoImageURL;

                    if (string.IsNullOrEmpty(clientLogoUrl) || string.IsNullOrWhiteSpace(clientLogoUrl))
                    {
                        link.ClientVisible = false;
                    }
                    else
                    {
                        link.ClientVisible = true;
                    }
                }
            }
        }

        protected void ClientLogoImageUrl_OnInit(object sender, EventArgs e)
        {
            var textbox = sender as ASPxTextBox;
            var container = textbox.NamingContainer as GridViewEditFormTemplateContainer;

            var editingIdForControl = container.KeyValue == null ? 0 : (int)container.KeyValue;


            // DO NOT CHANGE THIS NAME AS IT IS USED IN THE JAVASCRIPT AND FRONT END
            textbox.ClientInstanceName = "ClientLogoImageUrl_" + editingIdForControl;
            textbox.ClientVisible = false;
        }

        protected void ClientLogoImageFriendlyName_OnInit(object sender, EventArgs e)
        {
            var textbox = sender as ASPxTextBox;
            var container = textbox.NamingContainer as GridViewEditFormTemplateContainer;

            var editingIdForControl = container.KeyValue == null ? 0 : (int)container.KeyValue;

            // DO NOT CHANGE THIS NAME AS IT IS USED IN THE JAVASCRIPT AND FRONT END
            textbox.ClientInstanceName = "ClientLogoImageFriendlyName_" + editingIdForControl;
            textbox.ClientVisible = false;
        }

        protected void DeleteClientCallback_OnCallback(object source, CallbackEventArgs e)
        {
            var clientIdString = e.Parameter;
            var clientId = Convert.ToInt32(clientIdString);

            using (var dataContext = new EChallengeDataContext())
            {
                var clientExists =
                    dataContext.Clients
                        .Any(c => c.ClientID == clientId);
                if (clientExists)
                {
                    var hasGameCreatorUsers = (from g in dataContext.Games
                                               join u in dataContext.Users on g.UserID_CreatedBy equals u.UserId
                                               join u2c in dataContext.User2Clients on u.UserId equals u2c.UserID
                                               where u2c.ClientID == clientId
                                               select u2c).Any();

                    if (hasGameCreatorUsers)
                    {
                        e.Result = "The client has users who have created template games and cannot be deleted";
                        return;
                    }

                    var hasCardCreatorUsers = (from c in dataContext.Cards
                                               join u in dataContext.Users on c.UserID_CreatedBy equals u.UserId
                                               join u2c in dataContext.User2Clients on u.UserId equals u2c.UserID
                                               where u2c.ClientID == clientId
                                               select u2c).Any();

                    if (hasCardCreatorUsers)
                    {
                        e.Result = "The client has users who have created cards and cannot be deleted";
                        return;
                    }

                    var hasProductCreatorUsers = (from p in dataContext.Products
                                                  join u in dataContext.Users on p.UserID_CreatedBy equals u.UserId
                                                  join u2c in dataContext.User2Clients on u.UserId equals u2c.UserID
                                                  where u2c.ClientID == clientId
                                                  select u2c).Any();

                    if (hasProductCreatorUsers)
                    {
                        e.Result = "The client has users who have created products and cannot be deleted";
                        return;
                    }

                    var userId = (Page as BasePage).CurrentContext.UserIdentifier;
                    using (var transaction = dataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            dataContext.upDeleteClient(clientId, userId);
                            dataContext.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }

            }
        }

        protected void CheckSessionCallback_OnCallback(object source, CallbackEventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void ThemeDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            e.Result = dataContext.Themes
                .Where(c => !c.IsADeletedRow)
                .Select(c => new {c.ThemeID, c.ThemeName})
                .OrderBy(c => c.ThemeName);
        }
    }
}