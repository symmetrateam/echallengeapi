﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils.Win.Hook;
using DevExpress.Web;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class ClientUserGroupManagementControl : System.Web.UI.UserControl
    {
        private const string EDITING_CLIENT_ID_KEY = "EditingClientID";
        private const string EDITING_CLIENT_USERGROUP_KEY = "EditingClientUserGroupID";

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                EditingClientID = null;
                EditingClientUserGroupID = null;
            }

        }

        protected void ClientDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var dataContext = new EChallengeDataContext();

            if(Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = (from c in dataContext.Clients
                            select c).Distinct();
            }
            else
            {
                e.Result = (from c in dataContext.Clients
                            from d in c.Client2Distributors
                            join u in dataContext.User2Distributors on d.DistributorID equals u.DistributorID
                            where u.UserID == userId
                            select c).Distinct();
            }
        }

        protected void UserDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var clientId = EditingClientID;
            if (clientId > 0)
            {               
                var dataContext = new EChallengeDataContext();


                e.Result = from c in dataContext.User2Clients
                           join u in dataContext.Users on c.UserID equals u.UserId
                           where c.ClientID == clientId
                           select u;
            }
            else
            {
                e.Cancel = true;
            }

        }

        int? EditingClientID
        {
            get
            {
                if (Session[EDITING_CLIENT_ID_KEY] == null)
                {
                    return -1;
                }
                else
                {
                    return (int)Session[EDITING_CLIENT_ID_KEY];
                }
            }
            set { Session[EDITING_CLIENT_ID_KEY] = value; }

        }

        int? EditingClientUserGroupID
        {
            get
            {
                if (Session[EDITING_CLIENT_USERGROUP_KEY] == null)
                {
                    return null;
                }
                else
                {
                    return (int)Session[EDITING_CLIENT_USERGROUP_KEY];
                }
            }
            set { Session[EDITING_CLIENT_USERGROUP_KEY] = value; }
        }

        protected void ClientUserGroupDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var clientId = EditingClientID;
            if (clientId > 0)
            {                
                var dataContext = new EChallengeDataContext();

                var usersInClientGroups = (from c2u in dataContext.User2ClientUserGroups
                    join u in dataContext.Users on c2u.UserID equals u.UserId
                    where !c2u.IsADeletedRow && !c2u.IsAnAuditRow && !u.IsADeletedRow && !u.IsAnAuditRow
                    select new {c2u.ClientUserGroupID, u.UserId}).Distinct()
                    .GroupBy(c => c.ClientUserGroupID)
                    .Select(g => new {GroupId = g.Key, UserCount = g.Count(a => a.UserId != Guid.Empty)});

                e.Result = from c in dataContext.ClientUserGroups
                    join cu in usersInClientGroups on c.ClientUserGroupID equals cu.GroupId into cGroup
                    from userCount in cGroup.DefaultIfEmpty()
                    where c.ClientID == clientId
                    select
                        new
                        {
                            c.ClientUserGroupID,
                            c.ClientUserGroupName,
                            c.ClientUserGroupDescription,
                            c.LastModifiedOnDateTime,
                            TotalUsers = userCount == null ? 0 : userCount.UserCount
                        };

            }
            else
            {
                e.Cancel = true;
            }
        }

        protected void ClientUserGroupGridView_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
           EditingClientUserGroupID = (int?)e.EditingKeyValue;
        }

        protected void UserGridView_Load(object sender, EventArgs e)
        {
            var clientUserGroupId = EditingClientUserGroupID;
            var userGridView = sender as ASPxGridView;
            var dataContext = new EChallengeDataContext();

            var selectedUsers = from u in dataContext.User2ClientUserGroups
                                where u.ClientUserGroupID == clientUserGroupId
                                select u.UserID;

            foreach (var selectedUser in selectedUsers)
            {
                userGridView.Selection.SelectRowByKey(selectedUser);
            }
        }

        protected void ClientUserGroupGridView_CancelRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            EditingClientUserGroupID = null;
        }

        protected void ClientUserGroupGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var currentDateTime = DateTime.Now;
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var clientId = EditingClientID.Value;

            var clientUserGroup = new ClientUserGroup();

            clientUserGroup.LastModifiedOnDateTime = currentDateTime;
            clientUserGroup.UserID_LastModifiedBy = userId;
            clientUserGroup.UserID_CreatedBy = userId;
            clientUserGroup.CreatedOnDateTime = currentDateTime;
            clientUserGroup.ClientID = clientId;
            clientUserGroup.ClientUserGroupName = e.NewValues["ClientUserGroupName"] == null ? null : (e.NewValues["ClientUserGroupName"] as string).Trim();
            clientUserGroup.ClientUserGroupDescription = e.NewValues["ClientUserGroupDescription"] == null ? null : (e.NewValues["ClientUserGroupDescription"] as string).Trim();

            var userGridView = gridView.FindEditFormTemplateControl(@"UserGridView") as ASPxGridView;
            var selectedUsers = userGridView.GetSelectedFieldValues("UserId").Cast<Guid>();

            foreach (var selectedUser in selectedUsers)
            {
                clientUserGroup.User2ClientUserGroups.Add(new User2ClientUserGroup
                {
                    UserID = selectedUser,
                    ClientUserGroupID = clientUserGroup.ClientUserGroupID,
                    UserID_CreatedBy = userId,
                    CreatedOnDateTime = currentDateTime,
                    UserID_LastModifiedBy = userId,
                    LastModifiedOnDateTime = currentDateTime
                });
            }

            using(var dataContext = new EChallengeDataContext())
            {

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.ClientUserGroups.Add(clientUserGroup);

                        dataContext.SaveChanges();
                        transaction.Commit();

                        e.Cancel = true;
                        gridView.CancelEdit();

                        gridView.DataBind();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }

        protected void ClientUserGroupGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var currentDateTime = DateTime.Now;
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var clientId = EditingClientID;
            var clientUserGroupID = (int) e.Keys[0];
            var lastModifiedOn = (DateTime) e.OldValues["LastModifiedOnDateTime"];
            using (var dataContext = new EChallengeDataContext())
            {
                var clientUserGroup =
                    dataContext.ClientUserGroups.Where(c => c.ClientUserGroupID == clientUserGroupID).FirstOrDefault();

                if(lastModifiedOn != clientUserGroup.LastModifiedOnDateTime)
                {
                    throw new Exception(Resources.Exceptions.ConcurrencyException);
                }

                clientUserGroup.LastModifiedOnDateTime = currentDateTime;
                clientUserGroup.UserID_LastModifiedBy = userId;             
                clientUserGroup.ClientID = clientId.Value;

                if (e.NewValues["ClientUserGroupName"] != null)
                {
                    clientUserGroup.ClientUserGroupName = (e.NewValues["ClientUserGroupName"] as string).Trim();
                }

                if (e.NewValues["ClientUserGroupDescription"] != null)
                {
                    clientUserGroup.ClientUserGroupDescription = (e.NewValues["ClientUserGroupDescription"] as string).Trim();
                }

                var userGridView = gridView.FindEditFormTemplateControl(@"UserGridView") as ASPxGridView;
                var selectedUsers = userGridView.GetSelectedFieldValues("UserId").Cast<Guid>();

                var currentSelectedUsers = clientUserGroup.User2ClientUserGroups.ToList();
                foreach (var currentUser in currentSelectedUsers.Where(u => selectedUsers.All(s => s != u.UserID)))
                {
                    clientUserGroup.User2ClientUserGroups.Remove(currentUser);
                    dataContext.User2ClientUserGroups.Remove(currentUser);
                }

                foreach (var selectedUser in selectedUsers)
                {
                    clientUserGroup.User2ClientUserGroups.Add(new User2ClientUserGroup
                                                                  {
                                                                      UserID = selectedUser,
                                                                      ClientUserGroupID =
                                                                          clientUserGroup.ClientUserGroupID,
                                                                      UserID_CreatedBy = userId,
                                                                      CreatedOnDateTime = currentDateTime,
                                                                      UserID_LastModifiedBy = userId,
                                                                      LastModifiedOnDateTime = currentDateTime
                                                                  });
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();

                        e.Cancel = true;
                        gridView.CancelEdit();

                        gridView.DataBind();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
        
        protected void ClientUserGroupGridView_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            using (var dataContext = new EChallengeDataContext())
            {
                var clientUserGroupName = (e.NewValues["ClientUserGroupName"] as string).Trim().ToLower();

                var clientUserGroup =
                    dataContext.ClientUserGroups
                        .FirstOrDefault(
                            d => d.ClientUserGroupName.ToLower() == clientUserGroupName &&
                                 d.ClientID == EditingClientID);

                if (e.IsNewRow)
                {
                    if (clientUserGroup != null)
                    {
                        e.RowError = Resources.Exceptions.ClientUserGroupNameAlreadyExists;
                    }
                }
                else
                {
                    if (clientUserGroup != null)
                    {
                        var clientUserGroupId = (int)e.Keys[0];
                        if (clientUserGroup.ClientUserGroupID != clientUserGroupId)
                        {
                            e.RowError = Resources.Exceptions.ClientUserGroupNameAlreadyExists;
                        }
                    }
                }
            }
        }

        protected void ClientUserGroupGridView_BeforePerformDataSelect(object sender, EventArgs e)
        {
            EditingClientID = (int)(sender as ASPxGridView).GetMasterRowKeyValue();
        }
    }
}