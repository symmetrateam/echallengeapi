﻿<%@ control language="C#" autoeventwireup="true" codebehind="FlashAssetManagementControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.FlashAssetManagementControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ register assembly="Microsoft.AspNet.EntityDataSource" namespace="Microsoft.AspNet.EntityDataSource" tagPrefix="ef" %>

<dx:aspxgridview id="FlashAssetGridView" runat="server" clientinstancename="FlashAssetGridView"
    autogeneratecolumns="False" datasourceid="FlashAssetKeyDataSource" keyfieldname="FlashAssetID"
    width="100%">
    <settingsediting mode="Inline" />
    <settingspager pagesize="50" />
    <columns>
        <dx:gridviewcommandcolumn visibleindex="0" width="100px" ShowEditButton="True" ShowDeleteButton="True">
            <headercaptiontemplate>
                <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="AddNewRow(FlashAssetGridView);return false;" text="New"/>
            </headercaptiontemplate>
        </dx:gridviewcommandcolumn>
        <dx:gridviewdatatextcolumn fieldname="FlashAssetID" readonly="True" visible="False"
            visibleindex="1">
            <editformsettings visible="False" />
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="Key" visibleindex="2">
            <propertiestextedit>
                <validationsettings errordisplaymode="ImageWithTooltip">
                    <requiredfield isrequired="true" errortext="Key is a required field" />
                </validationsettings>
            </propertiestextedit>
        </dx:gridviewdatatextcolumn>
    </columns>
    <settingsdetail allowonlyonemasterrowexpanded="True" showdetailrow="True" />
    <templates>
        <detailrow>
            <dx:aspxgridview id="FlashAssetLanguageGridView" OnInit="FlashAssetLanguageGridView_OnInit" runat="server" clientinstancename="FlashAssetLanguageGridView"
                width="100%" autogeneratecolumns="False" datasourceid="FlashAssetValueDataSource"
                keyfieldname="FlashAsset_LanguageID" onbeforeperformdataselect="FlashAssetLanguageGridView_BeforePerformDataSelect">
                <settingsediting mode="Inline" />
                <settingspager pagesize="50" />
                <columns>
                    <dx:gridviewcommandcolumn visibleindex="0" width="100px" ShowEditButton="True" ShowDeleteButton="True">
                        <headercaptiontemplate>
                            <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="AddNewRow(FlashAssetLanguageGridView);return false;" text="New"/>
                        </headercaptiontemplate>
                    </dx:gridviewcommandcolumn>
                    <dx:gridviewdatatextcolumn fieldname="FlashAsset_LanguageID" readonly="True" visible="False"
                        visibleindex="1">
                        <editformsettings visible="False" />
                    </dx:gridviewdatatextcolumn>
                    <dx:gridviewdatatextcolumn fieldname="FlashAssetID" visible="False" visibleindex="2">
                    </dx:gridviewdatatextcolumn>
                    <dx:gridviewdatacomboboxcolumn caption="Language" fieldname="LanguageID" visibleindex="3">
                        <propertiescombobox datasourceid="LanguageDataSource" textfield="Language1" valuefield="LanguageID"
                            valuetype="System.Int32">
                            <validationsettings errordisplaymode="ImageWithTooltip">
                                <requiredfield isrequired="true" errortext="Language is a required field" />
                            </validationsettings>
                        </propertiescombobox>					
                    </dx:gridviewdatacomboboxcolumn>
                    <dx:gridviewdatatextcolumn fieldname="Value" visibleindex="4">
                        <propertiestextedit>
                            <validationsettings errordisplaymode="ImageWithTooltip">
                                <requiredfield isrequired="true" errortext="Value is a required field" />
                            </validationsettings>
                        </propertiestextedit>
                    </dx:gridviewdatatextcolumn>
                </columns>
            </dx:aspxgridview>
        </detailrow>
    </templates>
</dx:aspxgridview>
<ef:EntityDataSource id="FlashAssetKeyDataSource" EnableDelete="True" EnableInsert="True" EnableUpdate="True" 
    EntitySetName="FlashAssets" EnableViewState="False" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext" />   
<ef:EntityDataSource id="FlashAssetValueDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntitySetName="FlashAsset_Languages"  where="it.FlashAssetID == @parFlashAssetID"
    enabledelete="True" enableinsert="True" enableupdate="True" oninserting="FlashAssetValueDataSource_OnInserting"
    enableviewstate="false">
    <whereparameters>
        <asp:sessionparameter name="parFlashAssetID" sessionfield="FlashAssetID" DbType="Int32" />
    </whereparameters>
</ef:EntityDataSource>
<asp:linqdatasource id="LanguageDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" select="new (LanguageID, Language1)" tablename="Languages">
</asp:linqdatasource>
