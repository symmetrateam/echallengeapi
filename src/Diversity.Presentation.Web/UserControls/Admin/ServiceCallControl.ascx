﻿<%@ control language="C#" autoeventwireup="true" codebehind="ServiceCallControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.ServiceCallControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>




<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx1" %>
<script id="dxis_UserManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/ServiceCallControl.js") %>'>
</script>
<dx:aspxloadingpanel id="LoadingPanel" runat="server" modal="True" clientinstancename="LoadingPanel">
</dx:aspxloadingpanel>
<dx:aspxcallback id="ServiceCallCallback" runat="server" clientinstancename="ServiceCallCallback"
    oncallback="ServiceCallCallback_Callback">
    <clientsideevents callbackcomplete="ServiceCallCallback_CallbackComplete"></clientsideevents>
</dx:aspxcallback>
<dx:aspxpagecontrol id="AuthenticatePageControl" runat="server">
    <tabpages>
        <dx:tabpage runat="server" text="Authenticate">
            <contentcollection>
                <dx:contentcontrol>
                    <table>
                        <tr>
                            <td>
                                <dx:aspxlabel runat="server" id="UsernameLabel" text="Username" />
                            </td>
                            <td>
                                <dx:aspxtextbox runat="server" id="UsernameTextBox">
                                </dx:aspxtextbox>
                            </td>
                            <td>
                                <dx:aspxlabel runat="server" id="PasswordLabel" text="Password" />
                            </td>
                            <td>
                                <dx:aspxtextbox runat="server" id="PasswordTextBox">
                                </dx:aspxtextbox>
                            </td>
                            <td>
                                <dx:aspxbutton runat="server" id="AuthenticateButton" autopostback="False" text="Authenticate">
                                    <clientsideevents click="AuthenticateButton_Click"></clientsideevents>
                                </dx:aspxbutton>
                            </td>
                        </tr>
                    </table>
                </dx:contentcontrol>
            </contentcollection>
        </dx:tabpage>
    </tabpages>
</dx:aspxpagecontrol>
<dx:aspxpagecontrol id="DashBoardPageControl" runat="server">
    <tabpages>
        <dx:tabpage runat="server" text="Get Dashboard">
            <contentcollection>
                <dx:contentcontrol>
                    <table>
                        <tr>
                            <td>
                                <dx:aspxbutton runat="server" id="GetDashboardButton" autopostback="False" text="GetDashboard">
                                    <clientsideevents click="GetDashboardButton_Click"></clientsideevents>
                                </dx:aspxbutton>
                            </td>
                        </tr>
                    </table>
                </dx:contentcontrol>
            </contentcollection>
        </dx:tabpage>
    </tabpages>
</dx:aspxpagecontrol>
<dx:aspxpagecontrol id="PlayerResultJsonPageControl" runat="server">
    <tabpages>
        <dx:tabpage runat="server" text="Json Examples for saving player results">
            <contentcollection>
                <dx:contentcontrol>
                    <table>
                        <tr>
                            <td>
                                <dx:aspxbutton runat="server" id="MultipleChoicePlayerResultButton" autopostback="False"
                                    text="Show Multiple choice JSON">
                                    <clientsideevents click="MultipleChoicePlayerResultButton_Click"></clientsideevents>
                                </dx:aspxbutton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:aspxbutton runat="server" id="MultipleCheckBoxPlayerResultButton" autopostback="False"
                                    text="Show Multiple checkbox JSON">
                                    <clientsideevents click="MultipleCheckBoxPlayerResultButton_Click"></clientsideevents>
                                </dx:aspxbutton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:aspxbutton runat="server" id="TextPlayerResultButton" autopostback="False" text="Show textbox/textarea JSON">
                                    <clientsideevents click="TextPlayerResultButton_Click"></clientsideevents>
                                </dx:aspxbutton>
                            </td>
                        </tr>
                                                <tr>
                            <td>
                                <dx:aspxbutton runat="server" id="VotingPlayerResultButton" autopostback="False" text="Show voting JSON">
                                    <clientsideevents click="VotingPlayerResultButton_Click"></clientsideevents>
                                </dx:aspxbutton>
                            </td>
                        </tr>

                    </table>
                </dx:contentcontrol>
            </contentcollection>
        </dx:tabpage>
    </tabpages>
</dx:aspxpagecontrol>
<dx:aspxmemo id="ResultMemo" clientinstancename="ResultMemo" runat="server" height="400px"
    width="917px">
</dx:aspxmemo>
