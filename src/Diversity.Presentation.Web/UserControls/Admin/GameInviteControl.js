﻿// for multi select combobox
var textSeparator = ";";
function OnListBoxSelectionChanged(listBox, args) {
    //if(args.index == 0)
    //    args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
    //UpdateSelectAllItemState();
    UpdateText();
}
//function UpdateSelectAllItemState() {
//    IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
//}
//function IsAllSelected() {
//    var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
//    return checkListBox.GetSelectedItems().length == selectedDataItemCount;
//}
function UpdateText() {
    var selectedItems = checkListBox.GetSelectedItems();
    GameComboBox.SetText(GetSelectedItemsText(selectedItems));
}
function SynchronizeListBoxValues(dropDown, args) {
    checkListBox.UnselectAll();
    var texts = dropDown.GetText().split(textSeparator);
    var values = GetValuesByTexts(texts);
    checkListBox.SelectValues(values);
    //UpdateSelectAllItemState();
    UpdateText(); // for remove non-existing texts
}
function GetSelectedItemsText(items) {
    var texts = [];
    for (var i = 0; i < items.length; i++) {
        texts.push(items[i].text);
    }
    return texts.join(textSeparator);
}

function GetSelectedItemsValues(items) {
    var values = [];
    for (var i = 0; i < items.length; i++) {
        values.push(items[i].value);
    }
    return values.join(textSeparator);
}

function GetValuesByTexts(texts) {
    var actualValues = [];
    var item;
    for(var i = 0; i < texts.length; i++) {
        item = checkListBox.FindItemByText(texts[i]);
        if(item != null)
            actualValues.push(item.value);
    }
    return actualValues;
}

//end multi select combobox



function ClientComboBox_SelectedIndexChanged(s, e) {
    if (e.index < 0) {
        ConfirmClientButton.SetEnabled(false);
    }
    else {
        ConfirmClientButton.SetEnabled(true);
    }
}

function UserGridView_SelectionChanged(s, e) {
    var count = s.GetSelectedRowCount();
    if (count > 0) {
        SendEmailsButton.SetEnabled(true);
    }
    else {
        SendEmailsButton.SetEnabled(false);
    }
}

function SendEmails_Clicked(s, e) {
    if (!ASPxClientEdit.ValidateEditorsInContainer(null)) {
        return;
    }

    if (EmailCheckBox.GetChecked()) {
        SendInvites();
    } else {
        EmailsPopupControl.Show();
    }
}

function ConfirmClientButton_Click(s, e) {
    var param = ClientComboBox.GetValue().toString();
    ClientSelectionCallback.PerformCallback(param);
    ConfirmClientButton.SetEnabled(false);
}

function ClientSelectionCallback_CallbackComplete(s, e) {
    if (e.result == null) {
        ClientSelectionPopupControl.Hide();
        ClientUserGroupComboBox.PerformCallback();
    }
}

var lastClientUserGroup = null;
function ClientUserGroupComboBox_SelectedIndexChanged(s, e) {

    if (ThemeComboBox.InCallback()) {
        lastClientUserGroup = s.GetValue.toString();
    } else {
        ThemeComboBox.PerformCallback(s.GetValue());
    }

    if (s.GetSelectedIndex() == -1) {
        return;
    }

    UserGridView.PerformCallback(s.GetValue());

}

function OnThemeComboBoxEndCallback(s, e) {
    if (lastClientUserGroup) {
        ThemeComboBox.PerformCallback(lastClientUserGroup);
        lastClientUserGroup = null;
    }
}

function ClientUserGroupComboBox_LostFocus(s, e) {
    var item = s.FindItemByText(s.GetText());

    if (item == null) {
        s.SetText("");
    }
}

function SendInviteButton_Click(s, e) {
    SendInvites();
}

function SendInvites() {
    var rowSelected = UserGridView.GetSelectedRowCount() > 0;

    if (ASPxClientEdit.ValidateGroup('SaveGroup') && rowSelected) {
        var selectedItems = checkListBox.GetSelectedItems();
        var gameIds = GetSelectedItemsValues(selectedItems);
        SaveCallback.PerformCallback(gameIds);
        LoadingPanel.Show();
    }
    else {
        if (!ASPxClientEdit.ValidateEditorsInContainer(null)) {
            return;
        }
        else {
            ValidationLabel.SetVisible(true);
            SendInviteCompleted.SetVisible(false);
            SetFailMessageVisibility(false);
            InfoMessagePopupControl.Show();
        }
    }
}

function OkButton_Click(s, e) {
    InfoMessagePopupControl.Hide();
}

function SaveCallback_CallbackComplete(s, e) {
    EmailsPopupControl.Hide();
    LoadingPanel.Hide();
    SendInviteCompleted.SetVisible(true);

    if (e.result == null || e.result == "") {
        ValidationLabel.SetVisible(false);
        SetFailMessageVisibility(false);
    }
    else {
        ValidationLabel.SetVisible(false);

        if (e.result.length > 0) {

            SetFailMessageVisibility(true);
            FailRecipientMemo.SetText(e.result);
        }
        else {
            SetFailMessageVisibility(false);
        }
    }

    InfoMessagePopupControl.Show();
}

function SetFailMessageVisibility(visible) {
    FailRecipientMemo.SetVisible(visible);
    FailLabel.SetVisible(visible);
}