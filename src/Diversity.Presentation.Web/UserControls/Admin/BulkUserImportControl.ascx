﻿<%@ control language="C#" autoeventwireup="true" codebehind="BulkUserImportControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.BulkUserImportControl" %>
<%@ import namespace="ComLib" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>






<script id="dxis_BulkUserImportControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/BulkUserImportControl.js") %>'>

</script>
<table>
    <tr>
        <td colspan="5">
            <dx:ASPxHyperLink runat="server" ID="TemplateDownloadHyperLink" Text="Download Template" Target="_blank"/>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxlabel id="ClientLabel" runat="server" text="Client">
            </dx:aspxlabel>
        </td>       
        <td>
            <dx:aspxcombobox id="ClientComboBox" runat="server" clientinstancename="ClientComboBox"
                datasourceid="ClientDataSource" valuetype="System.Int32" valuefield="ClientID"
                textfield="ClientName" validationsettings-validationgroup="SaveGroup" dropdownstyle="DropDownList">
                <validationsettings errordisplaymode="ImageWithTooltip">
                    <requiredfield isrequired="true" errortext="Client is required" />
                </validationsettings>
            </dx:aspxcombobox>
        </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
            <dx:aspxlabel id="UploadLabel" runat="server" text="Select File">
            </dx:aspxlabel>
        </td>
        <td>
            <dx:aspxuploadcontrol id="UserUploadControl" runat="server" clientinstancename="UserUploadControl"
                showprogresspanel="true" size="35" onfileuploadcomplete="UserUploadControl_FileUploadComplete">
                <validationsettings maxfilesize="4194304" allowedfileextensions=".csv">
                </validationsettings>
                <clientsideevents fileuploadstart="UserUploadControl_FileUploadStart" fileuploadcomplete="UserUploadControl_FileUploadComplete"
                    textchanged="UserUploadControl_TextChanged" />
            </dx:aspxuploadcontrol>
        </td>
        <td class="spacer">
        </td>
        <td>
            <dx:aspxbutton id="UploadButton" runat="server" clientinstancename="UploadButton"
                text="Upload" autopostback="false" causesvalidation="false" clientenabled="false">
                <clientsideevents click="UploadButton_Click" />
            </dx:aspxbutton>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td style="text-align: left;">
            <dx:aspxlabel id="AllowMimeTypesLabel" runat="server" text="Allowed file types: .csv">
            </dx:aspxlabel>
            <br />
            <dx:aspxlabel id="MaxFileSizeLabel" runat="server" text="Maximum file size is 4Mb">
            </dx:aspxlabel>
        </td>
        <td class="spacer">
        </td>
        <td>
        </td>
    </tr>
</table>
<br />
<table width="100%">
    <tr>
        <td align="right">
            <dx:aspxbutton id="SaveButton" runat="server" clientinstancename="SaveButton" autopostback="false"
                clientenabled="false" text="Import">
                <clientsideevents click="SaveButton_Click" />
            </dx:aspxbutton>
        </td>
    </tr>
</table>
<br />
<dx:aspxgridview id="UserExceptionGridView" runat="server" clientinstancename="UserExceptionGridView"
    autogeneratecolumns="False" datasourceid="UserExceptionDataSource" keyfieldname="Email"
    width="900px" oncustomcallback="UserExceptionGridView_CustomCallback" onrowdeleting="UserExceptionGridView_RowDeleting"
    onrowupdating="UserExceptionGridView_RowUpdating" onrowvalidating="UserExceptionGridView_RowValidating"
    onstartrowediting="UserExceptionGridView_StartRowEditing">
    <clientsideevents endcallback="UserExceptionGridView_EndCallback" />
    <settings HorizontalScrollBarMode="Auto" showtitlepanel="true" />
    <settingsediting EditFormColumnCount="2" mode="EditForm" />
    <settingstext title="Invalid Users" />
    <settingspager pagesize="15">
    </settingspager>
    <columns>
        <dx:gridviewcommandcolumn visibleindex="0" ShowEditButton="true" ShowDeleteButton="true"/>
        <dx:gridviewdatatextcolumn fieldname="Firstname" caption="Firstname" visibleindex="1">
            <propertiestextedit Width="250px">
                <validationsettings errordisplaymode="ImageWithTooltip">
                    <requiredfield isrequired="true" errortext="Firstname is a required field" />
                </validationsettings>
            </propertiestextedit>
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="Lastname" caption="Lastname" visibleindex="2">
            <propertiestextedit Width="250px">
                <validationsettings errordisplaymode="ImageWithTooltip">
                    <requiredfield isrequired="true" errortext="Lastname is a required field" />
                </validationsettings>
            </propertiestextedit>
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="Email" caption="Email" visibleindex="3">
            <propertiestextedit Width="250px">
                <validationsettings errordisplaymode="ImageWithTooltip">
                    <requiredfield isrequired="true" errortext="Email is a required field" />
                    <regularexpression validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        errortext="Invalid email address" />
                </validationsettings>
            </propertiestextedit>
        </dx:gridviewdatatextcolumn>
        <dx:GridViewDataDateColumn FieldName="DateOfBirth" Caption="Birth Date" VisibleIndex="4" >
            <PropertiesDateEdit Width="250px" AllowNull="True">                
            </PropertiesDateEdit>            
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataComboBoxColumn FieldName="UserGender" Caption="Gender" VisibleIndex="5">
            <PropertiesComboBox Width="250px" TextField="Value" ValueField="Key" ValueType="System.String" DropDownStyle="DropDown">
                <Items>
                    <dx:ListEditItem Text="Female" Value="F"/>
                    <dx:ListEditItem Text="Male" Value="M"/>                    
                </Items>
            </PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataTextColumn FieldName="Mobile" Caption="Mobile" VisibleIndex="6">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="WorkEmail" Caption="Work Email" VisibleIndex="7">
             <propertiestextedit Width="250px">
                <validationsettings errordisplaymode="ImageWithTooltip">
                    <regularexpression validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        errortext="Invalid email address" />
                </validationsettings>
            </propertiestextedit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="TelNo1" Caption="Work Tel 1" VisibleIndex="8">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="TelNo2" Caption="Work Tel 2" VisibleIndex="9">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="WorkMobile" Caption="Work Mobile" VisibleIndex="10">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="JobTitle" Caption="Job Title" VisibleIndex="11">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="SalaryNo" Caption="Salary No" VisibleIndex="12">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Subsidiary" Caption="Subsidiary" VisibleIndex="13">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="BusinessUnit" Caption="Business Unit" VisibleIndex="14">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Location" Caption="Location" VisibleIndex="15">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CostCentre" Caption="Cost Centre" VisibleIndex="16">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Division" Caption="Division" VisibleIndex="17">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="UserGroup" Caption="Group" VisibleIndex="18">
            <PropertiesTextEdit Width="250px"></PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
    </columns>
    <Templates>
        <EditForm>
            <div style="padding: 4px 4px 4px 4px; width: 880px">
               <dx:ASPxGridViewTemplateReplacement ID="TemplateReplacementContent"  ReplacementType="EditFormContent" runat="server">
               </dx:ASPxGridViewTemplateReplacement>                               
            </div>   
        </EditForm>
    </Templates>
</dx:aspxgridview>
<br />
<dx:aspxgridview id="ValidUsersGridView" runat="server" clientinstancename="ValidUsersGridView"
    autogeneratecolumns="False" OnDataBinding="ValidUsersGridView_OnDataBinding" OnCustomCallback="ValidUsersGridView_OnCustomCallback" keyfieldname="Email"
    width="900px">
    <settings HorizontalScrollBarMode="Auto" showtitlepanel="true" />
    <settingstext title="Valid Users" />
    <settingspager pagesize="15">
    </settingspager>
    <columns>
        <dx:gridviewdatatextcolumn fieldname="Firstname" caption="Firstname" visibleindex="1" ReadOnly="True">            
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="Lastname" caption="Lastname" visibleindex="2" ReadOnly="True">            
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="Email" caption="Email" visibleindex="3" ReadOnly="True">            
        </dx:gridviewdatatextcolumn>
        <dx:GridViewDataDateColumn FieldName="DateOfBirth" Caption="Birth Date" VisibleIndex="4" ReadOnly="True" >            
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataComboBoxColumn FieldName="UserGender" Caption="Gender" VisibleIndex="5" ReadOnly="True">
            <PropertiesComboBox Width="250px" TextField="Value" ValueField="Key" ValueType="System.String" DropDownStyle="DropDown">
                <Items>
                    <dx:ListEditItem Text="Female" Value="F"/>
                    <dx:ListEditItem Text="Male" Value="M"/>                    
                </Items>
            </PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataTextColumn FieldName="Mobile" Caption="Mobile" VisibleIndex="6" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="WorkEmail" Caption="Work Email" VisibleIndex="7" ReadOnly="True">            
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="TelNo1" Caption="Work Tel 1" VisibleIndex="8" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="TelNo2" Caption="Work Tel 2" VisibleIndex="9" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="WorkMobile" Caption="Work Mobile" VisibleIndex="10" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="JobTitle" Caption="Job Title" VisibleIndex="11" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="SalaryNo" Caption="Salary No" VisibleIndex="12" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Subsidiary" Caption="Subsidiary" VisibleIndex="13" ReadOnly="True">            
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="BusinessUnit" Caption="Business Unit" VisibleIndex="14" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Location" Caption="Location" VisibleIndex="15" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CostCentre" Caption="Cost Centre" VisibleIndex="16" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Division" Caption="Division" VisibleIndex="17" ReadOnly="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="UserGroup" Caption="Group" VisibleIndex="18" ReadOnly="True">
        </dx:GridViewDataTextColumn>
    </columns>
</dx:aspxgridview>
<br />
<dx:aspxpopupcontrol id="InfoMessagePopupControl" runat="server" clientinstancename="InfoMessagePopupControl"
    popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter" allowdragging="false"
    headertext="Confirmation Message" closeaction="CloseButton" width="300px">
    <contentcollection>
        <dx:popupcontrolcontentcontrol>
            <dx:aspxpanel runat="server">
                <panelcollection>
                    <dx:panelcontent>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="SuccessLabel" runat="server" clientinstancename="SuccessLabel"
                                        text="Import completed successfully">
                                    </dx:aspxlabel>
                                    <dx:aspxlabel id="FailLabel" runat="server" clientinstancename="FailLabel" text="Failed to complete import. Please try again.">
                                    </dx:aspxlabel>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table style="width: 100%;">
                            <tr>
                                <td align="right">
                                    <dx:aspxbutton id="OkButton" runat="server" clientinstancename="OkButton" text="OK"
                                        autopostback="false">
                                        <clientsideevents click="OkButton_Click" />
                                    </dx:aspxbutton>
                                </td>
                            </tr>
                        </table>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxpanel>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>
<dx:aspxpopupcontrol id="UploadMessagePopupControl" runat="server" clientinstancename="UploadMessagePopupControl"
    popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter" allowdragging="false"
    headertext="Confirmation Message" closeaction="CloseButton" width="400px">
    <contentcollection>
        <dx:popupcontrolcontentcontrol>
            <dx:aspxpanel id="Aspxpanel1" runat="server">
                <panelcollection>
                    <dx:panelcontent>
                        <table>
                            <tr>
                                <td>
                                    <ul>
                                        <li>
                                            <dx:aspxlabel id="NumValidUserLabel" runat="server" clientinstancename="NumValidUserLabel" />
                                            &nbsp;<dx:aspxlabel id="ValidUserTextLabel" runat="server" clientinstancename="ValidUserTextLabel"
                                                text="valid users uploaded.">
                                            </dx:aspxlabel>
                                        </li>
                                        <li>
                                            <dx:aspxlabel id="NumInvalidUserLabel" runat="server" clientinstancename="NumInvalidUserLabel" />
                                            &nbsp;<dx:aspxlabel id="InValidUserTextLabel" runat="server" clientinstancename="InValidUserTextLabel"
                                                text="users failed validation.">
                                            </dx:aspxlabel>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="ValidUserInstructionLabel" runat="server" clientinstancename="ValidUserInstructionLabel"
                                        text="Users with validation errors appear in the 'Invalid Users' grid. These need to be corrected before clicking 'Import' button." />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table style="width: 100%;">
                            <tr>
                                <td align="right">
                                    <dx:aspxbutton id="UploadMessageOkButton" runat="server" clientinstancename="UploadMessageOkButton"
                                        text="OK" autopostback="false">
                                        <clientsideevents click="UploadMessageOkButton_Click" />
                                    </dx:aspxbutton>
                                </td>
                            </tr>
                        </table>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxpanel>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>
<asp:linqdatasource id="UserExceptionDataSource" runat="server" onselecting="UserExceptionDataSource_Selecting">
</asp:linqdatasource>
<dx:aspxloadingpanel id="LoadingPanel" runat="server" clientinstancename="LoadingPanel"
    modal="true">
</dx:aspxloadingpanel>
<dx:aspxcallback id="SaveCallback" runat="server" clientinstancename="SaveCallback"
    oncallback="SaveCallback_Callback">
    <clientsideevents callbackcomplete="SaveCallback_CallbackComplete" />
</dx:aspxcallback>
<asp:linqdatasource id="ClientDataSource" runat="server" onselecting="ClientDataSource_Selecting">
</asp:linqdatasource>
