﻿<%@ control language="C#" autoeventwireup="true" codebehind="GameControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.GameControl" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ register assembly="DevExpress.Web.ASPxTreeList.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web.ASPxTreeList" tagprefix="dx" %>




<script id="dxis_CreateGameControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/GameControl.js") %>'>

</script>
<dx:ASPxLoadingPanel ID="LoadingPanel" ClientInstanceName="LoadingPanel" runat="server"></dx:ASPxLoadingPanel>

<dx:aspxpopupcontrol id="DeleteCompletePopupControl" clientinstancename="DeleteCompletePopupControl" runat="server"
    closeaction="CloseButton" modal="true" popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter"
    headertext="Confirm" allowdragging="true" enableanimation="false" enableviewstate="false" showonpageload="False">
    <ClientSideEvents PopUp="function(s){s.UpdatePosition();}" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="DeleteCompleteLabel" ClientInstanceName="DeleteCompleteLabel" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                         <dx:ASPxButton runat="server" AutoPostBack="False" ID="CloseButton" ClientInstanceName="CloseButton" Text="Close">
                                <ClientSideEvents Click="CloseButton_Click"></ClientSideEvents>
                            </dx:ASPxButton>               
                    </td>
                </tr>
            </table>
        </dx:PopupControlContentControl>        
    </ContentCollection>
</dx:aspxpopupcontrol>

<dx:aspxpopupcontrol id="DeleteNotificationPopupControl" clientinstancename="DeleteNotificationPopupControl" runat="server"
    closeaction="CloseButton" modal="true" popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter"
    headertext="Confirm" allowdragging="true" enableanimation="false" Width="350px" enableviewstate="false" showonpageload="False">
    <ClientSideEvents PopUp="function(s){s.UpdatePosition();}" />
    <ContentCollection>        
        <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Deleting the game data will delete all the played games and user results associated to this game. Are you sure you want to delete this information?"/>
                    </td>
                </tr>
                <tr>
                    <td >
                         <table style="border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton runat="server" AutoPostBack="False" ID="DeleteDataButton" ClientInstanceName="DeleteGameButton" Text="Delete">
                                <ClientSideEvents Click="DeleteDataButton_Click"></ClientSideEvents>
                            </dx:ASPxButton>                   
                                            </td>
                                            <td class="spacer"></td>
                                            <td>
                                                <dx:ASPxButton runat="server" AutoPostBack="False" ID="CancelDataButton" ClientInstanceName="CancelDeleteGameButton" Text="Cancel">
                                <ClientSideEvents Click="CancelDataGameButton_Click"></ClientSideEvents>
                            </dx:ASPxButton>               
                                                </td>
                                        </tr>
                                    </table>
                    </td>
                </tr>
            </table>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:aspxpopupcontrol>
<dx:aspxpopupcontrol id="ProductSelectionPopupControl" runat="server" clientinstancename="ProductSelectionPopupControl"
    closeaction="None" showclosebutton="false" modal="true" popuphorizontalalign="WindowCenter"
    popupverticalalign="WindowCenter" headertext="Select a product" allowdragging="true"
    enableanimation="false" enableviewstate="false" width="250px">
    <contentcollection>
        <dx:popupcontrolcontentcontrol>
            <dx:aspxpanel id="ProductSelectionPanel" runat="server">
                <panelcollection>
                    <dx:panelcontent>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxcombobox id="ProductComboBox" runat="server" clientinstancename="ProductComboBox"
                                        valuetype="System.Int32" valuefield="ProductID" textfield="ProductName" dropdownstyle="DropDownList"
                                        datasourceid="ProductDataSource" validationsettings-validationgroup="ProductSelectionGroup">
                                        <validationsettings errordisplaymode="ImageWithTooltip">
                                            <requiredfield errortext="A product is required" isrequired="true" />
                                        </validationsettings>
                                        <clientsideevents selectedindexchanged="ProductComboBox_SelectedIndexChanged" />
                                    </dx:aspxcombobox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <dx:aspxbutton id="ConfirmProductButton" runat="server" clientinstancename="ConfirmProductButton"
                                                    autopostback="false" text="OK" validationgroup="ProductSelectionGroup">
                                                    <clientsideevents click="ConfirmProductButton_Click" />
                                                </dx:aspxbutton>
                                            </td>
                                            <td>
                                                <dx:aspxbutton id="CancelProductButton" runat="server" clientinstancename="CancelProductButton"
                                                    text="Cancel" causesvalidation="false" onclick="CancelProductButton_Click">
                                                </dx:aspxbutton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxpanel>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>
<dx:aspxcallback id="ProductSelectionCallback" runat="server" clientinstancename="ProductSelectionCallback"
    oncallback="ProductSelectionCallback_Callback">
    <clientsideevents callbackcomplete="ProductSelectionCallback_CallbackComplete" />
</dx:aspxcallback>
<dx:aspxcallback id="SaveGameCallback" runat="server" clientinstancename="SaveGameCallback"
    oncallback="SaveGameCallback_Callback">
    <clientsideevents callbackcomplete="SaveGameCallback_CallbackComplete" />
</dx:aspxcallback>
<dx:aspxcallback id="DeleteGameDataCallback" runat="server" clientinstancename="DeleteGameDataCallback"
    oncallback="DeleteGameDataCallback_OnCallback">
    <clientsideevents callbackcomplete="DeleteGameDataCallback_CallbackComplete" />
</dx:aspxcallback>
<table>
    <tr>
        <td colspan="6">
            <dx:aspxcallbackpanel id="ProductCallbackPanel" runat="server" clientinstancename="ProductCallbackPanel"
                width="100%" oncallback="ProductCallbackPanel_Callback">
                <panelcollection>
                    <dx:panelcontent runat="server">
                        <h2>
                            <asp:literal id="ProductNameLiteral" runat="server" text="">
                            </asp:literal>
                        </h2>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxcallbackpanel>
        </td>        
        <td align="right">
            <dx:ASPxButton ID="DeleteGameDataButton" ClientInstanceName="DeleteGameDataButton" runat="server" AutoPostBack="False" Text="Delete All Game Data">
                <ClientSideEvents Click="DeleteGameDataButton_Click"></ClientSideEvents>
            </dx:ASPxButton>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxlabel id="GameNameLabel" runat="server" text="Game Name">
            </dx:aspxlabel>
        </td>
        <td class="spacer">
        </td>
        <td>
            <dx:aspxtextbox id="GameNameTextBox" runat="server" clientinstancename="GameNameTextBox">
            </dx:aspxtextbox>
        </td>
        <td class="spacer">
        </td>
        <td>
            <dx:aspxlabel id="GameDescriptionLabel" runat="server" clientinstancename="GameDescription"
                text="Game Description">
            </dx:aspxlabel>
        </td>
        <td>
        </td>
        <td rowspan="2">
            <dx:aspxmemo id="GameDescriptionMemo" runat="server" clientinstancename="GameDescriptionMemo"
                width="250px" rows="4">
            </dx:aspxmemo>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxlabel id="CourseCodeLabel"  runat="server" text="Scorm Course Code">
            </dx:aspxlabel>
        </td>
        <td>
        </td>
        <td>
            <dx:aspxlabel id="CourseCodeTextLabel" runat="server" clientinstancename="CourseCodeTextLabel" Font-Bold="True" style="text-transform: uppercase;" Text="assigned automatically" />
            
        </td>
        <td class="spacer">
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr style="display: none;border-collapse: collapse;">
        <td>
            <dx:aspxlabel id="GameThemeLabel"  ClientVisible="False"  runat="server" text="Game Theme">
            </dx:aspxlabel>
        </td>
        <td>
        </td>
        <td>
            <dx:aspxcombobox id="GameThemeComboBox"  ClientVisible="False" runat="server" clientinstancename="GameThemeComboBox">
            </dx:aspxcombobox>
        </td>
        <td class="spacer">
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr style="display: none;border-collapse: collapse;">
        <td>
            <dx:aspxlabel id="GameTypeLabel"  ClientVisible="False"  runat="server" text="Game Type">
            </dx:aspxlabel>
        </td>
        <td>
        </td>
        <td>
            <dx:aspxcombobox id="GameTypeComboBox" ClientVisible="False"  runat="server" clientinstancename="GameTypeComboBox"
                dropdownstyle="DropDownList">
                <items>
                    <dx:listedititem text="Desktop" value="Desktop" />
                    <dx:listedititem text="Online" value="Online" />
                </items>
            </dx:aspxcombobox>
        </td>
        <td class="spacer">
        </td>
        <td>
            <dx:aspxlabel id="ScoringLabel"  ClientVisible="False"  runat="server" text="Scoring Level">
            </dx:aspxlabel>
        </td>
        <td>
        </td>
        <td>
            <dx:aspxcombobox id="ScoringComboBox"  ClientVisible="False"  runat="server" clientinstancename="ScoringComboBox">
                <items>
                    <dx:listedititem text="Card" value="Card" />
                    <dx:listedititem text="Category" value="Category" />
                </items>
            </dx:aspxcombobox>
        </td>
    </tr>
    <tr style="display: none;border-collapse: collapse;">
        <td>
            <dx:aspxlabel id="GameInLibraryLabel"  ClientVisible="False"  runat="server" text="Game Library">
            </dx:aspxlabel>
        </td>
        <td>
        </td>
        <td>
            <dx:aspxcombobox id="GameLibraryComboBox"  ClientVisible="False"  runat="server" clientinstancename="GameLibraryComboBox"
                dropdownstyle="DropDownList">
                <clientsideevents selectedindexchanged="GameLibraryComboBox_SelectedIndexChanged" />
                <items>
                    <dx:listedititem text="Client" value="Client" />
                    <dx:listedititem text="Public" value="Public" />
                </items>
            </dx:aspxcombobox>
        </td>
        <td>
        </td>
        <td>
            <dx:aspxlabel id="ClientLabel"  ClientVisible="False"  runat="server" text="Client Library">
            </dx:aspxlabel>
        </td>
        <td>
        </td>
        <td>
            <dx:aspxcombobox id="ClientLibraryComboBox"  ClientVisible="False"  runat="server" clientinstancename="ClientLibraryComboBox"
                datasourceid="ClientDataSource" valuefield="ClientID" valuetype="System.Int32"
                textfield="ClientName" dropdownstyle="DropDownList" clientenabled="false" oncallback="ClientLibraryComboBox_Callback" />
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxlabel id="NumTimesGameCanBePlayedLabel" runat="server" text="Number Of Plays">
            </dx:aspxlabel>
        </td>
        <td></td>
        <td>
            <table>
                <tr>
                    <td>
                        <dx:ASPxSpinEdit ID="NumberOfPlaySpinEdit" AllowNull="False" runat="server" MinValue="0" MaxValue="99999999" NumberType="Integer" >
                        </dx:ASPxSpinEdit>            
                    </td>
                    <td>
                        <dx:ASPxLabel runat="server" ID="ZeroPlaysLabel" ClientInstanceName="ZeroPlaysLabel" Text="(0 = unlimited plays)"/>
                    </td>
                </tr>
            </table>            
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <dx:aspxbutton id="SaveGameButton" runat="server" clientinstancename="SaveGameButton"
                text="Save" autopostback="false">
                <clientsideevents click="SaveGameButton_Click" />
            </dx:aspxbutton>
        </td>
    </tr>
</table>
<br />
<% if (!IsNewGame)
   { %>
<dx:aspxgridview id="GameCardGroupGridView" runat="server" clientinstancename="GameCardGroupGridView"
    datasourceid="GameCardGroupDataSource" autogeneratecolumns="false" keyfieldname="GameCardGroupId"
    width="100%" onrowinserting="GameCardGroupGridView_RowInserting" onrowupdating="GameCardGroupGridView_RowUpdating"
    oninitnewrow="GameCardGroupGridView_InitNewRow" onrowdeleting="GameCardGroupGridView_RowDeleting"
    ondatabound="GameCardGroupGridView_DataBound" oncustombuttoncallback="GameCardGroupGridView_CustomButtonCallback">
    <settings showstatusbar="Visible" showtitlepanel="true" />
    <settingsdetail showdetailrow="true" allowonlyonemasterrowexpanded="False" />
    <settingsediting mode="EditFormAndDisplayRow" />
    <settingsbehavior allowselectbyrowclick="true" allowselectsinglerowonly="true" allowsort="false" />
    <settingspager mode="ShowAllRecords" />
    <styles>
        <alternatingrow enabled="True" />
    </styles>
    <columns>
        <dx:gridviewcommandcolumn buttontype="Image" caption=" " width="30px" visibleindex="0">
            <custombuttons>
                <dx:gridviewcommandcolumncustombutton id="UpCustomButton" text="Up" visibility="AllDataRows">
                    <image url="~/App_Themes/Icons/ArrowUp.png" height="12" width="12" tooltip="Move Up" />
                </dx:gridviewcommandcolumncustombutton>
                <dx:gridviewcommandcolumncustombutton id="DownCustomButton" text="Down" visibility="AllDataRows">
                    <image url="~/App_Themes/Icons/ArrowDown.png" height="12" width="12" tooltip="Move Down" />
                </dx:gridviewcommandcolumncustombutton>
            </custombuttons>
        </dx:gridviewcommandcolumn>
        <dx:gridviewcommandcolumn visibleindex="1" ShowEditButton="true" ShowDeleteButton="true">
            <headercaptiontemplate>
                <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="AddNewRow(GameCardGroupGridView);return false;" text="New"/>
            </headercaptiontemplate>
        </dx:gridviewcommandcolumn>
        <dx:gridviewdatatextcolumn fieldname="CardGroupSequenceNo" visibleindex="2">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatacheckcolumn fieldname="IsAMandatoryCardGroup" visibleindex="3">
        </dx:gridviewdatacheckcolumn>
    </columns>
    <templates>
        <titlepanel>
            <h4>
                <asp:literal id="GameCardGroupTitleLiteral" runat="server" text="Game Card Group"></asp:literal></h4>
        </titlepanel>
        <editform>
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx:aspxcheckbox id="IsAMandatoryCardGroupCheckBox" runat="server" clientinstancename="IsAMandatoryCardGroupCheckBox"
                            text="This card group is mandatory" OnInit="IsAMandatoryCardGroupCheckBox_OnInit" value='<%# Bind("IsAMandatoryCardGroup") %>'>
                        </dx:aspxcheckbox>
                    </td>
                </tr>
                <%if (GameCardGroupGridView.IsNewRowEditing)
                  {%>
                <tr>
                    <td>
                        <dx:aspxlabel id="SelectCardLabel" runat="server" text="Select Card" clientvisible='<%# GameCardGroupGridView.IsNewRowEditing %>'>
                        </dx:aspxlabel>
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx:aspxcombobox id="SelectCardComboBox" runat="server" clientinstancename="SelectCardComboBox"
                            validationsettings-validationgroup='<%# Container.ValidationGroup %>' dropdownstyle="DropDownList"
                            clientvisible='<%# GameCardGroupGridView.IsNewRowEditing %>'>
                            <clientsideevents gotfocus="SelectCardComboBox_Click" />
                            <validationsettings errordisplaymode="ImageWithTooltip">
                                <requiredfield isrequired="true" errortext="Selection of a card is required" />
                            </validationsettings>
                        </dx:aspxcombobox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx:aspxcheckbox id="IsAMandatoryCardCheckBox" runat="server" clientinstancename="IsAMandatoryCardCheckBox"
                            text="Is the selected card mandatory" OnInit="IsAMandatoryCardCheckBox_OnInit" clientvisible='<%# GameCardGroupGridView.IsNewRowEditing %>'>
                        </dx:aspxcheckbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:aspxlabel id="LongShortVersionLabel" runat="server" text="Question/Answer Version"
                            clientvisible='<%# GameCardGroupGridView.IsNewRowEditing %>'>
                        </dx:aspxlabel>
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx:aspxcombobox id="LongShortVersionComboBox" runat="server" clientinstancename="LongShortVersionComboBox"
                            dropdownstyle="DropDownList" validationsettings-validationgroup='<%# Container.ValidationGroup %>'
                            clientvisible='<%# GameCardGroupGridView.IsNewRowEditing %>'>
                            <validationsettings errordisplaymode="ImageWithTooltip">
                                <requiredfield isrequired="true" errortext="Question/Answer version is required" />
                            </validationsettings>
                            <items>
                                <dx:listedititem text="Long" value="L" />
                                <dx:listedititem text="Short" value="S" />
                            </items>
                        </dx:aspxcombobox>
                    </td>
                </tr>
                <%
                  }
                %>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div id="aspxGridViewUpdateCancelButtonBar">
                            <div id="aspxGridViewCancelButton">
                                <dx:aspxgridviewtemplatereplacement id="CancelButton" replacementtype="EditFormCancelButton"
                                    runat="server" />
                            </div>
                            &nbsp;&nbsp;
                            <asp:linkbutton id="UpdateLinkButton" runat="server" onclientclick="GameCardGroupGridViewUpdateLinkButton_Click();return false;"
                                text="Update" />
                            <dx:aspxdateedit id="LastModifiedOnDateTimeDateEdit" runat="server" clientvisible="false"
                                value='<%#Bind("LastModifiedOnDateTime")%>'>
                            </dx:aspxdateedit>
                        </div>
                    </td>
                </tr>
            </table>
        </editform>
        <detailrow>
            <dx:aspxgridview id="GameCardGridView" runat="server" clientinstancename="GameCardGridView"
                autogeneratecolumns="false" keyfieldname="GameCardId" width="100%" datasourceid="GameCardDataSource"
                onbeforeperformdataselect="GameCardGridView_BeforePerformDataSelect" onrowinserting="GameCardGridView_RowInserting"
                onrowdeleting="GameCardGridView_RowDeleting" onrowupdating="GameCardGridView_RowUpdating"
                oncustombuttoncallback="GameCardGridView_CustomButtonCallback" oninit="GameCardGridView_Init" OnInitNewRow="GameCardGridView_OnInitNewRow">
                <clientsideevents begincallback="GameCardGridView_BeginCallback" endcallback="GameCardGridView_EndCallback" />
                <settings showpreview="true" showstatusbar="Visible" showtitlepanel="true" />
                <settingsediting mode="EditFormAndDisplayRow" />
                <settingsbehavior allowmultiselection="true" allowselectsinglerowonly="true" allowsort="false"
                    autoexpandallgroups="True" />
                <settingspager mode="ShowAllRecords" />
                <styles>
                    <alternatingrow enabled="True" />
                </styles>
                <columns>
                    <dx:gridviewcommandcolumn buttontype="Image" caption=" " width="30px" visibleindex="0">
                        <custombuttons>
                            <dx:gridviewcommandcolumncustombutton id="UpCustomButton" text="Up" visibility="AllDataRows">
                                <image url="~/App_Themes/Icons/ArrowUp.png" height="12" width="12" tooltip="Move Up" />
                            </dx:gridviewcommandcolumncustombutton>
                            <dx:gridviewcommandcolumncustombutton id="DownCustomButton" text="Down" visibility="AllDataRows">
                                <image url="~/App_Themes/Icons/ArrowDown.png" height="12" width="12" tooltip="Move Down" />
                            </dx:gridviewcommandcolumncustombutton>
                        </custombuttons>
                    </dx:gridviewcommandcolumn>
                    <dx:gridviewcommandcolumn visibleindex="1" ShowEditButton="true" ShowDeleteButton="true">
                        <headercaptiontemplate>
                            <dx:aspxhyperlink id="NewLinkButton" runat="server" text="New" oninit="NewLinkButton_Init"/>
                        </headercaptiontemplate>
                    </dx:gridviewcommandcolumn>                    
                    <dx:gridviewdatatextcolumn fieldname="SequenceNo" caption="Sequence No" visibleindex="2"
                        readonly="true">
                    </dx:gridviewdatatextcolumn>
                    <dx:gridviewdatatextcolumn fieldname="CardShortcutCode" caption="Shortcut Code" visibleindex="3">
                    </dx:gridviewdatatextcolumn>
                    <dx:gridviewdatatextcolumn fieldname="CardName" caption="Card Name" visibleindex="4">
                    </dx:gridviewdatatextcolumn>
                    <dx:gridviewdatacheckcolumn fieldname="IsRatified" caption="Ratified" visibleindex="5">
                    </dx:gridviewdatacheckcolumn>
                    <dx:gridviewdatacheckcolumn fieldname="IsAMandatoryCard" caption="Mandatory Card" visibleindex="6">                        
                    </dx:gridviewdatacheckcolumn>
                    <dx:GridViewCommandColumn  caption=" " VisibleIndex="7">
                        <CustomButtons>
                            <dx:gridviewcommandcolumncustombutton id="EditCardCustomButton" text="Edit Card" visibility="AllDataRows" />
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                </columns>
                <templates>
                    <titlepanel>
                        <asp:literal id="GameCardTitleLiteral" runat="server" text="Game Cards"></asp:literal>
                    </titlepanel>
                    <editform>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="SelectCardLabel" runat="server" text="Select Card">
                                    </dx:aspxlabel>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <dx:aspxcombobox id="SelectDetailCardComboBox" runat="server" clientinstancename="SelectDetailCardComboBox"
                                        validationsettings-validationgroup='<%#Container.ValidationGroup%>' dropdownstyle="DropDownList"
                                        oninit="SelectDetailCardComboBox_Init">
                                        <clientsideevents gotfocus="SelectDetailCardComboBox_Click" />
                                        <validationsettings errordisplaymode="ImageWithTooltip">
                                            <requiredfield isrequired="true" errortext="Selection of a card is required" />
                                        </validationsettings>
                                    </dx:aspxcombobox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <dx:aspxcheckbox id="IsAMandatoryCardCheckBox" runat="server" clientinstancename="IsAMandatoryCardCheckBox"
                                        text="Is the selected card mandatory" value='<%# Bind("IsAMandatoryCard") %>'>
                                    </dx:aspxcheckbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="LongShortVersionLabel" runat="server" text="Question/Answer Version">
                                    </dx:aspxlabel>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <dx:aspxcombobox id="LongShortVersionComboBox" runat="server" clientinstancename="LongShortVersionComboBox"
                                        dropdownstyle="DropDownList" validationsettings-validationgroup='<%#Container.ValidationGroup %>'
                                        ondatabound="LongShortVersionComboBox_DataBound">
                                        <validationsettings errordisplaymode="ImageWithTooltip">
                                            <requiredfield isrequired="true" errortext="Question/Answer version is required" />
                                        </validationsettings>
                                        <items>
                                            <dx:listedititem text="Long" value="L" />
                                            <dx:listedititem text="Short" value="S" />
                                        </items>
                                    </dx:aspxcombobox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div id="aspxGridViewUpdateCancelButtonBar">
                                        <div id="aspxGridViewCancelButton">
                                            <dx:aspxgridviewtemplatereplacement id="CancelButton" replacementtype="EditFormCancelButton"
                                                runat="server" />
                                        </div>
                                        &nbsp;&nbsp;
                                        <dx:aspxgridviewtemplatereplacement id="UpdateButton" replacementtype="EditFormUpdateButton"
                                            runat="server" />
                                        <dx:aspxdateedit id="LastModifiedOnDateTimeDateEdit" runat="server" clientvisible="false"
                                            value='<%#Bind("LastModifiedOnDateTime")%>'>
                                        </dx:aspxdateedit>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </editform>
                    <previewrow>
                        <%# Eval("DetailedCardDescription") %>
                    </previewrow>
                </templates>
            </dx:aspxgridview>
        </detailrow>
    </templates>
</dx:aspxgridview>
<%} %>
<dx:aspxpopupcontrol id="InformationMessagePopupControl" runat="server" clientinstancename="InformationMessagePopupControl"
    popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter" allowdragging="false"
    headertext="Confirmation Message" closeaction="CloseButton" width="300px">
    <contentcollection>
        <dx:popupcontrolcontentcontrol>
            <dx:aspxpanel runat="server">
                <panelcollection>
                    <dx:panelcontent>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="SuccessLabel" runat="server" clientinstancename="SuccessLabel"
                                        text="Saved game successfully">
                                    </dx:aspxlabel>
                                    <dx:aspxlabel id="FailLabel" runat="server" clientinstancename="FailLabel" text="Failed to save game successfully">
                                    </dx:aspxlabel>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <dx:aspxbutton id="OkButton" runat="server" clientinstancename="OkButton" text="OK"
                                        autopostback="false">
                                        <clientsideevents click="OkButton_Click" />
                                    </dx:aspxbutton>
                                </td>
                            </tr>
                        </table>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxpanel>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>
<dx:aspxpopupcontrol id="CardFinderPopupControl" runat="server" clientinstancename="CardFinderPopupControl"
    closeaction="CloseButton" modal="true" popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter"
    headertext="Card Finder" allowdragging="true" enableanimation="false" width="800px"
    showclosebutton="false" showfooter="true" footertext="" enableviewstate="false"
    loadcontentviacallback="OnPageLoad" ShowOnPageLoad="False">
    <clientsideevents closebuttonclick="CardFinderPopupControl_CloseButtonClick" PopUp="function(s){s.UpdatePosition();}" />
    <footertemplate>
        <table width="100%" style="border-collapse: collapse;">
            <tr>
                <td align="right">
                    <table>
                        <tr>
                            <td>
                                <dx:aspxbutton id="SelectCardButton" runat="server" clientinstancename="SelectCardButton"
                                    text=" OK " autopostback="false">
                                    <clientsideevents click="SelectCardButton_Click" />
                                </dx:aspxbutton>
                            </td>
                            <td>
                                <dx:aspxbutton id="CancelCardButton" runat="server" clientinstancename="CancelButton"
                                    text="Cancel" autopostback="false">
                                    <clientsideevents click="CancelCardButton_Click" />
                                </dx:aspxbutton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </footertemplate>
    <contentcollection>
        <dx:popupcontrolcontentcontrol>
            <dx:aspxpanel id="CardFinderPanel" runat="server" ScrollBars="Auto" Height="550px">
                <panelcollection>
                    <dx:panelcontent>
                        <table style="height: 100%;">
                            <tr>
                                <td valign="top" >
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:aspxlabel id="CardTypeLabel" runat="server" text="Card Type">
                                                </dx:aspxlabel>
                                            </td>
                                            <td>
                                            </td>
                                            <td>                                                
                                            </td>
                                        </tr>        
                                        <tr>
                                            <td colspan="3">
                                                <dx:ASPxListBox ID="CardTypeListBox" ClientInstanceName="CardTypeListBox" runat="server"
                                                    DataSourceID="CardTypeDataSource" ValueField="Internal_CardTypeID" ValueType="System.Int32"
                                                    TextField="Internal_CardTypeName" Height="350px"
                                                    ></dx:ASPxListBox>                                                
                                            </td>
                                        </tr>     
                                        <tr>
                                            <td colspan="3" class="spacer">                                                
                                            </td>
                                        </tr>                                                                    
                                        <tr>
                                            <td colspan="3">
                                                <dx:aspxtreelist id="CardClassificationTreeList" runat="server" autogeneratecolumns="False"
                                                    datasourceid="CardClassificationDataSource" width="255px" datacachemode="Disabled"
                                                    clientinstancename="CardClassificationTreeList" keyfieldname="CardClassificationGroupID"
                                                    parentfieldname="CardClassificationGroupID_Parent" caption="Classifications"
                                                    ondatabound="CardClassificationTreeList_DataBound">
                                                    <settings showcolumnheaders="false" />
                                                    <border borderstyle="Solid" />
                                                    <settingsselection enabled="true" allowselectall="true" />
                                                    <columns>
                                                        <dx:treelisttextcolumn fieldname="CardClassificationGroupID" caption="" visibleindex="0"
                                                            visible="false">
                                                        </dx:treelisttextcolumn>
                                                        <dx:treelisttextcolumn fieldname="CardClassificationGroup" caption="" visibleindex="1">
                                                        </dx:treelisttextcolumn>
                                                    </columns>
                                                </dx:aspxtreelist>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <dx:aspxtextbox id="SearchTextBox" runat="server" clientinstancename="SearchTextBox"
                                                    nulltext="Search text within cards" width="255px">
                                                </dx:aspxtextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="right">
                                                <dx:aspxbutton id="SearchButton" runat="server" clientinstancename="SearchButton"
                                                    text="Search" autopostback="false">
                                                    <clientsideevents click="SearchButton_Click" />
                                                </dx:aspxbutton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="border-left: 1px solid #B8B8B8; width: 25px;">
                                    &nbsp;
                                </td>
                                <td valign="top">
                                    <dx:aspxgridview id="SearchResultGridView" runat="server" clientinstancename="SearchResultGridView"
                                        autogeneratecolumns="false" keyfieldname="CardId" datasourceid="SearchResultDataSource"
                                        oncustomcallback="SearchResultGridView_CustomCallback" width="400px">
                                        <settings showpreview="true" />
                                        <settingsbehavior allowselectsinglerowonly="true" allowselectbyrowclick="true" />
                                        <settingspager pagesize="10">
                                        </settingspager>
                                        <columns>
                                            <dx:gridviewcommandcolumn showselectcheckbox="true" visibleindex="0">
                                            </dx:gridviewcommandcolumn>
                                            <dx:gridviewdatatextcolumn fieldname="CardId" visible="false" readonly="true" visibleindex="1">
                                            </dx:gridviewdatatextcolumn>
                                            <dx:gridviewdatatextcolumn fieldname="CardShortcutCode" caption="Shortcut Code" visibleindex="2">
                                            </dx:gridviewdatatextcolumn>
                                            <dx:gridviewdatatextcolumn caption="CardName" fieldname="CardName" visibleindex="3">
                                            </dx:gridviewdatatextcolumn>
                                            <dx:gridviewdatacheckcolumn caption="Ratified" fieldname="IsRatified" visibleindex="4" />
                                        </columns>
                                        <templates>
                                            <previewrow>
                                                <%# Eval("DetailedCardDescription") %>
                                            </previewrow>
                                        </templates>
                                    </dx:aspxgridview>
                                </td>
                            </tr>
                        </table>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxpanel>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>
<asp:linqdatasource id="CardClassificationDataSource" runat="server" onselecting="CardClassificationDataSource_Selecting"
    enabledelete="false" enableinsert="false" enableupdate="false">
</asp:linqdatasource>
<asp:linqdatasource id="ProductDataSource" runat="server" onselecting="ProductDataSource_Selecting"
    enabledelete="false" enableinsert="false" enableupdate="false">
</asp:linqdatasource>
<asp:linqdatasource id="SearchResultDataSource" runat="server" onselecting="SearchResultDataSource_Selecting"
    enabledelete="false" enableinsert="false" enableupdate="false">
</asp:linqdatasource>
<asp:linqdatasource id="ClientDataSource" runat="server" onselecting="ClientDataSource_Selecting"
    enabledelete="false" enableinsert="false" enableupdate="false">
</asp:linqdatasource>
<asp:linqdatasource id="CardTypeDataSource" runat="server" onselecting="CardTypeDataSource_Selecting"
    enabledelete="false" enableinsert="false" enableupdate="false">
</asp:linqdatasource>
<asp:linqdatasource id="GameCardGroupDataSource" runat="server" onselecting="GameCardGroupDataSource_Selecting"
    enabledelete="false" enableinsert="false" enableupdate="false">
</asp:linqdatasource>
<asp:linqdatasource id="GameCardDataSource" runat="server" onselecting="GameCardDataSource_Selecting"
    enabledelete="false" enableinsert="false" enableupdate="false">
</asp:linqdatasource>
