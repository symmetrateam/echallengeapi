﻿function ProductGridView_OnUpdateButtonClick() {
    var count = DistributorAccessGridView.GetSelectedRowCount();

    var messageElement = DistributorAccessValidateMessageLabel.GetMainElement();
    if (count == 0) {
        messageElement.style.color = 'Red';
    }
    else {
        messageElement.style.color = 'Black';
    }

    if (ASPxClientEdit.ValidateGroup('EditForm', true) && count > 0) {
        ProductGridView.UpdateEdit();
    }
}

function ProductLanguageGridView_OnUpdateButtonClick() {
    var count = DistributorAccessGridView.GetSelectedRowCount();

    var messageElement = DistributorAccessValidateMessageLabel.GetMainElement();
    if (count == 0) {
        messageElement.style.color = 'Red';
    }
    else {
        messageElement.style.color = 'Black';
    }

    if (ASPxClientEdit.ValidateGroup('EditForm', true) && count > 0) {
        ProductLanguageGridView.UpdateEdit();
    }
}

function DefaultThemeComboBox_Validate(s, e) {
    var selectedItem = s.GetSelectedItem();
    if (selectedItem == null) {
        e.isValid = false;
    }
}

function QuestionMixComboxBox_Validate(s, e) {
    var isAccessGrantedCheckBox = GetIsAccessGrantedCheckBox(s.cp_VisibleIndex);

    if (isAccessGrantedCheckBox.GetChecked()) {
        var selectedItem = s.GetSelectedItem();
        if (selectedItem == null) {
            e.isValid = false;
        }
    }    

}

function DistributorAccessGridView_SelectionChanged(s, e) {
    if (e.isSelected) {
        var isAccessGrantedCheckBox = GetIsAccessGrantedCheckBox(e.visibleIndex);
        var questionMixComboBox = GetQuestionMixComboBox(e.visibleIndex);
        isAccessGrantedCheckBox.SetChecked(true);
        questionMixComboBox.SetEnabled(true);
    }
}

var currentRowIndex = -1;
function IsAccessGrantedCheckBox_CheckChanged(s, e) {
        
    var isChecked = s.GetChecked();
    var count = DistributorAccessGridView.GetSelectedRowCount();
    var clientInstanceName = s.cp_ClientInstanceName;
    var visibleIndex = s.cp_VisibleIndex;
    var questionMixComboBox = GetQuestionMixComboBox(visibleIndex);

    currentRowIndex = -1;

    if (isChecked) {
        if (count == 0) {
            currentRowIndex = s.cp_VisibleIndex;
            DistributorAccessGridView.GetRowValues(currentRowIndex, 'HasAvailableProducts', OnCheckDistributorIsAvailable);
        }
    }
    else {
        if (count > 0) {
            var selectedKeys = DistributorAccessGridView.GetSelectedKeysOnPage();
            //as we know this is a single select grid
            var rowKey = selectedKeys[0];
            if (rowKey == s.cp_DistributorId) {
                s.SetChecked(true);
            }
        }
    }

    if (!s.GetChecked()) {
        questionMixComboBox.SetSelectedIndex(-1);
    }

    questionMixComboBox.SetEnabled(s.GetChecked());
}

function OnCheckDistributorIsAvailable(val) {
    if(currentRowIndex < 0) {
        return;
    }
    
    if (val) {
        DistributorAccessGridView.SelectRowOnPage(currentRowIndex);
    }
}

function GetIsAccessGrantedCheckBox(visibleIndex) {
    return eval("IsAccessGrantedCheckBox_" + visibleIndex);
}

function GetQuestionMixComboBox(visibleIndex) {
    return eval("QuestionMixComboBox_" + visibleIndex);
}


function ProductGridView_CustomButtonClick(s, e) {

    if (e.buttonID != 'DeleteProductButton') {
        return;
    }

    if (confirm('Deleting this Product will delete all associated Cards, Users and their game data. Are you sure you want to delete this Product?')) {
        s.DeleteRow(e.visibleIndex);
    }
}