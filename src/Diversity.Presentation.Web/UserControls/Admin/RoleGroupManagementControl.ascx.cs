﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BlueTorque.Net.Security.Web;
using DevExpress.Web;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class RoleGroupManagementControl : System.Web.UI.UserControl
    {
        private const string GROUP_KEY = "GroupName";


        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private string SelectedRoleGroupKey
        {
            get { return Convert.ToString(Session[GROUP_KEY]); }
            set { Session[GROUP_KEY] = value; }
        }


        protected void RolesDualListBox_DataBinding(object sender, EventArgs e)
        {
            var rolesDualListBoxControl = sender as DualListBoxControl;

            string[] allRoles = Roles.GetAllRoles().Where(r => r != GlobalConstants.USERS_ROLE).ToArray();

            var completeList = allRoles.Select(role => new ListEditItem(role, role)).ToList();

            string[] assignedGroupRoles = null;
            
            assignedGroupRoles = string.IsNullOrEmpty(SelectedRoleGroupKey) ? new string[] { } : RolesGroups.GetRoleGroupRoles(SelectedRoleGroupKey).ToArray();

            var selected = assignedGroupRoles.Select(role => new ListEditItem(role, role)).ToList();

            rolesDualListBoxControl.SetInitialValues(completeList, selected);
        }

        protected void RoleGroupGridView_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            SelectedRoleGroupKey = Convert.ToString(e.EditingKeyValue);
        }

        protected void RoleGroupGridView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            //so that the session key is refreshed
            SelectedRoleGroupKey = string.Empty;
        }

        protected void RoleGroupGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var groupNameTextBox = RoleGroupGridView.FindEditFormTemplateControl("GroupNameTextBox") as ASPxTextBox;            
            var dualListBoxControl = RoleGroupGridView.FindEditFormTemplateControl(@"RolesDualListBox") as DualListBoxControl;

            if (groupNameTextBox != null && !string.IsNullOrEmpty(groupNameTextBox.Text))
            {
                var groupName = groupNameTextBox.Text.Trim();
                var selectedItems = dualListBoxControl.SelectedItems;

                if (selectedItems.Count() > 0)
                {
                    var selectedRoles = selectedItems.ConvertAll(c => c.Value.ToString());
                    RolesGroups.CreateRoleGroup(groupName, selectedRoles);

                    e.Cancel = true;
                    RoleGroupGridView.CancelEdit();

                    SelectedRoleGroupKey = null;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("@One or more roles must be selected.");
                }
            }
            else
            {
                throw new Exception("A valid role group name must be provided.");
            }
        }

        protected void RoleGroupDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();
            e.Result = dataContext.GetRoleGroups(Membership.ApplicationName);
        }

        protected void RoleGroupGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            if (!string.IsNullOrEmpty(SelectedRoleGroupKey))
            {
                var groupNameTextBox = RoleGroupGridView.FindEditFormTemplateControl("GroupNameTextBox") as ASPxTextBox;
                var groupName = groupNameTextBox.Text.Trim();
                if(groupName.ToLower() != SelectedRoleGroupKey.ToLower())
                {
                    var exists =
                        RolesGroups.GetRoleGroups().Where(r => r.GroupName.ToLower() == groupName.ToLower());
                        
                    if(exists.Count() > 0)
                    {
                        throw new Exception(@"Group name already exists. Try a new name");
                    }
                }

                var dualListBoxControl = RoleGroupGridView.FindEditFormTemplateControl(@"RolesDualListBox") as DualListBoxControl;
                
                var selectedRoles = dualListBoxControl.SelectedItems.ConvertAll(c => c.Value.ToString());

                //first delete the original entire group
                RolesGroups.DeleteRoleGroup(SelectedRoleGroupKey);

                //now recreate
                RolesGroups.CreateRoleGroup(groupName, selectedRoles);

                

                //var applicationId = (from a in dataContext.aspnet_Applications
                //                     where a.ApplicationName == Membership.ApplicationName
                //                     select a.ApplicationId).FirstOrDefault();

                //var roleGroup = (from r in dataContext.aspnet_UserRolesGroups
                //                 where r.ApplicationId == applicationId && r.LoweredUserRolesGroupName == SelectedRoleGroupKey.ToLower()
                //                 select r).FirstOrDefault();

                //roleGroup.UserRolesGroupName = groupNameTextBox.Text.Trim();
                //roleGroup.LoweredUserRolesGroupName = groupNameTextBox.Text.Trim().ToLower();
                //dataContext.SubmitChanges();

                e.Cancel = true;
                RoleGroupGridView.CancelEdit();

                SelectedRoleGroupKey = null;
            }
            else
            {
                throw new Exception("Role group does not exist. Please refresh data and try again.");
            }

        }

        protected void RoleGroupGridView_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            var dualListBox =
                (sender as ASPxGridView).FindEditFormTemplateControl(@"RolesDualListBox") as DualListBoxControl;

            if(dualListBox.SelectedItems.Count <=0)
            {
                e.RowError = "At least one role must be selected.";
            }
        }
    }
}