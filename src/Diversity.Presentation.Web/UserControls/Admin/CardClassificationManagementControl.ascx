﻿<%@ control language="C#" autoeventwireup="true" codebehind="CardClassificationManagementControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.CardClassificationManagementControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ register assembly="DevExpress.Web.ASPxTreeList.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web.ASPxTreeList" tagprefix="dx" %>

<script id="dxis_CardClassificationManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/CardClassificationManagementControl.js") %>'>

</script>
<dx:aspxgridview id="ProductGridView" runat="server" autogeneratecolumns="False"
    datasourceid="ProductDataSource" keyfieldname="ProductID" width="927px">
    <settingsdetail allowonlyonemasterrowexpanded="true" showdetailrow="true" />
    <columns>
        <dx:gridviewdatatextcolumn fieldname="ProductID" visibleindex="0" visible="false">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="ProductName" visibleindex="1">
        </dx:gridviewdatatextcolumn>
    </columns>
    <templates>
        <detailrow>
            <table>
                <tr>
                    <td valign="top">
                        <dx:aspxtreelist id="CardClassificationTreeList" runat="server" clientinstancename="CardClassificationTreeList"
                            caption="Card Classifications" autogeneratecolumns="False" datasourceid="CardClassificationGroupDataSource"
                            datacachemode="Disabled" keyfieldname="CardClassificationGroupID" parentfieldname="CardClassificationGroupID_Parent"
                            onnodeinserting="CardClassificationTreeList_NodeInserting" oncancelnodeediting="CardClassificationTreeList_CancelNodeEditing"
                            onnodeupdating="CardClassificationTreeList_NodeUpdating" onstartnodeediting="CardClassificationTreeList_StartNodeEditing"
                            oncommandcolumnbuttoninitialize="CardClassificationTreeList_CommandColumnButtonInitialize"
                            width="420px" oninit="CardClassificationTreeList_Init">
                            <columns>
                                <dx:treelistcommandcolumn visibleindex="0" shownewbuttoninheader="true">
                                    <editbutton visible="True">
                                    </editbutton>
                                    <newbutton visible="True">
                                    </newbutton>
                                    <deletebutton visible="True">
                                    </deletebutton>
                                </dx:treelistcommandcolumn>
                                <dx:treelisttextcolumn fieldname="CardClassificationGroup" caption="Classification Group"
                                    visibleindex="1">
                                    <propertiestextedit>
                                        <validationsettings setfocusonerror="true" validationgroup="EditForm">
                                            <requiredfield isrequired="true" errortext="Meta tag is a required field" />
                                        </validationsettings>
                                    </propertiestextedit>
                                </dx:treelisttextcolumn>
                                <dx:treelistdatetimecolumn fieldname="LanguageLastModifiedOnDateTime" visibleindex="2"
                                    readonly="true" visible="false">
                                    <propertiesdateedit>
                                    </propertiesdateedit>
                                </dx:treelistdatetimecolumn>
                                <dx:treelistdatetimecolumn fieldname="ParentLastModifiedOnDateTime" visibleindex="3"
                                    readonly="true" visible="false">
                                    <propertiesdateedit>
                                    </propertiesdateedit>
                                </dx:treelistdatetimecolumn>
                                <dx:treelistcheckcolumn visibleindex="4" visible="false">
                                    <datacelltemplate>
                                        <%# Eval("IsAMetaTagClassificationGroup") %>
                                    </datacelltemplate>
                                </dx:treelistcheckcolumn>
                            </columns>
                            <settings showtreelines="true" gridlines="Both" />
                            <settingsediting allownodedragdrop="True" mode="Inline" />
                        </dx:aspxtreelist>
                    </td>
                    <td class="spacerlarge">
                    </td>
                    <td valign="top">
                        <dx:aspxtreelist id="CardMetaTagsTreeList" runat="server" caption="Card Metatags"
                            clientinstancename="CardMetaTagsTreeList" autogeneratecolumns="False" datasourceid="CardClassificationMetaTagDataSource"
                            datacachemode="Disabled" keyfieldname="CardClassificationGroupID" parentfieldname="CardClassificationGroupID_Parent"
                            oncancelnodeediting="CardMetaTagsTreeList_CancelNodeEditing" onnodeinserting="CardMetaTagsTreeList_NodeInserting"
                            onnodeupdating="CardMetaTagsTreeList_NodeUpdating" onstartnodeediting="CardMetaTagsTreeList_StartNodeEditing"
                            oncommandcolumnbuttoninitialize="CardMetaTagsTreeList_CommandColumnButtonInitialize"
                            width="420px" oninit="CardMetaTagsTreeList_Init">
                            <columns>
                                <dx:treelistcommandcolumn visibleindex="0" shownewbuttoninheader="true">
                                    <editbutton visible="True">
                                    </editbutton>
                                    <newbutton visible="True">
                                    </newbutton>
                                    <deletebutton visible="True">
                                    </deletebutton>
                                </dx:treelistcommandcolumn>
                                <dx:treelisttextcolumn fieldname="CardClassificationGroup" caption="Meta tag" visibleindex="1">
                                    <propertiestextedit>
                                        <validationsettings setfocusonerror="true" validationgroup="EditForm">
                                            <requiredfield isrequired="true" errortext="Meta tag is a required field" />
                                        </validationsettings>
                                    </propertiestextedit>
                                </dx:treelisttextcolumn>
                                <dx:treelisttextcolumn fieldname="LanguageLastModifiedOnDateTime" visibleindex="2"
                                    visible="false">
                                    <datacelltemplate>
                                        <dx:aspxdateedit id="DateEdit" runat="server" readonly="true" value='<%# Bind("LanguageLastModifiedOnDateTime") %>'>
                                        </dx:aspxdateedit>
                                    </datacelltemplate>
                                </dx:treelisttextcolumn>
                                <dx:treelisttextcolumn fieldname="ParentLastModifiedOnDateTime" visibleindex="3"
                                    visible="false">
                                    <datacelltemplate>
                                        <dx:aspxdateedit id="DateEdit" runat="server" readonly="true" value='<%# Bind("LanguageLastModifiedOnDateTime") %>'>
                                        </dx:aspxdateedit>
                                    </datacelltemplate>
                                </dx:treelisttextcolumn>
                                <dx:treelistcheckcolumn visibleindex="4" visible="false">
                                    <datacelltemplate>
                                        <%# Eval("IsAMetaTagClassificationGroup") %>
                                    </datacelltemplate>
                                </dx:treelistcheckcolumn>
                            </columns>
                            <settings showtreelines="true" gridlines="Both" />
                            <settingsediting allownodedragdrop="True" mode="Inline" />
                        </dx:aspxtreelist>
                    </td>
                </tr>
            </table>
        </detailrow>
    </templates>
</dx:aspxgridview>
<asp:linqdatasource id="CardClassificationGroupDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="CardClassificationGroupLanguage" onselecting="CardClassificationGroupDataSource_Selecting"
    tablename="CardClassificationGroup_Languages">
</asp:linqdatasource>
<asp:linqdatasource id="CardClassificationMetaTagDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="CardClassificationGroupLanguage" onselecting="CardClassificationMetaTagDataSource_Selecting"
    tablename="CardClassificationGroup_Languages">
</asp:linqdatasource>
<asp:linqdatasource id="ProductDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" tablename="Products" onselecting="ProductDataSource_Selecting">
</asp:linqdatasource>
