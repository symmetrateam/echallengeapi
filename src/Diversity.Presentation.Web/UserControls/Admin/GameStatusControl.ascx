﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameStatusControl.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.Admin.GameStatusControl" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>



<dx:ASPxGridView ID="UserStatusGridView" ClientInstanceName="UserStatusGridView"
    DataSourceID="UserStatusDataSource" runat="server" KeyFieldName="UserId" AutoGenerateColumns="False" Width="100%">
    <SettingsPager AlwaysShowPager="True">
        <AllButton Visible="True"></AllButton>
    </SettingsPager>
    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
    <Columns>
        <dx:GridViewDataTextColumn FieldName="UserId" ReadOnly="True" Visible="False" VisibleIndex="1">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="UserFirstName" Caption="First Name" VisibleIndex="2">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="UserLastName" Caption="LastName" VisibleIndex="3">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="ClientName" Caption="Client" VisibleIndex="4">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="GameTemplateNameForDashboard" Caption="Game" VisibleIndex="5">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="GameCompletedCount" Caption="Games Completed" VisibleIndex="7">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="GameIncompleteCount" Caption="Games Incomplete" VisibleIndex="8">
        </dx:GridViewDataTextColumn>  
        <dx:GridViewDataTextColumn FieldName="CompletionDate" Caption="Date of Last Completion" VisibleIndex="8">
        </dx:GridViewDataTextColumn>  
        <dx:GridViewDataProgressBarColumn FieldName="PercentageComplete" Caption="Status" VisibleIndex="6" Width="200" >
            <DataItemTemplate>
                <dx:ASPxProgressBar ID="PercentageCompleteProgressBar" runat="server" CustomDisplayFormat="" OnInit="PercentageCompleteProgressBar_OnInit" Value='<%# Eval("PercentageComplete") %>' Width="100%">
                </dx:ASPxProgressBar>
            </DataItemTemplate>
        </dx:GridViewDataProgressBarColumn>      
    </Columns>
</dx:ASPxGridView>

<asp:LinqDataSource ID="UserStatusDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="UserStatusDataSource_OnSelecting"></asp:LinqDataSource>
