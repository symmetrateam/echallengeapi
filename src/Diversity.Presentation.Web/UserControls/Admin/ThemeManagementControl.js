﻿function ThemeGridView_CustomButtonClick(s, e) {

    if (e.buttonID != 'DeleteThemeButton') {
        return;
    }

    if (confirm('Deleting this Theme will delete the theme and all associated mappings of themes to games. Are you sure you want to delete this theme?')) {
        s.DeleteRow(e.visibleIndex);
    }
}


function ClientUserGroupDetailGridView_CustomButtonClick(s, e) {

    if (e.buttonID != 'DeleteClientUserGroupButton') {
        return;
    }

    if (confirm('Are you sure you want to delete this client usergroup from the theme?')) {
        s.DeleteRow(e.visibleIndex);
    }
}

var lastClient = null;
function OnClientSelectionChanged(clientComboBox) {
    if (ClientUserGroupComboBox.InCallback()) {
        lastClient = clientComboBox.GetValue().toString();
    } else {
        ClientUserGroupComboBox.PerformCallback(clientComboBox.GetValue().toString());
    }
}

function OnClientUserGroupComboxEndCallback(s, e) {
    if (lastClient) {
        ClientUserGroupComboBox.PerformCallback(lastClient);
        lastClient = null;
    }
}

function ClientUserGroupDetailGridViewAddNewRow(s, e, gv) {
    gv.AddNewRow();
}