﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class GameListControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GameDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();


            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = from g in dataContext.Games
                           from p in g.Product.Product_Languages
                               .Where(l => l.LanguageID == language.LanguageID)
                           let showEdit = (from product in dataContext.Products
                                           from productLang in
                                               product.Product_Languages.Where(l => l.LanguageID == language.LanguageID)
                                           where g.ProductID == product.ProductID
                                           select productLang).Count() > 0
                           let gameInviteCount = (from i in dataContext.GameInvites
                                                  where i.GameID == g.GameID && !i.IsADeletedRow && !i.IsAnAuditRow
                                                  select i).Count()
                           let gameIncompleteCount = (from pg in dataContext.Games
                                                      where
                                                          pg.GameID_Template == g.GameID && !pg.IsADeletedRow &&
                                                          !pg.IsAnAuditRow && pg.GameCompletionDate == null
                                                      select pg
                                                     ).Count()
                           let gamesNotPlayed = (from i in dataContext.GameInvites
                                                 where
                                                     i.GameID == g.GameID && !i.IsADeletedRow && !i.IsAnAuditRow &&
                                                     dataContext.Games.Count(
                                                         pg =>
                                                         pg.UserID_CreatedBy == i.UserID && pg.GameID_Template.HasValue &&
                                                         pg.GameID_Template.Value == g.GameID) == 0
                                                 select i
                                                ).Count()
                           where g.IsAGameTemplate
                           orderby g.GameTemplateNameForDashboard
                           select new
                                      {
                                          g.GameID,
                                          g.GameTemplateNameForDashboard,
                                          p.ProductName,
                                          g.IsOnlineGame,
                                          g.GameNowLockedDown,
                                          g.GameThemeData,
                                          g.HasBeenDownloaded,
                                          g.NumberOfTimesRun,
                                          ShowEdit = showEdit,
                                          GameProgress =
                               gameInviteCount > 0
                                   ? ((double) gameInviteCount - (double) gameIncompleteCount - (double) gamesNotPlayed)/
                                     (double)gameInviteCount * 100 < 0 ? 0 : ((double)gameInviteCount - (double)gameIncompleteCount - (double)gamesNotPlayed) /
                                     (double)gameInviteCount * 100
                                   : 0
                                      };

            }
            else
            {
                e.Result = from g in dataContext.Games
                           from p in g.Product.Product_Languages
                               .Where(l => l.LanguageID == language.LanguageID)
                           from pd in p.Product.Product2Distributors
                           let distributors = dataContext.User2Distributors
                               .Where(d => d.UserID == userId)
                               .Select(d => d.DistributorID)
                           let showEdit = (from product in dataContext.Products
                                           from productLang in
                                               product.Product_Languages.Where(l => l.LanguageID == language.LanguageID)
                                           where g.ProductID == product.ProductID
                                           select productLang).Count() > 0
                           let gameInviteCount = (from i in dataContext.GameInvites
                                                  where i.GameID == g.GameID && !i.IsADeletedRow && !i.IsAnAuditRow
                                                  select i).Count()
                           let gameIncompleteCount = (from pg in dataContext.Games
                                                      where
                                                          pg.GameID_Template == g.GameID && !pg.IsADeletedRow &&
                                                          !pg.IsAnAuditRow && pg.GameCompletionDate == null
                                                      select pg
                                                     ).Count()
                           let gamesNotPlayed = (from i in dataContext.GameInvites
                                                 where
                                                     i.GameID == g.GameID && !i.IsADeletedRow && !i.IsAnAuditRow &&
                                                     dataContext.Games.Count(
                                                         pg =>
                                                         pg.UserID_CreatedBy == i.UserID && pg.GameID_Template.HasValue &&
                                                         pg.GameID_Template.Value == g.GameID) == 0
                                                 select i
                                                ).Count()
                           where g.IsAGameTemplate && distributors.Contains(pd.DistributorID)
                           orderby g.GameTemplateNameForDashboard
                           select new
                                      {
                                          g.GameID,
                                          g.GameTemplateNameForDashboard,
                                          p.ProductName,
                                          g.IsOnlineGame,
                                          g.GameNowLockedDown,
                                          g.GameThemeData,
                                          g.HasBeenDownloaded,
                                          g.NumberOfTimesRun,
                                          ShowEdit = showEdit,
                                          GameProgress =
                               gameInviteCount > 0
                                   ? ((double) gameInviteCount - (double) gameIncompleteCount - (double) gamesNotPlayed)/
                                     (double) gameInviteCount*100
                                   : 0
                                      };
            }

        }

        protected void DeleteGameCallback_OnCallback(object source, CallbackEventArgs e)
        {
            var gameIdString = e.Parameter;
            var gameId = Convert.ToInt32(gameIdString);

            using (var dataContext = new EChallengeDataContext())
            {
                var gameExists =
                    dataContext.Games
                        .Any(g => g.GameID == gameId);
                if (gameExists)
                {
                    var userId = (Page as BasePage).CurrentContext.UserIdentifier;
                    using (var transaction = dataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            dataContext.upDeleteGame(gameId, userId);
                            dataContext.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }

            }

        }

        protected void EditGameCallback_OnCallback(object source, CallbackEventArgs e)
        {
            var gameIdString = e.Parameter;
            var gameId = Convert.ToInt32(gameIdString);
            var url = string.Format(@"~/Admin/Game.aspx?id={0}", gameId);
            ASPxWebControl.RedirectOnCallback(url);
        }

        protected void GameStatusCallback_OnCallback(object source, CallbackEventArgs e)
        {
            var gameIdString = e.Parameter;
            var gameId = Convert.ToInt32(gameIdString);
            var url = string.Format(@"~/Admin/GameStatus.aspx?id={0}", gameId);
            ASPxWebControl.RedirectOnCallback(url);
        }

        protected void GameProgressProgressBar_OnInit(object sender, EventArgs e)
        {
            var progressBar = sender as ASPxProgressBar;

            if (progressBar == null)
            {
                return;
            }

            progressBar.ShowPosition = true;
            progressBar.ForeColor = Color.White;
        }
    }
}