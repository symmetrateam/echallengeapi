﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Diversity.Common;
using Diversity.Common.Enumerations;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class CardManagementControl
    {
        QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            var cardType = CurrentCardType;
            QuestionCard_Language cardLanguage = null;

            switch (cardType)
            {
                case CardType.MultipleChoiceQuestionCard:
                    cardLanguage = QuestionMultipleChoiceCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleChoiceImageQuestionCard:
                    cardLanguage = MultipleChoiceImageQuestionControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleCheckBoxQuestionCard:
                    cardLanguage = QuestionMultipleCheckBoxCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.LeadingQuestionCard:
                case CardType.TransitionUpCard:
                case CardType.TransitionDownCard:
                case CardType.IntroSurveyCard:
                case CardType.TextAreaSurveyCard:
                case CardType.TextAreaPriorityFeedbackCard:
                case CardType.TextAreaPersonalActionPlanCard:
                case CardType.GameCompletedCard:
                    cardLanguage = TextAreaIputControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.TextBoxPersonalActionPlanCard:
                case CardType.TextBoxSurveyCard:
                    cardLanguage = TextBoxInputControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleCheckBoxSurveyCard:
                    cardLanguage = SurveyCardMultipleCheckBoxControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleChoiceSurveyCard:
                    cardLanguage = SurveryCardMultipleChoiceControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleChoiceAuditStatementCard:
                    cardLanguage = AuditStatementCardMultipleChoiceControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleChoiceAuditStatementImageCard:
                    cardLanguage = AuditStatementImageCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleCheckBoxPriorityFeedbackCard:
                    cardLanguage = PriorityFeedbackMultipleCheckBoxCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleChoicePriorityFeedbackCard:
                    cardLanguage = PriorityFeedbackMultipleChoiceCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleCheckboxPersonalActionPlanCard:
                    cardLanguage = MultipleCheckBoxPersonalActionPlanCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MultipleChoicePersonalActionPlanCard:
                    cardLanguage = MultipleChoicePersonalActionPlanCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.CompanyPolicyCard:
                case CardType.LcdCategoryCard:
                case CardType.GameIntroCard:
                    cardLanguage = ImageAndTextCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.ThemeIntroCard:
                    cardLanguage = StatementListCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.PriorityIssueVotingCard:
                    cardLanguage = PriorityIssueVotingCardControl.GetQuestionCardLanguageDetail();
                    break;

                case CardType.MatchingListQuestionCard:
                    cardLanguage = MatchingListCardControl.GetQuestionCardLanguageDetail();
                    break;
                case CardType.MultiMediaCard:
                    cardLanguage = TextAreaLocationCardControl.GetQuestionCardLanguageDetail();
                    break;
                case CardType.MultipleCheckBoxImageQuestionCard:
                    cardLanguage = MultipleCheckBoxImageCardControl.GetQuestionCardLanguageDetail();
                    break;
                case CardType.BranchingCard:
                    cardLanguage = BranchingCardControl.GetQuestionCardLanguageDetail();
                    break;
            }

            return cardLanguage;
        }

        IEnumerable<QuestionCard_GeneralAnswer> GetQuestionCardGeneralAnswers()
        {
            var cardType = CurrentCardType;
            IEnumerable<QuestionCard_GeneralAnswer> answers = null;

            switch (cardType)
            {
                case CardType.MultipleCheckBoxQuestionCard:
                    answers = QuestionMultipleCheckBoxCardControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleChoiceQuestionCard:
                    answers = QuestionMultipleChoiceCardControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleChoiceImageQuestionCard:
                    answers = MultipleChoiceImageQuestionControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleCheckBoxSurveyCard:
                    answers = SurveyCardMultipleCheckBoxControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleChoiceSurveyCard:
                    answers = SurveryCardMultipleChoiceControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleChoiceAuditStatementCard:
                    answers = AuditStatementCardMultipleChoiceControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleChoiceAuditStatementImageCard:
                    answers = AuditStatementImageCardControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleCheckBoxPriorityFeedbackCard:
                    answers = PriorityFeedbackMultipleCheckBoxCardControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleChoicePriorityFeedbackCard:
                    answers = PriorityFeedbackMultipleChoiceCardControl.GetGeneralAnswers();
                    break;

                case CardType.PriorityIssueVotingCard:
                    answers = PriorityIssueVotingCardControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleCheckboxPersonalActionPlanCard:
                    answers = MultipleCheckBoxPersonalActionPlanCardControl.GetGeneralAnswers();
                    break;

                case CardType.MultipleChoicePersonalActionPlanCard:
                    answers = MultipleChoicePersonalActionPlanCardControl.GetGeneralAnswers();
                    break;
                case CardType.MatchingListQuestionCard:
                    answers = MatchingListCardControl.GetGeneralAnswers();
                    break;
                case CardType.MultipleCheckBoxImageQuestionCard:
                    answers = MultipleCheckBoxImageCardControl.GetGeneralAnswers();
                    break;

            }
            return answers;
        }

        IEnumerable<QuestionCardStatement_Language> GetQuestionCardStatementLanguages()
        {
            var cardType = CurrentCardType;
            IEnumerable<QuestionCardStatement_Language> statements = null;

            switch (cardType)
            {
                case CardType.ThemeIntroCard:
                    statements = StatementListCardControl.GetCardStatements();
                    break;
            }

            return statements;
        }

        void UpdateBranchingCard(BranchingCard card, DateTime currentDateTime, Guid userId, int languageId, out List<BranchingCard_Button> itemsToRemove)
        {
            BranchingCardControl.PopulateBranchingCardDetails(card);
            itemsToRemove = new List<BranchingCard_Button>();
            var branchingCardButtons = BranchingCardControl.GetBranchingCardButtons();

            if (branchingCardButtons.Any())
            {
                foreach (var branchingCardButton in branchingCardButtons)
                {
                    BranchingCard_Button cardButton;
                    var button =
                        card.BranchingCard_Buttons.FirstOrDefault(
                            b => b.BranchingCard_ButtonID == branchingCardButton.BranchingCard_ButtonID);

                    if (button == null)
                    {
                        cardButton = new BranchingCard_Button();
                        TransferBranchingCardButtonValues(cardButton, branchingCardButton, currentDateTime, userId, true);

                        card.BranchingCard_Buttons.Add(cardButton);

                        foreach (var buttonLanguage in branchingCardButton.BranchingCard_ButtonLanguage)
                        {
                            var newButtonLanguage = new BranchingCard_ButtonLanguage();
                            TransferBranchingCardButtonLanguageValues(newButtonLanguage, buttonLanguage, currentDateTime,
                                userId, languageId, branchingCardButton.BranchingCard_ButtonID, true);

                            cardButton.BranchingCard_ButtonLanguage.Add(newButtonLanguage);
                        }
                    }
                    else
                    {
                        cardButton = button;
                        TransferBranchingCardButtonValues(button, branchingCardButton, currentDateTime, userId, false);
                    }

                    foreach (var buttonLanguage in branchingCardButton.BranchingCard_ButtonLanguage.Where(b => b.LanguageID == languageId))
                    {
                        if (button == null)
                        {
                            var cardButtonLanguage = new BranchingCard_ButtonLanguage();

                            TransferBranchingCardButtonLanguageValues(cardButtonLanguage, buttonLanguage,
                                currentDateTime, userId, languageId, cardButton.BranchingCard_ButtonID, true);

                            cardButton.BranchingCard_ButtonLanguage.Add(cardButtonLanguage);
                        }
                        else
                        {
                            var cardButtonLanguage =
                                cardButton.BranchingCard_ButtonLanguage.FirstOrDefault(b => b.LanguageID == languageId);

                            if (cardButtonLanguage == null)
                            {
                                cardButtonLanguage = new BranchingCard_ButtonLanguage();

                                TransferBranchingCardButtonLanguageValues(cardButtonLanguage, buttonLanguage,
                                    currentDateTime, userId, languageId, cardButton.BranchingCard_ButtonID, true);

                                cardButton.BranchingCard_ButtonLanguage.Add(cardButtonLanguage);
                            }
                            else
                            {
                                TransferBranchingCardButtonLanguageValues(cardButtonLanguage, buttonLanguage,
                                    currentDateTime, userId, languageId, cardButton.BranchingCard_ButtonID, false);
                            }
                        }
                    }
                }

                var buttonIds = branchingCardButtons.Select(c => c.BranchingCard_ButtonID).ToList();
                var buttonsToRemove = card.BranchingCard_Buttons.Where(c => !buttonIds.Contains(c.BranchingCard_ButtonID)).ToList();

                itemsToRemove.AddRange(buttonsToRemove);
            }
            else
            {
                itemsToRemove.AddRange(card.BranchingCard_Buttons);
            }

            BranchingCardControl.MoveMultimediaFilesToLiveFolder();
        }

        void UpdateQuestionCard(QuestionCard card, DateTime currentDateTime, Guid userId, int languageId)
        {
            SetQuestionCardValues(card, currentDateTime, userId);

            QuestionCard_Language languageDetail = GetQuestionCardLanguageDetail();

            if (languageDetail != null)
            {
                var cardLanguage = card.QuestionCard_Languages.Where(c => c.LanguageID == languageId).FirstOrDefault();

                if (cardLanguage == null)
                {
                    cardLanguage = new QuestionCard_Language();
                    cardLanguage.CreatedOnDateTime = currentDateTime;
                    cardLanguage.UserID_CreatedBy = userId;

                    TransferQuestionCardLanguageValues(cardLanguage, currentDateTime, userId, languageId, languageDetail);
                    card.QuestionCard_Languages.Add(cardLanguage);
                }
                else
                {
                    TransferQuestionCardLanguageValues(cardLanguage, currentDateTime, userId, languageId, languageDetail);
                }
            }

            IEnumerable<QuestionCardStatement_Language> statements = GetQuestionCardStatementLanguages();

            if (statements != null)
            {
                foreach (var statement in statements)
                {
                    var statementLanguage =
                        card.QuestionCardStatement_Languages.FirstOrDefault(
                            c => c.QuestionCardStatement_LanguageID == statement.QuestionCardStatement_LanguageID);

                    if (statementLanguage == null)
                    {
                        statementLanguage = new QuestionCardStatement_Language();
                        statementLanguage.CreatedOnDateTime = currentDateTime;
                        statementLanguage.UserID_CreatedBy = userId;

                        TransferQuestionCardStatementLanguageValues(statementLanguage, statement, currentDateTime,
                                                                    userId, card.QuestionCardID,
                                                                    languageId, true);

                        card.QuestionCardStatement_Languages.Add(statementLanguage);
                    }
                    else
                    {
                        TransferQuestionCardStatementLanguageValues(statementLanguage, statement, currentDateTime,
                                                                    userId, card.QuestionCardID,
                                                                    languageId, false);
                    }
                }
            }

            IEnumerable<QuestionCard_GeneralAnswer> answers = GetQuestionCardGeneralAnswers();

            if (answers != null)
            {
                foreach (var answer in answers)
                {
                    var questionCardGeneralAnswer =
                        card.QuestionCard_GeneralAnswers.FirstOrDefault(c => c.QuestionCard_GeneralAnswerID == answer.QuestionCard_GeneralAnswerID && c.QuestionCard_GeneralAnswerID > 0);

                    if (questionCardGeneralAnswer == null)
                    {
                        questionCardGeneralAnswer = new QuestionCard_GeneralAnswer();
                        questionCardGeneralAnswer.CreatedOnDateTime = currentDateTime;
                        questionCardGeneralAnswer.UserID_CreatedBy = userId;

                        TransferQuestionCardGeneralAnswerValues(questionCardGeneralAnswer, answer, currentDateTime,
                                                                userId,
                                                                card.QuestionCardID, true);
                        card.QuestionCard_GeneralAnswers.Add(questionCardGeneralAnswer);
                    }
                    else
                    {
                        TransferQuestionCardGeneralAnswerValues(questionCardGeneralAnswer, answer, currentDateTime,
                                                                userId,
                                                                card.QuestionCardID, false);
                    }

                    var answerLanguage =
                        answer.QuestionCard_GeneralAnswer_Languages.FirstOrDefault(c => c.LanguageID == languageId);

                    var questionCardGeneralAnswerLanguage =
                        questionCardGeneralAnswer.QuestionCard_GeneralAnswer_Languages.FirstOrDefault(c => c.LanguageID == languageId);

                    if (questionCardGeneralAnswerLanguage == null)
                    {
                        questionCardGeneralAnswerLanguage = new QuestionCard_GeneralAnswer_Language();
                        TransferQuestionCardGeneralAnswerLanguageValues(questionCardGeneralAnswerLanguage,
                                                                        answerLanguage, currentDateTime, userId,
                                                                        languageId,
                                                                        questionCardGeneralAnswer.
                                                                            QuestionCard_GeneralAnswerID, true);
                        questionCardGeneralAnswer.QuestionCard_GeneralAnswer_Languages.Add(
                            questionCardGeneralAnswerLanguage);
                    }
                    else
                    {
                        if (answerLanguage != null)
                            TransferQuestionCardGeneralAnswerLanguageValues(questionCardGeneralAnswerLanguage,
                                                                            answerLanguage, currentDateTime, userId,
                                                                            languageId,
                                                                            questionCardGeneralAnswer.
                                                                                QuestionCard_GeneralAnswerID, false);
                    }
                }
            }
        }

        QuestionCard CreateNewQuestionCard(DateTime createdOn, Guid createdBy, int languageID)
        {
            var card = new QuestionCard();

            SetQuestionCardValues(card, createdOn, createdBy);

            QuestionCard_Language questionLanguage = GetQuestionCardLanguageDetail();

            if (questionLanguage != null)
            {
                var cardLanguage = new QuestionCard_Language();
                TransferQuestionCardLanguageValues(cardLanguage, createdOn, createdBy, languageID, questionLanguage);
                card.QuestionCard_Languages.Add(cardLanguage);
            }

            IEnumerable<QuestionCardStatement_Language> statements = GetQuestionCardStatementLanguages();

            if (statements != null)
            {
                foreach (var statement in statements)
                {
                    var statementLanguage = new QuestionCardStatement_Language();
                    
                    TransferQuestionCardStatementLanguageValues(statementLanguage, statement, createdOn,
                                                                createdBy, card.QuestionCardID,
                                                                languageID, true);

                    card.QuestionCardStatement_Languages.Add(statementLanguage);
                }
            }

            IEnumerable<QuestionCard_GeneralAnswer> answers = GetQuestionCardGeneralAnswers();

            if (answers != null)
            {
                foreach (var answer in answers)
                {
                    var generalAnswer = new QuestionCard_GeneralAnswer();

                    TransferQuestionCardGeneralAnswerValues(generalAnswer, answer, createdOn, createdBy,
                                                            card.QuestionCardID, true);

                    foreach (var language in answer.QuestionCard_GeneralAnswer_Languages)
                    {
                        var generalAnswerLanguage = new QuestionCard_GeneralAnswer_Language();
                        TransferQuestionCardGeneralAnswerLanguageValues(generalAnswerLanguage, language, createdOn,
                                                                        createdBy, languageID,
                                                                        generalAnswer.QuestionCard_GeneralAnswerID, true);
                        generalAnswer.QuestionCard_GeneralAnswer_Languages.Add(generalAnswerLanguage);
                    }

                    card.QuestionCard_GeneralAnswers.Add(generalAnswer);
                }
            }
            return card;
        }

        void TransferBranchingCardButtonValues(BranchingCard_Button transferTo, BranchingCard_Button transferFrom, 
            DateTime currentDateTime, Guid userId, bool isNew)
        {
            if (isNew)
            {
                transferTo.UserID_CreatedBy = userId;                
            }

            transferTo.UserID_LastModifiedBy = userId;
            transferTo.LastModifiedOnDateTime = currentDateTime;
            transferTo.IsADeletedRow = false;
            transferTo.IsAnAuditRow = false;
            transferTo.Position = transferFrom.Position;
            transferTo.CardGroupID_Result = transferFrom.CardGroupID_Result;
            transferTo.IsMultiClick = transferFrom.IsMultiClick;
            transferTo.MustReturnToBranchingCard = transferFrom.MustReturnToBranchingCard;
        }

        void TransferBranchingCardButtonLanguageValues(BranchingCard_ButtonLanguage transferTo, BranchingCard_ButtonLanguage transferFrom,
            DateTime currentDateTime, Guid userId, int languageId, int branchingCardButtonId, bool isNew)
        {
            if (isNew)
            {
                transferTo.UserID_CreatedBy = userId;
            }

            transferTo.UserID_LastModifiedBy = userId;
            transferTo.LastModifiedOnDateTime = currentDateTime;
            transferTo.IsADeletedRow = false;
            transferTo.IsAnAuditRow = false;
            transferTo.LanguageID = languageId;
            transferTo.BranchingCard_ButtonID = branchingCardButtonId;
            transferTo.ButtonText = transferFrom.ButtonText;
            transferTo.ButtonImageFriendlyName = transferFrom.ButtonImageFriendlyName;
            transferTo.ButtonImageMultimediaURL = transferFrom.ButtonImageMultimediaURL;
        }

        void TransferQuestionCardGeneralAnswerLanguageValues(QuestionCard_GeneralAnswer_Language transferTo, QuestionCard_GeneralAnswer_Language transferFrom,
            DateTime currentDateTime, Guid userId, int languageId, int answerId, bool isNew)
        {
            if (isNew)
            {
                transferTo.UserID_CreatedBy = userId;
                transferTo.CreatedOnDateTime = currentDateTime;
            }

            transferTo.LastModifiedOnDateTime = currentDateTime;
            transferTo.UserID_LastModifiedBy = userId;
            transferTo.QuestionCard_GeneralAnswerID = answerId;
            transferTo.LanguageID = languageId;
            transferTo.PossibleAnswer_LongVersion = transferFrom.PossibleAnswer_LongVersion;
            transferTo.PossibleAnswer_ShortVersion = transferFrom.PossibleAnswer_ShortVersion;
            transferTo.PossibleAnswerMultimediaURL = transferFrom.PossibleAnswerMultimediaURL;
            transferTo.PossibleAnswerMultimediaURLFriendlyName = transferFrom.PossibleAnswerMultimediaURLFriendlyName;
            transferTo.RHStatement_ShortVersion = transferFrom.RHStatement_ShortVersion;
            transferTo.RHStatement_LongVersion = transferFrom.RHStatement_LongVersion;
            transferTo.LHStatement_ShortVersion = transferFrom.LHStatement_ShortVersion;
            transferTo.LHStatement_LongVersion = transferFrom.LHStatement_LongVersion;
        }

        void TransferQuestionCardGeneralAnswerValues(QuestionCard_GeneralAnswer answer, QuestionCard_GeneralAnswer transferFrom, DateTime currentDateTime, Guid userId,
            int questionId, bool isNew)
        {
            if (isNew)
            {
                answer.UserID_CreatedBy = userId;
                answer.CreatedOnDateTime = currentDateTime;
            }

            answer.UserID_LastModifiedBy = userId;
            answer.LastModifiedOnDateTime = currentDateTime;
            answer.QuestionCardID = questionId;
            answer.SequenceNo = transferFrom.SequenceNo;
            answer.IsCorrectAnswer = transferFrom.IsCorrectAnswer;
        }

        void TransferQuestionCardStatementLanguageValues(QuestionCardStatement_Language statementLanguage, QuestionCardStatement_Language transferFrom, DateTime currentDateTime,
            Guid userId, int questionId, int languageId, bool isNew)
        {
            if (isNew)
            {
                statementLanguage.UserID_CreatedBy = userId;
                statementLanguage.CreatedOnDateTime = currentDateTime;
            }

            statementLanguage.LastModifiedOnDateTime = currentDateTime;
            statementLanguage.UserID_LastModifiedBy = userId;
            statementLanguage.QuestionCardID = questionId;
            statementLanguage.LanguageID = languageId;
            statementLanguage.SequenceNo = transferFrom.SequenceNo;
            statementLanguage.Statement_LongVersion = transferFrom.Statement_LongVersion;
            statementLanguage.Statement_ShortVersion = transferFrom.Statement_ShortVersion;
        }

        void TransferQuestionCardLanguageValues(QuestionCard_Language transferToCard,
            DateTime currentDateTime, Guid userId, int languageId,
            QuestionCard_Language transferFromCard)
        {
            if (transferFromCard == null || transferToCard == null)
            {
                return;
            }

            if (!CurrentCardId.HasValue)
            {
                transferToCard.UserID_CreatedBy = userId;
                transferToCard.CreatedOnDateTime = currentDateTime;
            }

            transferToCard.Question_Statement_LongVersion = transferFromCard.Question_Statement_LongVersion;
            transferToCard.Question_Statement_ShortVersion = transferFromCard.Question_Statement_ShortVersion;
            transferToCard.AnswerExplanation_LongVersion = transferFromCard.AnswerExplanation_LongVersion;
            transferToCard.AnswerExplanation_ShortVersion = transferFromCard.AnswerExplanation_ShortVersion;
            transferToCard.UserID_LastModifiedBy = userId;
            transferToCard.QuestionCardID = transferToCard.QuestionCardID;
            transferToCard.LastModifiedOnDateTime = currentDateTime;
            transferToCard.LanguageID = languageId;
            transferToCard.StatementHeader = transferFromCard.StatementHeader;
            transferToCard.StatementLocation = transferFromCard.StatementLocation;

        }

        void SetBranchingCardSpecificDetails(Card card)
        {
            if (CurrentCardType.HasValue && CurrentCardType.Value == CardType.BranchingCard)
            {
                var branchingCard = (BranchingCard)card;
                BranchingCardControl.PopulateBranchingCardDetails(branchingCard);
            }
        }

        void SetSpecificQuestionCardValues(QuestionCard card)
        {
            switch (CurrentCardType)
            {
                case CardType.MultipleChoiceQuestionCard:
                    card.OfferOTHERTextOption = QuestionMultipleChoiceCardControl.OfferOtherTextOption;
                    card.TextOptionInputIsTextArea = QuestionMultipleChoiceCardControl.TextOptionInputIsTextArea;
                    break;

                case CardType.MultipleChoiceImageQuestionCard:
                    card.OfferOTHERTextOption = MultipleChoiceImageQuestionControl.OfferOtherTextOption;
                    card.TextOptionInputIsTextArea = MultipleChoiceImageQuestionControl.TextOptionInputIsTextArea;
                    MultipleChoiceImageQuestionControl.MoveMultimediaFilesToLiveFolder();
                    break;

                case CardType.MultipleCheckBoxQuestionCard:
                    card.QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                        QuestionMultipleCheckBoxCardControl.QuestionIsOnlyCorrectIfAllAnswersAreTrue;
                    break;

                case CardType.MultipleCheckBoxSurveyCard:
                    card.OfferOptOutChoice = SurveyCardMultipleCheckBoxControl.OfferOptOut;
                    card.OfferOTHERTextOption = SurveyCardMultipleCheckBoxControl.OfferOtherOption;
                    card.TextOptionInputIsTextArea = SurveyCardMultipleCheckBoxControl.TextOptionInputIsTextArea;
                    break;

                case CardType.MultipleChoiceSurveyCard:
                    card.OfferOptOutChoice = SurveryCardMultipleChoiceControl.OfferOptOut;
                    card.OfferOTHERTextOption = SurveryCardMultipleChoiceControl.OfferOtherTextOption;
                    card.TextOptionInputIsTextArea = SurveryCardMultipleChoiceControl.TextOptionInputIsTextArea;
                    break;

                case CardType.MultipleChoiceAuditStatementCard:
                    card.OfferOptOutChoice = AuditStatementCardMultipleChoiceControl.OfferOptOut;
                    card.MustShowCompanyResults = AuditStatementCardMultipleChoiceControl.MustShowCompanyResults;
                    card.MinDataPointsForCompanyResult = card.MustShowCompanyResults
                        ? AuditStatementCardMultipleChoiceControl.MinimumDataPointsForCompanyResult
                        : 0;
                    break;

                case CardType.MultipleChoiceAuditStatementImageCard:
                    card.OfferOptOutChoice = AuditStatementImageCardControl.OfferOptOut;
                    card.MustShowCompanyResults = AuditStatementImageCardControl.MustShowCompanyResults;
                    card.MinDataPointsForCompanyResult = card.MustShowCompanyResults
                        ? AuditStatementImageCardControl.MinimumDataPointsForCompanyResult
                        : 0;
                    AuditStatementImageCardControl.MoveMultimediaFilesToLiveFolder();
                    break;

                case CardType.MultipleChoicePriorityFeedbackCard:
                    card.OfferOptOutChoice = PriorityFeedbackMultipleChoiceCardControl.OfferOptOut;
                    card.OfferOTHERTextOption = PriorityFeedbackMultipleChoiceCardControl.OfferOtherTextOption;
                    card.TextOptionInputIsTextArea = PriorityFeedbackMultipleChoiceCardControl.TextOptionInputIsTextArea;
                    break;

                case CardType.MultipleCheckBoxPriorityFeedbackCard:
                    card.OfferOptOutChoice = PriorityFeedbackMultipleCheckBoxCardControl.OfferOptOut;
                    card.OfferOTHERTextOption = PriorityFeedbackMultipleCheckBoxCardControl.OfferOtherOption;
                    card.TextOptionInputIsTextArea =
                        PriorityFeedbackMultipleCheckBoxCardControl.TextOptionInputIsTextArea;
                    break;

                case CardType.CompanyPolicyCard:
                case CardType.LcdCategoryCard:
                case CardType.GameIntroCard:
                    card.AssociatedImageUrl = ImageAndTextCardControl.AssociatedImageUrl;
                    card.AssociatedImageUrlFriendlyName = ImageAndTextCardControl.AssociatedImageUrlFriendlyName;
                    ImageAndTextCardControl.MoveImageToLiveFolder();
                    break;

                case CardType.PriorityIssueVotingCard:
                    card.UsePrimaryTopics = PriorityIssueVotingCardControl.UsePrimaryTopics;
                    card.TopNSelected = PriorityIssueVotingCardControl.TopNSelected;
                    break;

                case CardType.MultipleCheckboxPersonalActionPlanCard:
                    card.OfferOptOutChoice = MultipleCheckBoxPersonalActionPlanCardControl.OfferOptOut;
                    card.OfferOTHERTextOption = MultipleCheckBoxPersonalActionPlanCardControl.OfferOtherOption;
                    card.TextOptionInputIsTextArea =
                        MultipleCheckBoxPersonalActionPlanCardControl.TextOptionInputIsTextArea;
                    break;

                case CardType.MultipleChoicePersonalActionPlanCard:
                    card.OfferOptOutChoice = MultipleChoicePersonalActionPlanCardControl.OfferOptOut;
                    card.OfferOTHERTextOption = MultipleChoicePersonalActionPlanCardControl.OfferOtherTextOption;
                    card.TextOptionInputIsTextArea =
                        MultipleChoicePersonalActionPlanCardControl.TextOptionInputIsTextArea;
                    break;

                case CardType.TextAreaPersonalActionPlanCard:
                case CardType.TextAreaSurveyCard:
                case CardType.TextAreaPriorityFeedbackCard:
                    card.TextOptionInputIsTextArea = true;
                    break;

                case CardType.ThemeIntroCard:
                    card.AssociatedImageUrl = StatementListCardControl.AssociatedImageUrl;
                    card.AssociatedImageUrlFriendlyName = StatementListCardControl.AssociatedImageUrlFriendlyName;
                    StatementListCardControl.MoveImageToLiveFolder();
                    break;
                case CardType.MatchingListQuestionCard:
                    card.MustShowCompanyResults = MatchingListCardControl.MustShowCompanyResults;
                    card.MinDataPointsForCompanyResult = card.MustShowCompanyResults
                        ? MatchingListCardControl.MinimumDataPointsForCompanyResult
                        : 0;
                    break;
                case CardType.MultipleCheckBoxImageQuestionCard:
                    card.QuestionIsOnlyCorrectIfAllAnswersAreTrue =
                        MultipleCheckBoxImageCardControl.QuestionIsOnlyCorrectIfAllAnswersAreTrue;
                    MultipleCheckBoxImageCardControl.MoveMultimediaFilesToLiveFolder();
                    break;
                case CardType.BranchingCard:
                    // THIS LOGIC IS DONE IN THE INSERT CARD METHOD
                    break;
            }
        }

        void SetQuestionCardValues(QuestionCard card, DateTime createdOn, Guid createdBy)
        {
            if (!CurrentCardId.HasValue)
            {
                card.CreatedOnDateTime = createdOn;
                card.UserID_CreatedBy = createdBy;
            }

            card.LastModifiedOnDateTime = createdOn;
            card.UserID_LastModifiedBy = createdBy;

            card.QuestionMultimediaStartAutoElsePushPlayToStart =
                QuestionStartMultimediaAutoCheckBox.Checked;
            card.QuestionMultimediaShowPlaybackControls = QuestionShowPlaybackControlCheckBox.Checked;

            if (QuestionMultimediaTypeComboBox.SelectedItem != null)
            {
                var questionMultimediaType = QuestionMultimediaTypeComboBox.SelectedItem.Value.ToString();
                var oldQuestionImageVideoEtcType = card.QuestionMultimediaType_ImageVideoetc;
                card.QuestionMultimediaType_ImageVideoetc = questionMultimediaType;

                switch (questionMultimediaType)
                {
                    case "wistia":
                        var newUrl = QuestionMultimediaUrlTextBox.Text;
                        
                        if (!string.IsNullOrEmpty(card.QuestionMultimediaURL) && !string.IsNullOrWhiteSpace(card.QuestionMultimediaURL) 
                            && string.IsNullOrEmpty(newUrl) && string.IsNullOrWhiteSpace(newUrl))
                        {
                            RemoveQuestionMultimedia(card);
                        }
                        else
                        {
                            card.QuestionMultimediaURL = HttpUtility.UrlDecode(newUrl);    
                        }

                        break;

                    case "image":
                    case "document":
                        if (HiddenField.Contains("QuestionMultimediaDocumentHyperLinkNavigateUrl"))
                        {
                            var navigateUrl =
                                HiddenField.Get("QuestionMultimediaDocumentHyperLinkNavigateUrl") as string;
                            var text = HiddenField.Get("QuestionMultimediaDocumentHyperLinkText") as string;

                            if (string.IsNullOrEmpty(navigateUrl))
                            {
                                break;
                            }

                            var parts = navigateUrl.Split(new[] { "/" },
                                                          StringSplitOptions.
                                                              RemoveEmptyEntries);
                            if (parts.Count() > 0)
                            {
                                var filename = parts[parts.Length - 1];
                                if (string.Compare(filename, card.QuestionMultimediaURL, true) == 0)
                                {
                                    break;
                                }

                                var oldFilename = card.QuestionMultimediaURL;

                                card.QuestionMultimediaURL = filename;
                                card.QuestionMultimediaURLFriendlyName = text;
                                var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
                                var destinationPath = Path.Combine(
                                                              MapPath(ConfigurationHelper.LiveUploadLocation),
                                                              filename);

                                var oldFileDeletePath = string.IsNullOrEmpty(oldFilename)
                                                            ? string.Empty
                                                            : Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                                      oldFilename);

                                File.Move(tempPath, destinationPath);

                                if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                                {
                                    File.Delete(oldFileDeletePath);
                                }

                            }
                        }
                        else if (!string.IsNullOrEmpty(card.QuestionMultimediaURL) && !string.IsNullOrWhiteSpace(card.QuestionMultimediaURL))
                        {
                            RemoveQuestionMultimedia(card);
                        }
                        break;
                }
            }

            if (HiddenField.Contains("QuestionThumbnailHyperLinkNavigateUrl"))
            {
                var navigateUrl =
                                HiddenField.Get("QuestionThumbnailHyperLinkNavigateUrl") as string;
                var text = HiddenField.Get("QuestionThumbnailHyperLinkText") as string;

                if (!string.IsNullOrEmpty(navigateUrl))
                {
                    var parts = navigateUrl.Split(new[] { "/" },
                                              StringSplitOptions.
                                                  RemoveEmptyEntries);
                    if (parts.Any())
                    {
                        var filename = parts[parts.Length - 1];
                        if (string.Compare(filename, card.QuestionMultimediaThumbnailURL, true) != 0)
                        {
                            var oldFilename = card.QuestionMultimediaThumbnailURL;

                            card.QuestionMultimediaThumbnailURL = filename;
                            card.QuestionMultimediaThumbnailURLFriendlyName = text;
                            var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
                            var destinationPath = Path.Combine(
                                                          MapPath(ConfigurationHelper.LiveUploadLocation),
                                                          filename);

                            var oldFileDeletePath = string.IsNullOrEmpty(oldFilename)
                                                        ? string.Empty
                                                        : Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                                  oldFilename);

                            File.Move(tempPath, destinationPath);

                            if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                            {
                                File.Delete(oldFileDeletePath);
                            }   
                        }                        
                    }   
                }                
            }
            else if (card.QuestionMultimediaThumbnailURL != null && !string.IsNullOrEmpty(card.QuestionMultimediaThumbnailURL) 
                && !string.IsNullOrWhiteSpace(card.QuestionMultimediaThumbnailURL))
            {
                var oldFilename = card.QuestionMultimediaThumbnailURL;
                var oldFileDeletePath = string.IsNullOrEmpty(card.QuestionMultimediaThumbnailURL)
                                                        ? string.Empty
                                                        : Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                                  oldFilename);

                card.QuestionMultimediaThumbnailURL = null;
                card.QuestionMultimediaThumbnailURLFriendlyName = null;

                if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                {
                    File.Delete(oldFileDeletePath);
                }  
            }

            if (AnswerMultimediaTypeComboBox.SelectedItem != null)
            {
                var answerMultimediaType = AnswerMultimediaTypeComboBox.SelectedItem.Value.ToString();
                var oldAnswerImageVideoEtcType = card.AnswerExplanationMultimediaType_ImageVideoetc;
                card.AnswerExplanationMultimediaType_ImageVideoetc = answerMultimediaType;

                switch (answerMultimediaType)
                {
                    case "wistia":
                        card.AnswerExplanationMultimediaURL = AnswerMultimediaUrlTextBox.Text;
                        
                        if (!string.IsNullOrEmpty(card.AnswerExplanationMultimediaURL) && !string.IsNullOrWhiteSpace(card.AnswerExplanationMultimediaURL))
                        {
                            RemoveAnswerExplainMultimedia(card);
                        }

                        break;

                    case "image":
                    case "document":
                        if (HiddenField.Contains("AnswerMultimediaDocumentHyperLinkNavigateUrl"))
                        {
                            var navigateUrl = HiddenField.Get("AnswerMultimediaDocumentHyperLinkNavigateUrl") as string;
                            var text = HiddenField.Get("AnswerMultimediaDocumentHyperLinkText") as string;

                            if (string.IsNullOrEmpty(navigateUrl))
                            {
                                break;
                            }

                            var parts = navigateUrl.Split(new[] { "/" },
                                                          StringSplitOptions.
                                                              RemoveEmptyEntries);
                            if (parts.Count() > 0)
                            {
                                var filename = parts[parts.Length - 1];

                                if (string.Compare(filename, card.AnswerExplanationMultimediaURL, true) == 0)
                                {
                                    break;
                                }

                                var oldFilename = card.AnswerExplanationMultimediaURL;

                                card.AnswerExplanationMultimediaURL = filename;
                                card.AnswerExplanationMultimediaURLFriendlyName = text;

                                var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
                                var destinationPath = Path.Combine(
                                                              MapPath(ConfigurationHelper.LiveUploadLocation),
                                                              filename);

                                var oldFileDeletePath = string.IsNullOrEmpty(oldAnswerImageVideoEtcType)
                                                            ? string.Empty
                                                            : Path.Combine(
                                                                      MapPath(ConfigurationHelper.LiveUploadLocation),
                                                                      oldFilename);

                                File.Move(tempPath, destinationPath);

                                if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                                {
                                    File.Delete(oldFileDeletePath);
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(card.AnswerExplanationMultimediaURL) && !string.IsNullOrWhiteSpace(card.AnswerExplanationMultimediaURL))
                        {
                            RemoveAnswerExplainMultimedia(card);
                        }
                        break;
                }
            }

            if (HiddenField.Contains("AnswerThumbnailHyperLinkNavigateUrl"))
            {
                var navigateUrl =
                    HiddenField.Get("AnswerThumbnailHyperLinkNavigateUrl") as string;
                var text = HiddenField.Get("AnswerThumbnailHyperLinkText") as string;

                if (!string.IsNullOrEmpty(navigateUrl))
                {
                    var parts = navigateUrl.Split(new[] { "/" },
                                              StringSplitOptions.
                                                  RemoveEmptyEntries);
                    if (parts.Any())
                    {
                        var filename = parts[parts.Length - 1];
                        if (string.Compare(filename, card.AnswerExplanationMultimediaThumbnailURL, true) != 0)
                        {
                            var oldFilename = card.AnswerExplanationMultimediaThumbnailURL;

                            card.AnswerExplanationMultimediaThumbnailURL = filename;
                            card.AnswerExplanationMultimediaThumbnailURLFriendlyName = text;
                            var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
                            var destinationPath = Path.Combine(
                                                          MapPath(ConfigurationHelper.LiveUploadLocation),
                                                          filename);

                            var oldFileDeletePath = string.IsNullOrEmpty(oldFilename)
                                                        ? string.Empty
                                                        : Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                                  oldFilename);

                            File.Move(tempPath, destinationPath);

                            if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                            {
                                File.Delete(oldFileDeletePath);
                            }
                        }
                    }
                }
            }
            else if (card.AnswerExplanationMultimediaThumbnailURL != null && !string.IsNullOrEmpty(card.AnswerExplanationMultimediaThumbnailURL)
                && !string.IsNullOrWhiteSpace(card.AnswerExplanationMultimediaThumbnailURL))
            {
                var oldFilename = card.AnswerExplanationMultimediaThumbnailURL;
                var oldFileDeletePath = string.IsNullOrEmpty(card.AnswerExplanationMultimediaThumbnailURL)
                                                        ? string.Empty
                                                        : Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                                  oldFilename);

                card.AnswerExplanationMultimediaThumbnailURL = null;
                card.AnswerExplanationMultimediaThumbnailURLFriendlyName = null;

                if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                {
                    File.Delete(oldFileDeletePath);
                }
            }

            card.AnswerMultimediaStartAutoElsePushPlayToStart = AnswerStartMultimediaAutoCheckBox.Checked;
            card.AnswerMultimediaShowPlaybackControls = AnswerShowPlaynackControlCheckBox.Checked;


            card.DesktopCurrentLosingScore = Convert.ToInt32(CurrentPlayerLoseSpinEdit.Value);
            card.DesktopCurrentWinningScore = Convert.ToInt32(CurrentPlayerWinSpinEdit.Value);
            card.DesktopNonCurrentLosingScore = Convert.ToInt32(NonCurrentPlayerLoseSpinEdit.Value);
            card.DesktopNonCurrentWinningScore = Convert.ToInt32(NonCurrentPlayerWinSpinEdit.Value);
            card.OnlineLosingScore = Convert.ToInt32(OnlinePlayerLoseSpinEdit.Value);
            card.OnlineWinningScore = Convert.ToInt32(OnlinePlayerWinSpinEdit.Value);

            SetSpecificQuestionCardValues(card);
        }

        private void RemoveAnswerExplainMultimedia(QuestionCard card)
        {
            var oldFilename = card.AnswerExplanationMultimediaURL;
            var oldType = card.AnswerExplanationMultimediaType_ImageVideoetc;

            card.AnswerExplanationMultimediaURL = null;
            card.AnswerExplanationMultimediaURLFriendlyName = null;
            card.AnswerExplanationMultimediaType_ImageVideoetc = null;

            if (oldType == "wistia")
            {                
                return;
            }

            var oldFileDeletePath = string.IsNullOrEmpty(oldFilename)
                                        ? string.Empty
                                        : Path.Combine(
                                            MapPath(ConfigurationHelper.LiveUploadLocation),
                                            oldFilename);

            if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
            {
                File.Delete(oldFileDeletePath);
            }
        }

        private void RemoveQuestionMultimedia(QuestionCard card)
        {
            var oldFilename = card.QuestionMultimediaURL;
            var oldType = card.QuestionMultimediaType_ImageVideoetc;

            card.QuestionMultimediaURL = null;
            card.QuestionMultimediaURLFriendlyName = null;
            card.QuestionMultimediaType_ImageVideoetc = null;

            if (oldType == "wistia")
            {
                return;
            }

            var oldFileDeletePath = string.IsNullOrEmpty(oldFilename)
                                        ? string.Empty
                                        : Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                                       oldFilename);

            if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
            {
                File.Delete(oldFileDeletePath);
            }
        }

        protected void ValidationCallback_OnCallback(object source, CallbackEventArgs e)
        {
            List<string> errors;
            if (!CurrentCardType.HasValue)
            {
                return;
            }

            if(CurrentCardType.Value == CardType.PriorityIssueVotingCard)
            {
                if(PriorityIssueVotingCardControl.IsValid(out errors))
                {
                    e.Result = null;
                }
                else
                {
                    e.Result = string.Join(" ", errors);
                }
            }
            else if (CurrentCardType.Value == CardType.MultipleChoiceQuestionCard)
            {
                if (QuestionMultipleChoiceCardControl.IsValid(out errors))
                {
                    e.Result = null;
                }
                else
                {
                    e.Result = string.Join(" ", errors);
                }
            }
            else if (CurrentCardType.Value == CardType.MultipleChoiceImageQuestionCard)
            {
                if (MultipleChoiceImageQuestionControl.IsValid(out errors))
                {
                    e.Result = null;
                }
                else
                {
                    e.Result = string.Join(" ", errors);
                }
            }
            else if (CurrentCardType.Value == CardType.BranchingCard)
            {
                if (BranchingCardControl.IsValid(out errors))
                {
                    e.Result = null;
                }
                else
                {
                    e.Result = string.Join(" ", errors);
                }
            }

        }
    }
}