﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.Common;
using System.Net;
using Diversity.Domain.Model;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class ServiceCallControl : System.Web.UI.UserControl
    {
        #region Constants

        private const string AUTHENTICATION_TOKEN = "BDB561B9-135D-4C08-8F47-19928C82AF62";

        #endregion

        #region Classes

        [DataContract(Namespace = "")]
        public class ResponseResult<T>
        {
            private readonly List<Diversity.Domain.Model.ErrorMessage> _errors;

            public ResponseResult()
            {
                _errors = new List<Diversity.Domain.Model.ErrorMessage>();
            }

            /// <summary>
            /// Result of the contract/operation
            /// </summary>
            [DataMember]
            public T Result { get; set; }

            /// <summary>
            /// Error messages of associated contract/operation
            /// </summary>
            [DataMember]
            public List<Diversity.Domain.Model.ErrorMessage> Errors
            {
                get { return _errors; }
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        string AuthenticationToken
        {

            get
            {
                if (Session[AUTHENTICATION_TOKEN] == null)
                {
                    Session[AUTHENTICATION_TOKEN] = string.Empty;
                }

                return (Session[AUTHENTICATION_TOKEN]).ToString();
            }

            set { Session[AUTHENTICATION_TOKEN] = value; }
        }

        ResponseResult<T> GetResponseResult<T>(string uri, bool useAuthenticationTicket)
        {
            var httpWebRequest = HttpWebRequest.Create(uri) as HttpWebRequest;
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.ContentLength = 0;            

            if(useAuthenticationTicket)
            {
                httpWebRequest.Headers.Add("Authorization", AuthenticationToken);
            }

            var httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse;

            ResponseResult<T> result = null;

            if (httpWebResponse.ContentLength <= 0)
            {
                return result;
            }

            using (var stream = httpWebResponse.GetResponseStream())
            {
                var serializer =
                    new DataContractJsonSerializer(
                        typeof (ResponseResult<T>));

                result = serializer.ReadObject(stream) as ResponseResult<T>;
            }

            return result;
        }

        string GetErrors(List<Domain.Model.ErrorMessage> errorMessages)
        {
            var stringBuilder = new StringBuilder();

            if(errorMessages == null)
            {
                return string.Empty;
            }

            if (errorMessages.Any())
            {                
                foreach (var error in errorMessages)
                {
                    stringBuilder.AppendFormat("Error Code:{0}, {1}", error.Code, error.Description);
                    stringBuilder.AppendLine();
                }
            }

            return stringBuilder.ToString();
        }

        protected void ServiceCallCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            switch(e.Parameter)
            {
                case "authenticate":
                    Authenticate(e);
                    break;

                case "getdashboard":
                    GetDashboard(e);
                    break;

                case "multiplechoiceplayerresult":
                    GetMultipleChoicePlayerResultAnswerInJson(e);
                    break;

                case "multiplecheckboxplayerresult":
                    GetMultipleCheckBoxPlayerResultAnswerInJson(e);
                    break;

                case "textplayerresult":
                    GetTextPlayerResultAnswerInJson(e);
                    break;

                case "votingplayerresult":
                    GetVotingPlayerResultAnswerInJson(e);
                    break;
            }

        }

        void GetVotingPlayerResultAnswerInJson(DevExpress.Web.CallbackEventArgs e)
        {
            var answer = new Domain.Model.VotingPlayerResult();
            answer.PlayerId = Guid.NewGuid();
            answer.TimeTakenToAnswerInSeconds = 123;
            answer.Results = new List<PlayerResultAnswer>();

            for (var i = 0; i < 3; i++)
            {
                answer.Results.Add(new VotingPlayerResultAnswer()
                {
                    AnswerId = 154 + i
                });
            }

            var result = SerializeToJson(answer as PlayerResult);

            if (string.IsNullOrEmpty(result))
            {
                e.Result = "Error occurred";
            }
            else
            {
                e.Result = result;
            }
        }

        void GetTextPlayerResultAnswerInJson(DevExpress.Web.CallbackEventArgs e)
        {
            var answer = new Domain.Model.TextPlayerResult();
            answer.PlayerId = Guid.NewGuid();
            answer.TimeTakenToAnswerInSeconds = 123;
            answer.Answer = "This is my answer to the question";

            var result = SerializeToJson(answer as PlayerResult);

            if (string.IsNullOrEmpty(result))
            {
                e.Result = "Error occurred";
            }
            else
            {
                e.Result = result;
            }
        }

        void GetMultipleCheckBoxPlayerResultAnswerInJson(DevExpress.Web.CallbackEventArgs e)
        {
            var answer = new Domain.Model.MultipleCheckBoxPlayerResult();
            answer.PlayerId = Guid.NewGuid();
            answer.TimeTakenToAnswerInSeconds = 123;
            answer.Results = new List<PlayerResultAnswer>();
            
            for(var i = 0; i < 3; i++)
            {
                answer.Results.Add(new MultipleCheckBoxPlayerResultAnswer()
                                       {
                                           AnswerId = 154 + i
                                       });
            }

            var result = SerializeToJson(answer as PlayerResult);

            if (string.IsNullOrEmpty(result))
            {
                e.Result = "Error occurred";
            }
            else
            {
                e.Result = result;
            }
        }

        void GetMultipleChoicePlayerResultAnswerInJson(DevExpress.Web.CallbackEventArgs e)
        {
            var answer = new Domain.Model.MultipleChoicePlayerResult();
            answer.AnswerId = null;
            answer.PlayerId = Guid.NewGuid();
            answer.TimeTakenToAnswerInSeconds = 120;

            var result = SerializeToJson(answer as PlayerResult);

            if(string.IsNullOrEmpty(result))
            {
                e.Result = "Error occurred";
            }
            else
            {
                e.Result = result;
            }
        }

        void Authenticate(DevExpress.Web.CallbackEventArgs e)
        {
            var username = UsernameTextBox.Text.Trim();
            var password = Uri.EscapeDataString(PasswordTextBox.Text.Trim());

            var uri =
                string.Format("{0}/Authenticate?username={1}&password={2}", ConfigurationHelper.SecurityServiceUrl,
                              username, password);


            var result = GetResponseResult<Domain.Model.AuthenticationTicket>(uri, false);


            if (result == null)
            {
                e.Result = "No response from service";
                return;
            }

            if (result.Errors != null )
            {
                e.Result = GetErrors(result.Errors);
            }
            else
            {
                e.Result = string.Format(result.Result.Token);
                AuthenticationToken = result.Result.Token;
            }
        }


        void GetDashboard(DevExpress.Web.CallbackEventArgs e)
        {
            var uri = string.Format("{0}/GetDashboard", ConfigurationHelper.GameServiceUrl);

            var result = GetResponseResult<Domain.Model.Dashboard>(uri, true);

            if (result == null)
            {
                e.Result = "No response from service";
                return;
            }

            if (result.Errors != null )
            {
                e.Result = GetErrors(result.Errors);
            }
            else
            {
                var gametemplateReceived = result.Result.Games.Count();
                var player = result.Result.Player.ToString();

                e.Result = string.Format("Game count : {0} and player is {1}", gametemplateReceived, player);
            }
        }

        static string SerializeToJson<T>(T objectToSerialize)
        {
            var result = string.Empty;
            using (var stream = new MemoryStream())
            {
                DataContractJsonSerializer serializer =
                    new DataContractJsonSerializer(typeof(Domain.Model.PlayerResult));
                serializer.WriteObject(stream, objectToSerialize);
                result = Encoding.UTF8.GetString(stream.ToArray());
            }
            return result;
        }
    }
}