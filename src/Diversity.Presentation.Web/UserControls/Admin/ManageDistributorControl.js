﻿function OnUsePhysicalCheckBoxCheckChanged(s, e) {

    var checked = UsePhysicalCheckBox.GetValue();

    if (checked != null) {
        if (checked) {

            PostalAddress1TextBox.SetText(PhysicalAddress1TextBox.GetText());
            PostalAddress2TextBox.SetText(PhysicalAddress2TextBox.GetText());
            PostalStateProvinceTextBox.SetText(PhysicalStateProvinceTextBox.GetText());
            PostalCountryTextBox.SetText(PhysicalCountryTextBox.GetText());
            PostalPostCodeTextBox.SetText(PhysicalPostCodeTextBox.GetText());
        }
        else {
            PostalAddress1TextBox.SetText("");
            PostalAddress2TextBox.SetText("");
            PostalStateProvinceTextBox.SetText("");
            PostalCountryTextBox.SetText("");
            PostalPostCodeTextBox.SetText("");
        }
    }
}

function CreateProductCheckBox_OnCheckChanged(s, e) {
    var isChecked = s.GetChecked();
    if (isChecked != null) {

        NoProductSpinEdit.SetEnabled(isChecked);

        if (NoProductSpinEdit.GetEnabled()) {
            NoProductSpinEdit.SetValue(1);
        }
        else {
            NoProductSpinEdit.SetValue(0);
        }
    }
}

function DistributorGridView_CustomButtonClick(s,e) {
    
    if(e.buttonID != 'DeleteDistributorButton') {
        return;
    }

    if(confirm('Deleting this Distributor will delete all associated Clients, Products, Users and their game data. Are you sure you want to delete this distributor?')) {
        s.DeleteRow(e.visibleIndex);
    }
}