﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Diversity.DataModel.SqlRepository;
using EntityDataSourceChangingEventArgs = Microsoft.AspNet.EntityDataSource.EntityDataSourceChangingEventArgs;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class FlashAssetManagementControl : System.Web.UI.UserControl
    {
        private const string FLASH_ASSET_ID_SESSION_KEY = "FlashAssetID";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if(!Page.IsPostBack)
            {
                EditingFlashAssetId = -1;
            }
        }

        int EditingFlashAssetId
        {
            get
            {
                if (Session[FLASH_ASSET_ID_SESSION_KEY] == null)
                {
                    return -1;
                }

                return (int)Session[FLASH_ASSET_ID_SESSION_KEY];
            }
            set { Session[FLASH_ASSET_ID_SESSION_KEY] = value; }
        }

        protected void FlashAssetLanguageGridView_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;
            EditingFlashAssetId = Convert.ToInt32(gridView.GetMasterRowKeyValue());
        }

        protected void FlashAssetValueDataSource_OnInserting(object sender, EntityDataSourceChangingEventArgs e)
        {
            var flashAssetLanguage = e.Entity as FlashAsset_Language;
            flashAssetLanguage.FlashAssetID = EditingFlashAssetId;      
        }

        protected void FlashAssetLanguageGridView_OnInit(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;
            gridView.ForceDataRowType(typeof(FlashAsset_Language));
        }
    }
}