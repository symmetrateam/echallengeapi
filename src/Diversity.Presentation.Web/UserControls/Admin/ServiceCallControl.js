﻿function AuthenticateButton_Click(s, e) {
    LoadingPanel.Show();
    ServiceCallCallback.PerformCallback('authenticate');
}

function ServiceCallCallback_CallbackComplete(s, e) {
    LoadingPanel.Hide();
    ResultMemo.SetText(e.result);
}


function GetDashboardButton_Click(s, e) {
    LoadingPanel.Show();
    ServiceCallCallback.PerformCallback('getdashboard');
}

function MultipleChoicePlayerResultButton_Click(s, e) {
    LoadingPanel.Show();
    ServiceCallCallback.PerformCallback('multiplechoiceplayerresult');
}


function MultipleCheckBoxPlayerResultButton_Click(s, e) {
    LoadingPanel.Show();
    ServiceCallCallback.PerformCallback('multiplecheckboxplayerresult');
}

function TextPlayerResultButton_Click(s, e) {
    LoadingPanel.Show();
    ServiceCallCallback.PerformCallback('textplayerresult');
}

function VotingPlayerResultButton_Click(s, e) {
    LoadingPanel.Show();
    ServiceCallCallback.PerformCallback('votingplayerresult');
}







