﻿function setHiddenState(nodeKey, isHidden) {
    var name = "mycheck_" + nodeKey;
    theForm.elements[name].value = isHidden ? "H" : "V";
    saveState();
}

function saveState() {
    CardClassificationTreeList.PerformCustomCallback("");
}

window.scrollTo = function (x, y) {
    return true;
}

//Methods relating to the UploadPopup Control
var _uploadTYpe = "";
function UploadControl_FileUploadComplete(s, e) {
    var url = e.callbackData.split(";");

    if (url != 'undefined' || url.length > 0) {
        if (_uploadTYpe == "Question") {

            QuestionMultimediaDocumentHyperLink.SetNavigateUrl(url[0]);
            QuestionMultimediaDocumentHyperLink.SetText(url[1]);
            HiddenField.Set("QuestionMultimediaDocumentHyperLinkText", url[1]);
            HiddenField.Set("QuestionMultimediaDocumentHyperLinkNavigateUrl", url[0]);
            QuestionMultimediaBrowseButton.SetVisible(false);
            QuestionMultimediaRemoveHyperLink.SetVisible(true);

        }
        else if (_uploadTYpe == "Answer") {

            AnswerMultimediaDocumentHyperLink.SetNavigateUrl(url[0]);
            AnswerMultimediaDocumentHyperLink.SetText(url[1]);
            HiddenField.Set("AnswerMultimediaDocumentHyperLinkText", url[1]);
            HiddenField.Set("AnswerMultimediaDocumentHyperLinkNavigateUrl", url[0]);
            AnswerMultimediaBrowseButton.SetVisible(false);
            AnswerMultimediaRemoveHyperLink.SetVisible(true);

        }
        else if (_uploadTYpe == "CardReferenceMaterial") {
            CardReferenceMaterialDocumentHyperLink.SetNavigateUrl(url[0]);
            CardReferenceMaterialDocumentHyperLink.SetVisible(true);
            HiddenField.Set("CardReferenceMaterialDocumentHyperLinkNavigateUrl", url[0]);
        }
        else if (_uploadTYpe == "CardReferenceMaterialDetail") {
            DetailReferenceDocumentHyperLink.SetNavigateUrl(url[0]);
            DetailReferenceDocumentHyperLink.SetVisible(true);
            HiddenField.Set("DetailReferenceDocumentHyperLinkNavigateUrl", url[0]);
        }
        else if (_uploadTYpe == "AudioNarrative") {            
            AudioNarrativeMediaHyperLink.SetNavigateUrl(url[0]);
            AudioNarrativeMediaHyperLink.SetText(url[1]);
            AudioNarrativeMediaHyperLink.SetVisible(true);
            HiddenField.Set("AudioNarrativeMediaHyperLinkText", url[1]);
            HiddenField.Set("AudioNarrativeMediaHyperLinkNavigateUrl", url[0]);
            AudioNarrativeMediaRemoveHyperlink.SetVisible(true);
            AudioNarrativeMediaBrowseButton.SetVisible(false);
        }
        else if (_uploadTYpe == "QuestionThumbnail") {
            QuestionThumbnailHyperLink.SetNavigateUrl(url[0]);
            QuestionThumbnailHyperLink.SetText(url[1]);
            QuestionThumbnailHyperLink.SetVisible(true);
            HiddenField.Set("QuestionThumbnailHyperLinkText", url[1]);
            HiddenField.Set("QuestionThumbnailHyperLinkNavigateUrl", url[0]);
            QuestionThumnailRemoveHyperLink.SetVisible(true);
            QuestionThumbnailBrowseButton.SetVisible(false);
        }
        else if (_uploadTYpe == "AnswerThumbnail") {
            AnswerThumbnailHyperLink.SetNavigateUrl(url[0]);
            AnswerThumbnailHyperLink.SetText(url[1]);
            AnswerThumbnailHyperLink.SetVisible(true);
            HiddenField.Set("AnswerThumbnailHyperLinkText", url[1]);
            HiddenField.Set("AnswerThumbnailHyperLinkNavigateUrl", url[0]);
            AnswerThumnailRemoveHyperLink.SetVisible(true);
            AnswerThumbnailBrowseButton.SetVisible(false);
        }

        UploadPopupControl.Hide();
    }
}

function UploadPopupControl_Closing(s, e) {
    _uploadTYpe = "";
    UploadControl.ClearText();
    UploadButton.SetEnabled(UploadControl.GetText(0) != "");
}

function UploadControl_TextChanged(s, e) {
    UploadButton.SetEnabled(UploadControl.GetText(0) != "");
}

function ShowUploadWindow(uploadType) {
    _uploadTYpe = uploadType;
    UploadPopupControl.Show();
}

function UploadButton_Click(s, e) {
    UploadControl.Upload();
}

function QuestionMultimediaTypeComboBox_SelectedIndexChanged(s, e) {

    var text = QuestionMultimediaTypeComboBox.GetValue().toString();

    var urlTextBoxVisible = (text == null || text.toLowerCase() == "wistia");
    QuestionMultimediaUrlTextBox.SetVisible(urlTextBoxVisible);
    QuestionMultimediaUrlTextBox.SetText("");
    QuestionMultimediaDocumentHyperLink.SetNavigateUrl("");
    QuestionMultimediaDocumentHyperLink.SetText("");
    HiddenField.Remove("QuestionMultimediaDocumentHyperLinkText");
    HiddenField.Remove("QuestionMultimediaDocumentHyperLinkNavigateUrl");
    QuestionMultimediaRemoveHyperLink.SetVisible(false);
    QuestionMultimediaBrowseButton.SetVisible(!urlTextBoxVisible);
}

function QuestionThumnailRemoveHyperLink_Click(s, e)
{
    QuestionThumbnailHyperLink.SetNavigateUrl("#");
    QuestionThumbnailHyperLink.SetText("");
    HiddenField.Remove("QuestionThumbnailHyperLinkText");
    HiddenField.Remove("QuestionThumbnailHyperLinkNavigateUrl");    
    QuestionThumbnailBrowseButton.SetVisible(true);
    s.SetVisible(false);
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function AnswerThumnailRemoveHyperLink_Click(s,e) {
    AnswerThumbnailHyperLink.SetNavigateUrl("#");
    AnswerThumbnailHyperLink.SetText("");
    HiddenField.Remove("AnswerThumbnailHyperLinkText");
    HiddenField.Remove("AnswerThumbnailHyperLinkNavigateUrl");    
    AnswerThumbnailBrowseButton.SetVisible(true);
    s.SetVisible(false);
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function QuestionMultimediaRemoveHyperLink_Click(s, e) {
    QuestionMultimediaDocumentHyperLink.SetNavigateUrl("#");
    QuestionMultimediaDocumentHyperLink.SetText("");
    HiddenField.Remove("QuestionMultimediaDocumentHyperLinkText");
    HiddenField.Remove("QuestionMultimediaDocumentHyperLinkNavigateUrl");
    QuestionMultimediaBrowseButton.SetVisible(true);
    s.SetVisible(false);
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function AudioNarrativeMediaRemoveHyperlink_Click(s, e) {
    AudioNarrativeMediaHyperLink.SetNavigateUrl("#");
    AudioNarrativeMediaHyperLink.SetText("");
    AudioNarrativeMediaHyperLink.SetVisible(false);
    HiddenField.Remove("AudioNarrativeMediaHyperLinkText");
    HiddenField.Remove("AudioNarrativeMediaHyperLinkNavigateUrl");
    AudioNarrativeMediaBrowseButton.SetVisible(true);
    s.SetVisible(false);
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function AnswerMultimediaTypeComboBox_SelectedIndexChanged(s, e) {
    var text = AnswerMultimediaTypeComboBox.GetValue().toString();

    var urlTextBoxVisible = (text == null || text.toLowerCase() == "wistia");
    AnswerMultimediaUrlTextBox.SetVisible(urlTextBoxVisible);
    AnswerMultimediaUrlTextBox.SetText("");
    AnswerMultimediaDocumentHyperLink.SetNavigateUrl("");
    HiddenField.Remove("AnswerMultimediaDocumentHyperLinkText");
    HiddenField.Remove("AnswerMultimediaDocumentHyperLinkNavigateUrl");
    AnswerMultimediaDocumentHyperLink.SetText("");
    AnswerMultimediaRemoveHyperLink.SetVisible(false);
    AnswerMultimediaBrowseButton.SetVisible(!urlTextBoxVisible);
}

function AnswerMultimediaRemoveHyperLink_Click(s, e) {
    AnswerMultimediaDocumentHyperLink.SetNavigateUrl("#");
    AnswerMultimediaDocumentHyperLink.SetText("");
    HiddenField.Remove("AnswerMultimediaDocumentHyperLinkText");
    HiddenField.Remove("AnswerMultimediaDocumentHyperLinkNavigateUrl");
    AnswerMultimediaBrowseButton.SetVisible(true);
    s.SetVisible(false);
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function ReferenceMaterialGridViewNewButton_Click(s, e) {
    ReferenceMaterialGridView.AddNewRow();
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function DetailReferenceMaterialGridViewNewButton_Click(s, e) {
    DetailReferenceMaterialGridView.AddNewRow();
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function ShowDuplicateCardWindow() {
    DuplicateCardPopupControl.Show();
}

function DuplicateCardPopupControl_Closing(s, e) {

}

function DuplicateCardPopupControl_Popup(s, e) {
    s.UpdatePosition();
    ASPxClientEdit.ClearGroup('DuplicateCardValidationGroup');
    DuplicateCardShortcutCodeTextBox.SetText('');
    DuplicateCardNameTextBox.SetText('');
    DuplicateErrorMessageLabel.SetText('');
    DuplicateCardShortcutCodeTextBox.SetFocus();
}

function SaveDuplicateCardButton_Click(s, e) {
    if (ASPxClientEdit.ValidateGroup('DuplicateCardValidationGroup')) {
        LoadingPanel.Show();

        var parameters = DuplicateCardShortcutCodeTextBox.GetText() + "~;" + DuplicateCardNameTextBox.GetText() + "~;" + DuplicateCardTitleTextBox.GetText();

        DuplicateCardCallback.PerformCallback(parameters);
    }
}

function CancelDuplicateCardButton_Click(s, e) {
    DuplicateCardPopupControl.Hide();
}

function DuplicateCardCallback_CallbackComplete(s, e) {
    LoadingPanel.Hide();

    if (e.result == null || e.result.length <= 0) {
        DuplicateCardPopupControl.Hide();
    }
    else {
        DuplicateErrorMessageLabel.SetText(e.result);
    }
}

function ValidationCallback_CallbackComplete(s,e) {
    LoadingPanel.Hide();

    var result = e.result;
    if (result == null || result.length <= 0) {
        LoadingPanel.Show();
        UpdateButton.DoClick();
    }
    else {
        QuestionMultimediaUrlTextBox.SetText(unescape(QuestionMultimediaUrlTextBox.GetText()));
        PopupLabel.SetText(result);
        NotificationPopupControl.Show();
    }
}

function ValidateButton_Click(s, e) {
    if (ASPxClientEdit.ValidateEditorsInContainer(null)) {
        LoadingPanel.Show();
        QuestionMultimediaUrlTextBox.SetText(escape(QuestionMultimediaUrlTextBox.GetText()));
        ValidationCallback.PerformCallback();
    }        
}

/* Context Similar Association Methods */

function ContextSimilarHyperLink_Click(s, e) {
    AssociatedCardPopupControl.Show();
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function CancelAssociatedCardButton_Click(s, e) {
    AssociatedCardCallback.PerformCallback('reset');
    AssociatedCardPopupControl.Hide();

}

function AssociatedCardPopupControl_Popup(s, e) {
    s.UpdatePosition();
    AssociatedCardGridView.PerformCallback();
}

function SelectCheckBox_CheckChanged(s, e) {

    var isChecked = s.GetChecked();
    
    var selectedCount = AssociatedCardGridView.GetSelectedRowCount();
    var clientInstanceName = s.cp_ClientInstanceName;
    var visibleIndex = s.cp_VisibleIndex;
    

    if (isChecked) {
        if (selectedCount == 0) {
            AssociatedCardGridView.SelectRowOnPage(s.cp_VisibleIndex);
        }
    }
    else {
        if (selectedCount > 0) {
            var selectedKeys = AssociatedCardGridView.GetSelectedKeysOnPage();
            //as we know this is a single select grid
            var rowKey = selectedKeys[0];
            if (rowKey == s.cp_CardId) {
                s.SetChecked(true);
            }
        }
    }
    
    var parameters = "checked|" + s.cp_CardId + "|" + s.GetChecked();
    AssociatedCardCallback.PerformCallback(parameters);
}

function AssociatedCardGridView_SelectionChanged(s, e) {
    if (e.isSelected) {
        var cardId = s.GetRowKey(e.visibleIndex);
        var selectCheckBox = GetSelectCheckBox(e.visibleIndex);
        var isChecked = true;

        selectCheckBox.SetChecked(true);

        var parameters = "selected|" + cardId + "|" + isChecked;
        AssociatedCardCallback.PerformCallback(parameters);
    }
}

function GetSelectCheckBox(visibleIndex) {
    return eval("SelectCheckBox_" + visibleIndex);
}



/* Leading Question Associated Methods */


function LeadingQuestionSimilarHyperLink_Click(s, e) {
    LeadingQuestionAssociatedCardPopupControl.Show();
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}


function CancelLeadingQuestionAssociatedCardButton_Click(s, e) {
    LeadingQuestionAssociatedCardCallback.PerformCallback('reset');
    LeadingQuestionAssociatedCardPopupControl.Hide();

}

function LeadingQuestionAssociatedCardPopupControl_Popup(s, e) {
    s.UpdatePosition();
    LeadingQuestionAssociatedCardGridView.PerformCallback();
}

function LeadingQuestionSelectCheckBox_CheckChanged(s, e) {

    var isChecked = s.GetChecked();

    var selectedCount = LeadingQuestionAssociatedCardGridView.GetSelectedRowCount();
    var clientInstanceName = s.cp_ClientInstanceName;
    var visibleIndex = s.cp_VisibleIndex;


    if (isChecked) {
        if (selectedCount == 0) {
            LeadingQuestionAssociatedCardGridView.SelectRowOnPage(s.cp_VisibleIndex);
        }
    }
    else {
        if (selectedCount > 0) {
            var selectedKeys = LeadingQuestionAssociatedCardGridView.GetSelectedKeysOnPage();
            //as we know this is a single select grid
            var rowKey = selectedKeys[0];
            if (rowKey == s.cp_CardId) {
                s.SetChecked(true);
            }
        }
    }

    var parameters = "checked|" + s.cp_CardId + "|" + s.GetChecked();
    LeadingQuestionAssociatedCardCallback.PerformCallback(parameters);
}

function LeadingQuestionAssociatedCardGridView_SelectionChanged(s, e) {
    if (e.isSelected) {
        var cardId = s.GetRowKey(e.visibleIndex);
        var selectCheckBox = GetLeadingQuestionSelectCheckBox(e.visibleIndex);
        var isChecked = true;

        selectCheckBox.SetChecked(true);

        var parameters = "selected|" + cardId + "|" + isChecked;
        LeadingQuestionAssociatedCardCallback.PerformCallback(parameters);
    }
}

function GetLeadingQuestionSelectCheckBox(visibleIndex) {
    return eval("LeadingQuestionSelectCheckBox_" + visibleIndex);
}

/* Audit Statement Association Methods */


function AuditStatementSimilarHyperLink_Click(s, e) {
    AuditStatementAssociatedCardPopupControl.Show();
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}


function CancelAuditStatementAssociatedCardButton_Click(s, e) {
    AuditStatementAssociatedCardCallback.PerformCallback('reset');
    AuditStatementAssociatedCardPopupControl.Hide();

}

function AuditStatementAssociatedCardPopupControl_Popup(s, e) {
    s.UpdatePosition();
    AuditStatementAssociatedCardGridView.PerformCallback();
}

function AuditStatementSelectCheckBox_CheckChanged(s, e) {

    var isChecked = s.GetChecked();

    var selectedCount = AuditStatementAssociatedCardGridView.GetSelectedRowCount();
    var clientInstanceName = s.cp_ClientInstanceName;
    var visibleIndex = s.cp_VisibleIndex;


    if (isChecked) {
        if (selectedCount == 0) {
            AuditStatementAssociatedCardGridView.SelectRowOnPage(s.cp_VisibleIndex);
        }
    }
    else {
        if (selectedCount > 0) {
            var selectedKeys = AuditStatementAssociatedCardGridView.GetSelectedKeysOnPage();
            //as we know this is a single select grid
            var rowKey = selectedKeys[0];
            if (rowKey == s.cp_CardId) {
                s.SetChecked(true);
            }
        }
    }

    var parameters = "checked|" + s.cp_CardId + "|" + s.GetChecked();
    AuditStatementAssociatedCardCallback.PerformCallback(parameters);
}

function AuditStatementAssociatedCardGridView_SelectionChanged(s, e) {
    if (e.isSelected) {
        var cardId = s.GetRowKey(e.visibleIndex);
        var selectCheckBox = GetAuditStatementImageSelectCheckBox(e.visibleIndex);
        var isChecked = true;

        selectCheckBox.SetChecked(true);

        var parameters = "selected|" + cardId + "|" + isChecked;
        AuditStatementImageAssociatedCardCallback.PerformCallback(parameters);
    }
}

function AuditStatementImageSimilarHyperLink_Click(s,e) {    
    AuditStatementImageAssociatedCardPopupControl.Show();
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}


function CancelAuditStatementImageAssociatedCardButton_Click(s, e) {
    AuditStatementImageAssociatedCardCallback.PerformCallback('reset');
    AuditStatementImageAssociatedCardPopupControl.Hide();
}


function AuditStatementImageAssociatedCardPopupControl_Popup(s, e) {
    s.UpdatePosition();
    AuditStatementImageAssociatedCardGridView.PerformCallback();
}

function AuditStatementImageSelectCheckBox_CheckChanged(s, e) {

    var isChecked = s.GetChecked();

    var selectedCount = AuditStatementImageAssociatedCardGridView.GetSelectedRowCount();
    var clientInstanceName = s.cp_ClientInstanceName;
    var visibleIndex = s.cp_VisibleIndex;


    if (isChecked) {
        if (selectedCount == 0) {
            AuditStatementImageAssociatedCardGridView.SelectRowOnPage(s.cp_VisibleIndex);
        }
    }
    else {
        if (selectedCount > 0) {
            var selectedKeys = AuditStatementImageAssociatedCardGridView.GetSelectedKeysOnPage();
            //as we know this is a single select grid
            var rowKey = selectedKeys[0];
            if (rowKey == s.cp_CardId) {
                s.SetChecked(true);
            }
        }
    }

    var parameters = "checked|" + s.cp_CardId + "|" + s.GetChecked();
    AuditStatementImageAssociatedCardCallback.PerformCallback(parameters);
}


function AuditStatementImageAssociatedCardGridView_SelectionChanged(s, e) {
    if (e.isSelected) {
        var cardId = s.GetRowKey(e.visibleIndex);
        var selectCheckBox = GetAuditStatementImageSelectCheckBox(e.visibleIndex);
        var isChecked = true;

        selectCheckBox.SetChecked(true);

        var parameters = "selected|" + cardId + "|" + isChecked;
        AuditStatementImageAssociatedCardCallback.PerformCallback(parameters);
    }
}

function GetAuditStatementSelectCheckBox(visibleIndex) {
    return eval("AuditStatementSelectCheckBox_" + visibleIndex);
}

function GetAuditStatementImageSelectCheckBox(visibleIndex) {
    return eval("AuditStatementImageSelectCheckBox_" + visibleIndex);
}