﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CardManagementControl.ascx.cs"
    Inherits="Diversity.Presentation.Web.UserControls.Admin.CardManagementControl" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>





<%@ Import Namespace="Diversity.Common.Enumerations" %>


<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Src="Cards/MultipleCheckBoxQuestionCardControl.ascx" TagName="MultipleCheckBoxQuestionCardControl"
    TagPrefix="uc1" %>
<%@ Register Src="Cards/MultipleChoiceQuestionCardControl.ascx" TagName="MultipleChoiceQuestionCardControl"
    TagPrefix="uc2" %>
<%@ Register Src="Cards/TextAreaInputCardControl.ascx" TagName="TextAreaInputCardControl"
    TagPrefix="uc3" %>
<%@ Register Src="Cards/TextBoxInputCardControl.ascx" TagName="TextBoxInputCardControl"
    TagPrefix="uc4" %>
<%@ Register Src="Cards/MultipleCheckBoxSurveyCardControl.ascx" TagName="MultipleCheckBoxSurveyCardControl"
    TagPrefix="uc5" %>
<%@ Register Src="Cards/MultipleChoiceSurveyCardControl.ascx" TagName="MultipleChoiceSurveyCardControl"
    TagPrefix="uc6" %>
<%@ Register Src="Cards/MultipleChoiceAuditStatementCardControl.ascx" TagName="MultipleChoiceAuditStatementCardControl"
    TagPrefix="uc7" %>
<%@ Register Src="Cards/TextAndImageCardControl.ascx" TagName="TextAndImageCardControl"
    TagPrefix="uc8" %>
<%@ Register Src="Cards/PriorityVotingCardControl.ascx" TagName="PriorityVotingCardControl"
    TagPrefix="uc9" %>
<%@ Register Src="Cards/MultipleChoicePriorityFeedbackCardControl.ascx" TagName="MultipleChoicePriorityFeedbackCardControl"
    TagPrefix="uc10" %>
<%@ Register Src="Cards/MultipleCheckBoxPriorityFeedbackCardControl.ascx" TagName="MultipleCheckBoxPriorityFeedbackCardControl"
    TagPrefix="uc11" %>
<%@ Register Src="Cards/MultipleCheckBoxPersonalActionPlanControl.ascx" TagName="MultipleCheckBoxPersonalActionPlanControl"
    TagPrefix="uc12" %>
<%@ Register Src="Cards/MultipleChoicePersonalActionPlanControl.ascx" TagName="MultipleChoicePersonalActionPlanControl"
    TagPrefix="uc13" %>
<%@ Register Src="Cards/MultipleChoiceImageQuestionCardControl.ascx" TagName="MultipleChoiceImageQuestionControl"
    TagPrefix="uc14" %>
<%@ Register Src="Cards/MultipleChoiceAuditStatementImageCardControl.ascx" TagName="MultipleChoiceAuditStatementImageCardControl"
    TagPrefix="uc15" %>
<%@ Register Src="Cards/StatementListCardControl.ascx" TagName="StatementListCardControl" TagPrefix="uc16" %>
<%@ Register Src="Cards/MatchingListQuestionCard.ascx" TagName="MatchingListCardControl" TagPrefix="uc17" %>
<%@ Register src="Cards/TextAreaInputLocationCardControl.ascx" tagName="TextAreaLocationCardControl" tagPrefix="uc18" %>
<%@ Register src="Cards/MultipleCheckBoxImageQuestionCard.ascx" tagName="MultipleCheckBoxImageCardControl" tagPrefix="uc19" %>
<%@ Register src="Cards/BranchingCardControl.ascx" TagName="BranchingCardControl" tagPrefix="uc20" %>
<style type="text/css">
    .inline {
        display: inline-table;
    }
</style>
<script id="dxis_CardManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/CardManagementControl.js") %>'>
</script>
<dx:ASPxHiddenField ID="HiddenField" runat="server" ClientInstanceName="HiddenField">
</dx:ASPxHiddenField>
<dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" Modal="true" ClientInstanceName="LoadingPanel">
</dx:ASPxLoadingPanel>
<dx:ASPxCallback ID="DuplicateCardCallback" runat="server" ClientInstanceName="DuplicateCardCallback"
    OnCallback="DuplicateCardCallback_Callback">
    <ClientSideEvents CallbackComplete="DuplicateCardCallback_CallbackComplete" />
</dx:ASPxCallback>
<dx:ASPxCallback runat="server" ID="ValidationCallback" ClientInstanceName="ValidationCallback" OnCallback="ValidationCallback_OnCallback">
    <ClientSideEvents CallbackComplete="ValidationCallback_CallbackComplete"></ClientSideEvents>
</dx:ASPxCallback>
<dx:ASPxPopupControl ID="UploadPopupControl" runat="server" ClientInstanceName="UploadPopupControl"
    CloseAction="CloseButton" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Upload File" AllowDragging="true" EnableAnimation="false" EnableViewState="false">
    <ClientSideEvents Closing="UploadPopupControl_Closing" PopUp="function(s){s.UpdatePosition();}" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
            <dx:ASPxPanel ID="UploadPanel" runat="server" DefaultButton="UploadButton">
                <PanelCollection>
                    <dx:PanelContent>
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="SelectFileLabel" runat="server" Text="Select File">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxUploadControl ID="UploadControl" runat="server" ClientInstanceName="UploadControl"
                                        EnableViewState="false" ShowProgressPanel="true" OnFileUploadComplete="UploadControl_FileUploadComplete"
                                        UploadMode="Standard">
                                        <ValidationSettings MaxFileSize="4194304" MaxFileSizeErrorText="Maximum file size allowed is 4Mb">
                                        </ValidationSettings>
                                        <ClientSideEvents FileUploadComplete="UploadControl_FileUploadComplete" TextChanged="UploadControl_TextChanged" />
                                    </dx:ASPxUploadControl>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <dx:ASPxButton ID="UploadButton" runat="server" ClientInstanceName="UploadButton"
                                        Text="Upload" AutoPostBack="false" ClientEnabled="false">
                                        <ClientSideEvents Click="UploadButton_Click" />
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
<dx:ASPxPopupControl ID="DuplicateCardPopupControl" runat="server" ClientInstanceName="DuplicateCardPopupControl"
    CloseAction="CloseButton" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Duplicate Card Details" AllowDragging="true" EnableAnimation="false"
    Width="400px" EnableViewState="false">
    <ClientSideEvents Closing="DuplicateCardPopupControl_Closing" PopUp="DuplicateCardPopupControl_Popup" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="DuplicateCardContent1" runat="server">
            <dx:ASPxPanel ID="DuplicateCardPanel" runat="server" DefaultButton="PopupDuplicateCardButton">
                <PanelCollection>
                    <dx:PanelContent>
                        <table width="100%">
                            <tr>
                                <td colspan="3" class="errormsg">
                                    <dx:ASPxLabel ID="DuplicateErrorMessageLabel" runat="server" ClientInstanceName="DuplicateErrorMessageLabel">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="DuplicateCardShortcutCodeLabel" runat="server" Text="Card Shortcut code">
                                    </dx:ASPxLabel>
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxTextBox ID="DuplicateCardShortcutCodeTextBox" runat="server" ClientInstanceName="DuplicateCardShortcutCodeTextBox"
                                        ValidationSettings-ValidationGroup="DuplicateCardValidationGroup">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="true" ErrorText="A card shortcut code is required" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="DuplicateCardNameLabel" runat="server" Text="Card Name">
                                    </dx:ASPxLabel>
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxTextBox ID="DuplicateCardNameTextBox" runat="server" ClientInstanceName="DuplicateCardNameTextBox"
                                        ValidationSettings-ValidationGroup="DuplicateCardValidationGroup">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="true" ErrorText="A card name is required" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="DuplicateCardTitleLabel" runat="server" Text="Card Title">
                                    </dx:ASPxLabel>
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxTextBox ID="DuplicateCardTitleTextBox" runat="server" ClientInstanceName="DuplicateCardTitleTextBox"
                                        ValidationSettings-ValidationGroup="DuplicateCardValidationGroup">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField IsRequired="true" ErrorText="A card title is required" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right">
                                    <table style="border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="SaveDuplicateCardButton" runat="server" ClientInstanceName="SaveDuplicateCardButton"
                                                    AutoPostBack="false" Text="Save">
                                                    <ClientSideEvents Click="SaveDuplicateCardButton_Click" />
                                                </dx:ASPxButton>
                                            </td>
                                            <td class="spacer"></td>
                                            <td>
                                                <dx:ASPxButton ID="CancelDuplicateCardButton" runat="server" ClientInstanceName="CancelDuplicateCardButton"
                                                    AutoPostBack="false" Text="Cancel">
                                                    <ClientSideEvents Click="CancelDuplicateCardButton_Click" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
<dx:ASPxCallback ID="AssociatedCardCallback" runat="server" ClientInstanceName="AssociatedCardCallback"
    OnCallback="AssociatedCardCallback_Callback">
</dx:ASPxCallback>
<dx:ASPxPopupControl ID="AssociatedCardPopupControl" runat="server" ClientInstanceName="AssociatedCardPopupControl"
    CloseAction="CloseButton" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Associate Cards" AllowDragging="true" EnableAnimation="false" Width="800px"
    ShowFooter="true" EnableViewState="false">
    <ClientSideEvents PopUp="function(s,e){AssociatedCardPopupControl_Popup(s,e);}" />
    <ContentCollection>
        <dx:PopupControlContentControl>
            <dx:ASPxPanel ID="AssociatedCardPanel" runat="server">
                <PanelCollection>
                    <dx:PanelContent>
                        <dx:ASPxGridView ID="AssociatedCardGridView" runat="server" ClientInstanceName="AssociatedCardGridView"
                            AutoGenerateColumns="False" DataSourceID="CardDataSource" KeyFieldName="CardID"
                            Width="100%" OnDataBound="AssociatedCardGridView_DataBound">
                            <ClientSideEvents SelectionChanged="AssociatedCardGridView_SelectionChanged" />
                            <SettingsBehavior ColumnResizeMode="NextColumn" EnableRowHotTrack="True" AllowSelectSingleRowOnly="true" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                            <SettingsPager AlwaysShowPager="True">
                                <AllButton Visible="True">
                                    <Image ToolTip="Show All Products">
                                    </Image>
                                </AllButton>
                            </SettingsPager>
                            <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Visible="true">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataCheckColumn VisibleIndex="0">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox ID="SelectCheckBox" runat="server" OnLoad="SelectCheckBox_Load"
                                            ClientInstanceName="SelectCheckBox" Value='<%#Eval("Selected") %>'>
                                            <ClientSideEvents CheckedChanged="function(s,e){SelectCheckBox_CheckChanged(s,e);}" />
                                        </dx:ASPxCheckBox>
                                    </DataItemTemplate>
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewDataTextColumn FieldName="CardID" ReadOnly="True" VisibleIndex="0" Visible="false">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CardShortcutCode" Caption="Shortcut Code" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Internal_CardTypeName" Caption="Card Type"
                                    VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Library" Caption="Library" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DisplayCardName" Caption="Card Name" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DisplayCardDescription" Caption="Card Description"
                                    VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="IsRatified" Caption="Ratified" VisibleIndex="7">
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                        </dx:ASPxGridView>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
    <FooterTemplate>
        <table style="margin-left: auto; margin-right: 6px;">
            <tr>
                <td>
                    <dx:ASPxButton ID="CancelAssociatedCardButton" runat="server" ClientInstanceName="CancelAssociatedCardButton"
                        AutoPostBack="false" Text="Cancel">
                        <ClientSideEvents Click="function(s,e){CancelAssociatedCardButton_Click(s,e);}" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </FooterTemplate>
</dx:ASPxPopupControl>
<dx:ASPxCallback ID="LeadingQuestionAssociatedCardCallback" runat="server" ClientInstanceName="LeadingQuestionAssociatedCardCallback"
    OnCallback="LeadingQuestionAssociatedCardCallback_Callback">
</dx:ASPxCallback>
<dx:ASPxPopupControl ID="LeadingQuestionAssociatedCardPopupControl" runat="server"
    ClientInstanceName="LeadingQuestionAssociatedCardPopupControl" CloseAction="CloseButton"
    Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Leading Question Associate Cards" AllowDragging="true" EnableAnimation="false"
    Width="800px" ShowFooter="true" EnableViewState="false">
    <ClientSideEvents PopUp="function(s,e){LeadingQuestionAssociatedCardPopupControl_Popup(s,e);}" />
    <ContentCollection>
        <dx:PopupControlContentControl>
            <dx:ASPxPanel ID="LeadingQuestionAssociatedCardPanel" runat="server">
                <PanelCollection>
                    <dx:PanelContent>
                        <dx:ASPxGridView ID="LeadingQuestionAssociatedCardGridView" runat="server" ClientInstanceName="LeadingQuestionAssociatedCardGridView"
                            AutoGenerateColumns="False" DataSourceID="LeadingQuestionCardDataSource" KeyFieldName="CardID"
                            Width="100%" OnDataBound="LeadingQuestionAssociatedCardGridView_DataBound">
                            <ClientSideEvents SelectionChanged="LeadingQuestionAssociatedCardGridView_SelectionChanged" />
                            <SettingsBehavior ColumnResizeMode="NextColumn" EnableRowHotTrack="True" AllowSelectSingleRowOnly="true" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                            <SettingsPager AlwaysShowPager="True">
                                <AllButton Visible="True">
                                    <Image ToolTip="Show All Products">
                                    </Image>
                                </AllButton>
                            </SettingsPager>
                            <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Visible="true">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataCheckColumn VisibleIndex="0">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox ID="LeadingQuestionSelectCheckBox" runat="server" OnLoad="LeadingQuestionSelectCheckBox_Load"
                                            ClientInstanceName="LeadingQuestionSelectCheckBox" Value='<%#Eval("Selected") %>'>
                                            <ClientSideEvents CheckedChanged="function(s,e){LeadingQuestionSelectCheckBox_CheckChanged(s,e);}" />
                                        </dx:ASPxCheckBox>
                                    </DataItemTemplate>
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewDataTextColumn FieldName="CardID" ReadOnly="True" VisibleIndex="0" Visible="false">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CardShortcutCode" Caption="Shortcut Code" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Internal_CardTypeName" Caption="Card Type"
                                    VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Library" Caption="Library" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DisplayCardName" Caption="Card Name" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DisplayCardDescription" Caption="Card Description"
                                    VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="IsRatified" Caption="Ratified" VisibleIndex="7">
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                        </dx:ASPxGridView>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
    <FooterTemplate>
        <table style="margin-left: auto; margin-right: 6px;">
            <tr>
                <td>
                    <dx:ASPxButton ID="CancelLeadingQuestionAssociatedCardButton" runat="server" ClientInstanceName="CancelLeadingQuestionAssociatedCardButton"
                        AutoPostBack="false" Text="Cancel">
                        <ClientSideEvents Click="function(s,e){CancelLeadingQuestionAssociatedCardButton_Click(s,e);}" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </FooterTemplate>
</dx:ASPxPopupControl>
<dx:ASPxCallback ID="AuditStatementAssociatedCardCallback" runat="server" ClientInstanceName="AuditStatementAssociatedCardCallback"
    OnCallback="AuditStatementAssociatedCardCallback_Callback">
</dx:ASPxCallback>
<dx:ASPxPopupControl ID="AuditStatementAssociatedCardPopupControl" runat="server"
    ClientInstanceName="AuditStatementAssociatedCardPopupControl" CloseAction="CloseButton"
    Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Associate Audit Statement Cards" AllowDragging="true" EnableAnimation="false"
    Width="800px" ShowFooter="true" EnableViewState="false">
    <ClientSideEvents PopUp="function(s,e){AuditStatementAssociatedCardPopupControl_Popup(s,e);}" />
    <ContentCollection>
        <dx:PopupControlContentControl>
            <dx:ASPxPanel ID="AuditStatementAssociatedCardPanel" runat="server">
                <PanelCollection>
                    <dx:PanelContent>
                        <dx:ASPxGridView ID="AuditStatementAssociatedCardGridView" runat="server" ClientInstanceName="AuditStatementAssociatedCardGridView"
                            AutoGenerateColumns="False" DataSourceID="AuditStatementCardDataSource" KeyFieldName="CardID"
                            Width="100%" OnDataBound="AuditStatementAssociatedCardGridView_DataBound">
                            <ClientSideEvents SelectionChanged="AuditStatementAssociatedCardGridView_SelectionChanged" />
                            <SettingsBehavior ColumnResizeMode="NextColumn" EnableRowHotTrack="True" AllowSelectSingleRowOnly="true" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                            <SettingsPager AlwaysShowPager="True">
                                <AllButton Visible="True">
                                    <Image ToolTip="Show All Products">
                                    </Image>
                                </AllButton>
                            </SettingsPager>
                            <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Visible="true">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataCheckColumn VisibleIndex="0">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox ID="AuditStatementSelectCheckBox" runat="server" OnLoad="AuditStatementSelectCheckBox_Load"
                                            ClientInstanceName="AuditStatementSelectCheckBox" Value='<%#Eval("Selected") %>'>
                                            <ClientSideEvents CheckedChanged="function(s,e){AuditStatementSelectCheckBox_CheckChanged(s,e);}" />
                                        </dx:ASPxCheckBox>
                                    </DataItemTemplate>
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewDataTextColumn FieldName="CardID" ReadOnly="True" VisibleIndex="0" Visible="false">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CardShortcutCode" Caption="Shortcut Code" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Internal_CardTypeName" Caption="Card Type"
                                    VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Library" Caption="Library" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DisplayCardName" Caption="Card Name" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DisplayCardDescription" Caption="Card Description"
                                    VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="IsRatified" Caption="Ratified" VisibleIndex="7">
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                        </dx:ASPxGridView>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
    <FooterTemplate>
        <table style="margin-left: auto; margin-right: 6px;">
            <tr>
                <td>
                    <dx:ASPxButton ID="CancelAuditStatementAssociatedCardButton" runat="server" ClientInstanceName="CancelAuditStatementAssociatedCardButton"
                        AutoPostBack="false" Text="Cancel">
                        <ClientSideEvents Click="function(s,e){CancelAuditStatementAssociatedCardButton_Click(s,e);}" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </FooterTemplate>
</dx:ASPxPopupControl>

<dx:ASPxCallback ID="AuditStatementImageAssociatedCardCallback" runat="server" ClientInstanceName="AuditStatementImageAssociatedCardCallback"
    OnCallback="AuditStatementImageAssociatedCardCallback_Callback">
</dx:ASPxCallback>
<dx:ASPxPopupControl ID="AuditStatementImageAssociatedCardPopupControl" runat="server"
    ClientInstanceName="AuditStatementImageAssociatedCardPopupControl" CloseAction="CloseButton"
    Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Associate Audit Image Statement Cards" AllowDragging="true" EnableAnimation="false"
    Width="800px" ShowFooter="true" EnableViewState="false">
    <ClientSideEvents PopUp="function(s,e){AuditStatementImageAssociatedCardPopupControl_Popup(s,e);}" />
    <ContentCollection>
        <dx:PopupControlContentControl>
            <dx:ASPxPanel ID="ASPxPanel1" runat="server">
                <PanelCollection>
                    <dx:PanelContent>
                        <dx:ASPxGridView ID="AuditStatementImageAssociatedCardGridView" runat="server" ClientInstanceName="AuditStatementImageAssociatedCardGridView"
                            AutoGenerateColumns="False" DataSourceID="AuditStatementImageCardDataSource" KeyFieldName="CardID"
                            Width="100%" OnDataBound="AuditStatementImageAssociatedCardGridView_OnDataBound">
                            <ClientSideEvents SelectionChanged="AuditStatementImageAssociatedCardGridView_SelectionChanged" />
                            <SettingsBehavior ColumnResizeMode="NextColumn" EnableRowHotTrack="True" AllowSelectSingleRowOnly="true" />
                            <Styles>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                            </Styles>
                            <SettingsPager AlwaysShowPager="True">
                                <AllButton Visible="True">
                                    <Image ToolTip="Show All Products">
                                    </Image>
                                </AllButton>
                            </SettingsPager>
                            <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Visible="true">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataCheckColumn VisibleIndex="0">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox ID="AuditStatementImageSelectCheckBox" runat="server" OnLoad="AuditStatementImageSelectCheckBox_Load"
                                            ClientInstanceName="AuditStatementImageSelectCheckBox" Value='<%#Eval("Selected") %>'>
                                            <ClientSideEvents CheckedChanged="function(s,e){AuditStatementImageSelectCheckBox_CheckChanged(s,e);}" />
                                        </dx:ASPxCheckBox>
                                    </DataItemTemplate>
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewDataTextColumn FieldName="CardID" ReadOnly="True" VisibleIndex="0" Visible="false">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CardShortcutCode" Caption="Shortcut Code" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Internal_CardTypeName" Caption="Card Type"
                                    VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Library" Caption="Library" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DisplayCardName" Caption="Card Name" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DisplayCardDescription" Caption="Card Description"
                                    VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="IsRatified" Caption="Ratified" VisibleIndex="7">
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                        </dx:ASPxGridView>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
    <FooterTemplate>
        <table style="margin-left: auto; margin-right: 6px;">
            <tr>
                <td>
                    <dx:ASPxButton ID="CancelAuditStatementImageAssociatedCardButton" runat="server" ClientInstanceName="CancelAuditStatementImageAssociatedCardButton"
                        AutoPostBack="false" Text="Cancel">
                        <ClientSideEvents Click="function(s,e){CancelAuditStatementImageAssociatedCardButton_Click(s,e);}" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </FooterTemplate>
</dx:ASPxPopupControl>
<dx:ASPxPopupControl ID="NotificationPopupControl" ClientInstanceName="NotificationPopupControl"
    ShowCloseButton="True" PopupHorizontalAlign="WindowCenter" HeaderText="Error" PopupVerticalAlign="WindowCenter"
    CloseAction="CloseButton" Modal="True" runat="server" ShowOnPageLoad="False">
    <ContentCollection>
        <dx:PopupControlContentControl>
            <div>
                <p>
                    <dx:ASPxLabel runat="server" ID="PopupLabel" ClientInstanceName="PopupLabel" />
                </p>
            </div>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
<table style="width: 100%">
    <tr>
        <td style="vertical-align: top">
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="ProductLabel" runat="server" Text="Product" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxComboBox ID="ProductComboBox" runat="server" DataSourceID="ProductDataSource"
                            ClientInstanceName="ProductComboBox" TextField="ProductName" ValueField="ProductID"
                            ValueType="System.Int32" AutoPostBack="true" DropDownStyle="DropDownList" OnSelectedIndexChanged="ProductComboBox_SelectedIndexChanged">
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="A product is required" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="DefautCardLanguageLabel" runat="server" Text="Default Language" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxComboBox ID="DefaultLanguageComboBox" runat="server" ClientInstanceName="DefaultLanguageComboBox"
                            TextField="Language1" ValueField="LanguageID" ValueType="System.Int32" DataSourceID="DefaultLanguageDataSource"
                            DropDownStyle="DropDown">
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="A language is required" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                </tr>
            </table>
        </td>
        <td align="right" style="vertical-align: top;">
            <% if (CurrentCardId.HasValue)
               { %>
            <dx:ASPxButton ID="DuplicateCardButton" runat="server" ClientVisible="False" AutoPostBack="false"
                EnableTheming="false" EnableViewState="false" Cursor="pointer" ToolTip="Duplicate this card">
                <ClientSideEvents Click="function(s,e){ ShowDuplicateCardWindow(); }" />
                <Image Width="32px" Height="32px" AlternateText="Duplicate Card" Url="~/App_Themes/Icons/DuplicateOrCopy.png">
                </Image>
            </dx:ASPxButton>
            <%} %>
        </td>
    </tr>
</table>
<hr />
<table>
    <tr>
        <td>
            <h3>
                <asp:Literal ID="CardManagementLiteral" runat="server" Text="Card Management" /></h3>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="ShortcutCodeLabel" runat="server" Text="Shortcut code" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxTextBox ID="ShortcutCodeTextBox" runat="server">
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="A card shortcut code is required" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="CardNameLabel" runat="server" Text="Card name" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxTextBox ID="CardNameTextBox" runat="server">
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="A card name is required" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                </tr>                
                <tr>
                    <td>
                        <dx:ASPxLabel ID="CardDescriptionLabel" runat="server" Text="Card description" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxMemo ID="CardDescriptionMemo" runat="server" Rows="5" Width="100%">
                        </dx:ASPxMemo>
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td colspan="3">
                        <dx:ASPxCheckBox ID="StillInBetaCheckBox" runat="server" ClientVisible="False" Text="This card is still under development - do not allow for use in games">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td>
                        <dx:ASPxCheckBox ID="UseOnlineCheckBox" ClientVisible="False" runat="server" Text="Applicable for use online">
                        </dx:ASPxCheckBox>
                    </td>
                    <td></td>
                    <td>
                        <dx:ASPxCheckBox ID="UseDesktopCheckBox" ClientVisible="False" runat="server" Text="Applicable for use on desktop version">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td colspan="3">
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="LibraryLabel" runat="server" Text="Library" ClientVisible="False" />
                                </td>
                                <td>
                                    <dx:ASPxRadioButton ID="PublicRadioButton" runat="server" ClientVisible="False" GroupName="LibraryRadioButtonGroup"
                                        Text="Public">
                                    </dx:ASPxRadioButton>
                                </td>
                                <td>
                                    <dx:ASPxRadioButton ID="ClientRadioButton" runat="server" ClientVisible="False" GroupName="LibraryRadioButtonGroup"
                                        Text="Client">
                                    </dx:ASPxRadioButton>
                                </td>
                                <td>
                                    <dx:ASPxComboBox ID="ClientComboBox" runat="server" ClientVisible="False" DataSourceID="ClientDataSource"
                                        TextField="ClientName" ValueField="ClientID" ValueType="System.Int32">
                                    </dx:ASPxComboBox>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="OfferPublicButton" runat="server" ClientVisible="False" Text="Offer to public library">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td colspan="3">
                        <dx:ASPxHyperLink ID="ContextSimilarHyperLink" runat="server" Text="Click here "
                            ClientInstanceName="ContextSimilarHyperLink" ClientVisible="False" Cursor="pointer">
                            <ClientSideEvents Click="ContextSimilarHyperLink_Click" />
                        </dx:ASPxHyperLink>
                        <dx:ASPxLabel ID="ContextSimilarLabel" runat="server" ClientVisible="False" Text="to select cards contextually similar to this one." />
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td colspan="3">
                        <dx:ASPxHyperLink ID="LeadingQuestionSimilarHyperLink" runat="server" Text="Click here "
                            ClientInstanceName="LeadingQuestionSimilarHyperLink" ClientVisible="False" Cursor="pointer">
                            <ClientSideEvents Click="LeadingQuestionSimilarHyperLink_Click" />
                        </dx:ASPxHyperLink>
                        <dx:ASPxLabel ID="LeadingQuestionSimilarLabel" runat="server" ClientVisible="False" Text="to link to leading question cards." />
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td colspan="3">
                        <dx:ASPxHyperLink ID="AuditStatementSimilarHyperLink" runat="server" Text="Click here "
                            ClientInstanceName="AuditStatementSimilarHyperLink" ClientVisible="False" Cursor="pointer">
                            <ClientSideEvents Click="AuditStatementSimilarHyperLink_Click" />
                        </dx:ASPxHyperLink>
                        <dx:ASPxLabel ID="AuditStatementSimilarLabel" runat="server" ClientVisible="False" Text="to link to audit statement cards." />
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td colspan="3">
                        <dx:ASPxHyperLink ID="AuditStatementImageSimilarHyperLink" runat="server" Text="Click here "
                            ClientInstanceName="AuditStatementImageSimilarHyperLink" ClientVisible="False" Cursor="pointer">
                            <ClientSideEvents Click="AuditStatementImageSimilarHyperLink_Click" />
                        </dx:ASPxHyperLink>
                        <dx:ASPxLabel ID="AuditStatementImageSimilarLabel" runat="server" ClientVisible="False" Text="to link to audit statement image cards." />
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td>
                        <dx:ASPxLabel ID="SubmitRatificationLabel" ClientVisible="False" runat="server" Text="Submit for ratification?" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxComboBox ID="RatificationComboBox" ClientVisible="False" runat="server">
                            <Items>
                                <dx:ListEditItem Text="No" Selected="true" />
                                <dx:ListEditItem Text="Yes" />
                            </Items>
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr style="display: none; border-collapse: collapse;">
                    <td colspan="3">
                        <dx:ASPxLabel ID="RatificationStatusLabel" ClientVisible="False" runat="server" Text="Status:" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<hr />
<table style="width: 100%;">
    <tr>
        <td>
            <h3>
                <asp:Literal ID="CardContentLiteral" runat="server" Text="Card Content" /></h3>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="CardTitleLabel" runat="server" Text="On-screen Card Title" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxTextBox ID="CardTitleTextBox" runat="server">
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="A card title is required" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <% if (CurrentCardType == CardType.MultipleChoiceQuestionCard)
               {
            %>
            <uc2:MultipleChoiceQuestionCardControl ID="QuestionMultipleChoiceCardControl" runat="server"
                Width="100%" />
            <%
               }
               else if (CurrentCardType == CardType.MultipleCheckBoxQuestionCard)
               {
            %>
            <uc1:MultipleCheckBoxQuestionCardControl ID="QuestionMultipleCheckBoxCardControl"
                Width="100%" runat="server" />            

            <%
               }
               else if (CurrentCardType == CardType.LeadingQuestionCard ||
                    CurrentCardType == CardType.IntroSurveyCard ||
                    CurrentCardType == CardType.TextAreaSurveyCard ||
                    CurrentCardType == CardType.TextAreaPriorityFeedbackCard ||
                    CurrentCardType == CardType.TextAreaPersonalActionPlanCard ||
                    CurrentCardType == CardType.GameCompletedCard ||
                    CurrentCardType == CardType.TransitionUpCard ||
                    CurrentCardType == CardType.TransitionDownCard)
               {
            %>
            <uc3:TextAreaInputCardControl ID="TextAreaIputControl" runat="server" Width="100%" />
            <%
               }
               else if (CurrentCardType == CardType.TextBoxSurveyCard ||
                CurrentCardType == CardType.TextBoxPersonalActionPlanCard)
               {
            %>
            <uc4:TextBoxInputCardControl ID="TextBoxInputControl" runat="server" Width="100%" />
            <%
               }
               else if (CurrentCardType == CardType.MultipleChoiceImageQuestionCard)
               {%>
            <uc14:MultipleChoiceImageQuestionControl ID="MultipleChoiceImageQuestionControl" runat="server"
                Width="100%" />
            <%
               }
               else if (CurrentCardType == CardType.MultipleCheckBoxSurveyCard)
               {%>
            <uc5:MultipleCheckBoxSurveyCardControl ID="SurveyCardMultipleCheckBoxControl" runat="server"
                Width="100%" />
            <%
               }
               else if (CurrentCardType == CardType.MultipleChoiceSurveyCard)
               {
            %>
            <uc6:MultipleChoiceSurveyCardControl ID="SurveryCardMultipleChoiceControl" runat="server"
                Width="100%" />
            <%
               }
               else if (CurrentCardType == CardType.MultipleChoiceAuditStatementCard)
               {
            %>
            <uc7:MultipleChoiceAuditStatementCardControl ID="AuditStatementCardMultipleChoiceControl"
                Width="100%" runat="server" />
            <%                               
               }
               else if (CurrentCardType == CardType.MultipleChoiceAuditStatementImageCard)
               {
            %>
            <uc15:MultipleChoiceAuditStatementImageCardControl ID="AuditStatementImageCardControl"
                Width="100%" runat="server" />
            <%   
               }
               else if (CurrentCardType == CardType.LcdCategoryCard || CurrentCardType == CardType.CompanyPolicyCard || CurrentCardType == CardType.GameIntroCard)
               {
            %>
            <uc8:TextAndImageCardControl ID="ImageAndTextCardControl" ShowImageControls="False" runat="server" Width="100%" />
            <%                
               }
               else if (CurrentCardType == CardType.PriorityIssueVotingCard)
               {
            %>
            <uc9:PriorityVotingCardControl ID="PriorityIssueVotingCardControl" runat="server"
                Width="100%" />
            <%                      
               }
               else if (CurrentCardType == CardType.MultipleChoicePriorityFeedbackCard)
               {
            %>
            <uc10:MultipleChoicePriorityFeedbackCardControl ID="PriorityFeedbackMultipleChoiceCardControl"
                runat="server" Width="100%" />
            <%
                    
               }
               else if (CurrentCardType == CardType.MultipleCheckBoxPriorityFeedbackCard)
               {
            %>
            <uc11:MultipleCheckBoxPriorityFeedbackCardControl ID="PriorityFeedbackMultipleCheckBoxCardControl"
                runat="server" Width="100%" />
            <%                
               }
               else if (CurrentCardType == CardType.MultipleCheckboxPersonalActionPlanCard)
               {
            %>
            <uc12:MultipleCheckBoxPersonalActionPlanControl ID="MultipleCheckBoxPersonalActionPlanCardControl"
                runat="server" />
            <%

               }
               else if (CurrentCardType == CardType.MultipleChoicePersonalActionPlanCard)
               {
            %>
            <uc13:MultipleChoicePersonalActionPlanControl ID="MultipleChoicePersonalActionPlanCardControl"
                runat="server" />
            <% }
               else if (CurrentCardType == CardType.ThemeIntroCard)
               {
            %>
            <uc16:StatementListCardControl ID="StatementListCardControl"
                runat="server" />
            <%
               }
               else if (CurrentCardType == CardType.MatchingListQuestionCard)
               {
            %>
            <uc17:MatchingListCardControl ID="MatchingListCardControl" 
                runat="server" />
            <% } 
               else if (CurrentCardType == CardType.MultiMediaCard)
               {
               %>
             <uc18:TextAreaLocationCardControl ID="TextAreaLocationCardControl" 
                runat="server" />
            <% } 
               else if (CurrentCardType == CardType.MultipleCheckBoxImageQuestionCard)
               {
               %>
            <uc19:MultipleCheckBoxImageCardControl ID="MultipleCheckBoxImageCardControl" 
                runat="server" />
             <% } 
               else if (CurrentCardType == CardType.BranchingCard)
               {
               %>
                 <uc20:BranchingCardControl runat="server" ID="BranchingCardControl" />
            <% } %>
        </td>
    </tr>
</table>
<hr />
<table style="width: 100%">
    <tr>
        <td>
            <h3>
                <asp:Literal ID="AudioNarrativeFrameLiteral" runat="server" Text="Audio Narrative/Transcript" />
            </h3>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table>
                            <td>
                                <dx:ASPxLabel ID="AudioNarrativeMediaLabel" runat="server" Text="Audio Narrative" />
                            </td>
                            <td>
                                <dx:ASPxButton ID="AudioNarrativeMediaBrowseButton" ClientInstanceName="AudioNarrativeMediaBrowseButton"
                                    runat="server" Text="Browse.." AutoPostBack="false">
                                    <ClientSideEvents Click="function(s,e){ ShowUploadWindow('AudioNarrative'); }" />
                                </dx:ASPxButton>
                            </td>
                            <td>&nbsp;&nbsp;
                            </td>
                            <td>
                                <dx:ASPxHyperLink ID="AudioNarrativeMediaHyperLink" runat="server" ClientInstanceName="AudioNarrativeMediaHyperLink"
                                    Target="_blank" Text="" NavigateUrl="#">
                                </dx:ASPxHyperLink>
                            </td>
                            <td>
                                <dx:ASPxHyperLink ID="AudioNarrativeMediaRemoveHyperlink" runat="server" ClientInstanceName="AudioNarrativeMediaRemoveHyperlink"
                                    NavigateUrl="#" Text="Remove" ClientVisible="false">
                                    <ClientSideEvents Click="function(s,e){ AudioNarrativeMediaRemoveHyperlink_Click(s,e);}" />
                                </dx:ASPxHyperLink>
                            </td>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="TranscriptLabel" runat="server" Text="Transcript" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxMemo ID="TranscriptTextMemo" runat="server" NullText="Narrative/Transcript goes here"
                            Rows="10" Width="100%">
                        </dx:ASPxMemo>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<hr />
<table>
    <tr>
        <td>
            <h3>
                <asp:Literal ID="MultimediaFrameLiteral" runat="server" Text="Multimedia Frame" /></h3>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="QuestionMultiMediaFrameLabel" runat="server" Text="Question multimedia frame" />
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="QuestionMultimediaTypeComboBox" runat="server" ClientInstanceName="QuestionMultimediaTypeComboBox"
                            DropDownStyle="DropDownList" ValueType="System.String">
                            <Items>
                                <dx:ListEditItem Value="wistia" Text="WISTIA" />
                                <dx:ListEditItem Value="image" Text="Image" />
                                <dx:ListEditItem Value="document" Text="Document" />
                            </Items>
                            <ClientSideEvents SelectedIndexChanged="QuestionMultimediaTypeComboBox_SelectedIndexChanged" />
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                    <td>
                        <dx:ASPxCheckBox ID="QuestionShowPlaybackControlCheckBox" runat="server" Text="Show Playback controls">
                        </dx:ASPxCheckBox>
                    </td>
                    <td>
                        <dx:ASPxCheckBox ID="QuestionStartMultimediaAutoCheckBox" runat="server" Text="Start Multimedia automatically">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="4">
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td>
                                    <dx:ASPxMemo ID="QuestionMultimediaUrlTextBox" Width="350" Height="50" runat="server" ClientInstanceName="QuestionMultimediaUrlTextBox"
                                        NullText="url goes here">
                                    </dx:ASPxMemo>
                                </td>
                                <td>
                                    <dx:ASPxHyperLink ID="QuestionMultimediaDocumentHyperLink" runat="server" ClientInstanceName="QuestionMultimediaDocumentHyperLink"
                                        Target="_blank" Text="" NavigateUrl="#">
                                    </dx:ASPxHyperLink>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="QuestionMultimediaBrowseButton" ClientInstanceName="QuestionMultimediaBrowseButton"
                                        runat="server" Text="Browse.." AutoPostBack="false">
                                        <ClientSideEvents Click="function(s,e){ ShowUploadWindow('Question'); }" />
                                    </dx:ASPxButton>
                                </td>
                                <td>&nbsp;&nbsp;
                                </td>
                                <td>
                                    <dx:ASPxHyperLink ID="QuestionMultimediaRemoveHyperLink" runat="server" ClientInstanceName="QuestionMultimediaRemoveHyperLink"
                                        NavigateUrl="#" Text="Remove" ClientVisible="false">
                                        <ClientSideEvents Click="function(s,e){ QuestionMultimediaRemoveHyperLink_Click(s,e);}" />
                                    </dx:ASPxHyperLink>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="QuestionMultiMediaThumbnailLabel" runat="server" Text="Question Thumbnail Image" />
                    </td>
                    <td colspan="4">
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td></td>
                                <td>
                                    <dx:ASPxHyperLink ID="QuestionThumbnailHyperLink" runat="server" ClientInstanceName="QuestionThumbnailHyperLink"
                                        Target="_blank" Text="" NavigateUrl="#">
                                    </dx:ASPxHyperLink>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="QuestionThumbnailBrowseButton" ClientInstanceName="QuestionThumbnailBrowseButton"
                                        runat="server" Text="Browse.." AutoPostBack="false">
                                        <ClientSideEvents Click="function(s,e){ ShowUploadWindow('QuestionThumbnail'); }" />
                                    </dx:ASPxButton>
                                </td>
                                <td>&nbsp;&nbsp;
                                </td>
                                <td>
                                    <dx:ASPxHyperLink ID="QuestionThumnailRemoveHyperLink" runat="server" ClientInstanceName="QuestionThumnailRemoveHyperLink"
                                        NavigateUrl="#" Text="Remove" ClientVisible="false">
                                        <ClientSideEvents Click="function(s,e){ QuestionThumnailRemoveHyperLink_Click(s,e);}" />
                                    </dx:ASPxHyperLink>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="AnswerExplationationMediaLabel" runat="server" Text="Answer explanation media" />
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="AnswerMultimediaTypeComboBox" runat="server" ClientInstanceName="AnswerMultimediaTypeComboBox"
                            DropDownStyle="DropDownList" ValueType="System.String">
                            <Items>
                                <dx:ListEditItem Value="wistia" Text="WISTIA" />
                                <dx:ListEditItem Value="image" Text="Image" />
                                <dx:ListEditItem Value="document" Text="Document" />
                            </Items>
                            <ClientSideEvents SelectedIndexChanged="AnswerMultimediaTypeComboBox_SelectedIndexChanged" />
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                    <td>
                        <dx:ASPxCheckBox ID="AnswerShowPlaynackControlCheckBox" runat="server" Text="Show Playback controls">
                        </dx:ASPxCheckBox>
                    </td>
                    <td>
                        <dx:ASPxCheckBox ID="AnswerStartMultimediaAutoCheckBox" runat="server" Text="Start Multimedia automatically">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="4">
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td>
                                    <dx:ASPxMemo ID="AnswerMultimediaUrlTextBox" Width="350" Height="50" runat="server" ClientInstanceName="AnswerMultimediaUrlTextBox"
                                        NullText="url goes here">
                                    </dx:ASPxMemo>
                                </td>
                                <td>
                                    <dx:ASPxHyperLink ID="AnswerMultimediaDocumentHyperLink" runat="server" ClientInstanceName="AnswerMultimediaDocumentHyperLink"
                                        Target="_blank" Text="" NavigateUrl="#">
                                    </dx:ASPxHyperLink>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="AnswerMultimediaBrowseButton" ClientInstanceName="AnswerMultimediaBrowseButton"
                                        runat="server" Text="Browse.." AutoPostBack="false">
                                        <ClientSideEvents Click="function(s,e){ ShowUploadWindow('Answer'); }" />
                                    </dx:ASPxButton>
                                </td>
                                <td>&nbsp;&nbsp;
                                </td>
                                <td>
                                    <dx:ASPxHyperLink ID="AnswerMultimediaRemoveHyperLink" runat="server" ClientInstanceName="AnswerMultimediaRemoveHyperLink"
                                        NavigateUrl="#" Text="Remove" ClientVisible="false">
                                        <ClientSideEvents Click="function(s,e){ AnswerMultimediaRemoveHyperLink_Click(s,e);}" />
                                    </dx:ASPxHyperLink>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Answer Thumbnail Image" />
                    </td>
                    <td colspan="4">
                        <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td></td>
                                <td>
                                    <dx:ASPxHyperLink ID="AnswerThumbnailHyperLink" runat="server" ClientInstanceName="AnswerThumbnailHyperLink"
                                        Target="_blank" Text="" NavigateUrl="#">
                                    </dx:ASPxHyperLink>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="AnswerThumbnailBrowseButton" ClientInstanceName="AnswerThumbnailBrowseButton"
                                        runat="server" Text="Browse.." AutoPostBack="false">
                                        <ClientSideEvents Click="function(s,e){ ShowUploadWindow('AnswerThumbnail'); }" />
                                    </dx:ASPxButton>
                                </td>
                                <td>&nbsp;&nbsp;
                                </td>
                                <td>
                                    <dx:ASPxHyperLink ID="AnswerThumnailRemoveHyperLink" runat="server" ClientInstanceName="AnswerThumnailRemoveHyperLink"
                                        NavigateUrl="#" Text="Remove" ClientVisible="false">
                                        <ClientSideEvents Click="function(s,e){ AnswerThumnailRemoveHyperLink_Click(s,e);}" />
                                    </dx:ASPxHyperLink>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<hr />
<table>
    <tr>
        <td>
            <h3>
                <asp:Literal ID="GameplaySettingsLabel" runat="server" Text="Gameplay Settings" /></h3>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="TimeToAnswerLabel" runat="server" Text="Time to answer" />
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <table style="border-collapse: collapse">
                                        <tr>
                                            <td>
                                                <table style="border-collapse: collapse">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxSpinEdit ID="TimeToAnswerMinuteSpinEdit" runat="server" MinValue="0" MaxValue="59"
                                                                AllowUserInput="false" MaxLength="2" Width="40px">
                                                            </dx:ASPxSpinEdit>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="TimeToAnswerMinuteLabel" runat="server" Text="min" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table style="border-collapse: collapse">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxSpinEdit ID="TimeToAnswerSecondSpinEdit" runat="server" MinValue="0" MaxValue="59"
                                                                AllowUserInput="false" MaxLength="2" Width="40px">
                                                            </dx:ASPxSpinEdit>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxLabel ID="TimeToAnswerSecondLabel" runat="server" Text="sec" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxCheckBox ID="IsATimedCardCheckBox" runat="server" Text="Is a timer capable card">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <dx:ASPxCheckBox ID="IgnoreTimerCheckBox" ClientVisible="False" runat="server" Text="Ignore timer for this card">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <dx:ASPxCheckBox ID="UseCardTimerCheckBox" ClientVisible="False" runat="server" Text="Override game timer">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td style="display: none; border-collapse: collapse;">
                                    <dx:ASPxLabel ID="CurrentPlayerWinLabel" ClientVisible="False" runat="server" Text="Current Player Winning" />
                                </td>
                                <td style="display: none; border-collapse: collapse;"></td>
                                <td style="display: none; border-collapse: collapse;">
                                    <dx:ASPxSpinEdit ID="CurrentPlayerWinSpinEdit" ClientVisible="False" runat="server" Width="40px">
                                    </dx:ASPxSpinEdit>
                                </td>
                                <td style="display: none; border-collapse: collapse;"></td>
                                <td>
                                    <dx:ASPxLabel ID="OnlinePlayerWinLabel" runat="server" Text="Online Player Winning" />
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxSpinEdit ID="OnlinePlayerWinSpinEdit" runat="server" Width="40px">
                                    </dx:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr>
                                <td style="display: none; border-collapse: collapse;">
                                    <dx:ASPxLabel ID="NonCurrentPlayerWinLabel" ClientVisible="False" runat="server" Text="Non-Current Player Winning" />
                                </td>
                                <td style="display: none; border-collapse: collapse;"></td>
                                <td style="display: none; border-collapse: collapse;">
                                    <dx:ASPxSpinEdit ID="NonCurrentPlayerWinSpinEdit" ClientVisible="False" runat="server" Width="40px">
                                    </dx:ASPxSpinEdit>
                                </td>
                                <td style="display: none; border-collapse: collapse;"></td>
                                <td>
                                    <dx:ASPxLabel ID="OnlinePlayerLoseLabel" runat="server" Text="Online Player Losing" />
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxSpinEdit ID="OnlinePlayerLoseSpinEdit" runat="server" Width="40px">
                                    </dx:ASPxSpinEdit>
                                </td>
                            </tr>
                            <tr style="display: none; border-collapse: collapse;">
                                <td>
                                    <dx:ASPxLabel ID="CurrentPlayerLoseLabel" ClientVisible="False" runat="server" Text="Current Player Losing" />
                                </td>
                                <td></td>
                                <td>
                                    <dx:ASPxSpinEdit ID="CurrentPlayerLoseSpinEdit" ClientVisible="False" runat="server" Width="40px">
                                    </dx:ASPxSpinEdit>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="display: none; border-collapse: collapse;">
                                <td>
                                    <dx:ASPxLabel ID="NonCurrentPlayerLoseLabel" ClientVisible="False" runat="server" Text="Non-Current Player Losing" />
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxSpinEdit ID="NonCurrentPlayerLoseSpinEdit" ClientVisible="False" runat="server" Width="40px">
                                    </dx:ASPxSpinEdit>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="7"></td>
                            </tr>
                            <tr style="display: none; border-collapse: collapse;">
                                <td>
                                    <dx:ASPxLabel ID="CategorySelectorLabel" ClientVisible="False" runat="server" Text="Category Selector (LCD)" />
                                </td>
                                <td class="spacer"></td>
                                <td colspan="5">
                                    <dx:ASPxComboBox ID="CategorySelectorComboBox" ClientVisible="False" runat="server">
                                        <Items>
                                            <dx:ListEditItem Text="Flexibility" Selected="false" />
                                        </Items>
                                    </dx:ASPxComboBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<hr />
<table>
    <tr>
        <td colspan="2">
            <h3>
                <asp:Literal ID="NotesLiteral" runat="server" Text="Notes" /></h3>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <dx:ASPxGridView ID="ReferenceMaterialGridView" runat="server" ClientInstanceName="ReferenceMaterialGridView"
                Caption="Reference Material" Width="400px" AutoGenerateColumns="False" DataSourceID="CardReferenceMaterialDataSource"
                KeyFieldName="CardReferenceMaterialID" OnRowInserting="ReferenceMaterialGridView_RowInserting"
                OnRowUpdating="ReferenceMaterialGridView_RowUpdating" OnDetailRowGetButtonVisibility="ReferenceMaterialGridView_DetailRowGetButtonVisibility"
                OnCommandButtonInitialize="ReferenceMaterialGridView_CommandButtonInitialize">
                <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
                <Columns>
                    <dx:GridViewCommandColumn ShowDeleteButton="true" ShowEditButton="true">
                        <HeaderCaptionTemplate>
                            <dx:ASPxHyperLink ID="NewButton" runat="server" Text="New">
                                <ClientSideEvents Click="ReferenceMaterialGridViewNewButton_Click"/>
                            </dx:ASPxHyperLink>
                        </HeaderCaptionTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="CardReferenceMaterialDescription" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="CardReferenceMaterialUrl" VisibleIndex="8">
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="DocumentHyperlink" runat="server" Text="View Document" Target="_blank"
                                OnInit="DocumentHyperLink_Init">
                            </dx:ASPxHyperLink>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <Templates>
                    <EditForm>
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="CardReferenceMaterialDescriptionLabel" runat="server" Text="Ref Material Desc">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="CardReferenceMaterialDescriptionTextBox" runat="server" Text='<%# Eval("CardReferenceMaterialDescription")%>'>
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <table style="border-collapse: collapse">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="CardReferenceMaterialBrowseButton" ClientInstanceName="CardReferenceMaterialBrowseButton"
                                                    runat="server" Text="Attach File" AutoPostBack="false">
                                                    <ClientSideEvents Click="function(s,e){ ShowUploadWindow('CardReferenceMaterial'); }" />
                                                </dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxHyperLink ID="CardReferenceMaterialDocumentHyperLink" runat="server" ClientInstanceName="CardReferenceMaterialDocumentHyperLink"
                                                    Target="_blank" Text="View Document" NavigateUrl="#" ClientVisible='<%# !string.IsNullOrEmpty((string)Eval("CardReferenceMaterialUrl")) %>'>
                                                </dx:ASPxHyperLink>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div id="aspxGridViewUpdateCancelButtonBar">
                            <div id="aspxGridViewCancelButton">
                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                    runat="server" tabindex="19" />
                            </div>
                            &nbsp;&nbsp;
                            <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                runat="server" />
                        </div>
                    </EditForm>
                    <DetailRow>
                        <dx:ASPxGridView ID="DetailReferenceMaterialGridView" runat="server" ClientInstanceName="DetailReferenceMaterialGridView"
                            Width="350px" AutoGenerateColumns="false" DataSourceID="CardLanguageReferenceMaterialDataSource"
                            KeyFieldName="CardReferenceMaterialLanguageID" OnBeforePerformDataSelect="DetailReferenceMaterialGridView_BeforePerformDataSelect"
                            OnRowInserting="DetailReferenceMaterialGridView_RowInserting" OnRowInserted="DetailReferenceMaterialGridView_RowInserted">
                            <Columns>
                                <dx:GridViewCommandColumn VisibleIndex="0" ShowDeleteButton="true">
                                    <HeaderCaptionTemplate>
                                        <dx:ASPxHyperLink ID="NewButton" runat="server" Text="New" OnInit="DetailReferenceMaterialGridViewNewButton_Init">
                                            <ClientSideEvents Click="DetailReferenceMaterialGridViewNewButton_Click"/>
                                        </dx:ASPxHyperLink>
                                    </HeaderCaptionTemplate>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="CardReferenceMaterialDescription" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CardReferenceMaterialUrl" VisibleIndex="2">
                                    <DataItemTemplate>
                                        <dx:ASPxHyperLink ID="DocumentHyperlink" runat="server" Text="View Document" Target="_blank"
                                            OnInit="DocumentHyperLink_Init">
                                        </dx:ASPxHyperLink>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Templates>
                                <EditForm>
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:ASPxLabel ID="DetailReferenceDescriptionLabel" runat="server" Text="Ref Material Desc">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="DetailReferenceDescriptionTextBox" runat="server" Text='<%# Eval("CardReferenceMaterialDescription")%>'>
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <dx:ASPxCheckBox ID="IsDefaultLanguageForReferenceMaterialCheckBox" runat="server"
                                                    Text="Use current language as default language for reference material">
                                                </dx:ASPxCheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <table style="border-collapse: collapse">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="DetailReferenceBrowseButton" ClientInstanceName="DetailReferenceBrowseButton"
                                                                runat="server" Text="Attach File" AutoPostBack="false">
                                                                <ClientSideEvents Click="function(s,e){ ShowUploadWindow('CardReferenceMaterialDetail'); }" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxHyperLink ID="DetailReferenceDocumentHyperLink" runat="server" ClientInstanceName="DetailReferenceDocumentHyperLink"
                                                                Target="_blank" Text="View Document" NavigateUrl="#" ClientVisible='<%# !string.IsNullOrEmpty((string)Eval("CardReferenceMaterialUrl")) %>'>
                                                            </dx:ASPxHyperLink>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="aspxGridViewUpdateCancelButtonBar">
                                        <div id="aspxGridViewCancelButton">
                                            <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                runat="server" tabindex="19" />
                                        </div>
                                        &nbsp;&nbsp;
                                        <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                            runat="server" />
                                    </div>
                                </EditForm>
                            </Templates>
                        </dx:ASPxGridView>
                    </DetailRow>
                </Templates>
            </dx:ASPxGridView>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <dx:ASPxLabel ID="FacilitatorNoteLabel" runat="server" Text="Facilitator Notes" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <dx:ASPxMemo ID="FacilitatorNoteMemo" runat="server" NullText="Offscreen notes here"
                Rows="10" Width="100%">
            </dx:ASPxMemo>
        </td>
    </tr>
</table>
<hr />
<table>
    <tr>
        <td valign="top">
            <dx:ASPxTreeList ID="CardClassificationTreeList" runat="server" ClientInstanceName="CardClassificationTreeList"
                Caption="Card Classifications" Width="250px" AutoGenerateColumns="False" DataSourceID="CardClassificationDataSource"
                DataCacheMode="Disabled" KeyFieldName="CardClassificationGroupID" ParentFieldName="CardClassificationGroupID_Parent"
                OnDataBound="CardClassificationTreeList_DataBound" OnHtmlRowPrepared="CardClassificationTreeList_HtmlRowPrepared"
                OnCustomCallback="CardClassificationTreeList_CustomCallback">
                <Settings ShowColumnHeaders="true" />
                <Border BorderStyle="Solid" />
                <Columns>
                    <dx:TreeListTextColumn FieldName="CardClassificationGroup" Caption="Classification"
                        VisibleIndex="0">
                    </dx:TreeListTextColumn>
                    <dx:TreeListDataColumn Name="IsClassificationSelected" Caption="Use" VisibleIndex="1">
                        <DataCellTemplate>
                            <input name="mycheck" type="checkbox" <%# HiddenNodeList.Contains(Container.NodeKey) ? "checked=\"checked\"" : "" %>
                                <%# Container.Level == 1 ? "style=\"visibility:hidden\"" : ""%> onclick="setHiddenState('<%#Container.NodeKey %>', checked)" />
                            <input type="hidden" name="mycheck_<%#Container.NodeKey %>" value="" />
                        </DataCellTemplate>
                    </dx:TreeListDataColumn>
                    <dx:TreeListDataColumn Name="IsPrimaryCard" Caption="Primary" VisibleIndex="2">
                        <DataCellTemplate>
                            <input type="radio" name="myradio" value="<%#Container.NodeKey %>" <%# PrimaryNodeKey==Container.NodeKey ? "checked=\"checked\"" : "" %>
                                onclick="saveState();" <%# Container.Level == 1 ? "style=\"visibility:hidden\"" : ""%> />
                        </DataCellTemplate>
                    </dx:TreeListDataColumn>
                </Columns>
            </dx:ASPxTreeList>
        </td>
        <td class="spacerlarge"></td>
        <td valign="top">
            <dx:ASPxTreeList ID="CardMetaTagTreeList" runat="server" AutoGenerateColumns="False"
                DataSourceID="CardMetaTagDataSource" Width="250px" DataCacheMode="Disabled" KeyFieldName="CardClassificationGroupID"
                ParentFieldName="CardClassificationGroupID_Parent" Caption="Card Meta Tags" OnDataBound="CardMetaTagTreeList_DataBound">
                <Settings ShowColumnHeaders="false" />
                <Border BorderStyle="Solid" />
                <SettingsSelection Enabled="true" AllowSelectAll="true" />
                <Columns>
                    <dx:TreeListTextColumn FieldName="CardClassificationGroup" Caption="Meta Tag" VisibleIndex="1">
                    </dx:TreeListTextColumn>
                    <dx:TreeListCheckColumn VisibleIndex="8" Visible="false">
                        <DataCellTemplate>
                            <%# Eval("IsAMetaTagClassificationGroup") %>
                        </DataCellTemplate>
                    </dx:TreeListCheckColumn>
                </Columns>
            </dx:ASPxTreeList>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <dx:ASPxButton ID="UpdateButton" runat="server" ClientInstanceName="UpdateButton"
                Text="Update" OnClick="UpdateButton_Click" ClientVisible="False">
            </dx:ASPxButton>
            <dx:ASPxButton runat="server" ID="ValidateButton" ClientInstanceName="ValidateButton" Text="Update" AutoPostBack="False">
                <ClientSideEvents Click="ValidateButton_Click"></ClientSideEvents>
            </dx:ASPxButton>
        </td>
        <td class="spacerlarge"></td>
        <td>
            <dx:ASPxButton ID="CancelButton" runat="server" ClientInstanceName="CancelButton"
                Text="Cancel">
            </dx:ASPxButton>
        </td>
    </tr>
</table>
<asp:LinqDataSource ID="ProductDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="ProductDataSource_Selecting" TableName="Products">
</asp:LinqDataSource>
<asp:LinqDataSource ID="ClientDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="ClientDataSource_Selecting" TableName="Clients">
</asp:LinqDataSource>
<asp:LinqDataSource ID="CardClassificationDataSource" runat="server" OnSelecting="CardClassificationDataSource_Selecting"
    ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext" EntityTypeName=""
    TableName="CardClassificationGroups">
</asp:LinqDataSource>
<asp:LinqDataSource ID="CardMetaTagDataSource" runat="server" OnSelecting="CardMetaTagDataSource_Selecting"
    ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext" EntityTypeName=""
    TableName="CardClassificationGroups">
</asp:LinqDataSource>
<asp:LinqDataSource ID="DefaultLanguageDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="DefaultLanguageDataSource_Selecting" TableName="Languages">
    <SelectParameters>
        <asp:ControlParameter ControlID="ProductComboBox" Type="Int32" Name="ProductID" />
    </SelectParameters>
</asp:LinqDataSource>
<asp:LinqDataSource ID="CardReferenceMaterialDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="CardReferenceMaterialDataSource_Selecting" TableName="CardReferenceMaterial">
</asp:LinqDataSource>
<asp:LinqDataSource ID="CardLanguageReferenceMaterialDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="CardLanguageReferenceMaterialDataSource_Selecting"
    TableName="CardReferenceMaterial_Language">
</asp:LinqDataSource>
<asp:LinqDataSource ID="CardDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="CardDataSource_Selecting" TableName="Cards">
</asp:LinqDataSource>
<asp:LinqDataSource ID="LeadingQuestionCardDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="LeadingQuestionCardDataSource_Selecting" TableName="Cards">
</asp:LinqDataSource>
<asp:LinqDataSource ID="AuditStatementCardDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="AuditStatementCardDataSource_Selecting" TableName="Cards">
</asp:LinqDataSource>
<asp:LinqDataSource ID="AuditStatementImageCardDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="AuditStatementImageCardDataSource_OnSelecting" TableName="Cards">
</asp:LinqDataSource>
