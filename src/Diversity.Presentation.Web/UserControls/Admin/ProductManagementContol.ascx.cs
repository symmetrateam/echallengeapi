﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.Data;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class ProductManagementContol : System.Web.UI.UserControl
    {
        public class DistributorRow
        {
            public int DistributorID { get; set; }
            public string DistributorName { get; set; }
            public bool IsOwner { get; set; }
            public bool IsGranted { get; set; }
            public string QuestionMix { get; set; }
            public int MaxNoOfProductsDistributorCanCreate { get; set; }
            public int CurrentProductCount { get; set; }
            public bool HasAvailableProducts { get; set; }
        }

        private const string PRODUCT_ID_SESSION_KEY = "EditingProductID";

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                EditingProductID = null;
            }
        }

        protected void ProductDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = (from p in dataContext.Products
                            from currentLanguageProduct in dataContext.Product_Languages
                                .Where(d => d.ProductID == p.ProductID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            from pl in p.Product_Languages
                                .Where(d => d.ProductID == p.ProductID && d.LanguageID == p.DefaultLanguageID)
                            select new
                                       {
                                           p.ProductID,
                                           p.DefaultProductTheme,
                                           p.DefaultLanguageID,
                                           DisplayProductName =
                                currentLanguageProduct == null ? pl.ProductName : currentLanguageProduct.ProductName
                                       })
                    .OrderBy(r => r.DisplayProductName);
            }
            else
            {
                e.Result = (from p in dataContext.Products
                            from pd in p.Product2Distributors
                            from currentLanguageProduct in dataContext.Product_Languages
                                .Where(d => d.ProductID == p.ProductID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            from pl in p.Product_Languages
                                .Where(d => d.ProductID == p.ProductID && d.LanguageID == p.DefaultLanguageID)
                            let distributors = dataContext.User2Distributors
                                .Where(d => d.UserID == userId)
                                .Select(d => d.DistributorID)
                            where distributors.Contains(pd.DistributorID)
                            select new
                                       {
                                           p.ProductID,
                                           p.DefaultProductTheme,
                                           p.DefaultLanguageID,
                                           DisplayProductName =
                                currentLanguageProduct == null ? pl.ProductName : currentLanguageProduct.ProductName
                                       })
                    .OrderBy(r => r.DisplayProductName);

            }
        }

        protected void ProductLanguageDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {

            var dataContext = new EChallengeDataContext();

            var lcid = (Page as BasePage).CurrentContext.Culture;

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {

                var results = (from l in dataContext.Product_Languages
                                .Where(c => c.ProductID == EditingProductID)
                              select l)
                              .OrderBy(r=> r.ProductName);

                e.Result = results;
            }
            else
            {
                var distributors = from d in dataContext.User2Distributors
                                   where d.UserID == userId
                                   select d.DistributorID;

                e.Result = (from l in dataContext.Product_Languages
                            join pd in dataContext.Product2Distributors on l.ProductID equals EditingProductID
                            where distributors.Contains(pd.DistributorID)
                            select l)
                            .OrderBy(r=>r.ProductName)
                            .Distinct();
            }

        }


        protected void DistributorAccessGridView_DataBound(object sender, EventArgs e)
        {
            var distributorGrid = sender as ASPxGridView;
            distributorGrid.Selection.UnselectAll();

            for (var i = 0; i < distributorGrid.VisibleRowCount; i++)
            {
                if (distributorGrid.GetRowValues(i, "IsOwner") != null)
                {
                    if (Convert.ToBoolean(distributorGrid.GetRowValues(i, "IsOwner")))
                    {
                        distributorGrid.Selection.SelectRow(i);
                    }                   
                }                
            }
        }

        
        protected void DistributorAccessDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var productId = Session[PRODUCT_ID_SESSION_KEY] == null ? -1 : Convert.ToInt32(Session[PRODUCT_ID_SESSION_KEY]);
            var dataContext = new EChallengeDataContext();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                var result =
                    (from d in dataContext.Distributors
                     from cd in dataContext.Product2Distributors
                         .Where(c => d.DistributorID == c.DistributorID && c.ProductID == productId)
                         .DefaultIfEmpty()
                     orderby d.DistributorName
                     select new
                                {
                                    d.DistributorID,
                                    d.DistributorName,
                                    IsOwner =
                         cd.DistributorIsProductOwner == null ? false : cd.DistributorIsProductOwner,
                                    IsGranted = cd.DistributorID == null ? false : true,
                                    cd.QuestionMix,
                                    d.MaxNoOfProductsDistributorCanCreate
                                }).ToList();
                var items = new List<DistributorRow>();

                foreach (var item in result)
                {
                    var row = new DistributorRow()
                                  {
                                      DistributorID = item.DistributorID,
                                      DistributorName = item.DistributorName,
                                      IsGranted = item.IsGranted,
                                      IsOwner = item.IsOwner,
                                      QuestionMix = item.QuestionMix,
                                      MaxNoOfProductsDistributorCanCreate = item.MaxNoOfProductsDistributorCanCreate
                                  };

                    row.CurrentProductCount =
                        dataContext.Product2Distributors.Count(p => p.DistributorID == row.DistributorID && p.DistributorIsProductOwner);

                    row.HasAvailableProducts = row.MaxNoOfProductsDistributorCanCreate > row.CurrentProductCount;

                    items.Add(row);
                }

                e.Result = items;                
            }
            else
            {
                var result = (from d in dataContext.Distributors
                           from u in d.User2Distributors
                           from cd in dataContext.Product2Distributors
                            .Where(c => d.DistributorID == c.DistributorID && c.ProductID == productId)
                            .DefaultIfEmpty()
                           where u.UserID == userId
                           orderby d.DistributorName
                           select new
                           {
                               d.DistributorID,
                               d.DistributorName,
                               IsOwner = cd.DistributorIsProductOwner == null ? false : cd.DistributorIsProductOwner,
                               IsGranted = cd.DistributorID == null ? false : true,
                               cd.QuestionMix,
                               d.MaxNoOfProductsDistributorCanCreate
                           }).ToList();

                var items = new List<DistributorRow>();

                foreach (var item in result)
                {
                    var row = new DistributorRow()
                    {
                        DistributorID = item.DistributorID,
                        DistributorName = item.DistributorName,
                        IsGranted = item.IsGranted,
                        IsOwner = item.IsOwner,
                        QuestionMix = item.QuestionMix,
                        MaxNoOfProductsDistributorCanCreate = item.MaxNoOfProductsDistributorCanCreate
                    };

                    row.CurrentProductCount =
                        dataContext.Product2Distributors.Count(p => p.DistributorID == row.DistributorID && p.DistributorIsProductOwner);

                    row.HasAvailableProducts = row.MaxNoOfProductsDistributorCanCreate > row.CurrentProductCount;

                    items.Add(row);
                }

                e.Result = items;
            }

        }

        protected void ProductGridView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            EditingProductID = null;

            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = dataContext.Languages.Where(l => l.LCID == lcid).FirstOrDefault();

            if (language == null)
            {
                return;
            }
        }

        protected void ProductGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var productGridView = sender as ASPxGridView;

            var defaultThemeComboBox = productGridView.FindEditFormTemplateControl(@"DefaultThemeComboBox") as ASPxComboBox;


            var productNameTextBox = productGridView.FindEditFormTemplateControl(@"ProductNameTextBox") as ASPxTextBox;

            var productDescriptionTextBox =
                productGridView.FindEditFormTemplateControl(@"ProductDescriptionTextBox") as ASPxTextBox;

            using (var dataContext = new EChallengeDataContext())
            {
                var currentDateTime = DateTime.Now;
                var userId = (Page as BasePage).CurrentContext.UserIdentifier;
                var lcid = (Page as BasePage).CurrentContext.Culture;

                var language = (from l in dataContext.Languages
                                where l.LCID == lcid
                                select l).FirstOrDefault();

                var product = new Product()
                                  {
                                      CreatedOnDateTime = currentDateTime,
                                      IsADeletedRow = false,
                                      IsAnAuditRow = false,
                                      LastModifiedOnDateTime = currentDateTime,
                                      UserID_CreatedBy = userId,
                                      UserID_LastModifiedBy = userId,
                                      DefaultProductTheme = defaultThemeComboBox.SelectedItem == null ? null : defaultThemeComboBox.SelectedItem.Value.ToString(),
                                      DefaultLanguageID = language.LanguageID
                                  };

                var productLanguage = new Product_Language()
                                          {
                                              LanguageID = language.LanguageID,
                                              ProductDescription = productDescriptionTextBox.Text.Trim(),
                                              ProductName = productNameTextBox.Text.Trim(),
                                              UserID_CreatedBy = userId,
                                              UserID_LastModifiedBy = userId,
                                              ProductID = product.ProductID,
                                              CreatedOnDateTime = currentDateTime,
                                              LastModifiedOnDateTime = currentDateTime,
                                          };


                product.Product_Languages.Add(productLanguage);


                var distributorGrid =
                    productGridView.FindEditFormTemplateControl(@"DistributorAccessGridView") as ASPxGridView;

                int start = distributorGrid.PageIndex * distributorGrid.SettingsPager.PageSize;
                int end = (distributorGrid.PageIndex + 1) * distributorGrid.SettingsPager.PageSize;

                var isGrantedColumn = distributorGrid.Columns["IsGranted"] as GridViewDataColumn;
                var questionMixColumn = distributorGrid.Columns["QuestionMix"] as GridViewDataColumn;

                for (var i = start; i < end; i++)
                {
                    var isGrantedCheckBox =
                        distributorGrid.FindRowCellTemplateControl(i, isGrantedColumn, "IsAcessGrantedCheckBox") as
                        ASPxCheckBox;

                    var questionMixComboBox =
                        distributorGrid.FindRowCellTemplateControl(i, questionMixColumn, "QuestionMixComboBox") as
                        ASPxComboBox;

                    var id = Convert.ToInt32(distributorGrid.GetRowValues(i, distributorGrid.KeyFieldName));
                    var isOwner = distributorGrid.Selection.IsRowSelected(i);

                    if (isGrantedCheckBox != null && isGrantedCheckBox.Checked)
                    {
                        var mapping = new Product2Distributor()
                                          {

                                              ProductID = product.ProductID,
                                              CreatedOnDateTime = currentDateTime,
                                              DistributorID = id,
                                              DistributorIsProductOwner = isOwner,
                                              QuestionMix = questionMixComboBox.SelectedItem == null ? null : questionMixComboBox.SelectedItem.Value.ToString(),
                                              LastModifiedOnDateTime = currentDateTime,
                                              UserID_CreatedBy = userId,
                                              UserID_LastModifiedBy = userId
                                          };

                        product.Product2Distributors.Add(mapping);
                    }
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.Products.Add(product);
                        dataContext.SaveChanges();
                        transaction.Commit();
                        e.Cancel = true;
                        productGridView.CancelEdit();
                        productGridView.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }

        protected void ProductLanguageGridView_BeforePerformDataSelect(object sender, EventArgs e)
        {
            EditingProductID = Convert.ToInt32((sender as ASPxGridView).GetMasterRowKeyValue());
        }


        int? EditingProductID
        {
            get
            {
                if (Session[PRODUCT_ID_SESSION_KEY] == null)
                {
                    return null;
                }
                else
                {
                    return (int)Session[PRODUCT_ID_SESSION_KEY];
                }
            }
            set { Session[PRODUCT_ID_SESSION_KEY] = value; }
        }

        protected void ProductLanguageGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var defaultThemeComboBox = gridView.FindEditFormTemplateControl(@"DefaultThemeComboBox") as ASPxComboBox;
            var defaultLanguageComboBox =
                gridView.FindEditFormTemplateControl(@"DefaultLanguageComboBox") as ASPxComboBox;

            var lastModifiedOn = (DateTime) e.OldValues["LastModifiedOnDateTime"];

            using (var dataContext = new EChallengeDataContext())
            {
                var currentDateTime = DateTime.Now;
                var userId = (Page as BasePage).CurrentContext.UserIdentifier;

                var product = dataContext.Products.Where(p => p.ProductID == EditingProductID).FirstOrDefault();
            
                if(lastModifiedOn != product.LastModifiedOnDateTime)
                {
                    throw new Exception(Resources.Exceptions.ConcurrencyException);
                }
                                
                product.DefaultLanguageID = Convert.ToInt32(defaultLanguageComboBox.SelectedItem.Value);
                product.DefaultProductTheme = defaultThemeComboBox.SelectedItem == null
                                                  ? null
                                                  : defaultThemeComboBox.SelectedItem.Value.ToString();
                product.LastModifiedOnDateTime = currentDateTime;
                product.UserID_LastModifiedBy = userId;

                var productLanguageId = (int)e.Keys[0];

                var productLanguage =
                    product.Product_Languages.Where(l => l.Product_LanguageID == productLanguageId).FirstOrDefault();

                if (e.NewValues["ProductName"] != null)
                {
                    productLanguage.ProductName = (e.NewValues["ProductName"] as string).Trim();
                }

                if (e.NewValues["ProductDescription"] != null)
                {
                    productLanguage.ProductDescription = (e.NewValues["ProductDescription"] as string).Trim();
                }

                productLanguage.UserID_LastModifiedBy = userId;
                productLanguage.LastModifiedOnDateTime = currentDateTime;


                var distributorGrid =
                    gridView.FindEditFormTemplateControl(@"DistributorAccessGridView") as ASPxGridView;
                var isGrantedColumn = distributorGrid.Columns["IsGranted"] as GridViewDataColumn;
                var questionMixColumn = distributorGrid.Columns["QuestionMix"] as GridViewDataColumn;

                var product2Distributors = product.Product2Distributors.ToList();
                
                foreach (var product2Distributor in product2Distributors)
                {
                    var grantedCheckbox = distributorGrid.FindRowCellTemplateControlByKey(product2Distributor.DistributorID,
                                                                                  isGrantedColumn,
                                                                                  "IsAcessGrantedCheckBox") as ASPxCheckBox;

                    if (grantedCheckbox != null && grantedCheckbox.Checked && 
                        dataContext.Product2Distributors.Any(
                                p => p.ProductID == product2Distributor.ProductID && p.DistributorID == product2Distributor.DistributorID))
                    {
                        continue;
                    }

                    if(dataContext.DistributorClient2DistributorProducts.Any(d => d.Distributor2ProductID == product2Distributor.Product2DistributorID))
                    {
                        var distributorName =
                            dataContext.Distributors.Where(d => d.DistributorID == product2Distributor.DistributorID).
                                Select(s => s.DistributorName).FirstOrDefault();
                        throw new Exception(string.Format("This distributor {0} is already being used for this product", distributorName));
                    }

                    product.Product2Distributors.Remove(product2Distributor);
                    dataContext.Product2Distributors.Remove(product2Distributor);
                }

                int start = distributorGrid.PageIndex * distributorGrid.SettingsPager.PageSize;
                int end = (distributorGrid.PageIndex + 1) * distributorGrid.SettingsPager.PageSize;                

                for (var i = start; i < end; i++)
                {
                    var isGrantedCheckBox =
                        distributorGrid.FindRowCellTemplateControl(i, isGrantedColumn, "IsAcessGrantedCheckBox") as
                        ASPxCheckBox;

                    var questionMixComboBox =
                        distributorGrid.FindRowCellTemplateControl(i, questionMixColumn, "QuestionMixComboBox") as
                        ASPxComboBox;

                    var id = Convert.ToInt32(distributorGrid.GetRowValues(i, distributorGrid.KeyFieldName));
                    var isOwner = distributorGrid.Selection.IsRowSelected(i);

                    if (isGrantedCheckBox != null && isGrantedCheckBox.Checked)
                    {
                        var existingEntry =
                            dataContext.Product2Distributors.FirstOrDefault(
                                p => p.ProductID == product.ProductID && p.DistributorID == id);

                        if(existingEntry != null)
                        {
                            existingEntry.UserID_LastModifiedBy = userId;
                            existingEntry.LastModifiedOnDateTime = currentDateTime;
                            existingEntry.QuestionMix = questionMixComboBox.SelectedItem == null
                                                            ? null
                                                            : questionMixComboBox.SelectedItem.Value.ToString();
                            existingEntry.DistributorIsProductOwner = isOwner;
                        }
                        else
                        {
                            var mapping = new Product2Distributor()
                            {

                                ProductID = product.ProductID,
                                CreatedOnDateTime = currentDateTime,
                                DistributorID = id,
                                DistributorIsProductOwner = isOwner,
                                QuestionMix = questionMixComboBox.SelectedItem == null ? null : questionMixComboBox.SelectedItem.Value.ToString(),
                                LastModifiedOnDateTime = currentDateTime,
                                UserID_CreatedBy = userId,
                                UserID_LastModifiedBy = userId
                            };

                            product.Product2Distributors.Add(mapping);    
                        }
                    }
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();

                        e.Cancel = true;
                        gridView.CancelEdit();
                        gridView.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }

        protected void ProductLanguageGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var defaultThemeComboBox = gridView.FindEditFormTemplateControl(@"DefaultThemeComboBox") as ASPxComboBox;
            var defaultLanguageComboBox =
                gridView.FindEditFormTemplateControl(@"DefaultLanguageComboBox") as ASPxComboBox;

            using (var dataContext = new EChallengeDataContext())
            {
                var currentDateTime = DateTime.Now;
                var userId = (Page as BasePage).CurrentContext.UserIdentifier;
                var lcid = (Page as BasePage).CurrentContext.Culture;

                var language = (from l in dataContext.Languages
                                where l.LCID == lcid
                                select l).FirstOrDefault();

                var product = dataContext.Products.Where(p => p.ProductID == EditingProductID).FirstOrDefault();
                product.DefaultLanguageID = Convert.ToInt32(defaultLanguageComboBox.SelectedItem.Value);
                product.DefaultProductTheme = defaultThemeComboBox.SelectedItem == null
                                                  ? null
                                                  : defaultThemeComboBox.SelectedItem.Value.ToString();
                product.LastModifiedOnDateTime = currentDateTime;
                product.UserID_LastModifiedBy = userId;


                var productLanguage = new Product_Language()
                {
                    LanguageID = language.LanguageID,
                    ProductDescription = e.NewValues["ProductDescription"] == null ? null : (e.NewValues["ProductDescription"] as string).Trim(),
                    ProductName = e.NewValues["ProductName"] == null ? null : (e.NewValues["ProductName"] as string).Trim(),
                    UserID_CreatedBy = userId,
                    UserID_LastModifiedBy = userId,
                    ProductID = product.ProductID,
                    CreatedOnDateTime = currentDateTime,
                    LastModifiedOnDateTime = currentDateTime,
                };


                product.Product_Languages.Add(productLanguage);

                var product2Distributors = product.Product2Distributors.ToList();

                foreach (var product2Distributor in product2Distributors)
                {
                    product.Product2Distributors.Remove(product2Distributor);
                }

                var distributorGrid =
                    gridView.FindEditFormTemplateControl(@"DistributorAccessGridView") as ASPxGridView;

                int start = distributorGrid.PageIndex * distributorGrid.SettingsPager.PageSize;
                int end = (distributorGrid.PageIndex + 1) * distributorGrid.SettingsPager.PageSize;

                var isGrantedColumn = distributorGrid.Columns["IsGranted"] as GridViewDataColumn;
                var questionMixColumn = distributorGrid.Columns["QuestionMix"] as GridViewDataColumn;

                for (var i = start; i < end; i++)
                {
                    var isGrantedCheckBox =
                        distributorGrid.FindRowCellTemplateControl(i, isGrantedColumn, "IsAcessGrantedCheckBox") as
                        ASPxCheckBox;

                    var questionMixComboBox =
                        distributorGrid.FindRowCellTemplateControl(i, questionMixColumn, "QuestionMixComboBox") as
                        ASPxComboBox;


                    var id = Convert.ToInt32(distributorGrid.GetRowValues(i, distributorGrid.KeyFieldName));
                    var isOwner = distributorGrid.Selection.IsRowSelected(i);

                    if (isGrantedCheckBox != null && isGrantedCheckBox.Checked)
                    {
                        var mapping = new Product2Distributor()
                        {

                            ProductID = product.ProductID,
                            CreatedOnDateTime = currentDateTime,
                            DistributorID = id,
                            DistributorIsProductOwner = isOwner,
                            QuestionMix = questionMixComboBox.SelectedItem == null ? null : questionMixComboBox.SelectedItem.Value.ToString(),
                            LastModifiedOnDateTime = currentDateTime,
                            UserID_CreatedBy = userId,
                            UserID_LastModifiedBy = userId
                        };

                        product.Product2Distributors.Add(mapping);
                    }
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();

                        e.Cancel = true;
                        gridView.CancelEdit();
                        gridView.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }

            }
        }

        protected void ProductLanguageGridView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            using (var dataContext = new EChallengeDataContext())
            {
                var product = dataContext.Products.Where(p => p.ProductID == EditingProductID).FirstOrDefault();
                if (product == null)
                {
                    return;
                }

                var comboBox = gridView.FindEditFormTemplateControl(@"DefaultLanguageComboBox") as ASPxComboBox;

                var item = comboBox.Items.FindByValue(product.DefaultLanguageID);

                if (item != null)
                {
                    item.Selected = true;
                }

                var val = ProductGridView.GetRowValuesByKeyValue(EditingProductID, "DefaultProductTheme") as string;

                var defaultThemeComboBox = gridView.FindEditFormTemplateControl(@"DefaultThemeComboBox") as ASPxComboBox;
                var selectedItem = defaultThemeComboBox.Items.FindByValue(val);
                if (selectedItem != null)
                {
                    selectedItem.Selected = true;
                }

            }
        }

        protected void NewLinkButton_Init(object sender, EventArgs e)
        {
            using (var dataContext = new EChallengeDataContext())
            {
                var lcid = (Page as BasePage).CurrentContext.Culture;
                var language = dataContext.Languages.Where(l => l.LCID == lcid).FirstOrDefault();

                var languageExists = from p in dataContext.Products
                                     from pl in p.Product_Languages
                                     where pl.LanguageID == language.LanguageID && p.ProductID == EditingProductID
                                     select pl;

                var linkButton = sender as LinkButton;
                linkButton.Visible = languageExists.Count() > 0 ? false : true;
            }
        }

        protected void IsAcessGrantedCheckBox_Init(object sender, EventArgs e)
        {
            var checkBox = sender as ASPxCheckBox;
            var container = checkBox.NamingContainer as GridViewDataItemTemplateContainer;
            checkBox.ClientInstanceName = string.Format(@"IsAccessGrantedCheckBox_{0}",
                                                        container.VisibleIndex);
            checkBox.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            var distributorId = (int)container.Grid.GetRowValues(container.VisibleIndex, "DistributorID");
            checkBox.JSProperties["cp_DistributorId"] = distributorId;

        }

        protected void QuestionMixComboBox_Init(object sender, EventArgs e)
        {
            var comboBox = sender as ASPxComboBox;
            var container = comboBox.NamingContainer as GridViewDataItemTemplateContainer;
            comboBox.ClientInstanceName = string.Format(@"QuestionMixComboBox_{0}",
                                                        container.VisibleIndex);
            comboBox.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            var editFormTemplateContainer = container.Grid.Parent as GridViewEditFormTemplateContainer;
            comboBox.ValidationSettings.ValidationGroup = editFormTemplateContainer.ValidationGroup;
        }

        protected void LanguageDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            if (EditingProductID.HasValue)
            {
                var dataContext = new EChallengeDataContext();

                e.Result = (from p in dataContext.Products
                            from pl in p.Product_Languages
                            where p.ProductID == EditingProductID
                            select pl.Language).Distinct();

            }
        }


        protected void DistributorAccessGridView_OnCommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var isOwner = gridView.GetRowValues(e.VisibleIndex, "IsOwner") != null && Convert.ToBoolean(gridView.GetRowValues(e.VisibleIndex, "IsOwner"));

            if (isOwner)
            {
                return;
            }

            var hasAvailableProducts = gridView.GetRowValues(e.VisibleIndex, "HasAvailableProducts") != null && Convert.ToBoolean(gridView.GetRowValues(e.VisibleIndex, "HasAvailableProducts"));

            if (hasAvailableProducts)
            {
                return;
            }

            e.Enabled = false;
        }

        protected void ProductGridView_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            var productGridView = sender as ASPxGridView;
            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;
            var productId = (int)e.Keys[0];

            using (var dataContext = new EChallengeDataContext(userId))
            {
                var product = dataContext.Products.FirstOrDefault(p => p.ProductID == productId);

                if (product == null)
                {
                    throw new Exception(Resources.Exceptions.DeleteFailedRowDoesNotExist);
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.upDeleteProduct(product.ProductID, userId);
                        dataContext.SaveChanges();
                        transaction.Commit();

                        productGridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

            }
        }
    }
}