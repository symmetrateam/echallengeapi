﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using Diversity.Common.Enumerations;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class CardManagementControl
    {
        #region Classes

        public class CardAssociation
        {
            public int AssociatedCardId { get; set; }
            public int CardId { get; set; }
            public bool IsPrimaryAssociation { get; set; }
        }

        #endregion

        #region Constants

        private const string CONTEXTUALLY_SIMILAR_ASSOCIATED_CARD_DATA_KEY = "ContextuallySimilarAssociatedCardData";
        private const string LEADING_QUESTION_ASSOCIATED_CARD_DATA_KEY = "LeadingQuestionAssociatedCardData";
        private const string AUDIT_STATEMENT_ASSOCIATED_CARD_DATA_KEY = "AuditStatementAssociatedCardData";
        private const string AUDIT_STATEMENT_IMAGE_ASSOCIATED_CARD_DATA_KEY = "AuditStatementImageAssociatedCardData";

        #endregion 


        /// <summary>
        /// Contextually similar cards
        /// </summary>
        public List<CardAssociation> ContextuallySimilarAssociatedCardData
        {
            get
            {
                return Session[CONTEXTUALLY_SIMILAR_ASSOCIATED_CARD_DATA_KEY] == null ? null : Session[CONTEXTUALLY_SIMILAR_ASSOCIATED_CARD_DATA_KEY] as List<CardAssociation>;
            }

            set
            {
                Session[CONTEXTUALLY_SIMILAR_ASSOCIATED_CARD_DATA_KEY] = value;
            }
        }

        /// <summary>
        /// Leading question cards
        /// </summary>
        public List<CardAssociation> LeadingQuestionAssociatedCardData
        {
            get
            {
                return Session[LEADING_QUESTION_ASSOCIATED_CARD_DATA_KEY] == null ? null : Session[LEADING_QUESTION_ASSOCIATED_CARD_DATA_KEY] as List<CardAssociation>;
            }

            set
            {
                Session[LEADING_QUESTION_ASSOCIATED_CARD_DATA_KEY] = value;
            }
        }

        /// <summary>
        /// Audit statement cards
        /// </summary>
        public List<CardAssociation> AuditStatementAssociatedCardData
        {
            get
            {
                return Session[AUDIT_STATEMENT_ASSOCIATED_CARD_DATA_KEY] == null ? null : Session[AUDIT_STATEMENT_ASSOCIATED_CARD_DATA_KEY] as List<CardAssociation>;
            }

            set
            {
                Session[AUDIT_STATEMENT_ASSOCIATED_CARD_DATA_KEY] = value;
            }
        }

        /// <summary>
        /// Audit statement image cards
        /// </summary>
        public List<CardAssociation> AuditStatementImageAssociatedCardData
        {
            get
            {
                return Session[AUDIT_STATEMENT_IMAGE_ASSOCIATED_CARD_DATA_KEY] == null ? null : Session[AUDIT_STATEMENT_IMAGE_ASSOCIATED_CARD_DATA_KEY] as List<CardAssociation>;
            }

            set
            {
                Session[AUDIT_STATEMENT_IMAGE_ASSOCIATED_CARD_DATA_KEY] = value;
            }
        }

        /// <summary>
        /// Populates contextually similar cards
        /// </summary>
        public void PopulateContextuallySimilarAssociatedCardData()
        {
            if (CurrentCardId.HasValue)
            {
                var dataContext = new EChallengeDataContext();
                var results = from a in dataContext.AssociatedCards
                              where a.CardID_AssociatedCard == CurrentCardId && !a.IsAssociatedToElseContextuallySimilarTo 
                              select new CardAssociation()
                                         {
                                             AssociatedCardId = a.AssociatedCardID,
                                             CardId = a.CardID,
                                             IsPrimaryAssociation = a.IsPrimaryAssociation
                                         };

                ContextuallySimilarAssociatedCardData = results.ToList();
            }
            else
            {
                ContextuallySimilarAssociatedCardData = new List<CardAssociation>();
            }
        }

        /// <summary>
        /// Populates Leading question associated card data
        /// </summary>
        public void PopulateLeadingQuestionAssociatedCardData()
        {
            if (CurrentCardId.HasValue)
            {
                var results = GetCardAssociations(CardType.LeadingQuestionCard);
                LeadingQuestionAssociatedCardData = results.ToList();
            }
            else
            {
                LeadingQuestionAssociatedCardData = new List<CardAssociation>();
            }
        }


        /// <summary>
        /// Populates Audit State associated card data
        /// </summary>
        public void PopulateAuditStatementAssociatedCardData()
        {
            if (CurrentCardId.HasValue)
            {
                var results = GetCardAssociations(CardType.MultipleChoiceAuditStatementCard);
                AuditStatementAssociatedCardData = results.ToList();
            }
            else
            {
                AuditStatementAssociatedCardData = new List<CardAssociation>();
            }
        }

        /// <summary>
        /// Populates Audit State Image associated card data
        /// </summary>
        public void PopulateAuditStatementImageAssociatedCardData()
        {
            if (CurrentCardId.HasValue)
            {
                var results = GetCardAssociations(CardType.MultipleChoiceAuditStatementImageCard);
                AuditStatementImageAssociatedCardData = results.ToList();
            }
            else
            {
                AuditStatementImageAssociatedCardData = new List<CardAssociation>();
            }
        }

        private IQueryable<CardAssociation> GetCardAssociations(CardType cardType)
        {
            var cardShortCode = Enum.GetName(typeof (CardType), cardType);
            var dataContext = new EChallengeDataContext();
            var results = from c in dataContext.Cards
                              .Where(c => c.Internal_CardType.Internal_CardTypeShortCode == cardShortCode)
                          join a in dataContext.AssociatedCards on c.CardID equals a.CardID
                          where a.CardID_AssociatedCard == CurrentCardId && a.IsAssociatedToElseContextuallySimilarTo
                          select new CardAssociation()
                                     {
                                         AssociatedCardId = a.AssociatedCardID,
                                         CardId = a.CardID,
                                         IsPrimaryAssociation = a.IsPrimaryAssociation
                                     };
            return results;
        }

        int GetNextContextuallySimilarAssociatedCardId()
        {
            var id = -1;

            if (ContextuallySimilarAssociatedCardData.Count > 0)
            {
                var tempId = ContextuallySimilarAssociatedCardData.Min(a => a.AssociatedCardId);

                if (tempId < 0)
                {
                    id = tempId - 1;
                }
            }

            return id;
        }

        int GetNextLeadingQuestionAssociatedCardId()
        {
            var id = -1;

            if (LeadingQuestionAssociatedCardData.Count > 0)
            {
                var tempId = LeadingQuestionAssociatedCardData.Min(a => a.AssociatedCardId);

                if (tempId < 0)
                {
                    id = tempId - 1;
                }
            }

            return id;
        }

        int GetNextAuditStatementAssociatedCardId()
        {
            var id = -1;

            if (AuditStatementAssociatedCardData.Count > 0)
            {
                var tempId = AuditStatementAssociatedCardData.Min(a => a.AssociatedCardId);

                if (tempId < 0)
                {
                    id = tempId - 1;
                }
            }

            return id;
        }

        int GetNextAuditStatementImageAssociatedCardId()
        {
            var id = -1;

            if (AuditStatementImageAssociatedCardData.Count > 0)
            {
                var tempId = AuditStatementImageAssociatedCardData.Min(a => a.AssociatedCardId);

                if (tempId < 0)
                {
                    id = tempId - 1;
                }
            }

            return id;
        }


        protected void CardDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var associatedCards = ContextuallySimilarAssociatedCardData.Select(c => c.CardId);

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                var output = (from p in dataContext.Cards
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            let showEdit = (from product in dataContext.Products
                                            from productLang in
                                                product.Product_Languages.Where(l => l.LanguageID == language.LanguageID)
                                            where p.ProductID == product.ProductID
                                            select productLang).Any()
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)
                            let selected = associatedCards.Contains(p.CardID)
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library =
                     p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                     currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                     currentLanguageCard == null
                         ? pl.DetailedCardDescription
                         : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,                              
                                ShowEdit = showEdit,
                                Selected = selected
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.Internal_CardTypeName,
                                              o.Internal_CardTypeShortCode,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                                string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32((CardType)Enum.Parse(typeof(CardType), o.Internal_CardTypeShortCode))),
                                              o.ShowEdit,
                                              o.Selected
                                          }
                              );

                e.Result = results;
            }
            else
            {
                var output = (from p in dataContext.Cards
                            from pd in p.Product.Product2Distributors
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)
                            let selected = associatedCards.Contains(p.CardID)
                            let distributors = dataContext.User2Distributors
                                .Where(d => d.UserID == userId)
                                .Select(d => d.DistributorID)
                            where distributors.Contains(pd.DistributorID)                               
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library =
                     p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                     currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                     currentLanguageCard == null
                         ? pl.DetailedCardDescription
                         : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,                     
                                ShowEdit = currentLanguageCard == null ? false : true,
                                Selected = selected
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.Internal_CardTypeName,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType), o.Internal_CardTypeShortCode))),
                                              o.ShowEdit,
                                              o.Selected
                                          });

                e.Result = results;
            }
        }

        protected void LeadingQuestionCardDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var associatedCards = LeadingQuestionAssociatedCardData.Select(c => c.CardId);
            var leadingQuestionCardShortCode = Enum.GetName(typeof (CardType), CardType.LeadingQuestionCard);

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                var output = (from p in dataContext.Cards
                                  .Where(c=>c.Internal_CardType.Internal_CardTypeShortCode == leadingQuestionCardShortCode)
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            let showEdit = (from product in dataContext.Products
                                            from productLang in
                                                product.Product_Languages.Where(l => l.LanguageID == language.LanguageID)
                                            where p.ProductID == product.ProductID
                                            select productLang).Any()
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)
                            let selected = associatedCards.Contains(p.CardID)
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library =
                     p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                     currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                     currentLanguageCard == null
                         ? pl.DetailedCardDescription
                         : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,
                                ShowEdit = showEdit,
                                Selected = selected
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.Internal_CardTypeName,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType), o.Internal_CardTypeShortCode))),
                                              o.ShowEdit,
                                              o.Selected
                                          });

                e.Result = results;
            }
            else
            {
                var output = (from p in dataContext.Cards
                              .Where(c => c.Internal_CardType.Internal_CardTypeShortCode == leadingQuestionCardShortCode)
                            from pd in p.Product.Product2Distributors
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)                            
                            let selected = associatedCards.Contains(p.CardID)
                            let distributors = dataContext.User2Distributors
                                .Where(d => d.UserID == userId)
                                .Select(d => d.DistributorID)
                            where distributors.Contains(pd.DistributorID)
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library =
                     p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                     currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                     currentLanguageCard == null
                         ? pl.DetailedCardDescription
                         : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,
                                ShowEdit = currentLanguageCard == null ? false : true,
                                Selected = selected
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.Internal_CardTypeName,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType), o.Internal_CardTypeShortCode))),
                                              o.ShowEdit,
                                              o.Selected
                                          });

                e.Result = results;
            }
        }


        protected void AuditStatementCardDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var associatedCards = AuditStatementAssociatedCardData.Select(c => c.CardId);
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementCard);

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                var output = (from p in dataContext.Cards
                            .Where(c => c.Internal_CardType.Internal_CardTypeShortCode == cardShortCode)
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            let showEdit = (from product in dataContext.Products
                                            from productLang in
                                                product.Product_Languages.Where(l => l.LanguageID == language.LanguageID)
                                            where p.ProductID == product.ProductID
                                            select productLang).Count() > 0
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)
                            let selected = associatedCards.Contains(p.CardID)
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library =
                     p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                     currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                     currentLanguageCard == null
                         ? pl.DetailedCardDescription
                         : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,
                                ShowEdit = showEdit,
                                Selected = selected
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.Internal_CardTypeName,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType), o.Internal_CardTypeShortCode))),
                                              o.ShowEdit,
                                              o.Selected
                                          }
                              );

                e.Result = results;
            }
            else
            {
                var output = (from p in dataContext.Cards
                            .Where(c => c.Internal_CardType.Internal_CardTypeShortCode == cardShortCode)
                            from pd in p.Product.Product2Distributors
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)
                            let selected = associatedCards.Contains(p.CardID)
                            let distributors = dataContext.User2Distributors
                                .Where(d => d.UserID == userId)
                                .Select(d => d.DistributorID)
                            where distributors.Contains(pd.DistributorID)                                
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library =
                     p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                     currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                     currentLanguageCard == null
                         ? pl.DetailedCardDescription
                         : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,                     
                                ShowEdit = currentLanguageCard == null ? false : true,
                                Selected = selected
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.Internal_CardTypeName,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType),
                                                                o.Internal_CardTypeShortCode))),
                                              o.ShowEdit,
                                              o.Selected
                                          });

                e.Result = results;
            }
        }


        protected void AuditStatementImageCardDataSource_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var associatedCards = AuditStatementImageAssociatedCardData.Select(c => c.CardId);
            var cardShortCode = Enum.GetName(typeof(CardType), CardType.MultipleChoiceAuditStatementImageCard);

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                var output = (from p in dataContext.Cards
                            .Where(c => c.Internal_CardType.Internal_CardTypeShortCode == cardShortCode)
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            let showEdit = (from product in dataContext.Products
                                            from productLang in
                                                product.Product_Languages.Where(l => l.LanguageID == language.LanguageID)
                                            where p.ProductID == product.ProductID
                                            select productLang).Any()
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)
                            let selected = associatedCards.Contains(p.CardID)
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library =
                     p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                     currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                     currentLanguageCard == null
                         ? pl.DetailedCardDescription
                         : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,                               
                                ShowEdit = showEdit,
                                Selected = selected
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.Internal_CardTypeName,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType), o.Internal_CardTypeShortCode))),
                                              o.ShowEdit,
                                              o.Selected
                                          });

                e.Result = results;
            }
            else
            {
                var output = (from p in dataContext.Cards
                            .Where(c => c.Internal_CardType.Internal_CardTypeShortCode == cardShortCode)
                            from pd in p.Product.Product2Distributors
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)
                            let selected = associatedCards.Contains(p.CardID)
                            let distributors = dataContext.User2Distributors
                                .Where(d => d.UserID == userId)
                                .Select(d => d.DistributorID)
                            where distributors.Contains(pd.DistributorID)
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library =
                     p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                     currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                     currentLanguageCard == null
                         ? pl.DetailedCardDescription
                         : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,
                                ShowEdit = currentLanguageCard == null ? false : true,
                                Selected = selected
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.Internal_CardTypeName,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType), o.Internal_CardTypeShortCode))),
                                              o.ShowEdit,
                                              o.Selected
                                          });

                e.Result = results;
            }
        }


        protected void AssociatedCardGridView_DataBound(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var primaryAssociationCard = ContextuallySimilarAssociatedCardData.FirstOrDefault(a => a.IsPrimaryAssociation);

            if (primaryAssociationCard != null)
            {
                gridView.Selection.SelectRowByKey(primaryAssociationCard.CardId);
            }
        }

        protected void LeadingQuestionAssociatedCardGridView_DataBound(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var primaryAssociationCard = LeadingQuestionAssociatedCardData.FirstOrDefault(a => a.IsPrimaryAssociation);

            if (primaryAssociationCard != null)
            {
                gridView.Selection.SelectRowByKey(primaryAssociationCard.CardId);
            }
        }

        protected void AuditStatementAssociatedCardGridView_DataBound(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var primaryAssociationCard = AuditStatementAssociatedCardData.FirstOrDefault(a => a.IsPrimaryAssociation);

            if (primaryAssociationCard != null)
            {
                gridView.Selection.SelectRowByKey(primaryAssociationCard.CardId);
            }
        }

        protected void AuditStatementImageAssociatedCardGridView_OnDataBound(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var primaryAssociationCard = AuditStatementImageAssociatedCardData.FirstOrDefault(a => a.IsPrimaryAssociation);

            if (primaryAssociationCard != null)
            {
                gridView.Selection.SelectRowByKey(primaryAssociationCard.CardId);
            }
        }


        protected void AssociatedCardCallback_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
        {
            if (e.Parameter.Length > 0)
            {
                var parameters = e.Parameter.Split('|');
                var mode = parameters[0];

                switch (mode)
                {
                    case "selected":
                    case "checked":
                        var cardId = Convert.ToInt32(parameters[1]);
                        var isChecked = Convert.ToBoolean(parameters[2]);

                        var associatedCard = ContextuallySimilarAssociatedCardData.Where(c => c.CardId == cardId).FirstOrDefault();

                        if (isChecked)
                        {
                            if (associatedCard == null)
                            {
                                var association = new CardAssociation()
                                {
                                    AssociatedCardId = GetNextContextuallySimilarAssociatedCardId(),
                                    CardId = cardId
                                };

                                if (string.Compare("selected", mode, true) == 0)
                                {
                                    association.IsPrimaryAssociation = true;

                                    var currentPrimaryAssociation =
                                        ContextuallySimilarAssociatedCardData.Where(a => a.IsPrimaryAssociation).FirstOrDefault();
                                    if (currentPrimaryAssociation != null)
                                    {
                                        currentPrimaryAssociation.IsPrimaryAssociation = false;
                                    }
                                }
                                else
                                {
                                    association.IsPrimaryAssociation = false;
                                }

                                ContextuallySimilarAssociatedCardData.Add(association);
                            }
                            else
                            {
                                if (string.Compare("selected", mode, true) == 0)
                                {
                                    var currentPrimaryAssociation =
                                        ContextuallySimilarAssociatedCardData.Where(a => a.IsPrimaryAssociation).FirstOrDefault();
                                    if (currentPrimaryAssociation != null)
                                    {
                                        currentPrimaryAssociation.IsPrimaryAssociation = false;
                                    }

                                    associatedCard.IsPrimaryAssociation = true;
                                }
                            }
                        }
                        else
                        {
                            if (associatedCard != null)
                            {
                                ContextuallySimilarAssociatedCardData.Remove(associatedCard);
                            }
                        }
                        break;

                    case "reset":
                        PopulateContextuallySimilarAssociatedCardData();
                        AssociatedCardGridView.DataBind();
                        break;
                }
            }
        }

        protected void LeadingQuestionAssociatedCardCallback_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
        {
            if (e.Parameter.Length > 0)
            {
                var parameters = e.Parameter.Split('|');
                var mode = parameters[0];

                switch (mode)
                {
                    case "selected":
                    case "checked":
                        var cardId = Convert.ToInt32(parameters[1]);
                        var isChecked = Convert.ToBoolean(parameters[2]);

                        var associatedCard = LeadingQuestionAssociatedCardData.Where(c => c.CardId == cardId).FirstOrDefault();

                        if (isChecked)
                        {
                            if (associatedCard == null)
                            {
                                var association = new CardAssociation()
                                {
                                    AssociatedCardId = GetNextLeadingQuestionAssociatedCardId(),
                                    CardId = cardId
                                };

                                if (string.Compare("selected", mode, true) == 0)
                                {
                                    association.IsPrimaryAssociation = true;

                                    var currentPrimaryAssociation =
                                        LeadingQuestionAssociatedCardData.Where(a => a.IsPrimaryAssociation).FirstOrDefault();
                                    if (currentPrimaryAssociation != null)
                                    {
                                        currentPrimaryAssociation.IsPrimaryAssociation = false;
                                    }
                                }
                                else
                                {
                                    association.IsPrimaryAssociation = false;
                                }

                                LeadingQuestionAssociatedCardData.Add(association);
                            }
                            else
                            {
                                if (string.Compare("selected", mode, true) == 0)
                                {
                                    var currentPrimaryAssociation =
                                        LeadingQuestionAssociatedCardData.Where(a => a.IsPrimaryAssociation).FirstOrDefault();
                                    if (currentPrimaryAssociation != null)
                                    {
                                        currentPrimaryAssociation.IsPrimaryAssociation = false;
                                    }

                                    associatedCard.IsPrimaryAssociation = true;
                                }
                            }
                        }
                        else
                        {
                            if (associatedCard != null)
                            {
                                LeadingQuestionAssociatedCardData.Remove(associatedCard);
                            }
                        }
                        break;

                    case "reset":
                        PopulateLeadingQuestionAssociatedCardData();
                        LeadingQuestionAssociatedCardGridView.DataBind();
                        break;
                }
            }
        }


        protected void AuditStatementAssociatedCardCallback_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
        {
            if (e.Parameter.Length > 0)
            {
                var parameters = e.Parameter.Split('|');
                var mode = parameters[0];

                switch (mode)
                {
                    case "selected":
                    case "checked":
                        var cardId = Convert.ToInt32(parameters[1]);
                        var isChecked = Convert.ToBoolean(parameters[2]);

                        var associatedCard = AuditStatementAssociatedCardData.Where(c => c.CardId == cardId).FirstOrDefault();

                        if (isChecked)
                        {
                            if (associatedCard == null)
                            {
                                var association = new CardAssociation()
                                {
                                    AssociatedCardId = GetNextAuditStatementAssociatedCardId(),
                                    CardId = cardId
                                };

                                if (string.Compare("selected", mode, true) == 0)
                                {
                                    association.IsPrimaryAssociation = true;

                                    var currentPrimaryAssociation =
                                        AuditStatementAssociatedCardData.Where(a => a.IsPrimaryAssociation).FirstOrDefault();
                                    if (currentPrimaryAssociation != null)
                                    {
                                        currentPrimaryAssociation.IsPrimaryAssociation = false;
                                    }
                                }
                                else
                                {
                                    association.IsPrimaryAssociation = false;
                                }

                                AuditStatementAssociatedCardData.Add(association);
                            }
                            else
                            {
                                if (string.Compare("selected", mode, true) == 0)
                                {
                                    var currentPrimaryAssociation =
                                        AuditStatementAssociatedCardData.Where(a => a.IsPrimaryAssociation).FirstOrDefault();
                                    if (currentPrimaryAssociation != null)
                                    {
                                        currentPrimaryAssociation.IsPrimaryAssociation = false;
                                    }

                                    associatedCard.IsPrimaryAssociation = true;
                                }
                            }
                        }
                        else
                        {
                            if (associatedCard != null)
                            {
                                AuditStatementAssociatedCardData.Remove(associatedCard);
                            }
                        }
                        break;

                    case "reset":
                        PopulateAuditStatementAssociatedCardData();
                        AuditStatementAssociatedCardGridView.DataBind();
                        break;
                }
            }
        }


        protected void AuditStatementImageAssociatedCardCallback_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
        {
            if (e.Parameter.Length > 0)
            {
                var parameters = e.Parameter.Split('|');
                var mode = parameters[0];

                switch (mode)
                {
                    case "selected":
                    case "checked":
                        var cardId = Convert.ToInt32(parameters[1]);
                        var isChecked = Convert.ToBoolean(parameters[2]);

                        var associatedCard = AuditStatementImageAssociatedCardData.FirstOrDefault(c => c.CardId == cardId);

                        if (isChecked)
                        {
                            if (associatedCard == null)
                            {
                                var association = new CardAssociation()
                                {
                                    AssociatedCardId = GetNextAuditStatementImageAssociatedCardId(),
                                    CardId = cardId
                                };

                                if (string.Compare("selected", mode, true) == 0)
                                {
                                    association.IsPrimaryAssociation = true;

                                    var currentPrimaryAssociation =
                                        AuditStatementImageAssociatedCardData.FirstOrDefault(a => a.IsPrimaryAssociation);
                                    if (currentPrimaryAssociation != null)
                                    {
                                        currentPrimaryAssociation.IsPrimaryAssociation = false;
                                    }
                                }
                                else
                                {
                                    association.IsPrimaryAssociation = false;
                                }

                                AuditStatementImageAssociatedCardData.Add(association);
                            }
                            else
                            {
                                if (string.Compare("selected", mode, true) == 0)
                                {
                                    var currentPrimaryAssociation =
                                        AuditStatementImageAssociatedCardData.FirstOrDefault(a => a.IsPrimaryAssociation);
                                    if (currentPrimaryAssociation != null)
                                    {
                                        currentPrimaryAssociation.IsPrimaryAssociation = false;
                                    }

                                    associatedCard.IsPrimaryAssociation = true;
                                }
                            }
                        }
                        else
                        {
                            if (associatedCard != null)
                            {
                                AuditStatementImageAssociatedCardData.Remove(associatedCard);
                            }
                        }
                        break;

                    case "reset":
                        PopulateAuditStatementImageAssociatedCardData();
                        AuditStatementImageAssociatedCardGridView.DataBind();
                        break;
                }
            }
        }


        protected void AssociatedCardGridView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            gridview.DataBind();
        }

        protected void LeadingQuestionAssociatedCardGridView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            gridview.DataBind();
        }

        protected void AuditStatementAssociatedCardGridView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            gridview.DataBind();
        }

        protected void SelectCheckBox_Load(object sender, EventArgs e)
        {
            var cb = sender as ASPxCheckBox;
            var container = cb.NamingContainer as GridViewDataItemTemplateContainer;
            var cardId = (int)container.KeyValue;

            cb.ClientInstanceName = string.Format(@"SelectCheckBox_{0}", container.VisibleIndex);
            cb.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            cb.JSProperties["cp_CardId"] = cardId;
            cb.JSProperties["cp_ClientInstanceName"] = cb.ClientInstanceName;
            cb.Checked = ContextuallySimilarAssociatedCardData.Any(c => c.CardId == cardId);
        }

        protected void LeadingQuestionSelectCheckBox_Load(object sender, EventArgs e)
        {
            var cb = sender as ASPxCheckBox;
            var container = cb.NamingContainer as GridViewDataItemTemplateContainer;
            var cardId = (int)container.KeyValue;

            cb.ClientInstanceName = string.Format(@"LeadingQuestionSelectCheckBox_{0}", container.VisibleIndex);
            cb.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            cb.JSProperties["cp_CardId"] = cardId;
            cb.JSProperties["cp_ClientInstanceName"] = cb.ClientInstanceName;
            cb.Checked = LeadingQuestionAssociatedCardData.Any(c => c.CardId == cardId);
        }


        protected void AuditStatementSelectCheckBox_Load(object sender, EventArgs e)
        {
            var cb = sender as ASPxCheckBox;
            var container = cb.NamingContainer as GridViewDataItemTemplateContainer;
            var cardId = (int)container.KeyValue;

            cb.ClientInstanceName = string.Format(@"AuditStatementSelectCheckBox_{0}", container.VisibleIndex);
            cb.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            cb.JSProperties["cp_CardId"] = cardId;
            cb.JSProperties["cp_ClientInstanceName"] = cb.ClientInstanceName;
            cb.Checked = AuditStatementAssociatedCardData.Any(c => c.CardId == cardId);
        }


        protected void AuditStatementImageSelectCheckBox_Load(object sender, EventArgs e)
        {
            var cb = sender as ASPxCheckBox;
            var container = cb.NamingContainer as GridViewDataItemTemplateContainer;
            var cardId = (int)container.KeyValue;

            cb.ClientInstanceName = string.Format(@"AuditStatementImageSelectCheckBox_{0}", container.VisibleIndex);
            cb.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            cb.JSProperties["cp_CardId"] = cardId;
            cb.JSProperties["cp_ClientInstanceName"] = cb.ClientInstanceName;
            cb.Checked = AuditStatementAssociatedCardData.Any(c => c.CardId == cardId);
        }
    }
}