﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameListControl.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.Admin.GameListControl" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>






<script id="dxis_CardManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/GameListControl.js") %>'>
</script>
<dx:ASPxLoadingPanel ID="LoadingPanel" ClientInstanceName="LoadingPanel" runat="server"></dx:ASPxLoadingPanel>

<dx:ASPxPopupControl ID="DeleteNotificationPopupControl" ClientInstanceName="DeleteNotificationPopupControl" runat="server"
    CloseAction="CloseButton" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Confirm" AllowDragging="true" EnableAnimation="false" Width="350px" EnableViewState="false" ShowOnPageLoad="False">
    <ClientSideEvents PopUp="function(s){s.UpdatePosition();}" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel runat="server" Text="Deleting this game will delete all the played games as well as the user results associated to those games. Are you sure you want to delete this game?" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="border-collapse: collapse;">
                            <tr>
                                <td>
                                    <dx:ASPxButton runat="server" AutoPostBack="False" ID="DeleteGameButton" ClientInstanceName="DeleteGameButton" Text="Delete">
                                        <ClientSideEvents Click="DeleteGameButton_Click"></ClientSideEvents>
                                    </dx:ASPxButton>
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxButton runat="server" AutoPostBack="False" ID="CancelDeleteGameButton" ClientInstanceName="CancelDeleteGameButton" Text="Cancel">
                                        <ClientSideEvents Click="CancelDeleteGameButton_Click"></ClientSideEvents>
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
<dx:ASPxCallback ID="DeleteGameCallback" runat="server" ClientInstanceName="DeleteGameCallback" OnCallback="DeleteGameCallback_OnCallback">
    <ClientSideEvents CallbackComplete="DeleteGameCallback_CallbackComplete"></ClientSideEvents>
</dx:ASPxCallback>

<dx:ASPxCallback ID="EditGameCallback" runat="server" ClientInstanceName="EditGameCallback" OnCallback="EditGameCallback_OnCallback">
</dx:ASPxCallback>
<dx:ASPxCallback ID="GameStatusCallback" ClientInstanceName="GameStatusCallback" OnCallback="GameStatusCallback_OnCallback" runat="server"></dx:ASPxCallback>

<dx:ASPxGridView ID="GameGridView" ClientInstanceName="GameGridView" runat="server" AutoGenerateColumns="False" Width="925px"
    DataSourceID="GameDataSource" KeyFieldName="GameID">
    <ClientSideEvents CustomButtonClick="CustomButtonClick"></ClientSideEvents>
    <SettingsBehavior ColumnResizeMode="NextColumn" EnableRowHotTrack="True" />
    <SettingsPager AlwaysShowPager="True">
        <AllButton Visible="True">
            <Image ToolTip="Show All Products">
            </Image>
        </AllButton>
    </SettingsPager>
    <Settings UseFixedTableLayout="True" ShowFilterRow="True" ShowGroupPanel="True" />
    <Columns>
        <dx:GridViewCommandColumn VisibleIndex="0" Width="100" ShowClearFilterButton="True">            
            <CustomButtons>
                <dx:GridViewCommandColumnCustomButton ID="EditButton" Text="Edit">
                </dx:GridViewCommandColumnCustomButton>
                <dx:GridViewCommandColumnCustomButton ID="DeleteButton" Text="Delete">
                </dx:GridViewCommandColumnCustomButton>
                <dx:GridViewCommandColumnCustomButton ID="StatusButton" Text="Status">
                </dx:GridViewCommandColumnCustomButton>
            </CustomButtons>
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn FieldName="GameID" Caption="Game ID" ReadOnly="True" VisibleIndex="1">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="GameTemplateNameForDashboard" Caption="Template Name" VisibleIndex="2">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataComboBoxColumn FieldName="ProductName" Caption="Product" VisibleIndex="3">
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataCheckColumn FieldName="GameNowLockedDown" Caption="Locked" VisibleIndex="4">
        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataProgressBarColumn FieldName="GameProgress" VisibleIndex="5">
            <DataItemTemplate>
                <dx:ASPxProgressBar ID="GameProgressProgressBar" runat="server" CustomDisplayFormat="" OnInit="GameProgressProgressBar_OnInit" Value='<%# Eval("GameProgress") %>' Width="100%">
                </dx:ASPxProgressBar>
            </DataItemTemplate>
        </dx:GridViewDataProgressBarColumn>
        <dx:GridViewDataCheckColumn FieldName="ShowEdit" VisibleIndex="6" Visible="false" ReadOnly="true">
        </dx:GridViewDataCheckColumn>
    </Columns>
</dx:ASPxGridView>
<asp:LinqDataSource ID="GameDataSource" runat="server"
    ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" TableName="Games" OnSelecting="GameDataSource_Selecting">
</asp:LinqDataSource>

<asp:LinqDataSource ID="ProductDataSource" runat="server">
</asp:LinqDataSource>


