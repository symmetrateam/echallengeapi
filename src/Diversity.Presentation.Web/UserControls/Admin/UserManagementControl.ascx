﻿<%@ control language="C#" autoeventwireup="true" codebehind="UserManagementControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.UserManagementControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>




<%@ register src="../DualListBoxControl.ascx" tagname="DualListBoxControl" tagprefix="uc1" %>




<script id="dxis_UserManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/UserManagementControl.js") %>'>

</script>
<asp:linqdatasource id="UserDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" tablename="Users" enableinsert="true" onselecting="UserDataSource_Selecting">
</asp:linqdatasource>
<dx:ASPxLoadingPanel ID="LoadingPanel" Modal="True" ClientInstanceName="LoadingPanel" runat="server"></dx:ASPxLoadingPanel>
 <dx:aspxpopupcontrol id="NotificationPopupControl" clientinstancename="NotificationPopupControl"
        showclosebutton="True" popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter"
        closeaction="CloseButton" modal="True" runat="server" ShowOnPageLoad="False">
        <contentcollection>
            <dx:popupcontrolcontentcontrol>
                <div>
                    <p>
                        <dx:aspxlabel runat="server" id="PopupLabel" clientinstancename="PopupLabel" />
                    </p>
                </div>
            </dx:popupcontrolcontentcontrol>
        </contentcollection>
    </dx:aspxpopupcontrol>
<dx:ASPxPopupControl id="DeleteNotificationPopupControl" clientinstancename="DeleteNotificationPopupControl" runat="server"
    closeaction="CloseButton" modal="True" popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter"
    headertext="Confirm" allowdragging="true" enableanimation="false" enableviewstate="false" showonpageload="False">
    <ClientSideEvents PopUp="function(s){s.UpdatePosition();}" />
    <ContentCollection>        
        <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Deleting this user will delete all the games, results and invites for this user. Are you sure you want to delete this user?"/>
                    </td>
                </tr>
                <tr>
                    <td >
                         <table style="border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton runat="server" AutoPostBack="False" ID="DeleteGameButton" ClientInstanceName="DeleteGameButton" Text="Delete">
                                <ClientSideEvents Click="DeleteUserButton_Click"></ClientSideEvents>
                            </dx:ASPxButton>                   
                                            </td>
                                            <td class="spacer"></td>
                                            <td>
                                                <dx:ASPxButton runat="server" AutoPostBack="False" ID="CancelDeleteGameButton" ClientInstanceName="CancelDeleteGameButton" Text="Cancel">
                                <ClientSideEvents Click="CancelDeleteUserButton_Click"></ClientSideEvents>
                            </dx:ASPxButton>               
                                                </td>
                                        </tr>
                                    </table>
                    </td>
                </tr>
            </table>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
<dx:aspxcallback id="DeleteUserCallback" runat="server" clientinstancename="DeleteUserCallback" oncallback="DeleteUserCallback_OnCallback">
    <ClientSideEvents CallbackComplete="DeleteUserCallback_CallbackComplete" ></ClientSideEvents>
</dx:aspxcallback>
<dx:aspxgridview id="UserGridView" runat="server" autogeneratecolumns="False" datasourceid="UserDataSource"
    clientinstancename="UserGridView" keyfieldname="UserId" width="100%" oncustombuttoninitialize="UserGridView_CustomButtonInitialize"
    oncustombuttoncallback="UserGridView_CustomButtonCallback" onstartrowediting="UserGridView_StartRowEditing"
    onrowinserting="UserGridView_RowInserting" onrowupdating="UserGridView_RowUpdating"
    oncancelrowediting="UserGridView_CancelRowEditing" oninitnewrow="UserGridView_InitNewRow">
    <columns>
        <dx:gridviewcommandcolumn visibleindex="0" caption=" " buttontype="Link" ShowEditButton="true" ShowNewButton="false" ShowClearFilterButton="True">
            <headercaptiontemplate>
                <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="AddNewRow(UserGridView);return false;" text="New"/>
            </headercaptiontemplate>
        </dx:gridviewcommandcolumn>
        <dx:GridViewDataColumn VisibleIndex="1">
            <DataItemTemplate>
                <dx:ASPxHyperLink runat="server" ID="DeleteButton" Text="Delete" OnInit="DeleteButton_OnInit">
                </dx:ASPxHyperLink>
            </DataItemTemplate>
        </dx:GridViewDataColumn>
        <dx:gridviewcommandcolumn caption=" " buttontype="Image" visibleindex="2">
            <custombuttons>
                <dx:gridviewcommandcolumncustombutton id="ActivateUserButton" visibility="AllDataRows"
                    text="Activate User Account">
                    <image url="~/App_Themes/Aqua/Icons/Tick.png" height="16" width="16" />
                </dx:gridviewcommandcolumncustombutton>
                <dx:gridviewcommandcolumncustombutton id="DeActivateUserButton" visibility="AllDataRows"
                    text="De-Activate User Account">
                    <image url="~/App_Themes/Aqua/Icons/Cross.png" height="16" width="16" />
                </dx:gridviewcommandcolumncustombutton>
                <dx:gridviewcommandcolumncustombutton id="UnlockUserButton" visibility="AllDataRows"
                    text="Unlock User Account">
                    <image url="~/App_Themes/Aqua/Icons/Unlock.png" height="16" width="16" />
                </dx:gridviewcommandcolumncustombutton>
            </custombuttons>
        </dx:gridviewcommandcolumn>
        <dx:gridviewcommandcolumn buttontype="Link" visible="false" visibleindex="3" width="100px"/>
        <dx:gridviewdatatextcolumn fieldname="UserId" readonly="True" visible="false">
            <editformsettings visible="False" />
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="UserName" visibleindex="3">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn caption="User" visibleindex="4">
            <dataitemtemplate>
                <%# Eval("UserFirstName") + " " + Eval("UserLastName") %>
            </dataitemtemplate>
        </dx:gridviewdatatextcolumn>
    </columns>
    <settingsbehavior enablerowhottrack="True" />
    <settingspager alwaysshowpager="True">
        <allbutton visible="True">
            <image tooltip="Show All Users">
            </image>
        </allbutton>
    </settingspager>
    <settings showfilterrow="True" showgrouppanel="True" />
    <styles>
        <alternatingrow enabled="True">
        </alternatingrow>
    </styles>
    <templates>
        <editform>
            <dx:aspxvalidationsummary id="ValidationSummary" runat="server" rendermode="BulletedList"
                width="250px" clientinstancename="ValidationSummary" showerroraslink="false"
                validationgroup="dxValidationGroup">
            </dx:aspxvalidationsummary>
            <dx:aspxpagecontrol id="UserPageControl" runat="server" activetabindex="0" clientinstancename="UserPageControl">
                <tabpages>
                    <dx:tabpage text="Personal Details">
                        <contentcollection>
                            <dx:contentcontrol id="PersonalDetailContentControl" runat="server">
                                <table id="tblContainer0">
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="FirstNameLabel" runat="server" text="First name" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="FirstNameTextBox" runat="server" text='<%# Eval("UserFirstName") %>'
                                                validationsettings-validationgroup="dxValidationGroup">
                                                <validationsettings setfocusonerror="true" errordisplaymode="ImageWithTooltip">
                                                    <requiredfield isrequired="true" errortext="First name is a required field" />
                                                </validationsettings>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="LastNameLabel" runat="server" text="Last name" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="LastNameTextBox" runat="server" text='<%# Eval("UserLastName") %>'
                                                validationsettings-validationgroup="dxValidationGroup">
                                                <validationsettings setfocusonerror="true" errordisplaymode="ImageWithTooltip">
                                                    <requiredfield isrequired="true" errortext="Last name is a required field" />
                                                </validationsettings>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="DOBLabel" runat="server" text="Date of Birth" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxdateedit id="DOBDateEdit" runat="server" value='<%# Eval("UserDateOfBirth") %>'>
                                            </dx:aspxdateedit>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <dx:aspxlabel id="GenderLabel" runat="server" text="Gender" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxradiobuttonlist id="GenderRadioButtonList" runat="server" valuetype="System.String"
                                                repeatdirection="Horizontal" value='<%# Eval("UserGender") %>'>
                                                <items>
                                                    <dx:listedititem text="Male" value="M" />
                                                    <dx:listedititem text="Female" value="F" />
                                                </items>
                                            </dx:aspxradiobuttonlist>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="PersonalEmailLabel" runat="server" text="Personal Email - (Login email)" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="PersonalEmailTextBox" runat="server" text='<% #Eval("UserPersonalEmail1") %>'
                                                validationsettings-validationgroup="dxValidationGroup">
                                                <validationsettings setfocusonerror="True" errordisplaymode="ImageWithTooltip">
                                                    <requiredfield isrequired="True" errortext="Personal email address is a required field" />
                                                    <regularexpression validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                        errortext="Invalid personal email address" />
                                                </validationsettings>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="PersonalMobileLabel" runat="server" text="Mobile" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="PersonalMobileTextBox" runat="server" text='<%# Eval("UserPersonalMobile") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                </table>
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage text="Employment Details">
                        <contentcollection>
                            <dx:contentcontrol id="EmploymentDetailContentControl" runat="server">
                                <table id="tblContainer1">
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="JobTitleLabel" runat="server" text="Job Title" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="JobTitleTextBox" runat="server" text='<%#Eval("ClientJobTitle") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                        <td class="spacerlarge">
                                        </td>
                                        <td>
                                            <dx:aspxlabel id="CompanyLabel" runat="server" text="Company" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxcombobox id="CompanyComboBox" runat="server" dropdownstyle="DropDownList"
                                                valuetype="System.Int32" valuefield="ClientID" textfield="ClientName" datasourceid="ClientDataSource"
                                                ondatabound="CompanyComboBox_DataBound">
                                            </dx:aspxcombobox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="SalaryNoLabel" runat="server" text="Salary No" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="SalaryNoTextBox" runat="server" text='<%#Eval("ClientUniqueSalaryNo") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                        <td class="spacerlarge">
                                        </td>
                                        <td>
                                            <dx:aspxlabel id="SubsidiaryLabel" runat="server" text="Subsidiary" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="SubsidiaryTextBox" runat="server" text='<%# Eval("ClientSubsidiaryCompanyName") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="WorkEmailLabel" runat="server" text="Work Email" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="WorkEmailTextBox" runat="server" text='<%#Eval("UserWorkEmail") %>'
                                                validationsettings-validationgroup="dxValidationGroup">
                                                <validationsettings setfocusonerror="true" errordisplaymode="ImageWithTooltip">
                                                    <regularexpression validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                        errortext="Invalid work email address" />
                                                </validationsettings>
                                            </dx:aspxtextbox>
                                        </td>
                                        <td class="spacerlarge">
                                        </td>
                                        <td>
                                            <dx:aspxlabel id="BusinessUnitLabel" runat="server" text="Business Unit" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="BusinessUnitTextBox" runat="server" text='<%# Eval("ClientBusinessUnit") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="WorkTel1Label" runat="server" text="Tel 1" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="WorkTel1TextBox" runat="server" text='<%#Eval("UserWorkTel1") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                        <td class="spacerlarge">
                                        </td>
                                        <td>
                                            <dx:aspxlabel id="LocationLabel" runat="server" text="Location" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="LocationTextBox" runat="server" text='<%# Eval("ClientLocation") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="WorkTel2Label" runat="server" text="Tel 2" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="WorkTel2TextBox" runat="server" text='<%#Eval("UserWorkTel2") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                        <td class="spacerlarge">
                                        </td>
                                        <td>
                                            <dx:aspxlabel id="CostCentreLabel" runat="server" text="Cost Centre" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="CostCentreTextBox" runat="server" text='<%# Eval("ClientCostCentre") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="WorkMobileLabel" runat="server" text="Mobile" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="WorkMobileTextBox" runat="server" text='<%#Eval("UserWorkMobile") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                        <td class="spacerlarge">
                                        </td>
                                        <td>
                                            <dx:aspxlabel id="DivisionLabel" runat="server" text="Division" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="DivisionTextBox" runat="server" text='<%# Eval("ClientDivision") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                        </td>
                                        <td class="spacerlarge">
                                        </td>
                                        <td>
                                            <dx:aspxlabel id="UserGroupLabel" runat="server" text="User group" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="UserGroupTextBox" runat="server" text='<%# Eval("ClientUserGroup") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">
                                            <dx:aspxmemo id="WorkNotesMemo" runat="server" text='<%# Eval("UserWorkNotes") %>'
                                                rows="5" width="100%" nulltext="Brief note about this user - work">
                                            </dx:aspxmemo>
                                        </td>
                                    </tr>
                                </table>
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage text="Security">
                        <contentcollection>
                            <dx:contentcontrol id="SecurityContentControl" runat="server">
                                <table id="tblContainer2" width="100%">
                                    <tr>
                                        <td valign="top">
                                            <dx:aspxlabel id="DistributorAccessLabel" runat="server" text="Distributor Access" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxlistbox id="DistributorAccessListBox" runat="server" selectionmode="CheckColumn"
                                                datasourceid="DistributorDataSource" valuetype="System.Int32" width="250px" textfield="DistributorName"
                                                valuefield="DistributorID" height="210px" ondatabound="DistributorAccessListBox_DataBound"
                                                validationsettings-validationgroup="dxValidationGroup">                                                
                                                <columns>
                                                    <dx:listboxcolumn fieldname="DistributorName" caption="Distributor" width="100%" />
                                                </columns>
                                            </dx:aspxlistbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="ApplyTemplateLabel" runat="server" text="Apply role group template" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxcombobox id="DefaultRolesGroupComboBox" runat="server" dropdownstyle="DropDownList"
                                                datasourceid="RolesGroupDataSource">
                                                <clientsideevents selectedindexchanged="function(s,e){OnRoleGroupSelectedIndexChanged(s,e);}" />
                                            </dx:aspxcombobox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <dx:aspxcallbackpanel id="RolesDualListBoxCallbackPanel" runat="server" clientinstancename="RolesDualListBoxCallbackPanel"
                                                width="500px" oncallback="RolesDualListBoxCallbackPanel_Callback">
                                                <panelcollection>
                                                    <dx:panelcontent>
                                                        <uc1:duallistboxcontrol id="RolesDualListBox" runat="server" width="500px" height="100%"
                                                            oninit="RolesDualListBox_Init" />
                                                    </dx:panelcontent>
                                                </panelcollection>
                                            </dx:aspxcallbackpanel>
                                        </td>
                                    </tr>
                                </table>
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage text="Avatar Details">
                        <contentcollection>
                            <dx:contentcontrol id="Contentcontrol1" runat="server">
                                <table id="Table1">
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="Aspxlabel2" runat="server" text="Avatar" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="AvatarDataTextBox" runat="server" text='<%# Eval("PlayerAvatarData") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:aspxlabel id="Aspxlabel3" runat="server" text="Avatar Name" />
                                        </td>
                                        <td class="spacer">
                                        </td>
                                        <td>
                                            <dx:aspxtextbox id="AvatarNameTextBox" runat="server" text='<%# Eval("PlayerAvatarName") %>'>
                                            </dx:aspxtextbox>
                                        </td>
                                    </tr>
                                </table>
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
            <br />
            <div id="aspxGridViewUpdateCancelButtonBar">
                <div id="aspxGridViewCancelButton">
                    <dx:aspxgridviewtemplatereplacement id="CancelButton" replacementtype="EditFormCancelButton"
                        runat="server" columnid="" tabindex="19" />
                </div>
                &nbsp;&nbsp;
                <asp:linkbutton id="UpdateLinkButton" runat="server" onclientclick="OnUpdateButtonClick();return false;"
                    text="Update"></asp:linkbutton>
            </div>
            <dx:aspxdateedit id="LastModifiedOnDateTimeDateEdit" runat="server" clientvisible="false"
                value='<%#Bind("LastModifiedOnDateTime") %>' readonly="true">
            </dx:aspxdateedit>
        </editform>
    </templates>
</dx:aspxgridview>
<asp:objectdatasource id="RolesGroupDataSource" runat="server" typename="Diversity.Presentation.Web.UserControls.Admin.UserManagementControl"
    selectmethod="GetAllDefaultRoleGroups"></asp:objectdatasource>
<asp:linqdatasource id="DistributorDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" onselecting="DistributorDataSource_Selecting" tablename="Distributors">
</asp:linqdatasource>
<asp:linqdatasource id="ClientDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" onselecting="ClientDataSource_Selecting" orderby="ClientName"
    tablename="Clients">
</asp:linqdatasource>
