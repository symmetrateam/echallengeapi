﻿function UserUploadControl_FileUploadStart(s, e) {
    UploadButton.SetEnabled(false);
}

function UserUploadControl_TextChanged(s, e) {
    UpdateUploadButton();
}

function UserUploadControl_FileUploadComplete(s, e) {
    UpdateUploadButton();
    
    if (e.isValid) {
        var params = e.callbackData.split(';');

        var valid = params[0];
        var invalid = params[1];

        UploadMessagePopupControl.Show();

        NumValidUserLabel.SetText(valid);
        NumInvalidUserLabel.SetText(invalid);

        UserExceptionGridView.PerformCallback();
        ValidUsersGridView.PerformCallback();
    }
}

function UpdateUploadButton(){
    UploadButton.SetEnabled(UserUploadControl.GetText(0) != "");
}

function UploadButton_Click(s, e) {
    UserUploadControl.Upload();
}

function UserExceptionGridView_EndCallback(s, e) {
    SaveButton.SetEnabled(s.GetVisibleRowsOnPage() <= 0);
    ValidUsersGridView.PerformCallback();
}

function UploadMessageOkButton_Click(s, e) {
    UploadMessagePopupControl.Hide();   
}

function SaveButton_Click(s, e) {
    var isValidated = ASPxClientEdit.ValidateGroup('SaveGroup');
    if (isValidated) {
        SaveCallback.PerformCallback();
        LoadingPanel.Show();
    }
}

function SaveCallback_CallbackComplete(s, e) {
        LoadingPanel.Hide();
        SaveButton.SetEnabled(false);

        if (e.result == null) {
            SetInformationSuccessLabelVisible(true);
            UserExceptionGridView.PerformCallback();
            ValidUsersGridView.PerformCallback();
        }
        else {
            SetInformationSuccessLabelVisible(false);
        }

        InfoMessagePopupControl.Show();
}

function OkButton_Click(s, e) {
    InfoMessagePopupControl.Hide();
}

function SetInformationSuccessLabelVisible(visible) {
    SuccessLabel.SetVisible(visible);
    FailLabel.SetVisible(!visible);
}
