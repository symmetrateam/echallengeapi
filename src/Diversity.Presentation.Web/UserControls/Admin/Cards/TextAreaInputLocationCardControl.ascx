﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextAreaInputLocationCardControl.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.TextAreaInputLocationCardControl" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ register src="../Cards/TextAreaInputCardControl.ascx" tagname="TextAreaControl"
    tagprefix="txtAreaInput" %>


<table width='<%=Width%>'>
    <tr>
        <td>
            <txtAreaInput:TextAreaControl ID="TextAreaControl" runat="server" />        
        </td>        
    </tr>
    <tr>
        <table>
            <tr>
                <td>
                <dx:ASPxLabel id="LocationLiteral" runat="server" text="Text Location" />
            </td>
            <td class="spacerlarge"></td>
            <td>
                <dx:ASPxComboBox ID="LocationComboBox" runat="server">
                <Items>
                    <dx:ListEditItem Text="Left" Value="Left"/>
                    <dx:ListEditItem Text="Right" Value="Right"/>
                    <dx:ListEditItem Text="Top" Value="Top"/>
                    <dx:ListEditItem Text="Bottom" Value="Bottom"/>
                    <dx:ListEditItem Text="Overlay" Value="Overlay"/>
                </Items>
            </dx:ASPxComboBox>
            </td>
            </tr>                        
        </table>
    </tr>
</table>