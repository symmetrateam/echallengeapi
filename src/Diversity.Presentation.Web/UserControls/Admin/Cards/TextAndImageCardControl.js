﻿
function UpControl_FileUploadComplete(s, e) {
    var url = e.callbackData.split(";");

    if (url != 'undefined' || url.length > 0) {

        ImageHyperLink.SetNavigateUrl(url[0]);
        ImageHyperLink.SetText(url[1]);
        TextImageHiddenField.Set("ImageHyperLinkText", url[1]);
        TextImageHiddenField.Set("ImageHyperLinkNavigateUrl", url[0]);
        ImageLocationTextBox.SetVisible(false);
        BrowseButton.SetVisible(false);        
        RemoveHyperLink.SetVisible(true);
    }
    UpPopupControl.Hide();
}


function UpPopupControl_Closing(s, e) {
    UpControl.ClearText();
    UpButton.SetEnabled(UpControl.GetText(0) != "");
}

function UpControl_TextChanged(s, e) {
    UpButton.SetEnabled(UpControl.GetText(0) != "");
}

function ShowUpWindow() {    
    UpPopupControl.Show();
}

function UpButton_Click(s, e) {
    UpControl.Upload();
}

function RemoveHyperLink_Click(s, e) {
    ImageHyperLink.SetNavigateUrl("#");
    ImageHyperLink.SetText("");
    TextImageHiddenField.Remove("ImageLinkText");
    TextImageHiddenField.Remove("ImageHyperNavigateUrl");
    ImageLocationTextBox.SetVisible(true);
    BrowseButton.SetVisible(true);
    s.SetVisible(false);
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}
