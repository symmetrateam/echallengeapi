﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextBoxInputCardControl.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.TextBoxInputCardControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register TagPrefix="uc2" TagName="BasicHtmlEditorControl" Src="~/UserControls/BasicHtmlEditorControl.ascx" %>
<table style="width: 100%;">
    <tr>
        <td>
            <h3>
                <asp:literal id="QuestionLiteral" runat="server" text="Question" /></h3>
        </td>
    </tr>
    <tr>
        <td>
            <uc2:BasicHtmlEditorControl ID="QuestionLongVersionHtmlControl" runat="server" />
        </td>
    </tr>
</table>