﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class PriorityVotingCardControl : BaseCardControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }        

        public void Populate(Card card)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var questionCard = card.QuestionCard;

            if (questionCard == null)
            {
                return;
            }

            QuestionTopicRadioButton.Checked = questionCard.UsePrimaryTopics;
            PriorityListRadioButton.Checked = !questionCard.UsePrimaryTopics;

            SelectTopRadioButton.Checked = questionCard.TopNSelected > 0 ? true : false;
            ArrangePriorityRadioButton.Checked = questionCard.TopNSelected > 0 ? false : true;

            TopPrioritiesSpinEdit.ClientEnabled = questionCard.TopNSelected > 0;
            TopPrioritiesSpinEdit.Number = questionCard.TopNSelected;


            //populate the answers
            var answers = from q in questionCard.QuestionCard_GeneralAnswers
                          orderby q.SequenceNo
                          select q;

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var items = new List<EditListItem>();

            foreach (var answer in answers)
            {
                var answerLanguages = (from a in answer.QuestionCard_GeneralAnswer_Languages
                                       where a.LanguageID == languageId
                                       select a).FirstOrDefault();

                if (answerLanguages != null)
                {
                    items.Add(
                        new EditListItem
                        {
                            Id = answer.QuestionCard_GeneralAnswerID.ToString(),                            
                            Text = answerLanguages.PossibleAnswer_ShortVersion
                        }
                        );
                }
            }

            PriorityEditListControl.Items = items;

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();

            if (questionCardLanguage == null)
            {
                return;
            }

            QuestionShortVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_ShortVersion);
        }

        public int TopNSelected
        {
            get { return TopPrioritiesSpinEdit.Value == null ? 0 : Convert.ToInt32(TopPrioritiesSpinEdit.Value); }
        }

        public bool UsePrimaryTopics
        {
            get { return QuestionTopicRadioButton.Checked; }
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;

            var card = new QuestionCard_Language
            {
                Question_Statement_LongVersion = QuestionShortVersionHtmlControl.GetHtml(),
                Question_Statement_ShortVersion = QuestionShortVersionHtmlControl.GetHtml(),
                UserID_CreatedBy = userId,
                CreatedOnDateTime = currentDateTime,
                UserID_LastModifiedBy = userId,
                LastModifiedOnDateTime = currentDateTime
            };

            return card;
        }

        public List<QuestionCard_GeneralAnswer> GetGeneralAnswers()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var answers = new List<QuestionCard_GeneralAnswer>();

            if (PriorityListRadioButton.Checked)
            {
                var possibleAnswers = PriorityEditListControl.Items;

                for (var i = 0; i < possibleAnswers.Count; i++)
                {
                    var answer = new QuestionCard_GeneralAnswer
                                     {
                                         CreatedOnDateTime = currentDateTime,
                                         LastModifiedOnDateTime = currentDateTime,
                                         UserID_CreatedBy = userId,
                                         UserID_LastModifiedBy = userId,
                                         SequenceNo = i,
                                         IsCorrectAnswer = false
                                     };

                    if (!string.IsNullOrEmpty(possibleAnswers[i].Id))
                    {
                        answer.QuestionCard_GeneralAnswerID = Convert.ToInt32(possibleAnswers[i].Id);
                    }

                    var answerLanguage = new QuestionCard_GeneralAnswer_Language
                                             {
                                                 UserID_CreatedBy = userId,
                                                 CreatedOnDateTime = currentDateTime,
                                                 UserID_LastModifiedBy = userId,
                                                 LastModifiedOnDateTime = currentDateTime,
                                                 QuestionCard_GeneralAnswerID = answer.QuestionCard_GeneralAnswerID,
                                                 LanguageID = languageId,
                                                 PossibleAnswer_ShortVersion = possibleAnswers[i].Text
                                             };

                    answer.QuestionCard_GeneralAnswer_Languages.Add(answerLanguage);
                    answers.Add(answer);
                }
            }

            return answers;
        }

        public bool IsValid(out List<string> errors)
        {
            errors = new List<string>();
            if(TopNSelected > PriorityEditListControl.Items.Count)
            {
                errors.Add("The number of votes must be less than or equal to the number of items.");
                return false;
            }

            return true;
        }
    }
}