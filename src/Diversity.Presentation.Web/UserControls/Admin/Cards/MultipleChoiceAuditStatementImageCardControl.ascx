﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultipleChoiceAuditStatementImageCardControl.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.MultipleChoiceAuditStatementImageCardControl" %>

<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>

<%@ register src="../../MultiSelectEditListControl.ascx" tagname="EditListControl"
    tagprefix="uc1" %>



<%@ Register TagPrefix="uc2" TagName="BasicHtmlEditorControl" Src="~/UserControls/BasicHtmlEditorControl.ascx" %>
<script id="dxis_PriorityVotingCardControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/Cards/MultipleChoiceAuditStatementImageCardControl.js") %>'>
</script>
<table width='<%=Width%>'>
    <tr>
        <td>
            <dx:aspxpagecontrol id="QuestionPageControl" runat="server" activetabindex="0" width="100%">
                <tabpages>
                    <dx:tabpage name="ShortQuestionTab" text="Short Version of Question">
                        <contentcollection>
                            <dx:contentcontrol ID="Contentcontrol1" runat="server">
                                  <uc2:BasicHtmlEditorControl ID="QuestionShortVersionHtmlControl" runat="server" />    
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage name="LongQuestionTab" text="Long Version of Question">
                        <contentcollection>
                            <dx:contentcontrol ID="Contentcontrol2" runat="server">
                                <uc2:BasicHtmlEditorControl ID="QuestionLongVersionHtmlControl" runat="server" />   
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxhyperlink id="InsertDefaultAnswersHyperlink" runat="server" clientinstancename="InsertDefaultAnswersHyperlink"
                text="Add Default Possible Answers" cursor="pointer">
                <clientsideevents click="InsertDefaultAnswersHyperlink_Click" />
            </dx:aspxhyperlink>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxcallbackpanel id="PossibleAnswersCallbackPanel" runat="server" clientinstancename="PossibleAnswersCallbackPanel"
                width="100%" oncallback="PossibleAnswersCallbackPanel_Callback">
                <panelcollection>
                    <dx:panelcontent>
                        <uc1:editlistcontrol id="PossibleAnswerEditListControl" runat="server" allowsinglerowselectonly="true"
                            showtitlepanel="true" titletext="Possible Answers" longversioncolumncaption="Long version"
                            shortversioncolumncaption="Short version" ShowMultimediaColumn="True" MultimediaColumnCaption="Image" />
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxcallbackpanel>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>    
                    <td>
                        <dx:aspxcheckbox id="ShowCompanyResultsCheckBox" runat="server" text="Show Company Results" >
                            <ClientSideEvents CheckedChanged="function(s, e) { MinDataPointsSpinEdit.SetEnabled(s.GetChecked()); }" />     
                        </dx:aspxcheckbox>            
                    </td>               
                    <td>
                        <dx:ASPxLabel ID="MinDataPointsLabel" runat="server" Text="Minimum Data Points for Company Results" />
                    </td>
                    <td class="spacer"></td>
                    <td>
                        <dx:ASPxSpinEdit ID="MinDataPointsSpinEdit"  ClientInstanceName="MinDataPointsSpinEdit" runat="server" MinValue="1" MaxValue="999" NumberType="Integer"
                                                                AllowUserInput="true" MaxLength="3" Width="50px">
                                                            </dx:ASPxSpinEdit>
                    </td>
                    <td class="spacer"></td>
                     <td>
                        <dx:aspxcheckbox id="OfferOptOutCheckBox" runat="server" text="Offer Opt out choice">
                        </dx:aspxcheckbox>            
                    </td>                    
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxpagecontrol id="ExplanationPageControl" runat="server" activetabindex="0"
                width="100%">
                <tabpages>
                    <dx:tabpage name="ShortExplanationTab" text="Short Explanation of Answer">
                        <contentcollection>
                            <dx:contentcontrol ID="Contentcontrol3" runat="server">
                                < <uc2:BasicHtmlEditorControl ID="AnswerExplainationShortVersionHtmlEditor" runat="server" />  
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage name="LongExplanationTab" text="Long Explanation of Answer">
                        <contentcollection>
                            <dx:contentcontrol ID="Contentcontrol4" runat="server">
                                <<uc2:BasicHtmlEditorControl ID="AnswerExplainationLongVersionHtmlEditor" runat="server" />    
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
     </tr>
</table>
