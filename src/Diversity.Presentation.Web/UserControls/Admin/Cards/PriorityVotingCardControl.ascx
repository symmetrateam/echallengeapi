﻿<%@ control language="C#" autoeventwireup="true" codebehind="PriorityVotingCardControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.PriorityVotingCardControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register src="../../EditListControl.ascx" tagname="EditListControl" tagprefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="BasicHtmlEditorControl" Src="~/UserControls/BasicHtmlEditorControl.ascx" %>
<script id="dxis_PriorityVotingCardControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/Cards/PriorityVotingCardControl.js") %>'>
</script>
<table width='<%=Width%>'>
    <tr>
        <td>
             <uc1:BasicHtmlEditorControl ID="QuestionShortVersionHtmlControl" runat="server" />    
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxradiobutton id="QuestionTopicRadioButton" runat="server" clientinstancename="QuestionTopicRadioButton" groupname="PriorityCardTypeGroup" checked="true"
                text="Use question card primary topics">                
            </dx:aspxradiobutton>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxradiobutton id="PriorityListRadioButton" runat="server" clientinstancename="PriorityListRadioButton" groupname="PriorityCardTypeGroup"
                text="Use the following list of priorities">
            </dx:aspxradiobutton>
        </td>
    </tr>
    <tr>
        <td>
            <uc1:editlistcontrol id="PriorityEditListControl" runat="server" textcolumncaption="Statement" width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            <table style="border-collapse:collapse;">
                <tr>
                    <td>
                        <dx:aspxradiobutton id="SelectTopRadioButton" runat="server" clientinstancename="SelectTopRadioButton" groupname="PriorityAnswerTypeGroup" checked="true"
                            text="Select top">
                            <clientsideevents checkedchanged="SelectTopRadioButton_CheckChanged" />
                        </dx:aspxradiobutton>
                    </td>
                    <td>
                        <dx:aspxspinedit id="TopPrioritiesSpinEdit" runat="server" clientinstancename="TopPrioritiesSpinEdit" width="40px" 
                            number="1" minvalue="1" maxvalue="100" numbertype="Integer" >
                        </dx:aspxspinedit>
                    </td>
                    <td>
                        <dx:aspxlabel id="SelectTopLabel" runat="server" clientinstancename="SelectTopLabel" text="priorities" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxradiobutton id="ArrangePriorityRadioButton" ClientEnabled="False" runat="server" clientinstancename="ArrangePriorityRadioButton" groupname="PriorityAnswerTypeGroup"
                 text="Arrange the priorty list">
            </dx:aspxradiobutton>
        </td>
    </tr>
</table>
