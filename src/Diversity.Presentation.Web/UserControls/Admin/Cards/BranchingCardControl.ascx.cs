﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Browsing;
using DevExpress.Utils;
using DevExpress.Web;
using Diversity.Common;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class BranchingCardControl : BaseCardControl
    {
        public const string SessionItemsKey = "7E95EA87-02E0-41EC-827F-EFBDB85430CF";
        public const string OriginalButtonMultiMediaListKey = "948188D7-AB34-4BEF-97CD-12075C290EC5";
        private const string CallbackArgumentFormat = "function (s, e) {{ GridCallback.PerformCallback(\"{0}|{1}|\" + {2}); }}"; // key | field | value

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Items = new List<BranchingCardButtonListItem>();
            }

            base.OnInit(e);
        }

        public List<BranchingCardButtonListItem> Items
        {
            get
            {
                if (Session[SessionItemsKey] == null)
                {
                    Session[SessionItemsKey] = new List<BranchingCardButtonListItem>();
                }

                return Session[SessionItemsKey] as List<BranchingCardButtonListItem>;
            }
            set
            {
                Session[SessionItemsKey] = value;
            }
        }

        public int? CurrentCardId
        {
            get
            {
                if (Request.QueryString["id"] == null)
                {
                    return null;
                }
                return Convert.ToInt32(Request.QueryString["id"]);
            }
        }

        List<KeyValuePair<int, string>> OriginalButtonMultiMediaList
        {
            get
            {
                return ViewState[OriginalButtonMultiMediaListKey] as List<KeyValuePair<int, string>>;
            }
            set
            {
                ViewState[OriginalButtonMultiMediaListKey] = value;
            }
        }


        public void Populate(Card card)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var questionCard = card.QuestionCard;

            if (questionCard == null)
            {
                return;
            }

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();

            if (questionCardLanguage != null)
            {
                //set simple control values
                QuestionLongVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_LongVersion);
                QuestionShortVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_ShortVersion);
            }

            if (card is BranchingCard)
            {
                var branchingCard = (BranchingCard)card;

                GameComboBox.Value = branchingCard.GameID;
                ButtonsPressedInSequencyCheckbox.Checked = branchingCard.MustPressButtonsInSequence;
                AddBackgroundBorderCheckbox.Checked = branchingCard.MustAddBackgroundBorderToBranches;
                MinButtonsComboBox.Value = branchingCard.MinButtonPresses;
                ButtonOrientationComboBox.Value = branchingCard.ButtonOrientation;
                CompletedCardGroupCombobox.Value = branchingCard.CardGroupID_BranchingCompleted;

                var buttons = (from b in dataContext.BranchingCard_Button
                               where b.BranchingCardID == card.CardID
                               select b).OrderBy(b => b.Position).ToList();

                OriginalButtonMultiMediaList = new List<KeyValuePair<int, string>>();
                if (buttons.Any())
                {
                    foreach (var branchingCardButton in buttons)
                    {
                        var buttonLanguage = (from b in dataContext.BranchingCard_ButtonLanguage
                                              where b.BranchingCard_ButtonID == branchingCardButton.BranchingCard_ButtonID
                                              select b).FirstOrDefault();

                        if (buttonLanguage != null)
                        {
                            Items.Add(new BranchingCardButtonListItem()
                                      {
                                          BranchingCard_ButtonID = branchingCardButton.BranchingCard_ButtonID,
                                          BranchingCard_ButtonLanguageID = buttonLanguage.BranchingCard_ButtonLanguageID,
                                          IsMultimediaLive = true,
                                          Position = branchingCardButton.Position,
                                          ButtonText = buttonLanguage.ButtonText,
                                          MultimediaUrl = buttonLanguage.ButtonImageMultimediaURL,
                                          MultimediaUrlFriendlyName = buttonLanguage.ButtonImageFriendlyName,
                                          GoToCardGroupId = branchingCardButton.CardGroupID_Result,
                                          IsMultiClick = branchingCardButton.IsMultiClick,
                                          MustReturnToBranchingCard = branchingCardButton.MustReturnToBranchingCard
                                      });

                            OriginalButtonMultiMediaList.Add(new KeyValuePair<int, string>(buttonLanguage.BranchingCard_ButtonLanguageID,
                                                                           buttonLanguage.ButtonImageMultimediaURL));
                        }
                    }
                }
            }
        }

        protected void GameDataSource_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            e.Result = from g in dataContext.Games
                       where !g.IsADeletedRow && !g.IsAnAuditRow && g.IsAGameTemplate
                       select new
                       {
                           GameID = g.GameID,
                           GameName = g.GameTemplateNameForDashboard
                       };
        }

        protected void CardGroupsDataSource_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            if (GameComboBox.Value != null)
            {
                var gameId = (int)GameComboBox.Value;

                var dataContext = new EChallengeDataContext();

                var results = (from groupSequence in dataContext.GameCardGroupSequences
                    where !groupSequence.IsADeletedRow && !groupSequence.IsAnAuditRow
                          && groupSequence.GameID == gameId
                    select new
                           {
                               GroupSequenceID = groupSequence.GameCardGroupSequenceID,
                               GroupSequenceNumber = groupSequence.CardGroupSequenceNo,
                           });
                
                if (CurrentCardId.HasValue)
                {
                    var isGameCard =
                        dataContext.Cards.Where(c => c.CardID == CurrentCardId.Value)
                            .Select(c => c.IsForGame)
                            .FirstOrDefault();

                    if (isGameCard)
                    {
                        var cardGroupSequenceNumber = (from g in dataContext.GameCardGroupSequences
                            join c in dataContext.GameCardSequences on g.GameCardGroupSequenceID equals
                                c.GameCardGroupSequenceID
                            where c.CardID == CurrentCardId.Value && g.GameID == gameId
                            select g.CardGroupSequenceNo).FirstOrDefault();

                        results = results.Where(g => g.GroupSequenceNumber > cardGroupSequenceNumber);
                    }
                }
                

                e.Result = results.OrderBy(r => r.GroupSequenceNumber);
                return;
            }

            e.Cancel = true;
        }

        public void PopulateBranchingCardDetails(Card card)
        {
            if (card is BranchingCard)
            {
                var branchingCard = card as BranchingCard;

                branchingCard.GameID = Convert.ToInt32(GameComboBox.Value);
                branchingCard.MustPressButtonsInSequence = ButtonsPressedInSequencyCheckbox.Checked;
                branchingCard.MustAddBackgroundBorderToBranches = AddBackgroundBorderCheckbox.Checked;
                branchingCard.MinButtonPresses = Convert.ToInt32(MinButtonsComboBox.Value);
                branchingCard.ButtonOrientation = ButtonOrientationComboBox.Value.ToString();
                branchingCard.CardGroupID_BranchingCompleted = Convert.ToInt32(CompletedCardGroupCombobox.Value);
            }
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var currentDateTime = DateTime.Now;

            var card = new QuestionCard_Language
            {
                Question_Statement_LongVersion = QuestionShortVersionHtmlControl.GetHtml(),
                Question_Statement_ShortVersion = QuestionShortVersionHtmlControl.GetHtml(),
                UserID_CreatedBy = userId,
                CreatedOnDateTime = currentDateTime,
                UserID_LastModifiedBy = userId,
                LastModifiedOnDateTime = currentDateTime
            };

            return card;
        }

        public List<BranchingCard_Button> GetBranchingCardButtons()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var items = new List<BranchingCard_Button>();

            foreach (var branchingCardButtonListItem in Items)
            {
                var cardButtonLanguage = new BranchingCard_Button()
                {
                    BranchingCard_ButtonID = branchingCardButtonListItem.BranchingCard_ButtonID,
                    UserID_CreatedBy = userId,
                    UserID_LastModifiedBy = userId,
                    LastModifiedOnDateTime = currentDateTime,
                    IsADeletedRow = false,
                    IsAnAuditRow = false,
                    Position = branchingCardButtonListItem.Position,
                    CardGroupID_Result = branchingCardButtonListItem.GoToCardGroupId,
                    IsMultiClick = branchingCardButtonListItem.IsMultiClick,
                    MustReturnToBranchingCard = branchingCardButtonListItem.MustReturnToBranchingCard
                };

                cardButtonLanguage.BranchingCard_ButtonLanguage = new List<BranchingCard_ButtonLanguage>();

                cardButtonLanguage.BranchingCard_ButtonLanguage.Add(new BranchingCard_ButtonLanguage()
                                                                    {
                                                                        BranchingCard_ButtonLanguageID =
                                                                            branchingCardButtonListItem
                                                                            .BranchingCard_ButtonLanguageID,
                                                                        UserID_CreatedBy = userId,
                                                                        UserID_LastModifiedBy = userId,
                                                                        LastModifiedOnDateTime = currentDateTime,
                                                                        LanguageID = languageId,
                                                                        IsADeletedRow = false,
                                                                        IsAnAuditRow = false,
                                                                        ButtonText = branchingCardButtonListItem.ButtonText,
                                                                        ButtonImageMultimediaURL = branchingCardButtonListItem.MultimediaUrl,
                                                                        ButtonImageFriendlyName = branchingCardButtonListItem.MultimediaUrlFriendlyName
                                                                    });

                items.Add(cardButtonLanguage);
            }

            return items;
        }

        public void MoveMultimediaFilesToLiveFolder()
        {
            var buttons = Items.Where(p => !p.IsMultimediaLive).ToList();

            for (var i = 0; i < buttons.Count; i++)
            {
                var mediaUrl = buttons[i].MultimediaUrl;

                if (string.IsNullOrWhiteSpace(mediaUrl) || string.IsNullOrEmpty(mediaUrl))
                {
                    continue;
                }

                var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), mediaUrl);
                var destinationPath =
                    Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                 mediaUrl);

                File.Move(tempPath, destinationPath);

                int id = buttons[i].BranchingCard_ButtonLanguageID;

                if (OriginalButtonMultiMediaList == null)
                {
                    continue;
                }

                var originalValueEntry = OriginalButtonMultiMediaList.FirstOrDefault(m => m.Key == id);

                var oldFileDeletePath = string.Empty;
                if (!string.IsNullOrEmpty(originalValueEntry.Value))
                {
                    oldFileDeletePath =
                        Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                        originalValueEntry.Value);
                }

                if (!string.IsNullOrEmpty(originalValueEntry.Value) && File.Exists(oldFileDeletePath))
                {
                    File.Delete(oldFileDeletePath);
                }
            }
        }

        protected void CompletedCardGroupCombobox_OnCallback(object sender, CallbackEventArgsBase e)
        {
            var comboBox = (ASPxComboBox)sender;

            comboBox.DataBind();
        }


        public bool IsValid(out List<string> errors)
        {
            var isValid = true;
            errors = new List<string>();
            var hasMinButtons = true;

            if (GameComboBox.Value == null)
            {
                errors.Add("A game must be selected for the Branching Card.\n");
                isValid = false;
            }

            if (MinButtonsComboBox.Value == null)
            {
                errors.Add("The minimum number of button presses is required.\n");
                isValid = false;
                hasMinButtons = false;
            }

            if (CompletedCardGroupCombobox.Value == null)
            {
                errors.Add("The card group to continue to is required.\n");
                isValid = false;
            }

            if (Items.Any())
            {
                foreach (var branchingCardButtonListItem in Items)
                {
                    if (string.IsNullOrEmpty(branchingCardButtonListItem.ButtonText) ||
                        string.IsNullOrWhiteSpace(branchingCardButtonListItem.ButtonText))
                    {
                        errors.Add("The buttons for the branching card must have Button Text.\n");
                        isValid = false;
                    }

                    if (branchingCardButtonListItem.GoToCardGroupId <= 0)
                    {
                        errors.Add("The buttons for the branching card must have a Card Group to go to.\n");
                        isValid = false;
                    }

                    if (!isValid)
                    {
                        break;
                    }
                }


                if (hasMinButtons)
                {
                    var minButtonsValue = (int) MinButtonsComboBox.Value;
                    if (minButtonsValue > 1 || minButtonsValue == -1)
                    {
                        if (Items.Any(c => !c.MustReturnToBranchingCard))
                        {
                            errors.Add("All buttons must return to the branching card if a minimum number of buttons are required.\n");
                            isValid = false;
                        }
                    }
                }
            }

            return isValid;
        }

        protected void BranchButtonGridView_OnCustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (string.Compare(e.ButtonID, "UpCustomButton") == 0)
            {
                MoveSelection(-1, e.VisibleIndex);
            }
            else if (string.Compare(e.ButtonID, "DownCustomButton") == 0)
            {
                MoveSelection(1, e.VisibleIndex);
            }
            else if (string.Compare(e.ButtonID, "DeleteButton") == 0)
            {
                var gridView = sender as ASPxGridView;
                var item = gridView.GetRow(e.VisibleIndex) as BranchingCardButtonListItem;

                if (item != null)
                {
                    Items.Remove(item);
                }

                gridView.JSProperties["cp_RowCount"] = Items.Count;
                gridView.DataBind();
            }
        }

        bool CanMoveSelection(int offset, int visibleIndex)
        {
            return visibleIndex + offset >= 0 && visibleIndex + offset <= BranchButtonGridView.VisibleRowCount - 1;
        }

        void MoveSelection(int offset, int visibleIndex)
        {
            if (!CanMoveSelection(offset, visibleIndex))
            {
                return;
            }

            var count = Items.Count;
            var temp = new BranchingCardButtonListItem[count];

            temp[visibleIndex + offset] = Items[visibleIndex];
            temp[visibleIndex + offset].Position = visibleIndex + offset + 1;

            var index = 0;
            for (int i = 0; i < count; i++)
            {
                if (i == visibleIndex)
                {
                    continue;
                }

                while (temp[index] != null)
                {
                    index++;
                }

                temp[index] = Items[i];
                temp[index].Position = index + 1;
                index++;
            }

            Items.Clear();
            Items.AddRange(temp);
            BranchButtonGridView.DataBind();
        }

        protected void BranchButtonGridView_OnCustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            if (e.CellType == GridViewTableCommandCellType.Data)
            {
                if (e.VisibleIndex == 0)
                {
                    if (gridView.VisibleRowCount == 1)
                    {
                        if (string.Compare(e.ButtonID, "UpCustomButton") == 0 ||
                            string.Compare(e.ButtonID, "DownCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                    else
                    {
                        if (string.Compare(e.ButtonID, "UpCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                }
                else
                {
                    if (e.VisibleIndex == gridView.VisibleRowCount - 1)
                    {
                        if (string.Compare(e.ButtonID, "DownCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                }
            }
        }

        protected void BranchButtonGridView_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            if (string.Compare(e.Parameters, "NEW") == 0)
            {
                var button = new BranchingCardButtonListItem();
                button.Position = Items.Count + 1;
                Items.Add(button);
            }
            gridView.JSProperties["cp_RowCount"] = Items.Count;
            gridView.DataBind();
        }

        protected void TextMemo_OnInit(object sender, EventArgs e)
        {
            var memo = sender as ASPxMemo;
            var container = memo.NamingContainer as GridViewDataItemTemplateContainer;
            memo.ClientInstanceName = string.Format(@"TextMemo_{0}", container.VisibleIndex);
            memo.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            memo.ClientSideEvents.ValueChanged = string.Format(CallbackArgumentFormat,
                container.KeyValue,
                container.Column.FieldName,
                "s.GetText()"
                );
        }

        protected void GridCallback_OnCallback(object source, CallbackEventArgs e)
        {
            var p = e.Parameter.Split('|');

            var key = p[0];
            var field = p[1];
            var value = p[2];

            var item = Items.Find(c => c.ClientId == key);

            switch (field)
            {
                case "ButtonText":
                    if (value == "null")
                    {
                        value = String.Empty;
                    }
                    item.ButtonText = value;
                    break;
                case "MultimediaUrl":
                    if (value == "null")
                    {
                        value = String.Empty;
                    }
                    item.MultimediaUrl = value;
                    item.IsMultimediaLive = false;
                    break;
                case "MultimediaUrlFriendlyName":

                    if (value == "null")
                    {
                        value = String.Empty;
                    }
                    item.MultimediaUrlFriendlyName = value;
                    item.IsMultimediaLive = false;
                    break;
                case "IsMultiClick":
                    item.IsMultiClick = value != "null" && Convert.ToBoolean(value);
                    break;
                case "MustReturnToBranchingCard":
                    item.MustReturnToBranchingCard = value != "null" && Convert.ToBoolean(value);
                    break;
                case "GoToCardGroupId":
                    item.GoToCardGroupId = Convert.ToInt32(value);
                    break;

            }
        }

        protected void BranchButtonGridDataSource_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = Items;
        }

        protected void MultiSelectUploadControl_OnFileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            var splitOn = new[] { "." };
            var parts = e.UploadedFile.FileName.Split(splitOn, StringSplitOptions.RemoveEmptyEntries);
            var extension = parts.Length > 1 ? string.Format(".{0}", parts[parts.Length - 1]) : string.Empty;
            var filename = string.Format("{0}{1}", Guid.NewGuid(), extension);
            var fullPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
            e.UploadedFile.SaveAs(fullPath, false);
            var url = ResolveUrl(string.Format("{0}/{1}", ConfigurationHelper.TemporaryUploadLocation, filename));

            e.CallbackData = string.Format("{0};{1}", url, e.UploadedFile.FileName);
        }

        protected void MediaBrowseButton_OnInit(object sender, EventArgs e)
        {
            var button = sender as ASPxButton;
            var container = button.NamingContainer as GridViewDataItemTemplateContainer;
            button.ClientInstanceName = string.Format(@"MediaBrowseButton_{0}", container.VisibleIndex);
            button.JSProperties["cp_InstanceName"] = string.Format(@"MediaBrowseButton_{0}", container.VisibleIndex);
            button.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            button.JSProperties["cp_RowKey"] = container.KeyValue;

            button.ClientSideEvents.Click = "function (s, e) {MediaBrowseButton_Click(s,e);}";
        }

        protected void MultimediaHyperLink_OnInit(object sender, EventArgs e)
        {
            var link = sender as ASPxHyperLink;
            var container = link.NamingContainer as GridViewDataItemTemplateContainer;
            link.ClientInstanceName = string.Format(@"MultimediaHyperLink_{0}", container.VisibleIndex);
            link.JSProperties["cp_InstanceName"] = string.Format(@"MultimediaHyperLink_{0}", container.VisibleIndex);
            link.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
        }

        protected void RemoveMultimediaHyperLink_OnInit(object sender, EventArgs e)
        {
            var link = sender as ASPxHyperLink;
            var container = link.NamingContainer as GridViewDataItemTemplateContainer;
            link.ClientInstanceName = string.Format(@"RemoveMultimediaHyperLink_{0}", container.VisibleIndex);
            link.JSProperties["cp_InstanceName"] = string.Format(@"RemoveMultimediaHyperLink_{0}", container.VisibleIndex);
            link.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            link.JSProperties["cp_RowKey"] = container.KeyValue;

            link.ClientSideEvents.Click = "function (s, e) {RemoveMediaHyperLink_Click(s,e)}";
        }

        protected void MultimediaUrlMemo_OnInit(object sender, EventArgs e)
        {
            var memo = sender as ASPxMemo;
            var container = memo.NamingContainer as GridViewDataItemTemplateContainer;
            memo.ClientInstanceName = string.Format(@"MultimediaUrl_{0}", container.VisibleIndex);
            memo.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            memo.ClientSideEvents.ValueChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               "MultimediaUrl",
                                                               "s.GetText()"
                                                               );
        }

        protected void MultimediaUrlFriendlyNameMemo_OnInit(object sender, EventArgs e)
        {
            var memo = sender as ASPxMemo;
            var container = memo.NamingContainer as GridViewDataItemTemplateContainer;
            memo.ClientInstanceName = string.Format(@"MultimediaUrlFriendlyName_{0}", container.VisibleIndex);
            memo.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            memo.ClientSideEvents.ValueChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               "MultimediaUrlFriendlyName",
                                                               "s.GetText()"
                                                               );
        }

        protected void MultiClickCheckbox_OnInit(object sender, EventArgs e)
        {
            var checkbox = sender as ASPxCheckBox;
            var container = checkbox.NamingContainer as GridViewDataItemTemplateContainer;
            checkbox.ClientInstanceName = string.Format(@"MultiClickCheckbox_{0}", container.VisibleIndex);
            checkbox.JSProperties["cp_InstanceName"] = string.Format(@"MultiClickCheckbox_{0}", container.VisibleIndex);
            checkbox.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            checkbox.JSProperties["cp_RowKey"] = container.KeyValue;

            checkbox.ClientSideEvents.CheckedChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               container.Column.FieldName,
                                                               "s.GetValue()"
                                                               );
        }


        protected void MustReturnToBranchingCardCheckbox_OnInit(object sender, EventArgs e)
        {
            var checkbox = sender as ASPxCheckBox;
            var container = checkbox.NamingContainer as GridViewDataItemTemplateContainer;
            checkbox.ClientInstanceName = string.Format(@"MustReturnToBranchingCardCheckbox_{0}", container.VisibleIndex);
            checkbox.JSProperties["cp_InstanceName"] = string.Format(@"MustReturnToBranchingCardCheckbox_{0}", container.VisibleIndex);
            checkbox.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            checkbox.JSProperties["cp_RowKey"] = container.KeyValue;

            checkbox.ClientSideEvents.CheckedChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               container.Column.FieldName,
                                                               "s.GetValue()"
                                                               );
        }


        protected void GoToCardGroupCombobox_OnInit(object sender, EventArgs e)
        {
            var combobox = sender as ASPxComboBox;
            var container = combobox.NamingContainer as GridViewDataItemTemplateContainer;
            combobox.ClientInstanceName = string.Format(@"GoToCardGroupCombobox_{0}", container.VisibleIndex);
            combobox.JSProperties["cp_InstanceName"] = string.Format(@"GoToCardGroupCombobox_{0}", container.VisibleIndex);
            combobox.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            combobox.JSProperties["cp_RowKey"] = container.KeyValue;

            combobox.ClientSideEvents.SelectedIndexChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               container.Column.FieldName,
                                                               "s.GetValue()"
                                                               );
        }

        protected void BranchButtonGridView_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var item = gridView.GetRow(e.ListSourceRowIndex) as BranchingCardButtonListItem;

            SetUnboundColumnValues(gridView, item, e.ListSourceRowIndex);
        }

        private void SetUnboundColumnValues(ASPxGridView gridView, BranchingCardButtonListItem item, int rowIndex)
        {
            var column = gridView.Columns["Multimedia"] as GridViewDataColumn;

            var multimediaHyperlink =
                gridView.FindRowCellTemplateControl(rowIndex, column, "MultimediaHyperLink") as ASPxHyperLink;
            var removeHyperlink =
                gridView.FindRowCellTemplateControl(rowIndex, column, "RemoveMultimediaHyperLink") as ASPxHyperLink;
            var browseButton =
                gridView.FindRowCellTemplateControl(rowIndex, column, "MediaBrowseButton") as ASPxButton;

            if (string.IsNullOrEmpty(item.MultimediaUrl) || string.IsNullOrWhiteSpace(item.MultimediaUrl))
            {
                multimediaHyperlink.ClientVisible = false;
                multimediaHyperlink.NavigateUrl = "#";
                multimediaHyperlink.Text = string.Empty;

                removeHyperlink.ClientVisible = false;
                browseButton.ClientVisible = true;

                return;
            }

            var location = ConfigurationHelper.TemporaryUploadLocation;
            if (item.IsMultimediaLive)
            {
                location = ConfigurationHelper.LiveUploadLocation;
            }

            multimediaHyperlink.ClientVisible = true;
            multimediaHyperlink.NavigateUrl = ResolveUrl(string.Format(@"{0}/{1}", location, item.MultimediaUrl));
            multimediaHyperlink.Text = item.MultimediaUrlFriendlyName;

            removeHyperlink.ClientVisible = true;
            browseButton.ClientVisible = false;
        }


        protected void ResetGameDataCallback_OnCallback(object source, CallbackEventArgs e)
        {
            CompletedCardGroupCombobox.Value = -1;
            Items.Clear();
        }
    }
}