﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BranchingCardControl.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.BranchingCardControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register TagPrefix="uc2" TagName="BasicHtmlEditorControl" Src="~/UserControls/BasicHtmlEditorControl.ascx" %>
<script id="dxis_BranchingCardControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/Cards/BranchingCardControl.js") %>'> </script>


<dx:ASPxPopupControl ID="MultiSelectUploadPopupControl" runat="server" ClientInstanceName="MultiSelectUploadPopupControl"
    CloseAction="CloseButton" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Upload File" AllowDragging="true" EnableAnimation="false" EnableViewState="false">
    <ClientSideEvents Closing="MultiSelectUploadPopupControl_Closing" PopUp="function(s){s.UpdatePosition();}" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
            <dx:ASPxPanel ID="UploadPanel" runat="server" DefaultButton="UploadButton">
                <PanelCollection>
                    <dx:PanelContent>
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="SelectFileLabel" runat="server" Text="Select File">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxUploadControl ID="MultiSelectUploadControl" runat="server" ClientInstanceName="MultiSelectUploadControl"
                                        EnableViewState="false" ShowProgressPanel="true" OnFileUploadComplete="MultiSelectUploadControl_OnFileUploadComplete"
                                        UploadMode="Standard">
                                        <ValidationSettings MaxFileSize="4194304" MaxFileSizeErrorText="Maximum file size allowed is 4Mb">
                                        </ValidationSettings>
                                        <ClientSideEvents FileUploadComplete="MultiSelectUploadControl_FileUploadComplete" TextChanged="MultiSelectUploadControl_TextChanged" />
                                    </dx:ASPxUploadControl>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <dx:ASPxButton ID="MultiSelectUploadButton" runat="server" ClientInstanceName="MultiSelectUploadButton"
                                        Text="Upload" AutoPostBack="false" ClientEnabled="false">
                                        <ClientSideEvents Click="MultiSelectUploadButton_Click" />
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

<table width='<%=Width%>'>
    <tr>
        <td style="vertical-align: top">
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="GameLabel" runat="server" Text="Game" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxComboBox ID="GameComboBox" runat="server"                             
                            ClientInstanceName="GameComboBox" 
                            DataSourceID="GameDataSource"
                            TextField="GameName" 
                            ValueField="GameID"
                            ValueType="System.Int32"
                            DropDownStyle="DropDownList" 
                            >
                            <ClientSideEvents 
                                SelectedIndexChanged="OnGameChanged" 
                                Init="GameComboboxInit">                                
                            </ClientSideEvents>
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="A product is required" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                </tr>                
            </table>
        </td>
    </tr>
    <tr>
        <td class="spacerlarge"></td>
    </tr>
    <tr>
        <td>
            <dx:aspxpagecontrol id="QuestionPageControl" runat="server" activetabindex="0" width="100%">
                <tabpages>
                    <dx:tabpage name="ShortQuestionTab" text="Short Version of Question">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                <uc2:BasicHtmlEditorControl ID="QuestionShortVersionHtmlControl" runat="server" />   
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage name="LongQuestionTab" text="Long Version of Question">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                <uc2:BasicHtmlEditorControl ID="QuestionLongVersionHtmlControl" runat="server" />  
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
    </tr>
    <tr>         
         <td>
              <dx:ASPxCheckBox ID="ButtonsPressedInSequencyCheckbox" runat="server" Text="Buttons must be pressed in sequence">
                        </dx:ASPxCheckBox>
         </td>
    </tr>
    <tr>         
         <td>
              <dx:ASPxCheckBox ID="AddBackgroundBorderCheckbox" runat="server" Text="Add background border to branches">
                        </dx:ASPxCheckBox>
         </td>
    </tr>
    <tr>
        <td style="vertical-align: top">
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="MinButtonPressesLabel" runat="server" Text="Minimum Button Presses" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxComboBox ID="MinButtonsComboBox" runat="server"                             
                            ClientInstanceName="MinButtonsComboBox"                             
                            ValueType="System.Int32" 
                            DropDownStyle="DropDownList" 
                            >
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="Button orientation is required" />
                            </ValidationSettings>
                            <ClientSideEvents Init="OnMinButtonsComboBoxInit"></ClientSideEvents>                            
                        </dx:ASPxComboBox>
                    </td>
                </tr>        
                <tr>
                    <td>
                        <dx:ASPxLabel ID="ButtonOrientationLabel" runat="server" Text="Button Orientation" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxComboBox ID="ButtonOrientationComboBox" runat="server"                             
                            ClientInstanceName="ButtonOrientationComboBox" 
                            DropDownStyle="DropDownList" 
                            >
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="Button orientation is required" />
                            </ValidationSettings>
                            <Items>
                                <dx:ListEditItem Text="AUTO" Value="AUTO"/>
                                <dx:ListEditItem Text="2-Column" Value="2-Column"/>
                                <dx:ListEditItem Text="3-Column" Value="3-Column"/>
                                <dx:ListEditItem Text="Side-by-side" Value="Side-by-side"/>
                            </Items>
                        </dx:ASPxComboBox>
                    </td>
                </tr>   
                <tr>
                    <td>
                        <dx:ASPxLabel ID="CardGroupAfterBranchLabel" runat="server" Text="Card Group once this card is completed" />
                    </td>
                    <td class="spacerlarge"></td>
                    <td>
                        <dx:ASPxComboBox ID="CompletedCardGroupCombobox" runat="server"                             
                            ClientInstanceName="CompletedCardGroupCombobox" 
                            DataSourceID="CardGroupsDataSource"
                            TextField="GroupSequenceNumber" 
                            ValueField="GroupSequenceID"
                            ValueType="System.Int32" 
                            DropDownStyle="DropDownList" 
                            OnCallback="CompletedCardGroupCombobox_OnCallback"
                            >
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" ErrorText="A card group is required" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                </tr>                     
            </table>
        </td>
    </tr>
    <tr>
        <td class="spacerlarge"></td>
    </tr>
    <tr>
        <td style="vertical-align: top">
            <dx:ASPxGridView runat="server" 
                ID="BranchButtonGridView" 
                ClientInstanceName="BranchButtonGridView"
                AutoGenerateColumns="False"
                keyfieldname="ClientId"
                DataSourceID="BranchButtonGridDataSource"
                OnCustomCallback="BranchButtonGridView_OnCustomCallback"
                OnCustomButtonCallback="BranchButtonGridView_OnCustomButtonCallback"
                OnCustomButtonInitialize="BranchButtonGridView_OnCustomButtonInitialize"
                OnCustomUnboundColumnData="BranchButtonGridView_OnCustomUnboundColumnData">
                 <clientsideevents endcallback="BranchButtonGridView_EndCallback" />
                <Columns>
                    <dx:GridViewCommandColumn ButtonType="Image" VisibleIndex="0" Caption=" " Width="30px">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="UpCustomButton" Text="Up" Visibility="AllDataRows">
                                <Image Url="~/App_Themes/Icons/ArrowUp.png" Height="12" Width="12" ToolTip="Move Up" />
                            </dx:GridViewCommandColumnCustomButton>
                            <dx:GridViewCommandColumnCustomButton ID="DownCustomButton" Text="Down" Visibility="AllDataRows">
                                <Image Url="~/App_Themes/Icons/ArrowDown.png" Height="12" Width="12" ToolTip="Move Down" />
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewCommandColumn VisibleIndex="1" Width="50px">
                        <HeaderCaptionTemplate>
                            <asp:LinkButton ID="NewLinkButton" runat="server" OnClientClick="AddNewRow();return false;"
                                Text="New" />
                        </HeaderCaptionTemplate>
                       <%-- <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="DeleteButton" Text="Delete">
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>--%>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="Id" ReadOnly="true" Visible="false" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ButtonText" Caption="Button Text" VisibleIndex="3">
                        <DataItemTemplate>
                            <dx:ASPxMemo ID="TextMemo" runat="server" ClientInstanceName="TextMemo"
                                Rows="2" Text='<%# Bind("ButtonText") %>' OnInit="TextMemo_OnInit" 
                                AutoResizeWithContainer="True" Width="100%">
                            </dx:ASPxMemo>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn FieldName="Multimedia" Caption="Button Image" VisibleIndex="5" UnboundType="Object">
                        <DataItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="MediaBrowseButton" ClientInstanceName="MediaBrowseButton"
                                            runat="server" Text="Browse.." AutoPostBack="false" OnInit="MediaBrowseButton_OnInit">
                                        </dx:ASPxButton>
                                        <dx:ASPxHyperLink ID="MultimediaHyperLink" runat="server" ClientInstanceName="MultimediaHyperLink"
                                            Target="_blank" Text="" NavigateUrl="#" OnInit="MultimediaHyperLink_OnInit">
                                        </dx:ASPxHyperLink>
                                    </td>
                                    <td>
                                        <dx:ASPxHyperLink ID="RemoveMultimediaHyperLink" runat="server" ClientInstanceName="RemoveMultimediaHyperLink"
                                            Target="_blank" Text="Remove" ClientVisible="False" NavigateUrl="#" OnInit="RemoveMultimediaHyperLink_OnInit">
                                        </dx:ASPxHyperLink>
                                    </td>
                                    <td>
                                        <dx:ASPxMemo ID="MultimediaUrlMemo" runat="server" ClientInstanceName="MultimediaUrlMemo" Width="100%"
                                            Rows="2" Text='<%#Bind("MultimediaUrl") %>' ClientVisible="False" OnInit="MultimediaUrlMemo_OnInit">
                                        </dx:ASPxMemo>
                                        <dx:ASPxMemo ID="MultimediaUrlFriendlyNameMemo" runat="server" ClientInstanceName="MultimediaUrlFriendlyNameMemo" Width="100%"
                                            Rows="2" Text='<%#Bind("MultimediaUrlFriendlyName") %>' ClientVisible="False" OnInit="MultimediaUrlFriendlyNameMemo_OnInit">
                                        </dx:ASPxMemo>
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn FieldName="MultimediaUrl" Visible="False" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="MultimediaUrlFriendlyName" Visible="False" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                     <dx:GridViewDataCheckColumn FieldName="GoToCardGroupId" VisibleIndex="8" Caption="Jump to Card Group">
                         <DataItemTemplate>
                             <dx:ASPxComboBox ID="GoToCardGroupCombobox" runat="server"
                                 DataSourceID="CardGroupsDataSource"
                                 TextField="GroupSequenceNumber"
                                 ValueField="GroupSequenceID"
                                 ValueType="System.Int32"
                                 Value='<%# Bind("GoToCardGroupId") %>'
                                 OnInit="GoToCardGroupCombobox_OnInit"
                                 DropDownStyle="DropDownList">
                             </dx:ASPxComboBox>
                         </DataItemTemplate>
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataCheckColumn FieldName="IsMultiClick" VisibleIndex="9" Caption="Multiple Click">
                         <DataItemTemplate>
                            <dx:ASPxCheckBox ID="MultiClickCheckbox" runat="server" ClientInstanceName="MultiClickCheckbox"
                                Rows="2" Checked='<%# Bind("IsMultiClick") %>' OnInit="MultiClickCheckbox_OnInit"
                                AutoResizeWithContainer="True" Width="100%">
                            </dx:ASPxCheckBox>
                        </DataItemTemplate>
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataCheckColumn FieldName="MustReturnToBranchingCard" VisibleIndex="10" Caption="Return to Branching Card">
                         <DataItemTemplate>
                            <dx:ASPxCheckBox ID="MustReturnToBranchingCardCheckbox" runat="server" ClientInstanceName="MustReturnToBranchingCardCheckbox"
                                Rows="2" Checked='<%# Bind("MustReturnToBranchingCard") %>' OnInit="MustReturnToBranchingCardCheckbox_OnInit"
                                AutoResizeWithContainer="True" Width="100%">
                            </dx:ASPxCheckBox>
                        </DataItemTemplate>
                    </dx:GridViewDataCheckColumn>
                </Columns>
            </dx:ASPxGridView>
        </td>
    </tr>
    <tr>
        <td class="spacerlarge"></td>
    </tr>
</table>
<asp:LinqDataSource ID="GameDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="GameDataSource_OnSelecting"
    TableName="Game">
</asp:LinqDataSource>
<asp:LinqDataSource ID="CardGroupsDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="CardGroupsDataSource_OnSelecting"
    TableName="GameCardGroupSequence">
</asp:LinqDataSource>
<asp:linqdatasource id="BranchButtonGridDataSource" runat="server" onselecting="BranchButtonGridDataSource_OnSelecting">
</asp:linqdatasource>
<dx:aspxcallback id="GridCallback" runat="server" clientinstancename="GridCallback" OnCallback="GridCallback_OnCallback">
</dx:aspxcallback>
<dx:ASPxCallback ID="ResetGameDataCallback" ClientInstanceName="ResetGameDataCallback" runat="server" OnCallback="ResetGameDataCallback_OnCallback">
    <ClientSideEvents CallbackComplete="ResetGameDataCompleted"></ClientSideEvents>
</dx:ASPxCallback>
