﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchingListQuestionCard.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.MatchingListQuestionCard" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ register src="../../MultiSelectEditListControl.ascx" tagname="EditListControl"
    tagprefix="uc1" %>


<%@ Register TagPrefix="uc2" TagName="BasicHtmlEditorControl" Src="~/UserControls/BasicHtmlEditorControl.ascx" %>
<table width='<%=Width%>'>
    <tr>
        <td>
            <dx:aspxpagecontrol id="QuestionPageControl" runat="server" activetabindex="0" width="100%">
                <tabpages>
                    <dx:tabpage name="ShortQuestionTab" text="Short Version of Question">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                <uc2:BasicHtmlEditorControl ID="QuestionShortVersionHtmlControl" runat="server" />   
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage name="LongQuestionTab" text="Long Version of Question">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                <uc2:BasicHtmlEditorControl ID="QuestionLongVersionHtmlControl" runat="server" />  
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
    </tr>
    <tr>
        <td>
            <h3>
                <asp:literal id="PossibleAnswerLiteral" runat="server" text="Possible Answers" /></h3>
        </td>
    </tr>
    <tr>
        <td>
            <uc1:editlistcontrol id="PossibleAnswerEditListControl" ShowSelectColumn="False" runat="server" allowsinglerowselectonly="false"
                longversioncolumncaption="Right Matching Item" shortversioncolumncaption="Left Matching Item" ShowMultimediaColumn="False"/>
        </td>
    </tr>    
     <tr>
        <td>
            <table>
                <tr>                    
                     <td>
                        <dx:aspxcheckbox id="ShowCompanyResultsCheckBox" runat="server" text="Show Company Results" >
                            <ClientSideEvents CheckedChanged="function(s, e) { MinDataPointsSpinEdit.SetEnabled(s.GetChecked()); }" />     
                        </dx:aspxcheckbox>            
                    </td>     
                    <td class="spacerlarge"></td>                
                    <td>
                        <dx:ASPxLabel ID="MinDataPointsLabel" runat="server" Text="Minimum Data Points for Company Results" />
                    </td>
                    <td class="spacer"></td>
                    <td>
                        <dx:ASPxSpinEdit ID="MinDataPointsSpinEdit" ClientInstanceName="MinDataPointsSpinEdit" runat="server" ClientEnabled="False" MinValue="1" MaxValue="999" NumberType="Integer"
                                                                AllowUserInput="true" MaxLength="3" Width="50px">
                                                            </dx:ASPxSpinEdit>
                    </td>                                      
                </tr>
            </table>            
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxpagecontrol id="ExplanationPageControl" runat="server" activetabindex="0"
                width="100%">
                <tabpages>
                    <dx:tabpage name="ShortExplanationTab" text="Short Explanation of Answer">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                 <uc2:BasicHtmlEditorControl ID="AnswerExplainationShortVersionHtmlEditor" runat="server" />       
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage name="LongExplanationTab" text="Long Explanation of Answer">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                <uc2:BasicHtmlEditorControl ID="AnswerExplainationLongVersionHtmlEditor" runat="server" />    
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
     </tr>
</table>
