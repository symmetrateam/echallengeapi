﻿<%@ control language="C#" autoeventwireup="true" codebehind="TextAndImageCardControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.TextAndImageCardControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>




<script id="dxis_CardManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/Cards/TextAndImageCardControl.js") %>'>
</script>
<dx:aspxhiddenfield id="TextImageHiddenField" runat="server" clientinstancename="TextImageHiddenField">
</dx:aspxhiddenfield>
<dx:aspxpopupcontrol id="UpPopupControl" runat="server" clientinstancename="UpPopupControl"
    closeaction="CloseButton" modal="true" popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter"
    headertext="Upload File" allowdragging="true" enableanimation="false" enableviewstate="false">
    <clientsideevents closing="UpPopupControl_Closing" />
    <contentcollection>
        <dx:popupcontrolcontentcontrol id="Popupcontrolcontentcontrol1" runat="server">
            <dx:aspxpanel id="UploadPanel" runat="server" defaultbutton="UploadButton">
                <panelcollection>
                    <dx:panelcontent>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="SelectFileLabel" runat="server" text="Select File">
                                    </dx:aspxlabel>
                                </td>
                                <td>
                                    <dx:aspxuploadcontrol id="UpControl" runat="server" clientinstancename="UpControl"
                                        enableviewstate="false" showprogresspanel="true" onfileuploadcomplete="UploadControl_FileUploadComplete"
                                        uploadmode="Standard">
                                        <validationsettings maxfilesize="4194304" maxfilesizeerrortext="Maximum file size allowed is 4Mb">
                                        </validationsettings>
                                        <clientsideevents fileuploadcomplete="UpControl_FileUploadComplete" textchanged="UpControl_TextChanged" />
                                    </dx:aspxuploadcontrol>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <dx:aspxbutton id="UpButton" runat="server" clientinstancename="UpButton"
                                        text="Upload" autopostback="false" clientenabled="false">
                                        <clientsideevents click="UpButton_Click" />
                                    </dx:aspxbutton>
                                </td>
                            </tr>
                        </table>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxpanel>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>
<table>
    <tr>
        <td>
            <dx:aspxlabel id="NameLabel" runat="server" text="Card Statement" />
        </td>
        <td class="spacer">
        </td>
        <td colspan="2">
            <dx:aspxtextbox id="NameTextBox" runat="server">
            </dx:aspxtextbox>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxlabel id="ImageLocationLabel" runat="server" text="Image" />
        </td>
        <td class="spacer">
        </td>
        <td>
            <dx:aspxtextbox id="ImageLocationTextBox" runat="server" clientinstancename="ImageLocationTextBox">
            </dx:aspxtextbox>
        </td>
        <td>
            <dx:aspxhyperlink id="ImageHyperLink" runat="server" clientinstancename="ImageHyperLink"
                target="_blank" text="" navigateurl="#">
            </dx:aspxhyperlink>
        </td>
        <td>
            <dx:aspxbutton id="BrowseButton" runat="server" clientinstancename="BrowseButton"
                text="Browse..." autopostback="False">
                <clientsideevents click="function(s,e){ ShowUpWindow(); }" />
            </dx:aspxbutton>
        </td>
        <td>
            <dx:aspxhyperlink id="RemoveHyperLink" runat="server" clientinstancename="RemoveHyperLink"
                navigateurl="#" text="Remove" clientvisible="false">
                <clientsideevents click="function(s,e){ RemoveHyperLink_Click(s,e);}" />
            </dx:aspxhyperlink>
        </td>
    </tr>
</table>
