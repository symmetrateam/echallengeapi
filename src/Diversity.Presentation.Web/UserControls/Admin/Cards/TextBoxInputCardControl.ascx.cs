﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class TextBoxInputCardControl : BaseCardControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Populate(Card card)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var questionCard = card.QuestionCard;

            if(questionCard == null)
            {
                return;
            }

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();
            
            if(questionCardLanguage == null)
            {
                return;
            }

            //set simple control values
            QuestionLongVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_LongVersion);
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;
            var card = new QuestionCard_Language
            {
                Question_Statement_LongVersion = QuestionLongVersionHtmlControl.GetHtml(),
                Question_Statement_ShortVersion = QuestionLongVersionHtmlControl.GetHtml(),
                UserID_CreatedBy = userId,
                CreatedOnDateTime = currentDateTime,
                LastModifiedOnDateTime = currentDateTime,
                UserID_LastModifiedBy = userId
            };

            return card;
        }

    }
}