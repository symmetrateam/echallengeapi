﻿<%@ control language="C#" autoeventwireup="true" codebehind="TextAreaInputCardControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.TextAreaInputCardControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>


<%@ Register TagPrefix="uc2" TagName="BasicHtmlEditorControl" Src="~/UserControls/BasicHtmlEditorControl.ascx" %>
<table width='<%=Width%>'>
    <tr>
        <td>
            <dx:aspxpagecontrol id="QuestionPageControl" runat="server" activetabindex="0" width="100%">
                <tabpages>
                    <dx:tabpage name="QuestionTab" text="Question">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                               <uc2:BasicHtmlEditorControl ID="QuestionLongVersionHtmlControl" runat="server"/>
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
    </tr>
</table>
