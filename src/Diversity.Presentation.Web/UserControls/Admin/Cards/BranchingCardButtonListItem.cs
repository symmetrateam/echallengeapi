﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public class BranchingCardButtonListItem
    {
        public string ClientId { get; private set; }
        public int BranchingCard_ButtonID { get; set; }  
        public int BranchingCard_ButtonLanguageID { get; set; }  
        public bool IsMultimediaLive { get; set; }

        public int Position { get; set; }
        public string ButtonText { get; set; }
        public string MultimediaUrl { get; set; }
        public string MultimediaUrlFriendlyName { get; set; }
        public int GoToCardGroupId { get; set; }
        public bool IsMultiClick { get; set; }
        public bool MustReturnToBranchingCard { get; set; }        

        public BranchingCardButtonListItem()
        {
            ClientId = Guid.NewGuid().ToString();
        }
    }
}