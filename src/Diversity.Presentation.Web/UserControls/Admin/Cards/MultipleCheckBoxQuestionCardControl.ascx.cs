﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class MultipleCheckBoxQuestionCardControl : BaseCardControl
    {
        public int? CurrentCardId
        {
            get
            {
                if (Request.QueryString["id"] == null)
                {
                    return null;
                }
                return Convert.ToInt32(Request.QueryString["id"]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!CurrentCardId.HasValue)
                {
                    QuestionIsOnlyCorrectIfAllAnswersAreTrueCheckBox.Checked = true;
                }
            }
        }

        public void Populate(Card card)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var questionCard = card.QuestionCard;

            if(questionCard == null)
            {
                return;
            }

            QuestionIsOnlyCorrectIfAllAnswersAreTrueCheckBox.Checked = questionCard.QuestionIsOnlyCorrectIfAllAnswersAreTrue;


            //populate the answers
            var answers = from q in questionCard.QuestionCard_GeneralAnswers
                          orderby q.SequenceNo
                          select q;

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var items = new List<SelectEditListItem>();

            foreach (var answer in answers)
            {
                var answerLanguages = (from a in answer.QuestionCard_GeneralAnswer_Languages
                                       where a.LanguageID == languageId
                                       select a).FirstOrDefault();

                if (answerLanguages != null)
                {
                    items.Add(
                        new SelectEditListItem
                        {
                            Id = answer.QuestionCard_GeneralAnswerID.ToString(),
                            IsSelected = answer.IsCorrectAnswer,
                            ExtendedText = answerLanguages.PossibleAnswer_LongVersion,
                            Text = answerLanguages.PossibleAnswer_ShortVersion
                        }
                        );
                }
            }

            PossibleAnswerEditListControl.Items = items;

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();

            if (questionCardLanguage == null)
            {
                return;
            }

            //set simple control values
            QuestionLongVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_LongVersion);
            QuestionShortVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_ShortVersion);
            AnswerExplainationLongVersionHtmlEditor.PopulateHtml(questionCardLanguage.AnswerExplanation_LongVersion);
            AnswerExplainationShortVersionHtmlEditor.PopulateHtml(questionCardLanguage.AnswerExplanation_ShortVersion);
        }
        
        public bool QuestionIsOnlyCorrectIfAllAnswersAreTrue
        {
            get { return QuestionIsOnlyCorrectIfAllAnswersAreTrueCheckBox.Checked; }
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;

            var card = new QuestionCard_Language
            {
                Question_Statement_LongVersion = QuestionLongVersionHtmlControl.GetHtml(),
                Question_Statement_ShortVersion = QuestionShortVersionHtmlControl.GetHtml(),
                AnswerExplanation_LongVersion = AnswerExplainationLongVersionHtmlEditor.GetHtml(),
                AnswerExplanation_ShortVersion = AnswerExplainationShortVersionHtmlEditor.GetHtml(),
                UserID_CreatedBy = userId,
                CreatedOnDateTime = currentDateTime,
                UserID_LastModifiedBy = userId,
                LastModifiedOnDateTime = currentDateTime
            };

            return card;
        }

        public List<QuestionCard_GeneralAnswer> GetGeneralAnswers()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var answers = new List<QuestionCard_GeneralAnswer>();

            var possibleAnswers = PossibleAnswerEditListControl.Items;

            for (var i = 0; i < possibleAnswers.Count; i++)
            {
                var answer = new QuestionCard_GeneralAnswer
                {
                    CreatedOnDateTime = currentDateTime,
                    LastModifiedOnDateTime = currentDateTime,
                    UserID_CreatedBy = userId,
                    UserID_LastModifiedBy = userId,
                    SequenceNo = i,
                    IsCorrectAnswer = possibleAnswers[i].IsSelected
                };

                if (!string.IsNullOrEmpty(possibleAnswers[i].Id))
                {
                    answer.QuestionCard_GeneralAnswerID = Convert.ToInt32(possibleAnswers[i].Id);
                }

                var answerLanguage = new QuestionCard_GeneralAnswer_Language
                {
                    UserID_CreatedBy = userId,
                    CreatedOnDateTime = currentDateTime,
                    UserID_LastModifiedBy = userId,
                    LastModifiedOnDateTime = currentDateTime,
                    QuestionCard_GeneralAnswerID = answer.QuestionCard_GeneralAnswerID,
                    LanguageID = languageId,
                    PossibleAnswer_LongVersion = possibleAnswers[i].ExtendedText,
                    PossibleAnswer_ShortVersion = possibleAnswers[i].Text
                };

                answer.QuestionCard_GeneralAnswer_Languages.Add(answerLanguage);
                answers.Add(answer);
            }

            return answers;
        }      
    }
}