﻿<%@ control language="C#" autoeventwireup="true" codebehind="MultipleChoicePriorityFeedbackCardControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.MultipleChoicePriorityFeedbackCardControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ register src="../../MultiSelectEditListControl.ascx" tagname="MultiSelectEditListControl"
    tagprefix="uc1" %>


<%@ Register TagPrefix="uc2" TagName="BasicHtmlEditorControl" Src="~/UserControls/BasicHtmlEditorControl.ascx" %>
<table width='<%=Width%>'>
    <tr>
        <td>
            <dx:aspxpagecontrol id="QuestionPageControl" runat="server" activetabindex="0" width="100%">
                <tabpages>
                    <dx:tabpage name="ShortQuestionTab" text="Short Version of Question">
                        <contentcollection>
                            <dx:contentcontrol id="Contentcontrol1" runat="server">
                                  <uc2:BasicHtmlEditorControl ID="QuestionShortVersionHtmlControl" runat="server" />    
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage name="LongQuestionTab" text="Long Version of Question">
                        <contentcollection>
                            <dx:contentcontrol id="Contentcontrol2" runat="server">
                                 <uc2:BasicHtmlEditorControl ID="QuestionLongVersionHtmlControl" runat="server" />  
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
    </tr>
    <tr>
        <td>
            <h3>
                <asp:literal id="PossibleAnswerLiteral" runat="server" text="Possible Answers" /></h3>
        </td>
    </tr>
    <tr>
        <td>
            <uc1:multiselecteditlistcontrol id="PossibleAnswerEditListControl" runat="server"
                allowsinglerowselectonly="true" longversioncolumncaption="Long version" shortversioncolumncaption="Short version" ShowMultimediaColumn="False"
                UploadControlFileFormats=".jpg,.jpe,.jpeg,.gif,.png,.bmp"/>
        </td>
    </tr>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <tr>
                    <td>
                        <dx:aspxcheckbox id="OfferOtherOptionCheckbox" runat="server" text="Offer OTHER option">
                        </dx:aspxcheckbox>
                    </td>
                    <td class="spacerlarge">
                    </td>
                    <td>
                        <dx:aspxcheckbox id="TextAreaOrTextCheckBox" runat="server" text="OTHER is a text area otherwise textbox">
                        </dx:aspxcheckbox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxcheckbox id="OfferOptOutCheckBox" runat="server" text="Offer Opt out choice">
            </dx:aspxcheckbox>
        </td>
    </tr>
</table>
