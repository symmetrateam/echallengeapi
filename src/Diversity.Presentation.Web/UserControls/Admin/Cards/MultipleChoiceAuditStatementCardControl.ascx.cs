﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class MultipleChoiceAuditStatementCardControl : BaseCardControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);

            base.OnInit(e);
        }

        public void Populate(Card card)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var questionCard = card.QuestionCard;

            if (questionCard == null)
            {
                return;
            }

            OfferOptOutCheckBox.Checked = questionCard.OfferOptOutChoice.HasValue
                                  ? questionCard.OfferOptOutChoice.Value
                                  : false;

            //populate the answers
            var answers = from q in questionCard.QuestionCard_GeneralAnswers
                          orderby q.SequenceNo
                          select q;

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var items = new List<SelectEditListItem>();

            foreach (var answer in answers)
            {
                var answerLanguages = (from a in answer.QuestionCard_GeneralAnswer_Languages
                                       where a.LanguageID == languageId
                                       select a).FirstOrDefault();

                if (answerLanguages != null)
                {
                    items.Add(
                        new SelectEditListItem
                        {
                            Id = answer.QuestionCard_GeneralAnswerID.ToString(),
                            IsSelected = answer.IsCorrectAnswer,
                            ExtendedText = answerLanguages.PossibleAnswer_LongVersion,
                            Text = answerLanguages.PossibleAnswer_ShortVersion
                        }
                        );
                }
            }

            PossibleAnswerEditListControl.Items = items;

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();

            if (questionCardLanguage == null)
            {
                return;
            }

            //set simple control values
            QuestionLongVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_LongVersion);
            QuestionShortVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_ShortVersion);
            AnswerExplainationLongVersionHtmlEditor.PopulateHtml(questionCardLanguage.AnswerExplanation_LongVersion);
            AnswerExplainationShortVersionHtmlEditor.PopulateHtml(questionCardLanguage.AnswerExplanation_ShortVersion);

            ShowCompanyResultsCheckBox.Checked = questionCard.MustShowCompanyResults;

            MinDataPointsSpinEdit.ClientEnabled = ShowCompanyResultsCheckBox.Checked;

            if (ShowCompanyResultsCheckBox.Checked)
            {
                MinDataPointsSpinEdit.Value = questionCard.MinDataPointsForCompanyResult;
            }
        }

        public bool OfferOptOut
        {
            get { return OfferOptOutCheckBox.Checked; }
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;

            var card = new QuestionCard_Language
            {
                Question_Statement_LongVersion = QuestionLongVersionHtmlControl.GetHtml(),
                Question_Statement_ShortVersion = QuestionShortVersionHtmlControl.GetHtml(),
                UserID_CreatedBy = userId,
                CreatedOnDateTime = currentDateTime,
                UserID_LastModifiedBy = userId,
                LastModifiedOnDateTime = currentDateTime,
                AnswerExplanation_ShortVersion = AnswerExplainationShortVersionHtmlEditor.GetHtml(),
                AnswerExplanation_LongVersion = AnswerExplainationLongVersionHtmlEditor.GetHtml()
            };

            return card;
        }

        public bool MustShowCompanyResults
        {
            get { return ShowCompanyResultsCheckBox.Checked; }
        }

        public int MinimumDataPointsForCompanyResult
        {
            get { return Convert.ToInt32(MinDataPointsSpinEdit.Value); }
        }

        public List<QuestionCard_GeneralAnswer> GetGeneralAnswers()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var answers = new List<QuestionCard_GeneralAnswer>();

            var possibleAnswers = PossibleAnswerEditListControl.Items;

            for (var i = 0; i < possibleAnswers.Count; i++)
            {
                var answer = new QuestionCard_GeneralAnswer
                {
                    CreatedOnDateTime = currentDateTime,
                    LastModifiedOnDateTime = currentDateTime,
                    UserID_CreatedBy = userId,
                    UserID_LastModifiedBy = userId,
                    SequenceNo = i,
                    IsCorrectAnswer = possibleAnswers[i].IsSelected
                };

                if (!string.IsNullOrEmpty(possibleAnswers[i].Id))
                {
                    answer.QuestionCard_GeneralAnswerID = Convert.ToInt32(possibleAnswers[i].Id);
                }

                var answerLanguage = new QuestionCard_GeneralAnswer_Language
                {
                    UserID_CreatedBy = userId,
                    CreatedOnDateTime = currentDateTime,
                    LastModifiedOnDateTime = currentDateTime,
                    UserID_LastModifiedBy = userId,
                    QuestionCard_GeneralAnswerID = answer.QuestionCard_GeneralAnswerID,
                    LanguageID = languageId,
                    PossibleAnswer_LongVersion = possibleAnswers[i].ExtendedText,
                    PossibleAnswer_ShortVersion = possibleAnswers[i].Text
                };

                answer.QuestionCard_GeneralAnswer_Languages.Add(answerLanguage);
                answers.Add(answer);
            }

            return answers;
        }

        protected void PossibleAnswersCallbackPanel_Callback(object sender, CallbackEventArgsBase e)
        {
            PossibleAnswerEditListControl.Items = GetDefaultPossibleAnswers();
            PossibleAnswerEditListControl.DataBind();            
        }

        static List<SelectEditListItem> GetDefaultPossibleAnswers()
        {
            var items = new List<SelectEditListItem>();

            items.Add(new SelectEditListItem()
                          {
                              Text = Resources.Diversity.LikertScaleStronglyDisagree
                          });

            items.Add(new SelectEditListItem()
            {
                Text = Resources.Diversity.LikertScaleDisagree
            });


            items.Add(new SelectEditListItem()
            {
                Text = Resources.Diversity.LikertScaleNeutral
            });

            items.Add(new SelectEditListItem()
            {
                Text = Resources.Diversity.LikertScaleAgree
            });

            items.Add(new SelectEditListItem()
            {
                Text = Resources.Diversity.LikertScaleStronglyAgree
            });



            return items;
        }
    }
}