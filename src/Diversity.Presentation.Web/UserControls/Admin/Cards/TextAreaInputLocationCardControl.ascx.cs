﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class TextAreaInputLocationCardControl : BaseCardControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Populate(Card card)
        {
            TextAreaControl.Populate(card);

            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var questionCard = card.QuestionCard;

            if (questionCard == null)
            {
                return;
            }

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();

            if (questionCardLanguage == null)
            {
                return;
            }

            LocationComboBox.Value = questionCardLanguage.StatementLocation;
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            var questionCardLanguage = TextAreaControl.GetQuestionCardLanguageDetail();

            questionCardLanguage.StatementLocation = LocationComboBox.Value.ToString();

            return questionCardLanguage;
        }
    }
}