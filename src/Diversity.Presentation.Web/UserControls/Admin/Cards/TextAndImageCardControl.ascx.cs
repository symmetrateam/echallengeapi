﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Diversity.Common;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class TextAndImageCardControl : BaseCardControl
    {
        private const string OrginalAssociatedFileUrlKey = "334E74EE-9C3F-41EE-BD21-0A4E11C09E00";
        private const string AssociatedFileKey = "BADFA274-7A0B-4A80-919E-645819064FE9";
        private const string AssociatedUrlNameKey = "9F62D803-C96D-4265-80CE-08662FA840C4";
        

        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);
        }

        public void Populate(Card card)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var questionCard = card.QuestionCard;

            if (questionCard == null)
            {
                return;
            }


            if (string.IsNullOrEmpty(questionCard.AssociatedImageUrl))
            {
                ImageLocationTextBox.ClientVisible = true;
                BrowseButton.ClientVisible = true;
                RemoveHyperLink.ClientVisible = false;
            }
            else
            {
                OriginalAssociatedImageUrl = questionCard.AssociatedImageUrl;
                AssociatedImageUrl = questionCard.AssociatedImageUrl;
                AssociatedImageUrlFriendlyName = questionCard.AssociatedImageUrlFriendlyName;

                ImageHyperLink.NavigateUrl =
                ResolveUrl(string.Format(@"{0}/{1}",
                                         ConfigurationHelper.LiveUploadLocation,
                                         questionCard.AssociatedImageUrl));
                ImageHyperLink.Text =
                questionCard.AssociatedImageUrlFriendlyName;

                TextImageHiddenField.Set("ImageHyperLinkNavigateUrl",
                            ImageHyperLink.NavigateUrl);
                TextImageHiddenField.Set("ImageHyperLinkText",
                                ImageHyperLink.Text);

                RemoveHyperLink.ClientVisible = true;
                BrowseButton.ClientVisible = false;
                ImageLocationTextBox.ClientVisible = false;
            }

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();

            if (questionCardLanguage == null)
            {
                return;
            }

            //set simple control values
            NameTextBox.Text = questionCardLanguage.Question_Statement_LongVersion;
        }

        string OriginalAssociatedImageUrl
        {
            get { return Convert.ToString(ViewState[OrginalAssociatedFileUrlKey]); }
            set { ViewState[OrginalAssociatedFileUrlKey] = value; }
        }

        public string AssociatedImageUrl
        {
            get
            {
                EnsureValues();
                return Convert.ToString(ViewState[AssociatedFileKey]);
            }
            private set { ViewState[AssociatedFileKey] = value; }
        }

        public string AssociatedImageUrlFriendlyName
        {
            get
            {
                EnsureValues();
                return Convert.ToString(ViewState[AssociatedUrlNameKey]);
            }
            private set { ViewState[AssociatedUrlNameKey] = value; }

        }

        public bool ShowImageControls
        {
            set 
            {
                ImageLocationLabel.Visible = value;
                ImageLocationTextBox.Visible= value;
                ImageHyperLink.Visible = value;
                BrowseButton.Visible = value;
                RemoveHyperLink.Visible = value;
            }
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            EnsureValues();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var currentDateTime = DateTime.Now;

            var card = new QuestionCard_Language
            {
                Question_Statement_LongVersion = NameTextBox.Text.Trim(),
                Question_Statement_ShortVersion = NameTextBox.Text.Trim(),
                UserID_CreatedBy = userId,
                CreatedOnDateTime = currentDateTime,
                UserID_LastModifiedBy = userId,
                LastModifiedOnDateTime = currentDateTime
            };

            return card;
        }

        protected void UploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            var splitOn = new[] { "." };
            var parts = e.UploadedFile.FileName.Split(splitOn, StringSplitOptions.RemoveEmptyEntries);
            var extension = parts.Length > 1 ? string.Format(".{0}", parts[parts.Length - 1]) : string.Empty;
            var filename = string.Format("{0}{1}", Guid.NewGuid(), extension);
            var fullPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
            e.UploadedFile.SaveAs(fullPath, false);
            var url = ResolveUrl(string.Format("{0}/{1}", ConfigurationHelper.TemporaryUploadLocation, filename));

            e.CallbackData = string.Format("{0};{1}", url, e.UploadedFile.FileName);
        }


        void EnsureValues()
        {
            if (TextImageHiddenField.Contains("ImageHyperLinkNavigateUrl"))
            {
                var navigateUrl = TextImageHiddenField.Get("ImageHyperLinkNavigateUrl") as string;
                var text = TextImageHiddenField.Get("ImageHyperLinkText") as string;

                if (string.IsNullOrEmpty(navigateUrl))
                {
                    return;
                }

                var parts = navigateUrl.Split(new[] { "/" },
                                              StringSplitOptions.
                                                  RemoveEmptyEntries);
                if (parts.Count() > 0)
                {
                    var filename = parts[parts.Length - 1];

                    if (string.Compare(filename, OriginalAssociatedImageUrl, true) == 0)
                    {
                        return;
                    }

                    var oldFilename = OriginalAssociatedImageUrl;

                    AssociatedImageUrl = filename;
                    AssociatedImageUrlFriendlyName = text;
                }
            }
        }

        public void MoveImageToLiveFolder()
        {
            if (TextImageHiddenField.Contains("ImageHyperLinkNavigateUrl"))
            {
                var navigateUrl = TextImageHiddenField.Get("ImageHyperLinkNavigateUrl") as string;
                var text = TextImageHiddenField.Get("ImageHyperLinkText") as string;

                if (string.IsNullOrEmpty(navigateUrl))
                {
                    return;
                }

                var parts = navigateUrl.Split(new[] { "/" },
                                              StringSplitOptions.
                                                  RemoveEmptyEntries);
                if (parts.Count() > 0)
                {
                    var filename = parts[parts.Length - 1];

                    if (string.Compare(filename, OriginalAssociatedImageUrl, true) == 0)
                    {
                        return;
                    }

                    var oldFilename = OriginalAssociatedImageUrl;

                    var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
                    var destinationPath =
                        Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                     filename);

                    var oldFileDeletePath = string.Empty;
                    if (!string.IsNullOrEmpty(OriginalAssociatedImageUrl))
                    {
                        oldFileDeletePath =
                            Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                            oldFilename);
                    }


                    File.Move(tempPath, destinationPath);

                    if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                    {
                        File.Delete(oldFileDeletePath);
                    }
                }
            }

        }

    }
}