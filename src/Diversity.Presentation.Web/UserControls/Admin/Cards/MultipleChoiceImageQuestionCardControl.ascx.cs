﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diversity.Common;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class MultipleChoiceImageQuestionCardControl : BaseCardControl
    {
        public const string OriginalMultimediaListKey = "3C30B806-CA4C-4B52-A23A-77F44F1516D7";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        List<KeyValuePair<int, string>> OriginalMultimediaList
        {
            get { return ViewState[OriginalMultimediaListKey] as List<KeyValuePair<int, string>>; }
            set { ViewState[OriginalMultimediaListKey] = value; }
        }

        public void Populate(Card card)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var questionCard = card.QuestionCard;

            if (questionCard == null)
            {
                return;
            }

            OfferOtherOptionCheckbox.Checked = questionCard.OfferOTHERTextOption;
            TextAreaOrTextCheckBox.Checked = questionCard.TextOptionInputIsTextArea;

            //populate the answers
            var answers = from q in questionCard.QuestionCard_GeneralAnswers
                          orderby q.SequenceNo
                          select q;

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            List<SelectEditListItem> items = new List<SelectEditListItem>();
            OriginalMultimediaList = new List<KeyValuePair<int, string>>();
            foreach (var answer in answers)
            {
                var answerLanguages = (from a in answer.QuestionCard_GeneralAnswer_Languages
                                       where a.LanguageID == languageId
                                       select a).FirstOrDefault();

                if (answerLanguages != null)
                {
                    items.Add(
                        new SelectEditListItem
                        {
                            Id = answer.QuestionCard_GeneralAnswerID.ToString(),
                            IsSelected = answer.IsCorrectAnswer,
                            ExtendedText = answerLanguages.PossibleAnswer_LongVersion,
                            Text = answerLanguages.PossibleAnswer_ShortVersion,
                            MultimediaUrl = answerLanguages.PossibleAnswerMultimediaURL,
                            MultimediaUrlFriendlyName = answerLanguages.PossibleAnswerMultimediaURLFriendlyName,
                            IsMultimediaLive = true
                        }
                        );

                    OriginalMultimediaList.Add(new KeyValuePair<int, string>(answer.QuestionCard_GeneralAnswerID,
                                                                             answerLanguages.PossibleAnswerMultimediaURL));
                }
            }

            PossibleAnswerEditListControl.Items = items;

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();

            if (questionCardLanguage == null)
            {
                return;
            }

            //set simple control values
            QuestionLongVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_LongVersion);
            QuestionShortVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_ShortVersion);
            AnswerExplainationLongVersionHtmlEditor.PopulateHtml(questionCardLanguage.AnswerExplanation_LongVersion);
            AnswerExplainationShortVersionHtmlEditor.PopulateHtml(questionCardLanguage.AnswerExplanation_ShortVersion);
        }

        public bool OfferOtherTextOption
        {
            get { return OfferOtherOptionCheckbox.Checked; }
        }

        public bool TextOptionInputIsTextArea
        {
            get { return TextAreaOrTextCheckBox.Checked; }
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var currentDateTime = DateTime.Now;

            var card = new QuestionCard_Language
            {
                Question_Statement_LongVersion = QuestionLongVersionHtmlControl.GetHtml(),
                Question_Statement_ShortVersion = QuestionShortVersionHtmlControl.GetHtml(),
                AnswerExplanation_LongVersion = AnswerExplainationLongVersionHtmlEditor.GetHtml(),
                AnswerExplanation_ShortVersion = AnswerExplainationShortVersionHtmlEditor.GetHtml(),
                UserID_CreatedBy = userId,
                CreatedOnDateTime = currentDateTime,
                UserID_LastModifiedBy = userId,
                LastModifiedOnDateTime = currentDateTime
            };

            return card;
        }

        public List<QuestionCard_GeneralAnswer> GetGeneralAnswers()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();
            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var answers = new List<QuestionCard_GeneralAnswer>();

            var possibleAnswers = PossibleAnswerEditListControl.Items;

            for (var i = 0; i < possibleAnswers.Count; i++)
            {
                var answer = new QuestionCard_GeneralAnswer
                {
                    CreatedOnDateTime = currentDateTime,
                    LastModifiedOnDateTime = currentDateTime,
                    UserID_CreatedBy = userId,
                    UserID_LastModifiedBy = userId,
                    SequenceNo = i,
                    IsCorrectAnswer = possibleAnswers[i].IsSelected
                };

                if (!string.IsNullOrEmpty(possibleAnswers[i].Id))
                {
                    answer.QuestionCard_GeneralAnswerID = Convert.ToInt32(possibleAnswers[i].Id);
                }

                var answerLanguage = new QuestionCard_GeneralAnswer_Language
                {
                    UserID_CreatedBy = userId,
                    CreatedOnDateTime = currentDateTime,
                    LastModifiedOnDateTime = currentDateTime,
                    UserID_LastModifiedBy = userId,
                    QuestionCard_GeneralAnswerID = answer.QuestionCard_GeneralAnswerID,
                    LanguageID = languageId,
                    PossibleAnswer_LongVersion = possibleAnswers[i].ExtendedText,
                    PossibleAnswer_ShortVersion = possibleAnswers[i].Text,
                    PossibleAnswerMultimediaURL = possibleAnswers[i].MultimediaUrl,
                    PossibleAnswerMultimediaURLFriendlyName = possibleAnswers[i].MultimediaUrlFriendlyName
                };

                answer.QuestionCard_GeneralAnswer_Languages.Add(answerLanguage);
                answers.Add(answer);
            }

            return answers;
        }

        public void MoveMultimediaFilesToLiveFolder()
        {
            var possibleAnswers = PossibleAnswerEditListControl.Items.Where(p => !p.IsMultimediaLive).ToList();

            for (var i = 0; i < possibleAnswers.Count; i++)
            {
                var mediaUrl = possibleAnswers[i].MultimediaUrl;

                if (string.IsNullOrWhiteSpace(mediaUrl) || string.IsNullOrEmpty(mediaUrl))
                {
                    continue;
                }

                var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), mediaUrl);
                var destinationPath =
                    Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                 mediaUrl);

                File.Move(tempPath, destinationPath);

                int id = -1;
                if(Int32.TryParse(possibleAnswers[i].Id, out id))
                {
                    var originalValueEntry = OriginalMultimediaList.FirstOrDefault(m => m.Key == id);

                    var oldFileDeletePath = string.Empty;
                    if (!string.IsNullOrEmpty(originalValueEntry.Value))
                    {
                        oldFileDeletePath =
                            Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                            originalValueEntry.Value);
                    }
                
                    if (!string.IsNullOrEmpty(originalValueEntry.Value) && File.Exists(oldFileDeletePath))
                    {
                        File.Delete(oldFileDeletePath);
                    }
                }     
            }
        }

        public bool IsValid(out List<string> errors)
        {
            errors = new List<string>();

            if (PossibleAnswerEditListControl.Items.All(i => !i.IsSelected))
            {
                errors.Add("At least one of the items must be selected as the correct answer.");
                return false;
            }

            return true;
        }
    }
}