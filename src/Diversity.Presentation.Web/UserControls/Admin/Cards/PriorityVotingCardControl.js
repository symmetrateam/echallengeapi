﻿function SelectTopRadioButton_CheckChanged(s, e) {
    if (s.GetChecked()) {
        TopPrioritiesSpinEdit.SetEnabled(true);
        TopPrioritiesSpinEdit.SetNumber(1);
        TopPrioritiesSpinEdit.SetMinValue(1);
    }
    else {
        TopPrioritiesSpinEdit.SetEnabled(false);
        TopPrioritiesSpinEdit.SetNumber(0);
        TopPrioritiesSpinEdit.SetMinValue(0);
    }
}