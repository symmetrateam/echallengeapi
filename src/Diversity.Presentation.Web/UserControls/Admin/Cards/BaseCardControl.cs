﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public abstract class BaseCardControl : System.Web.UI.UserControl
    {
        [DefaultValue("100%")]
        public Unit Width
        {
            get
            {
                if (ViewState["Width"] == null)
                {
                    ViewState.Add("Width", Unit.Percentage(100));
                }
                return (Unit)ViewState["Width"];
            }
            set { ViewState["Width"] = value; }
        }
    }
}