﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultipleCheckBoxImageQuestionCard.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.Admin.Cards.MultipleCheckBoxMultiMediaQuestionCard" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ register src="../../MultiSelectEditListControl.ascx" tagname="EditListControl"
    tagprefix="uc1" %>


<%@ Register TagPrefix="uc2" TagName="BasicHtmlEditorControl" Src="~/UserControls/BasicHtmlEditorControl.ascx" %>
<table width='<%=Width%>'>
    <tr>
        <td>
            <dx:aspxpagecontrol id="QuestionPageControl" runat="server" activetabindex="0" width="100%">
                <tabpages>
                    <dx:tabpage name="ShortQuestionTab" text="Short Version of Question">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                 <uc2:BasicHtmlEditorControl ID="QuestionShortVersionHtmlControl" runat="server" />    
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage name="LongQuestionTab" text="Long Version of Question">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                <uc2:BasicHtmlEditorControl ID="QuestionLongVersionHtmlControl" runat="server" />  
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
    </tr>
    <tr>
        <td>
            <h3>
                <asp:literal id="PossibleAnswerLiteral" runat="server" text="Possible Answers" /></h3>
        </td>
    </tr>
    <tr>
        <td>
            <uc1:editlistcontrol id="PossibleAnswerEditListControl" runat="server" allowsinglerowselectonly="false"
                longversioncolumncaption="Long version" shortversioncolumncaption="Short version" ShowMultimediaColumn="True"/>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxcheckbox id="QuestionIsOnlyCorrectIfAllAnswersAreTrueCheckBox" runat="server"
                text="The player's response is only correct if he/she selects ALL the correct answers only, not just one of them">
            </dx:aspxcheckbox>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxpagecontrol id="ExplanationPageControl" runat="server" activetabindex="0"
                width="100%">
                <tabpages>
                    <dx:tabpage name="ShortExplanationTab" text="Short Explanation of Answer">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                 <uc2:BasicHtmlEditorControl ID="AnswerExplainationShortVersionHtmlEditor" runat="server" />        
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                    <dx:tabpage name="LongExplanationTab" text="Long Explanation of Answer">
                        <contentcollection>
                            <dx:contentcontrol runat="server">
                                <uc2:BasicHtmlEditorControl ID="AnswerExplainationLongVersionHtmlEditor" runat="server" />    
                            </dx:contentcontrol>
                        </contentcollection>
                    </dx:tabpage>
                </tabpages>
            </dx:aspxpagecontrol>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td class="spacerlarge">
        </td>
        <td>
        </td>
    </tr>
</table>