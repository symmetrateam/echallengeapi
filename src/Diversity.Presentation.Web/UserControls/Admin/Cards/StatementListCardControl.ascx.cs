﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Diversity.Common;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin.Cards
{
    public partial class StatementListCardControl : System.Web.UI.UserControl
    {
        private const string OrginalAssociatedFileUrlKey = "CCDCEB73-26F7-47CB-A32D-BAE93179CE1D";
        private const string AssociatedFileKey = "E21BEF92-986E-4880-B3F6-8C55BC432A71";
        private const string AssociatedUrlNameKey = "6924C537-C874-424A-94E0-9AAACC0DD5D0";

        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);
        }

        public void Populate(Card card)
        {
            var lcid = (Page as BasePage).CurrentContext.Culture;

            var dataContext = new EChallengeDataContext();

            var languageId = (from l in dataContext.Languages
                              where l.LCID == lcid
                              select l.LanguageID).FirstOrDefault();

            var questionCard = card.QuestionCard;

            if (questionCard == null)
            {
                return;
            }


            if (string.IsNullOrEmpty(questionCard.AssociatedImageUrl))
            {
                ImageLocationTextBox.ClientVisible = true;
                BrowseButton.ClientVisible = true;
                RemoveHyperLink.ClientVisible = false;
            }
            else
            {
                OriginalAssociatedImageUrl = questionCard.AssociatedImageUrl;
                AssociatedImageUrl = questionCard.AssociatedImageUrl;
                AssociatedImageUrlFriendlyName = questionCard.AssociatedImageUrlFriendlyName;

                ImageHyperLink.NavigateUrl =
                ResolveUrl(string.Format(@"{0}/{1}",
                                         ConfigurationHelper.LiveUploadLocation,
                                         questionCard.AssociatedImageUrl));
                ImageHyperLink.Text =
                questionCard.AssociatedImageUrlFriendlyName;

                StatenentListHiddenField.Set("ImageHyperLinkNavigateUrl",
                            ImageHyperLink.NavigateUrl);
                StatenentListHiddenField.Set("ImageHyperLinkText",
                                ImageHyperLink.Text);

                RemoveHyperLink.ClientVisible = true;
                BrowseButton.ClientVisible = false;
                ImageLocationTextBox.ClientVisible = false;
            }

            var questionCardLanguage = (from q in questionCard.QuestionCard_Languages
                                        where q.LanguageID == languageId
                                        select q).FirstOrDefault();

            if (questionCardLanguage == null)
            {
                return;
            }
            
            QuestionShortVersionHtmlControl.PopulateHtml(questionCardLanguage.Question_Statement_LongVersion);
            ListHeaderTextBox.Text = questionCardLanguage.StatementHeader;
            
            var items = new List<EditListItem>();

            var questionStatements = (from cl in questionCard.QuestionCard_Languages
                                      join s in questionCard.QuestionCardStatement_Languages on new { cl.QuestionCardID, cl.LanguageID } equals
                                          new {s.QuestionCardID, s.LanguageID}
                                      select s
                                     );

            foreach (var statement in questionStatements)
            {
                items.Add(new EditListItem()
                              {
                                  Id = statement.QuestionCardStatement_LanguageID.ToString(),
                                  Text = statement.Statement_ShortVersion
                              });
            }

            StatementsEditListControl.Items = items;
        }

        string OriginalAssociatedImageUrl
        {
            get { return Convert.ToString(ViewState[OrginalAssociatedFileUrlKey]); }
            set { ViewState[OrginalAssociatedFileUrlKey] = value; }
        }

        public string AssociatedImageUrl
        {
            get
            {
                EnsureValues();
                return Convert.ToString(ViewState[AssociatedFileKey]);
            }
            private set { ViewState[AssociatedFileKey] = value; }
        }

        public string AssociatedImageUrlFriendlyName
        {
            get
            {
                EnsureValues();
                return Convert.ToString(ViewState[AssociatedUrlNameKey]);
            }
            private set { ViewState[AssociatedUrlNameKey] = value; }

        }

        void EnsureValues()
        {
            if (StatenentListHiddenField.Contains("ImageHyperLinkNavigateUrl"))
            {
                var navigateUrl = StatenentListHiddenField.Get("ImageHyperLinkNavigateUrl") as string;
                var text = StatenentListHiddenField.Get("ImageHyperLinkText") as string;

                if (string.IsNullOrEmpty(navigateUrl))
                {
                    return;
                }

                var parts = navigateUrl.Split(new[] { "/" },
                                              StringSplitOptions.
                                                  RemoveEmptyEntries);
                if (parts.Count() > 0)
                {
                    var filename = parts[parts.Length - 1];

                    if (string.Compare(filename, OriginalAssociatedImageUrl, true) == 0)
                    {
                        return;
                    }

                    var oldFilename = OriginalAssociatedImageUrl;

                    AssociatedImageUrl = filename;
                    AssociatedImageUrlFriendlyName = text;
                }
            }
        }

        public QuestionCard_Language GetQuestionCardLanguageDetail()
        {
            EnsureValues();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var currentDateTime = DateTime.Now;

            var card = new QuestionCard_Language
            {
                Question_Statement_LongVersion = QuestionShortVersionHtmlControl.GetHtml(),
                Question_Statement_ShortVersion = QuestionShortVersionHtmlControl.GetHtml(),
                UserID_CreatedBy = userId,
                CreatedOnDateTime = currentDateTime,
                UserID_LastModifiedBy = userId,
                LastModifiedOnDateTime = currentDateTime,
                StatementHeader = ListHeaderTextBox.Text
            };

            return card;
        }

        public IEnumerable<QuestionCardStatement_Language> GetCardStatements()
        {
            var items = StatementsEditListControl.Items;

            var statements = new List<QuestionCardStatement_Language>();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var currentDateTime = DateTime.Now;
            var count = 0;

            foreach (var item in items)
            {
                statements.Add(new QuestionCardStatement_Language()
                                   {                       
                                       QuestionCardStatement_LanguageID = Convert.ToInt32(item.Id),
                                       UserID_CreatedBy = userId,
                                       CreatedOnDateTime = currentDateTime,
                                       UserID_LastModifiedBy = userId,
                                       LastModifiedOnDateTime = currentDateTime,
                                       SequenceNo = count,
                                       Statement_LongVersion = item.Text,
                                       Statement_ShortVersion = item.Text
                                   });

                count++;
            }

            return statements;
        }

        protected void UploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            var splitOn = new[] { "." };
            var parts = e.UploadedFile.FileName.Split(splitOn, StringSplitOptions.RemoveEmptyEntries);
            var extension = parts.Length > 1 ? string.Format(".{0}", parts[parts.Length - 1]) : string.Empty;
            var filename = string.Format("{0}{1}", Guid.NewGuid(), extension);
            var fullPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
            e.UploadedFile.SaveAs(fullPath, false);
            var url = ResolveUrl(string.Format("{0}/{1}", ConfigurationHelper.TemporaryUploadLocation, filename));

            e.CallbackData = string.Format("{0};{1}", url, e.UploadedFile.FileName);
        }

        public void MoveImageToLiveFolder()
        {
            if (StatenentListHiddenField.Contains("ImageHyperLinkNavigateUrl"))
            {
                var navigateUrl = StatenentListHiddenField.Get("ImageHyperLinkNavigateUrl") as string;

                if (string.IsNullOrEmpty(navigateUrl))
                {
                    return;
                }

                var parts = navigateUrl.Split(new[] { "/" },
                                              StringSplitOptions.
                                                  RemoveEmptyEntries);
                if (parts.Count() > 0)
                {
                    var filename = parts[parts.Length - 1];

                    if (string.Compare(filename, OriginalAssociatedImageUrl, true) == 0)
                    {
                        return;
                    }

                    var oldFilename = OriginalAssociatedImageUrl;

                    var tempPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
                    var destinationPath =
                        Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                                     filename);

                    var oldFileDeletePath = string.Empty;
                    if (!string.IsNullOrEmpty(OriginalAssociatedImageUrl))
                    {
                        oldFileDeletePath =
                            Path.Combine(MapPath(ConfigurationHelper.LiveUploadLocation),
                            oldFilename);
                    }


                    File.Move(tempPath, destinationPath);

                    if (!string.IsNullOrEmpty(oldFilename) && File.Exists(oldFileDeletePath))
                    {
                        File.Delete(oldFileDeletePath);
                    }
                }
            }

        }

    }
}