﻿<%@ control language="C#" autoeventwireup="true" codebehind="ManageDistributorControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.ManageDistributorControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>

<style type="text/css">
    .inline
    {
        display: inline-table;
    }
</style>
<script id="dxis_UserManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/ManageDistributorControl.js") %>'>

</script>
<asp:linqdatasource id="DistributorDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" tablename="Distributors" onselecting="DistributorDataSource_Selecting">
</asp:linqdatasource>
<dx:aspxgridview id="DistributorGridView" runat="server" autogeneratecolumns="False"
    datasourceid="DistributorDataSource" keyfieldname="DistributorID" width="100%"
    onrowinserting="DistributorGridView_RowInserting" 
    onrowdeleting="DistributorGridView_RowDeleting" onrowupdating="DistributorGridView_RowUpdating"
    clientinstancename="DistributorGridView" oninitnewrow="DistributorGridView_InitNewRow"
    onrowvalidating="DistributorGridView_RowValidating">
    <ClientSideEvents CustomButtonClick="DistributorGridView_CustomButtonClick"></ClientSideEvents>
    <settingsbehavior enablerowhottrack="True" columnresizemode="NextColumn" ConfirmDelete="True" />
    <SettingsText ConfirmDelete="Are you sure you want to delete this distributor"></SettingsText>
    <settingspager alwaysshowpager="True">
        <allbutton visible="True">
            <image tooltip="Show All Distributors">
            </image>
        </allbutton>
    </settingspager>
    <settings showfilterrow="true" showgrouppanel="true" />
    <settingsediting mode="EditFormAndDisplayRow" />
    <columns>
        <dx:gridviewcommandcolumn visibleindex="0" 
            width="100px" ShowEditButton="True" ShowNewButton="False">
            <CustomButtons>
                <dx:gridviewcommandcolumncustombutton ID="DeleteDistributorButton" Text="Delete"></dx:gridviewcommandcolumncustombutton>
            </CustomButtons>            
            <headercaptiontemplate>
                <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="AddNewRow(DistributorGridView);return false;"
                    text="New" />
            </headercaptiontemplate>
        </dx:gridviewcommandcolumn>
        <dx:gridviewdatatextcolumn fieldname="DistributorID" readonly="True" visible="false"
            visibleindex="0">
            <editformsettings visible="False" />
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="DistributorName" caption="Distributor Name"
            visibleindex="1"  width="300px">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatacheckcolumn caption="Create Products" fieldname="DistributorCanCreateProducts"
            showincustomizationform="True" visibleindex="2">
        </dx:gridviewdatacheckcolumn>
        <dx:gridviewdatatextcolumn caption="Max Products" fieldname="MaxNoOfProductsDistributorCanCreate"
            showincustomizationform="True" visibleindex="3">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn caption="Tel No" fieldname="DistributorTel1" showincustomizationform="True"
            visibleindex="4">
        </dx:gridviewdatatextcolumn>
    </columns>
    <styles>
        <alternatingrow enabled="True">
        </alternatingrow>
    </styles>
    <templates>
        <editform>
            <table>
                <tr>
                    <td>
                        <dx:aspxlabel id="DistributorNameLabel" runat="server" text="Distributor Name"  />
                    </td>
                    <td>
                        <dx:aspxtextbox id="DistributorNameTextBox" runat="server" text='<%# Bind("DistributorName") %>'
                            tabindex="1" validationsettings-validationgroup='<%#  Container.ValidationGroup  %>'>
                            <validationsettings errordisplaymode="ImageWithTooltip" setfocusonerror="True" causesvalidation="True">
                                <requiredfield isrequired="True" errortext="Distributor name is required" />
                            </validationsettings>
                        </dx:aspxtextbox>
                    </td>
                    <td class="spacer">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <dx:aspxlabel id="PhysicalAddressLabel" runat="server" text="Physical Address"  />
                    </td>
                    <td valign="top">
                        <table style="border-collapse: collapse">
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PhysicalAddress1TextBox" runat="server" clientinstancename="PhysicalAddress1TextBox"
                                        nulltext="Address Line 1" text='<%# Bind("PhysicalAddress1") %>' 
                                        tabindex="2">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PhysicalAddress2TextBox" runat="server" clientinstancename="PhysicalAddress2TextBox"
                                        nulltext="Address Line 2" text='<%# Bind("PhysicalAddress2") %>' tabindex="3">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PhysicalStateProvinceTextBox" runat="server" clientinstancename="PhysicalStateProvinceTextBox"
                                        nulltext="State or Province" text='<%# Bind("PhysicalStateProvince") %>' 
                                        tabindex="4">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PhysicalCountryTextBox" runat="server" nulltext="Country" clientinstancename="PhysicalCountryTextBox"
                                        text='<%# Bind("PhysicalCountry") %>' 
                                        tabindex="5">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PhysicalPostCodeTextBox" runat="server" clientinstancename="PhysicalPostCodeTextBox"
                                        nulltext="Post Code" text='<%# Bind("PhysicalPostalCode") %>' 
                                        tabindex="6">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="spacer">
                    </td>
                    <td valign="top">
                        <dx:aspxlabel id="PostalAddressLabel" runat="server" text="Postal Address"  />
                    </td>
                    <td valign="top">
                        <table style="border-collapse: collapse">
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PostalAddress1TextBox" runat="server" clientinstancename="PostalAddress1TextBox"
                                        nulltext="Address Line 1" text='<%# Bind("PostalAddress1") %>' tabindex="7">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PostalAddress2TextBox" runat="server" clientinstancename="PostalAddress2TextBox"
                                        nulltext="Address Line 2" text='<%# Bind("PostalAddress2") %>' tabindex="8">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PostalStateProvinceTextBox" runat="server" clientinstancename="PostalStateProvinceTextBox"
                                        nulltext="State or Province" text='<%# Bind("PostalStateProvince") %>' tabindex="9">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PostalCountryTextBox" runat="server" nulltext="Country" clientinstancename="PostalCountryTextBox"
                                        text='<%# Bind("PostalCountry") %>' 
                                        tabindex="10">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxtextbox id="PostalPostCodeTextBox" runat="server" nulltext="Post Code" clientinstancename="PostalPostCodeTextBox"
                                        text='<%# Bind("PostalPostalCode") %>' 
                                        tabindex="11">
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td class="spacer">
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx:aspxcheckbox id="UsePhysicalCheckBox" runat="server" text="Same as Physical Address"
                            clientinstancename="UsePhysicalCheckBox" checkstate="Unchecked" 
                            tabindex="12">
                            <clientsideevents checkedchanged="function(s,e){OnUsePhysicalCheckBoxCheckChanged(s,e);}" />
                        </dx:aspxcheckbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:aspxlabel id="TelephoneLabel" runat="server" text="Tel No."  />
                    </td>
                    <td>
                        <dx:aspxtextbox id="TelNumberTextBox" runat="server" text='<%# Bind("DistributorTel1") %>'
                             tabindex="13">
                        </dx:aspxtextbox>
                    </td>
                    <td class="spacer">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:aspxlabel id="EmailLabel" runat="server" text="Email"  />
                    </td>
                    <td colspan="3">
                        <dx:aspxtextbox id="EmailTextBox" runat="server" text='<%# Bind("DistributorEmailAddress") %>'
                            validationsettings-validationgroup='<%#  Container.ValidationGroup  %>' 
                            tabindex="14">
                            <validationsettings setfocusonerror="true" errordisplaymode="ImageWithTooltip">
                                <regularexpression validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    errortext="Invalid email" />
                            </validationsettings>
                        </dx:aspxtextbox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <dx:aspxmemo id="NotesMemo" runat="server" nulltext="Notes" width="100%" rows="5"
                            text='<%# Bind("DistributorNotes") %>' 
                            tabindex="15">
                        </dx:aspxmemo>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <dx:aspxcheckbox id="CreateProductCheckBox" runat="server" text="Can Create/Own Products"
                            clientinstancename="CreateProductCheckBox" value='<%# Bind("DistributorCanCreateProducts") %>'
                            checkstate="Unchecked" tabindex="16">
                            <clientsideevents checkedchanged="function(s,e){CreateProductCheckBox_OnCheckChanged(s,e);}" />
                        </dx:aspxcheckbox>
                    </td>
                    <td class="spacer">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" valign="middle">
                        <dx:aspxlabel id="NoProductLabel" runat="server" text="Number of products that can be created"
                            cssclass="inline" />
                        <dx:aspxspinedit id="NoProductSpinEdit" runat="server" numbertype="Integer" number="1"
                            clientinstancename="NoProductSpinEdit" minvalue="1" maxvalue="99999999" allowuserinput="False"
                            height="15px" value='<%# Bind("MaxNoOfProductsDistributorCanCreate") %>' width="50px"
                            cssclass="inline" allownull="False" 
                            tabindex="17" oninit="NoProductSpinEdit_Init">
                        </dx:aspxspinedit>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <div id="aspxGridViewUpdateCancelButtonBar">
                            <div id="aspxGridViewCancelButton">
                                <dx:aspxgridviewtemplatereplacement id="CancelButton" replacementtype="EditFormCancelButton"
                                    runat="server" columnid="" tabindex="19" />
                            </div>
                            &nbsp;&nbsp;
                            <dx:aspxgridviewtemplatereplacement id="UpdateButton" replacementtype="EditFormUpdateButton"
                                runat="server" columnid=""  tabindex="18" />
                        </div>

                        <dx:aspxdateedit id="LastModifiedOnDateTimeDateEdit" runat="server" clientvisible="false" 
                                    value='<%#Bind("LastModifiedOnDateTime") %>' readonly="true"> 
                                </dx:aspxdateedit>
                    </td>
                </tr>
            </table>
        </editform>
    </templates>
</dx:aspxgridview>
