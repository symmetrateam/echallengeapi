﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductManagementContol.ascx.cs"
    Inherits="Diversity.Presentation.Web.UserControls.Admin.ProductManagementContol" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<script id="dxis_ProductManagementControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/ProductManagementControl.js") %>'>

</script>
<dx:ASPxGridView ID="ProductGridView" runat="server" AutoGenerateColumns="False"
    DataSourceID="ProductDataSource" KeyFieldName="ProductID" Width="100%" EnableRowsCache="false"
    OnRowInserting="ProductGridView_RowInserting" ClientInstanceName="ProductGridView" OnRowDeleting="ProductGridView_OnRowDeleting"
    OnInitNewRow="ProductGridView_InitNewRow">
    <ClientSideEvents CustomButtonClick="ProductGridView_CustomButtonClick"></ClientSideEvents>
    <SettingsBehavior ColumnResizeMode="NextColumn" EnableRowHotTrack="True" />
    <SettingsPager AlwaysShowPager="True">
        <AllButton Visible="True">
            <Image ToolTip="Show All Products">
            </Image>
        </AllButton>
    </SettingsPager>
    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
    <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
    <Columns>
        <dx:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
            <CustomButtons>
                <dx:GridViewCommandColumnCustomButton ID="DeleteProductButton" Text="Delete">
                </dx:GridViewCommandColumnCustomButton>
            </CustomButtons>
            <HeaderCaptionTemplate>
                <asp:LinkButton ID="NewLinkButton" runat="server" OnClientClick="ProductGridView.AddNewRow();return false;" Text="New"/>
            </HeaderCaptionTemplate>
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn FieldName="DisplayProductName" Caption="Product Name"
            VisibleIndex="3">
        </dx:GridViewDataTextColumn>
    </Columns>
    <Templates>
        <EditForm>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="ProductNameLabel" runat="server" Text="Product Name" />
                    </td>
                    <td class="spacer"></td>
                    <td>
                        <dx:ASPxTextBox ID="ProductNameTextBox" runat="server" ValidationSettings-ValidationGroup='<%# Container.ValidationGroup %>'>
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true">
                                <RequiredField IsRequired="true" ErrorText="Product name is required" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="ProductDescriptionLabel" runat="server" Text="Description" />
                    </td>
                    <td class="spacer"></td>
                    <td>
                        <dx:ASPxTextBox ID="ProductDescriptionTextBox" runat="server" Width="300px">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="DefaultThemeLabel" runat="server" Text="Default Theme" />
                    </td>
                    <td class="spacer"></td>
                    <td>
                        <dx:ASPxComboBox ID="DefaultThemeComboBox" runat="server" DropDownStyle="DropDownList"
                            ValueType="System.String" validationsetting-validationgroup="EditForm" ClientInstanceName="DefaultThemeComboBox"
                            ValidationSettings-ValidationGroup='<%# Container.ValidationGroup %>'>
                            <ClientSideEvents Validation="DefaultThemeComboBox_Validate" />
                            <ValidationSettings SetFocusOnError="true" ErrorDisplayMode="ImageWithTooltip" ErrorText="Default theme is a required field">
                                <RequiredField IsRequired="true" ErrorText="Default theme is a required field" />
                            </ValidationSettings>
                            <Items>
                                <dx:ListEditItem Value="Apple" Text="Apple" />
                                <dx:ListEditItem Value="Default Theme" Text="Default Theme" />
                                <dx:ListEditItem Value="Windows" Text="Windows" />
                            </Items>
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br />
                        <dx:ASPxLabel ID="DistributorAccessValidateMessageLabel" runat="server" Text="At least one distributor must be owner with access granted"
                            ClientInstanceName="DistributorAccessValidateMessageLabel">
                        </dx:ASPxLabel>
                        <br />
                        <br />
                        <dx:ASPxGridView ID="DistributorAccessGridView" runat="server" AutoGenerateColumns="False"
                            KeyFieldName="DistributorID" DataSourceID="DistributorAccessDataSource" Caption="Distributor Access"
                            ClientInstanceName="DistributorAccessGridView"
                            OnCommandButtonInitialize="DistributorAccessGridView_OnCommandButtonInitialize" EnableRowsCache="false" OnDataBound="DistributorAccessGridView_DataBound">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" />
                            <ClientSideEvents SelectionChanged="DistributorAccessGridView_SelectionChanged" />
                            <Columns>
                                <dx:GridViewDataCheckColumn FieldName="IsGranted" Caption="Grant" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox ID="IsAcessGrantedCheckBox" runat="server" Value='<%# Eval("IsGranted") %>'
                                            OnInit="IsAcessGrantedCheckBox_Init">
                                            <ClientSideEvents CheckedChanged="IsAccessGrantedCheckBox_CheckChanged" />
                                        </dx:ASPxCheckBox>
                                    </DataItemTemplate>
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewDataTextColumn FieldName="DistributorName" Caption="Distributor Name"
                                    VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="2" Caption="Owner">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataComboBoxColumn FieldName="QuestionMix" Caption="QuestionMix" VisibleIndex="3">
                                    <DataItemTemplate>
                                        <dx:ASPxComboBox ID="QuestionMixComboBox" runat="server" Value='<%# Eval("QuestionMix") %>'
                                            DropDownStyle="DropDownList" OnInit="QuestionMixComboBox_Init" ClientEnabled='<%# Eval("IsGranted") %>'>
                                            <Items>
                                                <dx:ListEditItem Text="Mixed" Value="MIXED" />
                                                <dx:ListEditItem Text="Ratified Only" Value="RATIFIEDONLY" />
                                                <dx:ListEditItem Text="Unratified Only" Value="UNRATIFIEDONLY" />
                                            </Items>
                                            <ClientSideEvents Validation="QuestionMixComboxBox_Validate" />
                                            <ValidationSettings SetFocusOnError="true" ErrorDisplayMode="ImageWithTooltip" ErrorText="Question mix is a required field">
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </DataItemTemplate>
                                </dx:GridViewDataComboBoxColumn>
                                <dx:GridViewDataTextColumn FieldName="MaxNoOfProductsDistributorCanCreate" Caption="Max Products Can Be Owned"
                                    VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CurrentProductCount" Caption="Actual Products Owned"
                                    VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="HasAvailableProducts" Caption="Owner Status"
                                    VisibleIndex="6">
                                    <DataItemTemplate>
                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Available" ClientVisible='<%# (bool)Eval("HasAvailableProducts") %>' />
                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Unavailable" ClientVisible='<%# !(bool)Eval("HasAvailableProducts") %>' />
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridView>
                    </td>
                </tr>
            </table>
            <div id="aspxGridViewUpdateCancelButtonBar">
                <div id="aspxGridViewCancelButton">
                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                        runat="server" ColumnID="" tabindex="19" />
                </div>
                &nbsp;&nbsp;
                <asp:LinkButton ID="UpdateLinkButton" runat="server" OnClientClick="ProductGridView_OnUpdateButtonClick();return false;"
                    TabIndex="18" Text="Update" />
                <%--                <dx:aspxgridviewtemplatereplacement id="UpdateButton" replacementtype="EditFormUpdateButton"
                    runat="server" columnid="" tabindex="18" />
                --%>
            </div>
        </EditForm>
        <DetailRow>
            <dx:ASPxGridView ID="ProductLanguageGridView" runat="server" AutoGenerateColumns="False"
                ClientInstanceName="ProductLanguageGridView" DataSourceID="ProductLanguageDataSource"
                KeyFieldName="Product_LanguageID" OnBeforePerformDataSelect="ProductLanguageGridView_BeforePerformDataSelect"
                EnableRowsCache="false" OnRowInserting="ProductLanguageGridView_RowInserting"
                OnRowUpdating="ProductLanguageGridView_RowUpdating" OnInitNewRow="ProductLanguageGridView_InitNewRow"
                Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn VisibleIndex="0" ShowEditButton="true" ShowNewButton="false" ShowDeleteButton="true">
                        <HeaderCaptionTemplate>
                            <asp:LinkButton ID="NewLinkButton" runat="server" OnClientClick="AddNewRow(ProductLanguageGridView);return false;" Text="New" OnInit="NewLinkButton_Init"/>
                        </HeaderCaptionTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="ProductName" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ProductDescription" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <Templates>
                    <EditForm>
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ProductNameLabel" runat="server" Text="Product Name" />
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxTextBox ID="ProductNameTextBox" runat="server" Text='<%# Bind("ProductName") %>'
                                        ValidationSettings-ValidationGroup='<%#Container.ValidationGroup %>'>
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true">
                                            <RequiredField IsRequired="true" ErrorText="Product name is required" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ProductDescriptionLabel" runat="server" Text="Description" />
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxTextBox ID="ProductDescriptionMemo" runat="server" Text='<%# Bind("ProductDescription") %>'
                                        Width="300px">
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="DefaultLanguageLabel" runat="server" Text="Default Language" />
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxComboBox ID="DefaultLanguageComboBox" runat="server" DataSourceID="LanguageDataSource"
                                        ValueField="LanguageID" ValueType="System.Int32" TextField="Language1" Value='<%#Eval("Product.DefaultLanguageID")%>'>
                                    </dx:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="DefaultThemeLabel" runat="server" Text="Default Theme" />
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:ASPxComboBox ID="DefaultThemeComboBox" runat="server" DropDownStyle="DropDownList"
                                        ValueType="System.String" validationsetting-validationgroup="EditForm" ClientInstanceName="DefaultThemeComboBox"
                                        ValidationSettings-ValidationGroup='<%# Container.ValidationGroup %>' Value='<%# Eval("Product.DefaultProductTheme") %>'>
                                        <ClientSideEvents Validation="DefaultThemeComboBox_Validate" />
                                        <ValidationSettings SetFocusOnError="true" ErrorDisplayMode="ImageWithTooltip" ErrorText="Default theme is a required field">
                                            <RequiredField IsRequired="true" ErrorText="Default theme is a required field" />
                                        </ValidationSettings>
                                        <Items>
                                            <dx:ListEditItem Value="Apple" Text="Apple" />
                                            <dx:ListEditItem Value="Default Theme" Text="Default Theme" />
                                            <dx:ListEditItem Value="Windows" Text="Windows" />
                                        </Items>
                                    </dx:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <dx:ASPxLabel ID="DistributorAccessValidateMessageLabel" runat="server" Text="At least one distributor must be owner with access granted"
                                        ClientInstanceName="DistributorAccessValidateMessageLabel">
                                    </dx:ASPxLabel>
                                    <br />
                                    <br />
                                    <dx:ASPxGridView ID="DistributorAccessGridView" runat="server" AutoGenerateColumns="False"
                                        KeyFieldName="DistributorID" DataSourceID="DistributorAccessDataSource" Caption="Distributor Access"
                                        ClientInstanceName="DistributorAccessGridView" OnDataBound="DistributorAccessGridView_DataBound"
                                        OnCommandButtonInitialize="DistributorAccessGridView_OnCommandButtonInitialize">
                                        <SettingsBehavior AllowSelectSingleRowOnly="true" />
                                        <ClientSideEvents SelectionChanged="DistributorAccessGridView_SelectionChanged" />
                                        <Columns>
                                            <dx:GridViewDataCheckColumn FieldName="IsGranted" Caption="Grant" VisibleIndex="0">
                                                <DataItemTemplate>
                                                    <dx:ASPxCheckBox ID="IsAcessGrantedCheckBox" runat="server" Value='<%# Eval("IsGranted") %>'
                                                        OnInit="IsAcessGrantedCheckBox_Init">
                                                        <ClientSideEvents CheckedChanged="IsAccessGrantedCheckBox_CheckChanged" />
                                                    </dx:ASPxCheckBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataCheckColumn>
                                            <dx:GridViewDataTextColumn FieldName="DistributorName" Caption="Distributor Name"
                                                VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="2" Caption="Owner">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="QuestionMix" Caption="QuestionMix" VisibleIndex="3">
                                                <DataItemTemplate>
                                                    <dx:ASPxComboBox ID="QuestionMixComboBox" runat="server" Value='<%# Eval("QuestionMix") %>'
                                                        DropDownStyle="DropDownList" OnInit="QuestionMixComboBox_Init" ClientEnabled='<%# Eval("IsGranted") %>'>
                                                        <Items>
                                                            <dx:ListEditItem Text="Mixed" Value="MIXED" />
                                                            <dx:ListEditItem Text="Ratified Only" Value="RATIFIEDONLY" />
                                                            <dx:ListEditItem Text="Unratified Only" Value="UNRATIFIEDONLY" />
                                                        </Items>
                                                        <ClientSideEvents Validation="QuestionMixComboxBox_Validate" />
                                                        <ValidationSettings SetFocusOnError="true" ErrorDisplayMode="ImageWithTooltip" ErrorText="Question mix is a required field">
                                                        </ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataTextColumn FieldName="MaxNoOfProductsDistributorCanCreate" Caption="Max Products Can Be Owned"
                                                VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="CurrentProductCount" Caption="Actual Products Owned"
                                                VisibleIndex="5">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="HasAvailableProducts" Caption="Owner Status"
                                                VisibleIndex="6">
                                                <DataItemTemplate>
                                                    <dx:ASPxLabel runat="server" Text="Available" ClientVisible='<%# (bool)Eval("HasAvailableProducts") %>' />
                                                    <dx:ASPxLabel runat="server" Text="Unavailable" ClientVisible='<%# !(bool)Eval("HasAvailableProducts") %>' />
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </table>
                        <div id="aspxGridViewUpdateCancelButtonBar">
                            <div id="aspxGridViewCancelButton">
                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                    runat="server" ColumnID="" tabindex="19" />
                            </div>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="UpdateLinkButton" runat="server" OnClientClick="ProductLanguageGridView_OnUpdateButtonClick();return false;"
                                TabIndex="18" Text="Update" />
                            <dx:ASPxDateEdit ID="LastModifiedOnDateTimeDateEdit" runat="server" ClientVisible="false"
                                Value='<%#Bind("LastModifiedOnDateTime") %>' ReadOnly="true">
                            </dx:ASPxDateEdit>
                        </div>
                    </EditForm>
                </Templates>
            </dx:ASPxGridView>
        </DetailRow>
    </Templates>
</dx:ASPxGridView>
<asp:LinqDataSource ID="ProductDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="ProductDataSource_Selecting">
</asp:LinqDataSource>
<asp:LinqDataSource ID="DistributorAccessDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="DistributorAccessDataSource_Selecting" TableName="Distributors">
</asp:LinqDataSource>
<asp:LinqDataSource ID="ProductLanguageDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" OnSelecting="ProductLanguageDataSource_Selecting" TableName="Product_Languages">
</asp:LinqDataSource>
<asp:LinqDataSource ID="LanguageDataSource" runat="server" ContextTypeName="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    EntityTypeName="" TableName="Languages" OnSelecting="LanguageDataSource_Selecting">
</asp:LinqDataSource>
