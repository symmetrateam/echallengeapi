﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.Data;
using Diversity.Common.Enumerations;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class CardListControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CardDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                var output = (from p in dataContext.Cards                
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()      
                            let productName = p.Product.Product_Languages.FirstOrDefault(pl=>pl.LanguageID == language.LanguageID).ProductName
                            let showEdit = (from product in dataContext.Products
                                           from productLang in product.Product_Languages.Where(l=>l.LanguageID == language.LanguageID)
                                           where p.ProductID == product.ProductID
                                           select productLang).Count() > 0
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)                                                        
                            where !p.IsForGame
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,            
                                ProductName = productName,
                                p.Internal_CardType.Internal_CardTypeName,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                Library = p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                                currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription = 
                                    currentLanguageCard == null ? pl.DetailedCardDescription : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,                                
                                ShowEdit = showEdit
                            })
                    .OrderBy(r => r.DisplayCardName).ToList();

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.ProductName,
                                              o.Internal_CardTypeName,
                                              o.Internal_CardTypeShortCode,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              o.ShowEdit,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType), o.Internal_CardTypeShortCode)))
                                          }
                              );

                           

                e.Result = results;
            }
            else
            {
                 var output = (from p in dataContext.Cards
                            from pd in p.Product.Product2Distributors
                            from currentLanguageCard in dataContext.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == language.LanguageID)
                                .DefaultIfEmpty()
                            let productName = p.Product.Product_Languages.FirstOrDefault(pl => pl.LanguageID == language.LanguageID).ProductName
                            from pl in p.Card_Languages
                                .Where(d => d.CardID == p.CardID && d.LanguageID == p.DefaultLanguageID)
                            let distributors = dataContext.User2Distributors
                                .Where(d => d.UserID == userId)
                                .Select(d => d.DistributorID)                                                        
                            where distributors.Contains(pd.DistributorID) && !p.IsForGame
                            select new
                            {
                                p.CardID,
                                p.CardShortcutCode,
                                p.Internal_CardTypeID,
                                p.Internal_CardType.Internal_CardTypeShortCode,
                                ProductName = productName,
                                p.Internal_CardType.Internal_CardTypeName,
                                Library = p.Client == null ? Resources.Diversity.PublicCardLibraryName : p.Client.ClientName,
                                p.DefaultLanguageID,
                                DisplayCardName =
                                currentLanguageCard == null ? pl.CardName : currentLanguageCard.CardName,
                                DisplayCardDescription =
                                    currentLanguageCard == null ? pl.DetailedCardDescription : currentLanguageCard.DetailedCardDescription,
                                p.IsRatified,
                                ShowEdit = currentLanguageCard == null ? false : true
                            })
                    .OrderBy(r => r.DisplayCardName);

                var results = (from o in output
                               select new
                                          {
                                              o.CardID,
                                              o.CardShortcutCode,
                                              o.Internal_CardTypeID,
                                              o.ProductName,
                                              o.Internal_CardTypeName,
                                              o.Library,
                                              o.DefaultLanguageID,
                                              o.DisplayCardName,
                                              o.DisplayCardDescription,
                                              o.IsRatified,
                                              Url =
                                   string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", o.CardID,
                                                 Convert.ToInt32(
                                                     (CardType)
                                                     Enum.Parse(typeof (CardType), o.Internal_CardTypeShortCode))),
                                              o.ShowEdit
                                          });

                e.Result = results;

            }

        }

        protected void CardGridView_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var cardId = (int)e.Keys[0];

            using (var dataContext = new EChallengeDataContext())
            {
                var card = dataContext.Cards.FirstOrDefault(c => c.CardID == cardId);

                if(card == null)
                {
                    throw new Exception(Resources.Exceptions.DeleteFailedRowDoesNotExist);
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.upDeleteCard(cardId, userId, false);
                        dataContext.SaveChanges();
                        transaction.Commit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}