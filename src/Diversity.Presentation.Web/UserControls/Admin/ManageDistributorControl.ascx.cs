﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Diversity.Data.DataAccess;
using Diversity.Data.DataAccess.SqlServer;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class ManageDistributorControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void DistributorGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var distributorGridView = sender as ASPxGridView;
            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;

            var distributor = new Distributor();
            distributor.CreatedOnDateTime = modifiedOn;
            distributor.DistributorCanCreateProducts = e.NewValues["DistributorCanCreateProducts"] == null ? false : (bool)e.NewValues["DistributorCanCreateProducts"];
            distributor.DistributorEmailAddress = string.IsNullOrEmpty(e.NewValues["DistributorEmailAddress"] as string) ? null : (e.NewValues["DistributorEmailAddress"] as string).Trim();
            distributor.DistributorName = string.IsNullOrEmpty(e.NewValues["DistributorName"] as string) ? null : (e.NewValues["DistributorName"] as string).Trim();
            distributor.DistributorNotes = string.IsNullOrEmpty(e.NewValues["DistributorNotes"] as string) ? null : (e.NewValues["DistributorNotes"] as string).Trim();
            distributor.DistributorTel1 = string.IsNullOrEmpty(e.NewValues["DistributorTel1"] as string) ? null : (e.NewValues["DistributorTel1"] as string).Trim();
            distributor.LastModifiedOnDateTime = modifiedOn;
            distributor.MaxNoOfProductsDistributorCanCreate = distributor.DistributorCanCreateProducts
                                                                  ? (e.NewValues["MaxNoOfProductsDistributorCanCreate"
                                                                         ] == null
                                                                         ? 0
                                                                         : (int)
                                                                           e.NewValues[
                                                                               "MaxNoOfProductsDistributorCanCreate"]
                                                                    )
                                                                  : 0;
            distributor.PhysicalAddress1 = string.IsNullOrEmpty(e.NewValues["PhysicalAddress1"] as string) ? null : (e.NewValues["PhysicalAddress1"] as string).Trim();
            distributor.PhysicalAddress2 = string.IsNullOrEmpty(e.NewValues["PhysicalAddress2"] as string)
                                               ? null
                                               : (e.NewValues["PhysicalAddress2"] as string).Trim();
            distributor.PhysicalCountry = string.IsNullOrEmpty(e.NewValues["PhysicalCountry"] as string) ? null : (e.NewValues["PhysicalCountry"] as string).Trim();
            distributor.PhysicalPostalCode = string.IsNullOrEmpty(e.NewValues["PhysicalPostalCode"] as string) ? null : (e.NewValues["PhysicalPostalCode"] as string).Trim();
            distributor.PhysicalStateProvince = string.IsNullOrEmpty(e.NewValues["PhysicalStateProvince"] as string)
                                                    ? null
                                                    : (e.NewValues["PhysicalStateProvince"] as string).Trim();
            distributor.PostalAddress1 = string.IsNullOrEmpty(e.NewValues["PostalAddress1"] as string) ? null : (e.NewValues["PostalAddress1"] as string).Trim();
            distributor.PostalAddress2 = string.IsNullOrEmpty(e.NewValues["PostalAddress2"] as string)
                                             ? null
                                             : (e.NewValues["PostalAddress2"] as string).Trim();
            distributor.PostalCountry = string.IsNullOrEmpty(e.NewValues["PostalCountry"] as string)
                                            ? null
                                            : (e.NewValues["PostalCountry"] as string).Trim();
            distributor.PostalPostalCode = string.IsNullOrEmpty(e.NewValues["PostalPostalCode"] as string) ? null : (e.NewValues["PostalPostalCode"] as string).Trim();
            distributor.PostalStateProvince = string.IsNullOrEmpty(e.NewValues["PostalStateProvince"] as string)
                                                  ? null
                                                  : (e.NewValues["PostalStateProvince"] as string).Trim();
            distributor.UserID_CreatedBy = userId;
            distributor.UserID_LastModifiedBy = userId;

            using (var dataContext = new EChallengeDataContext())
            {
                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.Distributors.Add(distributor);
                        dataContext.SaveChanges();
                        transaction.Commit();
                        distributorGridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }

        protected void DistributorDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = from d in dataContext.Distributors
                           select d;
            }
            else
            {
                e.Result = from d in dataContext.Distributors
                           from u in d.User2Distributors
                           where u.UserID == userId
                           select d;
            }
        }

        protected void DistributorGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var distributorGridView = sender as ASPxGridView;
            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;
            var distributorId = (int)e.Keys[0];

            var lastModifiedOn = (DateTime)e.OldValues["LastModifiedOnDateTime"];

            using (var dataContext = new EChallengeDataContext())
            {
                var distributor = dataContext.Distributors.FirstOrDefault(d => d.DistributorID == distributorId);

                if (distributor == null)
                {
                    throw new Exception(Resources.Exceptions.UpdateFailedRowDoesNotExist);
                }

                if(lastModifiedOn != distributor.LastModifiedOnDateTime)
                {
                    throw new Exception(Resources.Exceptions.ConcurrencyException);
                }

                if (e.NewValues["DistributorCanCreateProducts"] != null)
                {
                    distributor.DistributorCanCreateProducts = e.NewValues["DistributorCanCreateProducts"] == null
                                                                   ? false
                                                                   : (bool)e.NewValues["DistributorCanCreateProducts"];

                    
                    if(!distributor.DistributorCanCreateProducts && 
                        dataContext.Product2Distributors.Any(d => d.DistributorID == distributor.DistributorID))
                    {
                        throw new Exception(Resources.Exceptions.DistributorUsedInProducts);
                    }
                }

                if (e.NewValues["DistributorEmailAddress"] != null)
                {
                    distributor.DistributorEmailAddress = (e.NewValues["DistributorEmailAddress"] as string).Trim();
                }

                if (e.NewValues["DistributorName"] != null)
                {
                    distributor.DistributorName = (e.NewValues["DistributorName"] as string).Trim();
                }

                if (e.NewValues["DistributorNotes"] != null)
                {
                    distributor.DistributorNotes = (e.NewValues["DistributorNotes"] as string).Trim();
                }

                if (e.NewValues["DistributorTel1"] != null)
                {
                    distributor.DistributorTel1 = (e.NewValues["DistributorTel1"] as string).Trim();
                }

                if (e.NewValues["MaxNoOfProductsDistributorCanCreate"] != null)
                {
                    if(distributor.DistributorCanCreateProducts)
                    {
                        distributor.MaxNoOfProductsDistributorCanCreate =
                        (int)e.NewValues["MaxNoOfProductsDistributorCanCreate"];    
                    }
                    else
                    {
                        distributor.MaxNoOfProductsDistributorCanCreate = 0;
                    }

                    var ownerCount =
                        dataContext.Product2Distributors.Count(
                            d => d.DistributorID == distributor.DistributorID && d.DistributorIsProductOwner);

                    if(ownerCount > distributor.MaxNoOfProductsDistributorCanCreate)
                    {
                        throw new Exception(string.Format(Resources.Exceptions.DistributorIsOwnerOfProducts, ownerCount));
                    }
                }

                if (e.NewValues["PhysicalAddress1"] != null)
                {
                    distributor.PhysicalAddress1 = (e.NewValues["PhysicalAddress1"] as string).Trim();
                }

                if (e.NewValues["PhysicalAddress2"] != null)
                {
                    distributor.PhysicalAddress2 = (e.NewValues["PhysicalAddress2"] as string).Trim();
                }

                if (e.NewValues["PhysicalCountry"] != null)
                {
                    distributor.PhysicalCountry = (e.NewValues["PhysicalCountry"] as string).Trim();
                }

                if (e.NewValues["PhysicalPostalCode"] != null)
                {
                    distributor.PhysicalPostalCode = (e.NewValues["PhysicalPostalCode"] as string).Trim();
                }

                if (e.NewValues["PhysicalStateProvince"] != null)
                {
                    distributor.PhysicalStateProvince = (e.NewValues["PhysicalStateProvince"] as string).Trim();
                }

                if (e.NewValues["PostalAddress1"] != null)
                {
                    distributor.PostalAddress1 = (e.NewValues["PostalAddress1"] as string).Trim();
                }

                if (e.NewValues["PostalAddress2"] != null)
                {
                    distributor.PostalAddress2 = (e.NewValues["PostalAddress2"] as string).Trim();
                }

                if (e.NewValues["PostalCountry"] != null)
                {
                    distributor.PostalCountry = (e.NewValues["PostalCountry"] as string).Trim();
                }

                if (e.NewValues["PostalPostalCode"] != null)
                {
                    distributor.PostalPostalCode = (e.NewValues["PostalPostalCode"] as string).Trim();
                }

                if (e.NewValues["PostalStateProvince"] != null)
                {
                    distributor.PostalStateProvince = (e.NewValues["PostalStateProvince"] as string).Trim();
                }


                distributor.LastModifiedOnDateTime = modifiedOn;
                distributor.UserID_LastModifiedBy = userId;

                using (var transaction = dataContext.Database.BeginTransaction())
                {

                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();
                        distributorGridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        protected void DistributorGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var distributorGridView = sender as ASPxGridView;
            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;
            var distributorId = (int)e.Keys[0];

            using (var dataContext = new EChallengeDataContext(userId))
            {
                var distributor = dataContext.Distributors.FirstOrDefault(d => d.DistributorID == distributorId);

                if (distributor == null)
                {
                    throw new Exception(Resources.Exceptions.DeleteFailedRowDoesNotExist);
                }

                //if(dataContext.Client2Distributors.Any(c => c.DistributorID == distributorId) ||
                //    dataContext.Product2Distributors.Any(p => p.DistributorID == distributorId)  ||
                //    dataContext.User2Distributors.Any(u => u.DistributorID == distributorId))
                //{
                //    throw new Exception(Resources.Exceptions.DeleteFailedDistributorReferenced);
                //}

                //distributor.UserID_LastModifiedBy = userId;
                //distributor.LastModifiedOnDateTime = modifiedOn;

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.upDeleteDistributor(distributor.DistributorID, userId);
                        dataContext.SaveChanges();
                        transaction.Commit();

                        distributorGridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }

        protected void DistributorGridView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["MaxNoOfProductsDistributorCanCreate"] = 0;
        }

        protected void DistributorGridView_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            using (var dataContext = new EChallengeDataContext())
            {
                var distributorName = (e.NewValues["DistributorName"] as string).Trim().ToLower();

                var distributor =
                    dataContext.Distributors.Where(d => d.DistributorName.ToLower() == distributorName).FirstOrDefault();

                if (e.IsNewRow)
                {
                    if (distributor != null)
                    {
                        e.RowError = Resources.Exceptions.DistributorNameAlreadyExists;
                    }
                }
                else
                {
                    if (distributor != null)
                    {
                        var editingDistributorId = (int)e.Keys[0];
                        if (distributor.DistributorID != editingDistributorId)
                        {
                            e.RowError = Resources.Exceptions.DistributorNameAlreadyExists;
                        }
                    }
                }
            }
        }

        protected void NoProductSpinEdit_Init(object sender, EventArgs e)
        {
            var noProductSpinEdit = sender as ASPxSpinEdit;

            var container = noProductSpinEdit.NamingContainer as GridViewEditFormTemplateContainer;

            var distributorCanCreateProducts = (bool?)DataBinder.Eval(container.DataItem, "DistributorCanCreateProducts");
            var noOfProducts = (int?) DataBinder.Eval(container.DataItem, "MaxNoOfProductsDistributorCanCreate");

            noProductSpinEdit.ClientEnabled = distributorCanCreateProducts.HasValue
                                                  ? distributorCanCreateProducts.Value
                                                  : false;

            if(noProductSpinEdit.ClientEnabled)
            {
                noProductSpinEdit.Value = noOfProducts.HasValue ? noOfProducts.Value : 1;
            }
            else
            {
                noProductSpinEdit.Value = 0;
            }    
        }
    }
}