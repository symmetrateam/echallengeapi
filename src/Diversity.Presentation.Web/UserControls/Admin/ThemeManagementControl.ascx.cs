﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.Data;
using Diversity.DataModel.SqlRepository;
using User = ComLib.Account.User;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class ThemeManagementControl : System.Web.UI.UserControl
    {
        private const string SELECTED_THEME_ID_KEY = "B6672AA9-2BCF-40E2-9788-80C7CDE58BD6";

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        int? SelectedThemeId
        {
            get
            {
                if (Session[SELECTED_THEME_ID_KEY] == null)
                {
                    return -1;
                }
                
                return (int)Session[SELECTED_THEME_ID_KEY];
            }
            set { Session[SELECTED_THEME_ID_KEY] = value; }

        }

        protected void OnThemeDataSourceSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            e.Result = dataContext.Themes
                .Where(c => !c.IsADeletedRow)
                .OrderBy(c => c.ThemeName);
        }

        protected void OnThemeClientUserGroupDataSourceSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var result = dataContext.Themes
                .Where(c => c.ThemeID == SelectedThemeId)
                .SelectMany(c => c.ClientUserGroups);

            e.Result = result
                .Select(c =>
                    new
                    {
                        c.ClientUserGroupID,
                        c.ClientUserGroup.ClientID,
                        c.ClientUserGroup.ClientUserGroupName,
                        c.ClientUserGroup.Client.ClientName,
                        c.ExternalID
                    })
                .OrderBy(c => c.ClientName)
                .ThenBy(c => c.ClientUserGroupName);
        }

        protected void OnClientDataSourceSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var clientUserGroups = dataContext
                .ClientUserGroups
                .Where(c=> !c.IsADeletedRow)
                .Select(c => c.ClientUserGroupID);

            var themeClientUserGroups = dataContext.Themes
                .Where(c => c.ThemeID == SelectedThemeId && !c.IsADeletedRow)
                .SelectMany(c => c.ClientUserGroups)
                .Select(c => c.ClientUserGroupID);

            var remaining = clientUserGroups.Except(themeClientUserGroups);

            e.Result = dataContext.ClientUserGroups
                .Where(c=> remaining.Contains(c.ClientUserGroupID) && !c.IsADeletedRow && !c.Client.IsADeletedRow)                
                .Select(c => new
                             {
                                 c.ClientID,
                                 c.Client.ClientName
                             })
                .OrderBy(c => c.ClientName);
        }

        protected void ThemeGridView_OnRowValidating(object sender, ASPxDataValidationEventArgs e)
        {
            using (var dataContext = new EChallengeDataContext())
            {
                var errorMessageBuilder = new StringBuilder();
                var themeCode = (e.NewValues["ThemeCode"] as string).Trim().ToLower();
                var themeName = (e.NewValues["ThemeName"] as string).Trim().ToLower();

                var themeByCode =
                    dataContext.Themes.FirstOrDefault(c => c.ThemeCode.ToLower() == themeCode);

                var themeByName =
                    dataContext.Themes.FirstOrDefault(c => c.ThemeName.ToLower() == themeName);

                if (e.IsNewRow)
                {
                    if (themeByCode != null)
                    {
                        errorMessageBuilder.AppendLine(Resources.Exceptions.ThemeWithCodeExists);
                    }

                    if (themeByName != null)
                    {
                        errorMessageBuilder.AppendLine(Resources.Exceptions.ThemeWithNameExists);
                    }
                }
                else
                {
                    var editingThemeId = (int)e.Keys[0];
                    
                    if (themeByCode != null)
                    {                        
                        if (themeByCode.ThemeID != editingThemeId)
                        {
                            errorMessageBuilder.AppendLine(Resources.Exceptions.ThemeWithCodeExists);
                        }
                    }

                    if (themeByName != null)
                    {
                        if (themeByName.ThemeID != editingThemeId)
                        {
                            errorMessageBuilder.AppendLine(Resources.Exceptions.ThemeWithNameExists);
                        }
                    }
                }

                e.RowError = errorMessageBuilder.ToString();
            }
        }

        protected void ThemeGridView_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            if (gridView == null)
            {
                return;
            }

            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;

            var theme = new Theme();
            theme.CreatedOnDateTime = modifiedOn;
            theme.ThemeCode = string.IsNullOrEmpty(e.NewValues["ThemeCode"] as string) ? null : (e.NewValues["ThemeCode"] as string).Trim();
            theme.ThemeName = string.IsNullOrEmpty(e.NewValues["ThemeName"] as string) ? null : (e.NewValues["ThemeName"] as string).Trim();
            theme.Description = string.IsNullOrEmpty(e.NewValues["Description"] as string) ? null : (e.NewValues["Description"] as string).Trim();            
            theme.LastModifiedOnDateTime = modifiedOn;            
            theme.UserID_CreatedBy = userId;
            theme.UserID_LastModifiedBy = userId;

            using (var dataContext = new EChallengeDataContext())
            {
                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.Themes.Add(theme);
                        dataContext.SaveChanges();
                        transaction.Commit();
                        gridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        protected void ThemeGridView_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            if (gridView == null)
            {
                return;
            }

            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;
            var themeId = (int)e.Keys[0];

            var lastModifiedOn = (DateTime)e.OldValues["LastModifiedOnDateTime"];

            using (var dataContext = new EChallengeDataContext())
            {
                var theme = dataContext.Themes.FirstOrDefault(d => d.ThemeID == themeId);

                if (theme == null)
                {
                    throw new Exception(Resources.Exceptions.UpdateFailedRowDoesNotExist);
                }

                if (lastModifiedOn != theme.LastModifiedOnDateTime)
                {
                    throw new Exception(Resources.Exceptions.ConcurrencyException);
                }


                if (e.NewValues["ThemeCode"] != null)
                {
                    theme.ThemeCode = (e.NewValues["ThemeCode"] as string).Trim();
                }

                if (e.NewValues["ThemeName"] != null)
                {
                    theme.ThemeName = (e.NewValues["ThemeName"] as string).Trim();
                }

                if (e.NewValues["Description"] != null)
                {
                    theme.Description = (e.NewValues["Description"] as string).Trim();
                }                

                theme.LastModifiedOnDateTime = modifiedOn;
                theme.UserID_LastModifiedBy = userId;

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();
                        gridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        protected void ThemeGridView_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;            
            var themeId = (int) e.Keys[0];

            using (var dataContext = new EChallengeDataContext(userId))
            {
                var theme = dataContext.Themes.FirstOrDefault(d => d.ThemeID== themeId);

                if (theme == null)
                {
                    throw new Exception(Resources.Exceptions.DeleteFailedRowDoesNotExist);
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.upDeleteTheme(theme.ThemeID, userId);
                        dataContext.SaveChanges();
                        transaction.Commit();

                        gridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }                
            }
        }

        protected void OnClientUserGroupDetailGridViewBeforePerformDataSelect(object sender, EventArgs e)
        {
            SelectedThemeId = (int)(sender as ASPxGridView).GetMasterRowKeyValue();                       
        }

        protected void ClientUserGroupDetailGridView_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var clientUserGroupId = (int)e.Keys[0];

            using (var dataContext = new EChallengeDataContext(userId))
            {
                var theme = dataContext.Themes
                    .Where(c => c.ThemeID == SelectedThemeId)
                    .Include(c => c.ClientUserGroups)
                    .FirstOrDefault();

                if (theme == null)
                {
                    return;
                }

                var clientUserGroup = theme.ClientUserGroups.FirstOrDefault(c => c.ClientUserGroupID == clientUserGroupId);

                if (clientUserGroup == null)
                {
                    throw new Exception(Resources.Exceptions.DeleteFailedRowDoesNotExist);
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        theme.ClientUserGroups.Remove(clientUserGroup);
                        theme.LastModifiedOnDateTime = DateTime.Now;
                        theme.UserID_LastModifiedBy = userId;
                        dataContext.SaveChanges();
                        transaction.Commit();

                        gridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        protected void ClientUserGroupDetailGridView_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            if (gridView == null)
            {
                return;
            }

            var userId = (this.Page as BasePage).CurrentContext.UserIdentifier;
            var modifiedOn = DateTime.Now;
                        
            var clientUserGroupId = e.NewValues["ClientUserGroupID"] == null ? null : (int?)e.NewValues["ClientUserGroupID"];            

            using (var dataContext = new EChallengeDataContext())
            {
                var theme = dataContext.Themes
                    .Where(c=> c.ThemeID == SelectedThemeId)
                    .Include(c=>c.ClientUserGroups)
                    .Select(c=> c)
                    .FirstOrDefault();                                

                var clientUserGroup =
                    dataContext.ThemeClientUserGroup.FirstOrDefault(c => c.ClientUserGroupID == clientUserGroupId && c.ThemeID == SelectedThemeId);

                if (!theme.ClientUserGroups.Contains(clientUserGroup))
                {

                    theme.ClientUserGroups.Add(new ThemeClientUserGroup()
                                               {
                                                   ClientUserGroupID =  clientUserGroupId.Value,
                                                   ThemeID = theme.ThemeID,
                                                   ExternalID = Guid.NewGuid().ToString().Replace("-", string.Empty)
                                               });
                    theme.LastModifiedOnDateTime = modifiedOn;
                    theme.UserID_LastModifiedBy = userId;
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {                        
                        dataContext.SaveChanges();
                        transaction.Commit();
                        gridView.CancelEdit();
                        e.Cancel = true;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        protected void ClientUserGroupComboBox_Callback(object sender, CallbackEventArgsBase e)
        {
            int clientId;
            if (!Int32.TryParse(e.Parameter, out clientId))
            {
                return;
            }

            PopulateClientUserGroupComboBox((ASPxComboBox)sender, clientId);
        }

        void PopulateClientUserGroupComboBox(ASPxComboBox comboBox, int clientId)
        {
            using (var dataContext = new EChallengeDataContext())
            {
                
                var clientUserGroupsAlreadyInTheme = dataContext.Themes
                    .Where(c => c.ThemeID == SelectedThemeId)
                    .SelectMany(c => c.ClientUserGroups)
                    .Where(c => c.ClientUserGroup.ClientID == clientId)
                    .Select(c => c.ClientUserGroupID);

                var clientUserGroups = dataContext.ClientUserGroups
                    .Where(c => c.ClientID == clientId && !c.IsADeletedRow && !clientUserGroupsAlreadyInTheme.Contains(c.ClientUserGroupID))
                    .Select(c =>
                        new
                        {
                            c.ClientUserGroupID,
                            c.ClientUserGroupName
                        }).ToList();

                comboBox.Items.Clear();
                comboBox.DataSource = clientUserGroups;
                comboBox.DataBind();
            }
        }

        protected void ClientComboBox_Init(object sender, EventArgs e)
        {
            var comboBox = sender as ASPxComboBox;

            if (comboBox == null)
            {
                return;
            }

            comboBox.DataSource = GetClients();
        }

        List<Client> GetClients()
        {
            var dataContext = new EChallengeDataContext();
            var clientUserGroups = dataContext
               .ClientUserGroups
               .Where(c => !c.IsADeletedRow)
               .Select(c => c.ClientUserGroupID);

            var themeClientUserGroups = dataContext.Themes
                .Where(c => c.ThemeID == SelectedThemeId && !c.IsADeletedRow)
                .SelectMany(c => c.ClientUserGroups)
                .Select(c => c.ClientUserGroupID);

            var remaining = clientUserGroups.Except(themeClientUserGroups);            

            return dataContext.ClientUserGroups
                .Where(c => remaining.Contains(c.ClientUserGroupID) && !c.IsADeletedRow && !c.Client.IsADeletedRow)
                .Select(c => c.Client)                
                .OrderBy(c => c.ClientName)
                .Distinct()
                .ToList();
        }

        protected void NewLinkButton_Init(object sender, EventArgs e)
        {
            var button = sender as ASPxHyperLink;
            if (button == null)
            {
                return;
            }
            
            var container = button.NamingContainer as GridViewHeaderTemplateContainer;
            var gridView = container.NamingContainer.NamingContainer as ASPxGridView;
            button.ClientSideEvents.Click = string.Format("function(s, e) {{ ClientUserGroupDetailGridViewAddNewRow(s, e, {0})}} ", gridView.ClientInstanceName);

            button.ClientVisible = GetClients().Any();

        }        
    }
}