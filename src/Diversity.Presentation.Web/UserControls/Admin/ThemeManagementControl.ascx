﻿<%@ control language="C#" autoeventwireup="true" codebehind="ThemeManagementControl.ascx.cs" inherits="Diversity.Presentation.Web.UserControls.Admin.ThemeManagementControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<script id="dxis_ThemeManagementControl" type="text/javascript" language="javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/ThemeManagementControl.js") %>'>
</script>
<asp:linqdatasource id="ThemeDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" tablename="Themes" onselecting="OnThemeDataSourceSelecting">
</asp:linqdatasource>
<asp:linqdatasource id="ThemeClientUserGroupDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" tablename="ClientUserGroup" onselecting="OnThemeClientUserGroupDataSourceSelecting">
</asp:linqdatasource>
<asp:linqdatasource id="ClientDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" tablename="Client" onselecting="OnClientDataSourceSelecting" />

<dx:aspxgridview id="ThemeGridView" runat="server" clientinstancename="ThemeGridView"
    autogeneratecolumns="False" datasourceid="ThemeDataSource" keyfieldname="ThemeID" width="100%"
    onrowvalidating="ThemeGridView_OnRowValidating" onrowdeleting="ThemeGridView_RowDeleting" onrowinserting="ThemeGridView_RowInserting"
    onrowupdating="ThemeGridView_RowUpdating">
    <settings showfilterrow="true" showgrouppanel="true" />
    <settingsdetail showdetailrow="True" />
    <settingsediting mode="EditFormAndDisplayRow" />
    <styles>
        <alternatingrow enabled="True" />
    </styles>
    <clientsideevents custombuttonclick="ThemeGridView_CustomButtonClick"></clientsideevents>
    <settingspager alwaysshowpager="True">
        <allbutton visible="True" />
    </settingspager>
    <columns>
        <dx:gridviewcommandcolumn visibleindex="0" width="100px" ShowEditButton="true" ShowNewButton="false">
            <custombuttons>
                <dx:gridviewcommandcolumncustombutton id="DeleteThemeButton" text="Delete"/>
            </custombuttons>
            <headercaptiontemplate>
                <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="AddNewRow(ThemeGridView);return false;" text="New"/>
            </headercaptiontemplate>
        </dx:gridviewcommandcolumn>
        <dx:gridviewdatatextcolumn fieldname="ThemeID" readonly="True" visible="false"
            visibleindex="1">
            <editformsettings visible="False" />
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="ThemeCode" caption="Theme Code"
            visibleindex="2" width="100px">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="ThemeName" caption="Theme Name"
            visibleindex="3" width="300px">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="Description" caption="Description"
            visibleindex="4" width="300px">
        </dx:gridviewdatatextcolumn>
    </columns>
    <templates>
        <detailrow>
            <dx:aspxgridview id="ClientUserGroupDetailGridView" runat="server" keyfieldname="ClientUserGroupID"
                datasourceid="ThemeClientUserGroupDataSource" clientinstancename="ClientUserGroupDetailGridView"
                onbeforeperformdataselect="OnClientUserGroupDetailGridViewBeforePerformDataSelect" onrowdeleting="ClientUserGroupDetailGridView_RowDeleting" onrowinserting="ClientUserGroupDetailGridView_RowInserting">
                <settings showfilterrow="true" showgrouppanel="true" />
                <settingspager alwaysshowpager="True">
                    <allbutton visible="True" />
                </settingspager>
                <settingsediting mode="EditFormAndDisplayRow" />
                <styles>
                    <alternatingrow enabled="True" />
                </styles>
                <clientsideevents custombuttonclick="ClientUserGroupDetailGridView_CustomButtonClick"></clientsideevents>
                <columns>
                    <dx:gridviewcommandcolumn visibleindex="0" width="100px" ShowNewButton="false">
                        <custombuttons>
                            <dx:gridviewcommandcolumncustombutton id="DeleteClientUserGroupButton" text="Delete"/>
                        </custombuttons>
                        <headercaptiontemplate>
                            <dx:aspxhyperlink id="NewLinkButton" runat="server" text="New" oninit="NewLinkButton_Init"/>
                        </headercaptiontemplate>
                    </dx:gridviewcommandcolumn>
                    <dx:gridviewdatacolumn fieldname="ClientUserGroupID" caption="Client User Group Id" visibleindex="1" visible="False" />
                    <dx:gridviewdatacolumn fieldname="ClientID" caption="Client Id" visibleindex="2" visible="False" />
                    <dx:gridviewdatatextcolumn fieldname="ClientName" caption="Client" visibleindex="3" />
                    <dx:gridviewdatatextcolumn fieldname="ClientUserGroupName" caption="Client User Group Name" visibleindex="4" />
                    <dx:gridviewdatatextcolumn fieldname="ExternalID" caption="Scorm Theme Code" visibleindex="5" />
                </columns>
                <templates>
                    <editform>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="ClientNameLabel" runat="server" text="Client" />
                                </td>
                                <td>
                                    <dx:aspxcombobox id="ClientComboBox" runat="server" valuetype="System.Int32" textfield="ClientName" valuefield="ClientID"
                                        value='<%# Bind("ClientID") %>'
                                        tabindex="1" validationsettings-validationgroup='<%#  Container.ValidationGroup  %>' oninit="ClientComboBox_Init">
                                        <validationsettings errordisplaymode="ImageWithTooltip" setfocusonerror="True" causesvalidation="True">
                                            <requiredfield isrequired="True" errortext="Client is required" />
                                        </validationsettings>
                                        <clientsideevents selectedindexchanged="function(s,e) { OnClientSelectionChanged(s); }"></clientsideevents>
                                    </dx:aspxcombobox>
                                </td>
                                <td class="spacer"></td>
                                <td>
                                    <dx:aspxlabel id="ClientUserGroupLabel" runat="server" text="ClientUserGroup" />
                                </td>
                                <td>
                                    <dx:aspxcombobox id="ClientUserGroupComboBox" runat="server" clientinstancename="ClientUserGroupComboBox"
                                        value='<%# Bind("ClientUserGroupID") %>' textfield="ClientUserGroupName" valuefield="ClientUserGroupID" valuetype="System.Int32"
                                        tabindex="2" validationsettings-validationgroup='<%#  Container.ValidationGroup  %>'
                                        oncallback="ClientUserGroupComboBox_Callback">
                                        <validationsettings errordisplaymode="ImageWithTooltip" setfocusonerror="True" causesvalidation="True">
                                            <requiredfield isrequired="True" errortext="Client User Group is required" />
                                        </validationsettings>
                                        <clientsideevents endcallback="OnClientUserGroupComboxEndCallback"></clientsideevents>
                                    </dx:aspxcombobox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <div id="aspxGridViewUpdateCancelButtonBar">
                                        <div id="aspxGridViewCancelButton">
                                            <dx:aspxgridviewtemplatereplacement id="CancelButton" replacementtype="EditFormCancelButton"
                                                runat="server" columnid="" tabindex="4" />
                                        </div>
                                        &nbsp;&nbsp;
                                            <dx:aspxgridviewtemplatereplacement id="UpdateButton" replacementtype="EditFormUpdateButton"
                                                runat="server" columnid="" tabindex="3" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </editform>
                </templates>
            </dx:aspxgridview>
        </detailrow>
        <editform>
            <table>
                <tr>
                    <td>
                        <dx:aspxlabel id="ThemeCodeLabel" runat="server" text="Theme Code" />
                    </td>
                    <td>
                        <dx:aspxtextbox id="ThemeCodeTextBox" runat="server" text='<%# Bind("ThemeCode") %>'
                            tabindex="0" validationsettings-validationgroup='<%#  Container.ValidationGroup  %>'>
                            <validationsettings errordisplaymode="ImageWithTooltip" setfocusonerror="True" causesvalidation="True">
                                <requiredfield isrequired="True" errortext="Theme Code is required" />
                            </validationsettings>
                        </dx:aspxtextbox>
                    </td>
                    <td class="spacer"></td>
                    <td>
                        <dx:aspxlabel id="ThemeNameLabel" runat="server" text="Theme Name" />
                    </td>
                    <td>
                        <dx:aspxtextbox id="ThemeNameTextBox" runat="server" text='<%# Bind("ThemeName") %>'
                            tabindex="1" validationsettings-validationgroup='<%#  Container.ValidationGroup  %>'>
                            <validationsettings errordisplaymode="ImageWithTooltip" setfocusonerror="True" causesvalidation="True">
                                <requiredfield isrequired="True" errortext="Theme Name is required" />
                            </validationsettings>
                        </dx:aspxtextbox>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <dx:aspxlabel id="ThemeDescriptionLabel" runat="server" text="Theme Description" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <dx:aspxmemo id="ThemeDescriptionMemo" runat="server" tabindex="2"
                            nulltext="Describe the theme" width="100%" rows="5"
                            text='<%# Bind("Description") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <div id="aspxGridViewUpdateCancelButtonBar">
                            <div id="aspxGridViewCancelButton">
                                <dx:aspxgridviewtemplatereplacement id="CancelButton" replacementtype="EditFormCancelButton"
                                    runat="server" columnid="" tabindex="5" />
                            </div>
                            &nbsp;&nbsp;
                            <dx:aspxgridviewtemplatereplacement id="UpdateButton" replacementtype="EditFormUpdateButton"
                                runat="server" columnid="" tabindex="6" />
                        </div>

                        <dx:aspxdateedit id="LastModifiedOnDateTimeDateEdit" runat="server" clientvisible="false"
                            value='<%#Bind("LastModifiedOnDateTime") %>' readonly="true">
                        </dx:aspxdateedit>
                    </td>
                </tr>
            </table>
        </editform>
    </templates>
</dx:aspxgridview>
