﻿function DeleteGameButton_Click(s, e) {
    DeleteGameCallback.PerformCallback(gameId);
    DeleteNotificationPopupControl.Hide();
    LoadingPanel.Show();
}

function CancelDeleteGameButton_Click(s, e) {
    DeleteNotificationPopupControl.Hide();
}

var gameId;
function CustomButtonClick(s, e) {
    if (e.buttonID == 'DeleteButton') {
        var rowVisibleIndex = e.visibleIndex;
        gameId = GameGridView.GetRowKey(rowVisibleIndex);
        DeleteNotificationPopupControl.Show();
    }
    else if (e.buttonID == 'EditButton') {
        var rowVisibleIndex = e.visibleIndex;
        gameId = GameGridView.GetRowKey(rowVisibleIndex);
        EditGameCallback.PerformCallback(gameId);
    }
    else if (e.buttonID == 'StatusButton') {
        var rowVisibleIndex = e.visibleIndex;
        gameId = GameGridView.GetRowKey(rowVisibleIndex);
        GameStatusCallback.PerformCallback(gameId);
    }
    else {
        gameId = -1;
    }
}

function DeleteGameCallback_CallbackComplete(s, e) {
    LoadingPanel.Hide();
    GameGridView.Refresh();
}