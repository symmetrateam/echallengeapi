﻿<%@ control language="C#" autoeventwireup="true" codebehind="GameInviteControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.Admin.GameInviteControl" %>
<%@ register assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>





<script id="dxis_GameInviteControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/Admin/GameInviteControl.js") %>'>
</script>
<dx:aspxpopupcontrol id="EmailsPopupControl" clientinstancename="EmailsPopupControl"
    runat="server" modal="True" showonpageload="False" showclosebutton="True" popuphorizontalalign="WindowCenter"
    popupverticalalign="WindowCenter" width="700px" headertext="Email Details">
    <contentcollection>
        <dx:popupcontrolcontentcontrol>
            <table width="100%">
                <tr>
                    <td>
                        <dx:aspxlabel id="ASPxLabel1" runat="server" text="Subject:" />
                    </td>
                    <td width="75%">
                        <dx:aspxtextbox id="SubjectTextBox" runat="server" width="100%">
                            <validationsettings validationgroup="SaveGroup" errordisplaymode="ImageWithTooltip">
                                <requiredfield isrequired="True" errortext="A subject must be specified"></requiredfield>
                            </validationsettings>
                        </dx:aspxtextbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:aspxlabel id="ASPxLabel2" runat="server" text="Email Body:" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <dx:aspxhtmleditor id="EmailBodyHtmlEditor" clientinstancename="EmailBodyHtmlEditor" runat="server" font-names="Tahoma">
                            <settings allowhtmlview="False" allowpreview="False"></settings>
                            <toolbars>
                                <dx:htmleditortoolbar runat="server">
                                    <items>
                                        <dx:toolbarfontsizeedit runat="server">
                                            <items>
                                                <dx:toolbarlistedititem value="1" text="1 (8pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="2" text="2 (10pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="3" text="3 (12pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="4" text="4 (14pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="5" text="5 (18pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="6" text="6 (24pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="7" text="7 (36pt)"></dx:toolbarlistedititem>
                                            </items>
                                        </dx:toolbarfontsizeedit>
                                        <dx:toolbarboldbutton runat="server" />
                                        <dx:toolbaritalicbutton runat="server" />
                                        <dx:toolbarunderlinebutton runat="server" />
                                        <dx:toolbarjustifyleftbutton runat="server" />
                                        <dx:toolbarjustifycenterbutton runat="server" />
                                        <dx:toolbarjustifyrightbutton runat="server" />
                                        <dx:toolbarindentbutton runat="server" />
                                        <dx:toolbaroutdentbutton runat="server" />
                                        <dx:toolbarinsertorderedlistbutton runat="server" />
                                        <dx:toolbarinsertunorderedlistbutton runat="server" />
                                    </items>
                                </dx:htmleditortoolbar>
                            </toolbars>
                            <settingsvalidation validationgroup="SaveGroup">
                                <requiredfield isrequired="True" errortext="Body text must be supplied"></requiredfield>
                            </settingsvalidation>
                        </dx:aspxhtmleditor>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:aspxlabel id="ASPxLabel3" runat="server" text="Email Footer:" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <dx:aspxhtmleditor id="EmailFooterHtmlEditor" clientinstancename="EmailFooterHtmlEditor" font-names="Tahoma" runat="server" height="150px">
                            <settings allowhtmlview="False" allowpreview="False"></settings>
                            <toolbars>
                                <dx:htmleditortoolbar runat="server">
                                    <items>
                                        <dx:toolbarfontsizeedit runat="server">
                                            <items>
                                                <dx:toolbarlistedititem value="1" text="1 (8pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="2" text="2 (10pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="3" text="3 (12pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="4" text="4 (14pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="5" text="5 (18pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="6" text="6 (24pt)"></dx:toolbarlistedititem>
                                                <dx:toolbarlistedititem value="7" text="7 (36pt)"></dx:toolbarlistedititem>
                                            </items>
                                        </dx:toolbarfontsizeedit>
                                        <dx:toolbarboldbutton runat="server" />
                                        <dx:toolbaritalicbutton runat="server" />
                                        <dx:toolbarunderlinebutton runat="server" />
                                        <dx:toolbarjustifyleftbutton runat="server" />
                                        <dx:toolbarjustifycenterbutton runat="server" />
                                        <dx:toolbarjustifyrightbutton runat="server" />
                                        <dx:toolbarindentbutton runat="server" />
                                        <dx:toolbaroutdentbutton runat="server" />
                                        <dx:toolbarinsertorderedlistbutton runat="server" />
                                        <dx:toolbarinsertunorderedlistbutton runat="server" />
                                    </items>
                                </dx:htmleditortoolbar>
                            </toolbars>
                        </dx:aspxhtmleditor>
                    </td>
                </tr>
            </table>
            <div align="right">
                <dx:aspxbutton id="ASPxButton1" runat="server" autopostback="False" text="Send Email">
                    <clientsideevents click="SendInviteButton_Click"></clientsideevents>
                </dx:aspxbutton>
            </div>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>


<dx:aspxpopupcontrol id="ClientSelectionPopupControl" runat="server" clientinstancename="ClientSelectionPopupControl"
    closeaction="None" showclosebutton="false" modal="true" popuphorizontalalign="WindowCenter"
    popupverticalalign="WindowCenter" headertext="Select a client" allowdragging="true"
    enableanimation="false" enableviewstate="false" width="250px" showonpageload="true">
    <contentcollection>
        <dx:popupcontrolcontentcontrol>
            <dx:aspxpanel id="ClientSelectionPanel" runat="server">
                <panelcollection>
                    <dx:panelcontent>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxcombobox id="ClientComboBox" runat="server" clientinstancename="ClientComboBox"
                                        valuetype="System.Int32" valuefield="ClientID" textfield="ClientName" dropdownstyle="DropDownList"
                                        datasourceid="ClientDataSource" validationsettings-validationgroup="ProductSelectionGroup">
                                        <validationsettings errordisplaymode="ImageWithTooltip">
                                            <requiredfield errortext="A client is required" isrequired="true" />
                                        </validationsettings>
                                        <clientsideevents selectedindexchanged="ClientComboBox_SelectedIndexChanged" />
                                    </dx:aspxcombobox>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <table style="border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <dx:aspxbutton id="ConfirmClientButton" runat="server" clientinstancename="ConfirmClientButton"
                                                    autopostback="false" text="OK" validationgroup="ClientSelectionGroup">
                                                    <clientsideevents click="ConfirmClientButton_Click" />
                                                </dx:aspxbutton>
                                            </td>
                                            <td>
                                                <dx:aspxbutton id="CancelClientButton" runat="server" clientinstancename="CancelClientButton"
                                                    text="Cancel" causesvalidation="false" onclick="CancelClientButton_Click">
                                                </dx:aspxbutton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxpanel>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>
<dx:aspxpopupcontrol id="InfoMessagePopupControl" runat="server" clientinstancename="InfoMessagePopupControl"
    popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter" allowdragging="false"
    headertext="Confirmation Message" closeaction="CloseButton" width="300px">
    <contentcollection>
        <dx:popupcontrolcontentcontrol>
            <dx:aspxpanel id="Aspxpanel1" runat="server">
                <panelcollection>
                    <dx:panelcontent>
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="SendInviteCompleted" runat="server" clientinstancename="SendInviteCompleted"
                                        text="The send invitations process has completed.">
                                    </dx:aspxlabel>
                                    <dx:aspxlabel id="ValidatonLabel" runat="server" clientinstancename="ValidationLabel"
                                        text="At least one user must be selected">
                                    </dx:aspxlabel>
                                    <dx:aspxlabel id="FailLabel" runat="server" clientinstancename="FailLabel" text="Invites could not be sent to the following recipients:">
                                    </dx:aspxlabel>
                                    <dx:aspxmemo id="FailRecipientMemo" runat="server" clientinstancename="FailRecipientMemo"
                                        height="250px" width="100%">
                                    </dx:aspxmemo>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <dx:aspxbutton id="OkButton" runat="server" clientinstancename="OkButton" text="OK"
                                        autopostback="false">
                                        <clientsideevents click="OkButton_Click" />
                                    </dx:aspxbutton>
                                </td>
                            </tr>
                        </table>
                    </dx:panelcontent>
                </panelcollection>
            </dx:aspxpanel>
        </dx:popupcontrolcontentcontrol>
    </contentcollection>
</dx:aspxpopupcontrol>
<asp:linqdatasource id="ClientDataSource" runat="server" onselecting="ClientDataSource_Selecting">
</asp:linqdatasource>
<asp:linqdatasource id="ClientUserGroupDataSource" runat="server" onselecting="ClientUserGroupDataSource_Selecting">
</asp:linqdatasource>
<dx:aspxcallback id="ClientSelectionCallback" runat="server" clientinstancename="ClientSelectionCallback"
    oncallback="ClientSelectionCallback_Callback">
    <clientsideevents callbackcomplete="ClientSelectionCallback_CallbackComplete" />
</dx:aspxcallback>
<table width="100%">
    <tr>
        <td>
            <dx:aspxlabel id="ClientUserGroupLabel" runat="server" text="Client User Group">
            </dx:aspxlabel>
        </td>
        <td>
            <dx:aspxcombobox id="ClientUserGroupComboBox" runat="server" clientinstancename="ClientUserGroupComboBox"
                dropdownstyle="DropDown" datasourceid="ClientUserGroupDataSource" valuetype="System.Int32"
                valuefield="ClientUserGroupID" textfield="ClientUserGroupName" oncallback="ClientUserGroupComboBox_Callback">
                <clientsideevents selectedindexchanged="ClientUserGroupComboBox_SelectedIndexChanged"
                    lostfocus="ClientUserGroupComboBox_LostFocus" />
                <validationsettings errordisplaymode="ImageWithTooltip">
                    <requiredfield isrequired="true" errortext="Client user group is required" />
                </validationsettings>
            </dx:aspxcombobox>
        </td>
        <td>
            <dx:aspxlabel id="GameLabel" runat="server" text="Game">
            </dx:aspxlabel>
        </td>
        <td>
     <dx:ASPxDropDownEdit ID="GameComboBox" ClientInstanceName="GameComboBox" Width="210px" runat="server" AnimationType="None" ReadOnly="True" >        
        <DropDownWindowTemplate>
            <dx:ASPxListBox Width="100%" ID="GameListBox" ClientInstanceName="checkListBox" SelectionMode="CheckColumn"
                runat="server" datasourceid="GameDataSource" valuefield="GameID"
                valuetype="System.Int32" textfield="GameTemplateNameForDashboard">
                <Border BorderStyle="None" />
                <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
            </dx:ASPxListBox>
<%--            <table style="width: 100%">
                <tr>
                    <td style="padding: 4px">
                        <dx:ASPxButton ID="ASPxButton1" AutoPostBack="False" runat="server" Text="Close" style="float: right">
                            <ClientSideEvents Click="function(s, e){ GameComboBox.HideDropDown(); }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>--%>
        </DropDownWindowTemplate>
        <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
         <ValidationSettings EnableCustomValidation="True" errordisplaymode="ImageWithTooltip">
                    <RequiredField isrequired="true" errortext="Game is required" />
         </ValidationSettings>
    </dx:ASPxDropDownEdit>

        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxlabel id="ThemeLabel" runat="server" text="Theme">
            </dx:aspxlabel>
        </td>
        <td>
            <dx:aspxcombobox id="ThemeComboBox" runat="server" clientinstancename="ThemeComboBox"
                dropdownstyle="DropDownList" valuetype="System.String"
                valuefield="ExternalID" textfield="ThemeName" oncallback="ThemeComboBox_Callback">
                <clientsideevents endcallback="OnThemeComboBoxEndCallback"/>
            </dx:aspxcombobox>
        </td>
        <td></td>
        <td><dx:ASPxCheckBox ID="EmailCheckBox" clientinstancename="EmailCheckBox" runat="server" Text="Do not send emails" ></dx:ASPxCheckBox></td>
    </tr>
    <tr>
        <td colspan="4">
            <dx:aspxbutton id="SendEmailsButton" clientinstancename="SendEmailsButton" runat="server" text="Send Emails to Selected Users" clientenabled="False" causesvalidation="False" autopostback="False">
                <clientsideevents click="SendEmails_Clicked"></clientsideevents>
            </dx:aspxbutton>
        </td>
    </tr>
</table>
<dx:aspxgridview id="UserGridView" runat="server" clientinstancename="UserGridView"
    autogeneratecolumns="False" keyfieldname="UserId" datasourceid="UserDataSource"
    width="100%" oncustomcallback="UserGridView_CustomCallback">
    <clientsideevents selectionchanged="UserGridView_SelectionChanged"></clientsideevents>
    <settingspager pagesize="25">
    </settingspager>
    <columns>
        <dx:gridviewcommandcolumn showselectcheckbox="True" showclearfilterbutton="true" visibleindex="0" selectallcheckboxmode="AllPages" />
        <dx:gridviewdatatextcolumn fieldname="UserId" visibleindex="1" readonly="true" visible="false">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="UserFirstName" caption="First Name" visibleindex="2">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="UserLastName" caption="Last Name" visibleindex="3">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="UserPersonalEmail1" caption="Email" visibleindex="4">
        </dx:gridviewdatatextcolumn>
    </columns>
</dx:aspxgridview>

<asp:linqdatasource id="UserDataSource" runat="server" onselecting="UserDataSource_Selecting">
</asp:linqdatasource>
<asp:linqdatasource id="GameDataSource" runat="server" onselecting="GameDataSource_Selecting">
</asp:linqdatasource>
<dx:aspxcallback id="SaveCallback" runat="server" clientinstancename="SaveCallback"
    oncallback="SaveCallback_Callback">
    <clientsideevents callbackcomplete="SaveCallback_CallbackComplete" />
</dx:aspxcallback>
<dx:aspxloadingpanel id="LoadingPanel" runat="server" clientinstancename="LoadingPanel"
    modal="true">
</dx:aspxloadingpanel>
