﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;
using ComLib;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Data;
using DevExpress.Web.Rendering;
using Diversity.Common.Enumerations;
using Diversity.DataModel.SqlRepository;
using Diversity.Services.Business;
using Diversity.Services.Business.Implementation;

namespace Diversity.Presentation.Web.UserControls.Admin
{
    public partial class GameControl : System.Web.UI.UserControl
    {
        #region Classes

        public class GameCardGroup
        {
            private List<GameCard> _cards;

            public GameCardGroup()
            {
                _cards = new List<GameCard>();
            }

            public int GameCardGroupId { get; set; }
            public int CardGroupSequenceNo { get; set; }
            public bool IsAMandatoryCardGroup { get; set; }
            public List<GameCard> Cards
            {
                get { return _cards; }
            }

            public DateTime? LastModifiedOnDateTime { get; set; }
            public bool HasBranchingCard { get; set; }
        }

        public class GameCard
        {
            public int GameCardId { get; set; }
            public int CardId { get; set; }
            public int CardTypeId { get; set; }
            public string CardShortcutCode { get; set; }
            public string CardName { get; set; }
            public string DetailedCardDescription { get; set; }
            public bool IsRatified { get; set; }
            public int SequenceNo { get; set; }
            public bool UseLongVersion { get; set; }
            public bool IsAMandatoryCard { get; set; }
            public DateTime? LastModifiedOnDateTime { get; set; }
        }

        public class CardClassification
        {
            public int CardClassificationGroupID { get; set; }
            public string CardClassificationGroupImageURL { get; set; }
            public int? CardClassificationGroupID_Parent { get; set; }
            public string CardClassificationGroup { get; set; }
            public bool IsPrimaryCard { get; set; }
        }

        public class SearchResult
        {
            public int CardId { get; set; }
            public int LanguageId { get; set; }
            public string CardShortcutCode { get; set; }
            public string CardName { get; set; }
            public string DetailedCardDescription { get; set; }
            public bool IsRatified { get; set; }
        }

        public class ProductListItem
        {
            public int ProductID { get; set; }
            public string ProductName { get; set; }
        }

        #endregion

        #region Constants

        private const string GAME_CARD_GROUP_DATA_KEY = "4c92cd2e-241b-49e7-8a13-92a64132b16c";
        private const string SEARCH_RESULTS_DATA_KEY = "5C2D5F53-F791-4357-88BE-8A7C6DDD291A";
        private const string SELECTING_GAME_CARD_GROUP_ID = "10EFB9C6-E514-4B5F-9CA2-D1785C603AA5";
        private const string PRODUCT_ID_SELECTED = "SelectedProductID";

        #endregion
        
        List<GameCardGroup> GameCardGroupData
        {
            get
            {
                if (Session[GAME_CARD_GROUP_DATA_KEY] == null)
                {
                    Session[GAME_CARD_GROUP_DATA_KEY] = CreateGameCardGroupData();
                }

                return Session[GAME_CARD_GROUP_DATA_KEY] as List<GameCardGroup>;
            }
            set { Session[GAME_CARD_GROUP_DATA_KEY] = value; }
        }

        List<SearchResult> SearchResultData
        {

            get
            {
                if (Session[SEARCH_RESULTS_DATA_KEY] == null)
                {
                    Session[SEARCH_RESULTS_DATA_KEY] = CreateSearchResultData();
                }

                return Session[SEARCH_RESULTS_DATA_KEY] as List<SearchResult>;
            }
            set { Session[SEARCH_RESULTS_DATA_KEY] = value; }
        }

        int ProductId
        {

            get
            {
                if (Session[PRODUCT_ID_SELECTED] == null)
                {
                    Session[PRODUCT_ID_SELECTED] = -1;
                }

                return Convert.ToInt32(Session[PRODUCT_ID_SELECTED]);
            }

            set { Session[PRODUCT_ID_SELECTED] = value; }
        }

        int? SelectingGameCardGroupId
        {
            get
            {
                if (Session[SELECTING_GAME_CARD_GROUP_ID] == null)
                {
                    return null;
                }
                else
                {
                    return (int)Session[SELECTING_GAME_CARD_GROUP_ID];
                }
            }
            set { Session[SELECTING_GAME_CARD_GROUP_ID] = value; }
        }

        protected bool IsNewGame
        {
            get
            {
                var v = Request.QueryString.Get("id");
                return v == null;
            }
        }

        int GameId
        {
            get
            {
                var v = Request.QueryString.Get("id");
                return v == null ? -1 : Convert.ToInt32(v);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if(Request.UrlReferrer == null)
            {
                Response.Redirect(ResolveUrl(@"~/Default.aspx"));
            }

            var count = GetProducts().Count();
            
            if(count <= 0)
            {
                var exception = new UIMessageException(Resources.Exceptions.ZeroProductAssignedToProfile);
                exception.MessageCode = "ZeroProductAssignedToProfile";
                throw exception;
            }

            if(IsNewGame)
            {
                ProductSelectionPopupControl.ShowOnPageLoad = true;
            }
        }

        private List<GameCardGroup> CreateGameCardGroupData()
        {
            return new List<GameCardGroup>();
        }

        List<SearchResult> CreateSearchResultData()
        {
            return new List<SearchResult>();                       
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);
            if (!Page.IsPostBack)
            {
                ProductId = -1;
                GameCardGroupData = CreateGameCardGroupData();
                SearchResultData = CreateSearchResultData();

                Populate();

            }
        }

        void Populate()
        {
            if (!IsNewGame)
            {
                using (var dataContext = new EChallengeDataContext())
                {
                    var game = dataContext.Games.Where(g => g.GameID == GameId).FirstOrDefault();
                    var lcid = (Page as BasePage).CurrentContext.Culture;
                    var language = (from l in dataContext.Languages
                                    where l.LCID == lcid
                                    select l).FirstOrDefault();

                    if (game == null)
                    {
                        throw new Exception(Resources.Exceptions.GameDoesNotExist);
                    }

                    ProductId = game.ProductID;

                    var product = game.Product.Product_Languages.Where(p => p.LanguageID == language.LanguageID).FirstOrDefault();

                    ProductNameLiteral.Text = product.ProductName;
                       

                    GameNameTextBox.Text = game.GameTemplateNameForDashboard;

                    var theme = GameThemeComboBox.Items.FindByValue(game.GameThemeData);
                    if (theme != null)
                    {
                        theme.Selected = true;
                    }
                    var gameType = game.IsOnlineGame ? "Online" : "Desktop";
                    var gameTypeItem = GameTypeComboBox.Items.FindByValue(gameType);
                    if (gameTypeItem != null)
                    {
                        gameTypeItem.Selected = true;
                    }

                    NumberOfPlaySpinEdit.Value = game.NumberOfTimesGameCanBePlayed;
                    CourseCodeTextLabel.Text = game.ExternalID;

                    LoadGameCardGroupData(game.GameID);

                    GameCardGroupGridView.DataBind();
                }
            }
            else
            {
                NumberOfPlaySpinEdit.Value = 1;
            }
        }

        void LoadGameCardGroupData(int gameId)
        {
            using (var dataContext = new EChallengeDataContext())
            {
                var game = dataContext.Games.Where(g => g.GameID == gameId).FirstOrDefault();
                var lcid = (Page as BasePage).CurrentContext.Culture;
                var language = (from l in dataContext.Languages
                                where l.LCID == lcid
                                select l).FirstOrDefault();

                GameCardGroupData = new List<GameCardGroup>();

                foreach (var groupSequence in game.GameCardGroupSequences.OrderBy(g=>g.CardGroupSequenceNo))
                {
                    var gameCardGroup = new GameCardGroup();
                    gameCardGroup.GameCardGroupId = groupSequence.GameCardGroupSequenceID;
                    gameCardGroup.CardGroupSequenceNo = groupSequence.CardGroupSequenceNo;
                    gameCardGroup.IsAMandatoryCardGroup = groupSequence.IsAMandatoryCardGroup;
                    gameCardGroup.LastModifiedOnDateTime = groupSequence.LastModifiedOnDateTime;

                    foreach (var cardSequence in groupSequence.GameCardSequences.OrderBy(g=>g.CardSequenceNo))
                    {
                        var gameCard = new GameCard();
                        gameCard.GameCardId = cardSequence.GameCardSequenceID;
                        gameCard.CardId = cardSequence.CardID;

                        CardType cardType;
                        bool result = Enum.TryParse(cardSequence.Card.Internal_CardType.Internal_CardTypeShortCode,
                                                    out cardType);

                        if(!result)
                        {
                            throw new Exception(Resources.Exceptions.UnableToDetermineCardType);
                        }

                        gameCard.CardTypeId = (int) cardType;

                        if (cardType == CardType.BranchingCard)
                        {
                            gameCardGroup.HasBranchingCard = true;
                        }

                        var cardLanguage =
                            dataContext.Card_Languages.FirstOrDefault(c => c.LanguageID == language.LanguageID && c.CardID == cardSequence.CardID);

                        gameCard.CardName = cardLanguage.CardName;

                        gameCard.CardShortcutCode = cardSequence.Card.CardShortcutCode;
                        gameCard.DetailedCardDescription = cardLanguage.DetailedCardDescription;
                        gameCard.IsAMandatoryCard = cardSequence.IsAMandatoryCard;
                        gameCard.IsRatified = cardSequence.Card.IsRatified;
                        gameCard.SequenceNo = cardSequence.CardSequenceNo;
                        gameCard.UseLongVersion = cardSequence.UseLongVersion;
                        gameCard.LastModifiedOnDateTime = cardSequence.LastModifiedOnDateTime;

                        gameCardGroup.Cards.Add(gameCard);
                    }

                    GameCardGroupData.Add(gameCardGroup);
                }
            }
        }

        protected void CardClassificationDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();

            var lcid = (Page as BasePage).CurrentContext.Culture;
            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            var ids = (from c in dataContext.Card2CardClassifications
                           select c.CardClassificationGroupID).Distinct();

            var children = from c in dataContext.CardClassificationGroups
                          from l in c.CardClassificationGroup_Languages
                          where l.LanguageID == language.LanguageID
                                && c.IsAMetaTagClassificationGroup == false
                                && c.ProductID == ProductId
                                && ids.Contains(c.CardClassificationGroupID) || ids.Contains(c.CardClassificationGroupID_Parent.Value)
                          select new
                                     {
                                         l.CardClassificationGroup,
                                         c.CardClassificationGroupID,
                                         c.CardClassificationGroupID_Parent
                                     };

            var results = (from c in dataContext.CardClassificationGroups
                           from l in c.CardClassificationGroup_Languages
                           join ch in children on c.CardClassificationGroupID equals ch.CardClassificationGroupID_Parent
                           where l.LanguageID == language.LanguageID
                                 && !c.IsAMetaTagClassificationGroup
                                 && c.ProductID == ProductId
                           select new
                                      {
                                          l.CardClassificationGroup,
                                          c.CardClassificationGroupID,
                                          c.CardClassificationGroupID_Parent
                                      })
                .Union(children);
                                                     
            e.Result = results;
        }

        protected void ProductDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = GetProducts();
        }
        
        IEnumerable<ProductListItem> GetProducts()
        {
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lcid = (Page as BasePage).CurrentContext.Culture;
            var dataContext = new EChallengeDataContext();

            var language = (from l in dataContext.Languages
                            where l.LCID == lcid
                            select l).FirstOrDefault();

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                return (from p in dataContext.Products
                        from pl in p.Product_Languages
                        where pl.LanguageID == language.LanguageID
                        select new ProductListItem
                                   {
                                       ProductID = p.ProductID,
                                       ProductName = pl.ProductName
                                   })
                    .OrderBy(r => r.ProductName);
            }

            return (from p in dataContext.Products
                    from pd in p.Product2Distributors
                    from pl in p.Product_Languages
                    let distributors = dataContext.User2Distributors
                        .Where(d => d.UserID == userId)
                        .Select(d => d.DistributorID)
                    where distributors.Contains(pd.DistributorID) && pl.LanguageID == language.LanguageID
                    select new ProductListItem
                               {
                                   ProductID = p.ProductID,
                                   ProductName = pl.ProductName
                               })
                .OrderBy(r => r.ProductName);
        }

        protected void GameCardGroupDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = GameCardGroupData.OrderBy(g => g.CardGroupSequenceNo);
        }

        protected void GameCardDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var gameCardGroup = GameCardGroupData.Where(g => g.GameCardGroupId == SelectingGameCardGroupId).FirstOrDefault();

            if (gameCardGroup == null)
            {
                e.Result = new List<GameCard>();
            }
            else
            {
                e.Result = gameCardGroup.Cards.OrderBy(g => g.SequenceNo);
            }
        }

        protected void ProductSelectionCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            ProductId = Convert.ToInt32(e.Parameter);
        }

        void SetNodeSelectionSettings(ASPxTreeList treeList)
        {
            var iterator = treeList.CreateNodeIterator();

            TreeListNode node;
            while (true)
            {
                node = iterator.GetNext();

                if (node == null)
                {
                    break;
                }

                if (node.Level <= 1)
                {
                    node.AllowSelect = false;
                }
            }
        }

        protected void CardClassificationTreeList_DataBound(object sender, EventArgs e)
        {
            var treeList = sender as ASPxTreeList;
            SetNodeSelectionSettings(treeList);
        }

        protected void ClientLibraryComboBox_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            var comboBox = sender as ASPxComboBox;
            comboBox.DataBind();
        }

        protected void ClientDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var dataContext = new EChallengeDataContext();
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
            {
                e.Result = from uc in dataContext.User2Clients
                           orderby uc.Client.ClientName
                           select uc.Client;
            }
            else
            {
                e.Result = from c in dataContext.Clients
                           from uc in c.User2Clients
                           where uc.UserID == userId
                           orderby c.ClientName
                           select c;
            }
        }

        protected void CardTypeDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = ListHelper.CardTypes.OrderBy(c=>c.Internal_CardTypeName);
        }

        protected void SearchResultDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = SearchResultData;
        }

        enum Library
        {
            Public,
            Client,
            Both
        }

        protected void SearchResultGridView_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            var parameters = e.Parameters.Split(new[] { "|" }, StringSplitOptions.None);
            if (parameters.Count() == 5)
            {
                var cardType = parameters[0].Trim();
                var version = parameters[1].Trim();
                var library = parameters[2].Trim();
                var classifications = parameters[3].Trim();
                var searchText = parameters[4].Trim();


                var dataContext = new EChallengeDataContext();

                var userId = (Page as BasePage).CurrentContext.UserIdentifier;
                var lcid = (Page as BasePage).CurrentContext.Culture;
                var language = (from l in dataContext.Languages
                                where l.LCID == lcid
                                select l).FirstOrDefault();

                var query = from c in dataContext.Cards
                            from cl in c.Card_Languages
                            where cl.LanguageID == language.LanguageID && c.Product.ProductID == ProductId && !c.IsForGame
                            select c;

                if (!Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE))
                {
                    query = from c in query
                            from pd in c.Product.Product2Distributors
                            let distributors = dataContext.User2Distributors
                                .Where(d => d.UserID == userId)
                                .Select(d => d.DistributorID)
                            where distributors.Contains(pd.DistributorID)
                            select c;
                }


                if (cardType.Length > 0)
                {
                    var cardTypeId = Convert.ToInt32(cardType);

                    query = query.Where(c => c.Internal_CardTypeID == cardTypeId);

                    var dbCardType =
                        dataContext.Internal_CardTypes.FirstOrDefault(t => t.Internal_CardTypeID == cardTypeId);

                    if (dbCardType.Internal_CardTypeShortCode == CardType.BranchingCard.ToString())
                    {
                        query = from c in query.OfType<BranchingCard>()
                            join g in dataContext.Games on c.GameID equals g.GameID
                            where g.GameID == GameId
                            select c;
                    }
                }

                bool? shortVersion = null;

                if (version.Length > 0)
                {
                    if (string.Compare(version, "short", true) == 0)
                    {
                        shortVersion = true;
                    }

                    if (string.Compare(version, "long", true) == 0)
                    {
                        shortVersion = false;
                    }

                    if (shortVersion.HasValue)
                    {
                        if (shortVersion.Value)
                        {
                            query = from c in query
                                    from ql in c.QuestionCard.QuestionCard_Languages
                                    where
                                        ql.Question_Statement_ShortVersion.Length > 0
                                    select c;
                        }
                        else
                        {
                            query = from c in query
                                    from ql in c.QuestionCard.QuestionCard_Languages
                                    where
                                        ql.Question_Statement_LongVersion.Length > 0
                                    select c;
                        }
                    }
                }

                if (classifications.Length > 0)
                {
                    var ids = classifications.Split(';');
                    var classificationIds = new List<int>();

                    if (ids.Count() > 0)
                    {
                        classificationIds.AddRange(ids.Select(i => Convert.ToInt32(i.Trim())));
                    }

                    query = from c in query
                            from classification in c.Card2CardClassifications
                            where classificationIds.Contains(classification.CardClassificationGroupID)
                            select c;
                }

                if (library.Length > 0)
                {
                    var libToSearch = (Library)Enum.Parse(typeof(Library), library, true);

                    switch (libToSearch)
                    {
                        case Library.Client:
                            query = query.Where(c => c.IsForClientlibraryElsePublicLibrary);
                            break;

                        case Library.Public:
                            query = query.Where(c => !c.IsForClientlibraryElsePublicLibrary);
                            break;
                    }
                }


                if (searchText.Length > 0)
                {
                    if (shortVersion.HasValue)
                    {
                        if (shortVersion.Value)
                        {
                            query = from c in query
                                    from ql in c.QuestionCard.QuestionCard_Languages
                                    where ql.Question_Statement_ShortVersion.Contains(searchText)
                                    select c;
                        }
                        else
                        {
                            query = from c in query
                                    from ql in c.QuestionCard.QuestionCard_Languages
                                    where ql.Question_Statement_LongVersion.Contains(searchText)
                                    select c;
                        }
                    }
                    else
                    {
                        query = from c in query
                                from ql in c.QuestionCard.QuestionCard_Languages
                                where ql.Question_Statement_ShortVersion.Contains(searchText) ||
                                      ql.Question_Statement_LongVersion.Contains(searchText)
                                select c;
                    }
                }

                var results = (from c in query                               
                              select new SearchResult()
                                         {
                                             CardId = c.CardID,
                                             //CardName = cardLanguage.CardName,
                                             CardShortcutCode = c.CardShortcutCode,
                                             //DetailedCardDescription = cardLanguage.DetailedCardDescription,
                                             IsRatified = c.IsRatified,
                                             LanguageId = language.LanguageID
                                         }).ToList();

                foreach (var searchResult in results)
                {
                    var cardLanguage =
                        dataContext.Card_Languages.FirstOrDefault(
                            l => l.CardID == searchResult.CardId && l.LanguageID == language.LanguageID);

                    searchResult.CardName = cardLanguage.CardName;
                    searchResult.DetailedCardDescription = cardLanguage.DetailedCardDescription;
                }

                SearchResultData = results;
                SearchResultGridView.DataBind();
            }
        }

        protected void GameCardGroupGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            var cardSelectionComboBox = gridview.FindEditFormTemplateControl(@"SelectCardComboBox") as ASPxComboBox;
            var mandatoryCardGroupCheckBox =
                gridview.FindEditFormTemplateControl(@"IsAMandatoryCardGroupCheckBox") as ASPxCheckBox;
            var mandatoryCardCheckBox = gridview.FindEditFormTemplateControl(@"IsAMandatoryCardCheckBox") as ASPxCheckBox;
            var versionTypeComboBox = gridview.FindEditFormTemplateControl(@"LongShortVersionComboBox") as ASPxComboBox;

            var createdOn = DateTime.Now;
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var isBranchingCardGroup = false;

            using (var dataContext = new EChallengeDataContext())
            {
                var game = dataContext.Games.FirstOrDefault(g => g.GameID == GameId);

                if (game == null)
                {
                    throw new Exception(Resources.Exceptions.GameDoesNotExist);
                }

                var sequenceNo = game.GameCardGroupSequences.Count + 1;

                var gameCardGroup = new GameCardGroupSequence();
                gameCardGroup.GameID = game.GameID;
                gameCardGroup.CardGroupSequenceNo = sequenceNo;
                gameCardGroup.CreatedOnDateTime = createdOn;
                gameCardGroup.UserID_CreatedBy = userId;
                gameCardGroup.LastModifiedOnDateTime = createdOn;
                gameCardGroup.UserID_LastModifiedBy = userId;

                if (mandatoryCardGroupCheckBox != null)
                {
                    gameCardGroup.IsAMandatoryCardGroup = mandatoryCardGroupCheckBox.Checked;
                }

                var gameCard = new GameCardSequence();

                if (cardSelectionComboBox != null)
                {
                    var cardId = Convert.ToInt32(cardSelectionComboBox.SelectedItem.Value);
                    var duplicateCard = GetDuplicateCardForGame(cardId, userId);
                    gameCard.CardID = duplicateCard.CardID;
                    gameCard.Card = duplicateCard;

                    if (duplicateCard is BranchingCard)
                    {
                        isBranchingCardGroup = true;
                    }
                }

                if (mandatoryCardCheckBox != null)
                {
                    gameCard.IsAMandatoryCard = mandatoryCardCheckBox.Checked;
                }

                if (versionTypeComboBox != null)
                {
                    gameCard.UseLongVersion =
                        string.Compare(versionTypeComboBox.SelectedItem.Value.ToString(), "L", true) == 0;
                }

                gameCard.CardSequenceNo = gameCardGroup.GameCardSequences.Count() + 1;
                gameCard.CreatedOnDateTime = createdOn;
                gameCard.UserID_CreatedBy = userId;
                gameCard.LastModifiedOnDateTime = createdOn;
                gameCard.UserID_LastModifiedBy = userId;

                gameCardGroup.GameCardSequences.Add(gameCard);

                 if (isBranchingCardGroup)
                {
                    var branchingCard = gameCard.Card as BranchingCard;
                    var cardGroupIds =
                        branchingCard.BranchingCard_Buttons.Select(b => b.CardGroupID_Result).ToList();

                    var minsequenceNumber = GameCardGroupData.Where(g => cardGroupIds.Contains(g.GameCardGroupId))
                        .Min(g => g.CardGroupSequenceNo);
                    
                    var cardGroupsToShuffle =
                        game.GameCardGroupSequences.Where(g => g.CardGroupSequenceNo >= minsequenceNumber).ToList();

                    foreach (var gameCardGroupSequence in cardGroupsToShuffle)
                    {
                        gameCardGroupSequence.CardGroupSequenceNo += 1;
                    }

                    gameCardGroup.CardGroupSequenceNo = minsequenceNumber;
                }
                
                using(var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.GameCardGroupSequences.Add(gameCardGroup);

                        dataContext.SaveChanges();
                        transaction.Commit();

                        LoadGameCardGroupData(game.GameID);

                        e.Cancel = true;
                        gridview.CancelEdit();
                        gridview.DataBind();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                       
                }
            }
        }

        protected void GameCardGroupGridView_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            SelectingGameCardGroupId = null;
            e.NewValues["IsAMandatoryCardGroup"] = true;
        }

        protected void GameCardGridView_BeforePerformDataSelect(object sender, EventArgs e)
        {
            SelectingGameCardGroupId = Convert.ToInt32((sender as ASPxGridView).GetMasterRowKeyValue());
        }

        protected void GameCardGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            var cardSelectionComboBox = gridview.FindEditFormTemplateControl(@"SelectDetailCardComboBox") as ASPxComboBox;
            var mandatoryCardCheckBox = gridview.FindEditFormTemplateControl(@"IsAMandatoryCardCheckBox") as ASPxCheckBox;
            var versionTypeComboBox = gridview.FindEditFormTemplateControl(@"LongShortVersionComboBox") as ASPxComboBox;
            var editingGameCardGroupId = Convert.ToInt32(gridview.GetMasterRowKeyValue());

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var createdOn = DateTime.Now;

            using (var dataContext = new EChallengeDataContext())
            {
                var gameCardGroup =
                    dataContext.GameCardGroupSequences.Where(g => g.GameCardGroupSequenceID == editingGameCardGroupId).FirstOrDefault();
                var sequenceNo = gameCardGroup.GameCardSequences.Count + 1;

                var gameCard = new GameCardSequence();                

                if (cardSelectionComboBox != null)
                {
                    var cardId = Convert.ToInt32(cardSelectionComboBox.SelectedItem.Value);
                    var duplicateCard = GetDuplicateCardForGame(cardId, userId);                    
                    gameCard.CardID = duplicateCard.CardID;
                    gameCard.Card = duplicateCard;
                }

                if (mandatoryCardCheckBox != null)
                {
                    gameCard.IsAMandatoryCard = mandatoryCardCheckBox.Checked;
                }

                if (versionTypeComboBox != null)
                {
                    gameCard.UseLongVersion =
                        string.Compare(versionTypeComboBox.SelectedItem.Value.ToString(), "L", true) == 0;
                }
                gameCard.GameCardGroupSequenceID = gameCardGroup.GameCardGroupSequenceID;
                gameCard.CardSequenceNo = gameCardGroup.GameCardSequences.Count() + 1;
                gameCard.CreatedOnDateTime = createdOn;
                gameCard.UserID_CreatedBy = userId;
                gameCard.LastModifiedOnDateTime = createdOn;
                gameCard.UserID_LastModifiedBy = userId;


                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.GameCardSequences.Add(gameCard);
                        dataContext.SaveChanges();
                        transaction.Commit();

                        LoadGameCardGroupData(gameCardGroup.GameID);

                        e.Cancel = true;
                        gridview.CancelEdit();
                        gridview.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        protected void GameCardGroupGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            var mandatoryCardGroupCheckBox =
                gridview.FindEditFormTemplateControl(@"IsAMandatoryCardGroupCheckBox") as ASPxCheckBox;
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var gameCardGroupId = (int)e.Keys[0];

            using (var dataContext = new EChallengeDataContext())
            {
                var gameCardGroup =
                    dataContext.GameCardGroupSequences.FirstOrDefault(g => g.GameCardGroupSequenceID == gameCardGroupId);

                if (gameCardGroup == null)
                {
                    throw new Exception(Resources.Exceptions.GameCardGroupDoesNotExist);
                }

                gameCardGroup.LastModifiedOnDateTime = DateTime.Now;
                gameCardGroup.UserID_LastModifiedBy = userId;

                if (mandatoryCardGroupCheckBox != null)
                {
                    gameCardGroup.IsAMandatoryCardGroup = mandatoryCardGroupCheckBox.Checked;
                }


                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();

                        LoadGameCardGroupData(gameCardGroup.GameID);

                        e.Cancel = true;
                        gridview.CancelEdit();
                        gridview.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }              
            }
        }

        protected void GameCardGroupGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            var gameCardGroupSequenceId = (int)e.Keys[0];

            using (var dataContext = new EChallengeDataContext())
            {
                var gameCardGroup =
                    dataContext.GameCardGroupSequences.Where(g => g.GameCardGroupSequenceID == gameCardGroupSequenceId).
                        FirstOrDefault();



                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.Cards.RemoveRange(gameCardGroup.GameCardSequences.Select(c => c.Card));
                        dataContext.GameCardGroupSequences.Remove(gameCardGroup);

                        dataContext.SaveChanges();
                        transaction.Commit();

                        LoadGameCardGroupData(GameId);

                        e.Cancel = true;
                        gridview.CancelEdit();
                        gridview.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }              
            }
        }

        protected void GameCardGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            var cardSelectionComboBox = gridview.FindEditFormTemplateControl(@"SelectDetailCardComboBox") as ASPxComboBox;
            var mandatoryCardCheckBox = gridview.FindEditFormTemplateControl(@"IsAMandatoryCardCheckBox") as ASPxCheckBox;
            var versionTypeComboBox = gridview.FindEditFormTemplateControl(@"LongShortVersionComboBox") as ASPxComboBox;

            var userId = (Page as BasePage).CurrentContext.UserIdentifier;
            var lastModifiedOn = DateTime.Now;

            using (var dataContext = new EChallengeDataContext())
            {
                var gameCardSequenceId = (int) e.Keys[0];

                var gameCard = dataContext.GameCardSequences.Single(c => c.GameCardSequenceID == gameCardSequenceId);

                if (gameCard != null)
                {
                    if (cardSelectionComboBox != null)
                    {
                        var cardId = Convert.ToInt32(cardSelectionComboBox.SelectedItem.Value);

                        if (cardId != gameCard.CardID)
                        {
                            var duplicateCard = GetDuplicateCardForGame(cardId, userId);
                            var oldCardId = gameCard.CardID;                            
                            gameCard.Card = duplicateCard;

                            var oldCard = dataContext.Cards.Where(c => c.CardID == oldCardId).FirstOrDefault();
                            dataContext.Cards.Remove(oldCard);
                        }

                    }

                    if (mandatoryCardCheckBox != null)
                    {
                        gameCard.IsAMandatoryCard = mandatoryCardCheckBox.Checked;
                    }

                    if (versionTypeComboBox != null)
                    {
                        gameCard.UseLongVersion =
                            string.Compare(versionTypeComboBox.SelectedItem.Value.ToString(), "L", true) == 0;
                    }

                    gameCard.LastModifiedOnDateTime = lastModifiedOn;
                    gameCard.UserID_LastModifiedBy = userId;
                }


                using(var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();

                        LoadGameCardGroupData(gameCard.GameCardGroupSequence.GameID);

                        e.Cancel = true;
                        gridview.CancelEdit();
                        gridview.DataBind();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }                
            }
        }

        private Card GetDuplicateCardForGame(int cardId, Guid userId)
        {
            var cardService = new CardService() as ICardService;
            var duplicateCard = cardService.CreateDuplicateCard(cardId, userId, true);
            duplicateCard.IsForGame = true;
            return duplicateCard;
        }

        protected void GameCardGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var gridview = sender as ASPxGridView;
            var gameCardSequenceId = (int)e.Keys[0];

            using (var dataContext = new EChallengeDataContext())
            {                
                var gameCard =
                    dataContext.GameCardSequences.FirstOrDefault(g => g.GameCardSequenceID == gameCardSequenceId);

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        var userId = (Page as BasePage).CurrentContext.UserIdentifier;
                        dataContext.upDeleteCard(gameCard.CardID, userId, true);
                        dataContext.SaveChanges();
                        transaction.Commit();

                        LoadGameCardGroupData(GameId);

                        e.Cancel = true;
                        gridview.CancelEdit();
                        gridview.DataBind();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }

        }

        protected void SelectDetailCardComboBox_Init(object sender, EventArgs e)
        {
            var comboBox = sender as ASPxComboBox;
            var editForm = (comboBox.NamingContainer as GridViewEditFormTemplateContainer);

            if (editForm.Grid.IsNewRowEditing)
            {
                return;
            }

            var visibleIndex = editForm.Grid.EditingRowVisibleIndex;

            var cardId = editForm.Grid.GetRowValues(visibleIndex, "CardId");
            var cardName = editForm.Grid.GetRowValues(visibleIndex, "CardName");

            if (cardId == null || cardName == null)
            {
                return;
            }

            var listeditItem = new ListEditItem();
            listeditItem.Value = Convert.ToInt32(cardId);
            listeditItem.Text = cardName.ToString();
            listeditItem.Selected = true;
            comboBox.Items.Add(listeditItem);

        }


        protected void LongShortVersionComboBox_DataBound(object sender, EventArgs e)
        {
            var comboBox = sender as ASPxComboBox;
            var editForm = (comboBox.NamingContainer as GridViewEditFormTemplateContainer);

            if (editForm.Grid.IsNewRowEditing)
            {
                return;
            }

            var visibleIndex = editForm.Grid.EditingRowVisibleIndex;

            var rowValues = editForm.Grid.GetRowValues(visibleIndex, "UseLongVersion");


            if (rowValues == null)
            {
                return;
            }

            var useLongVersion = Convert.ToBoolean(rowValues);

            if (useLongVersion)
            {
                comboBox.Items.FindByValue("L").Selected = true;
            }
            else
            {
                comboBox.Items.FindByValue("S").Selected = true;
            }
        }

        protected void GameCardGroupGridView_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {

            if (string.Compare(e.ButtonID, "UpCustomButton") == 0)
            {
                MoveGameCardGroupSelection(-1, e.VisibleIndex);
            }

            if (string.Compare(e.ButtonID, "DownCustomButton") == 0)
            {
                MoveGameCardGroupSelection(1, e.VisibleIndex);
            }
        }

        bool CanMoveSelection(int offset, ASPxGridView gridView, int visibleIndex)
        {
            return visibleIndex + offset >= 0 && visibleIndex + offset <= gridView.VisibleRowCount - 1;
        }

        void MoveGameCardGroupSelection(int offset, int visibleIndex)
        {
            if (!CanMoveSelection(offset, GameCardGroupGridView, visibleIndex))
            {
                return;
            }

            var count = GameCardGroupData.Count;
            var temp = new GameCardGroup[count];

            var selectedGameCardGroup = GameCardGroupData[visibleIndex];


            if (selectedGameCardGroup.Cards.Any(c => c.CardTypeId == (int)CardType.BranchingCard))
            {
                using (var dataContext = new EChallengeDataContext())
                {
                    var card =
                        selectedGameCardGroup.Cards.FirstOrDefault(
                            c => c.CardTypeId == (int) CardType.BranchingCard);

                    var branchingCard =
                        dataContext.Cards.OfType<BranchingCard>().FirstOrDefault(c => c.CardID == card.CardId);

                    var cardGroupIds =
                        branchingCard.BranchingCard_Buttons.Select(b => b.CardGroupID_Result).ToList();

                    var minsequenceNumber = GameCardGroupData.Where(g => cardGroupIds.Contains(g.GameCardGroupId))
                        .Min(g => g.CardGroupSequenceNo);

                    if (visibleIndex + 1 + offset >= minsequenceNumber)
                    {
                        throw new Exception("Unable to move Branching Card group");
                    }
                }
            }


            selectedGameCardGroup.CardGroupSequenceNo = visibleIndex + offset + 1;
            temp[visibleIndex + offset] = selectedGameCardGroup;
           
            var index = 0;
            for (var i = 0; i < count; i++)
            {
                if (i == visibleIndex)
                {
                    continue;
                }

                while (temp[index] != null)
                {
                    index++;
                }
                
                var gameGroup = GameCardGroupData[i];
                gameGroup.CardGroupSequenceNo = index + 1;
                temp[index] = gameGroup;
                index++;
            }

            GameCardGroupData.Clear();
            GameCardGroupData.AddRange(temp);
            SaveGameCardGroupSequences();
            GameCardGroupGridView.DataBind();
        }

        protected void GameCardGridView_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            if (string.Compare(e.ButtonID, "UpCustomButton") == 0)
            {
                MoveGameCardSelection(-1, gridView,  e.VisibleIndex);
            }

            if (string.Compare(e.ButtonID, "DownCustomButton") == 0)
            {
                MoveGameCardSelection(1, gridView, e.VisibleIndex);
            }

            if (string.Compare(e.ButtonID, "EditCardCustomButton") == 0)
            {
                var item = (GameCard)gridView.GetRow(e.VisibleIndex);

                if(item != null)
                {
                    var url = string.Format(@"~/Admin/Card.aspx?id={0}&type={1}", item.CardId, item.CardTypeId);
                    ASPxWebControl.RedirectOnCallback(url);    
                }
            }
        }


        void MoveGameCardSelection(int offset, ASPxGridView gridView, int visibleIndex)
        {
            if (!CanMoveSelection(offset, gridView, visibleIndex))
            {
                return;
            }

            var cardGroupId = Convert.ToInt32(gridView.GetMasterRowKeyValue());
            var gameCardGroup =
                GameCardGroupData.Where(g => g.GameCardGroupId == cardGroupId).FirstOrDefault();

            var count = gameCardGroup.Cards.Count;

            var cards = new GameCard[count];
            
            for (var i = 0; i < count; i++)
            {
                var card = gameCardGroup.Cards[i];
                
                cards[i] = new GameCard()
                               {
                                   GameCardId = card.GameCardId,
                                   CardId = card.CardId,
                                   CardTypeId = card.CardTypeId,
                                   CardName = card.CardName,
                                   CardShortcutCode = card.CardShortcutCode,
                                   DetailedCardDescription = card.DetailedCardDescription,
                                   IsAMandatoryCard = card.IsAMandatoryCard,
                                   IsRatified = card.IsRatified,
                                   SequenceNo = card.SequenceNo,
                                   UseLongVersion = card.UseLongVersion
                               };
            }

            var temp = new GameCard[count];

            var selectedGameCard = cards[visibleIndex];
            selectedGameCard.SequenceNo = visibleIndex + offset + 1;
            temp[visibleIndex + offset] = selectedGameCard;

            var index = 0;
            for (var i = 0; i < count; i++)
            {
                if (i ==visibleIndex)
                {
                    continue;
                }

                while (temp[index] != null)
                {
                    index++;
                }

                var gameCard = cards[i];
                gameCard.SequenceNo = index + 1;
                temp[index] = gameCard;
                index++;
            }

            gameCardGroup.Cards.Clear();
            gameCardGroup.Cards.AddRange(temp);
            SaveGameCardSequences(cardGroupId);
            gridView.DataBind();
        }

        protected void SaveGameCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            try
            {
                if (IsNewGame)
                {
                    InsertGame();
                }
                else
                {
                    UpdateGame();
                }
            }
            catch
            {
                e.Result = "Failed";                
            }            
        }

        void InsertGame()
        {
            var createdOn = DateTime.Now;
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;

            var game = new Game();
            game.GameTemplateNameForDashboard = GameNameTextBox.Text.Trim();
            game.CreatedOnDateTime = createdOn;
            game.UserID_CreatedBy = userId;
            game.LastModifiedOnDateTime = createdOn;
            game.UserID_LastModifiedBy = userId;
            game.ProductID = ProductId;
            game.IsAGameTemplate = true;
            game.NumberOfTimesGameCanBePlayed = Convert.ToInt32(NumberOfPlaySpinEdit.Value);

            var externalId = Guid.NewGuid().ToString();
            externalId = externalId.Replace("-", string.Empty);
            game.ExternalID = externalId;

            if (GameThemeComboBox.SelectedItem != null)
            {
                game.GameThemeData = GameThemeComboBox.SelectedItem.Value.ToString();
            }

            if (GameTypeComboBox.SelectedItem != null)
            {
                var selectedValue = GameTypeComboBox.SelectedItem.Value.ToString();
                game.IsOnlineGame = string.Compare(selectedValue, "online", true) == 0;
            }

            //if(ScoringComboBox.SelectedItem != null)
            //{
            //    var selectedValue = GameTypeComboBox.SelectedItem.Value.ToString();
            //    //game. = string.Compare(selectedValue, "online", true) == 0;
            //}

            using (var dataContext = new EChallengeDataContext())
            {
                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.Games.Add(game);
                        dataContext.SaveChanges();
                        transaction.Commit();

                        var url = string.Format("{0}?id={1}", Request.Url.AbsolutePath, game.GameID);
                        ASPxWebControl.RedirectOnCallback(url);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        void UpdateGame()
        {
            using (var dataContext = new EChallengeDataContext())
            {
                var modifiedOn = DateTime.Now;
                var userId = (Page as BasePage).CurrentContext.UserIdentifier;

                var game = dataContext.Games.Where(g => g.GameID == GameId).FirstOrDefault();
                game.GameTemplateNameForDashboard = GameNameTextBox.Text.Trim();
                game.LastModifiedOnDateTime = modifiedOn;
                game.UserID_LastModifiedBy = userId;
                game.NumberOfTimesGameCanBePlayed = Convert.ToInt32(NumberOfPlaySpinEdit.Value);

                if (GameThemeComboBox.SelectedItem != null)
                {
                    game.GameThemeData = GameThemeComboBox.SelectedItem.Value.ToString();
                }

                if (GameTypeComboBox.SelectedItem != null)
                {
                    var selectedValue = GameTypeComboBox.SelectedItem.Value.ToString();
                    game.IsOnlineGame = string.Compare(selectedValue, "online", true) == 0;
                }

                //if(ScoringComboBox.SelectedItem != null)
                //{
                //    var selectedValue = GameTypeComboBox.SelectedItem.Value.ToString();
                //    //game. = string.Compare(selectedValue, "online", true) == 0;
                //}

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }

        void SaveGameCardGroupSequences()
        {

            using (var dataContext = new EChallengeDataContext())
            {
                var gameCardGroups = dataContext.GameCardGroupSequences.Where(g => g.GameID == GameId);
                var modifiedOn = DateTime.Now;
                var userId = (Page as BasePage).CurrentContext.UserIdentifier;

                foreach (var cardGroup in gameCardGroups)
                {
                    var localCardGroup =
                        GameCardGroupData.FirstOrDefault(g => g.GameCardGroupId == cardGroup.GameCardGroupSequenceID);

                    if (localCardGroup == null)
                    {
                        continue;
                    }

                    cardGroup.CardGroupSequenceNo = localCardGroup.CardGroupSequenceNo;
                    cardGroup.LastModifiedOnDateTime = modifiedOn;
                    cardGroup.UserID_LastModifiedBy = userId;
                }

                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();

                        LoadGameCardGroupData(GameId);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }                    
                }
            }
        }
        

        void SaveGameCardSequences(int gameCardGroupId)
        {
            var modifiedOn = DateTime.Now;
            var userId = (Page as BasePage).CurrentContext.UserIdentifier;


            using(var dataContext = new EChallengeDataContext())
            {
                var gameCardGroup = dataContext.GameCardGroupSequences
                    .Where(g => g.GameCardGroupSequenceID == gameCardGroupId).FirstOrDefault();                                               

                var localGameCardGroup = GameCardGroupData
                    .Where(g => g.GameCardGroupId == gameCardGroupId).FirstOrDefault();

            
                foreach(var gameCard in gameCardGroup.GameCardSequences)
                {
                    var localCard = localGameCardGroup.Cards
                        .Where(g => g.GameCardId == gameCard.GameCardSequenceID).FirstOrDefault();

                    if(localCard == null)
                    {
                        continue;
                    }

                    gameCard.CardSequenceNo = localCard.SequenceNo;
                    gameCard.LastModifiedOnDateTime = modifiedOn;
                    gameCard.UserID_LastModifiedBy = userId;
                }
                using (var transaction = dataContext.Database.BeginTransaction())
                {
                    try
                    {
                        dataContext.SaveChanges();
                        transaction.Commit();

                        LoadGameCardGroupData(GameId);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                    
                }
            }
        }

        protected void ProductCallbackPanel_Callback(object sender, CallbackEventArgsBase e)
        {
            using(var dataContext = new EChallengeDataContext())
            {
                var lcid = (Page as BasePage).CurrentContext.Culture;
                var language = dataContext.Languages.FirstOrDefault(l => l.LCID == lcid);

                var product = dataContext.Product_Languages
                    .Where(p => p.ProductID == ProductId).FirstOrDefault(p => p.LanguageID == language.LanguageID);

                if(product == null)
                {
                    return;
                }

                ProductNameLiteral.Text = product.ProductName;
            }            
        }

        protected void CancelProductButton_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect(ResolveUrl(@"~/Default.aspx"));
        }

        protected void GameCardGroupGridView_DataBound(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;
            gridView.DetailRows.ExpandAllRows();
        }

        protected void GameCardGridView_Init(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;
            var container = gridView.NamingContainer as GridViewDetailRowTemplateContainer;
            gridView.ClientInstanceName = string.Format("GameCardGridView_{0}", container.VisibleIndex); 
        }

        protected void NewLinkButton_Init(object sender, EventArgs e)
        {
            var hyperLink = sender as ASPxHyperLink;
            var container = hyperLink.NamingContainer as GridViewHeaderTemplateContainer;
            var gridView = container.NamingContainer.NamingContainer as ASPxGridView;
            hyperLink.ClientSideEvents.Click = string.Format("function(s, e) {{ GameCardGridViewAddNewRow(s, e, {0})}} ", gridView.ClientInstanceName);

            var gridContainer = gridView.NamingContainer as GridViewDetailRowTemplateContainer;
            var key = (int)gridContainer.KeyValue;
            var cardGroup = GameCardGroupData.FirstOrDefault(g => g.GameCardGroupId == key);

            hyperLink.ClientVisible = !cardGroup.HasBranchingCard;
        }

        protected void DeleteGameDataCallback_OnCallback(object source, CallbackEventArgs e)
        {
            if(GameId == -1)
            {
                e.Result = "Game could not be determined";
                return;
            }

            using (var dataContext = new EChallengeDataContext())
            {
                var gameExists =
                    dataContext.Games
                        .Any(g => g.GameID == GameId);
                if (gameExists)
                {
                    var userId = (Page as BasePage).CurrentContext.UserIdentifier;


                    using (var transaction = dataContext.Database.BeginTransaction())
                    {
                        try
                        {                            
                            dataContext.upDeleteGameData(GameId, userId);
                            dataContext.SaveChanges();
                            transaction.Commit();

                            e.Result = "Game Data deleted successfully";
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }                        
                    }
                }
                else
                {
                    e.Result = "Game could not be retrieved.";
                }
            }

        }

        protected void GameCardGridView_OnInitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["IsAMandatoryCard"] = true;
        }

        protected void IsAMandatoryCardGroupCheckBox_OnInit(object sender, EventArgs e)
        {
            SetCheckBoxValueInEditTemplate(sender, true);
        }

        protected void IsAMandatoryCardCheckBox_OnInit(object sender, EventArgs e)
        {
            SetCheckBoxValueInEditTemplate(sender, true);
        }

        private static void SetCheckBoxValueInEditTemplate(object sender, bool isChecked)
        {
            var checkBox = (ASPxCheckBox) sender;
            var container = checkBox.NamingContainer as GridViewEditFormTemplateContainer;

            if (container != null && container.Grid.IsNewRowEditing)
            {
                checkBox.Checked = isChecked;
            }
        }
    }
}