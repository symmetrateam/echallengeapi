﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxHtmlEditor;

namespace Diversity.Presentation.Web.UserControls
{
    public partial class BasicHtmlEditorControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HtmlEditor.SettingsDialogFormElements.TableCellPropertiesDialog.ShowAlignmentSection = false;
            HtmlEditor.SettingsDialogFormElements.TableCellPropertiesDialog.ShowAppearanceSection = false;

            HtmlEditor.SettingsDialogFormElements.TableColumnPropertiesDialog.ShowAlignmentSection = false;
            HtmlEditor.SettingsDialogFormElements.TableColumnPropertiesDialog.ShowAppearanceSection= false;

            HtmlEditor.SettingsDialogFormElements.TableRowPropertiesDialog.ShowAlignmentSection = false;
            HtmlEditor.SettingsDialogFormElements.TableRowPropertiesDialog.ShowAppearanceSection = false;

            HtmlEditor.SettingsDialogFormElements.InsertTableDialog.ShowAccessibilitySection = false;
            HtmlEditor.SettingsDialogFormElements.InsertTableDialog.ShowLayoutSection = false;
            HtmlEditor.SettingsDialogFormElements.InsertTableDialog.ShowAppearanceSection = false;
            HtmlEditor.SettingsHtmlEditing.EnterMode = HtmlEditorEnterMode.BR;
        }

        public void PopulateHtml(string html)
        {
            HtmlEditor.Html = html;
        }

        public string GetHtml()
        {
            return HtmlEditor.Html;
        }

        public int Height
        {
            get { return Convert.ToInt32(HtmlEditor.Height.Value); }
            set { HtmlEditor.Height = new Unit(value); }
        }

        public string GetEncodedHtml()
        {
            return System.Web.HttpUtility.HtmlEncode(HtmlEditor.Html);
        }

        public void SetEncodedHtml(string encodedHtml)
        {
            HtmlEditor.Html = System.Web.HttpUtility.HtmlDecode(encodedHtml);
        }
    }
}