﻿<%@ control language="C#" autoeventwireup="true" codebehind="PasswordRecoveryControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.PasswordRecoveryControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<asp:passwordrecovery id="PaswordRecoveryControl" runat="server" usernameinstructiontext="Please enter your "
    usernamefailuretext="Sorry, the email address is not recognised." onsendingmail="ResetPwd_SendingMail"
    generalfailuretext="Your attempt to retrive your password was unsuccessful. <br/>Please contact the site administrator."
    onsendmailerror="PaswordRecoveryControl_SendMailError" onuserlookuperror="PaswordRecoveryControl_UserLookupError"
    onverifyinguser="PaswordRecoveryControl_VerifyingUser">
    <maildefinition bodyfilename="~/MyResources/Emails/ForgotPassword.txt" isbodyhtml="false"
        subject="Diversity - Your Password">
    </maildefinition>
    <usernametemplate>
        <table>
            <tr>
                <td colspan="2">
                    <h5 style="margin-bottom: 0px;">Enter your email address below to receive your password via email.</h5>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:aspxtextbox id="UserName" runat="server" Width="100%">
                        <validationsettings validationgroup="PasswordRecovery1" errordisplaymode="ImageWithTooltip">
                            <requiredfield isrequired="true" errortext="User name is required" />
                        </validationsettings>
                    </dx:aspxtextbox>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:aspxlabel id="FailureText" runat="server" enableviewstate="False" ForeColor="Red" ></dx:aspxlabel>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:aspxbutton id="SubmitButton" runat="server" commandname="Submit" text="Submit"
                        validationgroup="PasswordRecovery1">
                    </dx:aspxbutton>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <div id="loginLinks">
                        <dx:aspxhyperlink id="LoginHyperLink" runat="server" navigateurl="~/Login.aspx" text="Return to sign in page">
                        </dx:aspxhyperlink>
                    </div>
                </td>
            </tr>
        </table>
    </usernametemplate>
    <successtemplate>
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <h5 style="margin-bottom: 0px;">Your password has been emailed to you.</h5>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="loginLinks">
                                    <dx:aspxhyperlink id="LoginHyperLink" runat="server" navigateurl="~/Login.aspx" text="Return to sign in page">
                                    </dx:aspxhyperlink>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </successtemplate>   
</asp:passwordrecovery>
