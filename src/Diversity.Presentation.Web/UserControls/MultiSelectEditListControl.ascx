﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiSelectEditListControl.ascx.cs"
    Inherits="Diversity.Presentation.Web.UserControls.MultiSelectEditListControl" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>








<script id="dxis_MultiEditListControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/MultiSelectEditListControl.js") %>'>
</script>

<dx:ASPxPopupControl ID="MultiSelectUploadPopupControl" runat="server" ClientInstanceName="MultiSelectUploadPopupControl"
    CloseAction="CloseButton" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Upload File" AllowDragging="true" EnableAnimation="false" EnableViewState="false">
    <ClientSideEvents Closing="MultiSelectUploadPopupControl_Closing" PopUp="function(s){s.UpdatePosition();}" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
            <dx:ASPxPanel ID="UploadPanel" runat="server" DefaultButton="UploadButton">
                <PanelCollection>
                    <dx:PanelContent>
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="SelectFileLabel" runat="server" Text="Select File">
                                    </dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxUploadControl ID="MultiSelectUploadControl" runat="server" ClientInstanceName="MultiSelectUploadControl"
                                        EnableViewState="false" ShowProgressPanel="true" OnFileUploadComplete="UploadControl_OnFileUploadComplete"
                                        UploadMode="Standard">
                                        <ValidationSettings MaxFileSize="4194304" MaxFileSizeErrorText="Maximum file size allowed is 4Mb">
                                        </ValidationSettings>
                                        <ClientSideEvents FileUploadComplete="MultiSelectUploadControl_FileUploadComplete" TextChanged="MultiSelectUploadControl_TextChanged" />
                                    </dx:ASPxUploadControl>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <dx:ASPxButton ID="MultiSelectUploadButton" runat="server" ClientInstanceName="MultiSelectUploadButton"
                                        Text="Upload" AutoPostBack="false" ClientEnabled="false">
                                        <ClientSideEvents Click="MultiSelectUploadButton_Click" />
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

<dx:ASPxGridView ID="MultiEditListGridView" runat="server" ClientInstanceName="MultiEditListGridView"
    DataSourceID="MultiEditItemListDataSource" AutoGenerateColumns="False" KeyFieldName="ClientId"
    Width="100%" OnRowInserting="MultiEditListGridView_RowInserting" OnCustomCallback="MultiEditListGridView_CustomCallback"
    OnSelectionChanged="MultiEditListGridView_SelectionChanged"
    OnCustomButtonCallback="MultiEditListGridView_CustomButtonCallback"
    OnCustomButtonInitialize="MultiEditListGridView_CustomButtonInitialize"
    OnDataBound="MultiEditListGridView_DataBound" OnCustomUnboundColumnData="MultiEditListGridView_OnCustomUnboundColumnData">
    <ClientSideEvents EndCallback="MultiEditListGridView_EndCallback" />
    <Styles AlternatingRow-Enabled="True">
        <AlternatingRow Enabled="True">
        </AlternatingRow>
    </Styles>
    <SettingsPager Mode="ShowAllRecords">
    </SettingsPager>
    <SettingsBehavior ProcessSelectionChangedOnServer="true" />
    <Columns>
        <dx:GridViewCommandColumn Name="SelectColumn" VisibleIndex="0" ShowSelectCheckbox="true" Width="30px">
        </dx:GridViewCommandColumn>
        <dx:GridViewCommandColumn ButtonType="Image" VisibleIndex="1" Caption=" " Width="30px">
            <CustomButtons>
                <dx:GridViewCommandColumnCustomButton ID="UpCustomButton" Text="Up" Visibility="AllDataRows">
                    <Image Url="~/App_Themes/Icons/ArrowUp.png" Height="12" Width="12" ToolTip="Move Up" />
                </dx:GridViewCommandColumnCustomButton>
                <dx:GridViewCommandColumnCustomButton ID="DownCustomButton" Text="Down" Visibility="AllDataRows">
                    <Image Url="~/App_Themes/Icons/ArrowDown.png" Height="12" Width="12" ToolTip="Move Down" />
                </dx:GridViewCommandColumnCustomButton>
            </CustomButtons>
        </dx:GridViewCommandColumn>
        <dx:GridViewCommandColumn VisibleIndex="1" Width="50px" ShowDeleteButton="true">
            <HeaderCaptionTemplate>
                <asp:LinkButton ID="NewLinkButton" runat="server" OnClientClick="AddNewRow();return false;" Text="New"/>
            </HeaderCaptionTemplate>
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn FieldName="Id" ReadOnly="true" Visible="false" VisibleIndex="2">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Text" Caption="Short Version" VisibleIndex="3">
            <DataItemTemplate>
                <dx:ASPxMemo ID="ShortTextMemo" runat="server" ClientInstanceName="ShortTextMemo"
                    Rows="2" Text='<%# Bind("Text") %>' OnInit="ShortTextMemo_Init" AutoResizeWithContainer="True" Width="100%">
                </dx:ASPxMemo>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="ExtendedText" Caption="Long Version" VisibleIndex="4">
            <DataItemTemplate>
                <dx:ASPxMemo ID="LongTextMemo" runat="server" ClientInstanceName="LongTextMemo"
                    Rows="2" Text='<%#Bind("ExtendedText") %>' OnInit="LongTextMemo_Init" AutoResizeWithContainer="True" Width="100%">
                </dx:ASPxMemo>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>        
        <dx:GridViewDataColumn FieldName="Multimedia" Caption="Multimedia" VisibleIndex="5" UnboundType="Object">
            <DataItemTemplate>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="MediaBrowseButton" ClientInstanceName="MediaBrowseButton"
                                    runat="server" Text="Browse.." AutoPostBack="false" OnInit="MediaBrowseButton_OnInit" >
                                </dx:ASPxButton>
                            <dx:ASPxHyperLink ID="MultimediaHyperLink" runat="server" ClientInstanceName="MultimediaHyperLink"
                                Target="_blank" Text="" NavigateUrl="#" OnInit="MultimediaHyperLink_OnInit">
                            </dx:ASPxHyperLink>
                        </td>
                        <td>
                            <dx:ASPxHyperLink ID="RemoveMultimediaHyperLink" runat="server" ClientInstanceName="RemoveMultimediaHyperLink"
                                Target="_blank" Text="Remove" ClientVisible="False" NavigateUrl="#" OnInit="RemoveMultimediaHyperLink_OnInit">
                            </dx:ASPxHyperLink>
                        </td>
                        <td>
                            <dx:ASPxMemo ID="MultimediaUrlMemo" runat="server" ClientInstanceName="MultimediaUrlMemo" Width="100%"
                                Rows="2" Text='<%#Bind("MultimediaUrl") %>' ClientVisible="False" OnInit="MultimediaUrlMemo_OnInit">
                            </dx:ASPxMemo>
                            <dx:ASPxMemo ID="MultimediaUrlFriendlyNameMemo" runat="server" ClientInstanceName="MultimediaUrlFriendlyNameMemo" Width="100%"
                                Rows="2" Text='<%#Bind("MultimediaUrlFriendlyName") %>' ClientVisible="False" OnInit="MultimediaUrlFriendlyNameMemo_OnInit" >
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
        </dx:GridViewDataColumn>
        <dx:GridViewDataTextColumn FieldName="MultimediaUrl" Visible="False" VisibleIndex="6">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="MultimediaUrlFriendlyName" Visible="False" VisibleIndex="7">
        </dx:GridViewDataTextColumn>        
    </Columns>
</dx:ASPxGridView>
<asp:LinqDataSource ID="MultiEditItemListDataSource" runat="server" OnSelecting="MultiEditItemListDataSource_Selecting">
</asp:LinqDataSource>
<dx:ASPxCallback ID="GridCallback" runat="server" OnCallback="GridCallback_Callback"
    ClientInstanceName="GridCallback">
</dx:ASPxCallback>
