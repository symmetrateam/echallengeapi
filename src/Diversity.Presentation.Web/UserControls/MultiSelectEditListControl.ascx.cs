﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using Diversity.Common;

namespace Diversity.Presentation.Web.UserControls
{
    public partial class MultiSelectEditListControl : System.Web.UI.UserControl
    {
        private const string CallbackArgumentFormat = "function (s, e) {{ GridCallback.PerformCallback(\"{0}|{1}|\" + {2}); }}"; // key | field | value

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Items = new List<SelectEditListItem>();
            }

            base.OnInit(e);
        }

        string SessionItemsKey
        {
            get { return string.Format("{0}Items", ClientID); }
        }

        [DefaultValue(false)]
        public virtual bool AllowSingleRowSelectOnly
        {
            get { return MultiEditListGridView.SettingsBehavior.AllowSelectSingleRowOnly; }
            set { MultiEditListGridView.SettingsBehavior.AllowSelectSingleRowOnly = value; }
        }

        [DefaultValue(true)]
        public bool ShowSelectColumn
        {
            get { return MultiEditListGridView.Columns[0].Visible; }
            set { MultiEditListGridView.Columns[0].Visible = value; }
        }

        [DefaultValue(false)]
        public bool ShowMultimediaColumn
        {
            get { return MultiEditListGridView.Columns["Multimedia"].Visible; }
            set { MultiEditListGridView.Columns["Multimedia"].Visible = value; }
        }

        public bool ShowTextColumns
        {
            get { return MultiEditListGridView.Columns["Text"].Visible; }
            set
            {
                MultiEditListGridView.Columns["Text"].Visible = value;
                MultiEditListGridView.Columns["ExtendedText"].Visible = value;
            }
        }

        [DefaultValue("")]
        public string MultimediaColumnCaption
        {
            get { return MultiEditListGridView.Columns["Multimedia"].Caption; }
            set { MultiEditListGridView.Columns["Multimedia"].Caption = value; }
        }

        [DefaultValue("")]
        public string UploadControlFileFormats
        {
            get { return string.Join(",", MultiSelectUploadControl.ValidationSettings.AllowedFileExtensions); }
            set { MultiSelectUploadControl.ValidationSettings.AllowedFileExtensions = value.Split(','); }
        }

        [DefaultValue("")]
        public string LongVersionColumnCaption
        {
            get { return MultiEditListGridView.Columns["ExtendedText"].Caption; }
            set { MultiEditListGridView.Columns["ExtendedText"].Caption = value; }
        }

        [DefaultValue("")]
        public string ShortVersionColumnCaption
        {
            get { return MultiEditListGridView.Columns["Text"].Caption; }
            set { MultiEditListGridView.Columns["Text"].Caption = value; }
        }

        [DefaultValue(false)]
        public bool ShowTitlePanel
        {
            get { return MultiEditListGridView.Settings.ShowTitlePanel; }
            set { MultiEditListGridView.Settings.ShowTitlePanel = value; }
        }

        [DefaultValue("")]
        public string TitleText
        {
            get { return MultiEditListGridView.SettingsText.Title; }
            set { MultiEditListGridView.SettingsText.Title = value; }
        }

        public List<SelectEditListItem> Items
        {
            get
            {
                if (Session[SessionItemsKey] == null)
                {
                    Session[SessionItemsKey] = new List<SelectEditListItem>();
                }

                return Session[SessionItemsKey] as List<SelectEditListItem>;
            }
            set
            {
                Session[SessionItemsKey] = value;
            }
        }

        public void DataBind()
        {
            MultiEditListGridView.DataBind();
        }

        protected void MultiEditItemListDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = Items;
        }

        protected void MultiEditListGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            var longText = e.NewValues["ExtendedText"] as string;
            var shortText = e.NewValues["Text"] as string;

            var editListItem = new SelectEditListItem()
                                   {
                                       IsSelected = false,
                                       ExtendedText = longText,
                                       Text = shortText
                                   };

            Items.Add(editListItem);

            e.Cancel = true;
            gridView.CancelEdit();
            gridView.DataBind();
        }

        protected void MultiEditListGridView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            if (string.Compare(e.Parameters, "NEW") == 0)
            {
                Items.Add(new SelectEditListItem());
            }

            gridView.DataBind();
        }

        protected void ShortTextMemo_Init(object sender, EventArgs e)
        {
            var memo = sender as ASPxMemo;
            var container = memo.NamingContainer as GridViewDataItemTemplateContainer;
            memo.ClientInstanceName = string.Format(@"ShortTextMemo_{0}", container.VisibleIndex);
            memo.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            memo.ClientSideEvents.ValueChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               container.Column.FieldName,
                                                               "s.GetText()"
                                                               );
        }

        protected void LongTextMemo_Init(object sender, EventArgs e)
        {
            var memo = sender as ASPxMemo;
            var container = memo.NamingContainer as GridViewDataItemTemplateContainer;
            memo.ClientInstanceName = string.Format(@"LongTextMemo_{0}", container.VisibleIndex);
            memo.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            memo.ClientSideEvents.ValueChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               container.Column.FieldName,
                                                               "s.GetText()"
                                                               );

        }

        protected void GridCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            var p = e.Parameter.Split('|');

            var key = p[0];
            var field = p[1];
            var value = p[2];

            var item = Items.Find(c => c.ClientId == key);

            switch (field)
            {
                case "Text":
                    if (value == "null")
                    {
                        value = String.Empty;
                    }
                    item.Text = value;
                    break;
                case "ExtendedText":
                    if (value == "null")
                    {
                        value = String.Empty;
                    }
                    item.ExtendedText = value;
                    break;
                case "MultimediaUrl":
                    if (ShowMultimediaColumn)
                    {
                        if (value == "null")
                        {
                            value = String.Empty;
                        }
                        item.MultimediaUrl = value;
                        item.IsMultimediaLive = false;
                    }
                    break;
                case "MultimediaUrlFriendlyName":
                    if (ShowMultimediaColumn)
                    {
                        if (value == "null")
                        {
                            value = String.Empty;
                        }
                        item.MultimediaUrlFriendlyName = value;
                        item.IsMultimediaLive = false;
                    }
                    break;

            }
        }

        protected void MultiEditListGridView_SelectionChanged(object sender, EventArgs e)
        {
            //this is required here to ensure that
            //when selecting items on databound of page load
            //the selection is not undone by this event
            if (!Page.IsPostBack)
            {
                return;
            }

            var gridView = sender as ASPxGridView;

            foreach (var item in Items)
            {
                item.IsSelected = gridView.Selection.IsRowSelectedByKey(item.ClientId);

                if (ShowMultimediaColumn)
                {
                    SetUnboundColumnValues(gridView, item, gridView.FindVisibleIndexByKeyValue(item.ClientId));
                }
            }
        }

        protected void MultiEditListGridView_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (string.Compare(e.ButtonID, "UpCustomButton") == 0)
            {
                MoveSelection(-1, e.VisibleIndex);
            }

            if (string.Compare(e.ButtonID, "DownCustomButton") == 0)
            {
                MoveSelection(1, e.VisibleIndex);
            }
        }

        void MoveSelection(int offset, int visibleIndex)
        {
            if (!CanMoveSelection(offset, visibleIndex))
            {
                return;
            }

            var count = Items.Count;
            var temp = new SelectEditListItem[count];

            temp[visibleIndex + offset] = Items[visibleIndex];


            var index = 0;
            for (int i = 0; i < count; i++)
            {
                if (i == visibleIndex)
                {
                    continue;
                }

                while (temp[index] != null)
                {
                    index++;
                }

                temp[index] = Items[i];
                index++;
            }

            Items.Clear();
            Items.AddRange(temp);
            MultiEditListGridView.DataBind();
        }

        bool CanMoveSelection(int offset, int visibleIndex)
        {
            return visibleIndex + offset >= 0 && visibleIndex + offset <= MultiEditListGridView.VisibleRowCount - 1;
        }

        protected void MultiEditListGridView_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            if (e.CellType == GridViewTableCommandCellType.Data)
            {
                if (e.VisibleIndex == 0)
                {
                    if (gridView.VisibleRowCount == 1)
                    {
                        if (string.Compare(e.ButtonID, "UpCustomButton") == 0 ||
                            string.Compare(e.ButtonID, "DownCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                    else
                    {
                        if (string.Compare(e.ButtonID, "UpCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                }
                else
                {
                    if (e.VisibleIndex == gridView.VisibleRowCount - 1)
                    {
                        if (string.Compare(e.ButtonID, "DownCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                }
            }
        }

        protected void MultiEditListGridView_DataBound(object sender, EventArgs e)
        {
            var gridView = sender as ASPxGridView;

            foreach (var item in Items)
            {
                if (item.IsSelected)
                {
                    gridView.Selection.SelectRowByKey(item.ClientId);
                }
            }
        }

        protected void UploadControl_OnFileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            var splitOn = new[] { "." };
            var parts = e.UploadedFile.FileName.Split(splitOn, StringSplitOptions.RemoveEmptyEntries);
            var extension = parts.Length > 1 ? string.Format(".{0}", parts[parts.Length - 1]) : string.Empty;
            var filename = string.Format("{0}{1}", Guid.NewGuid(), extension);
            var fullPath = Path.Combine(MapPath(ConfigurationHelper.TemporaryUploadLocation), filename);
            e.UploadedFile.SaveAs(fullPath, false);
            var url = ResolveUrl(string.Format("{0}/{1}", ConfigurationHelper.TemporaryUploadLocation, filename));

            e.CallbackData = string.Format("{0};{1}", url, e.UploadedFile.FileName);
        }

        protected void MediaBrowseButton_OnInit(object sender, EventArgs e)
        {
            var button = sender as ASPxButton;
            var container = button.NamingContainer as GridViewDataItemTemplateContainer;
            button.ClientInstanceName = string.Format(@"MediaBrowseButton_{0}", container.VisibleIndex);
            button.JSProperties["cp_InstanceName"] = string.Format(@"MediaBrowseButton_{0}", container.VisibleIndex);
            button.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            button.JSProperties["cp_RowKey"] = container.KeyValue;

            button.ClientSideEvents.Click = "function (s, e) {MediaBrowseButton_Click(s,e);}";
        }

        protected void MultimediaHyperLink_OnInit(object sender, EventArgs e)
        {
            var link = sender as ASPxHyperLink;
            var container = link.NamingContainer as GridViewDataItemTemplateContainer;
            link.ClientInstanceName = string.Format(@"MultimediaHyperLink_{0}", container.VisibleIndex);
            link.JSProperties["cp_InstanceName"] = string.Format(@"MultimediaHyperLink_{0}", container.VisibleIndex);
            link.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
        }

        protected void RemoveMultimediaHyperLink_OnInit(object sender, EventArgs e)
        {
            var link = sender as ASPxHyperLink;
            var container = link.NamingContainer as GridViewDataItemTemplateContainer;
            link.ClientInstanceName = string.Format(@"RemoveMultimediaHyperLink_{0}", container.VisibleIndex);
            link.JSProperties["cp_InstanceName"] = string.Format(@"RemoveMultimediaHyperLink_{0}", container.VisibleIndex);
            link.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;
            link.JSProperties["cp_RowKey"] = container.KeyValue;

            link.ClientSideEvents.Click = "function (s, e) {RemoveMediaHyperLink_Click(s,e)}";
        }

        protected void MultimediaUrlMemo_OnInit(object sender, EventArgs e)
        {
            var memo = sender as ASPxMemo;
            var container = memo.NamingContainer as GridViewDataItemTemplateContainer;
            memo.ClientInstanceName = string.Format(@"MultimediaUrl_{0}", container.VisibleIndex);
            memo.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            memo.ClientSideEvents.ValueChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               "MultimediaUrl",
                                                               "s.GetText()"
                                                               );
        }

        protected void MultimediaUrlFriendlyNameMemo_OnInit(object sender, EventArgs e)
        {
            var memo = sender as ASPxMemo;
            var container = memo.NamingContainer as GridViewDataItemTemplateContainer;
            memo.ClientInstanceName = string.Format(@"MultimediaUrlFriendlyName_{0}", container.VisibleIndex);
            memo.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            memo.ClientSideEvents.ValueChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               "MultimediaUrlFriendlyName",
                                                               "s.GetText()"
                                                               );
        }

        protected void MultiEditListGridView_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (!ShowMultimediaColumn)
            {
                return;
            }

            var gridView = sender as ASPxGridView;
            var item = gridView.GetRow(e.ListSourceRowIndex) as SelectEditListItem;

            SetUnboundColumnValues(gridView, item, e.ListSourceRowIndex);
        }

        private void SetUnboundColumnValues(ASPxGridView gridView, SelectEditListItem item, int rowIndex)
        {
            if (!ShowMultimediaColumn)
            {
                return;
            }

            var column = gridView.Columns["Multimedia"] as GridViewDataColumn;

            var multimediaHyperlink =
                gridView.FindRowCellTemplateControl(rowIndex, column, "MultimediaHyperLink") as ASPxHyperLink;
            var removeHyperlink =
                gridView.FindRowCellTemplateControl(rowIndex, column, "RemoveMultimediaHyperLink") as ASPxHyperLink;
            var browseButton =
                gridView.FindRowCellTemplateControl(rowIndex, column, "MediaBrowseButton") as ASPxButton;

            if (string.IsNullOrEmpty(item.MultimediaUrl) || string.IsNullOrWhiteSpace(item.MultimediaUrl))
            {
                multimediaHyperlink.ClientVisible = false;
                multimediaHyperlink.NavigateUrl = "#";
                multimediaHyperlink.Text = string.Empty;

                removeHyperlink.ClientVisible = false;
                browseButton.ClientVisible = true;

                return;
            }

            var location = ConfigurationHelper.TemporaryUploadLocation;
            if (item.IsMultimediaLive)
            {
                location = ConfigurationHelper.LiveUploadLocation;
            }

            multimediaHyperlink.ClientVisible = true;
            multimediaHyperlink.NavigateUrl = ResolveUrl(string.Format(@"{0}/{1}", location, item.MultimediaUrl));
            multimediaHyperlink.Text = item.MultimediaUrlFriendlyName;

            removeHyperlink.ClientVisible = true;
            browseButton.ClientVisible = false;
        }
    }
}