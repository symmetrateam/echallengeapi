﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diversity.Presentation.Web.UserControls
{
    public class SelectEditListItem : EditListItem
    {       
        public bool IsSelected { get; set; }
        public string ExtendedText { get; set; }

        public string MultimediaUrl { get; set; }
        public string MultimediaUrlFriendlyName { get; set; }
        public bool IsMultimediaLive { get; set; }
    }
}