﻿<%@ control language="C#" autoeventwireup="true" codebehind="EditListControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.EditListControl" %>
<%@ import namespace="Diversity.Presentation.Web.UserControls" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>

    
    
    <script id="dxis_EditListControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/EditListControl.js") %>'>
</script>
<dx:aspxgridview id="EditListGridView" runat="server" clientinstancename="EditListGridView"
    datasourceid="EditListItemDataSource" autogeneratecolumns="false" keyfieldname="ClientId"
    width="100%" oncustomcallback="EditListGridView_CustomCallback"
    oncustombuttoncallback="EditListGridView_CustomButtonCallback" 
    oncustombuttoninitialize="EditListGridView_CustomButtonInitialize">
    <clientsideevents endcallback="EditListGridView_EndCallback" />
    <styles alternatingrow-enabled="True">
        <alternatingrow enabled="True">
        </alternatingrow>
    </styles>
    <settingspager mode="ShowAllRecords">
    </settingspager>
    <settingsbehavior/>
    <columns>
        <dx:gridviewcommandcolumn buttontype="Image" visibleindex="0" caption=" " width="30px">
            <custombuttons>
                <dx:gridviewcommandcolumncustombutton id="UpCustomButton" text="Up" visibility="AllDataRows">
                    <image url="~/App_Themes/Icons/ArrowUp.png" height="12" width="12" tooltip="Move Up" />
                </dx:gridviewcommandcolumncustombutton>                
                <dx:gridviewcommandcolumncustombutton id="DownCustomButton" text="Down" visibility="AllDataRows">
                    <image url="~/App_Themes/Icons/ArrowDown.png" height="12" width="12" tooltip="Move Down" />
                </dx:gridviewcommandcolumncustombutton>
            </custombuttons>
        </dx:gridviewcommandcolumn>
        <dx:gridviewcommandcolumn visibleindex="1" width="50px">
            <headercaptiontemplate>
                <asp:linkbutton id="NewLinkButton" runat="server" onclientclick="AddNewRow();return false;"
                    text="New" />
            </headercaptiontemplate>
            <CustomButtons>
                <dx:GridViewCommandColumnCustomButton ID="DeleteButton" Text="Delete">
                </dx:GridViewCommandColumnCustomButton>
            </CustomButtons>            
        </dx:gridviewcommandcolumn>
        <dx:gridviewdatatextcolumn fieldname="Id" readonly="true" visible="false" visibleindex="2">
        </dx:gridviewdatatextcolumn>
        <dx:gridviewdatatextcolumn fieldname="Text" caption="Text" visibleindex="3">
            <dataitemtemplate>
                <dx:aspxmemo id="TextMemo" runat="server" clientinstancename="TextMemo"
                    rows="2" text='<%# Bind("Text") %>' oninit="TextMemo_Init" AutoResizeWithContainer="True" Width="100%">
                </dx:aspxmemo>
            </dataitemtemplate>
        </dx:gridviewdatatextcolumn>

    </columns>
</dx:aspxgridview>
<asp:linqdatasource id="EditListItemDataSource" runat="server" onselecting="EditListItemDataSource_Selecting">
</asp:linqdatasource>
<dx:aspxcallback id="GridCallback" runat="server" oncallback="GridCallback_Callback"
    clientinstancename="GridCallback">
</dx:aspxcallback>

