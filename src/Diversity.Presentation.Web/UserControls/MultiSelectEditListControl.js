﻿var _callbackCommand;
function AddNewRow() {
    _callbackCommand = "NEW";
    MultiEditListGridView.PerformCallback(_callbackCommand);
}

function MultiEditListGridView_EndCallback(s, e) {
    if (_callbackCommand == "NEW") {
        _callbackCommand = "";
    }
}

var _buttonName = "";
var _hyperLinkName = "";
var _hyperLinkRemove = "";
var _currentRowIndex = "";
var _rowKey = "";

function MediaBrowseButton_Click(s, e) {
    _rowKey = s.cp_RowKey;
    _buttonName = s.cp_InstanceName;
    _hyperLinkName = _buttonName.replace("MediaBrowseButton_", "MultimediaHyperLink_");
    _hyperLinkRemove = _buttonName.replace("MediaBrowseButton_", "RemoveMultimediaHyperLink_");

    MultiSelectUploadPopupControl.Show();
}

function RemoveMediaHyperLink_Click(s,e) {
    _removeLinkName = s.cp_InstanceName;
    _rowKey = s.cp_RowKey;
    
    _hyperLinkName = _removeLinkName.replace("RemoveMultimediaHyperLink_", "MultimediaHyperLink_");
    var browseButtonName = _removeLinkName.replace("RemoveMultimediaHyperLink_", "MediaBrowseButton_");
    

    s.SetVisible(false);
    var browseButton = GetControl(browseButtonName);
    var mediaHyperLink = GetControl(_hyperLinkName);
    
    mediaHyperLink.SetNavigateUrl("#");
    mediaHyperLink.SetText("");
    mediaHyperLink.SetVisible(false);    
    browseButton.SetVisible(true);
    

    var mediaMemo = GetMediaMemoFromBrowseButtonName(browseButtonName);
    var mediaFriendlyMemo = GetMediaFriendlyMemoFromBrowseButtonName(browseButtonName);

    mediaMemo.SetText("");
    GridCallback.PerformCallback(_rowKey + "|MultimediaUrl| "); // key | field | value

    mediaFriendlyMemo.SetText("");
    GridCallback.PerformCallback(_rowKey + "|MultimediaUrlFriendlyName| "); // key | field | value

    ClearParams();
    ASPxClientUtils.PreventEvent(e.htmlEvent);
}

function MultiSelectUploadButton_Click(s, e) {
    MultiSelectUploadControl.Upload();
}

function MultiSelectUploadPopupControl_Closing(s, e) {
    _uploadTYpe = "";
    MultiSelectUploadControl.ClearText();
    MultiSelectUploadButton.SetEnabled(MultiSelectUploadControl.GetText(0) != "");
}

function MultiSelectUploadControl_TextChanged(s, e) {
    MultiSelectUploadButton.SetEnabled(MultiSelectUploadControl.GetText(0) != "");
}

function MultiSelectUploadControl_FileUploadComplete(s, e) {
    var url = e.callbackData.split(";");

    if (url != 'undefined' || url.length > 0) {

        var browseButton = GetControl(_buttonName);
        var mediaHyperLink = GetControl(_hyperLinkName);
        var removeMediHyperLink = GetControl(_hyperLinkRemove);

        mediaHyperLink.SetNavigateUrl(url[0]);
        mediaHyperLink.SetText(url[1]);
        mediaHyperLink.SetVisible(true);
        
        browseButton.SetVisible(false);
        mediaHyperLink.SetVisible(true);
        removeMediHyperLink.SetVisible(true);

        MultiSelectUploadPopupControl.Hide();

        var mediaMemo = GetMediaMemoFromBrowseButtonName(_buttonName);
        var mediaFriendlyMemo = GetMediaFriendlyMemoFromBrowseButtonName(_buttonName);
        
        var fileName =  url[0].substring(url[0].lastIndexOf('/') + 1);
        mediaMemo.SetText(fileName);
        GridCallback.PerformCallback(_rowKey + "|MultimediaUrl|" + fileName); // key | field | value
        
        mediaFriendlyMemo.SetText(url[1]);
        GridCallback.PerformCallback(_rowKey + "|MultimediaUrlFriendlyName|" + url[1]); // key | field | value

        ClearParams();
    }
}


function GetControl(controlName) {
    return eval(controlName);
}

function GetMediaMemoFromBrowseButtonName(buttonName) {
    var mediaUrl = buttonName.replace("MediaBrowseButton_", "MultimediaUrl_");
    return GetControl(mediaUrl);
}

function GetMediaFriendlyMemoFromBrowseButtonName(buttonName) {
    var mediaUrl = buttonName.replace("MediaBrowseButton_", "MultimediaUrlFriendlyName_");
    return GetControl(mediaUrl);
}

function ClearParams() {
    _buttonName = "";
    _hyperLinkName = "";
    _hyperLinkRemove = "";
    _currentRowIndex = "";
    _rowKey = "";
}