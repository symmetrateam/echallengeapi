﻿<%@ control language="C#" autoeventwireup="true" codebehind="LogFileViewerControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.LogFileViewerControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<table width="100%">
    <tr>
        <td>
            <dx:aspxcombobox id="LogFileComboBox" runat="server" clientinstancename="LogFileComboBox">
                <validationsettings errordisplaymode="ImageWithTooltip">
                    <requiredfield isrequired="true" errortext="Log file is a required field" />
                </validationsettings>
            </dx:aspxcombobox>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxbutton id="LoadButton" runat="server" text="Load" onclick="LoadButton_Click">
            </dx:aspxbutton>
        </td>
    </tr>
    <tr>
        <td>
            <dx:aspxlabel id="ErrorLabel" runat="server" text="">
            </dx:aspxlabel>
        </td>
    </tr>
</table>
<dx:aspxmemo id="LogMemo" runat="server" rows="100" width="100%">
</dx:aspxmemo>
