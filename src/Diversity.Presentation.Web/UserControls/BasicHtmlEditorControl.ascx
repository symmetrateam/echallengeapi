﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasicHtmlEditorControl.ascx.cs" Inherits="Diversity.Presentation.Web.UserControls.BasicHtmlEditorControl" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<script id="dxis_BasicHtmlEditorControl" language="javascript" type="text/javascript"
    src='<%=ResolveUrl("~/UserControls/BasicHtmlEditorControl.js") %>'>
</script>

<dx:ASPxHtmlEditor ID="HtmlEditor" ClientInstanceName="HtmlEditor" runat="server" Width="100%" Height="200">
    <Settings AllowInsertDirectImageUrls="False" />
    <SettingsHtmlEditing UpdateDeprecatedElements="False" AllowStyleAttributes="False"></SettingsHtmlEditing>
    <ClientSideEvents HtmlChanged="OnHtmlChanged" CommandExecuted="OnCommandExecuted" />
    <CssFiles>
        <dx:HtmlEditorCssFile FilePath="~/UserControls/BasicHtmlEditorControl.css"/>
    </CssFiles>
    <Toolbars>      
        <dx:HtmlEditorToolbar>
            <Items>
                <dx:ToolbarCustomCssEdit>
                    <Items>
                        <dx:ToolbarCustomCssListEditItem TagName="p" Text="Normal" CssClass="" />
                        <dx:ToolbarCustomCssListEditItem TagName="p" Text="Header 1" CssClass="Header1" />
                        <dx:ToolbarCustomCssListEditItem TagName="p" Text="Header 2" CssClass="Header2" />
                        <dx:ToolbarCustomCssListEditItem TagName="p" Text="Header 3" CssClass="Header3" />
                        <dx:ToolbarCustomCssListEditItem TagName="p" Text="Header 4" CssClass="Header4" />
                        <dx:ToolbarCustomCssListEditItem TagName="p" Text="Header 5" CssClass="Header5" />
                        <dx:ToolbarCustomCssListEditItem TagName="p" Text="Header 6" CssClass="Header6" />
                    </Items>
                </dx:ToolbarCustomCssEdit>
                <dx:ToolbarFontSizeEdit Visible="False" /> 
                <dx:ToolbarUndoButton />                
                <dx:ToolbarRedoButton />                
                <dx:ToolbarBoldButton BeginGroup="true" />                
                <dx:ToolbarItalicButton />                
                <dx:ToolbarUnderlineButton />                                     
                <dx:ToolbarInsertOrderedListButton/>
                <dx:ToolbarInsertUnorderedListButton/>
                <dx:ToolbarInsertTableDialogButton/>               
            </Items>
        </dx:HtmlEditorToolbar>
    </Toolbars>
</dx:ASPxHtmlEditor>

