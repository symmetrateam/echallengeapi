﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using log4net;
using log4net.Appender;

namespace Diversity.Presentation.Web.UserControls
{
    public partial class LogFileViewerControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if(!Page.IsPostBack)
            {
                PopulateLogFileComboBox();
            }
        }

        void PopulateLogFileComboBox()
        {
            var items = GetFileAppenders();

            foreach (var listEditItem in items)
            {
                LogFileComboBox.Items.Add(listEditItem);
            }
        }

        public static IAppender[] GetAllAppenders()
        {
            var appenders = new ArrayList();
            var h =(log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository();
            appenders.AddRange(h.Root.Appenders);
            return (IAppender[])appenders.ToArray(typeof(IAppender));
        } 

        public List<ListEditItem> GetFileAppenders()
        {
            var items = new List<ListEditItem>();

            var appenderList = GetAllAppenders();
            FileAppender fileAppender = null;
            foreach (IAppender appender in appenderList)
            {
                if (appender.GetType().FullName == typeof(RollingFileAppender).FullName)
                {
                    fileAppender = (FileAppender)appender;
                    
                    items.Add(new ListEditItem(fileAppender.Name, fileAppender.File));

                }
            }

            return items;
        }

        private void LoadFile(string path)
        {
            ErrorLabel.Text = string.Empty;


            if(!File.Exists(path))
            {
                ErrorLabel.Text = string.Format(@"Path {0} does not exist", path);
                return;
            }

            try
            {
                using (var fileStream = new FileStream(
                    path,
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.ReadWrite))
                {
                    using (var streamReader = new StreamReader(fileStream))
                    {
                        LogMemo.Text = streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLabel.Text = string.Format("Error loading log file: {0}", ex.Message);
            }
        }

        protected void LoadButton_Click(object sender, EventArgs e)
        {
            if(LogFileComboBox.SelectedItem == null)
            {
                return;                
            }

            var path = LogFileComboBox.SelectedItem.Value as string;

            LoadFile(path);
        }

    }
}