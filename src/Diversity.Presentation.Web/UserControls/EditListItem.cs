﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diversity.Presentation.Web.UserControls
{
    public class EditListItem
    {
        public string ClientId { get; private set; }
        public string Id { get; set; }        
        public string Text { get; set; }

        public EditListItem()
        {
            ClientId = Guid.NewGuid().ToString();
        }


    }
}