﻿<%@ control language="C#" autoeventwireup="true" codebehind="LoginUserControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.LoginUserControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<table>
    <tr>
        <td>
            <asp:login id="LoginControl" runat="server" onauthenticate="LoginControl_Authenticate"
                onloggedin="LoginControl_LoggedIn" destinationpageurl="~/Admin/Default.aspx">
                <layouttemplate>
                    <asp:panel id="LoginFormPanel" runat="server" defaultbutton="LoginButton">
                        <table>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="UserNameLabel" runat="server" text="Email" ForeColor="#ffffff">
                                    </dx:aspxlabel>
                                </td>
                                <td>
                                    &nbsp;&nbsp;
                                </td>
                                <td>
                                    <dx:aspxtextbox id="UserName" runat="server">
                                        <validationsettings validationgroup="LoginControl" errordisplaymode="ImageWithTooltip">
                                            <requiredfield isrequired="true" errortext="User Name is required" />
                                            <regularexpression validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" errortext="Invalid email address"/>
                                        </validationsettings>
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="PasswordLabel" runat="server" text="Password" ForeColor="#ffffff">
                                    </dx:aspxlabel>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <dx:aspxtextbox id="Password" runat="server" password="true" textmode="password" viewstatemode="Disabled">
                                        <validationsettings validationgroup="LoginControl" errordisplaymode="ImageWithTooltip">
                                            <requiredfield isrequired="true" errortext="Password is required" />
                                        </validationsettings>
                                    </dx:aspxtextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:aspxlabel id="LanguageLabel" runat="server" text="Language" ForeColor="#ffffff"/>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <dx:aspxcombobox id="LanguageDropDownList" runat="server" datasourceid="LanguageDataSource"
                                        textfield="Language1" valuefield="LCID" valuetype="System.String" dropdownstyle="DropDownList"
                                        ondatabound="LanguageDropDownList_DataBound">
                                    </dx:aspxcombobox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="errortext">
                                    <dx:aspxlabel id="FailureText" runat="server" ForeColor="Red" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td>
                                    <dx:aspxbutton id="LoginButton" runat="server" commandname="Login" text="Sign In" causesvalidation="true"
                                        validationgroup="LoginControl" />
                                </td>
                            </tr>
                        </table>
                        <asp:validationsummary id="LoginControlValidationSummary" runat="server" validationgroup="LoginControl"
                            showmessagebox="false" showsummary="false" />
                    </asp:panel>
                </layouttemplate>
            </asp:login>
        </td>
    </tr>
    <tr>
        <td>
            <div id="loginLinks">
                <dx:aspxhyperlink id="ForgotPasswordHyperLink" runat="server" navigateurl="~/ForgotPassword.aspx"
                    text="Forgot your password?">
                </dx:aspxhyperlink>
            </div>
        </td>
    </tr>
</table>
<asp:linqdatasource id="LanguageDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" select="new (Language1, LCID)" tablename="Languages">
</asp:linqdatasource>
