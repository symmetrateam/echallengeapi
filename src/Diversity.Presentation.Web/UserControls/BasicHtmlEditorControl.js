﻿var process = 1;
var text = "";
var res = "";

function OnHtmlChanged(s, e) {
    if (process == 2) {
        process = 3;
        text = s.GetHtml();
        if (text == "")
            text = "&nbsp;";
        res = {
            html: text,
            stripFontFamily: true
        };
        s.SetHtml("&nbsp;");
        s.ExecuteCommand(ASPxClientCommandConsts.PASTEFROMWORD_COMMAND, res);
        s.ExecuteCommand(ASPxClientCommandConsts.SELECT_ALL, "", false);
        s.ExecuteCommand(ASPxClientCommandConsts.REMOVEFORMAT_COMMAND, "", false);
    }
}

function OnCommandExecuted(s, e) {
    if (e.commandName == ASPxClientCommandConsts.KBPASTE_COMMAND) {
        process = 2;
        return;
    }
    if (e.commandName == ASPxClientCommandConsts.PASTE_COMMAND) {
        process = 2;
        OnHtmlChanged(s, e);
    }
    if (e.commandName == ASPxClientCommandConsts.PASTEFROMWORD_COMMAND) {
        process = 3;
        s.ExecuteCommand(ASPxClientCommandConsts.PASTEHTML_COMMAND, " ");
        return;
    }
    if (e.commandName == ASPxClientCommandConsts.PASTEHTML_COMMAND) {
        process = 1;
    }
}
