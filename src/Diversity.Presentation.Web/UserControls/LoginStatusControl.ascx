﻿<%@ control language="C#" autoeventwireup="true" codebehind="LoginStatusControl.ascx.cs"
    inherits="Diversity.Presentation.Web.UserControls.LoginStatusControl" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<div style="float:right;">
    <table>
        <tr>
            <td align="right">
                <dx:aspxcombobox id="LanguageDropDownList" runat="server" datasourceid="LanguageDataSource"
                    autopostback="true" valuefield="LCID" valuetype="System.String" textfield="Language1"
                    ondatabound="LanguageDropDownList_DataBound" onselectedindexchanged="LanguageDropDownList_SelectedIndexChanged"
                    dropdownstyle="DropDownList" width="120px">
                </dx:aspxcombobox>
            </td>
            <td>
                <asp:loginview id="LoginStatusCtl" runat="server">
                    <anonymoustemplate>
                        <a href='<%=ResolveUrl("~/Login.aspx") %>'>
                            <asp:literal id="SignInLiteral" runat="server" text="Sign In" /></a>
                    </anonymoustemplate>
                    <loggedintemplate>
                        &nbsp;&nbsp;<asp:literal id="WelcomeLiteral" runat="server" text="Welcome" />
                        <asp:label id="FullNameLabel" runat="server" ></asp:label>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href='<%=ResolveUrl("~/Authentication/ChangePassword.aspx") %>'>
                            <asp:literal id="ChangePasswordLiteral" runat="server" text="Change Password" /></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href='<%=ResolveUrl("~/Authentication/Logout.aspx") %>'>
                            <asp:literal id="SignOutLiteral" runat="server" text="Sign Out" /></a>
                    </loggedintemplate>
                </asp:loginview>
            </td>
        </tr>
    </table>
</div>
<asp:linqdatasource id="LanguageDataSource" runat="server" contexttypename="Diversity.DataModel.SqlRepository.EChallengeDataContext"
    entitytypename="" orderby="Language1" tablename="Languages">
</asp:linqdatasource>
