﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;

namespace Diversity.Presentation.Web.UserControls
{
    public partial class EditListControl : System.Web.UI.UserControl
    {
        private const string CallbackArgumentFormat = "function (s, e) {{ GridCallback.PerformCallback(\"{0}|{1}|\" + {2}); }}"; // key | field | value

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }               

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Items = new List<EditListItem>();
            }

            base.OnInit(e);
        }

        string SessionItemsKey
        {
            get { return string.Format("{0}EditListItems", ClientID); }
        }

        public List<EditListItem> Items
        {
            get
            {
                if (Session[SessionItemsKey] == null)
                {
                    Session[SessionItemsKey] = new List<EditListItem>();
                }

                return Session[SessionItemsKey] as List<EditListItem>;
            }
            set
            {
                Session[SessionItemsKey] = value;
            }
        }

        [DefaultValue("")]
        public string TextColumnCaption
        {
            get { return EditListGridView.Columns["Text"].Caption; }
            set { EditListGridView.Columns["Text"].Caption = value; }
        }


        protected void TextMemo_Init(object sender, EventArgs e)
        {
            var memo = sender as ASPxMemo;
            var container = memo.NamingContainer as GridViewDataItemTemplateContainer;
            memo.ClientInstanceName = string.Format(@"TextMemo_{0}", container.VisibleIndex);
            memo.JSProperties["cp_VisibleIndex"] = container.VisibleIndex;

            memo.ClientSideEvents.ValueChanged = string.Format(CallbackArgumentFormat,
                                                               container.KeyValue,
                                                               container.Column.FieldName,
                                                               "s.GetText()"
                                                               );
        }


        protected void EditListGridView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            var gridView = sender as ASPxGridView;

            if (string.Compare(e.Parameters, "NEW") == 0)
            {
                Items.Add(new EditListItem());
            }

            gridView.DataBind();
        }

        protected void EditListItemDataSource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = Items;
        }

        protected void EditListGridView_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (string.Compare(e.ButtonID, "UpCustomButton") == 0)
            {
                MoveSelection(-1, e.VisibleIndex);
            }
            else if (string.Compare(e.ButtonID, "DownCustomButton") == 0)
            {
                MoveSelection(1, e.VisibleIndex);
            }
            else if (string.Compare(e.ButtonID, "DeleteButton") == 0)
            {
                var gridView = sender as ASPxGridView;
                var item = gridView.GetRow(e.VisibleIndex) as EditListItem;

                if(item != null)
                {
                    Items.Remove(item);
                }

                gridView.DataBind();
            }
        }

        protected void GridCallback_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            var p = e.Parameter.Split('|');

            var key = p[0];
            var field = p[1];
            var value = p[2];

            var item = Items.Find(c => c.ClientId == key);

            switch (field)
            {
                case "Text":
                    if (value == "null")
                    {
                        value = String.Empty;
                    }
                    item.Text = value;
                    break;

            }
        }


        void MoveSelection(int offset, int visibleIndex)
        {
            if (!CanMoveSelection(offset, visibleIndex))
            {
                return;
            }

            var count = Items.Count;
            var temp = new EditListItem[count];

            temp[visibleIndex + offset] = Items[visibleIndex];


            var index = 0;
            for (var i = 0; i < count; i++)
            {
                if (i == visibleIndex)
                {
                    continue;
                }

                while (temp[index] != null)
                {
                    index++;
                }

                temp[index] = Items[i];
                index++;
            }

            Items.Clear();
            Items.AddRange(temp);
            EditListGridView.DataBind();
        }

        bool CanMoveSelection(int offset, int visibleIndex)
        {
            return visibleIndex + offset >= 0 && visibleIndex + offset <= EditListGridView.VisibleRowCount - 1;
        }

        protected void EditListGridView_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            var gridView = sender as ASPxGridView;
            if (e.CellType == GridViewTableCommandCellType.Data)
            {
                if (e.VisibleIndex == 0)
                {
                    if (gridView.VisibleRowCount == 1)
                    {
                        if (string.Compare(e.ButtonID, "UpCustomButton") == 0 ||
                            string.Compare(e.ButtonID, "DownCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                    else
                    {
                        if (string.Compare(e.ButtonID, "UpCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                }
                else
                {
                    if (e.VisibleIndex == gridView.VisibleRowCount - 1)
                    {
                        if (string.Compare(e.ButtonID, "DownCustomButton") == 0)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                }
            }
        }

    }
}