﻿function ConfirmNewPasswordValidate(s,e) {

    if (txtConfirmNewPassword.GetText() == "") {
        e.isValid = false;
        return;
    }

    if (txtConfirmNewPassword.GetText() == txtNewPassword.GetText()) {
        e.isValid = true;
    }
    else {
        e.isValid = false;
    }   
}