﻿var _callbackCommand;
function AddNewRow() {
    _callbackCommand = "NEW";
    EditListGridView.PerformCallback(_callbackCommand);
}

function EditListGridView_EndCallback(s, e) {
    if (_callbackCommand == "NEW") {
        _callbackCommand = "";
    }
}
