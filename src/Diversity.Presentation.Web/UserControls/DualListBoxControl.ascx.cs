﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

namespace Diversity.Presentation.Web.UserControls
{
    public partial class DualListBoxControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);
            base.OnInit(e);
        }


        #region Properties

        /// <summary>
        /// Gets or sets the height
        /// </summary>
        public Unit Height
        {
            get { return ContainerPanel.Height; }
            set { ContainerPanel.Height = value; }
        }

        /// <summary>
        /// Gets or sets the width
        /// </summary>
        public Unit Width
        {
            get { return ContainerPanel.Width; }
            set { ContainerPanel.Width = value; }
        }

        /// <summary>
        /// Gets the left list box itemstyle
        /// </summary>
        public ListBoxItemStyle LeftListBoxItemStyle
        {
            get { return LeftListBox.ItemStyle; }
        }

        /// <summary>
        /// Gets the right list box itemstyle
        /// </summary>
        public ListBoxItemStyle RightListBoxItemStyle
        {
            get { return RightListBox.ItemStyle; }
        }

        /// <summary>
        /// Gets a list of the selected items
        /// </summary>
        public List<ListEditItem> SelectedItems
        {
            get
            {
                return RightListBox.Items.Cast<ListEditItem>().ToList();
            }
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Sets the values for both lists
        /// </summary>
        public void SetInitialValues(List<ListEditItem> completeList, List<ListEditItem> selectedList)
        {
            if (completeList == null)
            {
                throw new ArgumentNullException(@"completeList");
            }

            if (selectedList == null)
            {
                throw new ArgumentNullException(@"selectedList");
            }

            if (completeList.Count() > 0)
            {
                LeftListBox.Items.Clear();
                RightListBox.Items.Clear();

                // add to left box list
                foreach (var item in completeList)
                {
                    LeftListBox.Items.Add(item);
                }

                // add hidden box items
                var options = new object[LeftListBox.Items.Count];
                for (var i = 0; i < LeftListBox.Items.Count; i++)
                {
                    options[i] = LeftListBox.Items[i].Value;
                }

                hiddenField["options"] = options;

                if (selectedList.Count() > 0)
                {
                    foreach (var item in selectedList)
                    {
                        if (item != null)
                        {
                            if (RightListBox.Items.FindByValue(item.Value) == null)
                            {
                                RightListBox.Items.Add(item);

                                var removeItem = LeftListBox.Items.FindByValue(item.Value);
                                LeftListBox.Items.Remove(removeItem);
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}