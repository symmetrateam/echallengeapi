﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Diversity.Data.DataAccess;
using Diversity.Data.DataAccess.SqlServer;

namespace Diversity.Presentation.Web
{
    public class CultureHelper
    {
        public static void SetupCulture(string languageLcid)
        {
            SetupCulture(Membership.GetUser().UserName, languageLcid);
        }

        public static void SetupCulture(string userName, string languageLcid)
        {
            var profile = AccountProfile.GetProfile(userName);
            profile.Culture = languageLcid;

            if (!string.IsNullOrEmpty(languageLcid))
            {
                ILanguageRepository languageRepository = new SqlLanguageRepository();
                var language = languageRepository.GetLanguages().Where(l => l.LCID == languageLcid).FirstOrDefault();
                profile.PageLayoutDirection = language.IsLeftToRight ? "LTR" : "RTL";
            }            
        }
    }
}