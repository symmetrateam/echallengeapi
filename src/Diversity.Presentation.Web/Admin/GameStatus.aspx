﻿<%@ Page Title="Game Status" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GameStatus.aspx.cs" Inherits="Diversity.Presentation.Web.Admin.GameStatus" %>
<%@ Register src="../UserControls/Admin/GameStatusControl.ascx" tagname="GameStatusControl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <uc1:GameStatusControl ID="GameStatusControl" runat="server" />
</asp:Content>
