﻿<%@ page title="Card" language="C#" autoeventwireup="true" codebehind="Card.aspx.cs" inherits="Diversity.Presentation.Web.Admin.Card"
    masterpagefile="~/Site.Master" %>
<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<%@ register src="../UserControls/EditListControl.ascx" tagname="EditListControl"
    tagprefix="uc1" %>
<%@ register src="../UserControls/Admin/CardManagementControl.ascx" tagname="CardManagementControl"
    tagprefix="uc2" %>

<asp:content id="Content1" contentplaceholderid="head" runat="server">
</asp:content>
<asp:content id="Content2" contentplaceholderid="MainContentPlaceHolder" runat="server">
    <uc2:cardmanagementcontrol id="CardManagementControl1" runat="server" />
</asp:content>
