﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Diversity.Presentation.Web.Admin
{
    public partial class Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Do not redirect from this page if the request did not come from the Login page
            if (Request.UrlReferrer == null || Request.UrlReferrer.AbsolutePath != "/Login.aspx")
            {
                return;
            }

            if (Page.User.IsInRole(GlobalConstants.ADMINISTRATORS_ROLE) ||
                Page.User.IsInRole(GlobalConstants.PAGEVIEW_MANAGEGAMES))
            {
                Response.Redirect(@"~/Admin/Games.aspx", true);
            }
        }
    }
}