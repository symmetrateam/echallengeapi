﻿<%@ Page Title="Bulk User Upload" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BulkUploadUsers.aspx.cs" Inherits="Diversity.Presentation.Web.Admin.BulkUploadUsers" %>
<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<%@ Register src="../UserControls/Admin/BulkUserImportControl.ascx" tagname="BulkUserImportControl" tagprefix="uc1" %>
<asp:content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" 
    runat="server">
    <uc1:bulkuserimportcontrol id="BulkUserImportControl1" runat="server" />
</asp:content>
