﻿<%@ Page Title="Manage Games" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Games.aspx.cs" Inherits="Diversity.Presentation.Web.Admin.Games" %>
<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<%@ Register src="../UserControls/Admin/GameListControl.ascx" tagname="GameListControl" tagprefix="uc1" %>
<asp:content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" 
    runat="server">
    <uc1:gamelistcontrol id="GameListControl1" runat="server" />
</asp:content>
