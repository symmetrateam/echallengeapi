﻿<%@ Page Title="Client User Groups" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientUserGroups.aspx.cs" Inherits="Diversity.Presentation.Web.Admin.ClientUserGroups" %>
<%@ Mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<%@ Register src="../UserControls/Admin/ClientUserGroupManagementControl.ascx" tagname="ClientUserGroupManagementControl" tagprefix="uc1" %>
<asp:content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" 
    runat="server">
    <uc1:clientusergroupmanagementcontrol id="ClientUserGroupManagementControl1" 
        runat="server" />
</asp:content>
