﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Diversity.DataModel.SqlRepository;

namespace Diversity.Presentation.Web
{
    public class ListHelper
    {
        public static List<Internal_CardType> CardTypes
        {
            get
            {
                var dataContext = new EChallengeDataContext();

                var results = (from i in dataContext.Internal_CardTypes
                               select i).ToList();
                return results;
            }   
        }
    }
}