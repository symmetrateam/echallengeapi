﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Diversity.Presentation.Web.MyResources.Errors
{
    public partial class Exception : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var exception = HttpContext.Current.Error;

            while(exception.InnerException != null)
            {
                exception = exception.InnerException;
            }

            DetailedMessageLabel.Text = exception.Message;

            HttpContext.Current.ClearError();
        }
    }
}