﻿<%@ Page Title="Access Denied" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AccessDenied.aspx.cs" Inherits="Diversity.Presentation.Web.MyResources.Errors.AccessDenied" %>

<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div>
        <p>
            <dx:aspxlabel id="MessageLine1Label" runat="server" text="You have attempted to access a page that you are not authorized to view." />
            <br />
            <dx:aspxlabel id="MessageLine2Label" runat="server" text="If you have any questions, please contact the site administrator." />
            <br />
            <dx:aspxlabel id="UseMenusLabel" runat="server" text="Use the menus above to navigate to the correct page or" />&nbsp;<a href='<% =ResolveUrl("~/Default.aspx") %>'> <dx:aspxlabel id="ReturnHomeLabel" runat="server" text="return to the home page." /></a>
        </p>
    </div>
</asp:Content>
