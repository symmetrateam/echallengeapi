﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Diversity.Presentation.Web.MyResources.Errors
{
    public partial class UIInformationMessage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var exceptionCode = Request.QueryString["code"];
            var resourceManager = Resources.Exceptions.ResourceManager;
            var message = resourceManager.GetString(exceptionCode);
            MessageLabel.Text = message;
        }
    }
}