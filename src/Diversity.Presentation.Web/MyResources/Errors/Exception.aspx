﻿<%@ page title="An error has occurred" language="C#" masterpagefile="~/Site.Master"
    autoeventwireup="true" codebehind="Exception.aspx.cs" inherits="Diversity.Presentation.Web.MyResources.Errors.Exception" %>

<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<asp:content id="Content1" contentplaceholderid="head" runat="server">
</asp:content>
<asp:content id="Content2" contentplaceholderid="MainContentPlaceHolder" runat="server">
    <div>
        <p>
            <dx:aspxlabel id="MessageLabel" runat="server" text="An unexpected error has occured in the application. The website administrator has been notified." />
        </p>
        <ul>
            <li>
                <dx:aspxhyperlink id="ReturnToHomeHyperLink" runat="server" navigateurl="~/Default.aspx"
                    text="Return to home page">
                </dx:aspxhyperlink>
            </li>
        </ul>
        <p>
            <b>
                <dx:aspxlabel id="DetailedMessageHeaderLabel" runat="server" text="The detailed exception message is as follows:">
                </dx:aspxlabel>
            </b>
            <br />
            <dx:aspxlabel id="DetailedMessageLabel" runat="server" text="">
            </dx:aspxlabel>
        </p>
    </div>
</asp:content>
