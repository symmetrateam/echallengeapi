﻿<%@ Page Title="Page Not Found" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="Diversity.Presentation.Web.MyResources.Errors._404" %>

<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<asp:content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" 
    runat="server">
    <div>
        <p>
            <dx:aspxlabel id="PageCannotBeFoundLabel" runat="server" text="The requested page could not be found." />
            <br />
            <br />
            <dx:aspxlabel id="UseMenusToNavigateLabel" runat="server" text="Use the menus above to navigate to the correct page or"/>&nbsp;<a href='<% =ResolveUrl("~/Default.aspx") %>'><dx:aspxlabel id="ReturnToHomePageLabel" runat="server" text="return to the home page."/></a>
        </p>
    </div>
</asp:content>
