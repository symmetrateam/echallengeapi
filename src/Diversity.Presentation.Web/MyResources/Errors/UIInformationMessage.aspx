﻿<%@ Page Title="Information Message" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UIInformationMessage.aspx.cs" Inherits="Diversity.Presentation.Web.MyResources.Errors.UIInformationMessage" %>
<%@ register assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    namespace="DevExpress.Web" tagprefix="dx" %>
<%@ mastertype typename="Diversity.Presentation.Web.BaseMasterPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
<div>
        <p>
            <dx:aspxlabel id="MessageLabel" runat="server" text="An unexpected error has occured in the application. The website administrator has been notified." />
        </p>
        <ul>
            <li>
                <dx:aspxhyperlink id="ReturnToHomeHyperLink" runat="server" navigateurl="~/Default.aspx"
                    text="Return to home page">
                </dx:aspxhyperlink>
            </li>
        </ul>
    </div>
</asp:Content>
