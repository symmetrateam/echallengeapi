﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace Diversity.Presentation.Web
{
    public class BasePage : System.Web.UI.Page
    {
        protected void PageLoad(object sender, EventArgs e)
        {
            if(!IsCallback)
            {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }

        #region Member Variables

        private AccountProfile _currentContext;
        
        #endregion

        public BasePage()
        {
            if (_currentContext == null)
            {
                _currentContext = AccountProfile.GetProfile();
            }
        }

        #region Properties

        public AccountProfile CurrentContext
        {
            get { return _currentContext; }
        }

        #endregion        

        #region Overrides

        protected override void InitializeCulture()
        {
            if (CurrentContext == null)
            {
                base.InitializeCulture();
                return;
            }

            //Use this
            this.UICulture = CurrentContext.Culture;
            this.Culture = CurrentContext.Culture;

            if (CurrentContext.Culture != "Auto")
            {
                var cultureInfo = new CultureInfo(CurrentContext.Culture);
                System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
                System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
            }            
        }

        #endregion        
    }
}