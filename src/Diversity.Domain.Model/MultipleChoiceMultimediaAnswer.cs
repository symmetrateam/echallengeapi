﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MultipleChoiceMultimediaAnswer: MultipleChoiceAnswer
    {
        [DataMember]
        public string PossibleAnswerMultimediaURL { get; set; }

        public string PossibleAnswerMultimediaURLFriendlyName { get; set; }
    }
}
