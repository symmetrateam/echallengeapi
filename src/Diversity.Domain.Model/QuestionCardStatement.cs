﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class QuestionCardStatement
    {
        public int QuestionCardStatementId { get; set; }

        public int QuestionCardId { get; set; }

        [DataMember]
        public int SequenceNo { get; set; }

        [DataMember]
        public string Statement { get; set; }
    }
}
