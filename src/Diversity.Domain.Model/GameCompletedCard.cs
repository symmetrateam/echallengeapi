﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class GameCompletedCard : Card
    {
        [DataMember]
        public QuestionExplain Question { get; set; }
    }
}
