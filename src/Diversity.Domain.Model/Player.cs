﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class Player
    {
        /// <summary>
        /// Gets or sets the players real name
        /// </summary>        
        [DataMember]
        public string RealName { get; set; }
        //[DataMember(Name="RealName", EmitDefaultValue=false)]
        //CDataWrapper RealNameCDATA
        //{
        //    get { return RealName; }
        //    set { RealName = value; }
        //}

        /// <summary>
        /// Gets or sets the players first name
        /// </summary>
        [DataMember]
        public string PlayerFirstName { get; set; }

        /// <summary>
        /// Gets or sets the players last name
        /// </summary>
        [DataMember]
        public string PlayerLastName { get; set; }

        /// <summary>
        /// Gets or sets the players avatar name
        /// </summary>        
        [DataMember]
        public string AvatarName { get; set; }
        //[DataMember(Name="AvatarName", EmitDefaultValue = false)]
        //CDataWrapper AvatarNameCDATA
        //{
        //    get { return AvatarName; }
        //    set { AvatarName = value; }
        //}

        /// <summary>
        /// Gets or sets the avatar data - should actually read AvatarImage
        /// </summary>        
        [DataMember]
        public string AvatarData { get; set; }
        //[DataMember(Name="AvatarData", EmitDefaultValue = false)]
        //CDataWrapper AvatarDataCDATA
        //{
        //    get { return AvatarData; }
        //    set { AvatarData = value; }
        //}

        /// <summary>
        /// Gets or sets the avatar gender
        /// </summary>
        [DataMember]
        public string AvatarGender { get; set; }
        //[DataMember(Name="AvatarGender", EmitDefaultValue = false)]
        //CDataWrapper AvatarGenderCDATA
        //{
        //    get { return AvatarGender; }
        //    set { AvatarGender = value; }
        //}

        /// <summary>
        /// Gets or sets the unique identifier
        /// </summary>
        [DataMember]
        public Guid? Id { get; set; }
        /// <summary>
        /// Gets or sets whether the player is simulated
        /// </summary>
        [DataMember]
        public bool IsSimulated { get; set; }
        /// <summary>
        /// Gets or sets whether the player has view the game tutorial
        /// </summary>
        [DataMember]
        public bool HasViewedGameTutorial { get; set; }
    }
}
