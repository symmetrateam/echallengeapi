﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class BranchingCard: Card
    {
        public BranchingCard()
        {
            BranchingCardButtons = new List<BranchingCardButton>();
        }

        [DataMember]
        public QuestionExplain Question { get; set; }
        [DataMember]
        public bool MustPressButtonsInSequence { get; set; }        
        [DataMember]
        public bool MustAddBackgroundBorderToBranches { get; set; }        
        [DataMember]
        public int MinButtonPresses { get; set; }
        [DataMember]
        public string ButtonOrientation { get; set; }
        [DataMember]
        public int BranchingCompletedCardGroupId { get; set; }
        [DataMember]
        public List<BranchingCardButton> BranchingCardButtons { get; set; }
    }
}
