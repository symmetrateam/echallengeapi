﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Diversity.Common.Enumerations;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]

    [KnownType(typeof(ThemeIntroCard))]

    [XmlInclude(typeof(ThemeIntroCard))]
    public class StatementListCard: Card
    {
        [DataMember]
        public List<QuestionCardStatement> StatementListItems { get; set; }

        [DataMember]
        public string Description { get; set; }        

        [DataMember]
        public string StatementListHeader { get; set; }

        [DataMember]
        public string StatementMultimediaUrl { get; set; }       
    }
}
