﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class Client
    {
        [DataMember]
        public string ClientLogoImageUrl { get; set; }

        public string ClientCode { get; set; }
    }
}
