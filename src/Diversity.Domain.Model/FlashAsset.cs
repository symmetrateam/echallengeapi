﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    /// <summary>
    /// This class represents a flash asset item
    /// </summary>
    [DataContract(Namespace = "")]
    public class FlashAsset
    {
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string Lcid { get; set; }
    }
}
