﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class ArrangeSequencePlayerResult : PlayerResult
    {
        private IList<PlayerResultAnswer> _results;

        public ArrangeSequencePlayerResult()
        {
            _results = new List<PlayerResultAnswer>();
        }

        [DataMember]
        public IList<PlayerResultAnswer> Results
        {
            get { return _results; }
            set { _results = value; }
        }

        [DataMember]
        public int TimeTakenToAnswerInSeconds { get; set; }
    }
}
