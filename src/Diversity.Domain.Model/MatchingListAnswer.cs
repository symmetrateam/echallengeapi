﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MatchingListAnswer : Answer
    {
        public bool IsCorrectAnswer { get; set; }

        [DataMember]
        public string LHStatement { get; set; }
        //[DataMember(Name="LHStatement", EmitDefaultValue=false)]
        //CDataWrapper LHStatementCDATA
        //{
        //    get { return LHStatement; }
        //    set { LHStatement = value; }
        //}

        [DataMember]
        public string RHStatement { get; set; }
        //[DataMember(Name = "RHStatement", EmitDefaultValue = false)]
        //CDataWrapper RHStatementCDATA
        //{
        //    get { return RHStatement; }
        //    set { RHStatement = value; }
        //}
    }
}
