﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    [KnownType(typeof(ArrangeSequencePlayerResultAnswer))]
    [KnownType(typeof(MatchingListPlayerResultAnswer))]
    [KnownType(typeof(MultipleCheckBoxPlayerResultAnswer))]
    [KnownType(typeof(VotingPlayerResultAnswer))]
    public class PlayerResultAnswer
    {
    }
}
