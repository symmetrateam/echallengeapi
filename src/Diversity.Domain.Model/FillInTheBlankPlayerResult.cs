﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class FillInTheBlankPlayerResult : PlayerResult
    {
        [DataMember]
        public int AnswerId { get; set; }

        [DataMember]
        public int TimeTakenToAnswerInSeconds { get; set; }

    }
}
