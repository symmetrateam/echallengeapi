﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class CompanyPolicyCard : Card
    {        
        public string CompanyPolicyAssociatedImageUrl { get; set; }
        //[DataMember(Name="CompanyPolicyAssociatedImageUrl", EmitDefaultValue= false)]
        //CDataWrapper CompanyPolicyAssociatedImageUrlCDATA
        //{
        //    get { return CompanyPolicyAssociatedImageUrl; }
        //    set { CompanyPolicyAssociatedImageUrl = value; }
        //}
    }
}
