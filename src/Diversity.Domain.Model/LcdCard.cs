﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class LcdCard : Card
    {      
        [DataMember]
        public string LcdCategory { get; set; }
        //[DataMember(Name = "LcdCategory", EmitDefaultValue = false)]
        //CDataWrapper LcdCategoryCDATA
        //{
        //    get { return LcdCategory; }
        //    set { LcdCategory = value; }
        //}
        
        public string LcdAssociatedImageUrl { get; set; }
        //[DataMember(Name="LcdAssociatedImageUrl", EmitDefaultValue = false)]
        //CDataWrapper LcdAssociatedImageUrlCDATA
        //{
        //    get { return LcdAssociatedImageUrl; }
        //    set { LcdAssociatedImageUrl = value; } 
        //}
    }
}
