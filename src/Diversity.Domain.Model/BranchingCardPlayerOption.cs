﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class BranchingCardPlayerOption
    {
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int GameCardGroupSequenceID { get; set; }
    }
}
