﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MultipleChoiceAnswer : Answer
    {
        [DataMember]
        public bool IsCorrectAnswer { get; set; }

        [DataMember]
        public string PossibleAnswer { get; set; }
        //[DataMember(Name="PossibleAnswer", EmitDefaultValue = false)]
        //CDataWrapper PossibleAnswerCDATA
        //{
        //    get { return PossibleAnswer; }
        //    set { PossibleAnswer = value; }
        //}
    }
}
