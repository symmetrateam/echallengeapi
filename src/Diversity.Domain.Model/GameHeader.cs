﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{    
    [DataContract(Namespace = "")]
    [KnownType(typeof(GameTemplateInstance))]
    [KnownType(typeof(GameInstance))]
    public class GameHeader
    {
        [DataContract(Namespace = "")]
        public struct GameSettingsInfo
        {
            /// <summary>
            /// Gets or sets whether the game is an online version
            /// </summary>
            [DataMember]
            public bool IsOnline { get; set; }
            /// <summary>
            /// Gets or sets if game is a template
            /// </summary>
            [DataMember]
            public bool IsTemplate { get; set; }

            public bool GameNowLockedDown { get; set; }

            public bool GameConfigCanBeModifiedAcrossInstances { get; set; }

            /// <summary>
            /// Gets or sets whether game is timed
            /// </summary>
            [DataMember]
            public bool IsTimed { get; set; }
            /// <summary>
            /// Gets or sets whether to use time from card
            /// </summary>
            [DataMember]
            public bool UseCardTime { get; set; }

            public DateTime? AllUsersMustFinishThisGameBy { get; set; }
            /// <summary>
            /// Gets or sets the default game time in seconds
            /// </summary>
            [DataMember]
            public int? DefaultGameTimeInSeconds { get; set; }
            /// <summary>
            /// Gets or sets when all users should complete game by
            /// </summary>
            [DataMember]
            public DateTime? StartDate { get; set; }
            /// <summary>
            /// Gets or sets when all users should complete game by
            /// </summary>
            [DataMember]
            public DateTime? CompletionDate { get; set; }
            /// <summary>
            /// Gets or sets if the tutorial can be skipped
            /// </summary>
            [DataMember]
            public bool CanSkipTutorial { get; set; }
            /// <summary>
            /// Gets or sets the game template name 
            /// </summary>            
            [DataMember]
            public string GameTemplateNameForDashboard { get; set; }

            public bool HasBeenDownloaded { get; set; }

            public int? NumberOfTimesRun { get; set; }

            public string GameThemeData { get; set; }

            /// <summary>
            /// Gets or sets the current status of the game
            /// </summary>
            [DataMember]
            public string CurrentStatus /*Added hack here for JSON return value*/
            {
                get
                {
                    if (IsTemplate)
                    {
                        return GameStatusType.New.ToString();
                    }

                    if (CompletionDate.HasValue)
                    {
                        return GameStatusType.Completed.ToString();
                    }

                    if (StartDate.HasValue && !CompletionDate.HasValue)
                    {
                        return GameStatusType.InProgress.ToString();
                    }

                    return GameStatusType.Unknown.ToString();
                }

                private set { ; }
            }            



        }


        /// <summary>
        /// Gets or sets the game identifier
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the settings for the game
        /// </summary>
        [DataMember]
        public GameSettingsInfo GameSettings { get; set; }

        /// <summary>
        /// Gets or set the percentage of the game completed
        /// </summary>
        public int PercentCompleted { get; set; }

        /// <summary>
        /// Gets or sets the total cards in game
        /// </summary>
        [DataMember]
        public int TotalCardsInGame { get; set; }

        /// <summary>
        /// Gets or sets the total cards viewed in game
        /// </summary>
        [DataMember]
        public int TotalCardsViewed { get; set; }

    }
}
