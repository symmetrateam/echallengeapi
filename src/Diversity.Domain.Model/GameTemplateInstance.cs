﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{   
    [DataContract(Namespace = "", Name="GameTemplate")]
    public class GameTemplateInstance : GameHeader
    {
        private IList<GameInstance> _games;
  
        public GameTemplateInstance()
        {
            _games = new List<GameInstance>();
        }

        /// <summary>
        /// Gets or sets a list of the games 
        /// </summary>
        [DataMember]
        public IList<GameInstance> Games
        {
            get { return _games; }
            set { _games = value; }
        }

        [DataMember]
        public int NumberOfTimesGameCanBePlayed { get; set; }
    }
}
