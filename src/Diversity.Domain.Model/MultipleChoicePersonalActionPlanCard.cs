﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MultipleChoicePersonalActionPlanCard : Card
    {
        private IList<Answer> _possibleAnswers;
        private IList<PlayerResult> _results;

        public MultipleChoicePersonalActionPlanCard()
        {
            _possibleAnswers = new List<Answer>();
            _results = new List<PlayerResult>();
        }

        [DataMember]
        public bool IsAMandatoryQuestion { get; set; }

        [DataMember]
        public bool OfferOtherOption { get; set; }

        [DataMember]
        public bool OtherOptionIsTextBoxElseTextArea { get; set; }

        [DataMember]
        public bool? OfferOptOutChoice { get; set; }

        [DataMember]
        public QuestionExplain Question { get; set; }

        [DataMember]
        public IList<Answer> PossibleAnswers
        {
            get { return _possibleAnswers; }
            set { _possibleAnswers = value; }
        }

        [DataMember]
        public IList<PlayerResult> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }

}
