﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class Language
    {
        /// <summary>
        /// The unique identifier of the language
        /// </summary>
        [DataMember]
        public int Id { get; set; }
        
        /// <summary>
        /// Gets or sets the long version of the language name
        /// </summary>        
        [DataMember]
        public string Name { get; set; }
        //[DataMember(Name="Name", EmitDefaultValue = false)]
        //CDataWrapper NameCDATA
        //{
        //    get { return Name; }
        //    set { Name = value; }
        //}

        /// <summary>
        /// Gets or sets the local id
        /// </summary>        
        [DataMember]
        public string LCID { get; set; }
        //[DataMember(Name="LCID", EmitDefaultValue = false)]
        //CDataWrapper LCIDCDATA
        //{
        //    get { return LCID; }
        //    set { LCID = value; }
        //}


        /// <summary>
        /// Gets or sets whether the language is a left to right language
        /// </summary>
        [DataMember]
        public bool IsLeftToRight { get; set; }
    }
}
