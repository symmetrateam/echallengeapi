﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MultipleCheckBoxPlayerResult : PlayerResult, IFeedbackResult
    {
        private IList<PlayerResultAnswer> _results;

        public MultipleCheckBoxPlayerResult()
        {
            _results = new List<PlayerResultAnswer>();
        }
            
        [DataMember]
        public IList<PlayerResultAnswer> Results
        {
            get { return _results; }
            set { _results = value; }
        }

        [DataMember]
        public int TimeTakenToAnswerInSeconds { get; set; }
    }
}
