﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    [KnownType(typeof(MultipleCheckBoxPlayerResult))]
    [KnownType(typeof(MultipleChoicePlayerResult))]
    [KnownType(typeof(ArrangeSequencePlayerResult))]
    [KnownType(typeof(MatchingListPlayerResult))]
    [KnownType(typeof(FillInTheBlankPlayerResult))]
    [KnownType(typeof(NumericPlayerResult))]
    [KnownType(typeof(TextPlayerResult))]
    [KnownType(typeof(VotingPlayerResult))]
    [KnownType(typeof(BranchingCardPlayerResult))]
    [KnownType(typeof(VotingCardFeedbackResult))]
    public class PlayerResult
    {
        [DataMember]
        public Guid PlayerId { get; set; }
    }
}
