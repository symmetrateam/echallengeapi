﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class BranchingCardPlayerResult: PlayerResult
    {
        private List<BranchingCardPlayerOption> _branchingCardPlayerOptions;

        public BranchingCardPlayerResult()
        {
            BranchingCardPlayerOptions = new List<BranchingCardPlayerOption>();
        }

        [DataMember]
        public List<BranchingCardPlayerOption> BranchingCardPlayerOptions
        {
            get { return _branchingCardPlayerOptions; }
            set { _branchingCardPlayerOptions = value; }
        }
    }
}
