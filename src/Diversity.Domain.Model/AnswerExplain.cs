﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class AnswerExplain
    {
        [DataMember]
        public string AnswerExplanation { get; set; }
        //[DataMember(Name="AnswerExplanation", EmitDefaultValue=false)]
        //private CDataWrapper AnswerExplanationCDATA
        //{
        //    get { return AnswerExplanation; }
        //    set { AnswerExplanation = value; }
        //}
        
        [DataMember]
        public string AnswerMultimediaUrl { get; set; }
        //[DataMember(Name="AnswerMultimediaUrl", EmitDefaultValue = false)]
        //CDataWrapper AnswerMultimediaUrlCDATA
        //{
        //    get { return AnswerMultimediaUrl; }
        //    set { AnswerMultimediaUrl = value; }
        //}
        
        [DataMember]
        public string AnswerMultimediaTypeImageVideoEtc { get; set; }
        //[DataMember(Name = "AnswerMultimediaTypeImageVideoEtc", EmitDefaultValue = false)]
        //CDataWrapper AnswerMultimediaTypeImageVideoEtcCDATA
        //{
        //    get { return AnswerMultimediaTypeImageVideoEtc; }
        //    set { AnswerMultimediaTypeImageVideoEtc = value; }
        //}

        [DataMember]
        public bool AnswerMultimediaStartAutoElsePushPlayToStart { get; set; }

        [DataMember]
        public bool AnswerMultimediaShowPlaybackControls { get; set; }
    }
}
