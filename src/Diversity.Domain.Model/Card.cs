﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Diversity.Common.Enumerations;


namespace Diversity.Domain.Model
{
    /// <summary>
    /// This class describes a card entity
    /// </summary>
    [DataContract(Namespace = "")]
    [KnownType(typeof(MultipleChoiceSurveyCard))]
    [KnownType(typeof(MultipleCheckBoxSurveyCard))]
    [KnownType(typeof(TextBoxSurveyCard))]
    [KnownType(typeof(TextAreaSurveyCard))]
    [KnownType(typeof(LcdCard))]
    [KnownType(typeof(MultipleChoiceAuditStatementCard))]
    [KnownType(typeof(LeadingQuestionCard))]
    [KnownType(typeof(CompanyPolicyCard))]
    [KnownType(typeof(MultipleCheckBoxQuestionCard))]
    [KnownType(typeof(MultipleChoiceQuestionCard))]
    [KnownType(typeof(FillInTheBlankQuestionCard))]
    [KnownType(typeof(ArrangeSequenceQuestionCard))]
    [KnownType(typeof(PotLuckQuestionCard))]
    [KnownType(typeof(MatchingListQuestionCard))]
    [KnownType(typeof(NumericQuestionCard))]
    [KnownType(typeof(MultipleCheckBoxPriorityFeedbackCard))]
    [KnownType(typeof(MultipleChoicePriorityFeedbackCard))]
    [KnownType(typeof(TextAreaPriorityFeedbackCard))]
    [KnownType(typeof(PriorityIssueVotingCard))]
    [KnownType(typeof(TextAreaPersonalActionPlanCard))]
    [KnownType(typeof(TextBoxPersonalActionPlanCard))]
    [KnownType(typeof(MultipleCheckBoxPersonalActionPlanCard))]
    [KnownType(typeof(MultipleChoicePersonalActionPlanCard))]
    [KnownType(typeof(ScoreboardCard))]
    [KnownType(typeof(RoundResultCard))]
    [KnownType(typeof(AwardCard))]
    [KnownType(typeof(CertificateCard))]
    [KnownType(typeof(GameCompletedCard))]
    [KnownType(typeof(IntroSurveyCard))]
    [KnownType(typeof(TransitionUpCard))]
    [KnownType(typeof(TransitionDownCard))]
    [KnownType(typeof(MultipleChoiceImageQuestionCard))]
    [KnownType(typeof(MultipleChoiceAuditStatementImageCard))]
    [KnownType(typeof(GameIntroCard))]
    [KnownType(typeof(ThemeIntroCard))]
    [KnownType(typeof(MultiMediaQuestionCard))]
    [KnownType(typeof(MultipleCheckBoxImageQuestionCard))]
    [KnownType(typeof(BranchingCard))]

    [XmlInclude(typeof(MultipleChoiceSurveyCard))]
    [XmlInclude(typeof(MultipleCheckBoxSurveyCard))]
    [XmlInclude(typeof(TextBoxSurveyCard))]
    [XmlInclude(typeof(TextAreaSurveyCard))]
    [XmlInclude(typeof(LcdCard))]
    [XmlInclude(typeof(MultipleChoiceAuditStatementCard))]
    [XmlInclude(typeof(LeadingQuestionCard))]
    [XmlInclude(typeof(CompanyPolicyCard))]
    [XmlInclude(typeof(MultipleCheckBoxQuestionCard))]
    [XmlInclude(typeof(MultipleChoiceQuestionCard))]
    [XmlInclude(typeof(FillInTheBlankQuestionCard))]
    [XmlInclude(typeof(ArrangeSequenceQuestionCard))]
    [XmlInclude(typeof(PotLuckQuestionCard))]
    [XmlInclude(typeof(MatchingListQuestionCard))]
    [XmlInclude(typeof(NumericQuestionCard))]
    [XmlInclude(typeof(MultipleCheckBoxPriorityFeedbackCard))]
    [XmlInclude(typeof(MultipleChoicePriorityFeedbackCard))]
    [XmlInclude(typeof(TextAreaPriorityFeedbackCard))]
    [XmlInclude(typeof(PriorityIssueVotingCard))]
    [XmlInclude(typeof(TextAreaPersonalActionPlanCard))]
    [XmlInclude(typeof(TextBoxPersonalActionPlanCard))]
    [XmlInclude(typeof(MultipleCheckBoxPersonalActionPlanCard))]
    [XmlInclude(typeof(MultipleChoicePersonalActionPlanCard))]
    [XmlInclude(typeof(ScoreboardCard))]
    [XmlInclude(typeof(RoundResultCard))]
    [XmlInclude(typeof(AwardCard))]
    [XmlInclude(typeof(CertificateCard))]
    [XmlInclude(typeof(GameCompletedCard))]
    [XmlInclude(typeof(IntroSurveyCard))]
    [XmlInclude(typeof(TransitionUpCard))]
    [XmlInclude(typeof(TransitionDownCard))]
    [XmlInclude(typeof(MultipleChoiceImageQuestionCard))]
    [XmlInclude(typeof(MultipleChoiceAuditStatementImageCard))]
    [XmlInclude(typeof(GameIntroCard))]
    [XmlInclude(typeof(ThemeIntroCard))]
    [XmlInclude(typeof(MultiMediaQuestionCard))]    
    [XmlInclude(typeof(MultipleCheckBoxImageQuestionCard))]
    [XmlInclude(typeof(BranchingCard))]
    public class Card 
    {               

        /// <summary>
        /// Gets or sets the card uniqueidentifier
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title of the card
        /// </summary>
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the card type
        /// </summary>             
        [DataMember]   
        public string CardType { get; set; }

        /// <summary>
        /// Gets or sets the card type group
        /// </summary>
        [DataMember]
        public string CardTypeGroup { get; set; }

        [DataMember]
        public bool IsATimedCard { get; set; }                                
        
        /// <summary>
        /// Gets or sets the sequence no of the card
        /// </summary>
        [DataMember]
        public int CardSequenceNo { get; set; }        

        public bool UseLongVersion { get; set; }

        [DataMember]
        public bool IsAMandatoryCard { get; set; }

        [DataMember]
        public bool CardViewedInGame { get; set; }

        public DateTime? CardViewedInGameOn { get; set; }

        public int CardGroupSequenceId { get; set; }

        [DataMember]
        public int CardSequenceId { get; set; }

        [DataMember]
        public string Topic { get; set; }

        [DataMember]
        public string SubTopic { get; set; }

        [DataMember]
        public string AudioTranscriptFile { get; set; }

        public string AudioTranscriptFriendlyName { get; set; }

        [DataMember]
        public string AudioTranscriptText { get; set; }


        #region question answer images

        [DataMember]
        public string QuestionMultimediaUrl { get; set; }
        
        [DataMember]
        public string QuestionMultimediaTypeImageVideoEtc { get; set; }
        
        [DataMember]
        public bool QuestionMultimediaStartAutoElsePushPlayToStart { get; set; }

        [DataMember]
        public bool QuestionMultimediaShowPlaybackControls { get; set; }

        [DataMember]
        public string AnswerMultimediaUrl { get; set; }
        
        [DataMember]
        public string AnswerMultimediaTypeImageVideoEtc { get; set; }
        
        [DataMember]
        public bool AnswerMultimediaStartAutoElsePushPlayToStart { get; set; }

        [DataMember]
        public bool AnswerMultimediaShowPlaybackControls { get; set; }

        [DataMember]
        public string QuestionMultimediaThumbnailUrl { get; set; }

        public string QuestionMultimediaThumbnailUrlFriendlyName { get; set; }

        [DataMember]
        public string AnswerExplanationMultimediaThumbnailUrl { get; set; }

        public string AnswerExplanationMultimediaThumbnailUrlFriendlyName { get; set; }

        #endregion
    }
}
