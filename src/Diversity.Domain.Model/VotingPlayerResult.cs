﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace="")]
    public class VotingPlayerResult : PlayerResult
    {
        private IList<PlayerResultAnswer> _results;

        public VotingPlayerResult()
        {
            _results = new List<PlayerResultAnswer>();
        }

        [DataMember]
        public int TimeTakenToAnswerInSeconds { get; set; }

        [DataMember]
        public IList<PlayerResultAnswer> Results
        {
            get { return _results; }
            set { _results = value; }
        }

    }
}
