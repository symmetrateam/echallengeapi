﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MatchingListPlayerResult : PlayerResult
    {
        private IList<PlayerResultAnswer> _results;
        private bool _isAnswerCorrect;

        public MatchingListPlayerResult()
        {
            _results = new List<PlayerResultAnswer>();
        }

        [DataMember]
        public int TimeTakenToAnswerInSeconds { get; set; }

        [DataMember]
        public IList<PlayerResultAnswer> Results
        {
            get { return _results; }
            set { _results = value; }
        }

        [DataMember]
        public bool IsAnswerCorrect
        {
            get { return _isAnswerCorrect; }
            set
            {
                if (Results == null || !Results.Any())
                {
                    _isAnswerCorrect = false;
                    return;
                }

                var matchingResults = Results.Cast<MatchingListPlayerResultAnswer>();
                _isAnswerCorrect = matchingResults.All(m => m.LHAnswerId == m.RHAnswerId && m.LHAnswerId > 0);
            }
        }
        
    }
}
