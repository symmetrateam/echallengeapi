﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class PriorityIssueVotingCard : Card
    {
        private IList<Answer> _possibleAnswers;
        private bool _arrangePriorityList;
        private int _selectTopN;
        private IList<PlayerResult> _results;

        public PriorityIssueVotingCard()
        {
            _possibleAnswers = new List<Answer>();
            _results = new List<PlayerResult>();
        }

        [DataMember]
        public QuestionExplain Question { get; set; }

        [DataMember]
        public IList<Answer> PossibleAnswers
        {
            get { return _possibleAnswers; }
            set { _possibleAnswers = value; }
        }

        public bool UseQuestionCardPrimaryTopics { get; set; }

        [DataMember]
        public int SelectTopN
        {
            get { return _selectTopN; }
            set
            {
                _selectTopN = value;
                _arrangePriorityList = value <= 0;
            }
        }

        [DataMember]
        public bool ArrangePriorityList
        {
            get { return _arrangePriorityList; }
            set { _arrangePriorityList = value; }
        }

        [DataMember]
        public IList<PlayerResult> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
