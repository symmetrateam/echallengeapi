﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class VotingCardFeedbackResult: PlayerResult
    {
        [DataMember]
        public int VotingCardId { get; set; }

        [DataMember]
        public int AnswerId { get; set; }

        [DataMember]
        public IFeedbackResult FeedbackResult { get; set; } 
    }
}
