﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Diversity.Domain.Model
{
    public class MatchingListAnswerStatistic: Statistic
    {
        [DataMember]
        public int RHAnswerId { get; set; }

        [DataMember]
        public int LHAnswerId { get; set; }

        [DataMember]
        public int AnswerCount { get; set; }

        [DataMember]
        public double AnswerPercentage { get; set; }
    }
}
