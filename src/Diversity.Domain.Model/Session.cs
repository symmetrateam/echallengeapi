﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    /// <summary>
    /// This class contacins a list of all games associated to the session 
    /// as well as more general information to create the session
    /// </summary>
    [DataContract(Namespace = "")]
    public class Session
    {
        public Session()
        {         
        }        

        /// <summary>
        /// Gets or sets the session identifier
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the games
        /// </summary>
        [DataMember]
        public Game Game { get; set; }
    }
}
