﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MultipleChoiceImageQuestionCard: Card
    {
         private IList<Answer> _possibleAnswers;
        private IList<PlayerResult> _results;

        public MultipleChoiceImageQuestionCard()
        {
            _possibleAnswers = new List<Answer>();
            _results = new List<PlayerResult>();
        }

        [DataMember]
        public bool IgnoreAnyTimersOnThisCard { get; set; }

        [DataMember]
        public bool UseCardTimeNotGameTime { get; set; }

        [DataMember]
        public int MaximumTimeToAnswerInSeconds { get; set; }

        [DataMember]
        public bool? OfferOptOutChoice { get; set; }

        [DataMember]
        public QuestionExplain Question { get; set; }

        [DataMember]
        public AnswerExplain AnswerExplanation { get; set; }

        [DataMember]
        public IList<Answer> PossibleAnswers
        {
            get { return _possibleAnswers; }
            set { _possibleAnswers = value; }
        }

        [DataMember]
        public OnlineScore Scoring { get; set; }

        [DataMember]
        public IList<PlayerResult> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
