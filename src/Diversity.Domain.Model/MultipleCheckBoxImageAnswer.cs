﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Diversity.Domain.Model
{
     [DataContract(Namespace = "")]
    public class MultipleCheckBoxImageAnswer: MultipleCheckBoxAnswer
    {
        [DataMember]
        public string PossibleAnswerMultimediaURL { get; set; }

        public string PossibleAnswerMultimediaURLFriendlyName { get; set; }
    }
}
