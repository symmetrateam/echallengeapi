﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MultipleCheckBoxPlayerResultAnswer : PlayerResultAnswer
    {
        [DataMember]
        public int AnswerId { get; set; }
    }
}
