﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MultipleChoiceAuditStatementImageCard: Card
    {
        private IList<Answer> _possibleAnswers;
        private IList<PlayerResult> _results;
        private IList<Statistic> _statistics;

        public MultipleChoiceAuditStatementImageCard()
        {
            _possibleAnswers = new List<Answer>();
            _results = new List<PlayerResult>();
            _statistics = new List<Statistic>();
        }

        [DataMember]
        public bool? OfferOptOutChoice { get; set; }

        [DataMember]
        public QuestionExplain Question { get; set; }

        [DataMember]        
        public IList<Answer> PossibleAnswers
        {
            get { return _possibleAnswers; }
            set { _possibleAnswers = value; }
        }

        [DataMember]
        public AnswerExplain AnswerExplanation { get; set; }

        [DataMember]
        public IList<PlayerResult> Results
        {
            get { return _results; }
            set { _results = value; }
        }

        [DataMember]
        public IList<Statistic> Statistics
        {
            get { return _statistics; }
            set { _statistics = value; }
        }

        [DataMember]
        public int TotalAnswerCount { get; set; }
    }
}
