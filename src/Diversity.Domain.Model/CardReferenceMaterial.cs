﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class CardReferenceMaterial
    {      
        [DataMember]
        public string Url { get; set; }
        //[DataMember(Name = "Url", EmitDefaultValue = false)]
        //CDataWrapper UrlCDATA
        //{
        //    get { return Url; }
        //    set { Url = value; }
        //}
    }
}
