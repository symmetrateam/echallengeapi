﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class Dashboard
    {
        private IList<GameTemplateInstance> _games;

        public Dashboard()
        {
            _games = new List<GameTemplateInstance>();
        }

        /// <summary>
        /// Gets or sets a list of the games 
        /// </summary>
        [DataMember]
        public IList<GameTemplateInstance> Games
        {
            get { return _games; }
            set { _games = value; }
        }

        /// <summary>
        /// Gets or sets the current players details
        /// </summary>
        [DataMember]
        public Player Player { get; set; }
    }
}
