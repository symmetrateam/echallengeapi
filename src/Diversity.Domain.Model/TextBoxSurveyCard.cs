﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class TextBoxSurveyCard : Card
    {
        private IList<PlayerResult> _results;

        public TextBoxSurveyCard()
        {
            _results = new List<PlayerResult>();
        }

        [DataMember]
        public bool IsAMandatoryQuestion { get; set; }

        [DataMember]
        public bool OtherOptionIsTextBoxElseTextArea { get; set; }

        [DataMember]
        public QuestionExplain Question { get; set; }

        [DataMember]
        public IList<PlayerResult> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
