﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class ArrangeSequencePlayerResultAnswer : PlayerResultAnswer
    {
        [DataMember]
        public int AnswerId { get; set; }

        [DataMember]
        public int AnswerSequenceNo { get; set; }
    }
}
