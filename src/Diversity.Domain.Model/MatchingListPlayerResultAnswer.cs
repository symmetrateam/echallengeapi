﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class MatchingListPlayerResultAnswer : PlayerResultAnswer
    {
        [DataMember]
        public int LHAnswerId { get; set; }

        [DataMember]
        public int RHAnswerId { get; set; }

        [DataMember]
        public int AnswerSequenceNo { get; set; }
    }
}
