﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    [KnownType(typeof(AuditStatementAnswerStatistic))]
    [KnownType(typeof(MatchingListAnswerStatistic))]

    [XmlInclude(typeof(AuditStatementAnswerStatistic))]    
    [XmlInclude(typeof(MatchingListAnswerStatistic))]
    public class Statistic
    {
        public int CardId { get; set; }

        public int GameTemplateId { get; set; }

        [DataMember]
        public int GeneralAnswerId { get; set; }
    }
}
