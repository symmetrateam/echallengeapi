﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class QuestionExplain
    {
        [DataMember]
        public string QuestionStatement { get; set; }
        //[DataMember(Name = "QuestionStatement", EmitDefaultValue = false)]
        //CDataWrapper QuestionStatementCDATA
        //{
        //    get { return QuestionStatement; }
        //    set { QuestionStatement = value; }
        //}

        [DataMember]
        public string QuestionMultimediaUrl { get; set; }
        //[DataMember(Name = "QuestionMultimediaUrl", EmitDefaultValue = false)]
        //CDataWrapper QuestionMultimediaUrlCDATA
        //{
        //    get { return QuestionMultimediaUrl; }
        //    set { QuestionMultimediaUrl = value; }   
        //}

        [DataMember]
        public string QuestionMultimediaTypeImageVideoEtc { get; set; }
        //[DataMember(Name = "QuestionMultimediaTypeImageVideoEtc", EmitDefaultValue = false)]
        //CDataWrapper QuestionMultimediaTypeImageVideoEtcCDATA
        //{
        //    get { return QuestionMultimediaTypeImageVideoEtc; }
        //    set { QuestionMultimediaTypeImageVideoEtc = value; }
        //}

        [DataMember]
        public bool QuestionMultimediaStartAutoElsePushPlayToStart { get; set; }

        [DataMember]
        public bool QuestionMultimediaShowPlaybackControls { get; set; }

        [DataMember]
        public bool MustShowCompanyResults { get; set; }

        [DataMember]
        public string QuestionStatementPosition { get; set; }
    }
}
