﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class BranchingCardButton
    {
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int ButtonPressedCardGroupId { get; set; }
        [DataMember]
        public bool IsMultiClick { get; set; }
        [DataMember]
        public bool MustReturnToBranchingCard { get; set; }
        
        public int LanguageId { get; set; }
        [DataMember]
        public string ButtonText { get; set; }
        [DataMember]
        public string ButtonImageMultimediaURL { get; set; }
        [DataMember]
        public string ButtonImageFriendlyName { get; set; }
    }
}
