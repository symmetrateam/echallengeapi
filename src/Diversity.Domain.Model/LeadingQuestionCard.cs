﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class LeadingQuestionCard : Card
    {
        [DataMember]
        public QuestionExplain Question { get; set; }
    }
}
