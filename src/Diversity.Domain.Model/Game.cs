﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    /// <summary>
    /// This is a container class for a game
    /// </summary>
    [DataContract(Namespace = "")]
    public class Game : GameHeader
    {
        #region Structs 
        /// <summary>
        /// Contains settings related to an
        /// avatar for the game
        /// </summary>
        [DataContract(Namespace = "")]
        public struct AvatarTurnSettingsInfo
        {
            public AvatarTurnSettingsInfo(bool useActualName, bool useHistoricAnswers, bool useActualAnsweringTimes)
            {
                UseActualName = useActualName;
                UseHistoricAnswers = useHistoricAnswers;
                UseActualAnsweringTimes = useActualAnsweringTimes;
            }

            [DataMember]
            public bool UseActualName;
            [DataMember]
            public bool UseHistoricAnswers;
            [DataMember]
            public bool UseActualAnsweringTimes;
        }

        /// <summary>
        /// Contains settings associated to a players turn in 
        /// the game
        /// </summary>
        [DataContract(Namespace = "")]
        public struct PlayerTurnSettingsInfo
        {
            public PlayerTurnSettingsInfo(bool randomNoRepeatUntillAllHavePlayed, bool bySequenceNoElseRandom)
            {
                RandomNoRepeatingUntilAllHavePlayed = randomNoRepeatUntillAllHavePlayed;
                BySequenceNoElseRandom = bySequenceNoElseRandom;
            }

            [DataMember]
            public bool RandomNoRepeatingUntilAllHavePlayed;
            [DataMember]
            public bool BySequenceNoElseRandom;            
        }


        #endregion

        #region Member Fields

        private IList<CardGroup> _cards;
        private IList<Player> _players;

        #endregion 

        #region Constructors

        public Game()
        {
            _cards = new List<CardGroup>();
            _players = new List<Player>();
        }

        #endregion

        /// <summary>
        /// Gets or sets the settings for avatars used
        /// in this game.
        /// </summary>
        [DataMember]
        public AvatarTurnSettingsInfo AvatarSettings { get; set; }

        /// <summary>
        /// Gets or sets the settings used when it is a players turn in 
        /// the game
        /// </summary>
        [DataMember]
        public PlayerTurnSettingsInfo PlayerTurnSettings { get; set; }

        /// <summary>
        /// Gets a list of cards
        /// </summary>
        [DataMember]
        public IList<CardGroup> CardGroups
        {
            get { return _cards; }
            set { _cards = value; }
        }

        /// <summary>
        /// Gets a list of players
        /// </summary>
        [DataMember]
        public IList<Player> Players
        {
            get { return _players; }
            set { _players = value; }
        }
        
        /// <summary>
        /// Gets or sets a value to indicate whether a game is being resumed
        /// </summary>
        [DataMember]
        public bool IsResumed { get; set; }

        [DataMember]
        public Client Client { get; set; }

        public string CourseCode { get; set; }

        [DataMember]
        public string ThemeKey { get; set; }
    }
}
