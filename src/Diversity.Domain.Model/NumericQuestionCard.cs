﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class NumericQuestionCard : Card
    {
        private IList<Answer> _possibleAnswers;
        private IList<PlayerResult> _results;

        public NumericQuestionCard()
        {
            _possibleAnswers = new List<Answer>();
            _results = new List<PlayerResult>();
        }

        [DataMember]
        public bool IgnoreAnyTimersOnThisCard { get; set; }

        [DataMember]
        public bool UseCardTimeNotGameTime { get; set; }

        [DataMember]
        public int MaximumTimeToAnswerInSeconds { get; set; }

        [DataMember]
        public bool QuestionIsOnlyCorrectIfAllAnswersAreTrue { get; set; }

        [DataMember]
        public bool ApplyScoreByCardElseForEachCorrectAnswer { get; set; }

        [DataMember]
        public bool ApplyNegScoringForEachInCorrectAnswer { get; set; }

        [DataMember]
        public QuestionExplain Question { get; set; }

        [DataMember]
        public AnswerExplain AnswerExplanation { get; set; }

        [DataMember]
        public OnlineScore Scoring { get; set; }

        [DataMember]
        public IList<Answer> PossibleAnswers
        {
            get { return _possibleAnswers; }
            set { _possibleAnswers = value; }
        }

        [DataMember]
        public IList<PlayerResult> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
