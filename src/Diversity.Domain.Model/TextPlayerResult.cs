﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class TextPlayerResult : PlayerResult, IFeedbackResult
    {
        [DataMember]
        public string Answer { get; set; }

        [DataMember]
        public int TimeTakenToAnswerInSeconds { get; set; }
    }
}
