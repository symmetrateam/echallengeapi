﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class AuditStatementAnswerStatistic: Statistic
    {
        [DataMember]
        public int AnswerCount { get; set; }

        [DataMember]
        public double AnswerPercentage { get; set; }
    }
}
