﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace="")]
    public class ErrorMessage
    {
        public int Id { get; set; }
        
        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Description { get; set; }

        public Language Language { get; set; }
    }
}
