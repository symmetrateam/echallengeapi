﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class NumericPlayerResult : PlayerResult
    {
        [DataMember]
        public int TimeTakenToAnswerInSeconds { get; set; }

        [DataMember]
        public float Answer { get; set; }
    }
}
