﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class NumericAnswer : Answer
    {
        [DataMember]
        public bool IsCorrectAnswer { get; set; }

        [DataMember]
        public string Operand { get; set; }
        //[DataMember(Name="Operand", EmitDefaultValue=false)]
        //CDataWrapper OperandCDATA
        //{
        //    get { return Operand; }   
        //    set { Operand = value; }
        //}

        [DataMember]
        public float Value1 { get; set; }

        [DataMember]
        public float Value2 { get; set; }
        
    }
}
