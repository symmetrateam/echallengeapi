﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class OnlineScore
    {
        [DataMember]
        public int OnlinePlayerWinning { get; set; }
        [DataMember]
        public int OnlinePlayerLosing { get; set; }
    }
}
