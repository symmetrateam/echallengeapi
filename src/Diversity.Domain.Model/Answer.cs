﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    [KnownType(typeof(StatementAnswer))]
    [KnownType(typeof(MultipleCheckBoxAnswer))]
    [KnownType(typeof(MultipleChoiceAnswer))]
    [KnownType(typeof(FillInTheBlankAnswer))]
    [KnownType(typeof(MatchingListAnswer))]
    [KnownType(typeof(ArrangeSequenceAnswer))]
    [KnownType(typeof(NumericAnswer))]
    [KnownType(typeof(MultipleChoiceMultimediaAnswer))]
    [KnownType(typeof(MultipleCheckBoxImageAnswer))]
    public class Answer
    {
        /// <summary>
        /// Gets or sets the unique identifier
        /// </summary>
        [DataMember]
        public int Id { get; set; }        
    }
}
