﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace="")]
    public class AuthenticationTicket
    {
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public bool IsFirstLogin { get; set; }
        [DataMember]
        public DateTime ExpiresAt { get; set; }
        [DataMember]
        public string ThemeKey { get; set; }
        public Guid UserId { get; set; }
    }
}
