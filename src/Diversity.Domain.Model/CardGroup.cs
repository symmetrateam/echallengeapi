﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Diversity.Domain.Model
{
    [DataContract(Namespace = "")]
    public class CardGroup
    {
        private IList<Card> _cards;

        public CardGroup()
        {
            _cards = new List<Card>();
        }

        public int Id { get; set; }

        [DataMember]
        public bool IsMandatoryGroup { get; set; }
        
        [DataMember]
        public int CardGroupSequenceNo { get; set; }

        [DataMember]
        public IList<Card> Cards
        {
            get { return _cards; }
            set { _cards = value; }
        }    
    }
}
